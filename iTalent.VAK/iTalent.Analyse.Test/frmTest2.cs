﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iTalent.Analyse.Test
{
    public partial class frmTest2 : Form
    {
        private int i = 0;
        public frmTest2()
        {
            InitializeComponent();
            i = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            i += 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            i += 2;
            MessageBox.Show("2 click " + i);
        }
    }
}
