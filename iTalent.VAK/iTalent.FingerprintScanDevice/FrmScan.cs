﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using ScanAPIHelper;

namespace iTalent.FingerprintScanDevice
{
    public class FrmScan : Form
    {
        private const int BI_BITFIELDS = 3;
        private const int BI_JPEG = 4;
        private const int BI_PNG = 5;
        private const int BI_RGB = 0;
        private const int BI_RLE4 = 2;
        private const int BI_RLE8 = 1;
        private const int CBM_INIT = 4;
        private const int DIB_PAL_COLORS = 1;
        private const int DIB_RGB_COLORS = 0;
        private const int ERROR_BAD_CONFIGURATION = 0x64a;
        private const int ERROR_CALL_NOT_IMPLEMENTED = 120;
        private const int ERROR_INVALID_PARAMETER = 0x57;
        private const int ERROR_MESSAGE_EXCEEDS_MAX_SIZE = 0x10f0;
        private const int ERROR_NO_MORE_ITEMS = 0x103;
        private const int ERROR_NO_SYSTEM_RESOURCES = 0x5aa;
        private const int ERROR_NOT_ENOUGH_MEMORY = 8;
        private const int ERROR_NOT_READY = 0x15;
        private const int ERROR_NOT_SUPPORTED = 50;
        private const int ERROR_TIMEOUT = 0x5b4;
        private const int ERROR_WRITE_PROTECT = 0x13;
        private const int FTR_ERROR_EMPTY_FRAME = 0x10d2;
        private const int FTR_ERROR_FIRMWARE_INCOMPATIBLE = 0x20000005;
        private const int FTR_ERROR_HARDWARE_INCOMPATIBLE = 0x20000004;
        private const int FTR_ERROR_INVALID_AUTHORIZATION_CODE = 0x20000006;
        private const int FTR_ERROR_MOVABLE_FINGER = 0x20000001;
        private const int FTR_ERROR_NO_FRAME = 0x20000002;
        private const int FTR_ERROR_USER_CANCELED = 0x20000003;
        private readonly string _namecustomer;
        private readonly string _reportid;
        private readonly string _tempDirName;
        private readonly string _dirName;
        private readonly IContainer components = null;
        private readonly string _url;
        public bool BContinue;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private CheckBox cL1;
        private CheckBox cL2;
        private CheckBox cL3;
        private CheckBox cL4;
        private CheckBox cL5;
        public int Count_Space;
        private CheckBox cR1;
        private CheckBox cR2;
        private CheckBox cR3;
        private CheckBox cR4;
        private CheckBox cR5;
        public string Fname;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private GroupBox groupBox4;
        private GroupBox groupBox5;
        private Label label1;
        private bool m_bCancelOperation;
        private Button m_btnClose;
        private Button m_btnGetFrame;
        private Button m_btnOpenDevice;
        private CheckBox m_chkDetectFakeFinger;
        private CheckBox m_chkFFD;
        private CheckBox m_chkReceiveLongImage;
        private ComboBox m_cmbInterfaces;
        private byte[] m_Frame;
        private GroupBox m_grpParameters;
        private GroupBox m_grpTests;
        private Device m_hDevice;
        private Label m_lblApiVersion;
        private Label m_lblCompatibility;
        private Label m_lblCurrentImageSize;
        private Label m_lblEEPROMSize;
        private Label m_lblFirmwareVersion;
        private Label m_lblHardwareVersion;
        private Label m_lblImageSize;
        private PictureBox m_picture;
        private Thread newThread;
        private RadioButton oL1;
        private RadioButton oL2;
        private RadioButton oL3;
        private RadioButton oL4;
        private RadioButton oL5;
        private RadioButton oR1;
        private RadioButton oR2;
        private RadioButton oR3;
        private RadioButton oR4;
        private RadioButton oR5;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label;
        private ThreadStart threadDelegate;
        private string _tempfinger;
        private string _tempfinger2;
        //private ConvertImg convert;
        public FrmScan(string reportId, string tempDirName)
        {
            InitializeComponent();
            //convert = new ConvertImg();
            m_btnOpenDevice.Enabled = false;
            m_btnClose.Enabled = false;
            m_grpParameters.Enabled = false;
            m_grpTests.Enabled = false;
            m_hDevice = null;
            _tempDirName = tempDirName;
            _reportid = reportId;
            _url = Directory.GetCurrentDirectory() + @"\Temp\" + _reportid + @"\" + _reportid;

            _tempfinger = Directory.GetCurrentDirectory() + @"\Temp\TempFinger";
            _tempfinger2 = Directory.GetCurrentDirectory() + @"\Temp\TempFinger";
            try
            {
                if (Directory.Exists(_tempfinger))
                    Directory.Delete(_tempfinger, true);
            }
            catch
            {
                //
            }

            Directory.CreateDirectory(_tempfinger);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                BContinue = false;
                var thread = newThread;
                if (thread != null)
                {
                    thread.Abort();
                }
                OnCloseDevice(sender, e);
                Dispose();
                Close();
            }
            catch
            {
                Environment.Exit(0);
            }
        }

        //btn1
        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                var filename = ImageDestSave(_url, "F");
                var temp = ImageDestSave(_tempfinger + @"\" + _reportid, "F");

                Bitmap bmp = new Bitmap(m_picture.Image);
                bmp.Save(temp, ImageFormat.Bmp);

                pictureBox1.Load(temp);
                //Bitmap bmp1 = new Bitmap(pictureBox1.Image);

                MyBitmapFile myFile = new MyBitmapFile(m_hDevice.ImageSize.Width, m_hDevice.ImageSize.Height, m_Frame);
                FileStream file = new FileStream(filename, FileMode.Create);
                file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                file.Close();

                //ConvertImg.ImageConverter(bmp1, filename);//
                //bmp1.Save(filename, ImageFormat.Bmp);
                pictureBox1.Load(filename);

                //pictureBox1.Load(temp);
                //pictureBox1.Image.Save(filename, ImageFormat.Bmp);
                //pictureBox1.Load(filename);

                load_checked();
                button3.Focus();
            }
            catch
            {
                // ignored
            }
        }

        private void button3_Click()
        {
            try
            {
                var filename = ImageDestSave(_url, "L");
                var temp = ImageDestSave(_tempfinger + @"\" + _reportid, "L");
                Bitmap bmp = new Bitmap(m_picture.Image);

                bmp.Save(temp, ImageFormat.Bmp);


                pictureBox2.Load(temp);

                MyBitmapFile myFile = new MyBitmapFile(m_hDevice.ImageSize.Width, m_hDevice.ImageSize.Height, m_Frame);
                FileStream file = new FileStream(filename, FileMode.Create);
                file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                file.Close();

                // Bitmap bmp1 = new Bitmap(pictureBox2.Image);
                //ConvertImg.ImageConverter(bmp1, filename);//
                //bmp1.Save(filename, ImageFormat.Bmp);
                pictureBox2.Load(filename);

                //pictureBox2.Load(temp);
                //pictureBox2.Image.Save(filename, ImageFormat.Bmp);
                //pictureBox2.Load(filename);
            }
            catch
            {
                // ignored
            }
        }

        //btn2
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var filename = ImageDestSave(_url, "L");
                var temp = ImageDestSave(_tempfinger + @"\" + _reportid, "L");
                Bitmap bmp = new Bitmap(m_picture.Image);
                bmp.Save(temp, ImageFormat.Bmp);

                pictureBox2.Load(temp);

                MyBitmapFile myFile = new MyBitmapFile(m_hDevice.ImageSize.Width, m_hDevice.ImageSize.Height, m_Frame);
                FileStream file = new FileStream(filename, FileMode.Create);
                file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                file.Close();

                //Bitmap bmp1 = new Bitmap(pictureBox2.Image);

                //ConvertImg.ImageConverter(bmp1, filename);//
                //bmp1.Save(filename, ImageFormat.Bmp);
                pictureBox2.Load(filename);

                //pictureBox2.Load(temp);
                //pictureBox2.Image.Save(filename, ImageFormat.Bmp);
                //pictureBox2.Load(filename);
                load_checked();
                button4.Focus();
            }
            catch
            {
                // ignored
            }
        }

        private void button4_Click()
        {
            try
            {
                var filename = ImageDestSave(_url, "R");
                var temp = ImageDestSave(_tempfinger + @"\" + _reportid, "R");
                Bitmap bmp = new Bitmap(m_picture.Image);
                bmp.Save(temp, ImageFormat.Bmp);

                pictureBox3.Load(temp);

                MyBitmapFile myFile = new MyBitmapFile(m_hDevice.ImageSize.Width, m_hDevice.ImageSize.Height, m_Frame);
                FileStream file = new FileStream(filename, FileMode.Create);
                file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                file.Close();

                // Bitmap bmp1 = new Bitmap(pictureBox3.Image);
                // ConvertImg.ImageConverter(bmp1, filename);//
                //bmp1.Save(filename, ImageFormat.Bmp);
                pictureBox3.Load(filename);

                //pictureBox3.Load(temp);
                //pictureBox3.Image.Save(filename, ImageFormat.Bmp);
                //pictureBox3.Load(filename);
            }
            catch
            {
                // ignored
            }
        }

        //btn3
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var filename = ImageDestSave(_url, "R");
                var temp = ImageDestSave(_tempfinger + @"\" + _reportid, "R");
                Bitmap bmp = new Bitmap(m_picture.Image);
                bmp.Save(temp, ImageFormat.Bmp);

                pictureBox3.Load(temp);
                //Bitmap bmp1 = new Bitmap(pictureBox3.Image);
                // ConvertImg.ImageConverter(bmp1, filename);//

                MyBitmapFile myFile = new MyBitmapFile(m_hDevice.ImageSize.Width, m_hDevice.ImageSize.Height, m_Frame);
                FileStream file = new FileStream(filename, FileMode.Create);
                file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                file.Close();

                //bmp1.Save(filename, ImageFormat.Bmp);
                pictureBox3.Load(filename);

                //pictureBox3.Load(temp);
                //pictureBox3.Image.Save(filename, ImageFormat.Bmp);
                //pictureBox3.Load(filename);

                MessageBox.Show(@"Press Enter to continue!!!");
                button2.Focus();

                if (oL1.Checked)
                {
                    oL2.Checked = true;
                }
                else if (oL2.Checked)
                {
                    oL3.Checked = true;
                    oL3_CheckedChanged(sender, e);
                }
                else if (oL3.Checked)
                {
                    oL4.Checked = true;
                }
                else if (oL4.Checked)
                {
                    oL5.Checked = true;
                }
                else if (oL5.Checked)
                {
                    oL5.Checked = false;
                    oR1.Checked = true;
                }
                else if (oR1.Checked)
                {
                    oR2.Checked = true;
                }
                else if (oR2.Checked)
                {
                    oR3.Checked = true;
                }
                else if (oR3.Checked)
                {
                    oR4.Checked = true;
                }
                else if (oR4.Checked)
                {
                    oR5.Checked = true;
                }
                else if (oR5.Checked)
                {
                    button1.Focus();
                }
                load_checked();
            }
            catch
            {
                // ignored
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void cL1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void clear_radio(string ButtonName)
        {
            if (ButtonName != "L1")
            {
                oL1.Checked = false;
            }
            if (ButtonName != "L2")
            {
                oL2.Checked = false;
            }
            if (ButtonName != "L3")
            {
                oL3.Checked = false;
            }
            if (ButtonName != "L4")
            {
                oL4.Checked = false;
            }
            if (ButtonName != "L5")
            {
                oL5.Checked = false;
            }
            if (ButtonName != "R1")
            {
                oR1.Checked = false;
            }
            if (ButtonName != "R2")
            {
                oR2.Checked = false;
            }
            if (ButtonName != "R3")
            {
                oR3.Checked = false;
            }
            if (ButtonName != "R4")
            {
                oR4.Checked = false;
            }
            if (ButtonName != "R5")
            {
                oR5.Checked = false;
            }
        }

        private Bitmap CreateBitmap(IntPtr hDC, Size bmpSize, byte[] data)
        {
            IntPtr ptr;
            var output = new MemoryStream();
            var writer = new BinaryWriter(output);
            var lpbmih = new BITMAPINFOHEADER();
            lpbmih.biSize = 40;
            lpbmih.biWidth = bmpSize.Width;
            lpbmih.biHeight = bmpSize.Height;
            lpbmih.biPlanes = 1;
            lpbmih.biBitCount = 8;
            lpbmih.biCompression = 0;

            writer.Write(lpbmih.biSize);
            writer.Write(lpbmih.biWidth);
            writer.Write(lpbmih.biHeight);
            writer.Write(lpbmih.biPlanes);
            writer.Write(lpbmih.biBitCount);
            writer.Write(lpbmih.biCompression);
            writer.Write(lpbmih.biSizeImage);
            writer.Write(lpbmih.biXPelsPerMeter);
            writer.Write(lpbmih.biYPelsPerMeter);
            writer.Write(lpbmih.biClrUsed);
            writer.Write(lpbmih.biClrImportant);

            for (var i = 0; i < 256; i++)
            {
                writer.Write((byte) i);
                writer.Write((byte) i);
                writer.Write((byte) i);
                writer.Write((byte) 0);
            }
            ptr = data != null
                ? CreateDIBitmap(hDC, ref lpbmih, 4, data, output.ToArray(), 0)
                : CreateDIBitmap(hDC, ref lpbmih, 0, null, output.ToArray(), 0);
            return Image.FromHbitmap(ptr);
        }

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateDIBitmap(IntPtr hdc, [In] ref BITMAPINFOHEADER lpbmih, uint fdwInit,
            byte[] lpbInit, byte[] lpbmi, uint fuUsage);

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void GetFrame()
        {
            string str;
            var lastErrorCode = 0;
            var flag = true;
            Label_00BF:
            while (!m_hDevice.IsFingerPresent && !m_bCancelOperation)
            {
                if (lastErrorCode != m_hDevice.LastErrorCode)
                {
                    lastErrorCode = m_hDevice.LastErrorCode;
                    switch (lastErrorCode)
                    {
                        case 0x20000001:
                            MessageDlg.SetMessageDlg("There is no stable fingerprint image on the device.");
                            goto Label_00B7;

                        case 0x20000002:
                            MessageDlg.SetMessageDlg("Fake finger was detected");
                            goto Label_00B7;

                        case 0x20000004:
                        case 0x20000005:
                            MessageDlg.SetMessageDlg("The device does not support the requested feature");
                            goto Label_00B7;

                        case 0x10d2:
                            MessageDlg.SetMessageDlg("Put finger to the scanner");
                            goto Label_00B7;

                        case 0:
                            goto Label_00B7;
                    }
                    str = string.Format("Unknown error #{0}", lastErrorCode);
                    MessageDlg.SetMessageDlg("msg");
                }
                Label_00B7:
                Thread.Sleep(0);
            }
            if (m_bCancelOperation)
            {
                return;
            }
            try
            {
                m_Frame = m_hDevice.GetFrame();
                flag = false;
            }
            catch (ScanAPIException)
            {
                lastErrorCode = m_hDevice.LastErrorCode;
                switch (lastErrorCode)
                {
                    case 0x20000001:
                        MessageDlg.SetMessageDlg("There is no stable fingerprint image on the device.");
                        goto Label_01A7;

                    case 0x20000002:
                        MessageDlg.SetMessageDlg("Fake finger was detected");
                        goto Label_01A7;

                    case 0x20000004:
                    case 0x20000005:
                        MessageDlg.SetMessageDlg("The device does not support the requested feature");
                        goto Label_01A7;

                    case 0x10d2:
                        MessageDlg.SetMessageDlg("Put finger to the scanner");
                        goto Label_01A7;

                    case 0:
                        goto Label_01A7;
                }
                str = string.Format("Unknown error #{0}", lastErrorCode);
                MessageDlg.SetMessageDlg("msg");
            }
            Label_01A7:
            if (flag)
            {
                goto Label_00BF;
            }
        }

        private void GetFrame2()
        {
            string str;
            var lastErrorCode = 0;
            BContinue = true;
            Label_00C7:
            try
            {
                while ((!m_hDevice.IsFingerPresent && !m_bCancelOperation) && !BContinue)
                {
                    if (lastErrorCode != m_hDevice.LastErrorCode)
                    {
                        lastErrorCode = m_hDevice.LastErrorCode;
                        switch (lastErrorCode)
                        {
                            case 0x20000001:
                                MessageDlg.SetMessageDlg("There is no stable fingerprint image on the device.");
                                goto Label_00BF;

                            case 0x20000002:
                                MessageDlg.SetMessageDlg("Fake finger was detected");
                                goto Label_00BF;

                            case 0x20000004:
                            case 0x20000005:
                                MessageDlg.SetMessageDlg("The device does not support the requested feature");
                                goto Label_00BF;

                            case 0x10d2:
                                MessageDlg.SetMessageDlg("Put finger to the scanner");
                                goto Label_00BF;

                            case 0:
                                goto Label_00BF;
                        }
                        str = string.Format("Unknown error #{0}", lastErrorCode);
                        MessageDlg.SetMessageDlg("msg");
                    }
                    Label_00BF:
                    Thread.Sleep(0);
                }
            }
            catch
            {
            }
            if ((m_bCancelOperation || !BContinue) || (m_hDevice == null))
            {
                return;
            }
            try
            {
                if (m_hDevice != null)
                {
                    try
                    {
                        m_Frame = m_hDevice.GetFrame();
                    }
                    catch
                    {
                    }
                }
                BContinue = true;
                if ((m_Frame != null) && (m_Frame.Length != 0))
                {
                    try
                    {
                        var imageSize = m_hDevice.ImageSize;
                        var bitmap = CreateBitmap(m_picture.CreateGraphics().GetHdc(), imageSize, m_Frame);
                        bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                        m_picture.Image = bitmap;
                    }
                    catch
                    {
                        BContinue = false;
                    }
                }
            }
            catch (ScanAPIException)
            {
                if (m_hDevice != null)
                {
                    lastErrorCode = m_hDevice.LastErrorCode;
                    switch (lastErrorCode)
                    {
                        case 0x20000001:
                            MessageDlg.SetMessageDlg("There is no stable fingerprint image on the device.");
                            goto Label_0281;

                        case 0x20000002:
                            MessageDlg.SetMessageDlg("Fake finger was detected");
                            goto Label_0281;

                        case 0x20000004:
                        case 0x20000005:
                            MessageDlg.SetMessageDlg("The device does not support the requested feature");
                            goto Label_0281;

                        case 0x10d2:
                            MessageDlg.SetMessageDlg("Put finger to the scanner");
                            goto Label_0281;

                        case 0:
                            goto Label_0281;
                    }
                    str = string.Format("Unknown error #{0}", lastErrorCode);
                    MessageDlg.SetMessageDlg("msg");
                }
            }
            Label_0281:
            if (BContinue)
            {
                goto Label_00C7;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {
        }

        private string ImageDestSave(string aName, string bName)
        {
            var str2 = "";
            if (oL1.Checked)
            {
                str2 = "L1";
            }
            if (oL2.Checked)
            {
                str2 = "L2";
            }
            if (oL3.Checked)
            {
                str2 = "L3";
            }
            if (oL4.Checked)
            {
                str2 = "L4";
            }
            if (oL5.Checked)
            {
                str2 = "L5";
            }
            if (oR1.Checked)
            {
                str2 = "R1";
            }
            if (oR2.Checked)
            {
                str2 = "R2";
            }
            if (oR3.Checked)
            {
                str2 = "R3";
            }
            if (oR4.Checked)
            {
                str2 = "R4";
            }
            if (oR5.Checked)
            {
                str2 = "R5";
            }
            return (aName + str2 + bName + ".bmp");
        }

        private void InitializeComponent()
        {
            this.m_grpParameters = new System.Windows.Forms.GroupBox();
            this.m_lblEEPROMSize = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.m_lblCurrentImageSize = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_lblImageSize = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.m_lblCompatibility = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_chkReceiveLongImage = new System.Windows.Forms.CheckBox();
            this.m_chkFFD = new System.Windows.Forms.CheckBox();
            this.m_chkDetectFakeFinger = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_lblFirmwareVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_lblHardwareVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_lblApiVersion = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_cmbInterfaces = new System.Windows.Forms.ComboBox();
            this.m_btnOpenDevice = new System.Windows.Forms.Button();
            this.m_btnClose = new System.Windows.Forms.Button();
            this.m_btnGetFrame = new System.Windows.Forms.Button();
            this.m_grpTests = new System.Windows.Forms.GroupBox();
            this.m_picture = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cL2 = new System.Windows.Forms.CheckBox();
            this.cL3 = new System.Windows.Forms.CheckBox();
            this.cL4 = new System.Windows.Forms.CheckBox();
            this.cL5 = new System.Windows.Forms.CheckBox();
            this.cL1 = new System.Windows.Forms.CheckBox();
            this.oL5 = new System.Windows.Forms.RadioButton();
            this.oL4 = new System.Windows.Forms.RadioButton();
            this.oL3 = new System.Windows.Forms.RadioButton();
            this.oL2 = new System.Windows.Forms.RadioButton();
            this.oL1 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cR2 = new System.Windows.Forms.CheckBox();
            this.cR3 = new System.Windows.Forms.CheckBox();
            this.cR4 = new System.Windows.Forms.CheckBox();
            this.cR5 = new System.Windows.Forms.CheckBox();
            this.cR1 = new System.Windows.Forms.CheckBox();
            this.oR5 = new System.Windows.Forms.RadioButton();
            this.oR4 = new System.Windows.Forms.RadioButton();
            this.oR3 = new System.Windows.Forms.RadioButton();
            this.oR2 = new System.Windows.Forms.RadioButton();
            this.oR1 = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.m_grpParameters.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.m_grpTests.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.m_picture)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox3)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_grpParameters
            // 
            this.m_grpParameters.Controls.Add(this.m_lblEEPROMSize);
            this.m_grpParameters.Controls.Add(this.label7);
            this.m_grpParameters.Controls.Add(this.m_lblCurrentImageSize);
            this.m_grpParameters.Controls.Add(this.label6);
            this.m_grpParameters.Controls.Add(this.groupBox1);
            this.m_grpParameters.Controls.Add(this.m_chkReceiveLongImage);
            this.m_grpParameters.Controls.Add(this.m_chkFFD);
            this.m_grpParameters.Controls.Add(this.m_chkDetectFakeFinger);
            this.m_grpParameters.Controls.Add(this.groupBox2);
            this.m_grpParameters.Location = new System.Drawing.Point(853, 114);
            this.m_grpParameters.Name = "m_grpParameters";
            this.m_grpParameters.Size = new System.Drawing.Size(118, 280);
            this.m_grpParameters.TabIndex = 0;
            this.m_grpParameters.TabStop = false;
            this.m_grpParameters.Text = " Parameters ";
            this.m_grpParameters.Visible = false;
            // 
            // m_lblEEPROMSize
            // 
            this.m_lblEEPROMSize.Location = new System.Drawing.Point(108, 251);
            this.m_lblEEPROMSize.Name = "m_lblEEPROMSize";
            this.m_lblEEPROMSize.Size = new System.Drawing.Size(250, 13);
            this.m_lblEEPROMSize.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "EEPROM size:";
            // 
            // m_lblCurrentImageSize
            // 
            this.m_lblCurrentImageSize.Location = new System.Drawing.Point(108, 227);
            this.m_lblCurrentImageSize.Name = "m_lblCurrentImageSize";
            this.m_lblCurrentImageSize.Size = new System.Drawing.Size(314, 13);
            this.m_lblCurrentImageSize.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 227);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Current image size: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_lblImageSize);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.m_lblCompatibility);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(6, 161);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 63);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Device information";
            // 
            // m_lblImageSize
            // 
            this.m_lblImageSize.Location = new System.Drawing.Point(119, 38);
            this.m_lblImageSize.Name = "m_lblImageSize";
            this.m_lblImageSize.Size = new System.Drawing.Size(233, 13);
            this.m_lblImageSize.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Image size: ";
            // 
            // m_lblCompatibility
            // 
            this.m_lblCompatibility.Location = new System.Drawing.Point(119, 16);
            this.m_lblCompatibility.Name = "m_lblCompatibility";
            this.m_lblCompatibility.Size = new System.Drawing.Size(233, 13);
            this.m_lblCompatibility.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Device compatibility: ";
            // 
            // m_chkReceiveLongImage
            // 
            this.m_chkReceiveLongImage.AutoSize = true;
            this.m_chkReceiveLongImage.Location = new System.Drawing.Point(139, 112);
            this.m_chkReceiveLongImage.Name = "m_chkReceiveLongImage";
            this.m_chkReceiveLongImage.Size = new System.Drawing.Size(125, 17);
            this.m_chkReceiveLongImage.TabIndex = 3;
            this.m_chkReceiveLongImage.Text = "Receive long image";
            this.m_chkReceiveLongImage.UseVisualStyleBackColor = true;
            // 
            // m_chkFFD
            // 
            this.m_chkFFD.AutoSize = true;
            this.m_chkFFD.Location = new System.Drawing.Point(6, 135);
            this.m_chkFFD.Name = "m_chkFFD";
            this.m_chkFFD.Size = new System.Drawing.Size(176, 17);
            this.m_chkFFD.TabIndex = 2;
            this.m_chkFFD.Text = "Fast finger detection method";
            this.m_chkFFD.UseVisualStyleBackColor = true;
            // 
            // m_chkDetectFakeFinger
            // 
            this.m_chkDetectFakeFinger.AutoSize = true;
            this.m_chkDetectFakeFinger.Location = new System.Drawing.Point(6, 112);
            this.m_chkDetectFakeFinger.Name = "m_chkDetectFakeFinger";
            this.m_chkDetectFakeFinger.Size = new System.Drawing.Size(134, 17);
            this.m_chkDetectFakeFinger.TabIndex = 1;
            this.m_chkDetectFakeFinger.Text = "Live Finger Detection";
            this.m_chkDetectFakeFinger.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_lblFirmwareVersion);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.m_lblHardwareVersion);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.m_lblApiVersion);
            this.groupBox2.Controls.Add(this.label);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(333, 87);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Version information ";
            // 
            // m_lblFirmwareVersion
            // 
            this.m_lblFirmwareVersion.Location = new System.Drawing.Point(110, 63);
            this.m_lblFirmwareVersion.Name = "m_lblFirmwareVersion";
            this.m_lblFirmwareVersion.Size = new System.Drawing.Size(242, 13);
            this.m_lblFirmwareVersion.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Firmware version: ";
            // 
            // m_lblHardwareVersion
            // 
            this.m_lblHardwareVersion.Location = new System.Drawing.Point(110, 39);
            this.m_lblHardwareVersion.Name = "m_lblHardwareVersion";
            this.m_lblHardwareVersion.Size = new System.Drawing.Size(242, 13);
            this.m_lblHardwareVersion.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hardware version: ";
            // 
            // m_lblApiVersion
            // 
            this.m_lblApiVersion.Location = new System.Drawing.Point(110, 16);
            this.m_lblApiVersion.Name = "m_lblApiVersion";
            this.m_lblApiVersion.Size = new System.Drawing.Size(242, 13);
            this.m_lblApiVersion.TabIndex = 1;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(6, 16);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(69, 13);
            this.label.TabIndex = 0;
            this.label.Text = "API version: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(167, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Default interface: ";
            // 
            // m_cmbInterfaces
            // 
            this.m_cmbInterfaces.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cmbInterfaces.FormattingEnabled = true;
            this.m_cmbInterfaces.Location = new System.Drawing.Point(264, 19);
            this.m_cmbInterfaces.Name = "m_cmbInterfaces";
            this.m_cmbInterfaces.Size = new System.Drawing.Size(59, 21);
            this.m_cmbInterfaces.TabIndex = 1;
            // 
            // m_btnOpenDevice
            // 
            this.m_btnOpenDevice.Location = new System.Drawing.Point(6, 28);
            this.m_btnOpenDevice.Name = "m_btnOpenDevice";
            this.m_btnOpenDevice.Size = new System.Drawing.Size(75, 23);
            this.m_btnOpenDevice.TabIndex = 2;
            this.m_btnOpenDevice.Text = "Open";
            this.m_btnOpenDevice.UseVisualStyleBackColor = true;
            // 
            // m_btnClose
            // 
            this.m_btnClose.Location = new System.Drawing.Point(87, 22);
            this.m_btnClose.Name = "m_btnClose";
            this.m_btnClose.Size = new System.Drawing.Size(75, 23);
            this.m_btnClose.TabIndex = 3;
            this.m_btnClose.Text = "Close";
            this.m_btnClose.UseVisualStyleBackColor = true;
            // 
            // m_btnGetFrame
            // 
            this.m_btnGetFrame.Location = new System.Drawing.Point(15, 18);
            this.m_btnGetFrame.Name = "m_btnGetFrame";
            this.m_btnGetFrame.Size = new System.Drawing.Size(75, 23);
            this.m_btnGetFrame.TabIndex = 4;
            this.m_btnGetFrame.Text = "Get Frame";
            this.m_btnGetFrame.UseVisualStyleBackColor = true;
            // 
            // m_grpTests
            // 
            this.m_grpTests.Controls.Add(this.m_btnGetFrame);
            this.m_grpTests.Location = new System.Drawing.Point(853, 400);
            this.m_grpTests.Name = "m_grpTests";
            this.m_grpTests.Size = new System.Drawing.Size(118, 57);
            this.m_grpTests.TabIndex = 5;
            this.m_grpTests.TabStop = false;
            this.m_grpTests.Text = "Tests";
            this.m_grpTests.Visible = false;
            // 
            // m_picture
            // 
            this.m_picture.BackColor = System.Drawing.Color.Black;
            this.m_picture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_picture.Location = new System.Drawing.Point(105, 24);
            this.m_picture.Name = "m_picture";
            this.m_picture.Size = new System.Drawing.Size(379, 405);
            this.m_picture.TabIndex = 6;
            this.m_picture.TabStop = false;
            this.m_picture.Click += new System.EventHandler(this.m_picture_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cL2);
            this.groupBox3.Controls.Add(this.cL3);
            this.groupBox3.Controls.Add(this.cL4);
            this.groupBox3.Controls.Add(this.cL5);
            this.groupBox3.Controls.Add(this.cL1);
            this.groupBox3.Controls.Add(this.oL5);
            this.groupBox3.Controls.Add(this.oL4);
            this.groupBox3.Controls.Add(this.oL3);
            this.groupBox3.Controls.Add(this.oL2);
            this.groupBox3.Controls.Add(this.oL1);
            this.groupBox3.Location = new System.Drawing.Point(12, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(87, 198);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Left";
            // 
            // cL2
            // 
            this.cL2.AutoSize = true;
            this.cL2.Enabled = false;
            this.cL2.Location = new System.Drawing.Point(60, 64);
            this.cL2.Name = "cL2";
            this.cL2.Size = new System.Drawing.Size(15, 14);
            this.cL2.TabIndex = 5;
            this.cL2.UseVisualStyleBackColor = true;
            // 
            // cL3
            // 
            this.cL3.AutoSize = true;
            this.cL3.Enabled = false;
            this.cL3.Location = new System.Drawing.Point(60, 95);
            this.cL3.Name = "cL3";
            this.cL3.Size = new System.Drawing.Size(15, 14);
            this.cL3.TabIndex = 7;
            this.cL3.UseVisualStyleBackColor = true;
            // 
            // cL4
            // 
            this.cL4.AutoSize = true;
            this.cL4.Enabled = false;
            this.cL4.Location = new System.Drawing.Point(60, 128);
            this.cL4.Name = "cL4";
            this.cL4.Size = new System.Drawing.Size(15, 14);
            this.cL4.TabIndex = 9;
            this.cL4.UseVisualStyleBackColor = true;
            // 
            // cL5
            // 
            this.cL5.AutoSize = true;
            this.cL5.Enabled = false;
            this.cL5.Location = new System.Drawing.Point(60, 161);
            this.cL5.Name = "cL5";
            this.cL5.Size = new System.Drawing.Size(15, 14);
            this.cL5.TabIndex = 11;
            this.cL5.UseVisualStyleBackColor = true;
            // 
            // cL1
            // 
            this.cL1.AutoSize = true;
            this.cL1.Enabled = false;
            this.cL1.Location = new System.Drawing.Point(60, 30);
            this.cL1.Name = "cL1";
            this.cL1.Size = new System.Drawing.Size(15, 14);
            this.cL1.TabIndex = 3;
            this.cL1.UseVisualStyleBackColor = true;
            this.cL1.CheckedChanged += new System.EventHandler(this.cL1_CheckedChanged);
            // 
            // oL5
            // 
            this.oL5.AutoSize = true;
            this.oL5.Location = new System.Drawing.Point(17, 157);
            this.oL5.Name = "oL5";
            this.oL5.Size = new System.Drawing.Size(42, 17);
            this.oL5.TabIndex = 10;
            this.oL5.TabStop = true;
            this.oL5.Text = "FL5";
            this.oL5.UseVisualStyleBackColor = true;
            this.oL5.CheckedChanged += new System.EventHandler(this.oL5_CheckedChanged);
            this.oL5.Click += new System.EventHandler(this.oL5_Click);
            // 
            // oL4
            // 
            this.oL4.AutoSize = true;
            this.oL4.Location = new System.Drawing.Point(17, 125);
            this.oL4.Name = "oL4";
            this.oL4.Size = new System.Drawing.Size(42, 17);
            this.oL4.TabIndex = 8;
            this.oL4.TabStop = true;
            this.oL4.Text = "FL4";
            this.oL4.UseVisualStyleBackColor = true;
            this.oL4.CheckedChanged += new System.EventHandler(this.oL4_CheckedChanged);
            this.oL4.Click += new System.EventHandler(this.oL4_Click);
            // 
            // oL3
            // 
            this.oL3.AutoSize = true;
            this.oL3.Location = new System.Drawing.Point(17, 93);
            this.oL3.Name = "oL3";
            this.oL3.Size = new System.Drawing.Size(42, 17);
            this.oL3.TabIndex = 6;
            this.oL3.TabStop = true;
            this.oL3.Text = "FL3";
            this.oL3.UseVisualStyleBackColor = true;
            this.oL3.CheckedChanged += new System.EventHandler(this.oR3_CheckedChanged);
            this.oL3.Click += new System.EventHandler(this.oL3_Click);
            // 
            // oL2
            // 
            this.oL2.AutoSize = true;
            this.oL2.Location = new System.Drawing.Point(17, 62);
            this.oL2.Name = "oL2";
            this.oL2.Size = new System.Drawing.Size(42, 17);
            this.oL2.TabIndex = 4;
            this.oL2.TabStop = true;
            this.oL2.Text = "FL2";
            this.oL2.UseVisualStyleBackColor = true;
            this.oL2.CheckedChanged += new System.EventHandler(this.oL2_CheckedChanged);
            this.oL2.Click += new System.EventHandler(this.oL2_Click);
            // 
            // oL1
            // 
            this.oL1.AutoSize = true;
            this.oL1.Checked = true;
            this.oL1.Location = new System.Drawing.Point(17, 28);
            this.oL1.Name = "oL1";
            this.oL1.Size = new System.Drawing.Size(42, 17);
            this.oL1.TabIndex = 2;
            this.oL1.TabStop = true;
            this.oL1.Text = "FL1";
            this.oL1.UseVisualStyleBackColor = true;
            this.oL1.Click += new System.EventHandler(this.oL1_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.button1.Location = new System.Drawing.Point(624, 84);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(194, 59);
            this.button1.TabIndex = 9;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(488, 275);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 152);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(609, 275);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(115, 152);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(730, 275);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(115, 152);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 17;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.button3.Location = new System.Drawing.Point(609, 226);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(115, 40);
            this.button3.TabIndex = 18;
            this.button3.Text = "Left";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.button4.Location = new System.Drawing.Point(730, 226);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(115, 40);
            this.button4.TabIndex = 19;
            this.button4.Text = "Right";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cR2);
            this.groupBox4.Controls.Add(this.cR3);
            this.groupBox4.Controls.Add(this.cR4);
            this.groupBox4.Controls.Add(this.cR5);
            this.groupBox4.Controls.Add(this.cR1);
            this.groupBox4.Controls.Add(this.oR5);
            this.groupBox4.Controls.Add(this.oR4);
            this.groupBox4.Controls.Add(this.oR3);
            this.groupBox4.Controls.Add(this.oR2);
            this.groupBox4.Controls.Add(this.oR1);
            this.groupBox4.Location = new System.Drawing.Point(490, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(87, 198);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Right";
            // 
            // cR2
            // 
            this.cR2.AutoSize = true;
            this.cR2.Enabled = false;
            this.cR2.Location = new System.Drawing.Point(60, 64);
            this.cR2.Name = "cR2";
            this.cR2.Size = new System.Drawing.Size(15, 14);
            this.cR2.TabIndex = 16;
            this.cR2.UseVisualStyleBackColor = true;
            // 
            // cR3
            // 
            this.cR3.AutoSize = true;
            this.cR3.Enabled = false;
            this.cR3.Location = new System.Drawing.Point(60, 95);
            this.cR3.Name = "cR3";
            this.cR3.Size = new System.Drawing.Size(15, 14);
            this.cR3.TabIndex = 18;
            this.cR3.UseVisualStyleBackColor = true;
            // 
            // cR4
            // 
            this.cR4.AutoSize = true;
            this.cR4.Enabled = false;
            this.cR4.Location = new System.Drawing.Point(60, 128);
            this.cR4.Name = "cR4";
            this.cR4.Size = new System.Drawing.Size(15, 14);
            this.cR4.TabIndex = 20;
            this.cR4.UseVisualStyleBackColor = true;
            // 
            // cR5
            // 
            this.cR5.AutoSize = true;
            this.cR5.Enabled = false;
            this.cR5.Location = new System.Drawing.Point(60, 161);
            this.cR5.Name = "cR5";
            this.cR5.Size = new System.Drawing.Size(15, 14);
            this.cR5.TabIndex = 22;
            this.cR5.UseVisualStyleBackColor = true;
            // 
            // cR1
            // 
            this.cR1.AutoSize = true;
            this.cR1.Enabled = false;
            this.cR1.Location = new System.Drawing.Point(60, 30);
            this.cR1.Name = "cR1";
            this.cR1.Size = new System.Drawing.Size(15, 14);
            this.cR1.TabIndex = 14;
            this.cR1.UseVisualStyleBackColor = true;
            // 
            // oR5
            // 
            this.oR5.AutoSize = true;
            this.oR5.Location = new System.Drawing.Point(17, 157);
            this.oR5.Name = "oR5";
            this.oR5.Size = new System.Drawing.Size(44, 17);
            this.oR5.TabIndex = 21;
            this.oR5.TabStop = true;
            this.oR5.Text = "FR5";
            this.oR5.UseVisualStyleBackColor = true;
            this.oR5.CheckedChanged += new System.EventHandler(this.oR5_CheckedChanged);
            this.oR5.Click += new System.EventHandler(this.oR5_Click);
            // 
            // oR4
            // 
            this.oR4.AutoSize = true;
            this.oR4.Location = new System.Drawing.Point(17, 125);
            this.oR4.Name = "oR4";
            this.oR4.Size = new System.Drawing.Size(44, 17);
            this.oR4.TabIndex = 19;
            this.oR4.TabStop = true;
            this.oR4.Text = "FR4";
            this.oR4.UseVisualStyleBackColor = true;
            this.oR4.CheckedChanged += new System.EventHandler(this.oR4_CheckedChanged);
            this.oR4.Click += new System.EventHandler(this.oR4_Click);
            // 
            // oR3
            // 
            this.oR3.AutoSize = true;
            this.oR3.Location = new System.Drawing.Point(17, 93);
            this.oR3.Name = "oR3";
            this.oR3.Size = new System.Drawing.Size(44, 17);
            this.oR3.TabIndex = 17;
            this.oR3.TabStop = true;
            this.oR3.Text = "FR3";
            this.oR3.UseVisualStyleBackColor = true;
            this.oR3.CheckedChanged += new System.EventHandler(this.oR3_CheckedChanged);
            this.oR3.Click += new System.EventHandler(this.oR3_Click);
            // 
            // oR2
            // 
            this.oR2.AutoSize = true;
            this.oR2.Location = new System.Drawing.Point(17, 62);
            this.oR2.Name = "oR2";
            this.oR2.Size = new System.Drawing.Size(44, 17);
            this.oR2.TabIndex = 15;
            this.oR2.TabStop = true;
            this.oR2.Text = "FR2";
            this.oR2.UseVisualStyleBackColor = true;
            this.oR2.CheckedChanged += new System.EventHandler(this.oR2_CheckedChanged);
            this.oR2.Click += new System.EventHandler(this.oR2_Click);
            // 
            // oR1
            // 
            this.oR1.AutoSize = true;
            this.oR1.Location = new System.Drawing.Point(17, 28);
            this.oR1.Name = "oR1";
            this.oR1.Size = new System.Drawing.Size(44, 17);
            this.oR1.TabIndex = 13;
            this.oR1.TabStop = true;
            this.oR1.Text = "FR1";
            this.oR1.UseVisualStyleBackColor = true;
            this.oR1.CheckedChanged += new System.EventHandler(this.oR1_CheckedChanged);
            this.oR1.Click += new System.EventHandler(this.oR1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.button2.Location = new System.Drawing.Point(488, 226);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 40);
            this.button2.TabIndex = 0;
            this.button2.Text = "Middle";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.m_btnOpenDevice);
            this.groupBox5.Controls.Add(this.m_btnClose);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.m_cmbInterfaces);
            this.groupBox5.Location = new System.Drawing.Point(853, 38);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(103, 66);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "groupBox5";
            this.groupBox5.Visible = false;
            // 
            // FrmScan
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(853, 441);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.m_picture);
            this.Controls.Add(this.m_grpTests);
            this.Controls.Add(this.m_grpParameters);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int) (((byte) (51)))), ((int) (((byte) (51)))),
                ((int) (((byte) (51)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmScan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scan Fingerprint";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnFormClosed);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.m_grpParameters.ResumeLayout(false);
            this.m_grpParameters.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.m_grpTests.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.m_picture)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox3)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        public void live_scan()
        {
            threadDelegate = OnTestGetFrame;
            newThread = new Thread(threadDelegate);
            newThread.Start();
        }

        private void load_checked()
        {
            if ((File.Exists(_url + "L1F.BMP") && File.Exists(_url + "L1L.BMP")) && File.Exists(_url + "L1R.BMP"))
            {
                cL1.Checked = true;
            }
            if ((File.Exists(_url + "L2F.BMP") && File.Exists(_url + "L2L.BMP")) && File.Exists(_url + "L2R.BMP"))
            {
                cL2.Checked = true;
            }
            if ((File.Exists(_url + "L3F.BMP") && File.Exists(_url + "L3L.BMP")) && File.Exists(_url + "L3R.BMP"))
            {
                cL3.Checked = true;
            }
            if ((File.Exists(_url + "L4F.BMP") && File.Exists(_url + "L4L.BMP")) && File.Exists(_url + "L4R.BMP"))
            {
                cL4.Checked = true;
            }
            if ((File.Exists(_url + "L5F.BMP") && File.Exists(_url + "L5L.BMP")) && File.Exists(_url + "L5R.BMP"))
            {
                cL5.Checked = true;
            }
            if ((File.Exists(_url + "R1F.BMP") && File.Exists(_url + "R1L.BMP")) && File.Exists(_url + "R1R.BMP"))
            {
                cR1.Checked = true;
            }
            if ((File.Exists(_url + "R2F.BMP") && File.Exists(_url + "R2L.BMP")) && File.Exists(_url + "R2R.BMP"))
            {
                cR2.Checked = true;
            }
            if ((File.Exists(_url + "R3F.BMP") && File.Exists(_url + "R3L.BMP")) && File.Exists(_url + "R3R.BMP"))
            {
                cR3.Checked = true;
            }
            if ((File.Exists(_url + "R4F.BMP") && File.Exists(_url + "R4L.BMP")) && File.Exists(_url + "R4R.BMP"))
            {
                cR4.Checked = true;
            }
            if ((File.Exists(_url + "R5F.BMP") && File.Exists(_url + "R5L.BMP")) && File.Exists(_url + "R5R.BMP"))
            {
                cR5.Checked = true;
            }
        }

        private void load_L1_Image()
        {
            Count_Space = 0;
            var f = _url + "L1F" + _namecustomer + ".BMP";
            var l = _url + "L1L" + _namecustomer + ".BMP";
            var r = _url + "L1R" + _namecustomer + ".BMP";
            load_picture(f, l, r);
        }

        private void load_picture(string f, string l, string r)
        {
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            if (f != null)
            {
                try
                {
                    pictureBox1.Load(f);
                }
                catch
                {
                    pictureBox1.Image = null;
                }
            }
            else
            {
                pictureBox1.Image = null;
            }
            if (l != null)
            {
                try
                {
                    pictureBox2.Load(l);
                }
                catch
                {
                    pictureBox2.Image = null;
                }
            }
            else
            {
                pictureBox2.Image = null;
            }
            if (r != null)
            {
                try
                {
                    pictureBox3.Load(r);
                }
                catch
                {
                    pictureBox3.Image = null;
                }
            }
            else
            {
                pictureBox3.Image = null;
            }


            if (pictureBox1.Image == null) button2.Focus();
            else if (pictureBox2.Image == null) button3.Focus();
            else if (pictureBox3.Image == null) button4.Focus();
            else load_checked();
        }

        private void m_cmbInterfaces_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_cmbInterfaces.SelectedIndex != -1)
            {
                var item = (ComboBoxItem) m_cmbInterfaces.Items[m_cmbInterfaces.SelectedIndex];
                try
                {
                    Device.BaseInterface = item.interfaceNumber;
                    m_btnOpenDevice.Enabled = true;
                }
                catch (ScanAPIException exception)
                {
                    ShowError(exception);
                }
            }
            else
            {
                m_btnOpenDevice.Enabled = false;
            }
        }

        private void m_grpParameters_Enter(object sender, EventArgs e)
        {
        }

        private void m_grpTests_Enter(object sender, EventArgs e)
        {
        }

        private void m_lblApiVersion_Click(object sender, EventArgs e)
        {
        }

        private void m_picture_Click(object sender, EventArgs e)
        {
        }

        private void oL1_Click(object sender, EventArgs e)
        {
            Count_Space = 0;

            if (oL1.Checked)
            {
                Count_Space = 0;
                var f = _url + "L1F" + _namecustomer + ".BMP";
                var l = _url + "L1L" + _namecustomer + ".BMP";
                var r = _url + "L1R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }

            button2.Focus();
        }

        private void oL1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cL1.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oL2.Focus();
                    oL2.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oL1_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("L1");
            button2.Focus();
        }

        private void oL2_CheckedChanged(object sender, EventArgs e)
        {
            if (oL2.Checked)
            {
                Count_Space = 0;
                var f = _url + "L2F" + _namecustomer + ".BMP";
                var l = _url + "L2L" + _namecustomer + ".BMP";
                var r = _url + "L2R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                clear_radio("L2");
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cL2.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oL3.Focus();
                    oL3.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oL2_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("L2");
            button2.Focus();
        }

        private void oL3_CheckedChanged(object sender, EventArgs e)
        {
            if (oL3.Checked)
            {
                Count_Space = 0;
                var f = _url + "L3F" + _namecustomer + ".BMP";
                var l = _url + "L3L" + _namecustomer + ".BMP";
                var r = _url + "L3R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cL3.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oL4.Focus();
                    oL4.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oL3_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("L3");
            button2.Focus();
        }

        private void oL4_CheckedChanged(object sender, EventArgs e)
        {
            if (oL4.Checked)
            {
                Count_Space = 0;
                var f = _url + "L4F" + _namecustomer + ".BMP";
                var l = _url + "L4L" + _namecustomer + ".BMP";
                var r = _url + "L4R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cL4.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oL5.Focus();
                    oL5.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oL4_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("L4");
            button2.Focus();
        }

        private void oL5_CheckedChanged(object sender, EventArgs e)
        {
            if (oL5.Checked)
            {
                Count_Space = 0;
                var f = _url + "L5F" + _namecustomer + ".BMP";
                var l = _url + "L5L" + _namecustomer + ".BMP";
                var r = _url + "L5R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cL5.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oL5.Checked = false;
                    oR1.Focus();
                    oR1.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oL5_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("L5");
            button2.Focus();
        }

        private void OnCloseDevice()
        {
            try
            {
                var imageSize = m_hDevice.ImageSize;
                m_hDevice.Dispose();
                m_hDevice = null;
                m_lblApiVersion.Text = string.Empty;
                m_lblHardwareVersion.Text = string.Empty;
                m_lblFirmwareVersion.Text = string.Empty;
                m_chkDetectFakeFinger.Checked = false;
                m_chkFFD.Checked = false;
                m_chkReceiveLongImage.Checked = false;
                m_lblCompatibility.Text = string.Empty;
                m_lblImageSize.Text = string.Empty;
                m_lblCurrentImageSize.Text = string.Empty;
                m_lblEEPROMSize.Text = string.Empty;
                var bitmap = CreateBitmap(m_picture.CreateGraphics().GetHdc(), imageSize, null);
                bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                m_picture.Image = bitmap;
                m_grpParameters.Enabled = false;
                m_grpTests.Enabled = false;
                m_cmbInterfaces.Enabled = true;
                m_btnOpenDevice.Enabled = true;
                m_btnClose.Enabled = false;
            }
            catch
            {
            }
        }

        private void OnCloseDevice(object sender, EventArgs e)
        {
            try
            {
                var mHDevice = m_hDevice;
                if (mHDevice != null)
                {
                    var imageSize = mHDevice.ImageSize;
                    mHDevice.Dispose();
                    m_hDevice = null;
                    m_lblApiVersion.Text = string.Empty;
                    m_lblHardwareVersion.Text = string.Empty;
                    m_lblFirmwareVersion.Text = string.Empty;
                    m_chkDetectFakeFinger.Checked = false;
                    m_chkFFD.Checked = false;
                    m_chkReceiveLongImage.Checked = false;
                    m_lblCompatibility.Text = string.Empty;
                    m_lblImageSize.Text = string.Empty;
                    m_lblCurrentImageSize.Text = string.Empty;
                    m_lblEEPROMSize.Text = string.Empty;
                    var bitmap = CreateBitmap(m_picture.CreateGraphics().GetHdc(), imageSize, null);
                    bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                    m_picture.Image = bitmap;
                }
                m_grpParameters.Enabled = false;
                m_grpTests.Enabled = false;
                m_cmbInterfaces.Enabled = true;
                m_btnOpenDevice.Enabled = true;
                m_btnClose.Enabled = false;
            }
            catch
            {
                m_hDevice.Close();
                m_hDevice = null;
                m_hDevice.Dispose();
            }
        }

        private void OnDetectFakeFinger(object sender, EventArgs e)
        {
            if (m_hDevice != null)
            {
                m_hDevice.DetectFakeFinger = m_chkDetectFakeFinger.Checked;
            }
        }

        private void OnFFD(object sender, EventArgs e)
        {
            if (m_hDevice != null)
            {
                m_hDevice.FastFingerDetectMethod = m_chkFFD.Checked;
            }
        }

        private void OnFormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_hDevice != null)
            {
                m_hDevice.Dispose();
                m_hDevice = null;
            }
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            //url = Program.Fname;
            //if (url == null)
            //{
            //    //url = @"c:\temp\123456\";
            //    url =_
            //}
            button2.Focus();
            try
            {
                var baseInterface = Device.BaseInterface;
                var interfaces = Device.GetInterfaces();
                for (var i = 0; i < interfaces.Length; i++)
                {
                    if (interfaces[i] == FTRSCAN_INTERFACE_STATUS.FTRSCAN_INTERFACE_STATUS_CONNECTED)
                    {
                        var num3 = m_cmbInterfaces.Items.Add(new ComboBoxItem(i.ToString(), i));
                        if (baseInterface == i)
                        {
                            m_cmbInterfaces.SelectedIndex = num3;
                            OpenDevice();
                            live_scan();
                            load_L1_Image();
                            load_checked();
                        }
                    }
                }
            }
            catch (ScanAPIException exception)
            {
                ShowError(exception);
            }
            button2.Focus();
        }

        private void OnOpenDevice(object sender, EventArgs e)
        {
            try
            {
                m_hDevice = new Device();
                m_hDevice.Open();
                var versionInformation = m_hDevice.VersionInformation;
                m_lblApiVersion.Text = versionInformation.APIVersion.ToString();
                m_lblHardwareVersion.Text = versionInformation.HardwareVersion.ToString();
                m_lblFirmwareVersion.Text = versionInformation.FirmwareVersion.ToString();
                m_chkDetectFakeFinger.Checked = m_hDevice.DetectFakeFinger;
                m_chkFFD.Checked = m_hDevice.FastFingerDetectMethod;
                m_chkReceiveLongImage.Checked = m_hDevice.ReceiveLongImage;
                var information = m_hDevice.Information;
                if (information.DeviceCompatibility == 0)
                {
                    m_lblCompatibility.Text = "USB 1.1 device";
                }
                else if (information.DeviceCompatibility == 1)
                {
                    m_lblCompatibility.Text = "USB 2.0 device";
                }
                else
                {
                    m_lblCompatibility.Text = "Unknown";
                }
                m_lblImageSize.Text = information.imageSize.ToString();
                m_lblCurrentImageSize.Text = m_hDevice.ImageSize.ToString();
                m_lblEEPROMSize.Text = m_hDevice.MemorySize.ToString(CultureInfo.InvariantCulture.NumberFormat);
                m_grpParameters.Enabled = true;
                m_grpTests.Enabled = true;
                m_cmbInterfaces.Enabled = false;
                m_btnOpenDevice.Enabled = false;
                m_btnClose.Enabled = true;
            }
            catch (ScanAPIException exception)
            {
                if (m_hDevice != null)
                {
                    m_hDevice.Dispose();
                    m_hDevice = null;
                }
                ShowError(exception);
            }
        }

        private void OnReceiveLongImage(object sender, EventArgs e)
        {
            if (m_hDevice != null)
            {
                try
                {
                    m_hDevice.ReceiveLongImage = m_chkReceiveLongImage.Checked;
                }
                catch (ScanAPIException exception)
                {
                    ShowError(exception);
                    m_chkReceiveLongImage.Checked = !m_chkReceiveLongImage.Checked;
                }
            }
        }

        private void OnTestGetFrame()
        {
            GetFrame2();
        }

        private void OnTestGetFrame(object sender, EventArgs e)
        {
            try
            {
                m_bCancelOperation = false;
                MessageDlg.ShowMessageDlg(this, "Put finger to the scanner", Text, OnUserBreak);
                GetFrame();
                MessageDlg.HideMessageDlg();
                if ((m_Frame != null) && (m_Frame.Length != 0))
                {
                    var imageSize = m_hDevice.ImageSize;
                    var bitmap = CreateBitmap(m_picture.CreateGraphics().GetHdc(), imageSize, m_Frame);
                    bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                    m_picture.Image = bitmap;
                }
            }
            catch
            {
            }
        }

        private void OnUserBreak()
        {
            m_bCancelOperation = true;
        }

        private void OpenDevice()
        {
            try
            {
                m_hDevice = new Device();
                m_hDevice.Open();
                var versionInformation = m_hDevice.VersionInformation;
                m_lblApiVersion.Text = versionInformation.APIVersion.ToString();
                m_lblHardwareVersion.Text = versionInformation.HardwareVersion.ToString();
                m_lblFirmwareVersion.Text = versionInformation.FirmwareVersion.ToString();
                m_chkDetectFakeFinger.Checked = m_hDevice.DetectFakeFinger;
                m_chkFFD.Checked = m_hDevice.FastFingerDetectMethod;
                m_chkReceiveLongImage.Checked = m_hDevice.ReceiveLongImage;
                var information = m_hDevice.Information;

                if (information.DeviceCompatibility == 0)
                {
                    m_lblCompatibility.Text = "USB 1.1 device";
                }
                else if (information.DeviceCompatibility == 1)
                {
                    m_lblCompatibility.Text = "USB 2.0 device";
                }
                else
                {
                    m_lblCompatibility.Text = "Unknown";
                }

                m_lblImageSize.Text = information.imageSize.ToString();
                m_lblCurrentImageSize.Text = m_hDevice.ImageSize.ToString();
                m_lblEEPROMSize.Text = m_hDevice.MemorySize.ToString(CultureInfo.InvariantCulture.NumberFormat);
                m_grpParameters.Enabled = true;
                m_grpTests.Enabled = true;
                m_cmbInterfaces.Enabled = false;
                m_btnOpenDevice.Enabled = false;
                m_btnClose.Enabled = true;
            }
            catch (ScanAPIException)
            {
                if (m_hDevice != null)
                {
                    m_hDevice.Dispose();
                    m_hDevice = null;
                    OpenDevice();
                }
            }
        }

        private void oR1_CheckedChanged(object sender, EventArgs e)
        {
            if (oR1.Checked)
            {
                Count_Space = 0;
                var f = _url + "R1F" + _namecustomer + ".BMP";
                var l = _url + "R1L" + _namecustomer + ".BMP";
                var r = _url + "R1R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cR1.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oR2.Focus();
                    oR2.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oR1_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("R1");
            button2.Focus();
        }

        private void oR2_CheckedChanged(object sender, EventArgs e)
        {
            if (oR2.Checked)
            {
                Count_Space = 0;
                var f = _url + "R2F" + _namecustomer + ".BMP";
                var l = _url + "R2L" + _namecustomer + ".BMP";
                var r = _url + "R2R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cR2.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oR3.Focus();
                    oR3.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oR2_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("R2");
            button2.Focus();
        }

        private void oR3_CheckedChanged(object sender, EventArgs e)
        {
            if (oR3.Checked)
            {
                Count_Space = 0;
                var f = _url + "R3F" + _namecustomer + ".BMP";
                var l = _url + "R3L" + _namecustomer + ".BMP";
                var r = _url + "R3R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cR3.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oR4.Focus();
                    oR4.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oR3_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("R3");
            button2.Focus();
        }

        private void oR4_CheckedChanged(object sender, EventArgs e)
        {
            if (oR4.Checked)
            {
                Count_Space = 0;
                var f = _url + "R4F" + _namecustomer + ".BMP";
                var l = _url + "R4L" + _namecustomer + ".BMP";
                var r = _url + "R4R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cR4.Checked = true;
                }
                if (Count_Space == 4)
                {
                    oR5.Focus();
                    oR5.Checked = true;
                    pictureBox1.Image = null;
                    pictureBox2.Image = null;
                    pictureBox3.Image = null;
                }
            }
        }

        private void oR4_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("R4");
            button2.Focus();
        }

        private void oR5_CheckedChanged(object sender, EventArgs e)
        {
            if (oR5.Checked)
            {
                Count_Space = 0;
                var f = _url + "R5F" + _namecustomer + ".BMP";
                var l = _url + "R5L" + _namecustomer + ".BMP";
                var r = _url + "R5R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Count_Space++;
                if (Count_Space == 1)
                {
                }
                if (Count_Space == 2)
                {
                    button3_Click();
                }
                if (Count_Space == 3)
                {
                    button4_Click();
                    cR5.Checked = true;
                }
                if (Count_Space == 4)
                {
                    button1.Focus();
                }
            }
        }

        private void oR5_MouseClick(object sender, MouseEventArgs e)
        {
            clear_radio("R5");
            button2.Focus();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (oL1.Checked)
            {
                Count_Space = 0;
                var f = _url + "L1F" + _namecustomer + ".BMP";
                var l = _url + "L1L" + _namecustomer + ".BMP";
                var r = _url + "L1R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void ShowError(ScanAPIException ex)
        {
            string str;
            switch (ex.ErrorCode)
            {
                case 0x57:
                    str = "Error code ERROR_INVALID_PARAMETER";
                    break;

                case 120:
                    str = "Error code ERROR_CALL_NOT_IMPLEMENTED";
                    break;

                case 0x103:
                    str = "Error code ERROR_NO_MORE_ITEMS";
                    break;

                case 0x13:
                    str = "Error code ERROR_WRITE_PROTECT";
                    break;

                case 0x15:
                    str = "Error code ERROR_NOT_READY";
                    break;

                case 50:
                    str = "Error code ERROR_NOT_SUPPORTED";
                    break;

                case 8:
                    str = "Error code ERROR_NOT_ENOUGH_MEMORY";
                    break;

                case 0x5aa:
                    str = "Error code ERROR_NO_SYSTEM_RESOURCES";
                    break;

                case 0x5b4:
                    str = "Error code ERROR_TIMEOUT";
                    break;

                case 0x64a:
                    str = "Error code ERROR_BAD_CONFIGURATION";
                    break;

                case 0x10d2:
                    str = "Error code FTR_ERROR_EMPTY_FRAME";
                    break;

                case 0x20000001:
                    str = "Error code FTR_ERROR_MOVABLE_FINGER";
                    break;

                case 0x20000002:
                    str = "Error code FTR_ERROR_NO_FRAME";
                    break;

                case 0x20000003:
                    str = "Error code FTR_ERROR_USER_CANCELED";
                    break;

                case 0x20000004:
                    str = "Error code FTR_ERROR_HARDWARE_INCOMPATIBLE";
                    break;

                case 0x20000005:
                    str = "Error code FTR_ERROR_FIRMWARE_INCOMPATIBLE";
                    break;

                case 0x20000006:
                    str = "Error code FTR_ERROR_INVALID_AUTHORIZATION_CODE";
                    break;

                case 0x10f0:
                    str = "Error code ERROR_MESSAGE_EXCEEDS_MAX_SIZE";
                    break;

                default:
                    str = string.Format("Error code: {0}", ex.ErrorCode);
                    break;
            }
            MessageBox.Show(str);
        }

        private void TestGetFrame()
        {
            m_bCancelOperation = false;
            MessageDlg.ShowMessageDlg(this, "Put finger to the scanner", Text, OnUserBreak);
            GetFrame();
            MessageDlg.HideMessageDlg();
            if ((m_Frame != null) && (m_Frame.Length != 0))
            {
                var imageSize = m_hDevice.ImageSize;
                var bitmap = CreateBitmap(m_picture.CreateGraphics().GetHdc(), imageSize, m_Frame);
                bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                m_picture.Image = bitmap;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct BITMAPINFOHEADER
        {
            public uint biSize;
            public int biWidth;
            public int biHeight;
            public ushort biPlanes;
            public ushort biBitCount;
            public uint biCompression;
            public uint biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public uint biClrUsed;
            public uint biClrImportant;
        }

        private class ComboBoxItem
        {
            private readonly int m_InterfaceNumber;
            private readonly string m_String;

            public ComboBoxItem(string value, int interfaceNumber)
            {
                m_String = value;
                m_InterfaceNumber = interfaceNumber;
            }

            public int interfaceNumber
            {
                get { return m_InterfaceNumber; }
            }

            public override string ToString()
            {
                return m_String;
            }
        }

        private void oL2_Click(object sender, EventArgs e)
        {
            if (oL2.Checked)
            {
                Count_Space = 0;
                var f = _url + "L2F" + _namecustomer + ".BMP";
                var l = _url + "L2L" + _namecustomer + ".BMP";
                var r = _url + "L2R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL3_Click(object sender, EventArgs e)
        {
            if (oL3.Checked)
            {
                Count_Space = 0;
                var f = _url + "L3F" + _namecustomer + ".BMP";
                var l = _url + "L3L" + _namecustomer + ".BMP";
                var r = _url + "L3R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL4_Click(object sender, EventArgs e)
        {
            if (oL4.Checked)
            {
                Count_Space = 0;
                var f = _url + "L4F" + _namecustomer + ".BMP";
                var l = _url + "L4L" + _namecustomer + ".BMP";
                var r = _url + "L4R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oL5_Click(object sender, EventArgs e)
        {
            if (oL5.Checked)
            {
                Count_Space = 0;
                var f = _url + "L5F" + _namecustomer + ".BMP";
                var l = _url + "L5L" + _namecustomer + ".BMP";
                var r = _url + "L5R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR1_Click(object sender, EventArgs e)
        {
            if (oR1.Checked)
            {
                Count_Space = 0;
                var f = _url + "R1F" + _namecustomer + ".BMP";
                var l = _url + "R1L" + _namecustomer + ".BMP";
                var r = _url + "R1R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR2_Click(object sender, EventArgs e)
        {
            if (oR2.Checked)
            {
                Count_Space = 0;
                var f = _url + "R2F" + _namecustomer + ".BMP";
                var l = _url + "R2L" + _namecustomer + ".BMP";
                var r = _url + "R2R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR3_Click(object sender, EventArgs e)
        {
            if (oR3.Checked)
            {
                Count_Space = 0;
                var f = _url + "R3F" + _namecustomer + ".BMP";
                var l = _url + "R3L" + _namecustomer + ".BMP";
                var r = _url + "R3R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR4_Click(object sender, EventArgs e)
        {
            if (oR4.Checked)
            {
                Count_Space = 0;
                var f = _url + "R4F" + _namecustomer + ".BMP";
                var l = _url + "R4L" + _namecustomer + ".BMP";
                var r = _url + "R4R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }

        private void oR5_Click(object sender, EventArgs e)
        {
            if (oR5.Checked)
            {
                Count_Space = 0;
                var f = _url + "R5F" + _namecustomer + ".BMP";
                var l = _url + "R5L" + _namecustomer + ".BMP";
                var r = _url + "R5R" + _namecustomer + ".BMP";
                load_picture(f, l, r);
            }
        }
    }
}