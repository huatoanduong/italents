﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using FingerprintHelper;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Service;
using iTalent.Analyse.UI.Language;

namespace iTalent.Analyse.UI
{
    public enum IconMessageBox
    {
        Information,
        Warning,
        Error,
        Question
    }

    //public interface IBaseForm : IDisposable
    //{
    //    string VietNamMsg { get; set; }
    //    string EnglishMsg { get; set; }
    //    bool VietNameLanguage { get; }

    //    List<Control> ListconControls { get; set; }

    //    Form Frm { get; set; }
    //}

    public class BaseForm //:IBaseForm
    {
        public BaseForm()
        {
            try
            {
                LoadLang();
            }
            catch
            { }
        }

        public string VietNamMsg { get; set; }
        public string EnglishMsg { get; set; }

        public string Seckey { get; set; }
        public string SerailKey { get; set; }
        public string ProductKey { get; set; }

        protected bool VietNameLanguage { get { return ICurrentSessionService.VietNamLanguage; } }
        public static ICollection<Control> ListconControls { get; set; }
        public static C4FunDataGridView DgView { get; set; }
        public static Form Frm { get; set; }

        public void Dispose()
        {
            EnglishMsg = string.Empty;
            VietNamMsg = string.Empty;
        }

        public void ShowMessage(IconMessageBox typeBox)
        {
            switch (typeBox)
            {
                    case IconMessageBox.Information:
                    MessageBox.Show(VietNameLanguage ? VietNamMsg : EnglishMsg, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    break;
                case IconMessageBox.Warning:
                    MessageBox.Show(VietNameLanguage ? VietNamMsg : EnglishMsg, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    break;
                case IconMessageBox.Error:
                    MessageBox.Show(VietNameLanguage ? VietNamMsg : EnglishMsg, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    break;
                case IconMessageBox.Question:
                    MessageBox.Show(VietNameLanguage ? VietNamMsg : EnglishMsg, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    break;
            }
            EnglishMsg = string.Empty;
            VietNamMsg = string.Empty;
        }

        public void ShowMessage(IconMessageBox typeBox,string message)
        {
            switch (typeBox)
            {
                case IconMessageBox.Information:
                    MessageBox.Show(message, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    break;
                case IconMessageBox.Warning:
                    MessageBox.Show(message, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    break;
                case IconMessageBox.Error:
                    MessageBox.Show(message, VietNameLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    break;
            }
        }

        public void LoadLang()
        {
            Lang.FrmActive = Frm;
            Lang.DgView = DgView;
            Lang.ListControls = ListconControls;
            var lag = Lang.Instance;
        }

        public bool CheckDevice(bool isFormated)
        {
            //if (!RuntimePolicyHelper.LegacyV2RuntimeEnabledSuccessfully) return false;
            Fingerprint.VietNamLanguage = VietNameLanguage;
            using (var fingerprint = new Fingerprint(isFormated))
            {
                Seckey = fingerprint.Seckey;
                SerailKey = fingerprint.Serial;
                ProductKey = fingerprint.ProductKey;
                
                //if (!fingerprint.Isconnect)
                //    MessageBox.Show(fingerprint.Message);
                return fingerprint.Isconnect;
            }
        }

        public void CheckDeviceRegisted()
        {
            //if (!RuntimePolicyHelper.LegacyV2RuntimeEnabledSuccessfully) return false;
            Fingerprint.VietNamLanguage = VietNameLanguage;
            using (var fingerprint = new Fingerprint(true))
            {
                Seckey = fingerprint.Seckey;
                SerailKey = fingerprint.Serial;
                ProductKey = fingerprint.ProductKey;
                if (!ICurrentSessionService.CurAgency.ProductKey.Contains(ProductKey))
                {
                    EnglishMsg = "Your device can not be different format!\nPlease contact supplier.";
                    VietNamMsg = "Thiết bị của bạn không đúng định dạng!\nVui lòng liên hệ nhà cung cấp."; 
                    ShowMessage(IconMessageBox.Warning);
                    Environment.Exit(0);
                }
            }

            
        }
    }
}
