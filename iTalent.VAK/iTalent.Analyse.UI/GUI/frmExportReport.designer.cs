﻿namespace iTalent.Analyse.UI.GUI
{
    partial class frmExportReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExportReport));
            this.c4FunGroupBox1 = new C4FunComponent.Toolkit.C4FunGroupBox();
            this.labDateCreate = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRATD = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtLATD = new System.Windows.Forms.TextBox();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.pbF = new System.Windows.Forms.PictureBox();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.labNote = new System.Windows.Forms.Label();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.labCity = new System.Windows.Forms.Label();
            this.labPhone = new System.Windows.Forms.Label();
            this.labMobile = new System.Windows.Forms.Label();
            this.labReportId = new System.Windows.Forms.Label();
            this.labMa = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.labEmail = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.labAddress = new System.Windows.Forms.Label();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.labParent = new System.Windows.Forms.Label();
            this.txtTenChaMe = new System.Windows.Forms.TextBox();
            this.labBirthday = new System.Windows.Forms.Label();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.labSex = new System.Windows.Forms.Label();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.lablNameForm = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.labStep1 = new System.Windows.Forms.Label();
            this.pnl = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.txtDirSave = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbxLoaiBaoCao = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.gbShowResult = new C4FunComponent.Toolkit.C4FunGroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtR1P = new System.Windows.Forms.TextBox();
            this.txtR2P = new System.Windows.Forms.TextBox();
            this.txtR3P = new System.Windows.Forms.TextBox();
            this.txtR4P = new System.Windows.Forms.TextBox();
            this.txtR5P = new System.Windows.Forms.TextBox();
            this.txtL5P = new System.Windows.Forms.TextBox();
            this.txtL4P = new System.Windows.Forms.TextBox();
            this.txtL3P = new System.Windows.Forms.TextBox();
            this.txtL2P = new System.Windows.Forms.TextBox();
            this.txtL1P = new System.Windows.Forms.TextBox();
            this.txtR1R = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtR1L = new System.Windows.Forms.TextBox();
            this.txtR2R = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtR2L = new System.Windows.Forms.TextBox();
            this.txtR3R = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtR3L = new System.Windows.Forms.TextBox();
            this.txtR4R = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtR4L = new System.Windows.Forms.TextBox();
            this.txtR5R = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtR5L = new System.Windows.Forms.TextBox();
            this.txtL5R = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtL5L = new System.Windows.Forms.TextBox();
            this.txtL4R = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtL4L = new System.Windows.Forms.TextBox();
            this.txtL3R = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtL3L = new System.Windows.Forms.TextBox();
            this.txtL2R = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtL2L = new System.Windows.Forms.TextBox();
            this.c4FunLabel2 = new C4FunComponent.Toolkit.C4FunLabel();
            this.c4FunLabel1 = new C4FunComponent.Toolkit.C4FunLabel();
            this.txtL1R = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtL1L = new System.Windows.Forms.TextBox();
            this.lblShowLeft = new C4FunComponent.Toolkit.C4FunLabel();
            this.lblShowRight = new C4FunComponent.Toolkit.C4FunLabel();
            this.picHand = new System.Windows.Forms.PictureBox();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1.Panel)).BeginInit();
            this.c4FunGroupBox1.Panel.SuspendLayout();
            this.c4FunGroupBox1.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbF)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.pnl.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult.Panel)).BeginInit();
            this.gbShowResult.Panel.SuspendLayout();
            this.gbShowResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHand)).BeginInit();
            this.SuspendLayout();
            // 
            // c4FunGroupBox1
            // 
            this.c4FunGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.c4FunGroupBox1.Location = new System.Drawing.Point(0, 50);
            this.c4FunGroupBox1.Name = "c4FunGroupBox1";
            this.c4FunGroupBox1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            // 
            // c4FunGroupBox1.Panel
            // 
            this.c4FunGroupBox1.Panel.Controls.Add(this.labDateCreate);
            this.c4FunGroupBox1.Panel.Controls.Add(this.label19);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtRATD);
            this.c4FunGroupBox1.Panel.Controls.Add(this.label18);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtLATD);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtTen);
            this.c4FunGroupBox1.Panel.Controls.Add(this.GroupBox5);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtGhiChu);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labNote);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtDiDong);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtDiaChi);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtDienThoai);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labCity);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labPhone);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labMobile);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labReportId);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labMa);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtEmail);
            this.c4FunGroupBox1.Panel.Controls.Add(this.rdFemale);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtThanhPho);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labEmail);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labName);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labAddress);
            this.c4FunGroupBox1.Panel.Controls.Add(this.rdMale);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labParent);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtTenChaMe);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labBirthday);
            this.c4FunGroupBox1.Panel.Controls.Add(this.dtpNgaySinh);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labSex);
            this.c4FunGroupBox1.Size = new System.Drawing.Size(356, 645);
            this.c4FunGroupBox1.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.Red;
            this.c4FunGroupBox1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c4FunGroupBox1.StateCommon.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Near;
            this.c4FunGroupBox1.StateCommon.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunGroupBox1.TabIndex = 235;
            this.c4FunGroupBox1.Tag = "THÔNG TIN KHÁCH HÀNG";
            this.c4FunGroupBox1.Values.Heading = "CUSTOMER INFOMATION";
            // 
            // labDateCreate
            // 
            this.labDateCreate.AutoSize = true;
            this.labDateCreate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labDateCreate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDateCreate.ForeColor = System.Drawing.Color.Red;
            this.labDateCreate.Location = new System.Drawing.Point(0, 0);
            this.labDateCreate.Margin = new System.Windows.Forms.Padding(3);
            this.labDateCreate.Name = "labDateCreate";
            this.labDateCreate.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.labDateCreate.Size = new System.Drawing.Size(162, 24);
            this.labDateCreate.TabIndex = 240;
            this.labDateCreate.Text = "01/06/2015 01:06:07";
            this.labDateCreate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labDateCreate.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 639);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 21);
            this.label19.TabIndex = 239;
            this.label19.Text = "R ATD :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label19.Visible = false;
            // 
            // txtRATD
            // 
            this.txtRATD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtRATD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRATD.Location = new System.Drawing.Point(73, 636);
            this.txtRATD.MaxLength = 5;
            this.txtRATD.Name = "txtRATD";
            this.txtRATD.Size = new System.Drawing.Size(51, 29);
            this.txtRATD.TabIndex = 238;
            this.txtRATD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRATD.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(130, 639);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 21);
            this.label18.TabIndex = 235;
            this.label18.Text = "L ATD :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // txtLATD
            // 
            this.txtLATD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtLATD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLATD.Location = new System.Drawing.Point(197, 636);
            this.txtLATD.MaxLength = 5;
            this.txtLATD.Name = "txtLATD";
            this.txtLATD.Size = new System.Drawing.Size(51, 29);
            this.txtLATD.TabIndex = 234;
            this.txtLATD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLATD.Visible = false;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Enabled = false;
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(124, 22);
            this.txtTen.MaxLength = 250;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(216, 29);
            this.txtTen.TabIndex = 210;
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.pbF);
            this.GroupBox5.Location = new System.Drawing.Point(135, 458);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Padding = new System.Windows.Forms.Padding(1);
            this.GroupBox5.Size = new System.Drawing.Size(135, 155);
            this.GroupBox5.TabIndex = 181;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Tag = "Gương Mặt";
            this.GroupBox5.Text = "FACE";
            // 
            // pbF
            // 
            this.pbF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbF.Location = new System.Drawing.Point(1, 14);
            this.pbF.Name = "pbF";
            this.pbF.Size = new System.Drawing.Size(133, 140);
            this.pbF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbF.TabIndex = 0;
            this.pbF.TabStop = false;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Enabled = false;
            this.txtGhiChu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtGhiChu.Location = new System.Drawing.Point(123, 270);
            this.txtGhiChu.MaxLength = 500;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(216, 78);
            this.txtGhiChu.TabIndex = 218;
            // 
            // labNote
            // 
            this.labNote.BackColor = System.Drawing.Color.Transparent;
            this.labNote.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNote.Location = new System.Drawing.Point(15, 273);
            this.labNote.Name = "labNote";
            this.labNote.Size = new System.Drawing.Size(106, 21);
            this.labNote.TabIndex = 229;
            this.labNote.Tag = "Ghi Chú :";
            this.labNote.Text = "Remark :";
            this.labNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiDong
            // 
            this.txtDiDong.Enabled = false;
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(125, 424);
            this.txtDiDong.MaxLength = 20;
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.Size = new System.Drawing.Size(216, 29);
            this.txtDiDong.TabIndex = 216;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Enabled = false;
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(123, 189);
            this.txtDiaChi.MaxLength = 500;
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(216, 75);
            this.txtDiaChi.TabIndex = 213;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtDienThoai.Enabled = false;
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(125, 354);
            this.txtDienThoai.MaxLength = 20;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(216, 29);
            this.txtDienThoai.TabIndex = 217;
            // 
            // labCity
            // 
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(15, 392);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(107, 21);
            this.labCity.TabIndex = 221;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labPhone
            // 
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(32, 357);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(91, 21);
            this.labPhone.TabIndex = 222;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Phone :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labMobile
            // 
            this.labMobile.AutoSize = true;
            this.labMobile.BackColor = System.Drawing.Color.Transparent;
            this.labMobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMobile.Location = new System.Drawing.Point(58, 428);
            this.labMobile.Name = "labMobile";
            this.labMobile.Size = new System.Drawing.Size(65, 21);
            this.labMobile.TabIndex = 223;
            this.labMobile.Tag = "Di Động :";
            this.labMobile.Text = "Mobile :";
            this.labMobile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labReportId
            // 
            this.labReportId.AutoSize = true;
            this.labReportId.BackColor = System.Drawing.Color.Transparent;
            this.labReportId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labReportId.Location = new System.Drawing.Point(642, 601);
            this.labReportId.Name = "labReportId";
            this.labReportId.Size = new System.Drawing.Size(70, 21);
            this.labReportId.TabIndex = 231;
            this.labReportId.Tag = "";
            this.labReportId.Text = "ReportId";
            this.labReportId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labReportId.Visible = false;
            // 
            // labMa
            // 
            this.labMa.AutoSize = true;
            this.labMa.BackColor = System.Drawing.Color.Transparent;
            this.labMa.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMa.Location = new System.Drawing.Point(642, 639);
            this.labMa.Name = "labMa";
            this.labMa.Size = new System.Drawing.Size(25, 21);
            this.labMa.TabIndex = 230;
            this.labMa.Tag = "0";
            this.labMa.Text = "ID";
            this.labMa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labMa.Visible = false;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEmail.Enabled = false;
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(124, 154);
            this.txtEmail.MaxLength = 250;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(216, 29);
            this.txtEmail.TabIndex = 215;
            // 
            // rdFemale
            // 
            this.rdFemale.AutoSize = true;
            this.rdFemale.BackColor = System.Drawing.Color.Transparent;
            this.rdFemale.Enabled = false;
            this.rdFemale.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdFemale.ForeColor = System.Drawing.Color.Navy;
            this.rdFemale.Location = new System.Drawing.Point(193, 92);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(69, 21);
            this.rdFemale.TabIndex = 233;
            this.rdFemale.Tag = "Nữ";
            this.rdFemale.Text = "Female";
            this.rdFemale.UseVisualStyleBackColor = false;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.Enabled = false;
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(125, 389);
            this.txtThanhPho.MaxLength = 20;
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.Size = new System.Drawing.Size(216, 29);
            this.txtThanhPho.TabIndex = 214;
            // 
            // labEmail
            // 
            this.labEmail.AutoSize = true;
            this.labEmail.BackColor = System.Drawing.Color.White;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(68, 162);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(55, 21);
            this.labEmail.TabIndex = 224;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labName
            // 
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(15, 25);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(107, 21);
            this.labName.TabIndex = 219;
            this.labName.Tag = "Tên Khách Hàng :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labAddress
            // 
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(18, 192);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(103, 21);
            this.labAddress.TabIndex = 220;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rdMale
            // 
            this.rdMale.AutoSize = true;
            this.rdMale.BackColor = System.Drawing.Color.Transparent;
            this.rdMale.Checked = true;
            this.rdMale.Enabled = false;
            this.rdMale.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdMale.ForeColor = System.Drawing.Color.Navy;
            this.rdMale.Location = new System.Drawing.Point(124, 92);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(55, 21);
            this.rdMale.TabIndex = 232;
            this.rdMale.TabStop = true;
            this.rdMale.Tag = "Nam";
            this.rdMale.Text = "Male";
            this.rdMale.UseVisualStyleBackColor = false;
            // 
            // labParent
            // 
            this.labParent.BackColor = System.Drawing.Color.Transparent;
            this.labParent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labParent.Location = new System.Drawing.Point(23, 60);
            this.labParent.Name = "labParent";
            this.labParent.Size = new System.Drawing.Size(98, 21);
            this.labParent.TabIndex = 225;
            this.labParent.Tag = "Tên Cha/Mẹ:";
            this.labParent.Text = "Parent :";
            this.labParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTenChaMe
            // 
            this.txtTenChaMe.Enabled = false;
            this.txtTenChaMe.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChaMe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTenChaMe.Location = new System.Drawing.Point(124, 57);
            this.txtTenChaMe.MaxLength = 250;
            this.txtTenChaMe.Name = "txtTenChaMe";
            this.txtTenChaMe.Size = new System.Drawing.Size(216, 29);
            this.txtTenChaMe.TabIndex = 211;
            // 
            // labBirthday
            // 
            this.labBirthday.BackColor = System.Drawing.Color.Transparent;
            this.labBirthday.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labBirthday.Location = new System.Drawing.Point(29, 123);
            this.labBirthday.Name = "labBirthday";
            this.labBirthday.Size = new System.Drawing.Size(92, 21);
            this.labBirthday.TabIndex = 228;
            this.labBirthday.Tag = "Ngày Sinh :";
            this.labBirthday.Text = "Birthday :";
            this.labBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CustomFormat = "MM/dd/yyyy";
            this.dtpNgaySinh.Enabled = false;
            this.dtpNgaySinh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(124, 117);
            this.dtpNgaySinh.MaxDate = new System.DateTime(5000, 12, 31, 0, 0, 0, 0);
            this.dtpNgaySinh.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(216, 29);
            this.dtpNgaySinh.TabIndex = 212;
            this.dtpNgaySinh.Value = new System.DateTime(2015, 9, 13, 0, 0, 0, 0);
            // 
            // labSex
            // 
            this.labSex.BackColor = System.Drawing.Color.Transparent;
            this.labSex.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSex.Location = new System.Drawing.Point(30, 91);
            this.labSex.Name = "labSex";
            this.labSex.Size = new System.Drawing.Size(91, 21);
            this.labSex.TabIndex = 226;
            this.labSex.Tag = "Giới Tính :";
            this.labSex.Text = "Sex :";
            this.labSex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.pnlMenu.Controls.Add(this.lablNameForm);
            this.pnlMenu.Controls.Add(this.btnThoat);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(885, 50);
            this.pnlMenu.TabIndex = 236;
            // 
            // lablNameForm
            // 
            this.lablNameForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lablNameForm.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablNameForm.ForeColor = System.Drawing.Color.White;
            this.lablNameForm.Location = new System.Drawing.Point(0, 0);
            this.lablNameForm.Name = "lablNameForm";
            this.lablNameForm.Size = new System.Drawing.Size(845, 50);
            this.lablNameForm.TabIndex = 10;
            this.lablNameForm.Tag = ".: XUẤT BÁO CÁO";
            this.lablNameForm.Text = ".: EXPORT REPORT";
            this.lablNameForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnThoat
            // 
            this.btnThoat.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThoat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnThoat.FlatAppearance.BorderSize = 0;
            this.btnThoat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnThoat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnThoat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoat.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.ForeColor = System.Drawing.Color.White;
            this.btnThoat.Image = global::iTalent.Analyse.UI.Properties.Resources.close;
            this.btnThoat.Location = new System.Drawing.Point(845, 0);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(0);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(40, 50);
            this.btnThoat.TabIndex = 15;
            this.btnThoat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // labStep1
            // 
            this.labStep1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.labStep1.ForeColor = System.Drawing.Color.Navy;
            this.labStep1.Location = new System.Drawing.Point(17, 8);
            this.labStep1.Name = "labStep1";
            this.labStep1.Size = new System.Drawing.Size(466, 24);
            this.labStep1.TabIndex = 237;
            this.labStep1.Tag = "1. CHỌN NƠI XUẤT BÁO CÁO";
            this.labStep1.Text = " 1. WHERE DO YOU EXPORT REPORT. PLEASE CHOOSE?";
            // 
            // pnl
            // 
            this.pnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl.Controls.Add(this.btnImport);
            this.pnl.Controls.Add(this.txtDirSave);
            this.pnl.Controls.Add(this.labStep1);
            this.pnl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnl.Location = new System.Drawing.Point(356, 473);
            this.pnl.Name = "pnl";
            this.pnl.Size = new System.Drawing.Size(529, 82);
            this.pnl.TabIndex = 238;
            // 
            // btnImport
            // 
            this.btnImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnImport.FlatAppearance.BorderSize = 0;
            this.btnImport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnImport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImport.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImport.ForeColor = System.Drawing.Color.White;
            this.btnImport.Location = new System.Drawing.Point(446, 30);
            this.btnImport.Margin = new System.Windows.Forms.Padding(0);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(40, 36);
            this.btnImport.TabIndex = 239;
            this.btnImport.Text = "...";
            this.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // txtDirSave
            // 
            this.txtDirSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtDirSave.Enabled = false;
            this.txtDirSave.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.txtDirSave.Location = new System.Drawing.Point(40, 35);
            this.txtDirSave.Name = "txtDirSave";
            this.txtDirSave.ReadOnly = true;
            this.txtDirSave.Size = new System.Drawing.Size(395, 28);
            this.txtDirSave.TabIndex = 238;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cbxLoaiBaoCao);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(356, 555);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 76);
            this.panel1.TabIndex = 239;
            // 
            // cbxLoaiBaoCao
            // 
            this.cbxLoaiBaoCao.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxLoaiBaoCao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxLoaiBaoCao.DropDownWidth = 120;
            this.cbxLoaiBaoCao.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxLoaiBaoCao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.cbxLoaiBaoCao.FormattingEnabled = true;
            this.cbxLoaiBaoCao.ItemHeight = 21;
            this.cbxLoaiBaoCao.Location = new System.Drawing.Point(81, 36);
            this.cbxLoaiBaoCao.Name = "cbxLoaiBaoCao";
            this.cbxLoaiBaoCao.Size = new System.Drawing.Size(267, 29);
            this.cbxLoaiBaoCao.TabIndex = 238;
            this.cbxLoaiBaoCao.Tag = "Nữ";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(17, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(466, 24);
            this.label1.TabIndex = 237;
            this.label1.Tag = "2. CHỌN LOẠI BÁO CÁO CẦN XUẤT ";
            this.label1.Text = "2. CHOOSE REPORT TYPE";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btnExport);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(356, 631);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(529, 64);
            this.panel2.TabIndex = 240;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.White;
            this.btnExport.Location = new System.Drawing.Point(203, 14);
            this.btnExport.Margin = new System.Windows.Forms.Padding(0);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(148, 36);
            this.btnExport.TabIndex = 240;
            this.btnExport.Tag = "XUẤT BÁO CÁO";
            this.btnExport.Text = "EXPORT REPORT";
            this.btnExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(17, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(476, 24);
            this.label2.TabIndex = 237;
            this.label2.Tag = "3. XUẤT BÁO CÁO ";
            this.label2.Text = "3. EXPORT REPORT";
            // 
            // gbShowResult
            // 
            this.gbShowResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbShowResult.Location = new System.Drawing.Point(356, 50);
            this.gbShowResult.Name = "gbShowResult";
            this.gbShowResult.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            // 
            // gbShowResult.Panel
            // 
            this.gbShowResult.Panel.Controls.Add(this.label22);
            this.gbShowResult.Panel.Controls.Add(this.label21);
            this.gbShowResult.Panel.Controls.Add(this.label20);
            this.gbShowResult.Panel.Controls.Add(this.label17);
            this.gbShowResult.Panel.Controls.Add(this.label16);
            this.gbShowResult.Panel.Controls.Add(this.label15);
            this.gbShowResult.Panel.Controls.Add(this.label14);
            this.gbShowResult.Panel.Controls.Add(this.label13);
            this.gbShowResult.Panel.Controls.Add(this.label12);
            this.gbShowResult.Panel.Controls.Add(this.label11);
            this.gbShowResult.Panel.Controls.Add(this.txtR1P);
            this.gbShowResult.Panel.Controls.Add(this.txtR2P);
            this.gbShowResult.Panel.Controls.Add(this.txtR3P);
            this.gbShowResult.Panel.Controls.Add(this.txtR4P);
            this.gbShowResult.Panel.Controls.Add(this.txtR5P);
            this.gbShowResult.Panel.Controls.Add(this.txtL5P);
            this.gbShowResult.Panel.Controls.Add(this.txtL4P);
            this.gbShowResult.Panel.Controls.Add(this.txtL3P);
            this.gbShowResult.Panel.Controls.Add(this.txtL2P);
            this.gbShowResult.Panel.Controls.Add(this.txtL1P);
            this.gbShowResult.Panel.Controls.Add(this.txtR1R);
            this.gbShowResult.Panel.Controls.Add(this.label10);
            this.gbShowResult.Panel.Controls.Add(this.txtR1L);
            this.gbShowResult.Panel.Controls.Add(this.txtR2R);
            this.gbShowResult.Panel.Controls.Add(this.label9);
            this.gbShowResult.Panel.Controls.Add(this.txtR2L);
            this.gbShowResult.Panel.Controls.Add(this.txtR3R);
            this.gbShowResult.Panel.Controls.Add(this.label8);
            this.gbShowResult.Panel.Controls.Add(this.txtR3L);
            this.gbShowResult.Panel.Controls.Add(this.txtR4R);
            this.gbShowResult.Panel.Controls.Add(this.label7);
            this.gbShowResult.Panel.Controls.Add(this.txtR4L);
            this.gbShowResult.Panel.Controls.Add(this.txtR5R);
            this.gbShowResult.Panel.Controls.Add(this.label6);
            this.gbShowResult.Panel.Controls.Add(this.txtR5L);
            this.gbShowResult.Panel.Controls.Add(this.txtL5R);
            this.gbShowResult.Panel.Controls.Add(this.label5);
            this.gbShowResult.Panel.Controls.Add(this.txtL5L);
            this.gbShowResult.Panel.Controls.Add(this.txtL4R);
            this.gbShowResult.Panel.Controls.Add(this.label4);
            this.gbShowResult.Panel.Controls.Add(this.txtL4L);
            this.gbShowResult.Panel.Controls.Add(this.txtL3R);
            this.gbShowResult.Panel.Controls.Add(this.label3);
            this.gbShowResult.Panel.Controls.Add(this.txtL3L);
            this.gbShowResult.Panel.Controls.Add(this.txtL2R);
            this.gbShowResult.Panel.Controls.Add(this.label23);
            this.gbShowResult.Panel.Controls.Add(this.txtL2L);
            this.gbShowResult.Panel.Controls.Add(this.c4FunLabel2);
            this.gbShowResult.Panel.Controls.Add(this.c4FunLabel1);
            this.gbShowResult.Panel.Controls.Add(this.txtL1R);
            this.gbShowResult.Panel.Controls.Add(this.label24);
            this.gbShowResult.Panel.Controls.Add(this.txtL1L);
            this.gbShowResult.Panel.Controls.Add(this.lblShowLeft);
            this.gbShowResult.Panel.Controls.Add(this.lblShowRight);
            this.gbShowResult.Panel.Controls.Add(this.picHand);
            this.gbShowResult.Size = new System.Drawing.Size(529, 423);
            this.gbShowResult.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbShowResult.TabIndex = 241;
            this.gbShowResult.Tag = "KẾT QUẢ";
            this.gbShowResult.Values.Heading = "RESULT";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label22.Location = new System.Drawing.Point(445, 184);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(21, 17);
            this.label22.TabIndex = 111;
            this.label22.Text = "R1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(416, 129);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 17);
            this.label21.TabIndex = 110;
            this.label21.Text = "R2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label20.Location = new System.Drawing.Point(371, 116);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 17);
            this.label20.TabIndex = 109;
            this.label20.Text = "R3";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(324, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 17);
            this.label17.TabIndex = 108;
            this.label17.Text = "R4";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(289, 153);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 17);
            this.label16.TabIndex = 107;
            this.label16.Text = "R5";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label15.Location = new System.Drawing.Point(251, 153);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 17);
            this.label15.TabIndex = 106;
            this.label15.Text = "L5";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label14.Location = new System.Drawing.Point(218, 129);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 17);
            this.label14.TabIndex = 105;
            this.label14.Text = "L4";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label13.Location = new System.Drawing.Point(172, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 17);
            this.label13.TabIndex = 104;
            this.label13.Text = "L3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label12.Location = new System.Drawing.Point(125, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 17);
            this.label12.TabIndex = 103;
            this.label12.Text = "L2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(98, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 17);
            this.label11.TabIndex = 102;
            this.label11.Text = "L1";
            // 
            // txtR1P
            // 
            this.txtR1P.BackColor = System.Drawing.Color.White;
            this.txtR1P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR1P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1P.ForeColor = System.Drawing.Color.Gray;
            this.txtR1P.Location = new System.Drawing.Point(439, 215);
            this.txtR1P.MaxLength = 2;
            this.txtR1P.Name = "txtR1P";
            this.txtR1P.ReadOnly = true;
            this.txtR1P.Size = new System.Drawing.Size(27, 22);
            this.txtR1P.TabIndex = 101;
            this.txtR1P.Tag = "L1L";
            this.txtR1P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR2P
            // 
            this.txtR2P.BackColor = System.Drawing.Color.White;
            this.txtR2P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR2P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2P.ForeColor = System.Drawing.Color.Gray;
            this.txtR2P.Location = new System.Drawing.Point(410, 159);
            this.txtR2P.MaxLength = 2;
            this.txtR2P.Name = "txtR2P";
            this.txtR2P.ReadOnly = true;
            this.txtR2P.Size = new System.Drawing.Size(27, 22);
            this.txtR2P.TabIndex = 100;
            this.txtR2P.Tag = "L1L";
            this.txtR2P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR3P
            // 
            this.txtR3P.BackColor = System.Drawing.Color.White;
            this.txtR3P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR3P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3P.ForeColor = System.Drawing.Color.Gray;
            this.txtR3P.Location = new System.Drawing.Point(367, 146);
            this.txtR3P.MaxLength = 2;
            this.txtR3P.Name = "txtR3P";
            this.txtR3P.ReadOnly = true;
            this.txtR3P.Size = new System.Drawing.Size(27, 22);
            this.txtR3P.TabIndex = 99;
            this.txtR3P.Tag = "L1L";
            this.txtR3P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR4P
            // 
            this.txtR4P.BackColor = System.Drawing.Color.White;
            this.txtR4P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR4P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4P.ForeColor = System.Drawing.Color.Gray;
            this.txtR4P.Location = new System.Drawing.Point(324, 157);
            this.txtR4P.MaxLength = 2;
            this.txtR4P.Name = "txtR4P";
            this.txtR4P.ReadOnly = true;
            this.txtR4P.Size = new System.Drawing.Size(27, 22);
            this.txtR4P.TabIndex = 98;
            this.txtR4P.Tag = "L1L";
            this.txtR4P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR5P
            // 
            this.txtR5P.BackColor = System.Drawing.Color.White;
            this.txtR5P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR5P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5P.ForeColor = System.Drawing.Color.Gray;
            this.txtR5P.Location = new System.Drawing.Point(292, 184);
            this.txtR5P.MaxLength = 2;
            this.txtR5P.Name = "txtR5P";
            this.txtR5P.ReadOnly = true;
            this.txtR5P.Size = new System.Drawing.Size(27, 22);
            this.txtR5P.TabIndex = 97;
            this.txtR5P.Tag = "L1L";
            this.txtR5P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL5P
            // 
            this.txtL5P.BackColor = System.Drawing.Color.White;
            this.txtL5P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL5P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5P.ForeColor = System.Drawing.Color.Gray;
            this.txtL5P.Location = new System.Drawing.Point(245, 184);
            this.txtL5P.MaxLength = 2;
            this.txtL5P.Name = "txtL5P";
            this.txtL5P.ReadOnly = true;
            this.txtL5P.Size = new System.Drawing.Size(27, 22);
            this.txtL5P.TabIndex = 96;
            this.txtL5P.Tag = "L1L";
            this.txtL5P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL4P
            // 
            this.txtL4P.BackColor = System.Drawing.Color.White;
            this.txtL4P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL4P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4P.ForeColor = System.Drawing.Color.Gray;
            this.txtL4P.Location = new System.Drawing.Point(214, 157);
            this.txtL4P.MaxLength = 2;
            this.txtL4P.Name = "txtL4P";
            this.txtL4P.ReadOnly = true;
            this.txtL4P.Size = new System.Drawing.Size(27, 22);
            this.txtL4P.TabIndex = 95;
            this.txtL4P.Tag = "L1L";
            this.txtL4P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL3P
            // 
            this.txtL3P.BackColor = System.Drawing.Color.White;
            this.txtL3P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL3P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3P.ForeColor = System.Drawing.Color.Gray;
            this.txtL3P.Location = new System.Drawing.Point(171, 146);
            this.txtL3P.MaxLength = 2;
            this.txtL3P.Name = "txtL3P";
            this.txtL3P.ReadOnly = true;
            this.txtL3P.Size = new System.Drawing.Size(27, 22);
            this.txtL3P.TabIndex = 94;
            this.txtL3P.Tag = "L1L";
            this.txtL3P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL2P
            // 
            this.txtL2P.BackColor = System.Drawing.Color.White;
            this.txtL2P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL2P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2P.ForeColor = System.Drawing.Color.Gray;
            this.txtL2P.Location = new System.Drawing.Point(128, 159);
            this.txtL2P.MaxLength = 2;
            this.txtL2P.Name = "txtL2P";
            this.txtL2P.ReadOnly = true;
            this.txtL2P.Size = new System.Drawing.Size(27, 22);
            this.txtL2P.TabIndex = 93;
            this.txtL2P.Tag = "L1L";
            this.txtL2P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL1P
            // 
            this.txtL1P.BackColor = System.Drawing.Color.White;
            this.txtL1P.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL1P.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1P.ForeColor = System.Drawing.Color.Gray;
            this.txtL1P.Location = new System.Drawing.Point(99, 215);
            this.txtL1P.MaxLength = 2;
            this.txtL1P.Name = "txtL1P";
            this.txtL1P.ReadOnly = true;
            this.txtL1P.Size = new System.Drawing.Size(27, 22);
            this.txtL1P.TabIndex = 92;
            this.txtL1P.Tag = "L1L";
            this.txtL1P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR1R
            // 
            this.txtR1R.BackColor = System.Drawing.Color.White;
            this.txtR1R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR1R.Location = new System.Drawing.Point(455, 70);
            this.txtR1R.MaxLength = 2;
            this.txtR1R.Name = "txtR1R";
            this.txtR1R.ReadOnly = true;
            this.txtR1R.Size = new System.Drawing.Size(27, 29);
            this.txtR1R.TabIndex = 91;
            this.txtR1R.Tag = "L1L";
            this.txtR1R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(458, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 17);
            this.label10.TabIndex = 90;
            this.label10.Text = "R1";
            // 
            // txtR1L
            // 
            this.txtR1L.BackColor = System.Drawing.Color.White;
            this.txtR1L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR1L.Location = new System.Drawing.Point(455, 35);
            this.txtR1L.MaxLength = 2;
            this.txtR1L.Name = "txtR1L";
            this.txtR1L.ReadOnly = true;
            this.txtR1L.Size = new System.Drawing.Size(27, 29);
            this.txtR1L.TabIndex = 89;
            this.txtR1L.Tag = "L1L";
            this.txtR1L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR2R
            // 
            this.txtR2R.BackColor = System.Drawing.Color.White;
            this.txtR2R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR2R.Location = new System.Drawing.Point(414, 70);
            this.txtR2R.MaxLength = 2;
            this.txtR2R.Name = "txtR2R";
            this.txtR2R.ReadOnly = true;
            this.txtR2R.Size = new System.Drawing.Size(27, 29);
            this.txtR2R.TabIndex = 88;
            this.txtR2R.Tag = "L1L";
            this.txtR2R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(417, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 17);
            this.label9.TabIndex = 87;
            this.label9.Text = "R2";
            // 
            // txtR2L
            // 
            this.txtR2L.BackColor = System.Drawing.Color.White;
            this.txtR2L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR2L.Location = new System.Drawing.Point(414, 35);
            this.txtR2L.MaxLength = 2;
            this.txtR2L.Name = "txtR2L";
            this.txtR2L.ReadOnly = true;
            this.txtR2L.Size = new System.Drawing.Size(27, 29);
            this.txtR2L.TabIndex = 86;
            this.txtR2L.Tag = "L1L";
            this.txtR2L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR3R
            // 
            this.txtR3R.BackColor = System.Drawing.Color.White;
            this.txtR3R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR3R.Location = new System.Drawing.Point(373, 70);
            this.txtR3R.MaxLength = 2;
            this.txtR3R.Name = "txtR3R";
            this.txtR3R.ReadOnly = true;
            this.txtR3R.Size = new System.Drawing.Size(27, 29);
            this.txtR3R.TabIndex = 85;
            this.txtR3R.Tag = "L1L";
            this.txtR3R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(376, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 17);
            this.label8.TabIndex = 84;
            this.label8.Text = "R3";
            // 
            // txtR3L
            // 
            this.txtR3L.BackColor = System.Drawing.Color.White;
            this.txtR3L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR3L.Location = new System.Drawing.Point(373, 35);
            this.txtR3L.MaxLength = 2;
            this.txtR3L.Name = "txtR3L";
            this.txtR3L.ReadOnly = true;
            this.txtR3L.Size = new System.Drawing.Size(27, 29);
            this.txtR3L.TabIndex = 83;
            this.txtR3L.Tag = "L1L";
            this.txtR3L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR4R
            // 
            this.txtR4R.BackColor = System.Drawing.Color.White;
            this.txtR4R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR4R.Location = new System.Drawing.Point(332, 70);
            this.txtR4R.MaxLength = 2;
            this.txtR4R.Name = "txtR4R";
            this.txtR4R.ReadOnly = true;
            this.txtR4R.Size = new System.Drawing.Size(27, 29);
            this.txtR4R.TabIndex = 82;
            this.txtR4R.Tag = "L1L";
            this.txtR4R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(336, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 17);
            this.label7.TabIndex = 81;
            this.label7.Text = "R4";
            // 
            // txtR4L
            // 
            this.txtR4L.BackColor = System.Drawing.Color.White;
            this.txtR4L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR4L.Location = new System.Drawing.Point(332, 35);
            this.txtR4L.MaxLength = 2;
            this.txtR4L.Name = "txtR4L";
            this.txtR4L.ReadOnly = true;
            this.txtR4L.Size = new System.Drawing.Size(27, 29);
            this.txtR4L.TabIndex = 80;
            this.txtR4L.Tag = "L1L";
            this.txtR4L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR5R
            // 
            this.txtR5R.BackColor = System.Drawing.Color.White;
            this.txtR5R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR5R.Location = new System.Drawing.Point(291, 70);
            this.txtR5R.MaxLength = 2;
            this.txtR5R.Name = "txtR5R";
            this.txtR5R.ReadOnly = true;
            this.txtR5R.Size = new System.Drawing.Size(27, 29);
            this.txtR5R.TabIndex = 79;
            this.txtR5R.Tag = "L1L";
            this.txtR5R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(293, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 17);
            this.label6.TabIndex = 78;
            this.label6.Text = "R5";
            // 
            // txtR5L
            // 
            this.txtR5L.BackColor = System.Drawing.Color.White;
            this.txtR5L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR5L.Location = new System.Drawing.Point(291, 35);
            this.txtR5L.MaxLength = 2;
            this.txtR5L.Name = "txtR5L";
            this.txtR5L.ReadOnly = true;
            this.txtR5L.Size = new System.Drawing.Size(27, 29);
            this.txtR5L.TabIndex = 77;
            this.txtR5L.Tag = "L1L";
            this.txtR5L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL5R
            // 
            this.txtL5R.BackColor = System.Drawing.Color.White;
            this.txtL5R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL5R.Location = new System.Drawing.Point(250, 70);
            this.txtL5R.MaxLength = 2;
            this.txtL5R.Name = "txtL5R";
            this.txtL5R.ReadOnly = true;
            this.txtL5R.Size = new System.Drawing.Size(27, 29);
            this.txtL5R.TabIndex = 76;
            this.txtL5R.Tag = "L1L";
            this.txtL5R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(252, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 17);
            this.label5.TabIndex = 75;
            this.label5.Text = "L5";
            // 
            // txtL5L
            // 
            this.txtL5L.BackColor = System.Drawing.Color.White;
            this.txtL5L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL5L.Location = new System.Drawing.Point(250, 35);
            this.txtL5L.MaxLength = 2;
            this.txtL5L.Name = "txtL5L";
            this.txtL5L.ReadOnly = true;
            this.txtL5L.Size = new System.Drawing.Size(27, 29);
            this.txtL5L.TabIndex = 74;
            this.txtL5L.Tag = "L1L";
            this.txtL5L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL4R
            // 
            this.txtL4R.BackColor = System.Drawing.Color.White;
            this.txtL4R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL4R.Location = new System.Drawing.Point(209, 70);
            this.txtL4R.MaxLength = 2;
            this.txtL4R.Name = "txtL4R";
            this.txtL4R.ReadOnly = true;
            this.txtL4R.Size = new System.Drawing.Size(27, 29);
            this.txtL4R.TabIndex = 73;
            this.txtL4R.Tag = "L1L";
            this.txtL4R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(211, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 17);
            this.label4.TabIndex = 72;
            this.label4.Text = "L4";
            // 
            // txtL4L
            // 
            this.txtL4L.BackColor = System.Drawing.Color.White;
            this.txtL4L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL4L.Location = new System.Drawing.Point(209, 35);
            this.txtL4L.MaxLength = 2;
            this.txtL4L.Name = "txtL4L";
            this.txtL4L.ReadOnly = true;
            this.txtL4L.Size = new System.Drawing.Size(27, 29);
            this.txtL4L.TabIndex = 71;
            this.txtL4L.Tag = "L1L";
            this.txtL4L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL3R
            // 
            this.txtL3R.BackColor = System.Drawing.Color.White;
            this.txtL3R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL3R.Location = new System.Drawing.Point(168, 70);
            this.txtL3R.MaxLength = 2;
            this.txtL3R.Name = "txtL3R";
            this.txtL3R.ReadOnly = true;
            this.txtL3R.Size = new System.Drawing.Size(27, 29);
            this.txtL3R.TabIndex = 70;
            this.txtL3R.Tag = "L1L";
            this.txtL3R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(170, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 17);
            this.label3.TabIndex = 69;
            this.label3.Text = "L3";
            // 
            // txtL3L
            // 
            this.txtL3L.BackColor = System.Drawing.Color.White;
            this.txtL3L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL3L.Location = new System.Drawing.Point(168, 35);
            this.txtL3L.MaxLength = 2;
            this.txtL3L.Name = "txtL3L";
            this.txtL3L.ReadOnly = true;
            this.txtL3L.Size = new System.Drawing.Size(27, 29);
            this.txtL3L.TabIndex = 68;
            this.txtL3L.Tag = "L1L";
            this.txtL3L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL2R
            // 
            this.txtL2R.BackColor = System.Drawing.Color.White;
            this.txtL2R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL2R.Location = new System.Drawing.Point(127, 70);
            this.txtL2R.MaxLength = 2;
            this.txtL2R.Name = "txtL2R";
            this.txtL2R.ReadOnly = true;
            this.txtL2R.Size = new System.Drawing.Size(27, 29);
            this.txtL2R.TabIndex = 67;
            this.txtL2R.Tag = "L1L";
            this.txtL2R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label23.Location = new System.Drawing.Point(129, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(21, 17);
            this.label23.TabIndex = 66;
            this.label23.Text = "L2";
            // 
            // txtL2L
            // 
            this.txtL2L.BackColor = System.Drawing.Color.White;
            this.txtL2L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL2L.Location = new System.Drawing.Point(127, 35);
            this.txtL2L.MaxLength = 2;
            this.txtL2L.Name = "txtL2L";
            this.txtL2L.ReadOnly = true;
            this.txtL2L.Size = new System.Drawing.Size(27, 29);
            this.txtL2L.TabIndex = 65;
            this.txtL2L.Tag = "L1L";
            this.txtL2L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c4FunLabel2
            // 
            this.c4FunLabel2.Location = new System.Drawing.Point(14, 75);
            this.c4FunLabel2.Name = "c4FunLabel2";
            this.c4FunLabel2.Size = new System.Drawing.Size(56, 20);
            this.c4FunLabel2.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.c4FunLabel2.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.c4FunLabel2.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel2.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel2.TabIndex = 64;
            this.c4FunLabel2.Tag = "PHẢI";
            this.c4FunLabel2.Values.Text = "RIGHT";
            // 
            // c4FunLabel1
            // 
            this.c4FunLabel1.Location = new System.Drawing.Point(16, 40);
            this.c4FunLabel1.Name = "c4FunLabel1";
            this.c4FunLabel1.Size = new System.Drawing.Size(47, 20);
            this.c4FunLabel1.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.c4FunLabel1.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.c4FunLabel1.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel1.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel1.TabIndex = 63;
            this.c4FunLabel1.Tag = "TRÁI";
            this.c4FunLabel1.Values.Text = "LEFT";
            // 
            // txtL1R
            // 
            this.txtL1R.BackColor = System.Drawing.Color.White;
            this.txtL1R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL1R.Location = new System.Drawing.Point(86, 70);
            this.txtL1R.MaxLength = 2;
            this.txtL1R.Name = "txtL1R";
            this.txtL1R.ReadOnly = true;
            this.txtL1R.Size = new System.Drawing.Size(27, 29);
            this.txtL1R.TabIndex = 62;
            this.txtL1R.Tag = "L1L";
            this.txtL1R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label24.Location = new System.Drawing.Point(90, 14);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 17);
            this.label24.TabIndex = 61;
            this.label24.Text = "L1";
            // 
            // txtL1L
            // 
            this.txtL1L.BackColor = System.Drawing.Color.White;
            this.txtL1L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL1L.Location = new System.Drawing.Point(86, 35);
            this.txtL1L.MaxLength = 2;
            this.txtL1L.Name = "txtL1L";
            this.txtL1L.ReadOnly = true;
            this.txtL1L.Size = new System.Drawing.Size(27, 29);
            this.txtL1L.TabIndex = 59;
            this.txtL1L.Tag = "L1L";
            this.txtL1L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblShowLeft
            // 
            this.lblShowLeft.Location = new System.Drawing.Point(168, 334);
            this.lblShowLeft.Name = "lblShowLeft";
            this.lblShowLeft.Size = new System.Drawing.Size(47, 20);
            this.lblShowLeft.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.lblShowLeft.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblShowLeft.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowLeft.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowLeft.TabIndex = 57;
            this.lblShowLeft.Tag = "TRÁI";
            this.lblShowLeft.Values.Text = "LEFT";
            // 
            // lblShowRight
            // 
            this.lblShowRight.Location = new System.Drawing.Point(349, 335);
            this.lblShowRight.Name = "lblShowRight";
            this.lblShowRight.Size = new System.Drawing.Size(56, 20);
            this.lblShowRight.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.lblShowRight.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblShowRight.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowRight.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowRight.TabIndex = 58;
            this.lblShowRight.Tag = "PHẢI";
            this.lblShowRight.Values.Text = "RIGHT";
            // 
            // picHand
            // 
            this.picHand.Image = global::iTalent.Analyse.UI.Properties.Resources.doibantay;
            this.picHand.Location = new System.Drawing.Point(84, 105);
            this.picHand.Name = "picHand";
            this.picHand.Size = new System.Drawing.Size(396, 252);
            this.picHand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHand.TabIndex = 56;
            this.picHand.TabStop = false;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "WT.bmp");
            this.ImageList1.Images.SetKeyName(1, "WS.bmp");
            this.ImageList1.Images.SetKeyName(2, "WX.bmp");
            this.ImageList1.Images.SetKeyName(3, "WE.bmp");
            this.ImageList1.Images.SetKeyName(4, "WC.bmp");
            this.ImageList1.Images.SetKeyName(5, "WD.bmp");
            this.ImageList1.Images.SetKeyName(6, "WI.bmp");
            this.ImageList1.Images.SetKeyName(7, "UL.bmp");
            this.ImageList1.Images.SetKeyName(8, "LF.bmp");
            this.ImageList1.Images.SetKeyName(9, "RL.bmp");
            this.ImageList1.Images.SetKeyName(10, "AS.bmp");
            this.ImageList1.Images.SetKeyName(11, "AT.bmp");
            this.ImageList1.Images.SetKeyName(12, "AU.bmp");
            this.ImageList1.Images.SetKeyName(13, "AE.bmp");
            this.ImageList1.Images.SetKeyName(14, "WP.bmp");
            this.ImageList1.Images.SetKeyName(15, "WL.bmp");
            // 
            // frmExportReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(885, 695);
            this.Controls.Add(this.gbShowResult);
            this.Controls.Add(this.pnl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.c4FunGroupBox1);
            this.Controls.Add(this.pnlMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmExportReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmExportReport";
            this.Load += new System.EventHandler(this.frmExportReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1.Panel)).EndInit();
            this.c4FunGroupBox1.Panel.ResumeLayout(false);
            this.c4FunGroupBox1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1)).EndInit();
            this.c4FunGroupBox1.ResumeLayout(false);
            this.GroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbF)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.pnl.ResumeLayout(false);
            this.pnl.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult.Panel)).EndInit();
            this.gbShowResult.Panel.ResumeLayout(false);
            this.gbShowResult.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult)).EndInit();
            this.gbShowResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHand)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunGroupBox c4FunGroupBox1;
        private System.Windows.Forms.Label labDateCreate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtRATD;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtLATD;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.GroupBox GroupBox5;
        private System.Windows.Forms.PictureBox pbF;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label labNote;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.Label labMobile;
        private System.Windows.Forms.Label labReportId;
        private System.Windows.Forms.Label labMa;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.Label labParent;
        private System.Windows.Forms.TextBox txtTenChaMe;
        private System.Windows.Forms.Label labBirthday;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.Label labSex;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Label lablNameForm;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Label labStep1;
        private System.Windows.Forms.Panel pnl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.TextBox txtDirSave;
        private System.Windows.Forms.ComboBox cbxLoaiBaoCao;
        private System.Windows.Forms.Button btnExport;
        private C4FunComponent.Toolkit.C4FunGroupBox gbShowResult;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtR1P;
        public System.Windows.Forms.TextBox txtR2P;
        public System.Windows.Forms.TextBox txtR3P;
        public System.Windows.Forms.TextBox txtR4P;
        public System.Windows.Forms.TextBox txtR5P;
        public System.Windows.Forms.TextBox txtL5P;
        public System.Windows.Forms.TextBox txtL4P;
        public System.Windows.Forms.TextBox txtL3P;
        public System.Windows.Forms.TextBox txtL2P;
        public System.Windows.Forms.TextBox txtL1P;
        public System.Windows.Forms.TextBox txtR1R;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtR1L;
        public System.Windows.Forms.TextBox txtR2R;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtR2L;
        public System.Windows.Forms.TextBox txtR3R;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtR3L;
        public System.Windows.Forms.TextBox txtR4R;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtR4L;
        public System.Windows.Forms.TextBox txtR5R;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtR5L;
        public System.Windows.Forms.TextBox txtL5R;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtL5L;
        public System.Windows.Forms.TextBox txtL4R;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtL4L;
        public System.Windows.Forms.TextBox txtL3R;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtL3L;
        public System.Windows.Forms.TextBox txtL2R;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.TextBox txtL2L;
        private C4FunComponent.Toolkit.C4FunLabel c4FunLabel2;
        private C4FunComponent.Toolkit.C4FunLabel c4FunLabel1;
        public System.Windows.Forms.TextBox txtL1R;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.TextBox txtL1L;
        private C4FunComponent.Toolkit.C4FunLabel lblShowLeft;
        private C4FunComponent.Toolkit.C4FunLabel lblShowRight;
        private System.Windows.Forms.PictureBox picHand;
        private System.Windows.Forms.ImageList ImageList1;
    }
}