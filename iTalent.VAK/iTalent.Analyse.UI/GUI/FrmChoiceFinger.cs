﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace iTalent.Analyse.UI.GUI
{
    public partial class FrmChoiceFinger : Form
    {
        public string TypeName { get; set; }
        public string TypeId { get; set; }
        public Bitmap FingerP { get; set; }

        public FrmChoiceFinger(Bitmap fingerImage, Bitmap typefinger, string typename, string typeid,string nameF)
        {
            InitializeComponent();
            FingerP = typefinger;
            TypeId = typeid;
            TypeName = typename;
            imgZoom.Image = fingerImage;
            imgP.Image = typefinger;
            lblNameF.Text = nameF;
            WindowState = FormWindowState.Maximized;
        }

        public void lvL_MC(string[] imgkey, string label)
        {
            string[] strArray = imgkey;
            TypeId = strArray[0];
            lblTypeId.Text = strArray[0];
            TypeName = label;
            lblNameP.Text = label;
           
            try
            {
                //string filename = Directory.GetCurrentDirectory() + @"\FingerType\" + TypeId + ".BMP";
                //imgP.Image = new Bitmap(filename);
                //FingerP = new Bitmap(filename);
                imgP.Image = ImageList1.Images[lv.SelectedItems[0].ImageKey.ToString()];
                FingerP = (Bitmap)imgP.Image;
            }
            catch
            {
                imgP.Image = null;
            }
        }

        private void lvFingerType_MouseClick(object sender, MouseEventArgs e)
        {
            lvL_MC(lv.SelectedItems[0].ImageKey.Split('.'), lv.SelectedItems[0].Text);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                FingerP = new Bitmap(imgP.Image);
                TypeName = lblNameP.Text;
                TypeId = lblTypeId.Text;
            }
            catch
            {
                
            }
            Close();
        }

        private void lvFingerType_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            lvL_MC(lv.SelectedItems[0].ImageKey.Split('.'), lv.SelectedItems[0].Text);
            FingerP = new Bitmap(imgP.Image);
            TypeName = lblNameP.Text;
            TypeId = lblTypeId.Text;
            Close();
        }


    }
}
