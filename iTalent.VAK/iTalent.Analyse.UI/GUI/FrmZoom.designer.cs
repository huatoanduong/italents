﻿namespace iTalent.Analyse.UI.GUI
{
    partial class FrmZoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmZoom));
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnXacNhan = new System.Windows.Forms.Button();
            this.lablNameForm = new System.Windows.Forms.Label();
            this.lblNameF = new System.Windows.Forms.Label();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.imgZoom = new System.Windows.Forms.PictureBox();
            this.txtRC = new System.Windows.Forms.TextBox();
            this.pnlMenu.SuspendLayout();
            this.groupBox42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgZoom)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.pnlMenu.Controls.Add(this.btnXacNhan);
            this.pnlMenu.Controls.Add(this.lablNameForm);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(590, 50);
            this.pnlMenu.TabIndex = 69;
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnXacNhan.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnXacNhan.FlatAppearance.BorderSize = 0;
            this.btnXacNhan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnXacNhan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnXacNhan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXacNhan.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXacNhan.ForeColor = System.Drawing.Color.White;
            this.btnXacNhan.Image = global::iTalent.Analyse.UI.Properties.Resources.Active;
            this.btnXacNhan.Location = new System.Drawing.Point(378, 0);
            this.btnXacNhan.Margin = new System.Windows.Forms.Padding(0);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(212, 50);
            this.btnXacNhan.TabIndex = 17;
            this.btnXacNhan.Text = "Xác Nhận";
            this.btnXacNhan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXacNhan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnXacNhan.UseVisualStyleBackColor = false;
            this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
            // 
            // lablNameForm
            // 
            this.lablNameForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lablNameForm.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablNameForm.ForeColor = System.Drawing.Color.White;
            this.lablNameForm.Location = new System.Drawing.Point(0, 0);
            this.lablNameForm.Name = "lablNameForm";
            this.lablNameForm.Size = new System.Drawing.Size(590, 50);
            this.lablNameForm.TabIndex = 10;
            this.lablNameForm.Text = ".: PHÓNG LỚN VÂN TAY";
            this.lablNameForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNameF
            // 
            this.lblNameF.BackColor = System.Drawing.Color.White;
            this.lblNameF.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNameF.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.lblNameF.Location = new System.Drawing.Point(0, 50);
            this.lblNameF.Name = "lblNameF";
            this.lblNameF.Size = new System.Drawing.Size(590, 44);
            this.lblNameF.TabIndex = 147;
            this.lblNameF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.label18);
            this.groupBox42.Controls.Add(this.imgZoom);
            this.groupBox42.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.groupBox42.Location = new System.Drawing.Point(12, 110);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(570, 518);
            this.groupBox42.TabIndex = 148;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Vân Tay Khách Hàng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(356, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 21);
            this.label18.TabIndex = 166;
            this.label18.Text = "Chỉ số vân tay :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // imgZoom
            // 
            this.imgZoom.BackColor = System.Drawing.Color.White;
            this.imgZoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgZoom.ErrorImage = null;
            this.imgZoom.InitialImage = null;
            this.imgZoom.Location = new System.Drawing.Point(3, 25);
            this.imgZoom.Name = "imgZoom";
            this.imgZoom.Size = new System.Drawing.Size(564, 490);
            this.imgZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgZoom.TabIndex = 5;
            this.imgZoom.TabStop = false;
            // 
            // txtRC
            // 
            this.txtRC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtRC.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtRC.Location = new System.Drawing.Point(499, 100);
            this.txtRC.MaxLength = 5;
            this.txtRC.Name = "txtRC";
            this.txtRC.Size = new System.Drawing.Size(76, 33);
            this.txtRC.TabIndex = 165;
            this.txtRC.Text = "0";
            this.txtRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // FrmZoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(590, 640);
            this.ControlBox = false;
            this.Controls.Add(this.txtRC);
            this.Controls.Add(this.groupBox42);
            this.Controls.Add(this.lblNameF);
            this.Controls.Add(this.pnlMenu);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmZoom";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnlMenu.ResumeLayout(false);
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgZoom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Label lablNameForm;
        private System.Windows.Forms.Label lblNameF;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.PictureBox imgZoom;
        private System.Windows.Forms.Button btnXacNhan;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRC;
    }
}