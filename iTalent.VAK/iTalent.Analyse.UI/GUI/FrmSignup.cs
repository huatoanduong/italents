﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;


using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.UI.GUI
{
    public partial class FrmSignup : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        //private string _seckey;
        //private string _serial;
        //private string _productkey;

        public FrmSignup()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1,labAddress,labCellPhone,labCity,labEmail,labID,labName,labPhone,labPassword,btnLuu};
            _baseForm = new BaseForm();
            
            //MessageBox.Show(_baseForm.ProductKey);

        }
        //private bool CheckDevice()
        //{
        //    //if (!RuntimePolicyHelper.LegacyV2RuntimeEnabledSuccessfully) return false;
        //    Fingerprint.VietNamLanguage = 
        //    using (var fingerprint = new Fingerprint(false))
        //    {
        //        bool kq = fingerprint.CheckRegisted;
        //        _seckey = fingerprint.Seckey;
        //        _serial = fingerprint.Serial;
        //        _productkey = fingerprint.ProductKey;
        //        if (!fingerprint.Isconnect)
        //            MessageBox.Show(fingerprint.Message);

        //        return fingerprint.Isconnect;
        //    }
        //}
        private void SetLisence()
        {
            try
            {
                const int point = 100;
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIPointModelService service = new FIPointModelService(unitOfWork);

                    int sl = service.GetNumberPointLeft(ICurrentSessionService.CurAgency.ID);
                    if (sl > 0)
                    {
                        return;
                    }

                    PointModel model = new PointModel
                    {
                        AgentId = ICurrentSessionService.CurAgency.ID,
                        PointOriginal = point
                    };

                    int numkey = service.GetNumberPointLeft(ICurrentSessionService.CurAgency.ID);
                    if (numkey <= 0)
                    {
                        bool res = service.GenerateNewKey(ref model);

                        if (!res)
                        {
                            _baseForm.ShowMessage(IconMessageBox.Warning,service.ErrMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }
        private void Register()
        {
            try
            {
                
                if (string.IsNullOrWhiteSpace(txtTen.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập tên!";
                    _baseForm.EnglishMsg = @"Please input your name!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                if (string.IsNullOrWhiteSpace(txtEmail.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập email!";
                    _baseForm.EnglishMsg = @"Please input your email!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    FIAgency objAgency = service.CreateEntity();
                    objAgency.Name = txtTen.Text;
                    objAgency.Username = "AC50015";
                    objAgency.Password = "AC50015";
                    objAgency.Email = txtEmail.Text;
                    objAgency.PhoneNo = txtDienThoai.Text;
                    objAgency.MobileNo = txtDiDong.Text;
                    objAgency.City = txtThanhPho.Text;

                    objAgency.Address1 = txtDiaChi.Text;
                    objAgency.SaveImages = ICurrentSessionService.DirSaveImages;
                    _baseForm.Seckey = txtKeyXacNhan.Text;
                    objAgency.SecKey = _baseForm.Seckey;
                    _baseForm.ProductKey = txtKeyDangKy.Text;
                    objAgency.ProductKey = _baseForm.ProductKey;
                    _baseForm.SerailKey = _baseForm.Seckey + _baseForm.ProductKey;
                    objAgency.SerailKey = _baseForm.SerailKey;

                    Issuccess = service.Add(objAgency);
                    if (Issuccess)
                    {
                        Issuccess = service.Login("AC50015", "AC50015");
                        if (!Issuccess)
                        {
                            _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                        }
                        else
                        {
                            SetLisence();
                            _baseForm.VietNamMsg = ".: Đăng ký thành công!\nBạn hãy chạy lại chương trình.";
                            _baseForm.EnglishMsg = ".: Register successfully!\nPlease open again.";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                        }
                    }
                    else
                    {
                        Issuccess = false;
                        _baseForm.ShowMessage(IconMessageBox.Warning, service.ErrMsg);
                    }
                    Close();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                Environment.Exit(0);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Dispose();
        }

        private void buttonSpecHeaderGroup1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            Register();
        }

        private void btnCheckKey_Click(object sender, EventArgs e)
        {
            btnLuu.Enabled = CheckKey();
        }

        private bool CheckKey()
        {
            try
            {
                if (txtKeyXacNhan.Text == "")
                {
                    _baseForm.VietNamMsg = "Vui lòng nhập key xác nhận!";
                    _baseForm.EnglishMsg = "Please input key verification!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return false;
                }

                if (txtKeyXacNhan.Text.Length != 7)
                {
                    _baseForm.VietNamMsg = "Key không hợp lệ!";
                    _baseForm.EnglishMsg = "Key is not valid!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return false;
                }

                bool check = GeneratePos.CheckKey(txtKeyXacNhan.Text);
                if (!check)
                {
                    _baseForm.VietNamMsg = "Key không hợp lệ!";
                    _baseForm.EnglishMsg = "Key is not valid!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _baseForm.VietNamMsg = "Key không hợp lệ!";
                _baseForm.EnglishMsg = "Key is not valid!";
                _baseForm.ShowMessage(IconMessageBox.Information);
                return false;
            }
            return true;
        }

        private void FrmSignup_Load(object sender, EventArgs e)
        {
            btnLuu.Enabled = false;
            //TODO: Enable again
            //_baseForm.CheckDevice(true);
            txtKeyDangKy.Text = GeneratePos.KeyGenerate();
            txtTen.Select();
        }
    }
}
