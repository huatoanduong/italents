﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Service;
using Microsoft.VisualBasic.CompilerServices;

namespace iTalent.Analyse.UI.GUI
{
    public partial class FrmWebCam : Form
    {
        private readonly string _reportId;
        private readonly string _tempDirName;
        public int CountSpace;
        private string _fp;
        private Bitmap _oBitmapF;
        private Bitmap _oBitmapL;
        private Bitmap _oBitmapR;
        private FilterInfoCollection _videoDevices;
        private VideoCaptureDevice _videoSource;
        private readonly BaseForm _baseForm;
        public FrmWebCam()
        {
            InitializeComponent();
        }

        public FrmWebCam(string reportid, string tempDirName)
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { lablNameForm, btnFace};
            _baseForm = new BaseForm();
            _reportId = reportid;
            _tempDirName = tempDirName;
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            CloseVideoSource();
            Close();
        }

        private void FrmWebCam_Load(object sender, EventArgs e)
        {
            picOutput.SizeMode = PictureBoxSizeMode.StretchImage;
            LoadCamera();
            _fp = _tempDirName + @"\" + _reportId;
            loop_picture();
            btnFace.Focus();
        }

        private void LoadCamera()
        {
            try
            {
                _videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (_videoDevices.Count > 0)
                {
                    _videoSource = new VideoCaptureDevice(_videoDevices[0].MonikerString)
                    {
                        DesiredFrameSize = new Size(160, 120)
                    };
                    _videoSource.NewFrame += video_NewFrame;
                    CloseVideoSource();
                    _videoSource.Start();
                }
                else
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage
                        ? @"Không thể khởi tạo webcam.Vui lòng tắt các chương trình đang sử dụng webcam."
                        : "Camera Is Not Running!");
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //eventhandler if new frame is ready
        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap img = (Bitmap) eventArgs.Frame.Clone();
            //do processing here
            picOutput.Image = img;
        }

        //close the device safely
        private void CloseVideoSource()
        {
            if (_videoSource != null)
                if (_videoSource.IsRunning)
                {
                    _videoSource.SignalToStop();
                    _videoSource = null;
                }
        }

        public void loop_picture()
        {
            if (File.Exists(_fp + "F.BMP"))
            {
                _oBitmapF = new Bitmap(_fp + "F.BMP");
                picFace.Image = _oBitmapF;
            }
            if (File.Exists(_fp + "L.BMP"))
            {
                _oBitmapL = new Bitmap(_fp + "L.BMP");
                picLHand.Image = _oBitmapL;
            }
            if (File.Exists(_fp + "R.BMP"))
            {
                _oBitmapR = new Bitmap(_fp + "R.BMP");
                picRHand.Image = _oBitmapR;
            }
        }

        private void btnFace_Click(object sender, EventArgs e)
        {
            try
            {
                if (_videoSource != null && _videoSource.IsRunning)
                {
                    if (_oBitmapF != null)
                    {
                        _oBitmapF.Dispose();
                    }
                    picFace.Image = picOutput.Image;
                    try
                    {
                        //MyBitmapFile myFile = new MyBitmapFile(320, 480, m_Frame);
                        //FileStream file = new FileStream(filename, FileMode.Create);
                        //file.Write(myFile.BitmatFileData, 0, myFile.BitmatFileData.Length);
                        //file.Close();

                        picFace.Image.Save(_fp + "F.BMP");
                        btnLHand.Focus();
                    }
                    catch (Exception exception1)
                    {
                        ProjectData.SetProjectError(exception1);
                        //Interaction.MsgBox(exception.Message, MsgBoxStyle.ApplicationModal, null);
                        ProjectData.ClearProjectError();
                    }
                }
                else
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage
                        ? "Không thể khởi tạo webcam."
                        : @"Camera Is Not Running!");
                }
            }
            catch
            {
                //
            }
        }

        private void btnLHand_Click(object sender, EventArgs e)
        {
            try
            {
                if (_oBitmapL != null)
                {
                    _oBitmapL.Dispose();
                }
                if (_videoSource.IsRunning)
                {
                    picLHand.Image = picOutput.Image;
                    //this.picLHand.Image = this.myCam.copyFrame(this.picOutput, rect);
                    picLHand.Image.Save(_fp + "L.BMP");
                    btnRHand.Focus();
                }
                else
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage
                        ? "Không thể khởi tạo webcam."
                        : @"Camera Is Not Running!");
                }
            }
            catch
            {
                // ignored
            }
        }

        private void btnRHand_Click(object sender, EventArgs e)
        {
            try
            {
                if (_oBitmapR != null)
                {
                    _oBitmapR.Dispose();
                }
                if (_videoSource.IsRunning)
                {
                    picRHand.Image = picOutput.Image;
                    picRHand.Image.Save(_fp + "R.BMP");
                }
                else
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage
                        ? "Không thể khởi tạo webcam."
                        : @"Camera Is Not Running!");
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.F1:
                    btnFace.PerformClick();
                    break;

                case (Keys.F3):
                    btnRHand.PerformClick();
                    break;

                case (Keys.F2):
                    btnLHand.PerformClick();
                    break;

                case Keys.F4:
                   Close();
                    break;

                case Keys.Escape:
                    Close();
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}