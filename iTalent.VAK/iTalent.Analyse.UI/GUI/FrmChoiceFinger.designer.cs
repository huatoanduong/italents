﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace iTalent.Analyse.UI.GUI
{
    partial class FrmChoiceFinger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // FrmChoiceFinger
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        //    this.BackColor = System.Drawing.Color.White;
        //    this.ClientSize = new System.Drawing.Size(821, 411);
        //    this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //    this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
        //    this.Name = "FrmChoiceFinger";
        //    this.ResumeLayout(false);

        //}

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "WT"}, "WT.bmp", System.Drawing.Color.White, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("WS", "WS.bmp");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("WX", "WX.bmp");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("WE", "WE.bmp");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("WC", "WC.bmp");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("WD", "WD.bmp");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("WI", "WI.bmp");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("UL", "UL.bmp");
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("LF", "LF.bmp");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("RL", "RL.bmp");
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("AS", "AS.bmp");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("AU", "AU.bmp");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("AT", "AT.bmp");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("AE", "AE.bmp");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("AR", "AR.bmp");
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("WP", "WP.bmp");
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem("WL", "WL.bmp");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChoiceFinger));
            this.imgZoom = new System.Windows.Forms.PictureBox();
            this.imgP = new System.Windows.Forms.PictureBox();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.lablNameForm = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNameF = new System.Windows.Forms.Label();
            this.lblTypeId = new System.Windows.Forms.Label();
            this.lblNameP = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.lv = new System.Windows.Forms.ListView();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgZoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgP)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.SuspendLayout();
            // 
            // imgZoom
            // 
            this.imgZoom.BackColor = System.Drawing.Color.White;
            this.imgZoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgZoom.ErrorImage = null;
            this.imgZoom.InitialImage = null;
            this.imgZoom.Location = new System.Drawing.Point(3, 25);
            this.imgZoom.Name = "imgZoom";
            this.imgZoom.Size = new System.Drawing.Size(279, 318);
            this.imgZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgZoom.TabIndex = 5;
            this.imgZoom.TabStop = false;
            // 
            // imgP
            // 
            this.imgP.BackColor = System.Drawing.Color.White;
            this.imgP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgP.ErrorImage = null;
            this.imgP.InitialImage = null;
            this.imgP.Location = new System.Drawing.Point(3, 25);
            this.imgP.Name = "imgP";
            this.imgP.Size = new System.Drawing.Size(279, 318);
            this.imgP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgP.TabIndex = 7;
            this.imgP.TabStop = false;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.pnlMenu.Controls.Add(this.lablNameForm);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(1018, 50);
            this.pnlMenu.TabIndex = 68;
            // 
            // lablNameForm
            // 
            this.lablNameForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lablNameForm.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablNameForm.ForeColor = System.Drawing.Color.White;
            this.lablNameForm.Location = new System.Drawing.Point(0, 0);
            this.lablNameForm.Name = "lablNameForm";
            this.lablNameForm.Size = new System.Drawing.Size(1018, 50);
            this.lablNameForm.TabIndex = 10;
            this.lablNameForm.Text = ".: CHỌN VÂN TAY MẪU";
            this.lablNameForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblNameF);
            this.panel1.Controls.Add(this.lblTypeId);
            this.panel1.Controls.Add(this.lblNameP);
            this.panel1.Controls.Add(this.btnOK);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox42);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(583, 672);
            this.panel1.TabIndex = 70;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 424);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(553, 21);
            this.label2.TabIndex = 145;
            this.label2.Text = ".:Chú ý : Có thể nhấp dúp chuột vào hình mẫu vân tay để chọn hình mẫu đó.";
            // 
            // lblNameF
            // 
            this.lblNameF.BackColor = System.Drawing.Color.White;
            this.lblNameF.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.lblNameF.Location = new System.Drawing.Point(4, 3);
            this.lblNameF.Name = "lblNameF";
            this.lblNameF.Size = new System.Drawing.Size(286, 63);
            this.lblNameF.TabIndex = 144;
            this.lblNameF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTypeId
            // 
            this.lblTypeId.BackColor = System.Drawing.Color.White;
            this.lblTypeId.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTypeId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.lblTypeId.Location = new System.Drawing.Point(657, 533);
            this.lblTypeId.Name = "lblTypeId";
            this.lblTypeId.Size = new System.Drawing.Size(53, 36);
            this.lblTypeId.TabIndex = 143;
            this.lblTypeId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTypeId.Visible = false;
            // 
            // lblNameP
            // 
            this.lblNameP.BackColor = System.Drawing.Color.White;
            this.lblNameP.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.lblNameP.Location = new System.Drawing.Point(293, 3);
            this.lblNameP.Name = "lblNameP";
            this.lblNameP.Size = new System.Drawing.Size(285, 63);
            this.lblNameP.TabIndex = 142;
            this.lblNameP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.Image = global::iTalent.Analyse.UI.Properties.Resources.Active;
            this.btnOK.Location = new System.Drawing.Point(360, 463);
            this.btnOK.Margin = new System.Windows.Forms.Padding(0);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(215, 46);
            this.btnOK.TabIndex = 141;
            this.btnOK.Text = "Xác Nhận";
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.imgP);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.groupBox1.Location = new System.Drawing.Point(293, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 346);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vân Tay Mẫu";
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.imgZoom);
            this.groupBox42.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.groupBox42.Location = new System.Drawing.Point(5, 69);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(285, 346);
            this.groupBox42.TabIndex = 71;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Vân Tay Khách Hàng";
            // 
            // lv
            // 
            this.lv.BackColor = System.Drawing.Color.Navy;
            this.lv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv.ForeColor = System.Drawing.Color.White;
            this.lv.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17});
            this.lv.LargeImageList = this.ImageList1;
            this.lv.Location = new System.Drawing.Point(583, 50);
            this.lv.Margin = new System.Windows.Forms.Padding(0);
            this.lv.MultiSelect = false;
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(435, 672);
            this.lv.TabIndex = 239;
            this.lv.TileSize = new System.Drawing.Size(184, 34);
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvFingerType_MouseClick);
            this.lv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvFingerType_MouseDoubleClick);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "WT.bmp");
            this.ImageList1.Images.SetKeyName(1, "WS.bmp");
            this.ImageList1.Images.SetKeyName(2, "WX.bmp");
            this.ImageList1.Images.SetKeyName(3, "WE.bmp");
            this.ImageList1.Images.SetKeyName(4, "WC.bmp");
            this.ImageList1.Images.SetKeyName(5, "WD.bmp");
            this.ImageList1.Images.SetKeyName(6, "WI.bmp");
            this.ImageList1.Images.SetKeyName(7, "UL.bmp");
            this.ImageList1.Images.SetKeyName(8, "LF.bmp");
            this.ImageList1.Images.SetKeyName(9, "RL.bmp");
            this.ImageList1.Images.SetKeyName(10, "AS.bmp");
            this.ImageList1.Images.SetKeyName(11, "AT.bmp");
            this.ImageList1.Images.SetKeyName(12, "AU.bmp");
            this.ImageList1.Images.SetKeyName(13, "AE.bmp");
            this.ImageList1.Images.SetKeyName(14, "AR.bmp");
            this.ImageList1.Images.SetKeyName(15, "WP.bmp");
            this.ImageList1.Images.SetKeyName(16, "WL.bmp");
            // 
            // FrmChoiceFinger
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1018, 722);
            this.ControlBox = false;
            this.Controls.Add(this.lv);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlMenu);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "FrmChoiceFinger";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.imgZoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgP)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox42.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private PictureBox imgP;
        private PictureBox imgZoom;
        #endregion
        private Panel pnlMenu;
        private Label lablNameForm;
        private Panel panel1;
        private GroupBox groupBox1;
        private GroupBox groupBox42;
        private Button btnOK;
        private Label lblNameP;
        private Label lblTypeId;
        private Label lblNameF;
        private Label label2;
        private ListView lv;
        private ImageList ImageList1;
    }
}