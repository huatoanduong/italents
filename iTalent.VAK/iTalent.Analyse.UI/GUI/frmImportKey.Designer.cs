﻿namespace iTalent.Analyse.UI.GUI
{
    partial class frmImportKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportKey));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnExit = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.labKey = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.labNumber = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.btnExit});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnSave);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtKey);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labKey);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtNumber);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labNumber);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(701, 208);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 152;
            this.c4FunHeaderGroup1.Tag = "";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Input Key Report";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.Analyse.UI.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // btnExit
            // 
            this.btnExit.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnExit.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::iTalent.Analyse.UI.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(488, 117);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(172, 46);
            this.btnSave.TabIndex = 186;
            this.btnSave.Tag = "Tiếp Tục";
            this.btnSave.Text = "Save";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtKey
            // 
            this.txtKey.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtKey.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtKey.Location = new System.Drawing.Point(137, 15);
            this.txtKey.Multiline = true;
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(523, 96);
            this.txtKey.TabIndex = 192;
            this.txtKey.UseSystemPasswordChar = true;
            // 
            // labKey
            // 
            this.labKey.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labKey.BackColor = System.Drawing.Color.Transparent;
            this.labKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labKey.Location = new System.Drawing.Point(6, 18);
            this.labKey.Name = "labKey";
            this.labKey.Size = new System.Drawing.Size(125, 21);
            this.labKey.TabIndex = 199;
            this.labKey.Tag = "";
            this.labKey.Text = "Input Key:";
            this.labKey.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNumber
            // 
            this.txtNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtNumber.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtNumber.Location = new System.Drawing.Point(137, 117);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.ReadOnly = true;
            this.txtNumber.Size = new System.Drawing.Size(163, 46);
            this.txtNumber.TabIndex = 187;
            // 
            // labNumber
            // 
            this.labNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labNumber.BackColor = System.Drawing.Color.Transparent;
            this.labNumber.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNumber.Location = new System.Drawing.Point(3, 130);
            this.labNumber.Name = "labNumber";
            this.labNumber.Size = new System.Drawing.Size(129, 21);
            this.labNumber.TabIndex = 194;
            this.labNumber.Tag = "Tên Đăng Nhập :";
            this.labNumber.Text = "Munber :";
            this.labNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmImportKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 208);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmImportKey";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmImportKey_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label labKey;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Label labNumber;
    }
}