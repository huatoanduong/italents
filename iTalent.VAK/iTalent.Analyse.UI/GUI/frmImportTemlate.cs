﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;

namespace iTalent.Analyse.UI.GUI
{
    public partial class frmImportTemlate : Form
    {
        string[] EnglishReport = new string[]
       {
            "Adult",
            "Children"
       };
        string[] VietNameseReport = new string[]
        {
            "Người Lớn",
            "Trẻ Em"
        };
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;

        public frmImportTemlate()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { c4FunHeaderGroup1, labNumber, labKey, btnSave };
            _baseForm = new BaseForm();

            Issuccess = false;

            cbxLoaiBaoCao.Items.AddRange(ICurrentSessionService.VietNamLanguage ? VietNameseReport : EnglishReport);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Multiselect = false,
                FilterIndex = 0,
                Title = ICurrentSessionService.VietNamLanguage?@"Chọn Báo Cáo Mẫu": "Choose Report Template ",
                Filter = ICurrentSessionService.VietNamLanguage ? @"Báo Cáo Mẫu (*.rptx)|*.*":"rptx file (*.rptx)|*.*",
                FileName = "*.rptx"
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtKey.Text = dialog.FileName;
                btnSave.Enabled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtKey.Text == "")
            {
                _baseForm.VietNamMsg = "Chọn mẫu báo cáo cần nhập!";
                _baseForm.EnglishMsg = "Choose report template  to import!";
                _baseForm.ShowMessage(IconMessageBox.Information);
                return;
            }

            if (cbxLoaiBaoCao.Text == "")
            {
                _baseForm.VietNamMsg = "Chọn loại mẫu báo cáo!";
                _baseForm.EnglishMsg = "Choose type template!";
                _baseForm.ShowMessage(IconMessageBox.Information);
                return;
            }

            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFITemplateService service = new FITemplateService(unitOfWork);
                bool res = service.ImportRptx(txtKey.Text, cbxLoaiBaoCao.SelectedIndex == 0 ? 1 : 2);
                if (!res)
                {
                    //_baseForm.VietNamMsg = "Nhập mẫu báo cáo thất bại!";
                    //_baseForm.EnglishMsg = "Import template failed!";
                    _baseForm.ShowMessage(IconMessageBox.Warning, service.ErrMsg);
                }
                else
                {
                    _baseForm.VietNamMsg = "Nhập mẫu báo cáo thành công!";
                    _baseForm.EnglishMsg = "Import template successful!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                }
            }
        }
    }
}
