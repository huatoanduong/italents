﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Analyse.Commons;
using iTalent.Utils;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualBasic;
using iTalent.Analyse.Model;
using C4FunComponent.Toolkit;
using iTalent.Analyse.UI.FlashForm;
using VAKFomula.Entity;

namespace iTalent.Analyse.UI.GUI
{
    public partial class frmExportReport : Form
    {
        string[] EnglishReport = new string[]
        {
            "Adult",
            "Children"
        };
        string[] VietNameseReport = new string[]
        {
            "Người Lớn",
            "Trẻ Em"
        };
        public FPIndex index = null;
        private List<FIFingerRecord> lstFingerIndex = null;
        private FICustomer currCustomer = null;
        private string _dirFile;
        public int CustomerId;

        public frmExportReport(Int32 customerId)
        {
            InitializeComponent();
            CustomerId = customerId;
        }

        private string SetFullNameReport(string dirsave)
        {
            return Path.Combine(dirsave, SetNameReport());
        }

        private string SetNameReport()
        {
            string typereport = "";
            if (cbxLoaiBaoCao.Text == @"Người Lớn" || cbxLoaiBaoCao.Text == @"Adult")
                typereport = ICurrentSessionService.VietNamLanguage ? "_NguoiLon" : "_Adult";
            if(cbxLoaiBaoCao.Text == @"Trẻ Em" || cbxLoaiBaoCao.Text == @"Children")
                typereport = ICurrentSessionService.VietNamLanguage ? "_TreEm" : "_Children";

            //return "ITALENT" + currCustomer.ReportID.Substring(7) + ConvertUtil.ConverToUnsign(currCustomer.Name).Replace(" ", "").ToUpper() + ".pdf";
            return "ITALENT" + DateTime.Now.ToString("ddMMyyyyHHmmss") +
                   ConvertUtil.ConverToUnsign(currCustomer.Name).Replace(" ", "").ToUpper() + typereport + ".pdf";
        }

        void GetCustomer(Int32 CustomerId)
        {
            ///////////Lấy dữ liệu customer vào và hình ảnh của nó.//////////
            GetCustomerData(CustomerId);
            GetCustomerFPImage();
            if (currCustomer == null)
            {
                MessageBox.Show(ICurrentSessionService.VietNamLanguage ? "Khách hàng này không có thông tin trong hệ thống!" : "This customer hasn't exist in system.");
                Close();
            }
            /////////////////////////////////////////////////////////////////

            if (currCustomer != null)
            {
                SetCustomerData();
                SetCustomerFPIndex();
            }
        }
        void GetCustomerData(Int32 CustomerId)
        {
            FICustomer localObject = null;
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFICustomerService service = new FICustomerService(unitOfWork);
                localObject = service.Find(CustomerId);
            }
            //After select localobject Load it to UI.
            currCustomer = localObject;
            if (currCustomer == null)
            {
                MessageBox.Show(ICurrentSessionService.VietNamLanguage ? @"Vui lòng chọn khách hàng trước khi phân tích" : "Please choose the customer from the list", @"Thông Báo", MessageBoxButtons.OK,
                   MessageBoxIcon.Information);
                Close();
            }
        }
        void GetCustomerFPImage()
        {
            GetPathContainFPImage();
            GetFingerprintImages();
        }
        void GetPathContainFPImage()
        {
            if (currCustomer != null)
            {
                var nameCustomer = ConvertUtil.ConverToUnsign(currCustomer.Name).Replace(" ", "").ToUpper();
                var folderCustomer = Path.Combine(ICurrentSessionService.DesDirNameSource, currCustomer.ReportID.Substring(7) + "_" + nameCustomer);
                if (!Directory.Exists(folderCustomer))
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage ? @"Khách hàng này phải được lấy vân tay trước khi đưa vào phân tích" : "Fingerprint files doesn't exist", @"Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                    //this.DialogResult = DialogResult.Cancel;
                    //Close();
                    //GC.Collect();
                    //GC.SuppressFinalize(this);
                    //Dispose();
                }
                else
                {
                    _dirFile = folderCustomer;
                }
            }
        }
        void GetFingerprintImages()
        {
            index = new FPIndex();
            try
            {
                //Webcam
                var dirImage = GetImageFPFace(_dirFile, currCustomer.ReportID.Substring(7) + "F");
                if (File.Exists(dirImage))
                {
                    index.ImageFace = Image.FromFile(dirImage);
                    pbF.Image = index.ImageFace;
                }

                //dirImage = GetImageFP(_dirFile, "L");
                //if (File.Exists(dirImage)) indexImageLeftHand = Image.FromFile(dirImage);

                //dirImage = GetImageFP(_dirFile, "R");
                //if (File.Exists(dirImage)) indexImageRightHand = Image.FromFile(dirImage);

                //Left hand RC
                dirImage = GetImageFP(_dirFile, "1_1");
                if (File.Exists(dirImage)) index.L1FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "1_2");
                if (File.Exists(dirImage)) index.L1LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "1_3");
                if (File.Exists(dirImage)) index.L1RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "2_1");
                if (File.Exists(dirImage)) index.L2FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "2_2");
                if (File.Exists(dirImage)) index.L2LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "2_3");
                if (File.Exists(dirImage)) index.L2RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "3_1");
                if (File.Exists(dirImage)) index.L3FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "3_2");
                if (File.Exists(dirImage)) index.L3LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "3_3");
                if (File.Exists(dirImage)) index.L3RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "4_1");
                if (File.Exists(dirImage)) index.L4FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "4_2");
                if (File.Exists(dirImage)) index.L4LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "4_3");
                if (File.Exists(dirImage)) index.L4RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "5_1");
                if (File.Exists(dirImage)) index.L5FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "5_2");
                if (File.Exists(dirImage)) index.L5LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "5_3");
                if (File.Exists(dirImage)) index.L5RImage = Image.FromFile(dirImage);

                //Right hand RC
                dirImage = GetImageFP(_dirFile, "6_1");
                if (File.Exists(dirImage)) index.R1FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "6_2");
                if (File.Exists(dirImage)) index.R1LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "6_3");
                if (File.Exists(dirImage)) index.R1RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "7_1");
                if (File.Exists(dirImage)) index.R2FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "7_2");
                if (File.Exists(dirImage)) index.R2LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "7_3");
                if (File.Exists(dirImage)) index.R2RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "8_1");
                if (File.Exists(dirImage)) index.R3FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "8_2");
                if (File.Exists(dirImage)) index.R3LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "8_3");
                if (File.Exists(dirImage)) index.R3RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "9_1");
                if (File.Exists(dirImage)) index.R4FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "9_2");
                if (File.Exists(dirImage)) index.R4LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "9_3");
                if (File.Exists(dirImage)) index.R4RImage = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "10_1");
                if (File.Exists(dirImage)) index.R5FImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "10_2");
                if (File.Exists(dirImage)) index.R5LImage = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "10_3");
                if (File.Exists(dirImage)) index.R5RImage = Image.FromFile(dirImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
        }
        string GetImageFP(string fileSave, string type)
        {
            return Path.Combine(fileSave, type + ".b");
        }
        string GetImageFPFace(string fileSave, string type)
        {
            return Path.Combine(fileSave, type + ".bmp");
        }
        void SetCustomerData()
        {
            if (currCustomer != null)
            {
                labMa.Text = currCustomer.ID.ToString();
                labReportId.Text = currCustomer.ReportID.ToString();
                labDateCreate.Text = DateTimeUtil.ConvertDate(currCustomer.Date.ToString()).ToString("dd/MM/yyyy");

                txtTen.Text = currCustomer.Name;
                txtTenChaMe.Text = currCustomer.Parent;
                if (currCustomer.Gender == "Nam" || currCustomer.Gender == "Male")
                {
                    rdMale.Checked = true;
                    rdFemale.Checked = false;
                }
                if (currCustomer.Gender == "Nữ" || currCustomer.Gender == "Female")
                {
                    rdMale.Checked = false;
                    rdFemale.Checked = true;
                }
                dtpNgaySinh.Value = DateTimeUtil.ConvertDate(currCustomer.DOB.ToString());
                txtEmail.Text = currCustomer.Email;
                txtDiaChi.Text = currCustomer.Address1;
                txtGhiChu.Text = currCustomer.Remark;
                txtDienThoai.Text = currCustomer.Tel;
                txtThanhPho.Text = currCustomer.Country;
                txtDiDong.Text = currCustomer.Mobile;
            }
        }
        void SetCustomerFPIndex()
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFingerRecordService service = new FIFingerRecordService(unitOfWork);
                var lst = service.FindByRecordId(currCustomer.ID);
                lstFingerIndex = lst;
                //if (lst == null) { MessageBox.Show(ICurrentSessionService.VietNamLanguage ? "Vui lòng phân tích dấu vân tay khách hàng này trước khi xuất báo cáo." : "Please Analyse fingerprint of this customer before choose EXPORT REPORT."); Close(); }
                if (lst == null || lst.Count == 0)
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage
                        ? "Vui lòng phân tích dấu vân tay khách hàng này trước khi xuất báo cáo."
                        : "Please Analyse fingerprint of this customer before choose EXPORT REPORT.");
                    //Close();
                    DialogResult = DialogResult.Cancel;
                    Close();
                }
                else
                {
                    SetCustomerFPIndexToUI(lst);
                }
            }
        }
        void SetCustomerFPIndexToUI(List<FIFingerRecord> lst)
        {
            //index = new FPIndex();
            var obj = lst.FirstOrDefault(s => s.Type == "LATD");
            if (obj != null)
            {
                txtLATD.Text = obj.ATDPoint.ToString();
                index.LATDPoint = obj.ATDPoint;
            }

            obj = lst.FirstOrDefault(s => s.Type == "RATD");
            if (obj != null)
            {
                txtRATD.Text = obj.ATDPoint.ToString();
                index.RATDPoint = obj.ATDPoint;
            }
            #region Left
            //L1
            obj = lst.FirstOrDefault(s => s.Type == "L1L");
            if (obj != null)
            {
                txtL1L.Text = obj.RCCount.ToString();
                txtL1P.Text = obj.FingerType.ToUpper();
                index.L1LPoint = obj.RCCount;
                index.L1P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L1R");
            if (obj != null)
            {
                txtL1R.Text = obj.RCCount.ToString();
                txtL1P.Text = obj.FingerType.ToUpper();
                index.L1RPoint = obj.RCCount;
                index.L1P = obj.FingerType.ToUpper();
            }

            //L2
            obj = lst.FirstOrDefault(s => s.Type == "L2L");
            if (obj != null)
            {
                txtL2L.Text = obj.RCCount.ToString();
                txtL2P.Text = obj.FingerType.ToUpper();
                index.L2LPoint = obj.RCCount;
                index.L2P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L2R");
            if (obj != null)
            {
                txtL2R.Text = obj.RCCount.ToString();
                txtL2P.Text = obj.FingerType.ToUpper();
                index.L2RPoint = obj.RCCount;
                index.L2P = obj.FingerType.ToUpper();
            }

            //L3
            obj = lst.FirstOrDefault(s => s.Type == "L3L");
            if (obj != null)
            {
                txtL3L.Text = obj.RCCount.ToString();
                txtL3P.Text = obj.FingerType.ToUpper();
                index.L3LPoint = obj.RCCount;
                index.L3P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L3R");
            if (obj != null)
            {
                txtL3R.Text = obj.RCCount.ToString();
                txtL3P.Text = obj.FingerType.ToUpper();
                index.L3RPoint = obj.RCCount;
                index.L3P = obj.FingerType.ToUpper();
            }

            //L4
            obj = lst.FirstOrDefault(s => s.Type == "L4L");
            if (obj != null)
            {
                txtL4L.Text = obj.RCCount.ToString();
                txtL4P.Text = obj.FingerType.ToUpper();
                index.L4LPoint = obj.RCCount;
                index.L4P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L4R");
            if (obj != null)
            {                
                txtL4R.Text = obj.RCCount.ToString();
                txtL4P.Text = obj.FingerType.ToUpper();
                index.L4RPoint = obj.RCCount;
                index.L4P = obj.FingerType.ToUpper();
            }

            //L5
            obj = lst.FirstOrDefault(s => s.Type == "L5L");
            if (obj != null)
            {
                txtL5L.Text = obj.RCCount.ToString();
                txtL5P.Text = obj.FingerType.ToUpper();
                index.L5LPoint = obj.RCCount;
                index.L5P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L5R");
            if (obj != null)
            {
                txtL5R.Text = obj.RCCount.ToString();
                txtL5P.Text = obj.FingerType.ToUpper();
                index.L5RPoint = obj.RCCount;
                index.L5P = obj.FingerType.ToUpper();
            }
            #endregion Left

            #region Right
            //R1
            obj = lst.FirstOrDefault(s => s.Type == "R1L");
            if (obj != null)
            {
                txtR1L.Text = obj.RCCount.ToString();
                txtR1P.Text = obj.FingerType.ToUpper();
                index.R1LPoint = obj.RCCount;
                index.R1P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R1R");
            if (obj != null)
            {
                txtR1R.Text = obj.RCCount.ToString();
                txtR1P.Text = obj.FingerType.ToUpper();
                index.R1RPoint = obj.RCCount;
                index.R1P = obj.FingerType.ToUpper();
            }

            //R2
            obj = lst.FirstOrDefault(s => s.Type == "R2L");
            if (obj != null)
            {
                txtR2L.Text = obj.RCCount.ToString();
                txtR2P.Text = obj.FingerType.ToUpper();
                index.R2LPoint = obj.RCCount;
                index.R2P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R2R");
            if (obj != null)
            {
                txtR2R.Text = obj.RCCount.ToString();
                txtR2P.Text = obj.FingerType.ToUpper();
                index.R2RPoint = obj.RCCount;
                index.R2P = obj.FingerType.ToUpper();
            }

            //R3
            obj = lst.FirstOrDefault(s => s.Type == "R3L");
            if (obj != null)
            {
                txtR3L.Text = obj.RCCount.ToString();
                txtR3P.Text = obj.FingerType.ToUpper();
                index.R3LPoint = obj.RCCount;
                index.R3P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R3R");
            if (obj != null)
            {
                txtR3R.Text = obj.RCCount.ToString();
                txtR3P.Text = obj.FingerType.ToUpper();
                index.R3RPoint = obj.RCCount;
                index.R3P = obj.FingerType.ToUpper();
            }

            //R4
            obj = lst.FirstOrDefault(s => s.Type == "R4L");
            if (obj != null)
            {
                txtR4L.Text = obj.RCCount.ToString();
                txtR4P.Text = obj.FingerType.ToUpper();
                index.R4LPoint = obj.RCCount;
                index.R4P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R4R");
            if (obj != null)
            {
                txtR4R.Text = obj.RCCount.ToString();
                txtR4P.Text = obj.FingerType.ToUpper();
                index.R4RPoint = obj.RCCount;
                index.R4P = obj.FingerType.ToUpper();
            }

            //R5
            obj = lst.FirstOrDefault(s => s.Type == "R5L");
            if (obj != null)
            {
                txtR5L.Text = obj.RCCount.ToString();
                txtR5P.Text = obj.FingerType.ToUpper();
                index.R5LPoint = obj.RCCount;
                index.R5P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R5R");
            if (obj != null)
            {               
                txtR5R.Text = obj.RCCount.ToString();
                txtR5P.Text = obj.FingerType.ToUpper();
                index.R5RPoint = obj.RCCount;
                index.R5P = obj.FingerType.ToUpper();
            }
            #endregion Right

            #region Get Fingerprinttype images
            if (ImageList1 != null && ImageList1.Images.Count>0)
            {
                index.L1PImage = ImageList1.Images[index.L1P.ToString() + ".bmp"];
                index.L2PImage = ImageList1.Images[index.L2P.ToString() + ".bmp"];
                index.L3PImage = ImageList1.Images[index.L3P.ToString() + ".bmp"];
                index.L4PImage = ImageList1.Images[index.L4P.ToString() + ".bmp"];
                index.L5PImage = ImageList1.Images[index.L5P.ToString() + ".bmp"];

                index.R1PImage = ImageList1.Images[index.R1P.ToString() + ".bmp"];
                index.R2PImage = ImageList1.Images[index.R2P.ToString() + ".bmp"];
                index.R3PImage = ImageList1.Images[index.R3P.ToString() + ".bmp"];
                index.R4PImage = ImageList1.Images[index.R4P.ToString() + ".bmp"];
                index.R5PImage = ImageList1.Images[index.R5P.ToString() + ".bmp"];
            }
            #endregion Get Fingerprinttype images
        }

        void FillReportType()
        {
            if (ICurrentSessionService.VietNamLanguage)
            {
                cbxLoaiBaoCao.Items.AddRange(VietNameseReport);
                cbxLoaiBaoCao.SelectedText = "Trẻ Em";
                foreach (Control c in from Control c in this.Controls where c.Tag != null select c)
                {
                    if (c is TextBox)
                        c.Text = c.Tag.ToString();
                    if (c is C4FunLabel)
                        c.Text = c.Tag.ToString();
                    if (c is C4FunGroupBox)
                        c.Text = c.Tag.ToString();
                    if (c is Label)
                        c.Text = c.Tag.ToString();
                    if (c is GroupBox)
                        c.Text = c.Tag.ToString();
                    if (c is Button)
                        c.Text = c.Tag.ToString();
                    if (c is RadioButton)
                        c.Text = c.Tag.ToString();
                }
                foreach (Control c1 in c4FunGroupBox1.Panel.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in gbShowResult.Panel.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in panel1.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in panel2.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnlMenu.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
            }
            else
            {
                cbxLoaiBaoCao.Items.AddRange(EnglishReport);
                cbxLoaiBaoCao.SelectedText = "Children";
            }
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            //Close();
            //Dispose();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            txtDirSave.Text = fd.ShowDialog() == DialogResult.OK ? fd.SelectedPath : _dirFile;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                
               
                //Ham Xuat file tai day
                int templateID = 0;
                string message = "";

                if (cbxLoaiBaoCao.Text == "")
                {
                    message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại báo cáo cần xuất!" : "Please choose the report type first!";
                    MessageBox.Show(message);
                    return;
                }

                Hide();
                FrmFlash.ShowSplash();
                Application.DoEvents();
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    if (cbxLoaiBaoCao.Text == "Người Lớn" || cbxLoaiBaoCao.Text == "Adult")
                        templateID = 1;
                    else
                        templateID = 2;

                    FIFingerAnalysisService service = new FIFingerAnalysisService(unitOfWork);
                    bool KQ = service.GenerateReport(currCustomer.ID, ICurrentSessionService.CurAgency.ID, templateID, templateID == 1 ? Path.Combine(ICurrentSessionService.DesDirNameTemplate, "Adult.rptx") : Path.Combine(ICurrentSessionService.DesDirNameTemplate, "Children.rptx"), SetFullNameReport(txtDirSave.Text), true);
                    if (KQ)
                    {
                        FrmFlash.CloseSplash();
                        Activate();
                        Show();

                        message = ICurrentSessionService.VietNamLanguage ? "Xuất báo cáo thành công!\nBạn có muốn mở thư mục khách hàng này không?" : "Export successfully!\nDo you want to open cumstomer folder?";
                        if (MessageBox.Show(message, @"", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (!Directory.Exists(txtDirSave.Text)) return;
                            var info = new ProcessStartInfo
                            {
                                FileName = txtDirSave.Text,
                                UseShellExecute = true,
                                Verb = "open"
                            };
                            Process.Start(info);
                        }
                        Close();
                    }
                    else
                    {
                        FrmFlash.CloseSplash();
                        MessageBox.Show(service.ErrMsg, @"", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Activate();
                        Show();
                    }
                }
                
            }
            catch (Exception ex)
            {
                FrmFlash.CloseSplash();
                Activate();
                MessageBox.Show(ex.Message, @"",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Show();
            }
        }

        private void frmExportReport_Load(object sender, EventArgs e)
        {
            FillReportType();
            GetCustomer(CustomerId);
            txtDirSave.Text = _dirFile;
        }
    }
}
