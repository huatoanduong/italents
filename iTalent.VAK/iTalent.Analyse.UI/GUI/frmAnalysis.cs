﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Analyse.Commons;
using iTalent.Utils;
using System.IO;
using Microsoft.VisualBasic;
using iTalent.Analyse.Model;
using VAKFomula.Entity;
using C4FunComponent.Toolkit;
using iTalent.Analyse.UI.FlashForm;

namespace iTalent.Analyse.UI.GUI
{
    public partial class frmAnalysis : C4FunForm
    {
        private FPIndex index = null;
        private List<FIFingerRecord> lstFingerIndex = null;
        private FICustomer currCustomer = null;

        private string _dirFile;
        private string _rcCount;
        private int _customerid;
        private BaseForm _baseForm;
        public frmAnalysis(Int32 CustomerId)
        {
            InitializeComponent();
            _customerid = CustomerId;
            _baseForm = new BaseForm();
            this.Text = ICurrentSessionService.VietNamLanguage ? "PHÂN TÍCH VÂN TAY" : ".: FINGERPRINT ANALYSIS";
        }
        void FillReportType()
        {
            if (ICurrentSessionService.VietNamLanguage)
            {
                foreach (Control c in from Control c in this.Controls where c.Tag != null select c)
                {
                    if (c is TextBox)
                        c.Text = c.Tag.ToString();
                    if (c is C4FunLabel)
                        c.Text = c.Tag.ToString();
                    if (c is C4FunGroupBox)
                        c.Text = c.Tag.ToString();
                    if (c is Label)
                        c.Text = c.Tag.ToString();
                    if (c is GroupBox)
                        c.Text = c.Tag.ToString();
                    if (c is Button)
                        c.Text = c.Tag.ToString();
                    if (c is RadioButton)
                        c.Text = c.Tag.ToString();
                }
                foreach (Control c1 in c4FunGroupBox1.Panel.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in gbShowResult.Panel.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in grpAnalysis.Panel.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnlAnalysis.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunGroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl1.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl2.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl3.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl4.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl5.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl6.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl7.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl8.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl9.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                foreach (Control c1 in pnl10.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
                //foreach (Control c1 in panel1.Controls)
                //{
                //    if (c1.Tag == null) continue;
                //    if (c1 is Label)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is GroupBox)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is C4FunLabel)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is Button)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is RadioButton)
                //        c1.Text = c1.Tag.ToString();
                //}
                //foreach (Control c1 in panel2.Controls)
                //{
                //    if (c1.Tag == null) continue;
                //    if (c1 is Label)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is GroupBox)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is C4FunLabel)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is Button)
                //        c1.Text = c1.Tag.ToString();
                //    if (c1 is RadioButton)
                //        c1.Text = c1.Tag.ToString();
                //}
                foreach (Control c1 in pnlMenu.Controls.Cast<Control>().Where(c1 => c1.Tag != null))
                {
                    if (c1 is Label)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is GroupBox)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is C4FunLabel)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is Button)
                        c1.Text = c1.Tag.ToString();
                    if (c1 is RadioButton)
                        c1.Text = c1.Tag.ToString();
                }
            }
            //L
            btnL1L.Text = "";
            btnL1R.Text = "";
            btnL2L.Text = "";
            btnL2R.Text = "";
            btnL3L.Text = "";
            btnL3R.Text = "";
            btnL4L.Text = "";
            btnL4R.Text = "";
            btnL5L.Text = "";
            btnL5R.Text = "";
            //R
            btnR1L.Text = "";
            btnR1R.Text = "";
            btnR2L.Text = "";
            btnR2R.Text = "";
            btnR3L.Text = "";
            btnR3R.Text = "";
            btnR4L.Text = "";
            btnR4R.Text = "";
            btnR5L.Text = "";
            btnR5R.Text = "";
        }
        void GetCustomer(Int32 CustomerId)
        {
            ///////////Lấy dữ liệu customer vào và hình ảnh của nó.//////////
            GetCustomerData(CustomerId);
            GetCustomerFPImage();
            /////////////////////////////////////////////////////////////////

            if (currCustomer != null)
            {
                SetCustomerData();
                SetCustomerFPIndex();
            }
        }
        void GetCustomerData(Int32 CustomerId)
        {
            FICustomer localObject = null;
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFICustomerService service = new FICustomerService(unitOfWork);
                localObject = service.Find(CustomerId);
            }
            //After select localobject Load it to UI.
            currCustomer = localObject;
            if (currCustomer == null)
            {
                MessageBox.Show(ICurrentSessionService.VietNamLanguage ? @"Vui lòng chọn khách hàng trước khi phân tích" : "Please choose the customer from the list", @"Thông Báo", MessageBoxButtons.OK,
                   MessageBoxIcon.Information);
                Close();
            }
        }
        void GetCustomerFPImage()
        {
            GetPathContainFPImage();
            GetFingerprintImages();
        }
        void GetPathContainFPImage()
        {
            if (currCustomer != null)
            {
                var nameCustomer = ConvertUtil.ConverToUnsign(currCustomer.Name).Replace(" ", "").ToUpper();
                var folderCustomer = Path.Combine(ICurrentSessionService.DesDirNameSource , currCustomer.ReportID.Substring(7) + "_" + nameCustomer);
                if (!Directory.Exists(folderCustomer))
                {
                    MessageBox.Show(ICurrentSessionService.VietNamLanguage ? @"Khách hàng này phải được lấy vân tay trước khi đưa vào phân tích" : "Fingerprint files doesn't exist", @"Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    _dirFile = folderCustomer;
                }
            }
        }
        void GetFingerprintImages()
        {
            try
            {
                //Webcam
                var dirImage = GetImageFPFace(_dirFile, currCustomer.ReportID.Substring(7) + "F");
                if (File.Exists(dirImage)) pbF.Image = Image.FromFile(dirImage);

                //dirImage = GetImageFP(_dirFile, "L");
                //if (File.Exists(dirImage)) index.ImageLeftHand = Image.FromFile(dirImage);

                //dirImage = GetImageFP(_dirFile, "R");
                //if (File.Exists(dirImage)) index.ImageRightHand = Image.FromFile(dirImage);
                
                //Left hand RC
                dirImage = GetImageFP(_dirFile, "1_1");
                if (File.Exists(dirImage)) L1F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "1_2");
                if (File.Exists(dirImage)) L1L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "1_3");
                if (File.Exists(dirImage)) L1R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "2_1");
                if (File.Exists(dirImage)) L2F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "2_2");
                if (File.Exists(dirImage)) L2L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "2_3");
                if (File.Exists(dirImage)) L2R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "3_1");
                if (File.Exists(dirImage)) L3F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "3_2");
                if (File.Exists(dirImage)) L3L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "3_3");
                if (File.Exists(dirImage)) L3R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "4_1");
                if (File.Exists(dirImage)) L4F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "4_2");
                if (File.Exists(dirImage)) L4L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "4_3");
                if (File.Exists(dirImage)) L4R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "5_1");
                if (File.Exists(dirImage)) L5F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "5_2");
                if (File.Exists(dirImage)) L5L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "5_3");
                if (File.Exists(dirImage)) L5R.Image = Image.FromFile(dirImage);

                //Right hand RC
                dirImage = GetImageFP(_dirFile, "6_1");
                if (File.Exists(dirImage)) R1F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "6_2");
                if (File.Exists(dirImage)) R1L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "6_3");
                if (File.Exists(dirImage)) R1R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "7_1");
                if (File.Exists(dirImage)) R2F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "7_2");
                if (File.Exists(dirImage)) R2L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "7_3");
                if (File.Exists(dirImage)) R2R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "8_1");
                if (File.Exists(dirImage)) R3F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "8_2");
                if (File.Exists(dirImage)) R3L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "8_3");
                if (File.Exists(dirImage)) R3R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "9_1");
                if (File.Exists(dirImage)) R4F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "9_2");
                if (File.Exists(dirImage)) R4L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "9_3");
                if (File.Exists(dirImage)) R4R.Image = Image.FromFile(dirImage);

                dirImage = GetImageFP(_dirFile, "10_1");
                if (File.Exists(dirImage)) R5F.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "10_2");
                if (File.Exists(dirImage)) R5L.Image = Image.FromFile(dirImage);
                dirImage = GetImageFP(_dirFile, "10_3");
                if (File.Exists(dirImage)) R5R.Image = Image.FromFile(dirImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
        }
        string GetImageFP(string fileSave, string type)
        {
            return Path.Combine(fileSave, type + ".b");
        }
        string GetImageFPFace(string fileSave, string type)
        {
            return Path.Combine(fileSave, type + ".bmp");
        }

        void SetCustomerData()
        {
            if (currCustomer != null)
            {
                labMa.Text = currCustomer.ID.ToString();
                labReportId.Text = currCustomer.ReportID.ToString();
                labDateCreate.Text = DateTimeUtil.ConvertDate(currCustomer.Date.ToString()).ToString("dd/MM/yyyy");

                txtTen.Text = currCustomer.Name;
                txtTenChaMe.Text = currCustomer.Parent;
                if (currCustomer.Gender == "Nam" || currCustomer.Gender == "Male")
                {
                    rdMale.Checked = true;
                    rdFemale.Checked = false;
                }
                if (currCustomer.Gender == "Nữ" || currCustomer.Gender == "Female")
                {
                    rdMale.Checked = false;
                    rdFemale.Checked = true;
                }
                dtpNgaySinh.Value = DateTimeUtil.ConvertDate(currCustomer.DOB.ToString());
                txtEmail.Text = currCustomer.Email;
                txtDiaChi.Text = currCustomer.Address1;
                txtGhiChu.Text = currCustomer.Remark;
                txtDienThoai.Text = currCustomer.Tel;
                txtThanhPho.Text = currCustomer.Country;
                txtDiDong.Text = currCustomer.Mobile;
            }
        }
        void SetCustomerFPIndex()
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFingerRecordService service = new FIFingerRecordService(unitOfWork);
                var lst = service.FindByRecordId(currCustomer.ID);
                lstFingerIndex = lst;
                if (lst == null) return;
                if (lst.Count == 0) return;
                SetCustomerFPIndexToUI(lst);
            }
        }
        void SetCustomerFPIndexToUI(List<FIFingerRecord> lst)
        {
            index = new FPIndex();
            var obj = lst.FirstOrDefault(s => s.Type == "LATD");
            if (obj != null)
            {
                txtLATD.Text = obj.ATDPoint.ToString();
                index.LATDPoint = obj.ATDPoint;
            }

            obj = lst.FirstOrDefault(s => s.Type == "RATD");
            if (obj != null)
            {
                txtRATD.Text = obj.ATDPoint.ToString();
                index.RATDPoint = obj.ATDPoint;
            }
            #region Left
            //L1
            obj = lst.FirstOrDefault(s => s.Type == "L1L");
            if (obj != null)
            {
                txtL1LResult.Text = obj.RCCount.ToString();
                txtL1PResult.Text = obj.FingerType.ToUpper();
                txtL1L.Text = obj.RCCount.ToString();
                txtL1P.Text = obj.FingerType.ToUpper();
                lblL1P.Text = obj.FingerType.ToUpper();
                index.L1LPoint = obj.RCCount;
                index.L1P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L1R");
            if (obj != null)
            {
                txtL1RResult.Text = obj.RCCount.ToString();
                txtL1PResult.Text = obj.FingerType.ToUpper();
                txtL1R.Text = obj.RCCount.ToString();
                txtL1P.Text = obj.FingerType.ToUpper();
                lblL1P.Text = obj.FingerType.ToUpper();
                index.L1RPoint = obj.RCCount;
                index.L1P = obj.FingerType.ToUpper();
            }

            //L2
            obj = lst.FirstOrDefault(s => s.Type == "L2L");
            if (obj != null)
            {
                txtL2LResult.Text = obj.RCCount.ToString();
                txtL2PResult.Text = obj.FingerType.ToUpper();
                txtL2L.Text = obj.RCCount.ToString();
                txtL2P.Text = obj.FingerType.ToUpper();
                lblL2P.Text = obj.FingerType.ToUpper();
                index.L2LPoint = obj.RCCount;
                index.L2P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L2R");
            if (obj != null)
            {
                txtL2RResult.Text = obj.RCCount.ToString();
                txtL2PResult.Text = obj.FingerType.ToUpper();
                txtL2R.Text = obj.RCCount.ToString();
                txtL2P.Text = obj.FingerType.ToUpper();
                lblL2P.Text = obj.FingerType.ToUpper();
                index.L2RPoint = obj.RCCount;
                index.L2P = obj.FingerType.ToUpper();
            }

            //L3
            obj = lst.FirstOrDefault(s => s.Type == "L3L");
            if (obj != null)
            {
                txtL3LResult.Text = obj.RCCount.ToString();
                txtL3PResult.Text = obj.FingerType.ToUpper();
                txtL3L.Text = obj.RCCount.ToString();
                txtL3P.Text = obj.FingerType.ToUpper();
                lblL3P.Text = obj.FingerType.ToUpper();
                index.L3LPoint = obj.RCCount;
                index.L3P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L3R");
            if (obj != null)
            {
                txtL3RResult.Text = obj.RCCount.ToString();
                txtL3PResult.Text = obj.FingerType.ToUpper();
                txtL3R.Text = obj.RCCount.ToString();
                txtL3P.Text = obj.FingerType.ToUpper();
                lblL3P.Text = obj.FingerType.ToUpper();
                index.L3RPoint = obj.RCCount;
                index.L3P = obj.FingerType.ToUpper();
            }

            //L4
            obj = lst.FirstOrDefault(s => s.Type == "L4L");
            if (obj != null)
            {
                txtL4LResult.Text = obj.RCCount.ToString();
                txtL4PResult.Text = obj.FingerType.ToUpper();
                txtL4L.Text = obj.RCCount.ToString();
                txtL4P.Text = obj.FingerType.ToUpper();
                lblL4P.Text = obj.FingerType.ToUpper();
                index.L4LPoint = obj.RCCount;
                index.L4P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L4R");
            if (obj != null)
            {
                txtL4RResult.Text = obj.RCCount.ToString();
                txtL4PResult.Text = obj.FingerType.ToUpper();
                txtL4R.Text = obj.RCCount.ToString();
                txtL4P.Text = obj.FingerType.ToUpper();
                lblL4P.Text = obj.FingerType.ToUpper();
                index.L4RPoint = obj.RCCount;
                index.L4P = obj.FingerType.ToUpper();
            }

            //L5
            obj = lst.FirstOrDefault(s => s.Type == "L5L");
            if (obj != null)
            {
                txtL5LResult.Text = obj.RCCount.ToString();
                txtL5PResult.Text = obj.FingerType.ToUpper();
                txtL5L.Text = obj.RCCount.ToString();
                txtL5P.Text = obj.FingerType.ToUpper();
                lblL5P.Text = obj.FingerType.ToUpper();
                index.L5LPoint = obj.RCCount;
                index.L5P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "L5R");
            if (obj != null)
            {
                txtL5RResult.Text = obj.RCCount.ToString();
                txtL5PResult.Text = obj.FingerType.ToUpper();
                txtL5R.Text = obj.RCCount.ToString();
                txtL5P.Text = obj.FingerType.ToUpper();
                lblL5P.Text = obj.FingerType.ToUpper();
                index.L5RPoint = obj.RCCount;
                index.L5P = obj.FingerType.ToUpper();
            }
            #endregion Left

            #region Right
            //R1
            obj = lst.FirstOrDefault(s => s.Type == "R1L");
            if (obj != null)
            {
                txtR1LResult.Text = obj.RCCount.ToString();
                txtR1PResult.Text = obj.FingerType.ToUpper();
                txtR1L.Text = obj.RCCount.ToString();
                txtR1P.Text = obj.FingerType.ToUpper();
                lblR1P.Text = obj.FingerType.ToUpper();
                index.R1LPoint = obj.RCCount;
                index.R1P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R1R");
            if (obj != null)
            {
                txtR1RResult.Text = obj.RCCount.ToString();
                txtR1PResult.Text = obj.FingerType.ToUpper();
                txtR1R.Text = obj.RCCount.ToString();
                txtR1P.Text = obj.FingerType.ToUpper();
                lblR1P.Text = obj.FingerType.ToUpper();
                index.R1RPoint = obj.RCCount;
                index.R1P = obj.FingerType.ToUpper();
            }

            //R2
            obj = lst.FirstOrDefault(s => s.Type == "R2L");
            if (obj != null)
            {
                txtR2LResult.Text = obj.RCCount.ToString();
                txtR2PResult.Text = obj.FingerType.ToUpper();
                txtR2L.Text = obj.RCCount.ToString();
                txtR2P.Text = obj.FingerType.ToUpper();
                lblR2P.Text = obj.FingerType.ToUpper();
                index.R2LPoint = obj.RCCount;
                index.R2P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R2R");
            if (obj != null)
            {
                txtR2RResult.Text = obj.RCCount.ToString();
                txtR2PResult.Text = obj.FingerType.ToUpper();
                txtR2R.Text = obj.RCCount.ToString();
                txtR2P.Text = obj.FingerType.ToUpper();
                lblR2P.Text = obj.FingerType.ToUpper();
                index.R2RPoint = obj.RCCount;
                index.R2P = obj.FingerType.ToUpper();
            }

            //R3
            obj = lst.FirstOrDefault(s => s.Type == "R3L");
            if (obj != null)
            {
                txtR3LResult.Text = obj.RCCount.ToString();
                txtR3PResult.Text = obj.FingerType.ToUpper();
                txtR3L.Text = obj.RCCount.ToString();
                txtR3P.Text = obj.FingerType.ToUpper();
                lblR3P.Text = obj.FingerType.ToUpper();
                index.R3LPoint = obj.RCCount;
                index.R3P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R3R");
            if (obj != null)
            {
                txtR3RResult.Text = obj.RCCount.ToString();
                txtR3PResult.Text = obj.FingerType.ToUpper();
                txtR3R.Text = obj.RCCount.ToString();
                txtR3P.Text = obj.FingerType.ToUpper();
                lblR3P.Text = obj.FingerType.ToUpper();
                index.R3RPoint = obj.RCCount;
                index.R3P = obj.FingerType.ToUpper();
            }

            //R4
            obj = lst.FirstOrDefault(s => s.Type == "R4L");
            if (obj != null)
            {
                txtR4LResult.Text = obj.RCCount.ToString();
                txtR4PResult.Text = obj.FingerType.ToUpper();
                txtR4L.Text = obj.RCCount.ToString();
                txtR4P.Text = obj.FingerType.ToUpper();
                lblR4P.Text = obj.FingerType.ToUpper();
                index.R4LPoint = obj.RCCount;
                index.R4P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R4R");
            if (obj != null)
            {
                txtR4RResult.Text = obj.RCCount.ToString();
                txtR4PResult.Text = obj.FingerType.ToUpper();
                txtR4R.Text = obj.RCCount.ToString();
                txtR4P.Text = obj.FingerType.ToUpper();
                lblR4P.Text = obj.FingerType.ToUpper();
                index.R4RPoint = obj.RCCount;
                index.R4P = obj.FingerType.ToUpper();
            }

            //R5
            obj = lst.FirstOrDefault(s => s.Type == "R5L");
            if (obj != null)
            {
                txtR5LResult.Text = obj.RCCount.ToString();
                txtR5PResult.Text = obj.FingerType.ToUpper();
                txtR5L.Text = obj.RCCount.ToString();
                txtR5P.Text = obj.FingerType.ToUpper();
                lblR5P.Text = obj.FingerType.ToUpper();
                index.R5LPoint = obj.RCCount;
                index.R5P = obj.FingerType.ToUpper();
            }

            obj = lst.FirstOrDefault(s => s.Type == "R5R");
            if (obj != null)
            {
                txtR5RResult.Text = obj.RCCount.ToString();
                txtR5PResult.Text = obj.FingerType.ToUpper();
                txtR5R.Text = obj.RCCount.ToString();
                txtR5P.Text = obj.FingerType.ToUpper();
                lblR5P.Text = obj.FingerType.ToUpper();
                index.R5RPoint = obj.RCCount;
                index.R5P = obj.FingerType.ToUpper();
            }
            #endregion Right

            #region Get Fingerprinttype images
            if (ImageList1 != null)
            {
                L1P.Image = ImageList1.Images[index.L1P.ToString() + ".bmp"];
                L2P.Image = ImageList1.Images[index.L2P.ToString() + ".bmp"];
                L3P.Image = ImageList1.Images[index.L3P.ToString() + ".bmp"];
                L4P.Image = ImageList1.Images[index.L4P.ToString() + ".bmp"];
                L5P.Image = ImageList1.Images[index.L5P.ToString() + ".bmp"];

                R1P.Image = ImageList1.Images[index.R1P.ToString() + ".bmp"];
                R2P.Image = ImageList1.Images[index.R2P.ToString() + ".bmp"];
                R3P.Image = ImageList1.Images[index.R3P.ToString() + ".bmp"];
                R4P.Image = ImageList1.Images[index.R4P.ToString() + ".bmp"];
                R5P.Image = ImageList1.Images[index.R5P.ToString() + ".bmp"];
            }
            #endregion Get Fingerprinttype images
        }

        private void SetStateButtonRc(Panel pnlRc,TextBox txtRc)
        {
            TextBox txtL = null;
            TextBox txtR = null;
            Label lbl = null;

            foreach (Label lblP in pnlRc.Controls.OfType<Label>().Where(lblP => lblP.Name.Contains("P")))
            {
                lbl = lblP;
            }
            foreach (TextBox txt in pnlRc.Controls.OfType<TextBox>().Where(txt => txt.Tag!=null && txt.Tag.ToString().Contains("_2")))
            {
                txtL = txt;
            }
            foreach (TextBox txt in pnlRc.Controls.OfType<TextBox>().Where(txt => txt.Tag != null && txt.Tag.ToString().Contains("_3")))
            {
                txtR = txt;
            }

            if (lbl != null && lbl.Text == @"UL")
            {
                if (txtRc.Tag.ToString().Contains("_2"))
                {
                    if (txtR != null) txtR.Text = @"0";
                }
                else
                {
                    if (txtL != null) txtL.Text = @"0";
                }
            }
            else if (lbl != null && (lbl.Text == @"AU" || lbl.Text == @"AT" || lbl.Text == @"AE" || lbl.Text == @"AR" || lbl.Text == @"AS" || lbl.Text == @"WX"))
            {
                if (txtR != null)
                {
                    txtR.Text = @"0";
                    txtR.Visible = false;
                }
                if (txtL != null)
                {
                    txtL.Text = @"0";
                    txtL.Visible = false;
                }

                foreach (Button btn in pnlRc.Controls.OfType<Button>().Where(btn => btn != null))
                {
                    btn.Visible = false;
                }
            }
        }

        //private void Call_RC_Count()
        //{
        //    using (var process = new Process())
        //    {
        //        foreach (var process3 in Process.GetProcessesByName("Untitled8f32"))
        //        {
        //            process3.Kill();
        //        }

        //        var info = new ProcessStartInfo("Untitled8f32.exe");
        //        process.StartInfo = info;
        //        process.EnableRaisingEvents = true;
        //        process.Exited += ExitRc;
        //        process.Start();
        //        process.WaitForExit();
        //    }
        //}
        //private void Write_RC_file(string fName)
        //{
        //    var str = Path.Combine(_dirFile, fName + ".b");
        //    var filename = Directory.GetCurrentDirectory() + @"\inbound.txt";
        //    var writer = new StreamWriter(filename, false, Encoding.ASCII);
        //    writer.Write(str);
        //    writer.Close();
        //}
        //private void ExitRc()
        //{
        //    if (!File.Exists(Directory.GetCurrentDirectory() + @"\outbound.dat")) return;
        //    string str;
        //    TextReader reader = new StreamReader(Directory.GetCurrentDirectory() + @"\outbound.dat");
        //    while (true)
        //    {
        //        str = reader.ReadLine();
        //        break;
        //    }
        //    reader.Close();
        //    _rcCount = str;

        //    if (File.Exists(Directory.GetCurrentDirectory() + @"\outbound.dat"))
        //    {
        //        File.Delete(Directory.GetCurrentDirectory() + @"\outbound.dat");
        //    }

        //    if (File.Exists(Directory.GetCurrentDirectory() + @"\inbound.txt"))
        //    {
        //        File.Delete(Directory.GetCurrentDirectory() + @"\inbound.txt");
        //    }
        //}
        //private void ExitRc(object sender, EventArgs e)
        //{
        //    ExitRc();
        //}
        //private void LayThongSoVanTay(TextBox text)
        //{
        //    try
        //    {
        //        _rcCount = text.Text;
        //        Write_RC_file(text.Tag.ToString());
        //        Call_RC_Count();
        //        text.Text = _rcCount;
        //    }
        //    catch
        //    {
        //        //
        //    }
        //}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }
        private FIFingerRecord SetFingerRecord(TextBox text, TextBox textP)
        {
            var agencyId = ICurrentSessionService.UserId;
            var c = decimal.Parse(text.Text);
            var type = text.Name.Replace("txt", "");
            var pt = "";
            if (textP != null)
                pt = textP.Text;

            return new FIFingerRecord
            {
                ID = 0,
                AgencyID = agencyId,
                ATDPoint = c,
                FingerType = pt,
                Type = type,
                RCCount = c,
                ReportID = currCustomer.ID,
                ScanCheck = 0
            };
        }
        private List<FIFingerRecord> GenerateListFPRecord()
        {
            List<FIFingerRecord> list = new List<FIFingerRecord>();
            var obj = SetFingerRecord(txtLATD, null);
            list.Add(obj);

            obj = SetFingerRecord(txtRATD, null);
            list.Add(obj);
            //L
            obj = SetFingerRecord(txtL1L, txtL1P);
            list.Add(obj);
            obj = SetFingerRecord(txtL1R, txtL1P);
            list.Add(obj);
            obj = SetFingerRecord(txtL2L, txtL2P);
            list.Add(obj);
            obj = SetFingerRecord(txtL2R, txtL2P);
            list.Add(obj);
            obj = SetFingerRecord(txtL3L, txtL3P);
            list.Add(obj);
            obj = SetFingerRecord(txtL3R, txtL3P);
            list.Add(obj);
            obj = SetFingerRecord(txtL4L, txtL4P);
            list.Add(obj);
            obj = SetFingerRecord(txtL4R, txtL4P);
            list.Add(obj);
            obj = SetFingerRecord(txtL5L, txtL5P);
            list.Add(obj);
            obj = SetFingerRecord(txtL5R, txtL5P);
            list.Add(obj);
            //R
            obj = SetFingerRecord(txtR1L, txtR1P);
            list.Add(obj);
            obj = SetFingerRecord(txtR1R, txtR1P);
            list.Add(obj);
            obj = SetFingerRecord(txtR2L, txtR2P);
            list.Add(obj);
            obj = SetFingerRecord(txtR2R, txtR2P);
            list.Add(obj);
            obj = SetFingerRecord(txtR3L, txtR3P);
            list.Add(obj);
            obj = SetFingerRecord(txtR3R, txtR3P);
            list.Add(obj);
            obj = SetFingerRecord(txtR4L, txtR4P);
            list.Add(obj);
            obj = SetFingerRecord(txtR4R, txtR4P);
            list.Add(obj);
            obj = SetFingerRecord(txtR5L, txtR5P);
            list.Add(obj);
            obj = SetFingerRecord(txtR5R, txtR5P);
            list.Add(obj);
            return list;
        }
        private void MapList(List<FIFingerRecord> source, List<FIFingerRecord> dest)
        {
            FIFingerRecord record;
            FIFingerRecord recordSource;
            for (int i = 0; i < 22; i++)
            {
                recordSource = source[i];
                record = dest.FirstOrDefault(p => (p.ReportID == recordSource.ReportID) && (p.Type == recordSource.Type));
                if (record == null)
                {
                    dest.Add(recordSource);
                }
                else
                {
                    record.ATDPoint = recordSource.ATDPoint;
                    record.RCCount = recordSource.RCCount;
                    record.FingerType = recordSource.FingerType;
                    record.ScanCheck = recordSource.ScanCheck;
                }
            }
        }
        private void Save()
        {
            try
            {
                string message;
                var kq = Check(out message);
                if (kq)
                {
                    var lisRecords = GenerateListFPRecord();
                    if (lisRecords == null) return;
                    if (lisRecords.Count == 0) return;

                    using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                    {
                        IFingerRecordService service = new FIFingerRecordService(unitOfWork);
                        MapList(lisRecords, lstFingerIndex);
                        foreach (FIFingerRecord record in lstFingerIndex)
                        {
                            if (record.ID <= 0)
                            {
                                service.Add(record);
                            }
                            else
                            {
                                service.Update(record);
                            }
                        }

                        IFingerRecordService fingerRecordService = new FIFingerRecordService(unitOfWork);
                        var lst = fingerRecordService.FindByRecordId(currCustomer.ID);
                        lstFingerIndex = lst;

                        /////////Insert/Update into FingerAnalysis for calculate by the formula///////////
                        IFIFingerAnalysisService analysisService = new FIFingerAnalysisService(unitOfWork);
                        kq = analysisService.CalculateFingerRecord(lstFingerIndex[0].ReportID);
                        if (!kq)
                        {
                            ShowMessageBox(ICurrentSessionService.VietNamLanguage
                                ? "Phân tích vân tay thất bại!"
                                : "Analysed failed!");
                        }
                        else
                        {
                            //////////////////////////////////////////////////////////////////////////////////

                            IFIAgencyService agencyService = new FIAgencyService(unitOfWork);
                            FIAgency agency = agencyService.Find(currCustomer.AgencyID);
                            if (agency == null)
                            {
                                agency = agencyService.Find(ICurrentSessionService.CurAgency.ID);
                            }
                            DatFileModel model = new DatFileModel()
                            {
                                FingerRecords = lstFingerIndex,
                                FICustomer = currCustomer,
                                FIAgency = agency
                            };

                            IDatFileService datFileService = new DatFileService(unitOfWork);
                            IDirectoryService directoryService = new DirectoryService();
                            string fileName = directoryService.GetFullDatFileName(currCustomer);

                            kq = datFileService.WriteDatFile(fileName, model);
                            if (kq)
                            {
                                ShowMessageBox(ICurrentSessionService.VietNamLanguage
                                    ? "Phân tích vân tay thành công!"
                                    : "Analysed Successfully!");
                            }
                            else
                                ShowMessageBox(datFileService.ErrMsg);
                        }
                        Close();
                    }
                }
                else
                {
                    ShowMessageBox(message);
                }
            }
            catch (Exception)
            {
                //
                //throw;
            }
        }
        private void ShowMessageBox(string mess)
        {
            MessageBox.Show(mess, @"Thông Báo");
        }
        private bool Check(out string message)
        {
            if (txtLATD.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage? "Vui lòng điền chỉ số 'L ATD' lớn hơn không.": "please input 'L ATD'";
                txtLATD.Select();
                return false;
            }
            if (txtRATD.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số 'R ATD' lớn hơn không." : "please input 'R ATD'";
                txtRATD.Select();
                return false;
            }
            if (txtL1PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón cái tay trái'L1'." : "please choose fingerprint type 'L1'";
                tabControl.SelectTab(0);
                txtL1L.Select();
                return false;
            }
            if (txtL1LResult.Text == "" && txtL1L.Text=="")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón cái tay trái 'L1 RC'." : "please input left 'L1 RC'";
                tabControl.SelectTab(0);
                txtL1L.Select();
                return false;
            }
            if (txtL1RResult.Text == "" && txtL1R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón cái tay trái 'L1 RC'." : "please input right 'L1 RC'";
                tabControl.SelectTab(0);
                txtL1R.Select();
                return false;
            }
            if (txtL2PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón trỏ tay trái'L2'." : "please choose fingerprint type 'L2'";
                tabControl.SelectTab(1);
                txtL2L.Select();
                return false;
            }
            if (txtL2LResult.Text == "" && txtL2L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón trỏ tay trái 'L2 RC'." : "please input left 'L2 RC'";
                tabControl.SelectTab(1);
                txtL2L.Select();
                return false;
            }
            if (txtL2RResult.Text == "" && txtL2R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón trỏ tay trái 'L2 RC'." : "please input right 'L2 RC'";
                tabControl.SelectTab(1);
                txtL2R.Select();
                return false;
            }
            if (txtL3PResult.Text == "" )
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón giữa tay trái 'L3'." : "please choose fingerprint type 'L3'";
                tabControl.SelectTab(2);
                txtL3L.Select();
                return false;
            }
            if (txtL3LResult.Text == "" && txtL3L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón giữa tay trái 'L3 RC'." : "please input left 'L3 RC'";
                tabControl.SelectTab(2);
                txtL3L.Select();
                return false;
            }
            if (txtL3RResult.Text == "" && txtL3R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón giữa tay trái 'L3 RC'." : "please input right 'L3 RC'";
                tabControl.SelectTab(2);
                txtL3R.Select();
                return false;
            }
            if (txtL4PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón cái tay nhẫn 'L4'." : "please choose fingerprint type 'L4'";
                tabControl.SelectTab(3);
                txtL4L.Select();
                return false;
            }
            if (txtL4LResult.Text == "" && txtL4L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón nhẫn tay trái 'L4 RC'." : "please input left 'L4 RC'";
                tabControl.SelectTab(3);
                txtL4L.Select();
                return false;
            }
            if (txtL4RResult.Text == "" && txtL4R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón nhẫn tay trái 'L4 RC'." : "please input right 'L4 RC'";
                tabControl.SelectTab(3);
                txtL4R.Select();
                return false;
            }
            if (txtL5PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón cái tay út 'L5'." : "please choose fingerprint type 'L5'";
                tabControl.SelectTab(4);
                txtL5L.Select();
                return false;
            }
            if (txtL5LResult.Text == "" && txtL5L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón út tay trái 'L5 RC'." : "please input left 'L5 RC'";
                tabControl.SelectTab(4);
                txtL5L.Select();
                return false;
            }
            if (txtL5RResult.Text == "" && txtL5R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón út tay trái 'L5 RC'." : "please input right 'L5 RC'";
                tabControl.SelectTab(4);
                txtL5R.Select();
                return false;
            }
            if (txtR1PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón cái tay phải 'R1'." : "please choose fingerprint type 'R1'";
                tabControl.SelectTab(5);
                txtR1L.Select();
                return false;
            }
            if (txtR1LResult.Text == "" && txtR1L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón cái tay phải 'R1 RC'." : "please input left 'R1 RC'";
                tabControl.SelectTab(5);
                txtR1L.Select();
                return false;
            }
            if (txtR1RResult.Text == "" && txtR1R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón cái tay phải 'R1 RC'." : "please input right 'R1 RC'";
                tabControl.SelectTab(5);
                txtR1R.Select();
                return false;
            }
            if (txtR2PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón trỏ tay phải 'R2'." : "please choose fingerprint type 'R2'";
                tabControl.SelectTab(6);
                txtR2L.Select();
                return false;
            }
            if (txtR2LResult.Text == "" && txtR2L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón trỏ tay phải 'R2 RC'." : "please input left 'R2 RC'";
                tabControl.SelectTab(6);
                txtR2L.Select();
                return false;
            }
            if (txtR2RResult.Text == "" && txtR2R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón trỏ tay phải 'R2 RC'." : "please input right 'R2 RC'";
                tabControl.SelectTab(6);
                txtR2R.Select();
                return false;
            }
            if (txtR3PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón giữa tay phải 'R3'." : "please choose fingerprint type 'R3'";
                tabControl.SelectTab(7);
                txtR3L.Select();
                return false;
            }
            if (txtR3LResult.Text == "" && txtR3L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón giữa tay phải 'R3 RC'." : "please input left 'R3 RC'";
                tabControl.SelectTab(7);
                txtR3L.Select();
                return false;
            }
            if (txtR3RResult.Text == "" && txtR3R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón giữa tay phải 'R3 RC'." : "please input right 'R3 RC'";
                tabControl.SelectTab(7);
                txtR3R.Select();
                return false;
            }
            if (txtR4PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón nhẫn tay phải 'R4'." : "please choose fingerprint type 'R4'";
                tabControl.SelectTab(8);
                txtR4L.Select();
                return false;
            }
            if (txtR4LResult.Text == "" && txtR4L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón nhẫn tay phải 'R4 RC'." : "please input left 'R4 RC'";
                tabControl.SelectTab(8);
                txtR4L.Select();
                return false;
            }
            if (txtR4RResult.Text == "" && txtR4R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón nhẫn tay phải 'R4 RC'." : "please input right 'R4 RC'";
                tabControl.SelectTab(8);
                txtR4R.Select();
                return false;
            }
            if (txtR5PResult.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng chọn loại vân tay của ngón út tay phải 'R5'." : "please choose fingerprint type 'R5'";
                tabControl.SelectTab(9);
                txtR5R.Select();
                return false;
            }
            if (txtR5LResult.Text == "" && txtR5L.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên trái của ngón út tay phải 'R5 RC'." : "please input left 'R5 RC'";
                tabControl.SelectTab(9);
                txtR5R.Select();
                return false;
            }
            if (txtR5RResult.Text == "" && txtR5R.Text == "")
            {
                message = ICurrentSessionService.VietNamLanguage ? "Vui lòng điền chỉ số vân tay bên phải của ngón út tay phải 'R5 RC'." : "please input right 'R5 RC'";
                tabControl.SelectTab(9);
                txtR5R.Select();
                return false;
            }
            message = "";
            return true;
        }
        private void SetButtonClickEvent()
        {
            foreach (Button button in tabControl.TabPages.Cast<TabPage>().SelectMany(page => page.Controls.OfType<Panel>().SelectMany(panel => panel.Controls.OfType<Button>())))
            {
                button.Click += btn_Click;
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            var txtname = btn.Tag.ToString();

            Panel pnl = btn.Parent as Panel;
            if (pnl != null)
            {
                int indexF = int.Parse(pnl.Name.Substring(3));

                Label lbl = null;
                foreach (Label lblP in pnl.Controls.OfType<Label>().Where(lblP => lblP.Name.Contains("P")))
                {
                    lbl = lblP;
                }

                if (lbl != null && (lbl.Text.Contains("L"+ indexF + @"P") || lbl.Text.Contains("R" + indexF + @"P")))
                {
                    _baseForm.VietNamMsg = "Vui lòng chọn loại vân tay trước!";
                    _baseForm.EnglishMsg = "Please choose a fingerprint type first!!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;
                }

                TextBox txtRc = null;
                foreach (TextBox txt in pnl.Controls.OfType<TextBox>().Where(txt => txt.Tag != null && txtname == txt.Tag.ToString()))
                {
                    txtRc = txt;
                }

                if (txtRc != null && indexF > 0)
                {
                    FrmCountFinger frm = new FrmCountFinger(indexF, _dirFile);
                    frm.ShowDialog();
                    if (frm.RCount != null)
                    {
                        if (frm.RCount != "")
                        {
                            txtRc.Text = frm.RCount;
                            SetStateButtonRc(pnl, txtRc);
                        } 
                    }
                }
            }
        }
        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            const string str = "0123456789";
            if (Strings.InStr(str, e.KeyChar.ToString()) == 0)
            {
                e.Handled = true;
            }
            switch (Convert.ToInt32(e.KeyChar))
            {
                case 8:
                    e.Handled = false;
                    break;

                case 190:
                    e.Handled = false;
                    break;

                case 46:
                    e.Handled = false;
                    break;
            }
        }

        #region Event LV Click
        private void lv_MC(ListView lv, TextBox txtLp, Label lblLp, PictureBox ptbLp,Button btnL,Button btnR,TextBox txtL,TextBox txtR)
        {
            txtLp.Text = lv.SelectedItems[0].ImageKey.Replace(".bmp", "");
            lblLp.Text = lv.SelectedItems[0].Text;
            try
            {
                Image img = ImageList1.Images[txtLp.Text + ".bmp"];
                ptbLp.Image = img;
                btnL.Visible = btnR.Visible = txtL.Visible = txtR.Visible = true;
                if (lblLp.Text == @"AU" || lblLp.Text == @"AT" || lblLp.Text == @"AE" || lblLp.Text == @"AS" || lblLp.Text == @"AR" || lblLp.Text == @"WX")
                {
                    txtR.Text = txtL.Text = @"0";
                    btnL.Visible = btnR.Visible = txtL.Visible = txtR.Visible = false;
                }
                else if (lblLp.Text == @"UL")
                {
                    if (txtL.Text != @"" && txtL.Text != @"0")
                    {
                        txtR.Text = @"0";
                    }
                    else if (txtR.Text != @"" && txtR.Text != @"0")
                    {
                        txtL.Text = @"0";
                    }
                    else
                    {
                        txtR.Text = @"";
                        txtL.Text = @"";
                    }
                }
                else
                {
                    txtR.Text = @"";
                    txtL.Text = @"";
                }
            }
            catch
            {
                ptbLp.Image = null;
            }
        }
        private void lv_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TabPage pg = tabControl.SelectedTab;
            switch (pg.Name)
            {
                case "TabPage1":
                    lv_MC(lvL1, txtL1P, lblL1P, L1P,btnL1L,btnL1R,txtL1L,txtL1R);
                    break;
                case "TabPage2":
                    lv_MC(lvL1, txtL2P, lblL2P, L2P, btnL2L, btnL2R, txtL2L, txtL2R);
                    break;
                case "TabPage3":
                    lv_MC(lvL1, txtL3P, lblL3P, L3P, btnL3L, btnL3R, txtL3L, txtL3R);
                    break;
                case "TabPage4":
                    lv_MC(lvL1, txtL4P, lblL4P, L4P, btnL4L, btnL4R, txtL4L, txtL4R);
                    break;
                case "TabPage5":
                    lv_MC(lvL1, txtL5P, lblL5P, L5P, btnL5L, btnL5R, txtL5L, txtL5R);
                    break;
                case "TabPage6":
                    lv_MC(lvL1, txtR1P, lblR1P, R1P, btnR1L, btnR1R, txtR1L, txtR1R);
                    break;
                case "TabPage7":
                    lv_MC(lvL1, txtR2P, lblR2P, R2P, btnR2L, btnR2R, txtR2L, txtR2R);
                    break;
                case "TabPage8":
                    lv_MC(lvL1, txtR3P, lblR3P, R3P, btnR3L, btnR3R, txtR3L, txtR3R);
                    break;
                case "TabPage9":
                    lv_MC(lvL1, txtR4P, lblR4P, R4P, btnR4L, btnR4R, txtR4L, txtR4R);
                    break;
                case "TabPage10":
                    lv_MC(lvL1, txtR5P, lblR5P, R5P, btnR5L, btnR5R, txtR5L, txtR5R);
                    break;
            }

        }
        private void SetTypeImage(PictureBox ptbF, PictureBox ptbP, TextBox txtP, Label lblP)
        {
            try
            {
                if (ptbF.Image == null) return;
                Bitmap bmpP = null;
                if (ptbP.Image != null)
                    bmpP = new Bitmap(ptbP.Image);

                var bmpF = new Bitmap(ptbF.Image);
                var typeId = txtP.Text;
                var nameType = lblP.Text;
                var nameF = ptbF.Name.ToUpper();

                Hide();
                FrmChoiceFinger frm = new FrmChoiceFinger(bmpF, bmpP, nameType, typeId, nameF);
                frm.ShowDialog(this);
                if (frm.FingerP != null)
                {
                    ptbP.Image = frm.FingerP;
                    txtP.Text = frm.TypeId;
                    lblP.Text = frm.TypeName;

                    Panel pnl = txtP.Parent as Panel;
                    if (pnl != null)
                    {
                        //int indexF = int.Parse(pnl.Name.Substring(3));
                        Button btnL = null;
                        foreach (
                           Button btn in
                               pnl.Controls.OfType<Button>()
                                   .Where(btn => btn.Tag != null && btn.Tag.ToString().Contains("_2")))
                        {
                            btnL = btn;
                        }

                        Button btnR = null;
                        foreach (
                           Button btn in
                               pnl.Controls.OfType<Button>()
                                   .Where(btn => btn.Tag != null && btn.Tag.ToString().Contains("_3")))
                        {
                            btnR = btn;
                        }

                        TextBox txtL = null;
                        foreach (
                            TextBox txt in
                                pnl.Controls.OfType<TextBox>()
                                    .Where(txt => txt.Tag != null && txt.Tag.ToString().Contains("_2")))
                        {
                            txtL = txt;
                        }

                        TextBox txtR = null;
                        foreach (
                            TextBox txt in
                                pnl.Controls.OfType<TextBox>()
                                    .Where(txt => txt.Tag != null && txt.Tag.ToString().Contains("_3")))
                        {
                            txtR = txt;
                        }

                        if (btnL != null)
                        {
                            if (btnR != null)
                            {
                                if (txtL != null)
                                {
                                    if (txtR != null)
                                    {
                                        btnL.Visible = btnR.Visible = txtL.Visible = txtR.Visible = true;
                                        if (lblP.Text == @"AU" || lblP.Text == @"AT" || lblP.Text == @"AE" || lblP.Text == @"AS" ||
                                            lblP.Text == @"WX")
                                        {
                                            txtR.Text = txtL.Text = @"0";
                                            btnL.Visible = btnR.Visible = txtL.Visible = txtR.Visible = false;
                                        }
                                        else if (lblP.Text == @"UL")
                                        {
                                            if (txtL.Text != @"" && txtL.Text != @"0")
                                            {
                                                txtR.Text = @"0";
                                            }

                                            if (txtR.Text != @"" && txtR.Text != @"0")
                                            {
                                                txtL.Text = @"0";
                                            }
                                        }
                                        else
                                        {
                                            txtR.Text = @"";
                                            txtL.Text = @"";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Activate();
                Show();
            }
            catch (Exception)
            {
                //
            }
        }
        private void ptbF_DoubleClick(object sender, EventArgs e)
        {
            PictureBox ptb = (PictureBox)sender;
            
            switch (ptb.Name)
            {
                case "L1F":
                    SetTypeImage(L1F, L1P, txtL1P, lblL1P);
                    break;
                case "L2F":
                    SetTypeImage(L2F, L2P, txtL2P, lblL2P);
                    break;
                case "L3F":
                    SetTypeImage(L3F, L3P, txtL3P, lblL3P);
                    break;
                case "L4F":
                    SetTypeImage(L4F, L4P, txtL4P, lblL4P);
                    break;
                case "L5F":
                    SetTypeImage(L5F, L5P, txtL5P, lblL5P);
                    break;
                case "R1F":
                    SetTypeImage(R1F, R1P, txtR1P, lblR1P);
                    break;
                case "R2F":
                    SetTypeImage(R2F, R2P, txtR2P, lblR2P);
                    break;
                case "R3F":
                    SetTypeImage(R3F, R3P, txtR3P, lblR3P);
                    break;
                case "R4F":
                    SetTypeImage(R4F, R4P, txtR4P, lblR4P);
                    break;
                case "R5F":
                    SetTypeImage(R5F, R5P, txtR5P, lblR5P);
                    break;
            }

        }
        #endregion

        private void ZoomImage(PictureBox ptb, TextBox txt)
        {
            FrmZoom frm = new FrmZoom(ptb, txt);
            frm.ShowDialog(this);
            txt.Text = frm.Rc;
        }
        private void ptb_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var ptb = (PictureBox)sender;
            var txtname = "txt" + ptb.Name;
            foreach (var tb in Controls.Find(txtname, true).Where(tb => tb.Name == txtname).OfType<TextBox>())
            {
                ZoomImage(ptb, tb);
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            TextBox txt = sender as TextBox;
            string name = txt.Name;
            switch (name)
            {
                #region Left
                //L1
                case "txtL1L":
                    //if (txtL1L.Text.Trim() != "")
                        txtL1LResult.Text = txtL1L.Text;
                    break;
                case "txtL1R":
                    //if (txtL1R.Text.Trim() != "")
                        txtL1RResult.Text = txtL1R.Text;
                    break;
                case "txtL1P":
                    //if (txtL1P.Text.Trim() != "")
                        txtL1PResult.Text = txtL1P.Text;
                    break;
                //L2
                case "txtL2L":
                    //if (txtL2L.Text.Trim() != "")
                        txtL2LResult.Text = txtL2L.Text;
                    break;
                case "txtL2R":
                    //if (txtL2R.Text.Trim() != "")
                        txtL2RResult.Text = txtL2R.Text;
                    break;
                case "txtL2P":
                    //if (txtL2P.Text.Trim() != "")
                        txtL2PResult.Text = txtL2P.Text;
                    break;
                //L3
                case "txtL3L":
                    //if (txtL3L.Text.Trim() != "")
                        txtL3LResult.Text = txtL3L.Text;
                    break;
                case "txtL3R":
                    //if (txtL3R.Text.Trim() != "")
                        txtL3RResult.Text = txtL3R.Text;
                    break;
                case "txtL3P":
                    //if (txtL3P.Text.Trim() != "")
                        txtL3PResult.Text = txtL3P.Text;
                    break;
                
                //L4
                case "txtL4L":
                    //if (txtL4L.Text.Trim() != "")
                        txtL4LResult.Text = txtL4L.Text;
                    break;
                case "txtL4R":
                    //if (txtL4R.Text.Trim() != "")
                        txtL4RResult.Text = txtL4R.Text;
                    break;
                case "txtL4P":
                    //if (txtL4P.Text.Trim() != "")
                        txtL4PResult.Text = txtL4P.Text;
                    break;

                //L5
                case "txtL5L":
                    //if (txtL5L.Text.Trim() != "")
                        txtL5LResult.Text = txtL5L.Text;
                    break;
                case "txtL5R":
                   // if (txtL5R.Text.Trim() != "")
                        txtL5RResult.Text = txtL5R.Text;
                    break;
                case "txtL5P":
                   // if (txtL5P.Text.Trim() != "")
                        txtL5PResult.Text = txtL5P.Text;
                    break;
                #endregion Left

                #region Right
                //R1
                case "txtR1L":
                    //if (txtR1L.Text.Trim() != "")
                        txtR1LResult.Text = txtR1L.Text;
                    break;
                case "txtR1R":
                   // if (txtR1R.Text.Trim() != "")
                        txtR1RResult.Text = txtR1R.Text;
                    break;
                case "txtR1P":
                   // if (txtR1P.Text.Trim() != "")
                        txtR1PResult.Text = txtR1P.Text;
                    break;
                //R2
                case "txtR2L":
                   // if (txtR2L.Text.Trim() != "")
                        txtR2LResult.Text = txtR2L.Text;
                    break;
                case "txtR2R":
                  //  if (txtR2R.Text.Trim() != "")
                        txtR2RResult.Text = txtR2R.Text;
                    break;
                case "txtR2P":
                   // if (txtR2P.Text.Trim() != "")
                        txtR2PResult.Text = txtR2P.Text;
                    break;
                //R3
                case "txtR3L":
                   // if (txtR3L.Text.Trim() != "")
                        txtR3LResult.Text = txtR3L.Text;
                    break;
                case "txtR3R":
                   // if (txtR3R.Text.Trim() != "")
                        txtR3RResult.Text = txtR3R.Text;
                    break;
                case "txtR3P":
                   // if (txtR3P.Text.Trim() != "")
                        txtR3PResult.Text = txtR3P.Text;
                    break;

                //R4
                case "txtR4L":
                   // if (txtR4L.Text.Trim() != "")
                        txtR4LResult.Text = txtR4L.Text;
                    break;
                case "txtR4R":
                   // if (txtR4R.Text.Trim() != "")
                        txtR4RResult.Text = txtR4R.Text;
                    break;
                case "txtR4P":
                  //  if (txtR4P.Text.Trim() != "")
                        txtR4PResult.Text = txtR4P.Text;
                    break;

                //L5
                case "txtR5L":
                    //if (txtR5L.Text.Trim() != "")
                        txtR5LResult.Text = txtR5L.Text;
                    break;
                case "txtR5R":
                   // if (txtR5R.Text.Trim() != "")
                        txtR5RResult.Text = txtR5R.Text;
                    break;
                case "txtR5P":
                  //  if (txtR5P.Text.Trim() != "")
                        txtR5PResult.Text = txtR5P.Text;
                    break;
                #endregion Right
            }
        }
        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            TabPage pg = tabControl.SelectedTab;
            foreach (Control txt in gbShowResult.Panel.Controls)
            {
                if (txt is TextBox)
                    txt.BackColor = Color.White;
            }
            switch (pg.Name)
            {
                case "TabPage1":
                    txtL1LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL1RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL1PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage2":
                    txtL2LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL2RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL2PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage3":
                    txtL3LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL3RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL3PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage4":
                    txtL4LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL4RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL4PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage5":
                    txtL5LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL5RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtL5PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage6":
                    txtR1LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR1RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR1PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage7":
                    txtR2LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR2RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR2PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage8":
                    txtR3LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR3RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR3PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage9":
                    txtR4LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR4RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR4PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
                case "TabPage10":
                    txtR5LResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR5RResult.BackColor = Color.FromArgb(255, 255, 128);
                    txtR5PResult.BackColor = Color.FromArgb(255, 255, 128);
                    break;
            }
        }
        private void frmAnalysis_Load(object sender, EventArgs e)
        {
            Hide();
            FrmFlash.ShowSplash();
            Application.DoEvents();

            FillReportType();
            tabControl_Selected(tabControl.TabPages[0], null);
            GetCustomer(_customerid);
            SetButtonClickEvent();

            FrmFlash.CloseSplash();
            Activate();
            Show();
        }
    }

    public class FPIndex
    {
        public Image ImageFace { get; set; }
        public Image ImageLeftHand { get; set; }
        public Image ImageRightHand { get; set; }

        public decimal? LATDPoint { get; set; }
        public decimal? RATDPoint { get; set; }
        
        public decimal? L1LPoint { get; set; }
        public decimal? L1RPoint { get; set; }
        public string L1P { get; set; }
        public Image L1FImage { get; set; }
        public Image L1LImage { get; set; }
        public Image L1RImage { get; set; }
        public Image L1PImage { get; set; }

        public decimal? L2LPoint { get; set; }
        public decimal? L2RPoint { get; set; }
        public string L2P { get; set; }
        public Image L2FImage { get; set; }
        public Image L2LImage { get; set; }
        public Image L2RImage { get; set; }
        public Image L2PImage { get; set; }

        public decimal? L3LPoint { get; set; }
        public decimal? L3RPoint { get; set; }
        public string L3P { get; set; }
        public Image L3FImage { get; set; }
        public Image L3LImage { get; set; }
        public Image L3RImage { get; set; }
        public Image L3PImage { get; set; }

        public decimal? L4LPoint { get; set; }
        public decimal? L4RPoint { get; set; }
        public string L4P { get; set; }
        public Image L4FImage { get; set; }
        public Image L4LImage { get; set; }
        public Image L4RImage { get; set; }
        public Image L4PImage { get; set; }

        public decimal? L5LPoint { get; set; }
        public decimal? L5RPoint { get; set; }
        public string L5P { get; set; }
        public Image L5FImage { get; set; }
        public Image L5LImage { get; set; }
        public Image L5RImage { get; set; }
        public Image L5PImage { get; set; }

        public decimal? R1LPoint { get; set; }
        public decimal? R1RPoint { get; set; }
        public string R1P { get; set; }
        public Image R1FImage { get; set; }
        public Image R1LImage { get; set; }
        public Image R1RImage { get; set; }
        public Image R1PImage { get; set; }

        public decimal? R2LPoint { get; set; }
        public decimal? R2RPoint { get; set; }
        public string R2P { get; set; }
        public Image R2FImage { get; set; }
        public Image R2LImage { get; set; }
        public Image R2RImage { get; set; }
        public Image R2PImage { get; set; }

        public decimal? R3LPoint { get; set; }
        public decimal? R3RPoint { get; set; }
        public string R3P { get; set; }
        public Image R3FImage { get; set; }
        public Image R3LImage { get; set; }
        public Image R3RImage { get; set; }
        public Image R3PImage { get; set; }

        public decimal? R4LPoint { get; set; }
        public decimal? R4RPoint { get; set; }
        public string R4P { get; set; }
        public Image R4FImage { get; set; }
        public Image R4LImage { get; set; }
        public Image R4RImage { get; set; }
        public Image R4PImage { get; set; }

        public decimal? R5LPoint { get; set; }
        public decimal? R5RPoint { get; set; }
        public string R5P { get; set; }
        public Image R5FImage { get; set; }
        public Image R5LImage { get; set; }
        public Image R5RImage { get; set; }
        public Image R5PImage { get; set; }
    }
    public class FPType
    {
        public string FPTypePath 
        {
            get { return Directory.GetCurrentDirectory() + @"\FingerType\"; }
        }

        public FileInfo[] FPTypeFiles 
        {
            get
            {
                DirectoryInfo di = new DirectoryInfo(FPTypePath);
                return di.GetFiles("*.BMP");
            }
        }

        public ImageList FPTypeImages
        {
            get
            {
                ImageList FPTypeImagesCollection = new ImageList();
                foreach (FileInfo file in FPTypeFiles)
                {
                    Bitmap m = new Bitmap(file.FullName);
                    FPTypeImagesCollection.Images.Add(file.Name, m);
                }
                return FPTypeImagesCollection;
            }
        }
    }
}
