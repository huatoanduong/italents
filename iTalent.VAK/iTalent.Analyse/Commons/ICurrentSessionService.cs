﻿using System;
using System.IO;
using System.Windows.Forms;
using VAKFomula.Entity;

namespace iTalent.Analyse.Commons
{
    public static class ICurrentSessionService
    {
        public static int UserId { get; set; }
        public static string Username { get; set; }
        public static FIAgency CurAgency { get; set; }
        public static bool IsAdmin { get; set; }

        public static bool IsInstallDriver { get; set; }
        public static string Serail { get; set; }
        public static string Seckey { get; set; }
        public static string Sec7Key { get; set; }
        public static string DesDirNameTemp = Directory.GetCurrentDirectory() + @"\Temp";
        public static string DesDirNameLoadTemp = Directory.GetCurrentDirectory() + @"\Temp\LoadTemp";

        private static string _DesDirNameTemplate = null;

        public static string DesDirNameTemplate
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_DesDirNameTemplate))
                {
                    string dir = Path.GetPathRoot(Environment.SystemDirectory);
                    if (System.Environment.Is64BitOperatingSystem)
                    {
                        dir += @"Windows\SysWOW64\FConfig";
                    }
                    else
                    {
                        dir += @"Windows\System32\FConfig";
                    }
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    _DesDirNameTemplate = dir;
                }
                return _DesDirNameTemplate;
            }
        }

        private static string _DesDirNameDatabase = null;

        public static string DesDirNameDatabase
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_DesDirNameDatabase))
                {

                    string dir = Path.GetPathRoot(Environment.SystemDirectory);
                    if (System.Environment.Is64BitOperatingSystem)
                    {
                        dir += @"Windows\SysWOW64\Database";
                    }
                    else
                    {
                        dir += @"Windows\System32\Database";
                    }
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    _DesDirNameDatabase = dir + @"\FINGERPRINTANALYSIS.mdb";
                }
                return _DesDirNameDatabase;
            }
        }

        public static string DesDirNameSource { get; set; } //= DirSaveImages + @"FingerprintCustomers";
        public static string DirSaveImages { get; set; }
        public static bool VietNamLanguage { get; set; }
        
        private static DateTime DateRegiter = new DateTime(2015, 09, 13);
        private static DateTime DateExpire = DateRegiter.AddDays(7);

        public static void CheckLicense()
        {
            DateTime today = DateTime.Now;
            TimeSpan trialDay = DateExpire.Subtract(today);
            if (trialDay.Days >= 0)
            {
                MessageBox.Show(
                    VietNamLanguage
                        ? "Bạn được sử dụng thêm " + trialDay.Days +
                          " ngày!\nVui lòng liên hệ nhà cung cấp để gia hạn sử dụng chương trình!"
                        : "Free - " + trialDay.Days + " day trials!\nPlease contact supplier!",
                    VietNamLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    VietNamLanguage
                        ? "Đã hết số ngày sử dụng.\nVui lòng liên hệ nhà cung cấp."
                        : "Expiration date on software!\nPlease contact supplier.",
                    VietNamLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Environment.Exit(0);
            }

        }
    }
}