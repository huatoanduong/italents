using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIFingerRecordRepository
    {
        int Delete(FIFingerRecord obj);
        int Insert(FIFingerRecord obj);
        int Update(FIFingerRecord obj);
        List<FIFingerRecord> FindListByQuery(string query);
        List<FIFingerRecord> FindByRecordId(int reportId);
        FIFingerRecord FindByKey(Int32 ID);
        List<FIFingerRecord> FindAll();
        List<FIFingerRecord> FindByCondition(FIFingerRecordConditionForm condt);
        int DeleteByCondition(FIFingerRecordConditionForm condt);
    }

    /// <summary>
    /// Summary description for FIFingerRecord.
    /// </summary>
    public class FIFingerRecordRepository : Repository, IFIFingerRecordRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FIFingerRecordRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			Type, 
			ATDPoint, 
			RCCount, 
			FingerType, 
			ScanCheck FROM FIFingerRecord WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FIFingerRecord SET 
            			[AgencyID] = @AgencyID, 
			[ReportID] = @ReportID, 
			[Type] = @Type, 
			[ATDPoint] = @ATDPoint, 
			[RCCount] = @RCCount, 
			[FingerType] = @FingerType, 
			[ScanCheck] = @ScanCheck
            WHERE (ID = @ID)";
            _SQLUpdate = query;

            query = @"INSERT INTO FIFingerRecord (
            [AgencyID], 
			[ReportID], 
			[Type], 
			[ATDPoint], 
			[RCCount], 
			[FingerType], 
			[ScanCheck]
            ) VALUES (
            @AgencyID, 
			@ReportID, 
			@Type, 
			@ATDPoint, 
			@RCCount, 
			@FingerType, 
			@ScanCheck)";
            _SQLInsert = query;

            query = @"DELETE FROM FIFingerRecord WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			Type, 
			ATDPoint, 
			RCCount, 
			FingerType, 
			ScanCheck 
		FROM 
			FIFingerRecord";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FIFingerRecord obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FIFingerRecord obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIFingerRecord obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FIFingerRecord> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        public List<FIFingerRecord> FindByRecordId(int reportId)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();

            string query = "SELECT * FROM FIFingerRecord WHERE [ReportID] = @ReportID;";
            command.CommandText = query;
            DalTools.AddDbDataParameter(command, "ReportID", reportId, DbType.Int32);

            return FindListByCommand(command);
        }
        
        protected List<FIFingerRecord> FindListByCommand(IDbCommand command)
        {
            List<FIFingerRecord> list = new List<FIFingerRecord>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FIFingerRecord obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FIFingerRecord();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FIFingerRecord obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (int) reader["AgencyID"];
                }
                if (!(reader["ReportID"] is DBNull))
                {
                    obj.ReportID = (int) reader["ReportID"];
                }
                if (!(reader["Type"] is DBNull))
                {
                    obj.Type = (string) reader["Type"];
                }
                if (!(reader["ATDPoint"] is DBNull))
                {
                    obj.ATDPoint = (decimal) reader["ATDPoint"];
                }
                if (!(reader["RCCount"] is DBNull))
                {
                    obj.RCCount = (decimal) reader["RCCount"];
                }
                if (!(reader["FingerType"] is DBNull))
                {
                    obj.FingerType = (string) reader["FingerType"];
                }
                if (!(reader["ScanCheck"] is DBNull))
                {
                    obj.ScanCheck = (Int16) reader["ScanCheck"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FIFingerRecord FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FIFingerRecord> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIFingerRecord> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FIFingerRecord> list = FindListByCommand(command);
            return list;
        }

        public List<FIFingerRecord> FindByCondition(FIFingerRecordConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIFingerRecord " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIFingerRecord> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIFingerRecordConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIFingerRecord " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIFingerRecordConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIFingerRecordConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Type", obj.Type, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDPoint", obj.ATDPoint, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCCount", obj.RCCount, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FingerType", obj.FingerType, DbType.String);
            DalTools.AddDbDataParameter(command, "ScanCheck", obj.ScanCheck, DbType.Int16);
        }
        
        protected void FillParamToCommand(IDbCommand command, FIFingerRecord obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Type", obj.Type, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDPoint", obj.ATDPoint, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCCount", obj.RCCount, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FingerType", obj.FingerType.ToDefaultIfBlank(""), DbType.String);
            DalTools.AddDbDataParameter(command, "ScanCheck", obj.ScanCheck, DbType.Int16);
        }

        #endregion
    }
}