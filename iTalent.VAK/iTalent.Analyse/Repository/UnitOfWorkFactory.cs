﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

namespace iTalent.Analyse.Repository
{
    public static class UnitOfWorkFactory
    {
        //private static string _DesDirNameDatabase = null;
        private static string _DesDirNameDatabase = null;

        public static string DesDirNameDatabase
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_DesDirNameDatabase))
                {
                    string dir = Path.GetPathRoot(Environment.SystemDirectory);
                    if (System.Environment.Is64BitOperatingSystem)
                    {
                        dir += @"Windows\SysWOW64\Database";
                    }
                    else
                    {
                        dir += @"Windows\System32\Database";
                    }
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    _DesDirNameDatabase = dir + @"\FINGERPRINTANALYSIS.mdb";
                }
                return _DesDirNameDatabase;
            }
            set { _DesDirNameDatabase = value; }
        }

        private static readonly string SourceDb = DesDirNameDatabase;

        private static string ConnectionString
        {
            get
            {
                //return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + SourceDb +
                //       ";Persist Security Info=True;Jet OLEDB:Database Password=@Zway@!";
                return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + SourceDb +
                       ";Persist Security Info=True;Jet OLEDB:Database Password=@toro@!";
            }
        }

        private static void CheckDb()
        {
            try
            {
                if (!File.Exists(SourceDb))
                {
                    string dirsrc = @"iTalent.dll";
                    dirsrc = Path.GetFullPath(dirsrc);
                    //Directory.CreateDirectory(SourceDb.)
                    File.Copy(dirsrc, SourceDb, true);
                    //File.Copy("Properties.Resources.FINGERPRINTCLIENT", SourceDb, true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static IUnitOfWorkAsync MakeUnitOfWork()
        {
            CheckDb();
            IDbConnection connection = new OleDbConnection(ConnectionString);
            IUnitOfWorkAsync unitOfWork = new UnitOfWorkAsync(connection);
            return unitOfWork;
        }
    }
}