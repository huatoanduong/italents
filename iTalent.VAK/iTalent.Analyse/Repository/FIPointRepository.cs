using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIPointRepository
    {
        int Delete(FIPoint obj);
        int Insert(FIPoint obj);
        int Update(FIPoint obj);
        List<FIPoint> FindListByQuery(string query);
        FIPoint FindByKey(Int32 ID);
        List<FIPoint> FindByAgency(Int32 ID);
        FIPoint FindByKeyPoint(string key);
        List<FIPoint> FindAll();
        List<FIPoint> FindByCondition(FIPointConditionForm condt);
        int DeleteByCondition(FIPointConditionForm condt);
    }

    /// <summary>
    /// Summary description for FIPoint.
    /// </summary>
    public class FIPointRepository : Repository, IFIPointRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FIPointRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			PointDate, 
			PointType, 
			Point, 
			KeyPoint, 
			KeyPointAfter, 
			AddedBy, 
			IsOutDate FROM FIPoint WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FIPoint SET AgencyID = @AgencyID, 
			ReportID = @ReportID, 
			PointDate = @PointDate, 
			PointType = @PointType, 
			Point = @Point, 
			KeyPoint = @KeyPoint, 
			KeyPointAfter = @KeyPointAfter, 
			AddedBy = @AddedBy, 
			IsOutDate = @IsOutDate WHERE (ID = @ID)";
            _SQLUpdate = query;

            query = @"INSERT INTO FIPoint (
            AgencyID, 
			ReportID, 
			PointDate, 
			PointType, 
			Point, 
			KeyPoint, 
			KeyPointAfter, 
			AddedBy, 
			IsOutDate) VALUES (
            @AgencyID, 
			@ReportID, 
			@PointDate, 
			@PointType, 
			@Point, 
			@KeyPoint, 
			@KeyPointAfter, 
			@AddedBy, 
			@IsOutDate)";
            _SQLInsert = query;

            query = @"DELETE FROM FIPoint WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			PointDate, 
			PointType, 
			Point, 
			KeyPoint, 
			KeyPointAfter, 
			AddedBy, 
			IsOutDate 
		FROM 
			FIPoint";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FIPoint obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FIPoint obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIPoint obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FIPoint> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        protected List<FIPoint> FindListByCommand(IDbCommand command)
        {
            List<FIPoint> list = new List<FIPoint>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FIPoint obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FIPoint();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FIPoint obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (int) reader["AgencyID"];
                }
                if (!(reader["ReportID"] is DBNull))
                {
                    obj.ReportID = (int) reader["ReportID"];
                }
                if (!(reader["PointDate"] is DBNull))
                {
                    obj.PointDate = (DateTime) reader["PointDate"];
                }
                if (!(reader["PointType"] is DBNull))
                {
                    obj.PointType = (string) reader["PointType"];
                }
                if (!(reader["Point"] is DBNull))
                {
                    obj.Point = (decimal) reader["Point"];
                }
                if (!(reader["KeyPoint"] is DBNull))
                {
                    obj.KeyPoint = (string) reader["KeyPoint"];
                }
                if (!(reader["KeyPointAfter"] is DBNull))
                {
                    obj.KeyPointAfter = (string) reader["KeyPointAfter"];
                }
                if (!(reader["AddedBy"] is DBNull))
                {
                    obj.AddedBy = (string) reader["AddedBy"];
                }
                if (!(reader["IsOutDate"] is DBNull))
                {
                    obj.IsOutDate = (Int16) reader["IsOutDate"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FIPoint FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FIPoint> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIPoint> FindByAgency(int ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = @"SELECT ID,AgencyID, ReportID,PointDate,PointType,Point,KeyPoint,KeyPointAfter,AddedBy,IsOutDate FROM FIPoint WHERE(AgencyID = @AgencyID) ORDER BY [PointDate] DESC;";
            DalTools.AddDbDataParameter(command, "AgencyID", ID, DbType.Int32);
            List<FIPoint> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list;
        }

        public FIPoint FindByKeyPoint(string key)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = @"SELECT ID,AgencyID, ReportID,PointDate,PointType,Point,KeyPoint,KeyPointAfter,AddedBy,IsOutDate FROM FIPoint WHERE(KeyPoint = @KeyPoint);";
            DalTools.AddDbDataParameter(command, "KeyPoint", key, DbType.String);
            List<FIPoint> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIPoint> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FIPoint> list = FindListByCommand(command);
            return list;
        }

        public List<FIPoint> FindByCondition(FIPointConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIPoint " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIPoint> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIPointConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIPoint " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIPointConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIPointConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "PointDate", obj.PointDate.ToDbFormatString(), DbType.String);
            DalTools.AddDbDataParameter(command, "PointType", obj.PointType, DbType.String);
            DalTools.AddDbDataParameter(command, "Point", obj.Point, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "KeyPoint", obj.KeyPoint, DbType.String);
            DalTools.AddDbDataParameter(command, "KeyPointAfter", obj.KeyPointAfter, DbType.String);
            DalTools.AddDbDataParameter(command, "AddedBy", obj.AddedBy, DbType.String);
            DalTools.AddDbDataParameter(command, "IsOutDate", obj.IsOutDate, DbType.Int16);
        }


        protected void FillParamToCommand(IDbCommand command, FIPoint obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "PointDate", obj.PointDate.ToDbFormatString(), DbType.String);
            DalTools.AddDbDataParameter(command, "PointType", obj.PointType, DbType.String);
            DalTools.AddDbDataParameter(command, "Point", obj.Point, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "KeyPoint", obj.KeyPoint, DbType.String);
            DalTools.AddDbDataParameter(command, "KeyPointAfter", obj.KeyPointAfter, DbType.String);
            DalTools.AddDbDataParameter(command, "AddedBy", obj.AddedBy, DbType.String);
            DalTools.AddDbDataParameter(command, "IsOutDate", obj.IsOutDate, DbType.Int16);
        }

        #endregion
    }
}