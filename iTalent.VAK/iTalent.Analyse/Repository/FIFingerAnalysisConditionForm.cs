using System;
using VAKFomula.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIFingerAnalysis.
	/// </summary>
	public class FIFingerAnalysisConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _ReportID;
      private decimal _LT_BLS;
      private decimal _RT_BLS;
      private decimal _Page_BLS;
      private decimal _SAP;
      private decimal _SBP;
      private decimal _SCP;
      private decimal _SDP;
      private decimal _SER;
      private decimal _TFRC;
      private decimal _EQVP;
      private decimal _IQVP;
      private decimal _AQVP;
      private decimal _CQVP;
      private decimal _L1RCP;
      private decimal _L2RCP;
      private decimal _L3RCP;
      private decimal _L4RCP;
      private decimal _L5RCP;
      private decimal _R1RCP;
      private decimal _R2RCP;
      private decimal _R3RCP;
      private decimal _R4RCP;
      private decimal _R5RCP;
      private decimal _L1RCP1;
      private decimal _L2RCP2;
      private decimal _L3RCP3;
      private decimal _L4RCP4;
      private decimal _L5RCP5;
      private decimal _R1RCP1;
      private decimal _R2RCP2;
      private decimal _R3RCP3;
      private decimal _R4RCP4;
      private decimal _R5RCP5;
      private decimal _ATDR;
      private decimal _ATDL;
      private string _ATDRA;
      private string _ATDRS;
      private string _ATDLA;
      private string _ATDLS;
      private decimal _AC2WP;
      private decimal _AC2UP;
      private decimal _AC2RP;
      private decimal _AC2XP;
      private decimal _AC2SP;
      private decimal _RCAP;
      private decimal _RCHP;
      private decimal _RCVP;
      private decimal _T1AP;
      private decimal _T1BP;
      private decimal _M1Q;
      private decimal _M2Q;
      private decimal _M3Q;
      private decimal _M4Q;
      private decimal _M5Q;
      private decimal _M6Q;
      private decimal _M7Q;
      private decimal _M8Q;
      private decimal _L1RCB;
      private decimal _L2RCB;
      private decimal _L3RCB;
      private decimal _L4RCB;
      private decimal _L5RCB;
      private decimal _R1RCB;
      private decimal _R2RCB;
      private decimal _R3RCB;
      private decimal _R4RCB;
      private decimal _R5RCB;
      private string _Label1;
      private string _Label2;
      private string _Label3;
      private string _Label4;
      private string _Label5;
      private string _Label6;
      private string _L1T;
      private string _L2T;
      private string _L3T;
      private string _L4T;
      private string _L5T;
      private string _R1T;
      private string _R2T;
      private string _R3T;
      private string _R4T;
      private string _R5T;
      private decimal _S1BZ;
      private decimal _S2BZ;
      private decimal _S3BZ;
      private decimal _S4BZ;
      private decimal _S5BZ;
      private decimal _S6BZ;
      private decimal _S7BZ;
      private decimal _S8BZ;
      private decimal _S9BZ;
      private decimal _S10BZ;
      private decimal _S11BZ;
      private decimal _S12BZ;
      private decimal _S13BZ;
      private decimal _S14BZ;
      private decimal _S15BZ;
      private decimal _S16BZ;
      private decimal _S17BZ;
      private decimal _S18BZ;
      private string _S1BZZ;
      private string _S2BZZ;
      private string _S3BZZ;
      private string _S4BZZ;
      private string _S5BZZ;
      private string _S6BZZ;
      private string _S7BZZ;
      private string _S8BZZ;
      private string _S9BZZ;
      private string _S10BZZ;
      private string _S11BZZ;
      private string _S12BZZ;
      private string _S13BZZ;
      private string _S14BZZ;
      private string _S15BZZ;
      private string _S16BZZ;
      private string _S17BZZ;
      private string _S18BZZ;
      private decimal _M1QPR;
      private decimal _M2QPR;
      private decimal _M3QPR;
      private decimal _M4QPR;
      private decimal _M5QPR;
      private decimal _M6QPR;
      private decimal _M7QPR;
      private decimal _M8QPR;
      private decimal _FPType1;
      private decimal _FPType2;
      private decimal _FPType3;
      private decimal _FPType4;
      private decimal _FPType5;
      private decimal _FPType6;
      private decimal _FPType7;
      private decimal _FPType8;
      private decimal _FPType9;
      private decimal _FPType10;
      private decimal _FPType11;
      private decimal _FPType12;
      private decimal _FPType13;
      private decimal _FPType14;
      private decimal _FPType15;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetReportID;
	public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { 
		_ReportID = value; 
		IsSetReportID = true;
		}
      }
	public bool IsSetLT_BLS;
	public bool IsLT_BLSNullable
      { get { return true;  } }
      public decimal LT_BLS
      {
         get { return _LT_BLS;  }
         set { 
		_LT_BLS = value; 
		IsSetLT_BLS = true;
		}
      }
	public bool IsSetRT_BLS;
	public bool IsRT_BLSNullable
      { get { return true;  } }
      public decimal RT_BLS
      {
         get { return _RT_BLS;  }
         set { 
		_RT_BLS = value; 
		IsSetRT_BLS = true;
		}
      }
	public bool IsSetPage_BLS;
	public bool IsPage_BLSNullable
      { get { return true;  } }
      public decimal Page_BLS
      {
         get { return _Page_BLS;  }
         set { 
		_Page_BLS = value; 
		IsSetPage_BLS = true;
		}
      }
	public bool IsSetSAP;
	public bool IsSAPNullable
      { get { return true;  } }
      public decimal SAP
      {
         get { return _SAP;  }
         set { 
		_SAP = value; 
		IsSetSAP = true;
		}
      }
	public bool IsSetSBP;
	public bool IsSBPNullable
      { get { return true;  } }
      public decimal SBP
      {
         get { return _SBP;  }
         set { 
		_SBP = value; 
		IsSetSBP = true;
		}
      }
	public bool IsSetSCP;
	public bool IsSCPNullable
      { get { return true;  } }
      public decimal SCP
      {
         get { return _SCP;  }
         set { 
		_SCP = value; 
		IsSetSCP = true;
		}
      }
	public bool IsSetSDP;
	public bool IsSDPNullable
      { get { return true;  } }
      public decimal SDP
      {
         get { return _SDP;  }
         set { 
		_SDP = value; 
		IsSetSDP = true;
		}
      }
	public bool IsSetSER;
	public bool IsSERNullable
      { get { return true;  } }
      public decimal SER
      {
         get { return _SER;  }
         set { 
		_SER = value; 
		IsSetSER = true;
		}
      }
	public bool IsSetTFRC;
	public bool IsTFRCNullable
      { get { return true;  } }
      public decimal TFRC
      {
         get { return _TFRC;  }
         set { 
		_TFRC = value; 
		IsSetTFRC = true;
		}
      }
	public bool IsSetEQVP;
	public bool IsEQVPNullable
      { get { return true;  } }
      public decimal EQVP
      {
         get { return _EQVP;  }
         set { 
		_EQVP = value; 
		IsSetEQVP = true;
		}
      }
	public bool IsSetIQVP;
	public bool IsIQVPNullable
      { get { return true;  } }
      public decimal IQVP
      {
         get { return _IQVP;  }
         set { 
		_IQVP = value; 
		IsSetIQVP = true;
		}
      }
	public bool IsSetAQVP;
	public bool IsAQVPNullable
      { get { return true;  } }
      public decimal AQVP
      {
         get { return _AQVP;  }
         set { 
		_AQVP = value; 
		IsSetAQVP = true;
		}
      }
	public bool IsSetCQVP;
	public bool IsCQVPNullable
      { get { return true;  } }
      public decimal CQVP
      {
         get { return _CQVP;  }
         set { 
		_CQVP = value; 
		IsSetCQVP = true;
		}
      }
	public bool IsSetL1RCP;
	public bool IsL1RCPNullable
      { get { return true;  } }
      public decimal L1RCP
      {
         get { return _L1RCP;  }
         set { 
		_L1RCP = value; 
		IsSetL1RCP = true;
		}
      }
	public bool IsSetL2RCP;
	public bool IsL2RCPNullable
      { get { return true;  } }
      public decimal L2RCP
      {
         get { return _L2RCP;  }
         set { 
		_L2RCP = value; 
		IsSetL2RCP = true;
		}
      }
	public bool IsSetL3RCP;
	public bool IsL3RCPNullable
      { get { return true;  } }
      public decimal L3RCP
      {
         get { return _L3RCP;  }
         set { 
		_L3RCP = value; 
		IsSetL3RCP = true;
		}
      }
	public bool IsSetL4RCP;
	public bool IsL4RCPNullable
      { get { return true;  } }
      public decimal L4RCP
      {
         get { return _L4RCP;  }
         set { 
		_L4RCP = value; 
		IsSetL4RCP = true;
		}
      }
	public bool IsSetL5RCP;
	public bool IsL5RCPNullable
      { get { return true;  } }
      public decimal L5RCP
      {
         get { return _L5RCP;  }
         set { 
		_L5RCP = value; 
		IsSetL5RCP = true;
		}
      }
	public bool IsSetR1RCP;
	public bool IsR1RCPNullable
      { get { return true;  } }
      public decimal R1RCP
      {
         get { return _R1RCP;  }
         set { 
		_R1RCP = value; 
		IsSetR1RCP = true;
		}
      }
	public bool IsSetR2RCP;
	public bool IsR2RCPNullable
      { get { return true;  } }
      public decimal R2RCP
      {
         get { return _R2RCP;  }
         set { 
		_R2RCP = value; 
		IsSetR2RCP = true;
		}
      }
	public bool IsSetR3RCP;
	public bool IsR3RCPNullable
      { get { return true;  } }
      public decimal R3RCP
      {
         get { return _R3RCP;  }
         set { 
		_R3RCP = value; 
		IsSetR3RCP = true;
		}
      }
	public bool IsSetR4RCP;
	public bool IsR4RCPNullable
      { get { return true;  } }
      public decimal R4RCP
      {
         get { return _R4RCP;  }
         set { 
		_R4RCP = value; 
		IsSetR4RCP = true;
		}
      }
	public bool IsSetR5RCP;
	public bool IsR5RCPNullable
      { get { return true;  } }
      public decimal R5RCP
      {
         get { return _R5RCP;  }
         set { 
		_R5RCP = value; 
		IsSetR5RCP = true;
		}
      }
	public bool IsSetL1RCP1;
	public bool IsL1RCP1Nullable
      { get { return true;  } }
      public decimal L1RCP1
      {
         get { return _L1RCP1;  }
         set { 
		_L1RCP1 = value; 
		IsSetL1RCP1 = true;
		}
      }
	public bool IsSetL2RCP2;
	public bool IsL2RCP2Nullable
      { get { return true;  } }
      public decimal L2RCP2
      {
         get { return _L2RCP2;  }
         set { 
		_L2RCP2 = value; 
		IsSetL2RCP2 = true;
		}
      }
	public bool IsSetL3RCP3;
	public bool IsL3RCP3Nullable
      { get { return true;  } }
      public decimal L3RCP3
      {
         get { return _L3RCP3;  }
         set { 
		_L3RCP3 = value; 
		IsSetL3RCP3 = true;
		}
      }
	public bool IsSetL4RCP4;
	public bool IsL4RCP4Nullable
      { get { return true;  } }
      public decimal L4RCP4
      {
         get { return _L4RCP4;  }
         set { 
		_L4RCP4 = value; 
		IsSetL4RCP4 = true;
		}
      }
	public bool IsSetL5RCP5;
	public bool IsL5RCP5Nullable
      { get { return true;  } }
      public decimal L5RCP5
      {
         get { return _L5RCP5;  }
         set { 
		_L5RCP5 = value; 
		IsSetL5RCP5 = true;
		}
      }
	public bool IsSetR1RCP1;
	public bool IsR1RCP1Nullable
      { get { return true;  } }
      public decimal R1RCP1
      {
         get { return _R1RCP1;  }
         set { 
		_R1RCP1 = value; 
		IsSetR1RCP1 = true;
		}
      }
	public bool IsSetR2RCP2;
	public bool IsR2RCP2Nullable
      { get { return true;  } }
      public decimal R2RCP2
      {
         get { return _R2RCP2;  }
         set { 
		_R2RCP2 = value; 
		IsSetR2RCP2 = true;
		}
      }
	public bool IsSetR3RCP3;
	public bool IsR3RCP3Nullable
      { get { return true;  } }
      public decimal R3RCP3
      {
         get { return _R3RCP3;  }
         set { 
		_R3RCP3 = value; 
		IsSetR3RCP3 = true;
		}
      }
	public bool IsSetR4RCP4;
	public bool IsR4RCP4Nullable
      { get { return true;  } }
      public decimal R4RCP4
      {
         get { return _R4RCP4;  }
         set { 
		_R4RCP4 = value; 
		IsSetR4RCP4 = true;
		}
      }
	public bool IsSetR5RCP5;
	public bool IsR5RCP5Nullable
      { get { return true;  } }
      public decimal R5RCP5
      {
         get { return _R5RCP5;  }
         set { 
		_R5RCP5 = value; 
		IsSetR5RCP5 = true;
		}
      }
	public bool IsSetATDR;
	public bool IsATDRNullable
      { get { return true;  } }
      public decimal ATDR
      {
         get { return _ATDR;  }
         set { 
		_ATDR = value; 
		IsSetATDR = true;
		}
      }
	public bool IsSetATDL;
	public bool IsATDLNullable
      { get { return true;  } }
      public decimal ATDL
      {
         get { return _ATDL;  }
         set { 
		_ATDL = value; 
		IsSetATDL = true;
		}
      }
	public bool IsSetATDRA;
	public bool IsATDRANullable
      { get { return true;  } }
      public string ATDRA
      {
         get { return _ATDRA;  }
         set { 
		_ATDRA = value; 
		IsSetATDRA = true;
		}
      }
	public bool IsSetATDRS;
	public bool IsATDRSNullable
      { get { return true;  } }
      public string ATDRS
      {
         get { return _ATDRS;  }
         set { 
		_ATDRS = value; 
		IsSetATDRS = true;
		}
      }
	public bool IsSetATDLA;
	public bool IsATDLANullable
      { get { return true;  } }
      public string ATDLA
      {
         get { return _ATDLA;  }
         set { 
		_ATDLA = value; 
		IsSetATDLA = true;
		}
      }
	public bool IsSetATDLS;
	public bool IsATDLSNullable
      { get { return true;  } }
      public string ATDLS
      {
         get { return _ATDLS;  }
         set { 
		_ATDLS = value; 
		IsSetATDLS = true;
		}
      }
	public bool IsSetAC2WP;
	public bool IsAC2WPNullable
      { get { return true;  } }
      public decimal AC2WP
      {
         get { return _AC2WP;  }
         set { 
		_AC2WP = value; 
		IsSetAC2WP = true;
		}
      }
	public bool IsSetAC2UP;
	public bool IsAC2UPNullable
      { get { return true;  } }
      public decimal AC2UP
      {
         get { return _AC2UP;  }
         set { 
		_AC2UP = value; 
		IsSetAC2UP = true;
		}
      }
	public bool IsSetAC2RP;
	public bool IsAC2RPNullable
      { get { return true;  } }
      public decimal AC2RP
      {
         get { return _AC2RP;  }
         set { 
		_AC2RP = value; 
		IsSetAC2RP = true;
		}
      }
	public bool IsSetAC2XP;
	public bool IsAC2XPNullable
      { get { return true;  } }
      public decimal AC2XP
      {
         get { return _AC2XP;  }
         set { 
		_AC2XP = value; 
		IsSetAC2XP = true;
		}
      }
	public bool IsSetAC2SP;
	public bool IsAC2SPNullable
      { get { return true;  } }
      public decimal AC2SP
      {
         get { return _AC2SP;  }
         set { 
		_AC2SP = value; 
		IsSetAC2SP = true;
		}
      }
	public bool IsSetRCAP;
	public bool IsRCAPNullable
      { get { return true;  } }
      public decimal RCAP
      {
         get { return _RCAP;  }
         set { 
		_RCAP = value; 
		IsSetRCAP = true;
		}
      }
	public bool IsSetRCHP;
	public bool IsRCHPNullable
      { get { return true;  } }
      public decimal RCHP
      {
         get { return _RCHP;  }
         set { 
		_RCHP = value; 
		IsSetRCHP = true;
		}
      }
	public bool IsSetRCVP;
	public bool IsRCVPNullable
      { get { return true;  } }
      public decimal RCVP
      {
         get { return _RCVP;  }
         set { 
		_RCVP = value; 
		IsSetRCVP = true;
		}
      }
	public bool IsSetT1AP;
	public bool IsT1APNullable
      { get { return true;  } }
      public decimal T1AP
      {
         get { return _T1AP;  }
         set { 
		_T1AP = value; 
		IsSetT1AP = true;
		}
      }
	public bool IsSetT1BP;
	public bool IsT1BPNullable
      { get { return true;  } }
      public decimal T1BP
      {
         get { return _T1BP;  }
         set { 
		_T1BP = value; 
		IsSetT1BP = true;
		}
      }
	public bool IsSetM1Q;
	public bool IsM1QNullable
      { get { return true;  } }
      public decimal M1Q
      {
         get { return _M1Q;  }
         set { 
		_M1Q = value; 
		IsSetM1Q = true;
		}
      }
	public bool IsSetM2Q;
	public bool IsM2QNullable
      { get { return true;  } }
      public decimal M2Q
      {
         get { return _M2Q;  }
         set { 
		_M2Q = value; 
		IsSetM2Q = true;
		}
      }
	public bool IsSetM3Q;
	public bool IsM3QNullable
      { get { return true;  } }
      public decimal M3Q
      {
         get { return _M3Q;  }
         set { 
		_M3Q = value; 
		IsSetM3Q = true;
		}
      }
	public bool IsSetM4Q;
	public bool IsM4QNullable
      { get { return true;  } }
      public decimal M4Q
      {
         get { return _M4Q;  }
         set { 
		_M4Q = value; 
		IsSetM4Q = true;
		}
      }
	public bool IsSetM5Q;
	public bool IsM5QNullable
      { get { return true;  } }
      public decimal M5Q
      {
         get { return _M5Q;  }
         set { 
		_M5Q = value; 
		IsSetM5Q = true;
		}
      }
	public bool IsSetM6Q;
	public bool IsM6QNullable
      { get { return true;  } }
      public decimal M6Q
      {
         get { return _M6Q;  }
         set { 
		_M6Q = value; 
		IsSetM6Q = true;
		}
      }
	public bool IsSetM7Q;
	public bool IsM7QNullable
      { get { return true;  } }
      public decimal M7Q
      {
         get { return _M7Q;  }
         set { 
		_M7Q = value; 
		IsSetM7Q = true;
		}
      }
	public bool IsSetM8Q;
	public bool IsM8QNullable
      { get { return true;  } }
      public decimal M8Q
      {
         get { return _M8Q;  }
         set { 
		_M8Q = value; 
		IsSetM8Q = true;
		}
      }
	public bool IsSetL1RCB;
	public bool IsL1RCBNullable
      { get { return true;  } }
      public decimal L1RCB
      {
         get { return _L1RCB;  }
         set { 
		_L1RCB = value; 
		IsSetL1RCB = true;
		}
      }
	public bool IsSetL2RCB;
	public bool IsL2RCBNullable
      { get { return true;  } }
      public decimal L2RCB
      {
         get { return _L2RCB;  }
         set { 
		_L2RCB = value; 
		IsSetL2RCB = true;
		}
      }
	public bool IsSetL3RCB;
	public bool IsL3RCBNullable
      { get { return true;  } }
      public decimal L3RCB
      {
         get { return _L3RCB;  }
         set { 
		_L3RCB = value; 
		IsSetL3RCB = true;
		}
      }
	public bool IsSetL4RCB;
	public bool IsL4RCBNullable
      { get { return true;  } }
      public decimal L4RCB
      {
         get { return _L4RCB;  }
         set { 
		_L4RCB = value; 
		IsSetL4RCB = true;
		}
      }
	public bool IsSetL5RCB;
	public bool IsL5RCBNullable
      { get { return true;  } }
      public decimal L5RCB
      {
         get { return _L5RCB;  }
         set { 
		_L5RCB = value; 
		IsSetL5RCB = true;
		}
      }
	public bool IsSetR1RCB;
	public bool IsR1RCBNullable
      { get { return true;  } }
      public decimal R1RCB
      {
         get { return _R1RCB;  }
         set { 
		_R1RCB = value; 
		IsSetR1RCB = true;
		}
      }
	public bool IsSetR2RCB;
	public bool IsR2RCBNullable
      { get { return true;  } }
      public decimal R2RCB
      {
         get { return _R2RCB;  }
         set { 
		_R2RCB = value; 
		IsSetR2RCB = true;
		}
      }
	public bool IsSetR3RCB;
	public bool IsR3RCBNullable
      { get { return true;  } }
      public decimal R3RCB
      {
         get { return _R3RCB;  }
         set { 
		_R3RCB = value; 
		IsSetR3RCB = true;
		}
      }
	public bool IsSetR4RCB;
	public bool IsR4RCBNullable
      { get { return true;  } }
      public decimal R4RCB
      {
         get { return _R4RCB;  }
         set { 
		_R4RCB = value; 
		IsSetR4RCB = true;
		}
      }
	public bool IsSetR5RCB;
	public bool IsR5RCBNullable
      { get { return true;  } }
      public decimal R5RCB
      {
         get { return _R5RCB;  }
         set { 
		_R5RCB = value; 
		IsSetR5RCB = true;
		}
      }
	public bool IsSetLabel1;
	public bool IsLabel1Nullable
      { get { return true;  } }
      public string Label1
      {
         get { return _Label1;  }
         set { 
		_Label1 = value; 
		IsSetLabel1 = true;
		}
      }
	public bool IsSetLabel2;
	public bool IsLabel2Nullable
      { get { return true;  } }
      public string Label2
      {
         get { return _Label2;  }
         set { 
		_Label2 = value; 
		IsSetLabel2 = true;
		}
      }
	public bool IsSetLabel3;
	public bool IsLabel3Nullable
      { get { return true;  } }
      public string Label3
      {
         get { return _Label3;  }
         set { 
		_Label3 = value; 
		IsSetLabel3 = true;
		}
      }
	public bool IsSetLabel4;
	public bool IsLabel4Nullable
      { get { return true;  } }
      public string Label4
      {
         get { return _Label4;  }
         set { 
		_Label4 = value; 
		IsSetLabel4 = true;
		}
      }
	public bool IsSetLabel5;
	public bool IsLabel5Nullable
      { get { return true;  } }
      public string Label5
      {
         get { return _Label5;  }
         set { 
		_Label5 = value; 
		IsSetLabel5 = true;
		}
      }
	public bool IsSetLabel6;
	public bool IsLabel6Nullable
      { get { return true;  } }
      public string Label6
      {
         get { return _Label6;  }
         set { 
		_Label6 = value; 
		IsSetLabel6 = true;
		}
      }
	public bool IsSetL1T;
	public bool IsL1TNullable
      { get { return true;  } }
      public string L1T
      {
         get { return _L1T;  }
         set { 
		_L1T = value; 
		IsSetL1T = true;
		}
      }
	public bool IsSetL2T;
	public bool IsL2TNullable
      { get { return true;  } }
      public string L2T
      {
         get { return _L2T;  }
         set { 
		_L2T = value; 
		IsSetL2T = true;
		}
      }
	public bool IsSetL3T;
	public bool IsL3TNullable
      { get { return true;  } }
      public string L3T
      {
         get { return _L3T;  }
         set { 
		_L3T = value; 
		IsSetL3T = true;
		}
      }
	public bool IsSetL4T;
	public bool IsL4TNullable
      { get { return true;  } }
      public string L4T
      {
         get { return _L4T;  }
         set { 
		_L4T = value; 
		IsSetL4T = true;
		}
      }
	public bool IsSetL5T;
	public bool IsL5TNullable
      { get { return true;  } }
      public string L5T
      {
         get { return _L5T;  }
         set { 
		_L5T = value; 
		IsSetL5T = true;
		}
      }
	public bool IsSetR1T;
	public bool IsR1TNullable
      { get { return true;  } }
      public string R1T
      {
         get { return _R1T;  }
         set { 
		_R1T = value; 
		IsSetR1T = true;
		}
      }
	public bool IsSetR2T;
	public bool IsR2TNullable
      { get { return true;  } }
      public string R2T
      {
         get { return _R2T;  }
         set { 
		_R2T = value; 
		IsSetR2T = true;
		}
      }
	public bool IsSetR3T;
	public bool IsR3TNullable
      { get { return true;  } }
      public string R3T
      {
         get { return _R3T;  }
         set { 
		_R3T = value; 
		IsSetR3T = true;
		}
      }
	public bool IsSetR4T;
	public bool IsR4TNullable
      { get { return true;  } }
      public string R4T
      {
         get { return _R4T;  }
         set { 
		_R4T = value; 
		IsSetR4T = true;
		}
      }
	public bool IsSetR5T;
	public bool IsR5TNullable
      { get { return true;  } }
      public string R5T
      {
         get { return _R5T;  }
         set { 
		_R5T = value; 
		IsSetR5T = true;
		}
      }
	public bool IsSetS1BZ;
	public bool IsS1BZNullable
      { get { return true;  } }
      public decimal S1BZ
      {
         get { return _S1BZ;  }
         set { 
		_S1BZ = value; 
		IsSetS1BZ = true;
		}
      }
	public bool IsSetS2BZ;
	public bool IsS2BZNullable
      { get { return true;  } }
      public decimal S2BZ
      {
         get { return _S2BZ;  }
         set { 
		_S2BZ = value; 
		IsSetS2BZ = true;
		}
      }
	public bool IsSetS3BZ;
	public bool IsS3BZNullable
      { get { return true;  } }
      public decimal S3BZ
      {
         get { return _S3BZ;  }
         set { 
		_S3BZ = value; 
		IsSetS3BZ = true;
		}
      }
	public bool IsSetS4BZ;
	public bool IsS4BZNullable
      { get { return true;  } }
      public decimal S4BZ
      {
         get { return _S4BZ;  }
         set { 
		_S4BZ = value; 
		IsSetS4BZ = true;
		}
      }
	public bool IsSetS5BZ;
	public bool IsS5BZNullable
      { get { return true;  } }
      public decimal S5BZ
      {
         get { return _S5BZ;  }
         set { 
		_S5BZ = value; 
		IsSetS5BZ = true;
		}
      }
	public bool IsSetS6BZ;
	public bool IsS6BZNullable
      { get { return true;  } }
      public decimal S6BZ
      {
         get { return _S6BZ;  }
         set { 
		_S6BZ = value; 
		IsSetS6BZ = true;
		}
      }
	public bool IsSetS7BZ;
	public bool IsS7BZNullable
      { get { return true;  } }
      public decimal S7BZ
      {
         get { return _S7BZ;  }
         set { 
		_S7BZ = value; 
		IsSetS7BZ = true;
		}
      }
	public bool IsSetS8BZ;
	public bool IsS8BZNullable
      { get { return true;  } }
      public decimal S8BZ
      {
         get { return _S8BZ;  }
         set { 
		_S8BZ = value; 
		IsSetS8BZ = true;
		}
      }
	public bool IsSetS9BZ;
	public bool IsS9BZNullable
      { get { return true;  } }
      public decimal S9BZ
      {
         get { return _S9BZ;  }
         set { 
		_S9BZ = value; 
		IsSetS9BZ = true;
		}
      }
	public bool IsSetS10BZ;
	public bool IsS10BZNullable
      { get { return true;  } }
      public decimal S10BZ
      {
         get { return _S10BZ;  }
         set { 
		_S10BZ = value; 
		IsSetS10BZ = true;
		}
      }
	public bool IsSetS11BZ;
	public bool IsS11BZNullable
      { get { return true;  } }
      public decimal S11BZ
      {
         get { return _S11BZ;  }
         set { 
		_S11BZ = value; 
		IsSetS11BZ = true;
		}
      }
	public bool IsSetS12BZ;
	public bool IsS12BZNullable
      { get { return true;  } }
      public decimal S12BZ
      {
         get { return _S12BZ;  }
         set { 
		_S12BZ = value; 
		IsSetS12BZ = true;
		}
      }
	public bool IsSetS13BZ;
	public bool IsS13BZNullable
      { get { return true;  } }
      public decimal S13BZ
      {
         get { return _S13BZ;  }
         set { 
		_S13BZ = value; 
		IsSetS13BZ = true;
		}
      }
	public bool IsSetS14BZ;
	public bool IsS14BZNullable
      { get { return true;  } }
      public decimal S14BZ
      {
         get { return _S14BZ;  }
         set { 
		_S14BZ = value; 
		IsSetS14BZ = true;
		}
      }
	public bool IsSetS15BZ;
	public bool IsS15BZNullable
      { get { return true;  } }
      public decimal S15BZ
      {
         get { return _S15BZ;  }
         set { 
		_S15BZ = value; 
		IsSetS15BZ = true;
		}
      }
	public bool IsSetS16BZ;
	public bool IsS16BZNullable
      { get { return true;  } }
      public decimal S16BZ
      {
         get { return _S16BZ;  }
         set { 
		_S16BZ = value; 
		IsSetS16BZ = true;
		}
      }
	public bool IsSetS17BZ;
	public bool IsS17BZNullable
      { get { return true;  } }
      public decimal S17BZ
      {
         get { return _S17BZ;  }
         set { 
		_S17BZ = value; 
		IsSetS17BZ = true;
		}
      }
	public bool IsSetS18BZ;
	public bool IsS18BZNullable
      { get { return true;  } }
      public decimal S18BZ
      {
         get { return _S18BZ;  }
         set { 
		_S18BZ = value; 
		IsSetS18BZ = true;
		}
      }
	public bool IsSetS1BZZ;
	public bool IsS1BZZNullable
      { get { return true;  } }
      public string S1BZZ
      {
         get { return _S1BZZ;  }
         set { 
		_S1BZZ = value; 
		IsSetS1BZZ = true;
		}
      }
	public bool IsSetS2BZZ;
	public bool IsS2BZZNullable
      { get { return true;  } }
      public string S2BZZ
      {
         get { return _S2BZZ;  }
         set { 
		_S2BZZ = value; 
		IsSetS2BZZ = true;
		}
      }
	public bool IsSetS3BZZ;
	public bool IsS3BZZNullable
      { get { return true;  } }
      public string S3BZZ
      {
         get { return _S3BZZ;  }
         set { 
		_S3BZZ = value; 
		IsSetS3BZZ = true;
		}
      }
	public bool IsSetS4BZZ;
	public bool IsS4BZZNullable
      { get { return true;  } }
      public string S4BZZ
      {
         get { return _S4BZZ;  }
         set { 
		_S4BZZ = value; 
		IsSetS4BZZ = true;
		}
      }
	public bool IsSetS5BZZ;
	public bool IsS5BZZNullable
      { get { return true;  } }
      public string S5BZZ
      {
         get { return _S5BZZ;  }
         set { 
		_S5BZZ = value; 
		IsSetS5BZZ = true;
		}
      }
	public bool IsSetS6BZZ;
	public bool IsS6BZZNullable
      { get { return true;  } }
      public string S6BZZ
      {
         get { return _S6BZZ;  }
         set { 
		_S6BZZ = value; 
		IsSetS6BZZ = true;
		}
      }
	public bool IsSetS7BZZ;
	public bool IsS7BZZNullable
      { get { return true;  } }
      public string S7BZZ
      {
         get { return _S7BZZ;  }
         set { 
		_S7BZZ = value; 
		IsSetS7BZZ = true;
		}
      }
	public bool IsSetS8BZZ;
	public bool IsS8BZZNullable
      { get { return true;  } }
      public string S8BZZ
      {
         get { return _S8BZZ;  }
         set { 
		_S8BZZ = value; 
		IsSetS8BZZ = true;
		}
      }
	public bool IsSetS9BZZ;
	public bool IsS9BZZNullable
      { get { return true;  } }
      public string S9BZZ
      {
         get { return _S9BZZ;  }
         set { 
		_S9BZZ = value; 
		IsSetS9BZZ = true;
		}
      }
	public bool IsSetS10BZZ;
	public bool IsS10BZZNullable
      { get { return true;  } }
      public string S10BZZ
      {
         get { return _S10BZZ;  }
         set { 
		_S10BZZ = value; 
		IsSetS10BZZ = true;
		}
      }
	public bool IsSetS11BZZ;
	public bool IsS11BZZNullable
      { get { return true;  } }
      public string S11BZZ
      {
         get { return _S11BZZ;  }
         set { 
		_S11BZZ = value; 
		IsSetS11BZZ = true;
		}
      }
	public bool IsSetS12BZZ;
	public bool IsS12BZZNullable
      { get { return true;  } }
      public string S12BZZ
      {
         get { return _S12BZZ;  }
         set { 
		_S12BZZ = value; 
		IsSetS12BZZ = true;
		}
      }
	public bool IsSetS13BZZ;
	public bool IsS13BZZNullable
      { get { return true;  } }
      public string S13BZZ
      {
         get { return _S13BZZ;  }
         set { 
		_S13BZZ = value; 
		IsSetS13BZZ = true;
		}
      }
	public bool IsSetS14BZZ;
	public bool IsS14BZZNullable
      { get { return true;  } }
      public string S14BZZ
      {
         get { return _S14BZZ;  }
         set { 
		_S14BZZ = value; 
		IsSetS14BZZ = true;
		}
      }
	public bool IsSetS15BZZ;
	public bool IsS15BZZNullable
      { get { return true;  } }
      public string S15BZZ
      {
         get { return _S15BZZ;  }
         set { 
		_S15BZZ = value; 
		IsSetS15BZZ = true;
		}
      }
	public bool IsSetS16BZZ;
	public bool IsS16BZZNullable
      { get { return true;  } }
      public string S16BZZ
      {
         get { return _S16BZZ;  }
         set { 
		_S16BZZ = value; 
		IsSetS16BZZ = true;
		}
      }
	public bool IsSetS17BZZ;
	public bool IsS17BZZNullable
      { get { return true;  } }
      public string S17BZZ
      {
         get { return _S17BZZ;  }
         set { 
		_S17BZZ = value; 
		IsSetS17BZZ = true;
		}
      }
	public bool IsSetS18BZZ;
	public bool IsS18BZZNullable
      { get { return true;  } }
      public string S18BZZ
      {
         get { return _S18BZZ;  }
         set { 
		_S18BZZ = value; 
		IsSetS18BZZ = true;
		}
      }
	public bool IsSetM1QPR;
	public bool IsM1QPRNullable
      { get { return true;  } }
      public decimal M1QPR
      {
         get { return _M1QPR;  }
         set { 
		_M1QPR = value; 
		IsSetM1QPR = true;
		}
      }
	public bool IsSetM2QPR;
	public bool IsM2QPRNullable
      { get { return true;  } }
      public decimal M2QPR
      {
         get { return _M2QPR;  }
         set { 
		_M2QPR = value; 
		IsSetM2QPR = true;
		}
      }
	public bool IsSetM3QPR;
	public bool IsM3QPRNullable
      { get { return true;  } }
      public decimal M3QPR
      {
         get { return _M3QPR;  }
         set { 
		_M3QPR = value; 
		IsSetM3QPR = true;
		}
      }
	public bool IsSetM4QPR;
	public bool IsM4QPRNullable
      { get { return true;  } }
      public decimal M4QPR
      {
         get { return _M4QPR;  }
         set { 
		_M4QPR = value; 
		IsSetM4QPR = true;
		}
      }
	public bool IsSetM5QPR;
	public bool IsM5QPRNullable
      { get { return true;  } }
      public decimal M5QPR
      {
         get { return _M5QPR;  }
         set { 
		_M5QPR = value; 
		IsSetM5QPR = true;
		}
      }
	public bool IsSetM6QPR;
	public bool IsM6QPRNullable
      { get { return true;  } }
      public decimal M6QPR
      {
         get { return _M6QPR;  }
         set { 
		_M6QPR = value; 
		IsSetM6QPR = true;
		}
      }
	public bool IsSetM7QPR;
	public bool IsM7QPRNullable
      { get { return true;  } }
      public decimal M7QPR
      {
         get { return _M7QPR;  }
         set { 
		_M7QPR = value; 
		IsSetM7QPR = true;
		}
      }
	public bool IsSetM8QPR;
	public bool IsM8QPRNullable
      { get { return true;  } }
      public decimal M8QPR
      {
         get { return _M8QPR;  }
         set { 
		_M8QPR = value; 
		IsSetM8QPR = true;
		}
      }
	public bool IsSetFPType1;
	public bool IsFPType1Nullable
      { get { return true;  } }
      public decimal FPType1
      {
         get { return _FPType1;  }
         set { 
		_FPType1 = value; 
		IsSetFPType1 = true;
		}
      }
	public bool IsSetFPType2;
	public bool IsFPType2Nullable
      { get { return true;  } }
      public decimal FPType2
      {
         get { return _FPType2;  }
         set { 
		_FPType2 = value; 
		IsSetFPType2 = true;
		}
      }
	public bool IsSetFPType3;
	public bool IsFPType3Nullable
      { get { return true;  } }
      public decimal FPType3
      {
         get { return _FPType3;  }
         set { 
		_FPType3 = value; 
		IsSetFPType3 = true;
		}
      }
	public bool IsSetFPType4;
	public bool IsFPType4Nullable
      { get { return true;  } }
      public decimal FPType4
      {
         get { return _FPType4;  }
         set { 
		_FPType4 = value; 
		IsSetFPType4 = true;
		}
      }
	public bool IsSetFPType5;
	public bool IsFPType5Nullable
      { get { return true;  } }
      public decimal FPType5
      {
         get { return _FPType5;  }
         set { 
		_FPType5 = value; 
		IsSetFPType5 = true;
		}
      }
	public bool IsSetFPType6;
	public bool IsFPType6Nullable
      { get { return true;  } }
      public decimal FPType6
      {
         get { return _FPType6;  }
         set { 
		_FPType6 = value; 
		IsSetFPType6 = true;
		}
      }
	public bool IsSetFPType7;
	public bool IsFPType7Nullable
      { get { return true;  } }
      public decimal FPType7
      {
         get { return _FPType7;  }
         set { 
		_FPType7 = value; 
		IsSetFPType7 = true;
		}
      }
	public bool IsSetFPType8;
	public bool IsFPType8Nullable
      { get { return true;  } }
      public decimal FPType8
      {
         get { return _FPType8;  }
         set { 
		_FPType8 = value; 
		IsSetFPType8 = true;
		}
      }
	public bool IsSetFPType9;
	public bool IsFPType9Nullable
      { get { return true;  } }
      public decimal FPType9
      {
         get { return _FPType9;  }
         set { 
		_FPType9 = value; 
		IsSetFPType9 = true;
		}
      }
	public bool IsSetFPType10;
	public bool IsFPType10Nullable
      { get { return true;  } }
      public decimal FPType10
      {
         get { return _FPType10;  }
         set { 
		_FPType10 = value; 
		IsSetFPType10 = true;
		}
      }
	public bool IsSetFPType11;
	public bool IsFPType11Nullable
      { get { return true;  } }
      public decimal FPType11
      {
         get { return _FPType11;  }
         set { 
		_FPType11 = value; 
		IsSetFPType11 = true;
		}
      }
	public bool IsSetFPType12;
	public bool IsFPType12Nullable
      { get { return true;  } }
      public decimal FPType12
      {
         get { return _FPType12;  }
         set { 
		_FPType12 = value; 
		IsSetFPType12 = true;
		}
      }
	public bool IsSetFPType13;
	public bool IsFPType13Nullable
      { get { return true;  } }
      public decimal FPType13
      {
         get { return _FPType13;  }
         set { 
		_FPType13 = value; 
		IsSetFPType13 = true;
		}
      }
	public bool IsSetFPType14;
	public bool IsFPType14Nullable
      { get { return true;  } }
      public decimal FPType14
      {
         get { return _FPType14;  }
         set { 
		_FPType14 = value; 
		IsSetFPType14 = true;
		}
      }
	public bool IsSetFPType15;
	public bool IsFPType15Nullable
      { get { return true;  } }
      public decimal FPType15
      {
         get { return _FPType15;  }
         set { 
		_FPType15 = value; 
		IsSetFPType15 = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIFingerAnalysisConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _ReportID = EmptyValues.v_Int32;
	IsSetReportID = false;
         _LT_BLS = EmptyValues.v_decimal;
	IsSetLT_BLS = false;
         _RT_BLS = EmptyValues.v_decimal;
	IsSetRT_BLS = false;
         _Page_BLS = EmptyValues.v_decimal;
	IsSetPage_BLS = false;
         _SAP = EmptyValues.v_decimal;
	IsSetSAP = false;
         _SBP = EmptyValues.v_decimal;
	IsSetSBP = false;
         _SCP = EmptyValues.v_decimal;
	IsSetSCP = false;
         _SDP = EmptyValues.v_decimal;
	IsSetSDP = false;
         _SER = EmptyValues.v_decimal;
	IsSetSER = false;
         _TFRC = EmptyValues.v_decimal;
	IsSetTFRC = false;
         _EQVP = EmptyValues.v_decimal;
	IsSetEQVP = false;
         _IQVP = EmptyValues.v_decimal;
	IsSetIQVP = false;
         _AQVP = EmptyValues.v_decimal;
	IsSetAQVP = false;
         _CQVP = EmptyValues.v_decimal;
	IsSetCQVP = false;
         _L1RCP = EmptyValues.v_decimal;
	IsSetL1RCP = false;
         _L2RCP = EmptyValues.v_decimal;
	IsSetL2RCP = false;
         _L3RCP = EmptyValues.v_decimal;
	IsSetL3RCP = false;
         _L4RCP = EmptyValues.v_decimal;
	IsSetL4RCP = false;
         _L5RCP = EmptyValues.v_decimal;
	IsSetL5RCP = false;
         _R1RCP = EmptyValues.v_decimal;
	IsSetR1RCP = false;
         _R2RCP = EmptyValues.v_decimal;
	IsSetR2RCP = false;
         _R3RCP = EmptyValues.v_decimal;
	IsSetR3RCP = false;
         _R4RCP = EmptyValues.v_decimal;
	IsSetR4RCP = false;
         _R5RCP = EmptyValues.v_decimal;
	IsSetR5RCP = false;
         _L1RCP1 = EmptyValues.v_decimal;
	IsSetL1RCP1 = false;
         _L2RCP2 = EmptyValues.v_decimal;
	IsSetL2RCP2 = false;
         _L3RCP3 = EmptyValues.v_decimal;
	IsSetL3RCP3 = false;
         _L4RCP4 = EmptyValues.v_decimal;
	IsSetL4RCP4 = false;
         _L5RCP5 = EmptyValues.v_decimal;
	IsSetL5RCP5 = false;
         _R1RCP1 = EmptyValues.v_decimal;
	IsSetR1RCP1 = false;
         _R2RCP2 = EmptyValues.v_decimal;
	IsSetR2RCP2 = false;
         _R3RCP3 = EmptyValues.v_decimal;
	IsSetR3RCP3 = false;
         _R4RCP4 = EmptyValues.v_decimal;
	IsSetR4RCP4 = false;
         _R5RCP5 = EmptyValues.v_decimal;
	IsSetR5RCP5 = false;
         _ATDR = EmptyValues.v_decimal;
	IsSetATDR = false;
         _ATDL = EmptyValues.v_decimal;
	IsSetATDL = false;
         _ATDRA = EmptyValues.v_string;
	IsSetATDRA = false;
         _ATDRS = EmptyValues.v_string;
	IsSetATDRS = false;
         _ATDLA = EmptyValues.v_string;
	IsSetATDLA = false;
         _ATDLS = EmptyValues.v_string;
	IsSetATDLS = false;
         _AC2WP = EmptyValues.v_decimal;
	IsSetAC2WP = false;
         _AC2UP = EmptyValues.v_decimal;
	IsSetAC2UP = false;
         _AC2RP = EmptyValues.v_decimal;
	IsSetAC2RP = false;
         _AC2XP = EmptyValues.v_decimal;
	IsSetAC2XP = false;
         _AC2SP = EmptyValues.v_decimal;
	IsSetAC2SP = false;
         _RCAP = EmptyValues.v_decimal;
	IsSetRCAP = false;
         _RCHP = EmptyValues.v_decimal;
	IsSetRCHP = false;
         _RCVP = EmptyValues.v_decimal;
	IsSetRCVP = false;
         _T1AP = EmptyValues.v_decimal;
	IsSetT1AP = false;
         _T1BP = EmptyValues.v_decimal;
	IsSetT1BP = false;
         _M1Q = EmptyValues.v_decimal;
	IsSetM1Q = false;
         _M2Q = EmptyValues.v_decimal;
	IsSetM2Q = false;
         _M3Q = EmptyValues.v_decimal;
	IsSetM3Q = false;
         _M4Q = EmptyValues.v_decimal;
	IsSetM4Q = false;
         _M5Q = EmptyValues.v_decimal;
	IsSetM5Q = false;
         _M6Q = EmptyValues.v_decimal;
	IsSetM6Q = false;
         _M7Q = EmptyValues.v_decimal;
	IsSetM7Q = false;
         _M8Q = EmptyValues.v_decimal;
	IsSetM8Q = false;
         _L1RCB = EmptyValues.v_decimal;
	IsSetL1RCB = false;
         _L2RCB = EmptyValues.v_decimal;
	IsSetL2RCB = false;
         _L3RCB = EmptyValues.v_decimal;
	IsSetL3RCB = false;
         _L4RCB = EmptyValues.v_decimal;
	IsSetL4RCB = false;
         _L5RCB = EmptyValues.v_decimal;
	IsSetL5RCB = false;
         _R1RCB = EmptyValues.v_decimal;
	IsSetR1RCB = false;
         _R2RCB = EmptyValues.v_decimal;
	IsSetR2RCB = false;
         _R3RCB = EmptyValues.v_decimal;
	IsSetR3RCB = false;
         _R4RCB = EmptyValues.v_decimal;
	IsSetR4RCB = false;
         _R5RCB = EmptyValues.v_decimal;
	IsSetR5RCB = false;
         _Label1 = EmptyValues.v_string;
	IsSetLabel1 = false;
         _Label2 = EmptyValues.v_string;
	IsSetLabel2 = false;
         _Label3 = EmptyValues.v_string;
	IsSetLabel3 = false;
         _Label4 = EmptyValues.v_string;
	IsSetLabel4 = false;
         _Label5 = EmptyValues.v_string;
	IsSetLabel5 = false;
         _Label6 = EmptyValues.v_string;
	IsSetLabel6 = false;
         _L1T = EmptyValues.v_string;
	IsSetL1T = false;
         _L2T = EmptyValues.v_string;
	IsSetL2T = false;
         _L3T = EmptyValues.v_string;
	IsSetL3T = false;
         _L4T = EmptyValues.v_string;
	IsSetL4T = false;
         _L5T = EmptyValues.v_string;
	IsSetL5T = false;
         _R1T = EmptyValues.v_string;
	IsSetR1T = false;
         _R2T = EmptyValues.v_string;
	IsSetR2T = false;
         _R3T = EmptyValues.v_string;
	IsSetR3T = false;
         _R4T = EmptyValues.v_string;
	IsSetR4T = false;
         _R5T = EmptyValues.v_string;
	IsSetR5T = false;
         _S1BZ = EmptyValues.v_decimal;
	IsSetS1BZ = false;
         _S2BZ = EmptyValues.v_decimal;
	IsSetS2BZ = false;
         _S3BZ = EmptyValues.v_decimal;
	IsSetS3BZ = false;
         _S4BZ = EmptyValues.v_decimal;
	IsSetS4BZ = false;
         _S5BZ = EmptyValues.v_decimal;
	IsSetS5BZ = false;
         _S6BZ = EmptyValues.v_decimal;
	IsSetS6BZ = false;
         _S7BZ = EmptyValues.v_decimal;
	IsSetS7BZ = false;
         _S8BZ = EmptyValues.v_decimal;
	IsSetS8BZ = false;
         _S9BZ = EmptyValues.v_decimal;
	IsSetS9BZ = false;
         _S10BZ = EmptyValues.v_decimal;
	IsSetS10BZ = false;
         _S11BZ = EmptyValues.v_decimal;
	IsSetS11BZ = false;
         _S12BZ = EmptyValues.v_decimal;
	IsSetS12BZ = false;
         _S13BZ = EmptyValues.v_decimal;
	IsSetS13BZ = false;
         _S14BZ = EmptyValues.v_decimal;
	IsSetS14BZ = false;
         _S15BZ = EmptyValues.v_decimal;
	IsSetS15BZ = false;
         _S16BZ = EmptyValues.v_decimal;
	IsSetS16BZ = false;
         _S17BZ = EmptyValues.v_decimal;
	IsSetS17BZ = false;
         _S18BZ = EmptyValues.v_decimal;
	IsSetS18BZ = false;
         _S1BZZ = EmptyValues.v_string;
	IsSetS1BZZ = false;
         _S2BZZ = EmptyValues.v_string;
	IsSetS2BZZ = false;
         _S3BZZ = EmptyValues.v_string;
	IsSetS3BZZ = false;
         _S4BZZ = EmptyValues.v_string;
	IsSetS4BZZ = false;
         _S5BZZ = EmptyValues.v_string;
	IsSetS5BZZ = false;
         _S6BZZ = EmptyValues.v_string;
	IsSetS6BZZ = false;
         _S7BZZ = EmptyValues.v_string;
	IsSetS7BZZ = false;
         _S8BZZ = EmptyValues.v_string;
	IsSetS8BZZ = false;
         _S9BZZ = EmptyValues.v_string;
	IsSetS9BZZ = false;
         _S10BZZ = EmptyValues.v_string;
	IsSetS10BZZ = false;
         _S11BZZ = EmptyValues.v_string;
	IsSetS11BZZ = false;
         _S12BZZ = EmptyValues.v_string;
	IsSetS12BZZ = false;
         _S13BZZ = EmptyValues.v_string;
	IsSetS13BZZ = false;
         _S14BZZ = EmptyValues.v_string;
	IsSetS14BZZ = false;
         _S15BZZ = EmptyValues.v_string;
	IsSetS15BZZ = false;
         _S16BZZ = EmptyValues.v_string;
	IsSetS16BZZ = false;
         _S17BZZ = EmptyValues.v_string;
	IsSetS17BZZ = false;
         _S18BZZ = EmptyValues.v_string;
	IsSetS18BZZ = false;
         _M1QPR = EmptyValues.v_decimal;
	IsSetM1QPR = false;
         _M2QPR = EmptyValues.v_decimal;
	IsSetM2QPR = false;
         _M3QPR = EmptyValues.v_decimal;
	IsSetM3QPR = false;
         _M4QPR = EmptyValues.v_decimal;
	IsSetM4QPR = false;
         _M5QPR = EmptyValues.v_decimal;
	IsSetM5QPR = false;
         _M6QPR = EmptyValues.v_decimal;
	IsSetM6QPR = false;
         _M7QPR = EmptyValues.v_decimal;
	IsSetM7QPR = false;
         _M8QPR = EmptyValues.v_decimal;
	IsSetM8QPR = false;
         _FPType1 = EmptyValues.v_decimal;
	IsSetFPType1 = false;
         _FPType2 = EmptyValues.v_decimal;
	IsSetFPType2 = false;
         _FPType3 = EmptyValues.v_decimal;
	IsSetFPType3 = false;
         _FPType4 = EmptyValues.v_decimal;
	IsSetFPType4 = false;
         _FPType5 = EmptyValues.v_decimal;
	IsSetFPType5 = false;
         _FPType6 = EmptyValues.v_decimal;
	IsSetFPType6 = false;
         _FPType7 = EmptyValues.v_decimal;
	IsSetFPType7 = false;
         _FPType8 = EmptyValues.v_decimal;
	IsSetFPType8 = false;
         _FPType9 = EmptyValues.v_decimal;
	IsSetFPType9 = false;
         _FPType10 = EmptyValues.v_decimal;
	IsSetFPType10 = false;
         _FPType11 = EmptyValues.v_decimal;
	IsSetFPType11 = false;
         _FPType12 = EmptyValues.v_decimal;
	IsSetFPType12 = false;
         _FPType13 = EmptyValues.v_decimal;
	IsSetFPType13 = false;
         _FPType14 = EmptyValues.v_decimal;
	IsSetFPType14 = false;
         _FPType15 = EmptyValues.v_decimal;
	IsSetFPType15 = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetReportID)
            {
                s += " AND ReportID = @ReportID ";
            }
            if (IsSetLT_BLS)
            {
                s += " AND LT_BLS = @LT_BLS ";
            }
            if (IsSetRT_BLS)
            {
                s += " AND RT_BLS = @RT_BLS ";
            }
            if (IsSetPage_BLS)
            {
                s += " AND Page_BLS = @Page_BLS ";
            }
            if (IsSetSAP)
            {
                s += " AND SAP = @SAP ";
            }
            if (IsSetSBP)
            {
                s += " AND SBP = @SBP ";
            }
            if (IsSetSCP)
            {
                s += " AND SCP = @SCP ";
            }
            if (IsSetSDP)
            {
                s += " AND SDP = @SDP ";
            }
            if (IsSetSER)
            {
                s += " AND SER = @SER ";
            }
            if (IsSetTFRC)
            {
                s += " AND TFRC = @TFRC ";
            }
            if (IsSetEQVP)
            {
                s += " AND EQVP = @EQVP ";
            }
            if (IsSetIQVP)
            {
                s += " AND IQVP = @IQVP ";
            }
            if (IsSetAQVP)
            {
                s += " AND AQVP = @AQVP ";
            }
            if (IsSetCQVP)
            {
                s += " AND CQVP = @CQVP ";
            }
            if (IsSetL1RCP)
            {
                s += " AND L1RCP = @L1RCP ";
            }
            if (IsSetL2RCP)
            {
                s += " AND L2RCP = @L2RCP ";
            }
            if (IsSetL3RCP)
            {
                s += " AND L3RCP = @L3RCP ";
            }
            if (IsSetL4RCP)
            {
                s += " AND L4RCP = @L4RCP ";
            }
            if (IsSetL5RCP)
            {
                s += " AND L5RCP = @L5RCP ";
            }
            if (IsSetR1RCP)
            {
                s += " AND R1RCP = @R1RCP ";
            }
            if (IsSetR2RCP)
            {
                s += " AND R2RCP = @R2RCP ";
            }
            if (IsSetR3RCP)
            {
                s += " AND R3RCP = @R3RCP ";
            }
            if (IsSetR4RCP)
            {
                s += " AND R4RCP = @R4RCP ";
            }
            if (IsSetR5RCP)
            {
                s += " AND R5RCP = @R5RCP ";
            }
            if (IsSetL1RCP1)
            {
                s += " AND L1RCP1 = @L1RCP1 ";
            }
            if (IsSetL2RCP2)
            {
                s += " AND L2RCP2 = @L2RCP2 ";
            }
            if (IsSetL3RCP3)
            {
                s += " AND L3RCP3 = @L3RCP3 ";
            }
            if (IsSetL4RCP4)
            {
                s += " AND L4RCP4 = @L4RCP4 ";
            }
            if (IsSetL5RCP5)
            {
                s += " AND L5RCP5 = @L5RCP5 ";
            }
            if (IsSetR1RCP1)
            {
                s += " AND R1RCP1 = @R1RCP1 ";
            }
            if (IsSetR2RCP2)
            {
                s += " AND R2RCP2 = @R2RCP2 ";
            }
            if (IsSetR3RCP3)
            {
                s += " AND R3RCP3 = @R3RCP3 ";
            }
            if (IsSetR4RCP4)
            {
                s += " AND R4RCP4 = @R4RCP4 ";
            }
            if (IsSetR5RCP5)
            {
                s += " AND R5RCP5 = @R5RCP5 ";
            }
            if (IsSetATDR)
            {
                s += " AND ATDR = @ATDR ";
            }
            if (IsSetATDL)
            {
                s += " AND ATDL = @ATDL ";
            }
            if (IsSetATDRA)
            {
                s += " AND ATDRA = @ATDRA ";
            }
            if (IsSetATDRS)
            {
                s += " AND ATDRS = @ATDRS ";
            }
            if (IsSetATDLA)
            {
                s += " AND ATDLA = @ATDLA ";
            }
            if (IsSetATDLS)
            {
                s += " AND ATDLS = @ATDLS ";
            }
            if (IsSetAC2WP)
            {
                s += " AND AC2WP = @AC2WP ";
            }
            if (IsSetAC2UP)
            {
                s += " AND AC2UP = @AC2UP ";
            }
            if (IsSetAC2RP)
            {
                s += " AND AC2RP = @AC2RP ";
            }
            if (IsSetAC2XP)
            {
                s += " AND AC2XP = @AC2XP ";
            }
            if (IsSetAC2SP)
            {
                s += " AND AC2SP = @AC2SP ";
            }
            if (IsSetRCAP)
            {
                s += " AND RCAP = @RCAP ";
            }
            if (IsSetRCHP)
            {
                s += " AND RCHP = @RCHP ";
            }
            if (IsSetRCVP)
            {
                s += " AND RCVP = @RCVP ";
            }
            if (IsSetT1AP)
            {
                s += " AND T1AP = @T1AP ";
            }
            if (IsSetT1BP)
            {
                s += " AND T1BP = @T1BP ";
            }
            if (IsSetM1Q)
            {
                s += " AND M1Q = @M1Q ";
            }
            if (IsSetM2Q)
            {
                s += " AND M2Q = @M2Q ";
            }
            if (IsSetM3Q)
            {
                s += " AND M3Q = @M3Q ";
            }
            if (IsSetM4Q)
            {
                s += " AND M4Q = @M4Q ";
            }
            if (IsSetM5Q)
            {
                s += " AND M5Q = @M5Q ";
            }
            if (IsSetM6Q)
            {
                s += " AND M6Q = @M6Q ";
            }
            if (IsSetM7Q)
            {
                s += " AND M7Q = @M7Q ";
            }
            if (IsSetM8Q)
            {
                s += " AND M8Q = @M8Q ";
            }
            if (IsSetL1RCB)
            {
                s += " AND L1RCB = @L1RCB ";
            }
            if (IsSetL2RCB)
            {
                s += " AND L2RCB = @L2RCB ";
            }
            if (IsSetL3RCB)
            {
                s += " AND L3RCB = @L3RCB ";
            }
            if (IsSetL4RCB)
            {
                s += " AND L4RCB = @L4RCB ";
            }
            if (IsSetL5RCB)
            {
                s += " AND L5RCB = @L5RCB ";
            }
            if (IsSetR1RCB)
            {
                s += " AND R1RCB = @R1RCB ";
            }
            if (IsSetR2RCB)
            {
                s += " AND R2RCB = @R2RCB ";
            }
            if (IsSetR3RCB)
            {
                s += " AND R3RCB = @R3RCB ";
            }
            if (IsSetR4RCB)
            {
                s += " AND R4RCB = @R4RCB ";
            }
            if (IsSetR5RCB)
            {
                s += " AND R5RCB = @R5RCB ";
            }
            if (IsSetLabel1)
            {
                s += " AND Label1 = @Label1 ";
            }
            if (IsSetLabel2)
            {
                s += " AND Label2 = @Label2 ";
            }
            if (IsSetLabel3)
            {
                s += " AND Label3 = @Label3 ";
            }
            if (IsSetLabel4)
            {
                s += " AND Label4 = @Label4 ";
            }
            if (IsSetLabel5)
            {
                s += " AND Label5 = @Label5 ";
            }
            if (IsSetLabel6)
            {
                s += " AND Label6 = @Label6 ";
            }
            if (IsSetL1T)
            {
                s += " AND L1T = @L1T ";
            }
            if (IsSetL2T)
            {
                s += " AND L2T = @L2T ";
            }
            if (IsSetL3T)
            {
                s += " AND L3T = @L3T ";
            }
            if (IsSetL4T)
            {
                s += " AND L4T = @L4T ";
            }
            if (IsSetL5T)
            {
                s += " AND L5T = @L5T ";
            }
            if (IsSetR1T)
            {
                s += " AND R1T = @R1T ";
            }
            if (IsSetR2T)
            {
                s += " AND R2T = @R2T ";
            }
            if (IsSetR3T)
            {
                s += " AND R3T = @R3T ";
            }
            if (IsSetR4T)
            {
                s += " AND R4T = @R4T ";
            }
            if (IsSetR5T)
            {
                s += " AND R5T = @R5T ";
            }
            if (IsSetS1BZ)
            {
                s += " AND S1BZ = @S1BZ ";
            }
            if (IsSetS2BZ)
            {
                s += " AND S2BZ = @S2BZ ";
            }
            if (IsSetS3BZ)
            {
                s += " AND S3BZ = @S3BZ ";
            }
            if (IsSetS4BZ)
            {
                s += " AND S4BZ = @S4BZ ";
            }
            if (IsSetS5BZ)
            {
                s += " AND S5BZ = @S5BZ ";
            }
            if (IsSetS6BZ)
            {
                s += " AND S6BZ = @S6BZ ";
            }
            if (IsSetS7BZ)
            {
                s += " AND S7BZ = @S7BZ ";
            }
            if (IsSetS8BZ)
            {
                s += " AND S8BZ = @S8BZ ";
            }
            if (IsSetS9BZ)
            {
                s += " AND S9BZ = @S9BZ ";
            }
            if (IsSetS10BZ)
            {
                s += " AND S10BZ = @S10BZ ";
            }
            if (IsSetS11BZ)
            {
                s += " AND S11BZ = @S11BZ ";
            }
            if (IsSetS12BZ)
            {
                s += " AND S12BZ = @S12BZ ";
            }
            if (IsSetS13BZ)
            {
                s += " AND S13BZ = @S13BZ ";
            }
            if (IsSetS14BZ)
            {
                s += " AND S14BZ = @S14BZ ";
            }
            if (IsSetS15BZ)
            {
                s += " AND S15BZ = @S15BZ ";
            }
            if (IsSetS16BZ)
            {
                s += " AND S16BZ = @S16BZ ";
            }
            if (IsSetS17BZ)
            {
                s += " AND S17BZ = @S17BZ ";
            }
            if (IsSetS18BZ)
            {
                s += " AND S18BZ = @S18BZ ";
            }
            if (IsSetS1BZZ)
            {
                s += " AND S1BZZ = @S1BZZ ";
            }
            if (IsSetS2BZZ)
            {
                s += " AND S2BZZ = @S2BZZ ";
            }
            if (IsSetS3BZZ)
            {
                s += " AND S3BZZ = @S3BZZ ";
            }
            if (IsSetS4BZZ)
            {
                s += " AND S4BZZ = @S4BZZ ";
            }
            if (IsSetS5BZZ)
            {
                s += " AND S5BZZ = @S5BZZ ";
            }
            if (IsSetS6BZZ)
            {
                s += " AND S6BZZ = @S6BZZ ";
            }
            if (IsSetS7BZZ)
            {
                s += " AND S7BZZ = @S7BZZ ";
            }
            if (IsSetS8BZZ)
            {
                s += " AND S8BZZ = @S8BZZ ";
            }
            if (IsSetS9BZZ)
            {
                s += " AND S9BZZ = @S9BZZ ";
            }
            if (IsSetS10BZZ)
            {
                s += " AND S10BZZ = @S10BZZ ";
            }
            if (IsSetS11BZZ)
            {
                s += " AND S11BZZ = @S11BZZ ";
            }
            if (IsSetS12BZZ)
            {
                s += " AND S12BZZ = @S12BZZ ";
            }
            if (IsSetS13BZZ)
            {
                s += " AND S13BZZ = @S13BZZ ";
            }
            if (IsSetS14BZZ)
            {
                s += " AND S14BZZ = @S14BZZ ";
            }
            if (IsSetS15BZZ)
            {
                s += " AND S15BZZ = @S15BZZ ";
            }
            if (IsSetS16BZZ)
            {
                s += " AND S16BZZ = @S16BZZ ";
            }
            if (IsSetS17BZZ)
            {
                s += " AND S17BZZ = @S17BZZ ";
            }
            if (IsSetS18BZZ)
            {
                s += " AND S18BZZ = @S18BZZ ";
            }
            if (IsSetM1QPR)
            {
                s += " AND M1QPR = @M1QPR ";
            }
            if (IsSetM2QPR)
            {
                s += " AND M2QPR = @M2QPR ";
            }
            if (IsSetM3QPR)
            {
                s += " AND M3QPR = @M3QPR ";
            }
            if (IsSetM4QPR)
            {
                s += " AND M4QPR = @M4QPR ";
            }
            if (IsSetM5QPR)
            {
                s += " AND M5QPR = @M5QPR ";
            }
            if (IsSetM6QPR)
            {
                s += " AND M6QPR = @M6QPR ";
            }
            if (IsSetM7QPR)
            {
                s += " AND M7QPR = @M7QPR ";
            }
            if (IsSetM8QPR)
            {
                s += " AND M8QPR = @M8QPR ";
            }
            if (IsSetFPType1)
            {
                s += " AND FPType1 = @FPType1 ";
            }
            if (IsSetFPType2)
            {
                s += " AND FPType2 = @FPType2 ";
            }
            if (IsSetFPType3)
            {
                s += " AND FPType3 = @FPType3 ";
            }
            if (IsSetFPType4)
            {
                s += " AND FPType4 = @FPType4 ";
            }
            if (IsSetFPType5)
            {
                s += " AND FPType5 = @FPType5 ";
            }
            if (IsSetFPType6)
            {
                s += " AND FPType6 = @FPType6 ";
            }
            if (IsSetFPType7)
            {
                s += " AND FPType7 = @FPType7 ";
            }
            if (IsSetFPType8)
            {
                s += " AND FPType8 = @FPType8 ";
            }
            if (IsSetFPType9)
            {
                s += " AND FPType9 = @FPType9 ";
            }
            if (IsSetFPType10)
            {
                s += " AND FPType10 = @FPType10 ";
            }
            if (IsSetFPType11)
            {
                s += " AND FPType11 = @FPType11 ";
            }
            if (IsSetFPType12)
            {
                s += " AND FPType12 = @FPType12 ";
            }
            if (IsSetFPType13)
            {
                s += " AND FPType13 = @FPType13 ";
            }
            if (IsSetFPType14)
            {
                s += " AND FPType14 = @FPType14 ";
            }
            if (IsSetFPType15)
            {
                s += " AND FPType15 = @FPType15 ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}