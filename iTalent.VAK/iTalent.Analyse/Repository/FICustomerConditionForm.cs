using System;
using VAKFomula.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FICustomer.
	/// </summary>
	public class FICustomerConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private string _ReportID;
      private DateTime _Date;
      private string _Name;
      private string _Gender;
      private DateTime _DOB;
      private string _Parent;
      private string _Tel;
      private string _Mobile;
      private string _Email;
      private string _Address1;
      private string _Address2;
      private string _City;
      private string _State;
      private string _ZipPosTalCode;
      private string _Country;
      private string _Remark;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetReportID;
	public bool IsReportIDNullable
      { get { return true;  } }
      public string ReportID
      {
         get { return _ReportID;  }
         set { 
		_ReportID = value; 
		IsSetReportID = true;
		}
      }
	public bool IsSetDate;
	public bool IsDateNullable
      { get { return true;  } }
      public DateTime Date
      {
         get { return _Date;  }
         set { 
		_Date = value; 
		IsSetDate = true;
		}
      }
	public bool IsSetName;
	public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { 
		_Name = value; 
		IsSetName = true;
		}
      }
	public bool IsSetGender;
	public bool IsGenderNullable
      { get { return true;  } }
      public string Gender
      {
         get { return _Gender;  }
         set { 
		_Gender = value; 
		IsSetGender = true;
		}
      }
	public bool IsSetDOB;
	public bool IsDOBNullable
      { get { return true;  } }
      public DateTime DOB
      {
         get { return _DOB;  }
         set { 
		_DOB = value; 
		IsSetDOB = true;
		}
      }
	public bool IsSetParent;
	public bool IsParentNullable
      { get { return true;  } }
      public string Parent
      {
         get { return _Parent;  }
         set { 
		_Parent = value; 
		IsSetParent = true;
		}
      }
	public bool IsSetTel;
	public bool IsTelNullable
      { get { return true;  } }
      public string Tel
      {
         get { return _Tel;  }
         set { 
		_Tel = value; 
		IsSetTel = true;
		}
      }
	public bool IsSetMobile;
	public bool IsMobileNullable
      { get { return true;  } }
      public string Mobile
      {
         get { return _Mobile;  }
         set { 
		_Mobile = value; 
		IsSetMobile = true;
		}
      }
	public bool IsSetEmail;
	public bool IsEmailNullable
      { get { return true;  } }
      public string Email
      {
         get { return _Email;  }
         set { 
		_Email = value; 
		IsSetEmail = true;
		}
      }
	public bool IsSetAddress1;
	public bool IsAddress1Nullable
      { get { return true;  } }
      public string Address1
      {
         get { return _Address1;  }
         set { 
		_Address1 = value; 
		IsSetAddress1 = true;
		}
      }
	public bool IsSetAddress2;
	public bool IsAddress2Nullable
      { get { return true;  } }
      public string Address2
      {
         get { return _Address2;  }
         set { 
		_Address2 = value; 
		IsSetAddress2 = true;
		}
      }
	public bool IsSetCity;
	public bool IsCityNullable
      { get { return true;  } }
      public string City
      {
         get { return _City;  }
         set { 
		_City = value; 
		IsSetCity = true;
		}
      }
	public bool IsSetState;
	public bool IsStateNullable
      { get { return true;  } }
      public string State
      {
         get { return _State;  }
         set { 
		_State = value; 
		IsSetState = true;
		}
      }
	public bool IsSetZipPosTalCode;
	public bool IsZipPosTalCodeNullable
      { get { return true;  } }
      public string ZipPosTalCode
      {
         get { return _ZipPosTalCode;  }
         set { 
		_ZipPosTalCode = value; 
		IsSetZipPosTalCode = true;
		}
      }
	public bool IsSetCountry;
	public bool IsCountryNullable
      { get { return true;  } }
      public string Country
      {
         get { return _Country;  }
         set { 
		_Country = value; 
		IsSetCountry = true;
		}
      }
	public bool IsSetRemark;
	public bool IsRemarkNullable
      { get { return true;  } }
      public string Remark
      {
         get { return _Remark;  }
         set { 
		_Remark = value; 
		IsSetRemark = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FICustomerConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _ReportID = EmptyValues.v_string;
	IsSetReportID = false;
         _Date = EmptyValues.v_DateTime;
	IsSetDate = false;
         _Name = EmptyValues.v_string;
	IsSetName = false;
         _Gender = EmptyValues.v_string;
	IsSetGender = false;
         _DOB = EmptyValues.v_DateTime;
	IsSetDOB = false;
         _Parent = EmptyValues.v_string;
	IsSetParent = false;
         _Tel = EmptyValues.v_string;
	IsSetTel = false;
         _Mobile = EmptyValues.v_string;
	IsSetMobile = false;
         _Email = EmptyValues.v_string;
	IsSetEmail = false;
         _Address1 = EmptyValues.v_string;
	IsSetAddress1 = false;
         _Address2 = EmptyValues.v_string;
	IsSetAddress2 = false;
         _City = EmptyValues.v_string;
	IsSetCity = false;
         _State = EmptyValues.v_string;
	IsSetState = false;
         _ZipPosTalCode = EmptyValues.v_string;
	IsSetZipPosTalCode = false;
         _Country = EmptyValues.v_string;
	IsSetCountry = false;
         _Remark = EmptyValues.v_string;
	IsSetRemark = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetReportID)
            {
                s += " AND ReportID = @ReportID ";
            }
            if (IsSetDate)
            {
                s += " AND Date = @Date ";
            }
            if (IsSetName)
            {
                s += " AND Name = @Name ";
            }
            if (IsSetGender)
            {
                s += " AND Gender = @Gender ";
            }
            if (IsSetDOB)
            {
                s += " AND DOB = @DOB ";
            }
            if (IsSetParent)
            {
                s += " AND Parent = @Parent ";
            }
            if (IsSetTel)
            {
                s += " AND Tel = @Tel ";
            }
            if (IsSetMobile)
            {
                s += " AND Mobile = @Mobile ";
            }
            if (IsSetEmail)
            {
                s += " AND Email = @Email ";
            }
            if (IsSetAddress1)
            {
                s += " AND Address1 = @Address1 ";
            }
            if (IsSetAddress2)
            {
                s += " AND Address2 = @Address2 ";
            }
            if (IsSetCity)
            {
                s += " AND City = @City ";
            }
            if (IsSetState)
            {
                s += " AND State = @State ";
            }
            if (IsSetZipPosTalCode)
            {
                s += " AND ZipPosTalCode = @ZipPosTalCode ";
            }
            if (IsSetCountry)
            {
                s += " AND Country = @Country ";
            }
            if (IsSetRemark)
            {
                s += " AND Remark = @Remark ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}