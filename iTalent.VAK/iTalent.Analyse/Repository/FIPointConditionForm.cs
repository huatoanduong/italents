using System;
using VAKFomula.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIPoint.
	/// </summary>
	public class FIPointConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _ReportID;
      private DateTime _PointDate;
      private string _PointType;
      private decimal _Point;
      private string _KeyPoint;
      private string _KeyPointAfter;
      private string _AddedBy;
      private Int16 _IsOutDate;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetReportID;
	public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { 
		_ReportID = value; 
		IsSetReportID = true;
		}
      }
	public bool IsSetPointDate;
	public bool IsPointDateNullable
      { get { return true;  } }
      public DateTime PointDate
      {
         get { return _PointDate;  }
         set { 
		_PointDate = value; 
		IsSetPointDate = true;
		}
      }
	public bool IsSetPointType;
	public bool IsPointTypeNullable
      { get { return true;  } }
      public string PointType
      {
         get { return _PointType;  }
         set { 
		_PointType = value; 
		IsSetPointType = true;
		}
      }
	public bool IsSetPoint;
	public bool IsPointNullable
      { get { return true;  } }
      public decimal Point
      {
         get { return _Point;  }
         set { 
		_Point = value; 
		IsSetPoint = true;
		}
      }
	public bool IsSetKeyPoint;
	public bool IsKeyPointNullable
      { get { return true;  } }
      public string KeyPoint
      {
         get { return _KeyPoint;  }
         set { 
		_KeyPoint = value; 
		IsSetKeyPoint = true;
		}
      }
	public bool IsSetKeyPointAfter;
	public bool IsKeyPointAfterNullable
      { get { return true;  } }
      public string KeyPointAfter
      {
         get { return _KeyPointAfter;  }
         set { 
		_KeyPointAfter = value; 
		IsSetKeyPointAfter = true;
		}
      }
	public bool IsSetAddedBy;
	public bool IsAddedByNullable
      { get { return true;  } }
      public string AddedBy
      {
         get { return _AddedBy;  }
         set { 
		_AddedBy = value; 
		IsSetAddedBy = true;
		}
      }
	public bool IsSetIsOutDate;
	public bool IsIsOutDateNullable
      { get { return true;  } }
      public Int16 IsOutDate
      {
         get { return _IsOutDate;  }
         set { 
		_IsOutDate = value; 
		IsSetIsOutDate = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIPointConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _ReportID = EmptyValues.v_Int32;
	IsSetReportID = false;
         _PointDate = EmptyValues.v_DateTime;
	IsSetPointDate = false;
         _PointType = EmptyValues.v_string;
	IsSetPointType = false;
         _Point = EmptyValues.v_decimal;
	IsSetPoint = false;
         _KeyPoint = EmptyValues.v_string;
	IsSetKeyPoint = false;
         _KeyPointAfter = EmptyValues.v_string;
	IsSetKeyPointAfter = false;
         _AddedBy = EmptyValues.v_string;
	IsSetAddedBy = false;
         _IsOutDate = EmptyValues.v_Int16;
	IsSetIsOutDate = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetReportID)
            {
                s += " AND ReportID = @ReportID ";
            }
            if (IsSetPointDate)
            {
                s += " AND PointDate = @PointDate ";
            }
            if (IsSetPointType)
            {
                s += " AND PointType = @PointType ";
            }
            if (IsSetPoint)
            {
                s += " AND Point = @Point ";
            }
            if (IsSetKeyPoint)
            {
                s += " AND KeyPoint = @KeyPoint ";
            }
            if (IsSetKeyPointAfter)
            {
                s += " AND KeyPointAfter = @KeyPointAfter ";
            }
            if (IsSetAddedBy)
            {
                s += " AND AddedBy = @AddedBy ";
            }
            if (IsSetIsOutDate)
            {
                s += " AND IsOutDate = @IsOutDate ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}