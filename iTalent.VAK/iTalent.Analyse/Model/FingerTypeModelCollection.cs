﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VAKFomula.Entity;

namespace iTalent.Analyse.Model
{
    public class FingerTypeModel
    {
        public FIFingerType FingerType { get; set; }

        public string TypeString
        {
            get { return FingerType.Types; }
        }

        public decimal Value { get; set; }
    }

    public class FingerTypeModelCollection
    {
        public List<FIFingerType> FingerTypes { get; set; }

        public List<FingerTypeModel> TypeList { get; set; }

        //NHÓM ĐẠI BÀNG: WT, WS
        //NHÓM SƯ TỬ: WE
        //NHÓM CON MÈO: WC, WD, WI
        //NHÓM CÁ HEO: RL
        //NHÓM CON KHỈ: UL
        //NHÓM CON CÚ: ARCH
        //NHÓM MẮT CÔNG: WP, WL
        //NHÓM TỔ HỢP: LF, WX

        public FingerTypeModel DaiBang { get; set; }
        public FingerTypeModel SuTu { get; set; }
        public FingerTypeModel ConMeo { get; set; }
        public FingerTypeModel CaHeo { get; set; }
        public FingerTypeModel ConKhi { get; set; }
        public FingerTypeModel ConCu { get; set; }
        public FingerTypeModel MatCong { get; set; }
        public FingerTypeModel ToHop { get; set; }

        public FingerTypeModel NhomLF { get; set; }

        public Dictionary<string, int> FingerTypeValue;

        public FingerTypeModelCollection(List<FIFingerType> types)
        {
            FingerTypes = types;
            UpdateProperties();
        }

        private void UpdateProperties()
        {
            FingerTypes = FingerTypes.OrderBy(p => p.ID).ToList();
            TypeList = new List<FingerTypeModel>();

            for (int i = 0; i < FingerTypes.Count; i++)
            {
                TypeList.Add(new FingerTypeModel()
                             {
                                 FingerType = FingerTypes[i]
                             });
            }

            DaiBang = TypeList[0];
            SuTu = TypeList[1];
            ConMeo = TypeList[2];
            CaHeo = TypeList[3];
            ConKhi = TypeList[4];
            ConCu = TypeList[5];
            MatCong = TypeList[6];
            ToHop = TypeList[7];
            NhomLF = new FingerTypeModel();
        }

        public void MapFingerAnalyse(FIFingerAnalysis fingerAnalysis)
        {
            fingerAnalysis.FPType1 = DaiBang.Value;
            fingerAnalysis.FPType2 = SuTu.Value;
            fingerAnalysis.FPType3 = ConMeo.Value;
            fingerAnalysis.FPType4 = CaHeo.Value;
            fingerAnalysis.FPType5 = ConKhi.Value;
            fingerAnalysis.FPType6 = ConCu.Value;
            fingerAnalysis.FPType7 = MatCong.Value;
            fingerAnalysis.FPType8 = ToHop.Value;

            decimal totalW = DaiBang.Value + SuTu.Value + ConMeo.Value + MatCong.Value + ToHop.Value - NhomLF.Value;
            fingerAnalysis.FPType9 = totalW;

            decimal ULLF = NhomLF.Value + ConKhi.Value;
            fingerAnalysis.FPType10 = ULLF;
        }

        public FingerTypeModel GetByType(string fingerType)
        {
            return TypeList.FirstOrDefault(p => p.TypeString.Contains(fingerType));
        }
    }
}