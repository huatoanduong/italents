﻿using System.Collections.Generic;
using VAKFomula.Entity;

namespace iTalent.Analyse.Model
{
    public class ReportFieldModel
    {
        public FIAgency Distributor { get; set; }
        public FICustomer Customer { get; set; }
        public FIFingerAnalysis FingerAnalysis { get; set; }
        public FIMQChart MQ_Chart { get; set; }

        #region Formula Fields

        public decimal? _100AQ { get; set; }
        public decimal? _100CQ { get; set; }
        public decimal? _100EQ { get; set; }
        public decimal? _100IQ { get; set; }
        public decimal? ATD_Average { get; set; }
        public decimal? BLRZZ { get; set; }
        public string IN1_IT_construction { get; set; }
        public string IN10_psycho { get; set; }
        public string IN10_sales { get; set; }
        public string IN11_mass { get; set; }
        public string IN12_lang { get; set; }
        public string IN13_lit { get; set; }
        public string IN14_edu { get; set; }
        public string IN15_pol { get; set; }
        public string IN16_mgn { get; set; }
        public string IN17_fin { get; set; }
        public string IN18_spo { get; set; }
        public string IN19_fin2 { get; set; }
        public string IN2_eng { get; set; }
        public string IN3_math { get; set; }
        public string IN4_med { get; set; }
        public string IN5_life { get; set; }
        public string IN6_agri { get; set; }
        public string IN7_earth { get; set; }
        public string IN9_art { get; set; }
        public decimal? L1RCBVA { get; set; }
        public decimal? L1RCD { get; set; }
        public string L1TCT { get; set; }
        public decimal? L2RCBVA { get; set; }
        public decimal? L2RCD { get; set; }
        public decimal? L3RCBVA { get; set; }
        public decimal? L3RCD { get; set; }
        public decimal? L4RCBVA { get; set; }
        public decimal? L4RCD { get; set; }
        public decimal? L5RCBVA { get; set; }
        public decimal? L5RCD { get; set; }
        public decimal? L5RCP_Final { get; set; }
        public decimal? LTRC { get; set; }
        public decimal? LTRCP { get; set; }
        public decimal? M1QP { get; set; }
        public decimal? M2QP { get; set; }
        public decimal? M3QP { get; set; }
        public decimal? M4QP { get; set; }
        public decimal? M5QP { get; set; }
        public decimal? M6QP { get; set; }
        public decimal? M7QP { get; set; }
        public decimal? M8QP { get; set; }
        public string MA1 { get; set; }
        public string MA2 { get; set; }
        public string MA3 { get; set; }
        public string MA4 { get; set; }
        public string MA5 { get; set; }
        public string MA6 { get; set; }
        public string MA7 { get; set; }
        public string MA8 { get; set; }
        public string MALATD { get; set; }
        public string MARATD { get; set; }
        public string plus_X { get; set; }
        public decimal? R1N { get; set; }
        public decimal? R1RCBVA { get; set; }
        public decimal? R1RCD { get; set; }
        public string R1TCT { get; set; }
        public decimal? R2RCBVA { get; set; }
        public decimal? R2RCD { get; set; }
        public decimal? R3RCBVA { get; set; }
        public decimal? R3RCD { get; set; }
        public decimal? R4RCBVA { get; set; }
        public decimal? R4RCD { get; set; }
        public decimal? R5RCBVA { get; set; }
        public decimal? R5RCD { get; set; }
        public string Rank_AHP { get; set; }
        public decimal? RCVA { get; set; }
        public decimal? RTRC { get; set; }
        public decimal? RTRCP { get; set; }
        public decimal? T1BP_2 { get; set; }
        public decimal? Vrank { get; set; }

        #endregion

        protected int? ToNumber(decimal? dec)
        {
            if (dec == null)
            {
                return null;
            }
            return (int) dec;
        }

        public void CalculateFormula()
        {
            Cal_LTRC();
            Cal_RTRC();
            Cal_RCVA();
            //--------------------------
            Cal_100AQ();
            Cal_100CQ();
            Cal_100EQ();
            Cal_100IQ();
            Cal_ATD_Average();
            Cal_BLRZZ();
            Cal_IN1_IT_construction();
            Cal_IN10_psycho();
            Cal_IN10_sales();
            Cal_IN11_mass();
            Cal_IN12_lang();
            Cal_IN13_lit();
            Cal_IN14_edu();
            Cal_IN15_pol();
            Cal_IN16_mgn();
            Cal_IN17_fin();
            Cal_IN18_spo();
            Cal_IN19_fin2();
            Cal_IN2_eng();
            Cal_IN3_math();
            Cal_IN4_med();
            Cal_IN5_life();
            Cal_IN6_agri();
            Cal_IN7_earth();
            Cal_IN9_art();
            Cal_L1RCBVA();
            Cal_L1RCD();
            Cal_L1TCT();
            Cal_L2RCBVA();
            Cal_L2RCD();
            Cal_L3RCBVA();
            Cal_L3RCD();
            Cal_L4RCBVA();
            Cal_L4RCD();
            Cal_L5RCBVA();
            Cal_L5RCD();
            Cal_L5RCP_Final();
            Cal_LTRCP();
            Cal_M1QP();
            Cal_M2QP();
            Cal_M3QP();
            Cal_M4QP();
            Cal_M5QP();
            Cal_M6QP();
            Cal_M7QP();
            Cal_M8QP();
            Cal_MA1();
            Cal_MA2();
            Cal_MA3();
            Cal_MA4();
            Cal_MA5();
            Cal_MA6();
            Cal_MA7();
            Cal_MA8();
            Cal_MALATD();
            Cal_MARATD();
            Cal_plus_X();
            Cal_R1N();
            Cal_R1RCBVA();
            Cal_R1RCD();
            Cal_R1TCT();
            Cal_R2RCBVA();
            Cal_R2RCD();
            Cal_R3RCBVA();
            Cal_R3RCD();
            Cal_R4RCBVA();
            Cal_R4RCD();
            Cal_R5RCBVA();
            Cal_R5RCD();
            Cal_Rank_AHP();
            Cal_RTRCP();
            Cal_T1BP_2();
            Cal_Vrank();
        }

        public Dictionary<string, object> ToDictionary()
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            //Distributor
            dictionary.Add("Distributor.ID", Distributor.ID);
            dictionary.Add("Distributor.Username", Distributor.Username);
            dictionary.Add("Distributor.Password", Distributor.Password);
            dictionary.Add("Distributor.Name", Distributor.Name);
            dictionary.Add("Distributor.Point", Distributor.Point);
            dictionary.Add("Distributor.Gender", Distributor.Gender);
            dictionary.Add("Distributor.DOB", Distributor.DOB);
            dictionary.Add("Distributor.Address1", Distributor.Address1);
            dictionary.Add("Distributor.Address2", Distributor.Address2);
            dictionary.Add("Distributor.City", Distributor.City);
            dictionary.Add("Distributor.State", Distributor.State);
            dictionary.Add("Distributor.PostalCode", Distributor.PostalCode);
            dictionary.Add("Distributor.Country", Distributor.Country);
            dictionary.Add("Distributor.PhoneNo", Distributor.PhoneNo);
            dictionary.Add("Distributor.MobileNo", Distributor.MobileNo);
            dictionary.Add("Distributor.Email", Distributor.Email);
            dictionary.Add("Distributor.PassEmail", Distributor.PassEmail);
            dictionary.Add("Distributor.SaveImages", Distributor.SaveImages);
            dictionary.Add("Distributor.LastActiveDate", Distributor.LastActiveDate);
            dictionary.Add("Distributor.Day_Actived", Distributor.Day_Actived);
            dictionary.Add("Distributor.Dis_Activated", Distributor.Dis_Activated);
            dictionary.Add("Distributor.ProductKey", Distributor.ProductKey);
            dictionary.Add("Distributor.SerailKey", Distributor.SerailKey);
            dictionary.Add("Distributor.SecKey", Distributor.SecKey);


            //Customer
            dictionary.Add("Customer.ID", Customer.ID);
            dictionary.Add("Customer.AgencyID", Customer.AgencyID);
            dictionary.Add("Customer.ReportID", Customer.ReportID);
            dictionary.Add("Customer.Name", Customer.Name);
            dictionary.Add("Customer.Date", Customer.Date);
            dictionary.Add("Customer.Gender", Customer.Gender);
            dictionary.Add("Customer.DOB", Customer.DOB);
            dictionary.Add("Customer.Parent", Customer.Parent);
            dictionary.Add("Customer.Tel", Customer.Tel);
            dictionary.Add("Customer.Mobile", Customer.Mobile);
            dictionary.Add("Customer.Email", Customer.Email);
            dictionary.Add("Customer.Address1", Customer.Address1);
            dictionary.Add("Customer.Address2", Customer.Address2);
            dictionary.Add("Customer.City", Customer.City);
            dictionary.Add("Customer.State", Customer.State);
            dictionary.Add("Customer.ZipPosTalCode", Customer.ZipPosTalCode);
            dictionary.Add("Customer.Country", Customer.Country);
            dictionary.Add("Customer.Remark", Customer.Remark);


            //FingerAnalysis
            dictionary.Add("FingerAnalysis.ID", FingerAnalysis.ID);
            dictionary.Add("FingerAnalysis.AgencyID", FingerAnalysis.AgencyID);
            dictionary.Add("FingerAnalysis.ReportID", FingerAnalysis.ReportID);
            dictionary.Add("FingerAnalysis.LT_BLS", FingerAnalysis.LT_BLS);
            dictionary.Add("FingerAnalysis.RT_BLS", FingerAnalysis.RT_BLS);
            dictionary.Add("FingerAnalysis.Page_BLS", FingerAnalysis.Page_BLS);
            dictionary.Add("FingerAnalysis.SAP", FingerAnalysis.SAP);
            dictionary.Add("FingerAnalysis.SBP", FingerAnalysis.SBP);
            dictionary.Add("FingerAnalysis.SCP", FingerAnalysis.SCP);
            dictionary.Add("FingerAnalysis.SDP", FingerAnalysis.SDP);
            dictionary.Add("FingerAnalysis.SER", FingerAnalysis.SER);
            dictionary.Add("FingerAnalysis.TFRC", FingerAnalysis.TFRC);
            dictionary.Add("FingerAnalysis.EQVP", FingerAnalysis.EQVP);
            dictionary.Add("FingerAnalysis.IQVP", FingerAnalysis.IQVP);
            dictionary.Add("FingerAnalysis.AQVP", FingerAnalysis.AQVP);
            dictionary.Add("FingerAnalysis.CQVP", FingerAnalysis.CQVP);
            dictionary.Add("FingerAnalysis.L1RCP", FingerAnalysis.L1RCP);
            dictionary.Add("FingerAnalysis.L2RCP", FingerAnalysis.L2RCP);
            dictionary.Add("FingerAnalysis.L3RCP", FingerAnalysis.L3RCP);
            dictionary.Add("FingerAnalysis.L4RCP", FingerAnalysis.L4RCP);
            dictionary.Add("FingerAnalysis.L5RCP", FingerAnalysis.L5RCP);
            dictionary.Add("FingerAnalysis.R1RCP", FingerAnalysis.R1RCP);
            dictionary.Add("FingerAnalysis.R2RCP", FingerAnalysis.R2RCP);
            dictionary.Add("FingerAnalysis.R3RCP", FingerAnalysis.R3RCP);
            dictionary.Add("FingerAnalysis.R4RCP", FingerAnalysis.R4RCP);
            dictionary.Add("FingerAnalysis.R5RCP", FingerAnalysis.R5RCP);
            dictionary.Add("FingerAnalysis.L1RCP1", FingerAnalysis.L1RCP1);
            dictionary.Add("FingerAnalysis.L2RCP2", FingerAnalysis.L2RCP2);
            dictionary.Add("FingerAnalysis.L3RCP3", FingerAnalysis.L3RCP3);
            dictionary.Add("FingerAnalysis.L4RCP4", FingerAnalysis.L4RCP4);
            dictionary.Add("FingerAnalysis.L5RCP5", FingerAnalysis.L5RCP5);
            dictionary.Add("FingerAnalysis.R1RCP1", FingerAnalysis.R1RCP1);
            dictionary.Add("FingerAnalysis.R2RCP2", FingerAnalysis.R2RCP2);
            dictionary.Add("FingerAnalysis.R3RCP3", FingerAnalysis.R3RCP3);
            dictionary.Add("FingerAnalysis.R4RCP4", FingerAnalysis.R4RCP4);
            dictionary.Add("FingerAnalysis.R5RCP5", FingerAnalysis.R5RCP5);
            dictionary.Add("FingerAnalysis.ATDR", FingerAnalysis.ATDR);
            dictionary.Add("FingerAnalysis.ATDL", FingerAnalysis.ATDL);
            dictionary.Add("FingerAnalysis.ATDRA", FingerAnalysis.ATDRA);
            dictionary.Add("FingerAnalysis.ATDRS", FingerAnalysis.ATDRS);
            dictionary.Add("FingerAnalysis.ATDLA", FingerAnalysis.ATDLA);
            dictionary.Add("FingerAnalysis.ATDLS", FingerAnalysis.ATDLS);
            dictionary.Add("FingerAnalysis.AC2WP", FingerAnalysis.AC2WP);
            dictionary.Add("FingerAnalysis.AC2UP", FingerAnalysis.AC2UP);
            dictionary.Add("FingerAnalysis.AC2RP", FingerAnalysis.AC2RP);
            dictionary.Add("FingerAnalysis.AC2XP", FingerAnalysis.AC2XP);
            dictionary.Add("FingerAnalysis.AC2SP", FingerAnalysis.AC2SP);
            dictionary.Add("FingerAnalysis.RCAP", FingerAnalysis.RCAP);
            dictionary.Add("FingerAnalysis.RCHP", FingerAnalysis.RCHP);
            dictionary.Add("FingerAnalysis.RCVP", FingerAnalysis.RCVP);
            dictionary.Add("FingerAnalysis.T1AP", FingerAnalysis.T1AP);
            dictionary.Add("FingerAnalysis.T1BP", FingerAnalysis.T1BP);
            dictionary.Add("FingerAnalysis.M1Q", FingerAnalysis.M1Q);
            dictionary.Add("FingerAnalysis.M2Q", FingerAnalysis.M2Q);
            dictionary.Add("FingerAnalysis.M3Q", FingerAnalysis.M3Q);
            dictionary.Add("FingerAnalysis.M4Q", FingerAnalysis.M4Q);
            dictionary.Add("FingerAnalysis.M5Q", FingerAnalysis.M5Q);
            dictionary.Add("FingerAnalysis.M6Q", FingerAnalysis.M6Q);
            dictionary.Add("FingerAnalysis.M7Q", FingerAnalysis.M7Q);
            dictionary.Add("FingerAnalysis.M8Q", FingerAnalysis.M8Q);
            dictionary.Add("FingerAnalysis.L1RCB", FingerAnalysis.L1RCB);
            dictionary.Add("FingerAnalysis.L2RCB", FingerAnalysis.L2RCB);
            dictionary.Add("FingerAnalysis.L3RCB", FingerAnalysis.L3RCB);
            dictionary.Add("FingerAnalysis.L4RCB", FingerAnalysis.L4RCB);
            dictionary.Add("FingerAnalysis.L5RCB", FingerAnalysis.L5RCB);
            dictionary.Add("FingerAnalysis.R1RCB", FingerAnalysis.R1RCB);
            dictionary.Add("FingerAnalysis.R2RCB", FingerAnalysis.R2RCB);
            dictionary.Add("FingerAnalysis.R3RCB", FingerAnalysis.R3RCB);
            dictionary.Add("FingerAnalysis.R4RCB", FingerAnalysis.R4RCB);
            dictionary.Add("FingerAnalysis.R5RCB", FingerAnalysis.R5RCB);
            dictionary.Add("FingerAnalysis.Label1", FingerAnalysis.Label1);
            dictionary.Add("FingerAnalysis.Label2", FingerAnalysis.Label2);
            dictionary.Add("FingerAnalysis.Label3", FingerAnalysis.Label3);
            dictionary.Add("FingerAnalysis.Label4", FingerAnalysis.Label4);
            dictionary.Add("FingerAnalysis.Label5", FingerAnalysis.Label5);
            dictionary.Add("FingerAnalysis.Label6", FingerAnalysis.Label6);
            dictionary.Add("FingerAnalysis.L1T", FingerAnalysis.L1T);
            dictionary.Add("FingerAnalysis.L2T", FingerAnalysis.L2T);
            dictionary.Add("FingerAnalysis.L3T", FingerAnalysis.L3T);
            dictionary.Add("FingerAnalysis.L4T", FingerAnalysis.L4T);
            dictionary.Add("FingerAnalysis.L5T", FingerAnalysis.L5T);
            dictionary.Add("FingerAnalysis.R1T", FingerAnalysis.R1T);
            dictionary.Add("FingerAnalysis.R2T", FingerAnalysis.R2T);
            dictionary.Add("FingerAnalysis.R3T", FingerAnalysis.R3T);
            dictionary.Add("FingerAnalysis.R4T", FingerAnalysis.R4T);
            dictionary.Add("FingerAnalysis.R5T", FingerAnalysis.R5T);
            dictionary.Add("FingerAnalysis.S1BZ", FingerAnalysis.S1BZ);
            dictionary.Add("FingerAnalysis.S2BZ", FingerAnalysis.S2BZ);
            dictionary.Add("FingerAnalysis.S3BZ", FingerAnalysis.S3BZ);
            dictionary.Add("FingerAnalysis.S4BZ", FingerAnalysis.S4BZ);
            dictionary.Add("FingerAnalysis.S5BZ", FingerAnalysis.S5BZ);
            dictionary.Add("FingerAnalysis.S6BZ", FingerAnalysis.S6BZ);
            dictionary.Add("FingerAnalysis.S7BZ", FingerAnalysis.S7BZ);
            dictionary.Add("FingerAnalysis.S8BZ", FingerAnalysis.S8BZ);
            dictionary.Add("FingerAnalysis.S9BZ", FingerAnalysis.S9BZ);
            dictionary.Add("FingerAnalysis.S10BZ", FingerAnalysis.S10BZ);
            dictionary.Add("FingerAnalysis.S11BZ", FingerAnalysis.S11BZ);
            dictionary.Add("FingerAnalysis.S12BZ", FingerAnalysis.S12BZ);
            dictionary.Add("FingerAnalysis.S13BZ", FingerAnalysis.S13BZ);
            dictionary.Add("FingerAnalysis.S14BZ", FingerAnalysis.S14BZ);
            dictionary.Add("FingerAnalysis.S15BZ", FingerAnalysis.S15BZ);
            dictionary.Add("FingerAnalysis.S16BZ", FingerAnalysis.S16BZ);
            dictionary.Add("FingerAnalysis.S17BZ", FingerAnalysis.S17BZ);
            dictionary.Add("FingerAnalysis.S18BZ", FingerAnalysis.S18BZ);
            dictionary.Add("FingerAnalysis.S1BZZ", FingerAnalysis.S1BZZ);
            dictionary.Add("FingerAnalysis.S2BZZ", FingerAnalysis.S2BZZ);
            dictionary.Add("FingerAnalysis.S3BZZ", FingerAnalysis.S3BZZ);
            dictionary.Add("FingerAnalysis.S4BZZ", FingerAnalysis.S4BZZ);
            dictionary.Add("FingerAnalysis.S5BZZ", FingerAnalysis.S5BZZ);
            dictionary.Add("FingerAnalysis.S6BZZ", FingerAnalysis.S6BZZ);
            dictionary.Add("FingerAnalysis.S7BZZ", FingerAnalysis.S7BZZ);
            dictionary.Add("FingerAnalysis.S8BZZ", FingerAnalysis.S8BZZ);
            dictionary.Add("FingerAnalysis.S9BZZ", FingerAnalysis.S9BZZ);
            dictionary.Add("FingerAnalysis.S10BZZ", FingerAnalysis.S10BZZ);
            dictionary.Add("FingerAnalysis.S11BZZ", FingerAnalysis.S11BZZ);
            dictionary.Add("FingerAnalysis.S12BZZ", FingerAnalysis.S12BZZ);
            dictionary.Add("FingerAnalysis.S13BZZ", FingerAnalysis.S13BZZ);
            dictionary.Add("FingerAnalysis.S14BZZ", FingerAnalysis.S14BZZ);
            dictionary.Add("FingerAnalysis.S15BZZ", FingerAnalysis.S15BZZ);
            dictionary.Add("FingerAnalysis.S16BZZ", FingerAnalysis.S16BZZ);
            dictionary.Add("FingerAnalysis.S17BZZ", FingerAnalysis.S17BZZ);
            dictionary.Add("FingerAnalysis.S18BZZ", FingerAnalysis.S18BZZ);
            dictionary.Add("FingerAnalysis.M1QPR", FingerAnalysis.M1QPR);
            dictionary.Add("FingerAnalysis.M2QPR", FingerAnalysis.M2QPR);
            dictionary.Add("FingerAnalysis.M3QPR", FingerAnalysis.M3QPR);
            dictionary.Add("FingerAnalysis.M4QPR", FingerAnalysis.M4QPR);
            dictionary.Add("FingerAnalysis.M5QPR", FingerAnalysis.M5QPR);
            dictionary.Add("FingerAnalysis.M6QPR", FingerAnalysis.M6QPR);
            dictionary.Add("FingerAnalysis.M7QPR", FingerAnalysis.M7QPR);
            dictionary.Add("FingerAnalysis.M8QPR", FingerAnalysis.M8QPR);


            //MQ_Chart
            dictionary.Add("MQ_Chart.ID", MQ_Chart.ID);
            dictionary.Add("MQ_Chart.AgencyID", MQ_Chart.AgencyID);
            dictionary.Add("MQ_Chart.ReportID", MQ_Chart.ReportID);
            dictionary.Add("MQ_Chart.MQID", MQ_Chart.MQID);
            dictionary.Add("MQ_Chart.MQDesc", MQ_Chart.MQDesc);
            dictionary.Add("MQ_Chart.MQValue", MQ_Chart.MQValue);


            //Formula
            dictionary.Add("Formula.100AQ", this._100AQ);
            dictionary.Add("Formula.100CQ", this._100CQ);
            dictionary.Add("Formula.100EQ", this._100EQ);
            dictionary.Add("Formula.100IQ", this._100IQ);
            dictionary.Add("Formula.ATD_Average", this.ATD_Average);
            dictionary.Add("Formula.BLRZZ", this.BLRZZ);
            dictionary.Add("Formula.IN1_IT_construction", this.IN1_IT_construction);
            dictionary.Add("Formula.IN10_psycho", this.IN10_psycho);
            dictionary.Add("Formula.IN10_sales", this.IN10_sales);
            dictionary.Add("Formula.IN11_mass", this.IN11_mass);
            dictionary.Add("Formula.IN12_lang", this.IN12_lang);
            dictionary.Add("Formula.IN13_lit", this.IN13_lit);
            dictionary.Add("Formula.IN14_edu", this.IN14_edu);
            dictionary.Add("Formula.IN15_pol", this.IN15_pol);
            dictionary.Add("Formula.IN16_mgn", this.IN16_mgn);
            dictionary.Add("Formula.IN17_fin", this.IN17_fin);
            dictionary.Add("Formula.IN18_spo", this.IN18_spo);
            dictionary.Add("Formula.IN19_fin2", this.IN19_fin2);
            dictionary.Add("Formula.IN2_eng", this.IN2_eng);
            dictionary.Add("Formula.IN3_math", this.IN3_math);
            dictionary.Add("Formula.IN4_med", this.IN4_med);
            dictionary.Add("Formula.IN5_life", this.IN5_life);
            dictionary.Add("Formula.IN6_agri", this.IN6_agri);
            dictionary.Add("Formula.IN7_earth", this.IN7_earth);
            dictionary.Add("Formula.IN9_art", this.IN9_art);
            dictionary.Add("Formula.L1RCBVA", this.L1RCBVA);
            dictionary.Add("Formula.L1RCD", this.L1RCD);
            dictionary.Add("Formula.L1TCT", this.L1TCT);
            dictionary.Add("Formula.L2RCBVA", this.L2RCBVA);
            dictionary.Add("Formula.L2RCD", this.L2RCD);
            dictionary.Add("Formula.L3RCBVA", this.L3RCBVA);
            dictionary.Add("Formula.L3RCD", this.L3RCD);
            dictionary.Add("Formula.L4RCBVA", this.L4RCBVA);
            dictionary.Add("Formula.L4RCD", this.L4RCD);
            dictionary.Add("Formula.L5RCBVA", this.L5RCBVA);
            dictionary.Add("Formula.L5RCD", this.L5RCD);
            dictionary.Add("Formula.L5RCP_Final", this.L5RCP_Final);
            dictionary.Add("Formula.LTRC", this.LTRC);
            dictionary.Add("Formula.LTRCP", this.LTRCP);
            dictionary.Add("Formula.M1QP", this.M1QP);
            dictionary.Add("Formula.M2QP", this.M2QP);
            dictionary.Add("Formula.M3QP", this.M3QP);
            dictionary.Add("Formula.M4QP", this.M4QP);
            dictionary.Add("Formula.M5QP", this.M5QP);
            dictionary.Add("Formula.M6QP", this.M6QP);
            dictionary.Add("Formula.M7QP", this.M7QP);
            dictionary.Add("Formula.M8QP", this.M8QP);
            dictionary.Add("Formula.MA1", this.MA1);
            dictionary.Add("Formula.MA2", this.MA2);
            dictionary.Add("Formula.MA3", this.MA3);
            dictionary.Add("Formula.MA4", this.MA4);
            dictionary.Add("Formula.MA5", this.MA5);
            dictionary.Add("Formula.MA6", this.MA6);
            dictionary.Add("Formula.MA7", this.MA7);
            dictionary.Add("Formula.MA8", this.MA8);
            dictionary.Add("Formula.MALATD", this.MALATD);
            dictionary.Add("Formula.MARATD", this.MARATD);
            dictionary.Add("Formula.plus_X", this.plus_X);
            dictionary.Add("Formula.R1N", this.R1N);
            dictionary.Add("Formula.R1RCBVA", this.R1RCBVA);
            dictionary.Add("Formula.R1RCD", this.R1RCD);
            dictionary.Add("Formula.R1TCT", this.R1TCT);
            dictionary.Add("Formula.R2RCBVA", this.R2RCBVA);
            dictionary.Add("Formula.R2RCD", this.R2RCD);
            dictionary.Add("Formula.R3RCBVA", this.R3RCBVA);
            dictionary.Add("Formula.R3RCD", this.R3RCD);
            dictionary.Add("Formula.R4RCBVA", this.R4RCBVA);
            dictionary.Add("Formula.R4RCD", this.R4RCD);
            dictionary.Add("Formula.R5RCBVA", this.R5RCBVA);
            dictionary.Add("Formula.R5RCD", this.R5RCD);
            dictionary.Add("Formula.Rank_AHP", this.Rank_AHP);
            dictionary.Add("Formula.RCVA", this.RCVA);
            dictionary.Add("Formula.RTRC", this.RTRC);
            dictionary.Add("Formula.RTRCP", this.RTRCP);
            dictionary.Add("Formula.T1BP_2", this.T1BP_2);
            dictionary.Add("Formula.Vrank", this.Vrank);


            return dictionary;
        }

        #region Formula

        protected void Cal_100AQ()
        {
            this._100AQ = FingerAnalysis.AQVP/
                          (FingerAnalysis.AQVP + FingerAnalysis.EQVP + FingerAnalysis.CQVP + FingerAnalysis.IQVP)*100;
        }

        protected void Cal_100CQ()
        {
            this._100CQ = FingerAnalysis.CQVP/
                          (FingerAnalysis.AQVP + FingerAnalysis.EQVP + FingerAnalysis.CQVP + FingerAnalysis.IQVP)*100;
        }

        protected void Cal_100EQ()
        {
            this._100EQ = FingerAnalysis.EQVP/
                          (FingerAnalysis.AQVP + FingerAnalysis.EQVP + FingerAnalysis.CQVP + FingerAnalysis.IQVP)*100;
        }

        protected void Cal_100IQ()
        {
            this._100IQ = FingerAnalysis.IQVP/
                          (FingerAnalysis.EQVP + FingerAnalysis.IQVP + FingerAnalysis.CQVP + FingerAnalysis.AQVP)*100;
        }

        protected void Cal_ATD_Average()
        {
            this.ATD_Average = (FingerAnalysis.ATDL + FingerAnalysis.ATDR)/2;
        }

        protected void Cal_BLRZZ()
        {
        }

        protected void Cal_IN1_IT_construction()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 <= 2.4m)
            {
                IN1_IT_construction = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 <= 3m))
            {
                IN1_IT_construction = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 <= 4.99m))
            {
                IN1_IT_construction = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 <= 7.99m))
            {
                IN1_IT_construction = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR)/2 > 7.99m)
            {
                IN1_IT_construction = @"❤❤";
            }
        }

        protected void Cal_IN10_psycho()
        {
            if ((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 <= 2.4m)
            {
                IN10_psycho = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 > 2.4m)
                     && ((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 <= 3m))
            {
                IN10_psycho = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 > 3m)
                     && ((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 <= 4.99m))
            {
                IN10_psycho = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 > 4.99m)
                     && ((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 <= 7.99m))
            {
                IN10_psycho = @"❤❤❤";
            }
            else if ((FingerAnalysis.M7QPR + FingerAnalysis.M8QPR)/2 > 7.99m)
            {
                IN10_psycho = @"❤❤";
            }
        }

        protected void Cal_IN10_sales()
        {
            if ((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 <= 2.4m)
            {
                IN10_sales = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 > 2.4m) &&
                     ((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 <= 3m))
            {
                IN10_sales = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 > 3m) &&
                     ((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 <= 4.99m))
            {
                IN10_sales = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 > 4.99m) &&
                     ((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 <= 7.99m))
            {
                IN10_sales = @"❤❤❤";
            }
            else if ((FingerAnalysis.M1QPR + (FingerAnalysis.M6QPR) + FingerAnalysis.M7QPR)/3 > 7.99m)
            {
                IN10_sales = @"❤❤";
            }
        }

        protected void Cal_IN11_mass()
        {
            if ((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 <= 2.4m)
            {
                IN11_mass = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 > 2.4m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 <= 3m))
            {
                IN11_mass = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 > 3m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 <= 4.99m))
            {
                IN11_mass = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 > 4.99m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 <= 7.99m))
            {
                IN11_mass = @"❤❤❤";
            }
            else if ((FingerAnalysis.M1QPR + FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/3 > 7.99m)
            {
                IN11_mass = @"❤❤";
            }

        }

        protected void Cal_IN12_lang()
        {
            if ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 2.4m)
            {
                IN12_lang = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 2.4m) &&
                     ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 3))
            {
                IN12_lang = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 >= 3m) &&
                     ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 4.99m))
            {
                IN12_lang = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 4.99m) &&
                     ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 7.99m))
            {
                IN12_lang = @"❤❤❤";
            }
            else if ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 7.99m)
            {
                IN12_lang = @"❤❤";
            }
        }

        protected void Cal_IN13_lit()
        {
            if ((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 <= 2.4m)
            {
                IN13_lit = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 > 2.4m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 <= 3))
            {
                IN13_lit = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 > 3m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 <= 4.99m))
            {
                IN13_lit = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 > 4.99m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 <= 7.99m))
            {
                IN13_lit = @"❤❤❤";
            }
            else if ((FingerAnalysis.M1QPR + FingerAnalysis.M8QPR)/2 > 7.99m)
            {
                IN13_lit = @"❤❤";
            }
        }

        protected void Cal_IN14_edu()
        {
            if ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 2.4m)
            {
                IN14_edu = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 2.4m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 3m))
            {
                IN14_edu = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 3m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 4.99m))
            {
                IN14_edu = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 4.99m) &&
                     ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 7.99m))
            {
                IN14_edu = @"❤❤❤";
            }
            else if ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 7.99m)
            {
                IN14_edu = @"❤❤";
            }
        }

        protected void Cal_IN15_pol()
        {
            if ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 2.4m)
            {
                IN15_pol = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 3))
            {
                IN15_pol = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 3)
                     && ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 4.99m))
            {
                IN15_pol = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 <= 7.99m))
            {
                IN15_pol = @"❤❤❤";
            }
            else if ((FingerAnalysis.M1QPR + FingerAnalysis.M2QPR + FingerAnalysis.M7QPR)/3 > 7.99m)
            {
                IN15_pol = @"❤❤";
            }
        }

        protected void Cal_IN16_mgn()
        {
            if ((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 <= 2.4m)
            {
                IN16_mgn = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 <= 3m))
            {
                IN16_mgn = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 > 3m)
                     && ((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 <= 4.99m))
            {
                IN16_mgn = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 <= 7.99m))
            {
                IN16_mgn = @"❤❤❤";
            }
            else if ((FingerAnalysis.M1QPR + (FingerAnalysis.M8QPR) + FingerAnalysis.M7QPR)/3 > 7.99m)
            {
                IN16_mgn = @"❤❤";
            }
        }

        protected void Cal_IN17_fin()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 <= 2.4m)
            {
                IN17_fin = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 <= 3m))
            {
                IN17_fin = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 <= 4.99m))
            {
                IN17_fin = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 <= 7.99m))
            {
                IN17_fin = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M2QPR)/2 > 7.99m)
            {
                IN17_fin = @"❤❤";
            }
        }

        protected void Cal_IN18_spo()
        {
            if ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 2.4m)
            {
                IN18_spo = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 2.4m)
                     && ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 3m))
            {
                IN18_spo = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 3m)
                     && ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 4.99m))
            {
                IN18_spo = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 4.99m)
                     && ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 <= 7.99m))
            {
                IN18_spo = @"❤❤❤";
            }
            else if ((FingerAnalysis.M6QPR + FingerAnalysis.M3QPR)/2 > 7.99m)
            {
                IN18_spo = @"❤❤";
            }
        }

        protected void Cal_IN19_fin2()
        {
            if ((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 <= 2.4m)
            {
                IN19_fin2 = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 <= 3m))
            {
                IN19_fin2 = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 > 3m)
                     && ((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 <= 4.99m))
            {
                IN19_fin2 = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 <= 7.99m))
            {
                IN19_fin2 = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + (FingerAnalysis.M3QPR) + FingerAnalysis.M7QPR)/3 > 7.99m)
            {
                IN19_fin2 = @"❤❤";
            }
        }

        protected void Cal_IN2_eng()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 2.4m)
            {
                IN2_eng = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 3m))
            {
                IN2_eng = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 4.99m))
            {
                IN2_eng = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 7.99m))
            {
                IN2_eng = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 7.99m)
            {
                IN2_eng = @"❤❤";
            }
        }

        protected void Cal_IN3_math()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 2.4m)
            {
                IN3_math = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 3m))
            {
                IN3_math = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 4.99m))
            {
                IN3_math = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 7.99m))
            {
                IN3_math = @"❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 7.99m))
            {
                IN3_math = @"❤❤";
            }
        }

        protected void Cal_IN4_med()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 <= 2.4m)
            {
                IN4_med = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 <= 3m))
            {
                IN4_med = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 <= 4.99m))
            {
                IN4_med = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 <= 7.99m))
            {
                IN4_med = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M6QPR)/3 > 7.99m)
            {
                IN4_med = @"❤❤";
            }
        }

        protected void Cal_IN5_life()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 <= 2.4m)
            {
                IN5_life = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 <= 3m))
            {
                IN5_life = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 <= 4.99m))
            {
                IN5_life = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 <= 7.99m))
            {
                IN5_life = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR + FingerAnalysis.M3QPR)/3 > 7.99m)
            {
                IN5_life = @"❤❤";
            }
        }

        protected void Cal_IN6_agri()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 <= 2.4m)
            {
                IN6_agri = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 <= 3))
            {
                IN6_agri = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 > 3)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 <= 4.99m))
            {
                IN6_agri = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 <= 7.99m))
            {
                IN6_agri = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M4QPR)/2 > 7.99m)
            {
                IN6_agri = @"❤❤";
            }
        }

        protected void Cal_IN7_earth()
        {
            if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 2.4m)
            {
                IN7_earth = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 3m))
            {
                IN7_earth = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 3m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 4.99m))
            {
                IN7_earth = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 <= 7.99m))
            {
                IN7_earth = @"❤❤❤";
            }
            else if ((FingerAnalysis.M2QPR + FingerAnalysis.M3QPR + FingerAnalysis.M4QPR)/3 > 7.99m)
            {
                IN7_earth = @"❤❤";
            }
        }

        protected void Cal_IN9_art()
        {
            if ((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 <= 2.4m)
            {
                IN9_art = @"❤❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 > 2.4m)
                     && ((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 <= 3))
            {
                IN9_art = @"❤❤❤❤❤";
            }
            else if (((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 > 3)
                     && ((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 <= 4.99m))
            {
                IN9_art = @"❤❤❤❤";
            }
            else if (((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 > 4.99m)
                     && ((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 <= 7.99m))
            {
                IN9_art = @"❤❤❤";
            }
            else if ((FingerAnalysis.M5QPR + FingerAnalysis.M3QPR + FingerAnalysis.M6QPR)/3 > 7.99m)
            {
                IN9_art = @"❤❤";
            }
        }

        protected void Cal_L1RCBVA()
        {
            this.L1RCBVA = FingerAnalysis.L1RCB*RCVA;
        }

        protected void Cal_L1RCD()
        {
            if (FingerAnalysis.L1T == "AS")
            {
                L1RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }

            else if (FingerAnalysis.L1T == "AT")
            {
                L1RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                L1RCD = (FingerAnalysis.L1RCB*0.97m);
            }
        }

        protected void Cal_L1TCT()
        {
            switch (FingerAnalysis.L1T)
            {
                case "Wt":
                case "We":
                case "Ws":
                case "Wi":
                case "Wp":
                case "Wc":
                case "Wd":
                    this.L1TCT = "W";
                    break;
                case "U":
                case "Lf":
                    this.L1TCT = "U";
                    break;
                case "R":
                case "Wlr":
                case "Wpr":
                    this.L1TCT = "R";
                    break;
                case "As":
                case "Ae":
                case "Au":
                case "Ar":
                case "At":
                case "Nx":
                case "Mx":
                case "Xa":
                    this.L1TCT = "X";
                    break;

                default:
                    this.L1TCT = "";
                    break;
            }
        }

        protected void Cal_L2RCBVA()
        {
            this.L2RCBVA = FingerAnalysis.L2RCB*RCVA;
        }

        protected void Cal_L2RCD()
        {
            if (FingerAnalysis.L2T == "AS")
            {
                L2RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.L2T == "AT")
            {

                L2RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) +
                         ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                L2RCD = (FingerAnalysis.L2RCB*0.97m);
            }
        }

        protected void Cal_L3RCBVA()
        {
            this.L3RCBVA = FingerAnalysis.L3RCB*@RCVA;
        }

        protected void Cal_L3RCD()
        {
            if (FingerAnalysis.L3T == "AS")
            {
                L3RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.L3T == "AT")
            {

                L3RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) +
                         ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                L3RCD = (FingerAnalysis.L3RCB*0.97m);
            }
        }

        protected void Cal_L4RCBVA()
        {
            this.L4RCBVA = FingerAnalysis.L4RCB*@RCVA;
        }

        protected void Cal_L4RCD()
        {
            if (FingerAnalysis.L4T == "AS")
            {
                L4RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.L4T == "AT")
            {
                L4RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) +
                         ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                L4RCD = (FingerAnalysis.L4RCB*0.97m);
            }
        }

        protected void Cal_L5RCBVA()
        {
            this.L5RCBVA = FingerAnalysis.L5RCB*@RCVA;
        }

        protected void Cal_L5RCD()
        {
            if (FingerAnalysis.L5T == "AS")
            {
                L5RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.L5T == "AT")
            {
                L5RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) +
                         ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                L5RCD = (FingerAnalysis.L5RCB*0.97m);
            }
        }

        protected void Cal_L5RCP_Final()
        {
            this.L5RCP_Final =
                100 - FingerAnalysis.L1RCP - FingerAnalysis.L2RCP - FingerAnalysis.L3RCP - FingerAnalysis.L4RCP -
                FingerAnalysis.L5RCP - FingerAnalysis.R1RCP - FingerAnalysis.R2RCP - FingerAnalysis.R3RCP -
                FingerAnalysis.R4RCP - FingerAnalysis.R5RCP + FingerAnalysis.L5RCP;
        }

        protected void Cal_LTRC()
        {
            this.LTRC = FingerAnalysis.L1RCB + FingerAnalysis.L2RCB + FingerAnalysis.L3RCB + FingerAnalysis.L4RCB +
                        FingerAnalysis.L5RCB;
        }

        protected void Cal_LTRCP()
        {
            this.LTRCP = ((FingerAnalysis.R1RCP + FingerAnalysis.R2RCP + FingerAnalysis.R3RCP + FingerAnalysis.R4RCP +
                           FingerAnalysis.R5RCP)/
                          (FingerAnalysis.L1RCP + FingerAnalysis.L2RCP + FingerAnalysis.L3RCP + FingerAnalysis.L4RCP +
                           FingerAnalysis.L5RCP + FingerAnalysis.R1RCP + FingerAnalysis.R2RCP + FingerAnalysis.R3RCP +
                           FingerAnalysis.R4RCP + FingerAnalysis.R5RCP))*100;
        }

        protected void Cal_M1QP()
        {
            this.M1QP = FingerAnalysis.M1Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_M2QP()
        {
            this.M2QP = FingerAnalysis.M2Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_M3QP()
        {
            this.M3QP = FingerAnalysis.M3Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_M4QP()
        {
            this.M4QP = FingerAnalysis.M4Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)
                        *100;
        }

        protected void Cal_M5QP()
        {
            this.M5QP = FingerAnalysis.M5Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_M6QP()
        {
            this.M6QP = FingerAnalysis.M6Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_M7QP()
        {
            this.M7QP = FingerAnalysis.M7Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_M8QP()
        {
            this.M8QP = FingerAnalysis.M8Q/
                        (FingerAnalysis.M1Q + FingerAnalysis.M2Q + FingerAnalysis.M3Q + FingerAnalysis.M4Q +
                         FingerAnalysis.M5Q + FingerAnalysis.M6Q + FingerAnalysis.M7Q + FingerAnalysis.M8Q)*100;
        }

        protected void Cal_MA1()
        {
            if (FingerAnalysis.M1QPR == 1)
            {
                MA1 = @"★★★★★";
            }
            else if ((FingerAnalysis.M1QPR >= 1.01m) && (FingerAnalysis.M1QPR <= 3m))
            {
                MA1 = @"★★★★";
            }
            else if ((FingerAnalysis.M1QPR >= 3.01m) && (FingerAnalysis.M1QPR <= 4.99m))
            {
                MA1 = @"★★★";
            }
            else if ((FingerAnalysis.M1QPR >= 5m) && (FingerAnalysis.M1QPR <= 7.99m))
            {
                MA1 = @"★★";
            }
            else if (FingerAnalysis.M1QPR == 8m)
            {
                MA1 = @"★";
            }
        }

        protected void Cal_MA2()
        {
            if (FingerAnalysis.M2QPR == 1)
            {
                MA2 = @"★★★★★";
            }
            else if ((FingerAnalysis.M2QPR >= 1.01m) && (FingerAnalysis.M2QPR <= 3m))
            {
                MA2 = @"★★★★";
            }
            else if ((FingerAnalysis.M2QPR >= 3.01m) && (FingerAnalysis.M2QPR <= 4.99m))
            {
                MA2 = @"★★★";
            }
            else if ((FingerAnalysis.M2QPR >= 5m) && (FingerAnalysis.M2QPR <= 7.99m))
            {
                MA2 = @"★★";
            }
            else if (FingerAnalysis.M2QPR == 8m)
            {
                MA2 = @"★";
            }
        }

        protected void Cal_MA3()
        {
            if (FingerAnalysis.M3QPR == 1)
            {
                MA3 = @"★★★★★";
            }
            else if ((FingerAnalysis.M3QPR >= 1.01m) && (FingerAnalysis.M3QPR <= 3m))
            {
                MA3 = @"★★★★";
            }
            else if ((FingerAnalysis.M3QPR >= 3.01m) && (FingerAnalysis.M3QPR <= 4.99m))
            {
                MA3 = @"★★★";
            }
            else if ((FingerAnalysis.M3QPR >= 5m) && (FingerAnalysis.M3QPR <= 7.99m))
            {
                MA3 = @"★★";
            }
            else if (FingerAnalysis.M3QPR == 8m)
            {
                MA3 = @"★";
            }
        }

        protected void Cal_MA4()
        {
            if (FingerAnalysis.M4QPR == 1)
            {
                MA4 = @"★★★★★";
            }
            else if ((FingerAnalysis.M4QPR >= 1.01m) && (FingerAnalysis.M4QPR <= 3m))
            {
                MA4 = @"★★★★";
            }
            else if ((FingerAnalysis.M4QPR >= 3.01m) && (FingerAnalysis.M4QPR <= 4.99m))
            {
                MA4 = @"★★★";
            }
            else if ((FingerAnalysis.M4QPR >= 5m) && (FingerAnalysis.M4QPR <= 7.99m))
            {
                MA4 = @"★★";
            }
            else if (FingerAnalysis.M4QPR == 8m)
            {
                MA4 = @"★";
            }
        }

        protected void Cal_MA5()
        {
            if (FingerAnalysis.M5QPR == 1)
            {
                MA5 = @"★★★★★";
            }
            else if ((FingerAnalysis.M5QPR >= 1.01m) && (FingerAnalysis.M5QPR <= 3m))
            {
                MA5 = @"★★★★";
            }
            else if ((FingerAnalysis.M5QPR >= 3.01m) && (FingerAnalysis.M5QPR <= 4.99m))
            {
                MA5 = @"★★★";
            }
            else if ((FingerAnalysis.M5QPR >= 5m) && (FingerAnalysis.M5QPR <= 7.99m))
            {
                MA5 = @"★★";
            }
            else if (FingerAnalysis.M5QPR == 8m)
            {
                MA5 = @"★";
            }
        }

        protected void Cal_MA6()
        {
            if (FingerAnalysis.M6QPR == 1)
            {
                MA6 = @"★★★★★";
            }
            else if ((FingerAnalysis.M6QPR >= 1.01m) && (FingerAnalysis.M6QPR <= 3m))
            {
                MA6 = @"★★★★";
            }
            else if ((FingerAnalysis.M6QPR >= 3.01m) && (FingerAnalysis.M6QPR <= 4.99m))
            {
                MA6 = @"★★★";
            }
            else if ((FingerAnalysis.M6QPR >= 5m) && (FingerAnalysis.M6QPR <= 7.99m))
            {
                MA6 = @"★★";
            }
            else if (FingerAnalysis.M6QPR == 8m)
            {
                MA6 = @"★";
            }
        }

        protected void Cal_MA7()
        {
            if (FingerAnalysis.M7QPR == 1)
            {
                MA7 = @"★★★★★";
            }
            else if ((FingerAnalysis.M7QPR >= 1.01m) && (FingerAnalysis.M7QPR <= 3m))
            {
                MA7 = @"★★★★";
            }
            else if ((FingerAnalysis.M7QPR >= 3.01m) && (FingerAnalysis.M7QPR <= 4.99m))
            {
                MA7 = @"★★★";
            }
            else if ((FingerAnalysis.M7QPR >= 5m) && (FingerAnalysis.M7QPR <= 7.99m))
            {
                MA7 = @"★★";
            }
            else if (FingerAnalysis.M7QPR == 8m)
            {
                MA7 = @"★";
            }
        }

        protected void Cal_MA8()
        {
            if (FingerAnalysis.M8QPR == 1)
            {
                MA8 = @"★★★★★";
            }
            else if ((FingerAnalysis.M8QPR >= 1.01m) && (FingerAnalysis.M8QPR <= 3m))
            {
                MA8 = @"★★★★";
            }
            else if ((FingerAnalysis.M8QPR >= 3.01m) && (FingerAnalysis.M8QPR <= 4.99m))
            {
                MA8 = @"★★★";
            }
            else if ((FingerAnalysis.M8QPR >= 5m) && (FingerAnalysis.M8QPR <= 7.99m))
            {
                MA8 = @"★★";
            }
            else if (FingerAnalysis.M8QPR == 8m)
            {
                MA8 = @"★";
            }
        }

        protected void Cal_MALATD()
        {
            if ((FingerAnalysis.ATDL) < 35)
            {
                MALATD = @"★★★★★";
            }
            else if ((FingerAnalysis.ATDL) > 35 &&
                     (FingerAnalysis.ATDL) < 40)
            {
                MALATD = @"★★★★☆";
            }
            else if ((FingerAnalysis.ATDL) > 39 &&
                     (FingerAnalysis.ATDL) < 46)
            {
                MALATD = @"★★★☆☆";
            }
            else if ((FingerAnalysis.ATDL) > 45 &&
                     (FingerAnalysis.ATDL) < 56)
            {
                MALATD = @"★★☆☆☆";
            }
            else if ((FingerAnalysis.ATDL) > 55)
            {
                MALATD = @"★☆☆☆☆";
            }
        }

        protected void Cal_MARATD()
        {
            if ((FingerAnalysis.ATDR) < 35)
            {
                MARATD = @"★★★★★";
            }
            else if ((FingerAnalysis.ATDR) > 35 &&
                     (FingerAnalysis.ATDR) < 40)
            {
                MARATD = @"★★★★☆";
            }
            else if ((FingerAnalysis.ATDR) > 39 &&
                     (FingerAnalysis.ATDR) < 46)
            {
                MARATD = @"★★★☆☆";
            }
            else if ((FingerAnalysis.ATDR) > 45 &&
                     (FingerAnalysis.ATDR) < 56)
            {
                MARATD = @"★★☆☆☆";
            }
            else if ((FingerAnalysis.ATDR) > 55)
            {
                MARATD = @"★☆☆☆☆";
            }
        }

        protected void Cal_plus_X()
        {
            if (FingerAnalysis.AC2XP == 10)
            {
                plus_X = @"+X";
            }
            else if (FingerAnalysis.AC2XP == 20)
            {
                plus_X = @"+2X";
            }
            else if (FingerAnalysis.AC2XP == 30)
            {
                plus_X = @"+3X";
            }
            else if (FingerAnalysis.AC2XP == 40)
            {
                plus_X = @"+4X";
            }
            else if (FingerAnalysis.AC2XP == 50)
            {
                plus_X = @"+5X";
            }
            else if (FingerAnalysis.AC2XP == 60)
            {
                plus_X = @"+6X";
            }
            else if (FingerAnalysis.AC2XP == 70)
            {
                plus_X = @"+7X";
            }
            else if (FingerAnalysis.AC2XP == 80)
            {
                plus_X = @"+8X";
            }
            else if (FingerAnalysis.AC2XP == 90)
            {
                plus_X = @"+9X";
            }
            else if (FingerAnalysis.AC2XP == 100)
            {
                plus_X = @"+10X";
            }

        }

        protected void Cal_R1N()
        {
        }

        protected void Cal_R1RCBVA()
        {
            this.R1RCBVA = FingerAnalysis.R1RCB*RCVA;
        }

        protected void Cal_R1RCD()
        {
            if (FingerAnalysis.R1T == "AS")
            {
                this.R1RCD = (FingerAnalysis.TFRC)/
                             (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                              ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }

            else if (FingerAnalysis.R1T == "AT")
            {
                this.R1RCD = (FingerAnalysis.TFRC)/
                             (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                              ToNumber(FingerAnalysis.AC2RP) +
                              ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                this.R1RCD = (FingerAnalysis.R1RCB*0.97m);
            }
        }

        protected void Cal_R1TCT()
        {
            switch (FingerAnalysis.R1T)
            {
                case "Wt":
                case "We":
                case "Ws":
                case "Wi":
                case "Wp":
                case "Wc":
                case "Wd":
                    R1TCT = "W";
                    break;
                case "U":
                case "Lf":
                    R1TCT = "U";
                    break;
                case "Wlr":
                case "Wpr":
                case "R":
                    R1TCT = "R";
                    break;
                case "As":
                case "Ae":
                case "Au":
                case "Ar":
                case "At":
                case "Nx":
                case "Mx":
                case "Xa":
                    R1TCT = "X";
                    break;
                default:
                    R1TCT = "";
                    break;
            }
        }

        protected void Cal_R2RCBVA()
        {
            this.R2RCBVA = FingerAnalysis.R2RCB*RCVA;
        }

        protected void Cal_R2RCD()
        {
            if (FingerAnalysis.R2T == "AS")
            {
                this.R2RCD = (FingerAnalysis.TFRC)/
                             (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                              ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.R2T == "AT")
            {

                this.R2RCD = (FingerAnalysis.TFRC)/
                             (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                              ToNumber(FingerAnalysis.AC2RP) +
                              ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                this.R2RCD = (FingerAnalysis.R2RCB*0.99m);
            }
        }

        protected void Cal_R3RCBVA()
        {
            this.R3RCBVA = FingerAnalysis.R3RCB*RCVA;
        }

        protected void Cal_R3RCD()
        {
            if (FingerAnalysis.R3T == "AS")
            {
                R3RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.R3T == "AT")
            {
                R3RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) +
                         ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                R3RCD = (FingerAnalysis.R3RCB*1.02m);
            }
        }

        protected void Cal_R4RCBVA()
        {
            this.R4RCBVA = FingerAnalysis.R4RCB*RCVA;
        }

        protected void Cal_R4RCD()
        {
            if (FingerAnalysis.R4T == "AS")
            {
                R4RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else if (FingerAnalysis.R4T == "AT")
            {
                R4RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                R4RCD = (FingerAnalysis.R4RCB*1.03m);
            }

        }

        protected void Cal_R5RCBVA()
        {
            this.R5RCBVA = FingerAnalysis.R5RCB*RCVA;
        }

        protected void Cal_R5RCD()
        {
            if (FingerAnalysis.R5T == "AS")
            {
                R5RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }

            else if (FingerAnalysis.R5T == "AT")
            {
                R5RCD = (FingerAnalysis.TFRC)/
                        (2*ToNumber(FingerAnalysis.AC2WP) + ToNumber(FingerAnalysis.AC2UP) +
                         ToNumber(FingerAnalysis.AC2RP) + ToNumber(FingerAnalysis.AC2SP));
            }
            else
            {
                R5RCD = (FingerAnalysis.R5RCB*1.07m);
            }
        }

        protected void Cal_Rank_AHP()
        {
            if (FingerAnalysis.RCAP >= FingerAnalysis.RCHP)
            {
                if (FingerAnalysis.RCHP >= FingerAnalysis.RCVP)
                {
                    Rank_AHP = @"KAV";
                }
                else if (FingerAnalysis.RCAP >= FingerAnalysis.RCVP)
                {
                    Rank_AHP = @"KVA";
                }
                else
                    Rank_AHP = @"VKA";
            }
            else if (FingerAnalysis.RCAP < FingerAnalysis.RCHP)
            {
                if (FingerAnalysis.RCAP >= FingerAnalysis.RCVP)
                {
                    Rank_AHP = @"AKV";
                }
                else if (FingerAnalysis.RCHP >= FingerAnalysis.RCVP)
                {
                    Rank_AHP = @"AVK";
                }
                else
                    Rank_AHP = @"VAK";
            }
        }


        protected void Cal_RCVA()
        {
            if (RTRC > LTRC)
            {
                this.RCVA = RTRC/LTRC;
            }

            else
            {
                this.RCVA = LTRC/RTRC;
            }
        }

        protected void Cal_RTRC()
        {
            this.RTRC = FingerAnalysis.R1RCB + FingerAnalysis.R2RCB + FingerAnalysis.R3RCB + FingerAnalysis.R4RCB +
                        FingerAnalysis.R5RCB;
        }

        protected void Cal_RTRCP()
        {
            this.RTRCP = ((FingerAnalysis.L1RCP + FingerAnalysis.L2RCP + FingerAnalysis.L3RCP + FingerAnalysis.L4RCP +
                           FingerAnalysis.L5RCP)/
                          (FingerAnalysis.L1RCP + FingerAnalysis.L2RCP + FingerAnalysis.L3RCP + FingerAnalysis.L4RCP +
                           FingerAnalysis.L5RCP + FingerAnalysis.R1RCP + FingerAnalysis.R2RCP + FingerAnalysis.R3RCP +
                           FingerAnalysis.R4RCP + FingerAnalysis.R5RCP))*100;
        }

        protected void Cal_T1BP_2()
        {
            this.T1BP_2 = (100 - FingerAnalysis.T1AP - FingerAnalysis.T1BP) + FingerAnalysis.T1BP;
        }

        protected void Cal_Vrank()
        {
        }

        #endregion
        
    }
}