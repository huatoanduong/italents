﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using iTalent.Analyse.Model;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFormularService
    {
        void CalculateRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
            List<FIFingerType> fingerTypes);
    }

    public class FormularService : IFormularService
    {
        //protected void calculateFingerRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
        //    List<FIFingerType> fingerTypes , out decimal[] typeValue)
        //{
        //    FIFingerType type;
        //    int count;
        //    int fingerTypesCount = fingerTypes.Count;
        //    typeValue = new decimal[fingerTypesCount];

        //    fingerTypes = fingerTypes.OrderBy(p => p.ID).ToList();

        //    //Chỉ lấy các finger type có chữ L cuối (Left
        //    List<FIFingerRecord> compareRecords = fingerRecords.Where(p => p.Type.EndsWith("L")).ToList();
        //    FIFingerRecord L1 = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L1")); //Ngón cái trái
        //    FIFingerRecord R1 = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R1")); // Ngón cái phải

        //    FIFingerRecord RLrecord = compareRecords.FirstOrDefault(p => p.FingerType == "RL");
        //    List<FIFingerRecord> Archrecords = compareRecords.Where(p => p.FingerType.StartsWith("A")).ToList();

        //    //Chi can ngon cai la whorl thi chiem 30% con cac dieu kien phu khong can check nua
        //    //Truong hop 10 whorl thi moi ngon 10 % phan bo y chang thuong
        //    if ((L1.FingerType.StartsWith("W") || R1.FingerType.StartsWith("W"))
        //        && compareRecords.Count(p => p.FingerType.StartsWith("W")) < 10)
        //    {
        //        int totalPercent = 100;
        //        int totalFinger = 10;
        //        decimal percentPerFinger;
        //        List<FIFingerRecord> exceptList = compareRecords;
        //        if (L1.FingerType.StartsWith("W"))
        //        {
        //            exceptList = exceptList.Where(p => !p.Type.StartsWith("L1")).ToList();
        //            totalPercent -= 30;
        //            totalFinger--;
        //        }
        //        if (R1.FingerType.StartsWith("W"))
        //        {
        //            exceptList = exceptList.Where(p => !p.Type.StartsWith("R1")).ToList();
        //            totalPercent -= 30;
        //            totalFinger--;
        //        }
        //        percentPerFinger = (decimal)totalPercent / (decimal)totalFinger;
        //        foreach (FIFingerRecord record in exceptList)
        //        {
        //            int index = getFingerTypeIndex(record.FingerType, fingerTypes);
        //            if (index >= 0)
        //            {
        //                typeValue[index] += percentPerFinger;
        //            }
        //        }
        //        if (L1.FingerType.StartsWith("W"))
        //        {
        //            typeValue[getFingerTypeIndex(L1.FingerType, fingerTypes)] += 30;
        //        }
        //        if (R1.FingerType.StartsWith("W"))
        //        {
        //            typeValue[getFingerTypeIndex(R1.FingerType, fingerTypes)] += 30;
        //        }
        //    }

        //    //---(2) Nếu có Arch và có cả RL thì
        //    //Nếu có 1 ngón có RL, thì các ngón còn lại có Arch sẽ bị bớt qua ngón RL 5 %
        //    else if (Archrecords.Count > 0 && RLrecord != null)
        //    {
        //        int archCount = 0;
        //        foreach (FIFingerRecord record in compareRecords)
        //        {
        //            int index = getFingerTypeIndex(record.FingerType, fingerTypes);
        //            if (index >= 0)
        //            {
        //                if (record.FingerType.StartsWith("A"))
        //                {
        //                    typeValue[index] = 5;
        //                }
        //                else
        //                {
        //                    typeValue[index] = 10;
        //                }
        //            }
        //        }
        //        typeValue[getFingerTypeIndex(RLrecord.FingerType, fingerTypes)] = 10 + 5 * archCount;
        //    }

        //    //Trường hợp còn lại
        //    else
        //    {
        //        //for (int i = 0; i < fingerTypesCount; i++)
        //        //{
        //        //    type = fingerTypes[i];
        //        //    count = 0;
        //        //    foreach (FIFingerRecord record in fingerRecords)
        //        //    {
        //        //        if (!record.FingerType.IsBlank() && type.Types.Contains(record.FingerType))
        //        //        {
        //        //            count++;
        //        //        }
        //        //    }
        //        //    typeValue[i] = count * 10;
        //        //}

        //        foreach (FIFingerRecord record in compareRecords)
        //        {
        //            int index = getFingerTypeIndex(record.FingerType, fingerTypes);
        //            if (index >= 0)
        //            {
        //                typeValue[index] += 10;
        //            }
        //        }
        //    }
        //}

        //protected void calculateFingerRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
        //    List<FIFingerType> fingerTypes, out FingerTypeValueCollection fingerTypeValue)
        //{
        //    fingerTypeValue = new FingerTypeValueCollection(fingerTypes);
        //    RecordTypeCollection records = new RecordTypeCollection(fingerRecords, fingerTypeValue);

        //    if (records.CountStartWithType("W") == 10)
        //    {
        //        foreach (RecordTypeModel record in records.CompareRecords)
        //        {
        //            record.FingerTypeModel.Value += 10;
        //        }
        //        return;
        //    }

        //    decimal totalPercent = 100;
        //    int totalFinger = 10;
        //    decimal percentPerFinger;

        //    if (records.L1T.FingerType.StartsWith("W") || records.R1T.FingerType.StartsWith("W"))
        //    {
        //        RecordTypeModel L1 = records.L1T;
        //        RecordTypeModel R1 = records.R1T;

        //        List<RecordTypeModel> leftFingers = null;
        //        leftFingers = records.GetListNotThumb();

        //        if (L1.FingerType.StartsWith("W") && R1.FingerType.StartsWith("W"))
        //        {
        //            L1.FingerTypeModel.Value += 30;
        //            R1.FingerTypeModel.Value += 30;
        //            totalFinger -= 2;
        //            totalPercent -= 60;
        //        }
        //        else
        //        {
        //            if (L1.FingerType.StartsWith("W"))
        //            {
        //                L1.FingerTypeModel.Value += 30;
        //                totalFinger--;
        //                leftFingers.Add(R1);
        //            }
        //            else
        //            {
        //                R1.FingerTypeModel.Value += 30;
        //                totalFinger--;
        //                leftFingers.Add(L1);
        //            }
        //            totalPercent -= 30;
        //        }

        //        int leftWCount = leftFingers.Count(p => p.FingerType.StartsWith("W"));
        //        decimal notThumbWPercent = leftWCount * 5;
        //        totalPercent -= notThumbWPercent;
        //        totalFinger -= leftWCount;

        //        percentPerFinger = totalPercent / totalFinger;

        //        foreach (RecordTypeModel model in leftFingers)
        //        {
        //            if (model.FingerType.StartsWith("W"))
        //            {
        //                model.FingerTypeModel.Value += 5;
        //            }
        //            else
        //            {
        //                model.FingerTypeModel.Value += percentPerFinger;
        //            }
        //        }
        //        return;
        //    }

        //    if (records.CountStartWithType("A") > 0)
        //    {
        //        percentPerFinger = 10;
        //        foreach (RecordTypeModel model in records.CompareRecords)
        //        {
        //            model.FingerTypeModel.Value += percentPerFinger;
        //        }
        //        fingerTypeValue.ConCu.Value -= 5;
        //        fingerTypeValue.CaHeo.Value += 5;
        //    }

        //    //All left cases

        //    percentPerFinger = 10;
        //    foreach (RecordTypeModel model in records.CompareRecords)
        //    {
        //        model.FingerTypeModel.Value += percentPerFinger;
        //    }
        //}

        protected void calculateFingerRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
            List<FIFingerType> fingerTypes, out FingerTypeModelCollection fingerTypeModel)
        {
            fingerTypeModel = new FingerTypeModelCollection(fingerTypes);
            RecordTypeCollection records = new RecordTypeCollection(fingerRecords, fingerTypeModel);

            if (records.CountStartWithType("W") == 10)
            {
                foreach (RecordTypeModel record in records.CompareRecords)
                {
                    if (record.FingerType.StartsWith("LF"))
                    {
                        fingerTypeModel.NhomLF.Value += 10;
                    }
                    record.FingerTypeModel.Value += 10;
                }
                return;
            }

            decimal totalPercent = 100;
            int totalFinger = 10;
            decimal percentPerFinger;

            bool isWLeft = records.L1T.FingerType.StartsWith("W");
            bool isWRight = records.R1T.FingerType.StartsWith("W");
            List<RecordTypeModel> notThumbList = records.GetListNotThumb();

            if (isWLeft ^ isWRight)
            {
                if (isWLeft)
                {
                    records.L1T.FingerTypeModel.Value += 26;

                    if (records.R1T.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += 10 / 2;
                        fingerTypeModel.CaHeo.Value += 10 / 2;
                    }
                    else
                    {
                        if(records.R1T.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += 10;
                        }
                        records.R1T.FingerTypeModel.Value += 10;
                    }
                }
                if (isWRight)
                {
                    records.R1T.FingerTypeModel.Value += 26;
                    if (records.L1T.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += 10 / 2;
                        fingerTypeModel.CaHeo.Value += 10 / 2;
                    }
                    else
                    {
                        if (records.L1T.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += 10;
                        }
                        records.L1T.FingerTypeModel.Value += 10;
                    }
                }

                percentPerFinger = 8;
                foreach (RecordTypeModel model in notThumbList)
                {
                    if (model.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += percentPerFinger / 2;
                        fingerTypeModel.CaHeo.Value += percentPerFinger / 2;
                    }
                    else
                    {
                        if (model.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += percentPerFinger;
                        }
                        model.FingerTypeModel.Value += percentPerFinger;
                    }
                }
                return;
            }
            else if (isWLeft && isWRight)
            {
                records.L1T.FingerTypeModel.Value += 30;
                records.R1T.FingerTypeModel.Value += 30;

                percentPerFinger = 5;
                foreach (RecordTypeModel model in notThumbList)
                {
                    if (model.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += percentPerFinger / 2;
                        fingerTypeModel.CaHeo.Value += percentPerFinger / 2;
                    }
                    else
                    {
                        if (model.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += percentPerFinger;
                        }
                        model.FingerTypeModel.Value += percentPerFinger;
                    }
                }
                return;
            }

            percentPerFinger = 10;
            foreach (RecordTypeModel model in records.CompareRecords)
            {
                if (model.FingerType.StartsWith("AR"))
                {
                    fingerTypeModel.ConCu.Value += percentPerFinger / 2;
                    fingerTypeModel.CaHeo.Value += percentPerFinger / 2;
                }
                else
                {
                    if (model.FingerType.StartsWith("LF"))
                    {
                        fingerTypeModel.NhomLF.Value += percentPerFinger;
                    }
                    model.FingerTypeModel.Value += percentPerFinger;
                }
            }
            return;
        }

        //protected bool isContainStartWith(List<FIFingerRecord> records, string startString)
        //{
        //    foreach (FIFingerRecord record in records)
        //    {
        //        if (record.FingerType.StartsWith(startString))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //protected int getFingerTypeIndex(string value, List<FIFingerType> types)
        //{
        //    for (int i = 0; i < types.Count; i++)
        //    {
        //        if (!value.IsBlank() && types[i].Types.Contains(value)) { return i; }
        //    }
        //    return -1;
        //}

        protected void updateFingerAnalyse(FIFingerAnalysis fingerAnalysis,
            FingerTypeModelCollection fingerTypeModelCollection)
        {
            //fingerAnalysis.FPType1 = value[0];
            //fingerAnalysis.FPType2 = value[1];
            //fingerAnalysis.FPType3 = value[2];
            //fingerAnalysis.FPType4 = value[3];
            //fingerAnalysis.FPType5 = value[4];
            //fingerAnalysis.FPType6 = value[5];
            //fingerAnalysis.FPType7 = value[6];
            //fingerAnalysis.FPType8 = value[7];

            fingerTypeModelCollection.MapFingerAnalyse(fingerAnalysis);
        }

        public void CalculateRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
            List<FIFingerType> fingerTypes)
        {
            FingerTypeModelCollection fingerTypeModelCollection;
            calculateFingerRecord(analysis, fingerRecords, fingerTypes, out fingerTypeModelCollection);
            updateFingerAnalyse(analysis, fingerTypeModelCollection);
        }
    }
}