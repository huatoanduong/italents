﻿using System;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFITemplateHistoryService : IService
    {
        FITemplateHistory Find(int id);

        bool Add(FITemplateHistory obj);
        bool Update(FITemplateHistory obj);
        bool Delete(FITemplateHistory obj);
    }

    public class FITemplateHistoryService : Pattern.Service, IFITemplateHistoryService
    {
        public FITemplateHistoryService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FITemplateHistory Find(int id)
        {
            FITemplateHistory obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                FITemplateHistoryRepository repository = new FITemplateHistoryRepository(this._unitOfWork);
                obj = repository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public bool Add(FITemplateHistory obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFITemplateHistoryRepository repository = new FITemplateHistoryRepository(this._unitOfWork);
                FITemplateHistory obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã lịch sử template đã tồn tại"
                        : "Template History Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm lịch sử template"
                        : "Cannot add new template history";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FITemplateHistory obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFITemplateHistoryRepository repository = new FITemplateHistoryRepository(this._unitOfWork);
                FITemplateHistory obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã lịch sử template không tồn tại"
                        : "Template History Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin lịch sử template"
                        : "Cannot modify template history infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FITemplateHistory obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFITemplateHistoryRepository repository = new FITemplateHistoryRepository(this._unitOfWork);
                FITemplateHistory obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã lịch sử template không tồn tại"
                        : "Template history Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa lịch sử template"
                        : "Cannot remove template history";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}