﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Bibliography;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IDatFileService : IService
    {
        DatFileModel LoadDatFile(string datFilePath);
        bool WriteDatFile(string datFilePath, DatFileModel model);
        bool MoveDatFile(string sourceFilePath, string desFilePath, DatFileModel model);
    }

    public class DatFileService : Pattern.Service, IDatFileService
    {
        protected const string NullConst = "<NULL>";
    
        public DatFileService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        protected DatFileModel readDatFile_v2(string fileContent)
        {
            DatFileModel model = new DatFileModel();
            FICustomer objCustomer;
            FIAgency objAgency;
            List<FIFingerRecord> fingerRecords;
            objCustomer = new FICustomerService(this._unitOfWork).CreateEntity();
            objAgency = new FIAgencyService(this._unitOfWork).CreateEntity();
            fingerRecords = new List<FIFingerRecord>();
            FIFingerRecord record;
            List<string> strList =
                fileContent.Split(new string[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries).ToList();

            int index = 0;
            int rowCount = strList.Count;
            string tmp;
            int tmpIndex;
            List<string> tmps;

            ///streamWriter.WriteLine("Name: " + objCustomer.Name);
            ///streamWriter.WriteLine("Gender: " + objCustomer.Gender);
            ///streamWriter.WriteLine("Telephone: " + objCustomer.Tel);
            ///streamWriter.WriteLine("Birthday: " + DateTimeUtil.ConvertDate(objCustomer.DOB.ToString()).ToString("MM/dd/yyyy"));
            ///streamWriter.WriteLine("Address: " + objCustomer.Address1);
            ///streamWriter.WriteLine("Email: " + objCustomer.Email);
            ///streamWriter.WriteLine("Parents/Remarks: " + objCustomer.Parent + "/" + objCustomer.Remark);
            ///streamWriter.WriteLine("-----------------------------------------------------------------------------------------");

            objCustomer.Name = strList[index++].Substring("Name: ".Length).ToNullIfEqual(NullConst);
            objCustomer.Gender = strList[index++].Substring("Gender: ".Length).ToNullIfEqual(NullConst);
            objCustomer.Tel = strList[index++].Substring("Telephone: ".Length).ToNullIfEqual(NullConst);
            tmp = strList[index++].Substring("Birthday: ".Length);
            objCustomer.DOB = tmp.ToDateTime("MM/dd/yyyy");
            objCustomer.Address1 = strList[index++].Substring("Address: ".Length).ToNullIfEqual(NullConst);
            objCustomer.Email = strList[index++].Substring("Email: ".Length).ToNullIfEqual(NullConst);
            tmp = strList[index++].Substring("Parents/Remarks: ".Length).ToNullIfEqual(NullConst);
            tmps = tmp.Split('/').ToList();
            objCustomer.Parent = tmps[0].ToNullIfEqual(NullConst);
            objCustomer.Remark = tmps[1].ToNullIfEqual(NullConst);
            index++;

            if (index < rowCount)
            {
                ///streamWriter.WriteLine(
                /// "0|" + 
                /// objAgency.SecKey + "|" + 
                /// objAgency.Name + 
                /// "|0|||" + 
                /// objAgency.Address1 + "|||||||" + 
                /// objAgency.MobileNo + 
                /// "|X||||||||||" + 
                /// objAgency.Username + 
                /// objAgency.Name);

                tmpIndex = 0;
                tmps = strList[index++].Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries).ToList();

                tmpIndex++;
                objAgency.SecKey = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objAgency.Name = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                tmpIndex++;
                objAgency.Address1 = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objAgency.MobileNo = tmps[tmpIndex++].ToNullIfEqual(NullConst);

                ///streamWriter.WriteLine(
                /// "1|" + 
                /// objAgency.SecKey + "|" + 
                /// objCustomer.ReportID + "|" +
                /// DateTimeUtil.ConvertDate(objCustomer.Date.ToString()).ToString("dd/MM/yyyy") + "|" + 
                /// objCustomer.Name + "|" + 
                /// objCustomer.Parent + "|" + 
                /// objCustomer.Gender + "|" + 
                /// DateTimeUtil.ConvertDate(objCustomer.DOB.ToString()).ToString("dd/MM/yyyy") + "|" + 
                /// objCustomer.Address1 + "|" + 
                /// objCustomer.Address2 + "|" + 
                /// objCustomer.City + "|" + 
                /// objCustomer.ZipPosTalCode + "|" + 
                /// objCustomer.State + "|" + 
                /// objCustomer.Country + "|" + 
                /// objCustomer.Tel + "|" + 
                /// objCustomer.Mobile + "|" + 
                /// objCustomer.Remark + "|" + 
                /// objCustomer.Email);

                tmpIndex = 0;
                tmps = strList[index++].Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries).ToList();
                tmpIndex++;
                tmpIndex++; //objAgency.SecKey = tmps[tmpIndex++];
                objCustomer.ReportID = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Date = tmps[tmpIndex++].ToDateTime("dd/MM/yyyy");
                objCustomer.Name = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Parent = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Gender = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.DOB = tmps[tmpIndex++].ToDateTime("dd/MM/yyyy");
                objCustomer.Address1 = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Address2 = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.City = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.ZipPosTalCode = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.State = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Country = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Tel = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Mobile = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Remark = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                objCustomer.Email = tmps[tmpIndex++].ToNullIfEqual(NullConst);

                IFingerRecordService fingerRecordService = new FIFingerRecordService(this._unitOfWork);
                while (index < rowCount)
                {
                    ///streamWriter.WriteLine(
                    /// "2|" + 
                    /// obj.AgencyID + "|" + 
                    /// obj.ReportID + "|" + 
                    /// obj.Type + "|" + 
                    /// obj.ATDPoint + "|" + 
                    /// obj.RCCount + "|" + 
                    /// obj.FingerType + "|" + 
                    /// "");

                    record = fingerRecordService.CreateEntity();
                    tmpIndex = 0;
                    tmps = strList[index++].Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries).ToList();
                    tmpIndex++;
                    record.AgencyID = tmps[tmpIndex++].ToInt();
                    record.ReportID = tmps[tmpIndex++].ToInt();
                    record.Type = tmps[tmpIndex++].ToNullIfEqual(NullConst);
                    record.ATDPoint = tmps[tmpIndex++].ToDecimal();
                    record.RCCount = tmps[tmpIndex++].ToDecimal();
                    record.FingerType = tmps[tmpIndex++].ToNullIfEqual(NullConst);

                    fingerRecords.Add(record);
                }
            }
            model.FingerRecords = fingerRecords;
            model.FIAgency = objAgency;
            model.FICustomer = objCustomer;
            return model;
        }

        protected string writeDatFile(DatFileModel model)
        {
            FICustomer objCustomer = model.FICustomer;
            FIAgency objAgency = model.FIAgency;
            List<FIFingerRecord> fingerRecords = model.FingerRecords;

            string result = "";

            result += @"Name: " + objCustomer.Name.ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"Gender: " + objCustomer.Gender.ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"Telephone: " + objCustomer.Tel.ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"Birthday: " + objCustomer.DOB.ToString("MM/dd/yyyy") +
                      Environment.NewLine;
            result += @"Address: " + objCustomer.Address1.ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"Email: " + objCustomer.Email.ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"Parents/Remarks: " + objCustomer.Parent.ToDefaultIfBlank(NullConst) + "/" +
                      objCustomer.Remark.ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"-----------------------------------------------------------------------------------------" +
                      Environment.NewLine;
            result += @"0|" + objAgency.SecKey.ToDefaultIfBlank(NullConst) + "|" +
                      objAgency.Name.ToDefaultIfBlank(NullConst) + "|0|||" +
                      objAgency.Address1.ToDefaultIfBlank(NullConst) + "|||||||" +
                      objAgency.MobileNo.ToDefaultIfBlank(NullConst) + "|X||||||||||" +
                      (objAgency.Username + objAgency.Name).ToDefaultIfBlank(NullConst) + Environment.NewLine;
            result += @"1|" + objAgency.SecKey.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.ReportID.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Date.ToString("dd/MM/yyyy") + "|" +
                      objCustomer.Name.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Parent.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Gender.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.DOB.ToString("dd/MM/yyyy") + "|" +
                      objCustomer.Address1.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Address2.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.City.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.ZipPosTalCode.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.State.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Country.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Tel.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Mobile.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Remark.ToDefaultIfBlank(NullConst) + "|" +
                      objCustomer.Email.ToDefaultIfBlank(NullConst) +
                      Environment.NewLine;

            if (fingerRecords != null && fingerRecords.Count > 0)
            {
                foreach (FIFingerRecord obj in fingerRecords)
                {

                    result += @"2|" + obj.AgencyID + "|" +
                              obj.ReportID + "|" +
                              obj.Type.ToDefaultIfBlank(NullConst) + "|" +
                              obj.ATDPoint + "|" +
                              obj.RCCount + "|" +
                              obj.FingerType.ToDefaultIfBlank(NullConst) + "|" +
                              Environment.NewLine;
                }
            }
            return result;
        }

        public DatFileModel LoadDatFile(string datFilePath)
        {
            DatFileModel model = null;
            try
            {
                string fileContent;
                using (TextReader tr = new StreamReader(datFilePath))
                {
                    fileContent = tr.ReadToEnd();
                    tr.Close();
                }
                model = readDatFile_v2(fileContent);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                model = null;
            }
            return model;
        }

        public bool WriteDatFile(string datFilePath, DatFileModel model)
        {
            bool res = true;
            try
            {
                string directory = Path.GetDirectoryName(datFilePath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string fileContent = writeDatFile(model);
                using (TextWriter tr = new StreamWriter(datFilePath, false))
                {
                    tr.Write(fileContent);
                    tr.Close();
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }

        public bool MoveDatFile(string sourceFilePath, string desFilePath, DatFileModel model)
        {
            bool res = true;
            try
            {
                var fileContent = writeDatFile(model);
                using (TextWriter tr = new StreamWriter(desFilePath, false))
                {
                    tr.Write(fileContent);
                    tr.Close();
                }

                string sourceFileDirectory = Path.GetDirectoryName(sourceFilePath);
                string desFileDirectory = Path.GetDirectoryName(desFilePath);
                string fileName;
                string[] fileNames = Directory.GetFiles(sourceFileDirectory);
                foreach (string file in fileNames)
                {
                    if (file != sourceFilePath)
                    {
                        fileName = Path.GetFileName(file);
                        fileName = Path.Combine(desFileDirectory, fileName);
                        File.Copy(file, fileName, true);
                    }
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }
    }
}