﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Util;
using iTalent.Utils;
using VAKFomula.Entity;
using VAKFomula.Model;

namespace iTalent.Analyse.Service
{
    public interface IFIFingerAnalysisService : IService
    {
        FIFingerAnalysis Find(int id);

        FIFingerAnalysis CreateEntity(int agencyId, int reportId);

        bool CalculateFingerRecord(int reportId);

        bool Add(FIFingerAnalysis obj);
        bool Update(FIFingerAnalysis obj);
        bool Delete(FIFingerAnalysis obj);
    }

    public class FIFingerAnalysisService : Pattern.Service, IFIFingerAnalysisService
    {
        public FIFingerAnalysisService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {

        }

        public FIFingerAnalysis Find(int id)
        {
            FIFingerAnalysis device = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                device = deviceRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                device = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return device;
        }

        public FIFingerAnalysis CreateEntity(int agencyId, int reportId)
        {
            FIFingerAnalysis analysis =
                new FIFingerAnalysis()
                {
                    AgencyID = agencyId,
                    ReportID = reportId,
                };
            return analysis;
        }

        protected List<FIFingerRecord> fingerRecords { get; set; }

        private int GetRcount(string type, out string p)
        {
            foreach (FIFingerRecord record in this.fingerRecords)
            {
                if (type == record.Type)
                {
                    p = record.FingerType;
                    if (type.Contains("ATD"))
                    {
                        return (int)record.ATDPoint;
                    }
                    return (int)record.RCCount;
                }
            }
            p = "";
            return 0;
        }

        private FingerRecordModel GetFingerRecordModel(int reportId)
        {
            try
            {
                IFIFingerRecordRepository _fingerRecordRepository = new FIFingerRecordRepository(this._unitOfWork);
                fingerRecords = _fingerRecordRepository.FindByRecordId(reportId);

                string p;
                FingerRecordModel model = new FingerRecordModel
                {
                    ATDL = GetRcount("LATD", out p),
                    ATDR = GetRcount("RATD", out p),
                    L1L = GetRcount("L1L", out p),
                    L1R = GetRcount("L1R", out p),
                    L1T = p,
                    L2L = GetRcount("L2L", out p),
                    L2R = GetRcount("L2R", out p),
                    L2T = p,
                    L3L = GetRcount("L3L", out p),
                    L3R = GetRcount("L3R", out p),
                    L3T = p,
                    L4L = GetRcount("L4L", out p),
                    L4R = GetRcount("L4R", out p),
                    L4T = p,
                    L5L = GetRcount("L5L", out p),
                    L5R = GetRcount("L5R", out p),
                    L5T = p,
                    R1L = GetRcount("R1L", out p),
                    R1R = GetRcount("R1R", out p),
                    R1T = p,
                    R2L = GetRcount("R2L", out p),
                    R2R = GetRcount("R2R", out p),
                    R2T = p,
                    R3L = GetRcount("R3L", out p),
                    R3R = GetRcount("R3R", out p),
                    R3T = p,
                    R4L = GetRcount("R4L", out p),
                    R4R = GetRcount("R4R", out p),
                    R4T = p,
                    R5L = GetRcount("R5L", out p),
                    R5R = GetRcount("R5R", out p),
                    R5T = p
                };

                if (model.L1T.StartsWith("A")
                    && model.L2T.StartsWith("A")
                    && model.L3T.StartsWith("A")
                    && model.L4T.StartsWith("A")
                    && model.L5T.StartsWith("A")

                    && model.R1T.StartsWith("A")
                    && model.R2T.StartsWith("A")
                    && model.R3T.StartsWith("A")
                    && model.R4T.StartsWith("A")
                    && model.R5T.StartsWith("A"))
                {
                    model.L1L = 12;
                    model.L2L = 12;
                    model.L3L = 12;
                    model.L4L = 12;
                    model.L5L = 12;

                    model.L1R = 12;
                    model.L2R = 12;
                    model.L3R = 12;
                    model.L4R = 12;
                    model.L5R = 12;

                    model.R1L = 12;
                    model.R2L = 12;
                    model.R3L = 12;
                    model.R4L = 12;
                    model.R5L = 12;

                    model.R1R = 12;
                    model.R2R = 12;
                    model.R3R = 12;
                    model.R4R= 12;
                    model.R5R = 12;
                }

                if (model.L1T.StartsWith("WX")
                    && model.L2T.StartsWith("WX")
                    && model.L3T.StartsWith("WX")
                    && model.L4T.StartsWith("WX")
                    && model.L5T.StartsWith("WX")

                    && model.R1T.StartsWith("WX")
                    && model.R2T.StartsWith("WX")
                    && model.R3T.StartsWith("WX")
                    && model.R4T.StartsWith("WX")
                    && model.R5T.StartsWith("WX"))
                {
                    model.L1L = 12;
                    model.L2L = 12;
                    model.L3L = 12;
                    model.L4L = 12;
                    model.L5L = 12;

                    model.L1R = 12;
                    model.L2R = 12;
                    model.L3R = 12;
                    model.L4R = 12;
                    model.L5R = 12;

                    model.R1L = 12;
                    model.R2L = 12;
                    model.R3L = 12;
                    model.R4L = 12;
                    model.R5L = 12;

                    model.R1R = 12;
                    model.R2R = 12;
                    model.R3R = 12;
                    model.R4R = 12;
                    model.R5R = 12;
                }

                return model;
            }
            catch
            {
                return null;
            }
        }

        protected void MapMQChart(List<FIMQChart> source, List<FIMQChart> dest)
        {
            FIMQChart record;
            FIMQChart recordSource;
            for (int i = 0; i < 8; i++)
            {
                recordSource = source[i];
                record = dest.FirstOrDefault(p => (p.ReportID == recordSource.ReportID) && (p.MQID == recordSource.MQID));
                if (record == null)
                {
                    dest.Add(recordSource);
                }
                else
                {
                    record.MQDesc = recordSource.MQDesc;
                    record.MQValue = recordSource.MQValue;
                }
            }
        }

        public bool CalculateFingerRecord(int reportId)
        {
            bool res = true;

            try
            {
                IFIFingerTypeRepository fingerTypeRepository = new FIFingerTypeRepository(this._unitOfWork);
                IFIFingerRecordRepository fingerRecordRepository = new FIFingerRecordRepository(this._unitOfWork);
                IFIFingerAnalysisRepository analysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIMQChartRepository mqChartRepository = new FIMQChartRepository(this._unitOfWork);

                List<FIFingerType> fingerTypes = fingerTypeRepository.FindAll();
                List<FIFingerRecord> fingerRecords = fingerRecordRepository.FindByRecordId(reportId);
                FIFingerAnalysis analysis = analysisRepository.FindByReportId(reportId);
                List<FIMQChart> dbMQCharts = mqChartRepository.FindByRecordId(reportId);

                List<FIMQChart> mqCharts = new List<FIMQChart>();

                if (analysis == null)
                {
                    analysis = CreateEntity(ICurrentSessionService.UserId, reportId);
                }

                IFormularService formularService = new FormularService();
                formularService.CalculateRecord(analysis, fingerRecords, fingerTypes);

                FingerRecordModel recordModel = GetFingerRecordModel(reportId);

                CustomerAnalyseModel fieldModel = new CustomerAnalyseModel(ref analysis, ref mqCharts, recordModel);
                MapMQChart(mqCharts, dbMQCharts);

                foreach (FIMQChart chart in dbMQCharts)
                {
                    if (chart.ID <= 0)
                    {
                        mqChartRepository.Insert(chart);
                    }
                    else
                    {
                        mqChartRepository.Update(chart);
                    }
                }


                if (analysis.ID <= 0)
                {
                    res = Add(analysis);
                }
                else
                {
                    res = Update(analysis);
                }
                if (!res)
                {
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }

            return res;
        }

        public bool GenerateReport(int reportId, int agencyId, int templateId, string templateFileName, string destinationFileName,
            bool isExportPdf)
        {
            try
            {
                /// Step of Generating Report:
                /// 1.1 Get Template file
                /// 1.2 Check Customer exist
                /// 1.3 Check Agency exist
                /// 1.4 Check License key valid or not
                /// 2. Copy to temporary folder
                /// 3. Convert to Word
                /// 4.1 Select all Analyse by CustomerId
                /// 4.2 Calculate FIFingerAnalysis and List<FIMQChart>
                /// 4.3 Insert/Update FIMQChart/AnalysisEntity
                /// 4.4 Generate Dictionary
                /// 5. Replace text inside Word (By ReportFieldModel)
                /// 6 Copy replaced text file to destinat folder with the name generated
                /// 7.1 Check if destinationFile exist
                /// 7.2 Write FIPrintRecord
                /// 7.3 Subtract point by 1
                /// 7.4 Delete tmpDat & tmpDocx


                /// 1.1 Get Template file

                IFITemplateRepository _templateRepository = new FITemplateRepository(this._unitOfWork);

                FITemplate template = _templateRepository.FindByKey(templateId);
                if (template == null
                    || !File.Exists(templateFileName))
                {
                    ThrowException("Mẫu báo cáo không tồn tại");
                }

                /// 1.2 Check Customer exist

                IFICustomerRepository _customerRepository = new FICustomerRepository(this._unitOfWork);
                FICustomer customer = _customerRepository.FindByKey(reportId);
                if (customer == null)
                {
                    ThrowException("Dữ liệu khách hàng không tồn tại");
                }

                /// 1.3 Check Agency exist

                IFIAgencyRepository _agencyRepository = new FIAgencyRepository(this._unitOfWork);
                FIAgency agency = _agencyRepository.FindByKey(agencyId);
                if (agency == null)
                {
                    ThrowException("Dữ liệu đại lý không tồn tại");
                }

                /// 1.4 Check License key valid or not

                IFIPointModelService _pointService = new FIPointModelService(this._unitOfWork);
                List<PointModel> pointModels = _pointService.FindByAgency(agencyId);
                PointModel pointModel = pointModels.FirstOrDefault(p => !p.IsError && p.PointCurrent > 0);
                if (pointModel == null)
                {
                    ThrowException("Không tìm thấy license key còn hiệu lực");
                }

                /// 2. Copy to temporary folder

                //string templateFileName = template.TemplateFileName;
                string tmpFolderPath = ICurrentSessionService.DesDirNameTemp;
                ///It will be full fileName
                string tmpFileName = getRandomFileName(tmpFolderPath, "dat");
                string tmpDatFileName = Path.ChangeExtension(tmpFileName, "dat");
                string tmpDocxFileName = Path.ChangeExtension(tmpFileName, "dattmp");
                string destFileName = destinationFileName;
                bool res;

                Directory.CreateDirectory(Path.GetDirectoryName(destFileName));
                if (File.Exists(tmpDocxFileName))
                {
                    File.Delete(tmpDocxFileName);
                }
                File.Copy(templateFileName, tmpDatFileName);

                /// 3. Convert to Word
                IFileConverterService service = new FileConverterService(this._unitOfWork);
                    res = service.FromRptxToDocx(tmpDatFileName, tmpDocxFileName);
                if (!res)
                {
                    ThrowException(service.ErrMsg);
                }


                /// 4.1 Select all Analyse by CustomerId

                FingerRecordModel recordModel = GetFingerRecordModel(reportId);
                //FIFingerAnalysis fingerAnalysis = new FIFingerAnalysis();
                IFIFingerAnalysisRepository analysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis fingerAnalysis = analysisRepository.FindByReportId(reportId);
                List<FIMQChart> mqCharts = new List<FIMQChart>();

                if (fingerAnalysis == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Dữ liệu khách hàng chưa được phân tích"
                        : "Customer data have not been analysed yet";
                    ThrowException(ErrMsg);
                }

                /// 4.2 Calculate FIFingerAnalysis and List<FIMQChart>

                CustomerAnalyseModel fieldModel = new CustomerAnalyseModel(ref fingerAnalysis, ref mqCharts, recordModel);
                fieldModel.Distributor = agency;
                fieldModel.Customer = customer;

                FingerImageModel fieldImageModel = new FingerImageModel();
                fieldImageModel.ReportID = customer.ReportID.Substring(7);
                fieldImageModel.UnsignCharCustomerName = ConvertUtil.ConverToUnsign(customer.Name).Replace(" ", "").ToUpper();
                fieldImageModel.SaveImagePath = ICurrentSessionService.DesDirNameSource;
                //fieldImageModel = FingerImageModel.Instance;
                /// 4.3 Insert/Update FIMQChart/AnalysisEntity

                //saveFingerAnalysis(agencyId, customerId, fingerAnalysis);
                //saveMQCharts(agencyId, customerId, mqCharts);

                //if (SaveChanges() <= 0)
                //{
                //    ThrowException("Không thể lưu thông tin");
                //}

                /// 4.4 Generate Dictionary

                //Dictionary<string, object> dictionary = fieldModel.ToDictionary();
                Dictionary<string, string> dictionary = fieldModel.ToDictionary();
                Dictionary<string, Image> dictionaryImage = fieldImageModel.ToDictionary();

                /// 5. Replace text inside Word (By ReportFieldModel)

                MSWordUtil.MergeFile(tmpDocxFileName);
                MSWordUtil.ReplaceAllText(tmpDocxFileName, dictionary, dictionaryImage);


                /// 6. Copy replaced text file to destinate folder with the name generated

                if (File.Exists(destFileName))
                {
                    File.Delete(destFileName);
                }
                //if Export to Pdf

                if (isExportPdf)
                {
                    //util.LoadDocument(tmpDocxFileName);
                    //util.ExportToPdf(destFileName);
                    MSWordUtil.ExportPDF2(tmpDocxFileName, destFileName);
                }
                //if Export to Docx
                else
                {
                    File.Copy(tmpDocxFileName, destFileName);
                }
                /////////////////////////////////////////////////////////////
                //Directory.CreateDirectory(Path.GetDirectoryName(destFileName));
                //File.Copy(tmpDocxFileName, destFileName);


                /// 7.1 Check if destinationFile exist



                /// 7.2 Write FIPrintRecord
                int pointId = pointModel.ID;
                writePrintRecord(agencyId, templateId, reportId, pointId);

                /// 7.3 Subtract point by 1

                res = _pointService.SubtractPointValue(pointId, 1);
                if (!res)
                {
                    ThrowException(_pointService.ErrMsg);
                }


                /// 7.2 Delete tmpDat & tmpDocx
                if (File.Exists(tmpDocxFileName))
                {
                    File.Delete(tmpDocxFileName);
                }
                if (File.Exists(tmpDatFileName))
                {
                    File.Delete(tmpDatFileName);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        protected string getRandomFileName(string folderPath, string extName)
        {
            string fileName = "";
            string fullFileName;
            string strTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            byte count = 0;
            do
            {
                fileName = Path.ChangeExtension((strTime + (count++).ToString("D2")), extName);
                fullFileName = Path.Combine(folderPath, fileName);
            } while (Directory.Exists(fullFileName));
            return fullFileName;
        }

        private void writePrintRecord(int agencyId, int templateId, int customerId, int pointId)
        {
            IFIPrintRecordRepository _printRecordRepository = new FIPrintRecordRepository(this._unitOfWork);
            FIPrintRecord obj = _printRecordRepository.CreateEntity();
            obj.AgencyID = agencyId;
            obj.TemplateID = templateId;
            obj.ReportID = customerId;
            obj.PointID = pointId;
            _printRecordRepository.Insert(obj);
        }

        public bool Add(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay đã tồn tại"
                        : "Finger analyse Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm phân tích vân tay"
                        : "Cannot add new finger analyse";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }

        public bool Update(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay không tồn tại"
                        : "Finger analyse Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin phân tích vân tay"
                        : "Cannot modify finger analyse infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }

        public bool Delete(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay không tồn tại"
                        : "Finger analyse Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa phân tích vân tay"
                        : "Cannot remove finger analyse";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }
    }
}