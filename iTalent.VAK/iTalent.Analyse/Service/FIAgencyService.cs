﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFIAgencyService : IService
    {
        FIAgency CreateEntity();
        FIAgency Find(int id);
        FIAgency FindBySeckey(string key);
        List<FIAgency> FindAll();

        bool Add(FIAgency obj);
        bool Update(FIAgency obj);
        bool Delete(FIAgency obj);

        long Count();

        List<FIAgency> Search(string textSearch);

        bool Login(string username, string password);
        bool Login(string name);
        bool ChangePass(int id, string oldPassword, string newPassword);

        bool ResetPass(int id);
    }

    public class FIAgencyService : Pattern.Service, IFIAgencyService
    {
        protected IFIAgencyRepository agencyRepository;
        protected const string KeyEncrypt = "friek@ouer048!940#kdpe@";

        public FIAgencyService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
            agencyRepository = new FIAgencyRepository(this._unitOfWork);
        }

        protected string Encrypt(string message)
        {
            return Security.Encrypt(message, KeyEncrypt);
        }

        public FIAgency CreateEntity()
        {

            var obj = new FIAgency
            {
                Country = "VietNam",
                DOB = DateTime.Now.AddYears(-20),
                Day_Actived = 0,
                Dis_Activated = "1",
                Gender = "None",
                Username = "",
                Password = "",
                ID = 0,
                Point = -1,
                ProductKey = "",
                SecKey = "",
                SerailKey = "",
                State = "1"
            };

            return obj;
        }

        public FIAgency Find(int id)
        {
            FIAgency obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                obj = agencyRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public FIAgency FindBySeckey(string key)
        {
            FIAgency obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                obj = agencyRepository.FindBySeckey(key);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public List<FIAgency> FindAll()
        {
            try
            {
                _unitOfWork.OpenConnection();
                return agencyRepository.FindAll();
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
        }
        
        public bool Add(FIAgency obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                FIAgency obj2 = agencyRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã đại lý đã tồn tại"
                        : "Agency Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = agencyRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm đại lý"
                        : "Cannot add new agency";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIAgency obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                FIAgency obj2 = agencyRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã đại lý không tồn tại"
                        : "Agency Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = agencyRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin đại lý"
                        : "Cannot modify agency infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIAgency obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                FIAgency obj2 = agencyRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã đại lý không tồn tại"
                        : "Agency Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = agencyRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa đại lý"
                        : "Cannot remove agency";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public long Count()
        {
            long res;
            try
            {
                res = agencyRepository.Count();
                return res;
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = 0;
            }
            finally

            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public List<FIAgency> Search(string textSearch)
        {
            List<FIAgency> list = null;
            try
            {
                _unitOfWork.OpenConnection();
                list = agencyRepository.Search(textSearch);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                list = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return list;
        }

        public bool Login(string username, string password)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                if (username.IsBlank()
                    || password.IsBlank())
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Vui lòng nhập mật khẩu!"
                        : @"Please input password!";
                    ThrowException(ErrMsg);
                }

                //password = Encrypt(password);
                FIAgency obj = agencyRepository.FindLogin(username, password);
                if (obj == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Mã đại lý hoặc mật khẩu không đúng!" + "\n" +
                          " Vui lòng nhập lại mã đại lý và mật khẩu của bạn!"
                        : @"Incorrect agency ID or password!" + "\n" +
                          " Please input again!";
                    ThrowException(ErrMsg);
                }

                ICurrentSessionService.CurAgency = obj;
                ICurrentSessionService.Username = obj.Username;
                ICurrentSessionService.UserId = obj.ID;
                ICurrentSessionService.Seckey = obj.SecKey;
                ICurrentSessionService.Serail = obj.SerailKey;
                return true;
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally

            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Login(string name)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                if (string.IsNullOrWhiteSpace(name))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Vui lòng nhập mật khẩu!"
                        : @"Please input password!";
                    ThrowException(ErrMsg);
                }

                FIAgency obj = this.agencyRepository.FindByName(name);
                if (obj == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? (@"Mật khẩu không đúng!" + "\n" +
                           " Vui lòng nhập lại mật khẩu của bạn!")
                        : (@"Incorrect password!" + "\n" +
                           " Please input again!");
                    ThrowException(ErrMsg);
                }

                //EnsureObjProperties(obj, true);

                ICurrentSessionService.CurAgency = obj;
                ICurrentSessionService.Username = obj.Username;
                ICurrentSessionService.UserId = obj.ID;
                ICurrentSessionService.Seckey = obj.SecKey;
                ICurrentSessionService.Serail = obj.SerailKey;
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally

            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool ChangePass(int id, string oldPassword, string newPassword)
        {
            bool res = true;
            try
            {
                if (id <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                        : @"Agency's information dosen't exists!";
                    ThrowException(ErrMsg);
                }

                if (oldPassword.IsBlank() || newPassword.IsBlank())
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Vui lòng nhập mật khẩu cũ và mới!"
                        : @"Please input old password and new password!";
                    ThrowException(ErrMsg);
                }

                newPassword = Encrypt(newPassword);
                if (oldPassword == newPassword)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Vui lòng nhập mật khẩu cũ khác với mật khẩu mới!"
                        : @"Please input old password different new password!";
                    ThrowException(ErrMsg);
                }

                var obj = this.agencyRepository.FindByKey(id);
                if (obj == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Sai tên đăng nhập hoặc mật khẩu cũ"
                        : @"Incorrect agency ID or old password!";
                    ThrowException(ErrMsg);
                }
                if (obj != null)
                {
                    obj.Password = newPassword;
                    this.agencyRepository.Update(obj);
                }
            }

            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool ResetPass(int id)
        {
            bool res = true;
            try
            {
                if (id <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                        : @"Agency's information dosen't exists!";
                    ThrowException(ErrMsg);
                }

                var tmpObj = this.agencyRepository.FindByKey(id);
                if (tmpObj == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                        : @"Agency's information dosen't exists!";
                    ThrowException(ErrMsg);
                }
                tmpObj.Password = "AC50015";
                agencyRepository.Update(tmpObj);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

    }
}