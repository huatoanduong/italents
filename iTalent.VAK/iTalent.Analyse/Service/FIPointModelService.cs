﻿using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFIPointModelService : IService
    {
        PointModel Find(Int32 id);
        PointModel FindByOriginalKey(string key);
        FIPoint CreateEntity();
        List<PointModel> GetAll();
        List<PointModel> FindByAgency(int agencyId);
        List<PointModel> FindByCustomer(int customerId);
        PointModel FindFirstUnexpiredKey(int agencyId);
        List<PointModel> FindAllUnexpiredKey(int agencyId);
        int GetNumberPointLeft(int agencyId);
        //int CountAll();

        int GetNumAddKey(string key);

        /// <summary>
        /// Generate and Save to Database
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="pointValue"></param>
        /// <returns></returns>
        bool GenerateNewKey(ref PointModel model);

        bool RegisterLicense(string key);

        bool SubtractPointValue(int pointId, int subtractValue);
    }

    public class FIPointModelService : Pattern.Service, IFIPointModelService
    {
        protected readonly string GenericKeyEnc = @"h/w(s6!+crlHk~5r09+abc";

        private readonly IFIAgencyRepository _agencyRepository;
        private readonly IFICustomerRepository _customerRepository;
        private readonly IFIPointRepository _pointRepository;

        public FIPointModelService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
            _agencyRepository = new FIAgencyRepository(unitOfWork);
            _customerRepository = new FICustomerRepository(unitOfWork);
            _pointRepository = new FIPointRepository(unitOfWork);
        }

        /// <summary>
        /// Use internal, not throw Exception
        /// </summary>
        /// <param name="pointEntity"></param>
        /// <returns></returns>
        protected PointModel MakeModel(FIPoint pointEntity)
        {
            PointModel model = null;
            try
            {
                model = makeModel(pointEntity);
            }
            catch (Exception exception)
            {
                FIAgency obj = _agencyRepository.FindByKey((int)pointEntity.AgencyID);
                model = new PointModel
                {
                    SecKey = obj.SecKey,
                    PointEntity = pointEntity,
                    IsError = true
                };
                //model.ErrMsg = exception.Message;
                model.PointCurrent = model.PointOriginal = -1;
                model.KeyOriginal = model.KeyCurrent = exception.Message;
            }
            return model;
        }

        protected List<PointModel> MakeModel(List<FIPoint> pointEntities)
        {
            List<PointModel> list = new List<PointModel>();
            PointModel model;

            if (pointEntities != null)
            {
                foreach (FIPoint point in pointEntities)
                {
                    model = MakeModel(point);
                    list.Add(model);
                }
            }

            return list;
        }
        protected PointModel makeModel(FIPoint pointEntity)
        {
            PointModel model = null;

            try
            {
                string keyOrigin = pointEntity.KeyPoint;
                string keyCurrent = pointEntity.KeyPointAfter;
                string keyEncrypt;
                FIAgency obj = _agencyRepository.FindByKey(pointEntity.AgencyID);

                if (pointEntity.AgencyID > 0)
                    keyEncrypt = this.GenericKeyEnc + obj.SecKey;
                else
                {
                    keyEncrypt = this.GenericKeyEnc; //ICurrentSessionService.Seckey;
                }

                string sfront, srear;
                int startIndex, endIndex;
                int pointOrigin;
                int pointCurrent;

                keyOrigin = Decrypt(keyOrigin, keyEncrypt);
                keyCurrent = Decrypt(keyCurrent, keyEncrypt);

                if (!keyOrigin.EndsWith("#00")
                    || !keyCurrent.EndsWith("#01"))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "không thể xác định key."
                        : "The key is not modify";
                    ThrowException(ErrMsg);
                }

                startIndex = keyOrigin.IndexOf('!');
                endIndex = keyOrigin.IndexOf('#');
                sfront = keyOrigin.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyOrigin.LastIndexOf('!');
                endIndex = keyOrigin.LastIndexOf('#');
                srear = keyOrigin.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                pointOrigin = int.Parse(sfront);

                startIndex = keyCurrent.IndexOf('!');
                endIndex = keyCurrent.IndexOf('#');
                sfront = keyCurrent.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyCurrent.LastIndexOf('!');
                endIndex = keyCurrent.LastIndexOf('#');
                srear = keyCurrent.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                pointCurrent = int.Parse(sfront);

                model = new PointModel
                {
                    IsError = false,
                    PointEntity = pointEntity,
                    PointCurrent = pointCurrent,
                    PointOriginal = pointOrigin,
                    KeyOriginal = pointEntity.KeyPoint,
                    KeyCurrent = pointEntity.KeyPointAfter
                };
                //if (pointEntity.PointDate != null) model.PointDate = pointEntity.PointDate;
            }
            catch(Exception ex)
            {
                      ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã bản quyền không hợp lệ."
                        : " the key is not valid!";
                ThrowException(ErrMsg);
            }
            return model;
        }
        protected string makeKey(string agentCode, DateTime dateMade, int point, bool isOriginalKey)
        {
            string key = null;

            string keyEncrypt = this.GenericKeyEnc + agentCode;
            string strPoint = point.ToString();
            key = "!" + strPoint + "#"
                  + agentCode
                  + dateMade.ToString("yyMMddHHmmss")
                  + "!" + strPoint + "#";
            if (isOriginalKey)
            {
                key += "00";
            }
            else
            {
                key += "01";
            }

            key = Encrypt(key, keyEncrypt);

            return key;
        }

        protected FIPoint parseKey(string key)
        {
            FIPoint obj = null;
            try
            {
                FIAgency agency = ICurrentSessionService.CurAgency;
                //string keyEncrypt = this.GenericKeyEnc + agency.Username;
                string keyEncrypt = this.GenericKeyEnc + agency.SecKey;

                string sfront, srear;
                int startIndex, endIndex;
                int pointValue;

                string keyDecrypt = Decrypt(key, keyEncrypt);

                startIndex = keyDecrypt.IndexOf('!');
                endIndex = keyDecrypt.IndexOf('#');
                sfront = keyDecrypt.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyDecrypt.LastIndexOf('!');
                endIndex = keyDecrypt.LastIndexOf('#');
                srear = keyDecrypt.Substring(startIndex + 1, endIndex - startIndex - 1);

                if (sfront != srear
                    || !keyDecrypt.EndsWith("#00"))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                      ? "License Key trống"
                      : "License key null";
                    ThrowException(ErrMsg);
                }
                pointValue = int.Parse(sfront);
                string keyCurrent = keyDecrypt.Substring(0, keyDecrypt.Length - 3);
                keyCurrent += "#01";
                keyCurrent = Encrypt(keyCurrent, keyEncrypt);

                obj = new FIPoint();
                obj.AgencyID = agency.ID;
                obj.ReportID = 0;
                obj.PointDate = DateTime.Now;
                obj.PointType = "0";
                obj.Point = pointValue;
                obj.KeyPoint = key;
                obj.KeyPointAfter = keyCurrent;
                obj.AddedBy = agency.SecKey;
                short i = 0;
                if (pointValue <= 0) i = 1;
                obj.IsOutDate = i;
            }
            catch
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage
                       ? "License Key không hợp lệ"
                       : "This license key is not existed";
                ThrowException(ErrMsg);
            }
            return obj;
        }
        protected string Encrypt(string message, string keyEncrypt)
        {
            return Security.Encrypt(message, keyEncrypt);
        }
        protected string Decrypt(string message, string keyEncrypt)
        {
            return Security.Decrypt(message, keyEncrypt);
        }

        //public int CountAll()
        //{
        //    return _customerRepository.Count();
        //}

        public FIPointModelService CreateEntity()
        {
            throw new NotImplementedException();
        }

        public PointModel Find(Int32 id)
        {
            FIPoint point = _pointRepository.FindByKey(id);
            if (point == null)
            {
                return null;
            }
            return MakeModel(point);
        }

        public List<PointModel> FindAllUnexpiredKey(int agencyId)
        {
            FIPointConditionForm _cond = new FIPointConditionForm { AgencyID = agencyId, IsOutDate = 0 };
            return MakeModel(_pointRepository.FindByCondition(_cond));
        }

        public List<PointModel> FindByAgency(int agencyId)
        {
            //FIPointConditionForm _cond = new FIPointConditionForm { AgencyID = agencyId};
            return MakeModel(_pointRepository.FindByAgency(agencyId));
        }

        public List<PointModel> FindByCustomer(int customerId)
        {
            FIPointConditionForm _cond = new FIPointConditionForm { ReportID = customerId };
            return MakeModel(_pointRepository.FindByCondition(_cond));
        }

        public PointModel FindByOriginalKey(string key)
        {
            FIPointConditionForm _cond = new FIPointConditionForm { KeyPoint = key};
            FIPoint point = _pointRepository.FindByCondition(_cond)[0];
            if (point == null)
            {
                return null;
            }
            return MakeModel(point);
        }

        public PointModel FindFirstUnexpiredKey(int agencyId)
        {
            FIPointConditionForm _cond = new FIPointConditionForm { AgencyID = agencyId, IsOutDate = 0 };
            return MakeModel(_pointRepository.FindByCondition(_cond)[0]);
        }

        public bool GenerateNewKey(ref PointModel model)
        {
            try
            {
                if (model == null
                    || model.AgentId <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã Đại lý không tồn tại"
                        : "This agency is not existed";
                    ThrowException(ErrMsg);
                }
                if (model == null
                    || model.PointOriginal <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                       ? "Không thể cấp điểm báo cáo bằng hoặc nhỏ hơn 0"
                       : "Cannot create numeric point equal zero!";
                    ThrowException(ErrMsg);
                }

                FIAgency agency = _agencyRepository.FindByKey(model.AgentId);
                if (agency == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã Đại lý không tồn tại"
                        : "This agency is not existed";
                    ThrowException(ErrMsg);
                }
                if (string.IsNullOrEmpty(agency.SecKey))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                       ? "Mã Đại lý trống"
                       : "the ID is not null";
                    ThrowException(ErrMsg);
                }

                DateTime dateCreated = DateTime.Now;
                string licKey = makeKey(agency.SecKey, dateCreated, model.PointOriginal, true);
                string licKeyCurrent = makeKey(agency.SecKey, dateCreated, model.PointOriginal, false);
                //string keyEncrypt = this.GenericKeyEnc + agency.SecKey;
                //licKey = Encrypt(licKey, keyEncrypt);
                //FIPointConditionForm _cond = new FIPointConditionForm { KeyPoint = licKey };
                FIPoint tmpPoint = null;

                try
                {
                    tmpPoint = _pointRepository.FindByKeyPoint(licKey);
                }
                catch
                {
                    
                }

                if (tmpPoint != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                       ? "License Key này đã tồn tại trong dữ liệu"
                       : "This licensekey was existed";
                    ThrowException(ErrMsg);
                }

                FIPoint pointEntity = new FIPoint();
                pointEntity.AgencyID = agency.ID;
                pointEntity.ReportID = 0;
                pointEntity.PointDate = dateCreated;
                pointEntity.PointType = "0";
                pointEntity.Point = model.PointOriginal;
                pointEntity.KeyPoint = licKey;
                pointEntity.KeyPointAfter = licKeyCurrent;
                pointEntity.AddedBy = ICurrentSessionService.CurAgency.SecKey;
                pointEntity.IsOutDate = 0;

                int res = _pointRepository.Insert(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm mã bản quyền"
                        : "Cannot add new licensekey";
                    ThrowException(ErrMsg);
                }
                //model = MakeModel(pointEntity);
                model.PointEntity = pointEntity;
                model.PointCurrent = model.PointOriginal;
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public List<PointModel> GetAll()
        {
            return MakeModel(_pointRepository.FindAll());
        }

        public int GetNumAddKey(string key)
        {
            int num = -1;
            try
            {
                FIAgency agency = ICurrentSessionService.CurAgency;
                string keyEncrypt = this.GenericKeyEnc + agency.SecKey;

                string sfront, srear;
                int startIndex, endIndex;

                string key1 = Decrypt(key, keyEncrypt);

                startIndex = key1.IndexOf('!');
                endIndex = key1.IndexOf('#');
                sfront = key1.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = key1.LastIndexOf('!');
                endIndex = key1.LastIndexOf('#');
                srear = key1.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                num = int.Parse(sfront);
            }
            catch (Exception)
            {
                //throw;
            }
            return num;
        }

        public int GetNumberPointLeft(int agencyId)
        {
            int i = 0;

            try
            {
                List<PointModel> list = FindAllUnexpiredKey(agencyId);

                i = list.Sum(p => p.PointCurrent);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                i = 0;
            }

            return i;
        }

        public bool RegisterLicense(string key)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                      ? "Vui lòng nhập License Key"
                      : "Please input license key";
                    ThrowException(ErrMsg);
                }

                //FIPointConditionForm _cond = new FIPointConditionForm { KeyPoint = key };
                FIPoint tmpPoint = null;

                try
                {
                    tmpPoint = _pointRepository.FindByKeyPoint(key);
                }
                catch
                {

                }

                if (tmpPoint != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                     ? "License Key này đã được nhập!"
                     : "license key has inserted in system!";
                    ThrowException(ErrMsg);
                }

                FIPoint pointEntity = parseKey(key);

                int res = _pointRepository.Insert(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm mã bản quyền"
                        : "Cannot add new licensekey";
                    ThrowException(ErrMsg);
                }

            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public bool SubtractPointValue(int pointId, int subtractValue)
        {
            PointModel model = null;
            try
            {
                model = Find(pointId);
                if (model == null || model.IsError)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                       ? "Không tìm thấy license key hợp lệ trong dữ liệu"
                       : "This license key is not existed";
                    ThrowException(ErrMsg);
                }
                if (model.PointCurrent < subtractValue)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                     ? "License key không còn đủ để sử dụng"
                     : "This license key is not used";
                    ThrowException(ErrMsg);
                }
                int pointAfter = model.PointCurrent - subtractValue;
                string key = makeKey(ICurrentSessionService.CurAgency.SecKey,(DateTime)model.PointDate,pointAfter, false);
                FIPoint pointEntity = model.PointEntity;
                pointEntity.KeyPointAfter = key;
                pointEntity.Point = pointAfter;
                short i = 0;
                if (pointAfter <= 0) i = 1;
                pointEntity.IsOutDate = i;

                var res = _pointRepository.Update(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin mã bản quyền"
                        : "Cannot modify FIPoint infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        FIPoint IFIPointModelService.CreateEntity()
        {
            FIPoint _obj = new FIPoint
            {
                KeyPoint = "",
                ID = 0,
                AddedBy=ICurrentSessionService.CurAgency.SecKey,
                AgencyID = 0,
                Point =0,
                IsOutDate = 0,
                KeyPointAfter ="",
                PointDate = Utils.DateTimeUtil.GetCurrentTime(),
                PointType ="0"
            };
            return _obj;
        }

        protected override void InitRepositories()
        {
            
        }
    }
}
