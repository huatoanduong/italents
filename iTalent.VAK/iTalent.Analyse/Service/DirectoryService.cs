﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Pattern;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IDirectoryService : IErrorProvider
    {
        string GetReportId();
        string GetTempFolderName(FICustomer customer);
        string GetCustomerFolderName(FICustomer customer);
        string GetDatFileName(FICustomer customer);
        string GetFullDatFileName(FICustomer customer);
    }

    public class DirectoryService : ErrorProvider, IDirectoryService
    {
        public string GetReportId()
        {
            return ICurrentSessionService.Seckey + DateTime.Now.ToString("ddMMyyyyHHmmss");
        }

        protected string getFileName(string customerName, string secKey, string reportId)
        {
            string fileName = reportId;
            //if(reportId.StartsWith(secKey))
            //{
            //    fileName = fileName.Substring(secKey.Length);
            //}
            if (reportId.Length > 7)
            {
                fileName = fileName.Substring(7);
            }
            fileName += "_" + customerName.ToUnsign().ToUpper().Replace(" ", "");
            return fileName;
        }

        public string getFileName(string customerName, string reportId)
        {
            string fileName = reportId;
            fileName = fileName.Substring(7);
            fileName += "_" + customerName.ToUnsign().ToUpper().Trim();
            return fileName;
        }

        public string GetTempFolderName(FICustomer customer)
        {
            string customerName = customer.Name;
            string secKey = ICurrentSessionService.Seckey;
            string reportId = customer.ReportID;
            string filename = getFileName(customerName, secKey, reportId);

            return filename;
        }

        public string GetCustomerFolderName(FICustomer customer)
        {
            string customerName = customer.Name;
            string secKey = ICurrentSessionService.Seckey;
            string reportId = customer.ReportID;
            string filename = getFileName(customerName, secKey, reportId);

            return filename;
        }

        public string GetDatFileName(FICustomer customer)
        {
            string customerName = customer.Name;
            string secKey = ICurrentSessionService.Seckey;
            string reportId = customer.ReportID;
            string filename = getFileName(customerName, secKey, reportId);

            filename += "profile.dat";
            return filename;
        }

        public string GetFullDatFileName(FICustomer customer)
        {
            string dirCusName = Path.Combine(ICurrentSessionService.DesDirNameSource,GetCustomerFolderName(customer));
            if (!Directory.Exists(dirCusName)) Directory.CreateDirectory(dirCusName);
            return Path.Combine(dirCusName,GetDatFileName(customer));
        }
    }
}