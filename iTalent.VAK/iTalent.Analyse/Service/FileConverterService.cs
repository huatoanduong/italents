﻿using System;
using System.IO;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFileConverterService : IService
    {
        bool FromDocxToRptx(int agencyId, string templateName, string inputFilePath, string outputFilePath);
        bool FromRptxToDocx(string inputFilePath, string outputFilePath);
    }

    public class FileConverterService : Pattern.Service, IFileConverterService
    {
        protected string Key = GlobalContext.ReportKeyEncrypt;

        public FileConverterService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        protected void ThrowExceptionWithErrMsg(string errMsg)
        {
            throw new Exception(errMsg);
        }

        public bool FromDocxToRptx(int agencyId, string templateName, string inputFilePath, string outputFilePath)
        {
            try
            {
                this._unitOfWork.OpenConnection();

                if (!File.Exists(inputFilePath))
                {
                    ThrowExceptionWithErrMsg("Không tìm thấy file nguồn");
                }

                //if (
                //    (Path.GetExtension(outputFilePath).ToLower() != "dat")
                //    || (Path.GetExtension(inputFilePath).ToLower() != "doc")
                //    || (Path.GetExtension(inputFilePath).ToLower() != "docx")
                //    )
                //{
                //    ThrowExceptionWithErrMsg("File không đúng định dạng");
                //}

                FIAgencyRepository agencyRepository = new FIAgencyRepository(this._unitOfWork);
                FIAgency agency = agencyRepository.FindByKey(agencyId);
                if (agency == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? @"Đại lý không tồn tại"
                        : @"Agency does not exist";
                    ThrowExceptionWithErrMsg(ErrMsg);
                }

                FileEncryptorUtil util = new FileEncryptorUtil();
                util.EncryptFile(inputFilePath, outputFilePath, Key);

                IFITemplateHistoryRepository templateHistoryRepository =
                    new FITemplateHistoryRepository(this._unitOfWork);
                FITemplateHistory templateHistory
                    = new FITemplateHistory()
                      {
                          AgencyID = agencyId,
                          DateCreate = DateTime.Now,
                          KeyEn = this.Key,
                          TemplateName = templateName,
                      };
                templateHistoryRepository.Insert(templateHistory);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                //ErrMsg = @"Không thể chuyển đổi định dạng file";
                return false;
            }
            finally
            {
                this._unitOfWork.CloseConnection();
            }
            return true;
        }

        public bool FromRptxToDocx(string inputFilePath, string outputFilePath)
        {
            try
            {
                if (!File.Exists(inputFilePath))
                {
                    ThrowExceptionWithErrMsg("Không tìm thấy file nguồn");
                }

                //if (
                //    (Path.GetExtension(inputFilePath).ToLower() != "dat")
                //    || (Path.GetExtension(outputFilePath).ToLower() != "doc")
                //    || (Path.GetExtension(outputFilePath).ToLower() != "docx")
                //    )
                //{
                //    ThrowExceptionWithErrMsg("File không đúng định dạng");
                //}

                FileEncryptorUtil util = new FileEncryptorUtil();
                util.DecryptFile(inputFilePath, outputFilePath, Key);


                //File.Copy(inputFilePath, outputFilePath, true);
            }
            catch (Exception exception)
            {
                //ErrMsg = exception.Message;
                ErrMsg = @"Không thể chuyển đổi định dạng file";
                return false;
            }
            finally
            {
                //this._unitOfWork.CloseConnection();
            }
            return true;
        }

    }
}