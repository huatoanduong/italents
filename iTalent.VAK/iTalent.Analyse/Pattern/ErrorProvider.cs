﻿using System;

namespace iTalent.Analyse.Pattern
{
    public interface IErrorProvider
    {
        string ErrMsg { get; set; }
        string InnerErrMsg { get; set; }
    }

    public class ErrorProvider : IErrorProvider
    {
        public string ErrMsg { get; set; }
        public string InnerErrMsg { get; set; }

        protected void ThrowException(string message = null, string innerMessage = null)
        {
            this.ErrMsg = message;
            this.InnerErrMsg = innerMessage;
            throw new Exception(message);
        }
    }
}