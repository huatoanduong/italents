﻿using iTalent.Analyse.Repository;

namespace iTalent.Analyse.Pattern
{
    public interface IService : IErrorProvider
    {
    }

    public abstract class Service : ErrorProvider, IService
    {
        #region Private Fields

        protected IUnitOfWorkAsync _unitOfWork;

        #endregion Private Fields

        #region Properties
        
        #endregion

        #region Constructor

        protected Service(IUnitOfWorkAsync unitOfWork)
        {
            _unitOfWork = unitOfWork;
            InitRepositories();
        }

        protected abstract void InitRepositories();

        #endregion Constructor

    }
}