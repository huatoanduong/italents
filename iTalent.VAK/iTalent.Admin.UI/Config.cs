﻿using System;
using System.Windows.Forms;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Service;
using iTalent.Utils;

namespace iTalent.Admin.UI
{
    public interface IConfig
    {
        bool Load();
        void Save();
    }
    public class Config:IConfig
    {
        protected readonly string GenericKeyEnc = @"sep&p!e#t^e%r";
        protected string Encrypt(string message, string keyEncrypt)
        {
            return Security.Encrypt(message, keyEncrypt);
        }
        protected string Decrypt(string message, string keyEncrypt)
        {
            return Security.Decrypt(message, keyEncrypt);
        }

        public bool CheckLogin(string password)
        {
            try
            {
                return password == KeyLogin;
            }
            catch
            {
                return false;
            }
        }

        //private static Config _intance;
        public string ErrMsg { get; set; }
        //public static Config Intance
        //{
        //    get
        //    {
        //        if (_intance == null)
        //        {
        //            _intance = new Config();
        //            _intance.LoadThongSo();
        //        }
        //        else
        //        {
        //            _intance.LoadThongSo();
        //        }
        //        return _intance;
        //    }
        //}
        protected readonly AppSetting AppSetting;
        public Config()
        {
            AppSetting = new AppSetting();
            LoadThongSo();
        }
      
        public bool IsVietNamLag { get; set; }
        public bool IsInstallDriver { get; set; }
        public string LocalSaveImages { get; set; }
        public string KeyLogin { get; set; }

        private bool LoadThongSo()
        {
            try
            {
                string sValue = AppSetting.GetValue("VietNamLanguage");
                IsVietNamLag = bool.Parse(sValue);

                sValue = AppSetting.GetValue("SaveImages");
                LocalSaveImages = sValue;

                sValue = AppSetting.GetValue("IsInstallDriver");
                IsInstallDriver = bool.Parse(sValue);

                sValue = AppSetting.GetValue("sKey");
                KeyLogin = Decrypt(sValue, GenericKeyEnc);

                ICurrentSessionService.VietNamLanguage = IsVietNamLag;
                ICurrentSessionService.DirSaveImages = LocalSaveImages;
                ICurrentSessionService.IsInstallDriver = IsInstallDriver;
                ICurrentSessionService.DesDirNameSource = LocalSaveImages + @"FingerprintCustomers";
            }
            catch (Exception ex)
            {
                IsVietNamLag = true;
                LocalSaveImages =@"C:\";
               ErrMsg = ex.Message;
                return false;
            }
            return true;
        }

        private void SaveThongSo()
        {
            try
            {
                if (KeyLogin == "")
                {
                    MessageBox.Show(IsVietNamLag
                   ? @"Vui lòng điền mật khẩu đăng nhập!"
                   : @"Please input password to login!");
                    return;
                }

                AppSetting.SetValue("VietNamLanguage", IsVietNamLag.ToString());
                AppSetting.SetValue("SaveImages", LocalSaveImages);
                AppSetting.SetValue("sKey", Encrypt(KeyLogin,GenericKeyEnc));
                MessageBox.Show(IsVietNamLag
                    ? @"Cấu hình thành công! Hệ thống sẽ tự động đóng lại."
                    : @"config was successfully! system needs to be open again.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }

        public void SetInstallDriver()
        {
            try
            {
                AppSetting.SetValue("IsInstallDriver", "True");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }
        public bool GetInstallDriver()
        {
            try
            {
                string sValue = AppSetting.GetValue("IsInstallDriver");
                IsInstallDriver = bool.Parse(sValue);
                ICurrentSessionService.IsInstallDriver = IsInstallDriver;
                return IsInstallDriver;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
            return false;
        }

        public bool Load()
        {
            return LoadThongSo();
        }

        public void Save()
        {
            SaveThongSo();
        }
    }
}
