﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Windows.Forms;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Service;
using iTalent.Utils;
using Microsoft.Win32;

//using static System.Security.Principal.WindowsIdentity;

namespace iTalent.Admin.UI
{
    public interface ISetup : IDisposable
    {
        bool Intall();
        bool ProcessUnikey();
    }

    public class Setup : ISetup
    {
        public string Message { get; set; }

        public bool IsElevated
        {
            get { return new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator); }
        }

        public Setup()
        {

        }

        public bool ProcessUnikey()
        {
            try
            {
                var procs = Process.GetProcesses();
                if (procs.Length <= 0) return false;
                bool isrun = false;
                foreach (Process pro in procs.Where(pro => pro.ProcessName.ToLower().Contains("unikeynt")))
                {
                    if (!IsElevated)
                    {
                        pro.Kill();
                        break;
                    }
                    isrun = true;
                    break;
                }

                if (isrun) return true;

                var myProcess = new Process();
                var info = new ProcessStartInfo
                {
                    FileName = Directory.GetCurrentDirectory() + @"\UnikeyNT.exe",
                    UseShellExecute = true,
                    Verb = "runas",
                    CreateNoWindow = true
                };

                myProcess.StartInfo = info;

                if (myProcess.StartInfo.FileName != "")
                {
                    myProcess.Start();
                }
                else
                {
                    Message = ICurrentSessionService.VietNamLanguage ?@"Không tồn tại Unikey trong hệ thống!":"Unikey not exists!";
                }

                myProcess.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return false;
        }

        private bool IsApplictionInstalled(string pName)
        {
            try
            {
                string displayName;

                // search in: CurrentUser
                var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
                foreach (var keyName in key.GetSubKeyNames())
                {
                    var subkey = key.OpenSubKey(keyName);
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (pName.Equals(displayName, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }

                // search in: LocalMachine_32
                key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
                foreach (var subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (pName.Equals(displayName, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }

                // search in: LocalMachine_64
                key =
                    Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
                foreach (var subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (pName.Equals(displayName, StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public void Dispose()
        {
            GC.Collect();
            GC.SuppressFinalize(this);
        }

        public bool Intall()
        {
            try
            {
                var kq = false;

                Analyse.Commons.Config config = new Analyse.Commons.Config();
                if (config.GetInstallDriver()) return true;

                //FrmFlash.ShowSplash();
                //Application.DoEvents();

                ////Chuong cai dat bo sung


                //Cai Dat Driver
                var os = OsInfo.GetOsFriendlyName();
                var myProcess = new Process();
                var info = new ProcessStartInfo();

                if (os.Contains("8") || os.Contains("10"))
                {
                    if (!IsApplictionInstalled("Futronic Fingerprint Scanner"))
                    {
                        info.FileName = Directory.GetCurrentDirectory() + @"\FP_Win8.exe";
                        info.Verb = "runas";
                        myProcess.StartInfo = info;

                        if (myProcess.StartInfo.FileName != "")
                        {
                            myProcess.StartInfo.UseShellExecute = true;
                            myProcess.Start();
                            myProcess.WaitForExit();
                        }
                        else
                        {
                            MessageBox.Show(ICurrentSessionService.VietNamLanguage
                                ? "Không tìm thấy phần mềm điều khiển thiết bị!\nVui lòng cài đặt lại."
                                : "No device driver found for installing.\n Please contact the supplier.");
                        }
                    }
                }
                else
                {
                    info.FileName = Directory.GetCurrentDirectory() + @"\FP_Win7_WinXP.exe";
                    info.Verb = "runas";
                    myProcess.StartInfo = info;

                    if (myProcess.StartInfo.FileName != "")
                    {
                        myProcess.StartInfo.UseShellExecute = true;
                        myProcess.Start();
                        myProcess.WaitForExit();
                    }
                    else
                    {
                        MessageBox.Show(ICurrentSessionService.VietNamLanguage
                            ? "Không tìm thấy phần mềm điều khiển thiết bị!\nVui lòng cài đặt lại."
                            : "No device driver found for installing.\n Please contact the supplier.");
                    }
                }

                myProcess.Dispose();
                //FrmFlash.CloseSplash();
                config.SetInstallDriver();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
