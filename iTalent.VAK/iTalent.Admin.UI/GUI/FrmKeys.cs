﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using iTalent.Analyse.Model;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using VAKFomula.Entity;

namespace iTalent.Admin.UI.GUI
{
    public partial class FrmKeys : Form
    {
        public FIAgency CurrAgency { get; set; }
        public bool IsSuccess { get; set; }
        private readonly BaseForm _baseForm;
        public FrmKeys(FIAgency objAgency)
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.DgView = dgvData;
            BaseForm.ListconControls = new List<Control>
            {
                c4FunHeaderGroup1,
                labAddress,
                labCellPhone,
                labCity,
                labEmail,
                labID,
                labName,
                labPhone,
                btnCopyKey,
                btnGenerate
            };
            _baseForm = new BaseForm();
            dgvData.AutoGenerateColumns = false;
            CurrAgency = objAgency;
        }
        private void LoadUi(FIAgency objAgency)
        {
            try
            {
                if (objAgency == null) return;
                txtTen.Text = CurrAgency.Name;
                txtID.Text = CurrAgency.ID.ToString();
                txtDiDong.Text = CurrAgency.MobileNo;
                txtDiaChi.Text = CurrAgency.Address1;
                txtDienThoai.Text = CurrAgency.PhoneNo;
                txtEmail.Text = CurrAgency.Email;
                txtThanhPho.Text = CurrAgency.City;
                txtNumber.Text = @"0";
                txtNumber.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }

        private void LoadData(FIAgency objAgency)
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIPointModelService service = new FIPointModelService(unitOfWork);
                    List<PointModel> listPoint = service.FindByAgency(objAgency.ID);

                    int point = listPoint.Sum(s => s.PointCurrent);
                    if (CurrAgency == null) return;

                    dgvData.DataSource = listPoint;
                    dgvData.Refresh();

                    txtSLTong.Text = point == 0 ? @"0" : string.Format("{0:0,00}", point);
                }
            }
            catch
            {
                //
            }
        }

        private void GenerateKey(FIAgency objAgency)
        {
            try
            {
                int id = objAgency.ID;
                int point = int.Parse(txtNumber.Text);

                PointModel model = new PointModel {AgentId = id, PointOriginal = point};
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    FIPointModelService service = new FIPointModelService(unitOfWork);
                    bool res = service.GenerateNewKey(ref model);

                    if (!res)
                    {
                        _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                    }
                    else
                    {
                        txtKey.Text = model.KeyOriginal;
                        txtKey.Select();
                    }

                }

               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }

            LoadData(CurrAgency);
        }
        private void dgvData_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                int id = int.Parse(row.Cells[0].Value.ToString());
                //int idAgency = int.Parse(row.Cells[1].Value.ToString());
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    FIPointModelService servicepModelService = new FIPointModelService(unitOfWork);
                    PointModel model = servicepModelService.Find(id);

                    //IFIAgencyService service = new FIAgencyService(unitOfWork);
                    //CurrAgency = service.Find(idAgency);

                    txtNumberOfkey.Text = model.PointCurrent == 0 ? @"0" : string.Format("{0:0,00}", model.PointCurrent);
                    txtKey.Text = model.KeyOriginal;
                    txtNumber.Text = @"0";
                    txtNumber.Select();
                }
            }
            catch (Exception)
            {
                txtNumber.Text = @"0";
                txtNumberOfkey.Text = @"0";
                txtKey.Text = "";
                txtNumber.Select();
                //CurrAgency = null;
            }
        }

        private void dgvData_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            dgvData.Rows[e.RowIndex].Cells[2].Value = e.RowIndex + 1;
        }

        private void FrmKeys_Load(object sender, EventArgs e)
        {
            LoadUi(CurrAgency);
            LoadData(CurrAgency);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            GenerateKey(CurrAgency);
        }

        private void buttonSpecHeaderGroup1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnCopyKey_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtKey.Text.Trim());
        }
    }
}
