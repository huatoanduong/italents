﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.Admin.UI.GUI
{
    public partial class FrmAgency : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        public FIAgency CurrAgency { get; set; }

        public FrmAgency()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1,labAddress,labCellPhone,labCity,labEmail,labID,labName,labPhone,labPassword, labKeyXacNhan, labKey,labNote, btnGenerateKey, btnLuu };
            _baseForm = new BaseForm();
            btnLuu.Enabled = false;
            txtKeyRegister.Select();
        }

        public FrmAgency(FIAgency objAgency)
        {
            InitializeComponent();
            CurrAgency = objAgency;
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { c4FunHeaderGroup1, labAddress, labCellPhone, labCity, labEmail, labID, labName, labPhone, labPassword,labKeyXacNhan, labKey, labNote, btnGenerateKey, btnLuu };
            _baseForm = new BaseForm();
            btnLuu.Enabled = true;
            txtTen.Select();
        }

        private void LoadToUi(FIAgency obj)
        {
            try
            {
                //txtKeyCheck.Enabled = false;
                txtKeyRegister.Visible = false;
                btnGenerateKey.Visible = false;
                labKey.Visible = false;
                labNote.Visible = false;
                //using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                //{
                //    unitOfWork.OpenConnection();
                //    IFIAgencyService service = new FIAgencyService(unitOfWork);
                if (obj == null)
                {
                    //txtKeyCheck.Enabled = true;
                    txtKeyRegister.Visible = true;
                    btnGenerateKey.Visible = true;
                    labKey.Visible = true;
                    labNote.Visible = true;
                    //obj = service.CreateEntity();
                    obj = new FIAgency()
                    {
                        Country = "VietNam",
                        DOB = DateTime.Now.AddYears(-20),
                        Day_Actived = 0,
                        Dis_Activated = "1",
                        Gender = "None",
                        Username = "",
                        Password = "",
                        ID = 0,
                        Point = -1,
                        ProductKey = "",
                        SecKey = "",
                        SerailKey = "",
                        State = "1"
                    };
                    obj.ProductKey = "";
                    obj.SecKey = "";
                    obj.SerailKey = "";
                    obj.SaveImages = @"C:\";
                    obj.Email = "@gmail.com";
                    obj.City = "HCM";
                    obj.Country = "Việt Nam";
                }

                txtID.Text = obj.ID.ToString();
                txtTen.Text = obj.Name;
                txtDiaChi.Text = obj.Address1;
                txtThanhPho.Text = obj.City;
                txtEmail.Text = obj.Email;
                txtDiDong.Text = obj.MobileNo;
                txtDienThoai.Text = obj.PhoneNo;
                txtKeyCheck.Text = obj.SecKey;

                CurrAgency = obj;
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }

        private void Register()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtKeyCheck.Text))
                {

                    _baseForm.VietNamMsg = @"Chưa có mã xác nhận!";
                    _baseForm.EnglishMsg = @"Key verification null!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                if (string.IsNullOrWhiteSpace(txtTen.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập tên!";
                    _baseForm.EnglishMsg = @"Please input your name!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                if (string.IsNullOrWhiteSpace(txtEmail.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập email!";
                    _baseForm.EnglishMsg = @"Please input your email!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;
                }

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    bool isAdd = false;
                    int id = ConvertUtil.ToInt(txtID.Text);
                    FIAgency objAgency = service.Find(id);
                    if (objAgency == null)
                    {
                        objAgency = new FIAgency()
                        {
                            Country = "VietNam",
                            DOB = DateTime.Now.AddYears(-20),
                            Day_Actived = 0,
                            Dis_Activated = "1",
                            Gender = "None",
                            Username = "",
                            Password = "",
                            ID = 0,
                            Point = -1,
                            ProductKey = "",
                            SecKey = "",
                            SerailKey = "",
                            State = "1"
                        };
                        isAdd = true;
                    }

                    objAgency.Username = "AC50015";
                    objAgency.Password = "AC50015";
                    objAgency.Name = txtTen.Text;
                    objAgency.Email = txtEmail.Text;
                    objAgency.PhoneNo = txtDienThoai.Text;
                    objAgency.MobileNo = txtDiDong.Text;
                    objAgency.City = txtThanhPho.Text;
                    objAgency.SaveImages = "C:\\";
                    objAgency.Address1 = txtDiaChi.Text;
                    objAgency.SecKey = txtKeyCheck.Text;
                    objAgency.ProductKey = txtKeyRegister.Text;
                    objAgency.SerailKey = txtKeyCheck.Text + txtKeyRegister.Text;

                    Issuccess = isAdd ? service.Add(objAgency) : service.Update(objAgency);

                    if (!Issuccess)
                    {
                        _baseForm.ShowMessage(IconMessageBox.Warning, service.ErrMsg);
                        return;
                    }
                    base.Dispose();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                Environment.Exit(0);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Dispose();
        }

        private void buttonSpecHeaderGroup1_Click(object sender, EventArgs e)
        {
            base.Dispose();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            Register();
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            try
            {
                string key = GeneratePos.GetKeyDe(txtKeyRegister.Text.Trim());
                if (key != "")
                {
                    using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                    {
                        IFIAgencyService service = new FIAgencyService(unitOfWork);
                        var tmpObj1 = service.FindBySeckey(key);
                        if (tmpObj1 != null)
                        {
                            txtKeyCheck.Text = "";
                            _baseForm.VietNamMsg = "Mã xác nhận này đã có đại lý sử dụng!\nVui lòng liên hệ người quản lý.";
                            _baseForm.EnglishMsg = "The key has exists!\nPlease contract admin.";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            return;
                        }
                    }

                    txtKeyCheck.Text = key;
                    txtKeyRegister.Enabled = false;
                    btnGenerateKey.Enabled = false;
                    btnLuu.Enabled = true;

                    _baseForm.VietNamMsg = "Mã hợp lệ!\nVui lòng điền đầy đủ thông tin đại lý.";
                    _baseForm.EnglishMsg = "The key is valid!\nPlease input information's agency.";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                }
            }
            catch (Exception ex)
            {
                txtKeyCheck.Text = "";
               _baseForm.VietNamMsg = @"Key không hợp lệ!";
                _baseForm.EnglishMsg = @"the key is not valid!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void FrmAgency_Load(object sender, EventArgs e)
        {
            LoadToUi(CurrAgency);
        }
    }
}
