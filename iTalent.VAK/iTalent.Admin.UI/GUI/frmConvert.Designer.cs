﻿namespace iTalent.Admin.UI.GUI
{
    partial class FrmConvert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConvert));
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.lblInputFile = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.lblOutputFile = new System.Windows.Forms.Label();
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnExit = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.btnRevert = new System.Windows.Forms.Button();
            this.btnBrowseOutput = new System.Windows.Forms.Button();
            this.btnBrowseInput = new System.Windows.Forms.Button();
            this.btnToDocx = new System.Windows.Forms.Button();
            this.btnToRptx = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            // 
            // txtInput
            // 
            this.txtInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInput.Location = new System.Drawing.Point(145, 18);
            this.txtInput.Name = "txtInput";
            this.txtInput.ReadOnly = true;
            this.txtInput.Size = new System.Drawing.Size(250, 29);
            this.txtInput.TabIndex = 192;
            // 
            // lblInputFile
            // 
            this.lblInputFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblInputFile.BackColor = System.Drawing.Color.Transparent;
            this.lblInputFile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputFile.Location = new System.Drawing.Point(14, 21);
            this.lblInputFile.Name = "lblInputFile";
            this.lblInputFile.Size = new System.Drawing.Size(125, 21);
            this.lblInputFile.TabIndex = 199;
            this.lblInputFile.Tag = "File Nhập :";
            this.lblInputFile.Text = "Input File :";
            this.lblInputFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtOutput
            // 
            this.txtOutput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtOutput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtOutput.Location = new System.Drawing.Point(145, 53);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(250, 29);
            this.txtOutput.TabIndex = 187;
            // 
            // lblOutputFile
            // 
            this.lblOutputFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOutputFile.BackColor = System.Drawing.Color.Transparent;
            this.lblOutputFile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutputFile.Location = new System.Drawing.Point(10, 56);
            this.lblOutputFile.Name = "lblOutputFile";
            this.lblOutputFile.Size = new System.Drawing.Size(129, 21);
            this.lblOutputFile.TabIndex = 194;
            this.lblOutputFile.Tag = "File Xuất :";
            this.lblOutputFile.Text = "Output File :";
            this.lblOutputFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.btnExit});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnRevert);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnBrowseOutput);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnBrowseInput);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnToDocx);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnToRptx);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInput);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.lblInputFile);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtOutput);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.lblOutputFile);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(514, 173);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 151;
            this.c4FunHeaderGroup1.Tag = "Chuyển Đổi";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Convert File";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.Admin.UI.Properties.Resources.refresh_32;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // btnExit
            // 
            this.btnExit.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnExit.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRevert
            // 
            this.btnRevert.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRevert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnRevert.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnRevert.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnRevert.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnRevert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRevert.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRevert.ForeColor = System.Drawing.Color.White;
            this.btnRevert.Image = global::iTalent.Admin.UI.Properties.Resources.refresh_32;
            this.btnRevert.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRevert.Location = new System.Drawing.Point(447, 32);
            this.btnRevert.Margin = new System.Windows.Forms.Padding(0);
            this.btnRevert.Name = "btnRevert";
            this.btnRevert.Size = new System.Drawing.Size(42, 43);
            this.btnRevert.TabIndex = 203;
            this.btnRevert.Tag = "...";
            this.btnRevert.UseVisualStyleBackColor = false;
            this.btnRevert.Visible = false;
            this.btnRevert.Click += new System.EventHandler(this.btnRevert_Click);
            // 
            // btnBrowseOutput
            // 
            this.btnBrowseOutput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnBrowseOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBrowseOutput.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnBrowseOutput.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnBrowseOutput.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnBrowseOutput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowseOutput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseOutput.ForeColor = System.Drawing.Color.White;
            this.btnBrowseOutput.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBrowseOutput.Location = new System.Drawing.Point(398, 53);
            this.btnBrowseOutput.Margin = new System.Windows.Forms.Padding(0);
            this.btnBrowseOutput.Name = "btnBrowseOutput";
            this.btnBrowseOutput.Size = new System.Drawing.Size(37, 29);
            this.btnBrowseOutput.TabIndex = 202;
            this.btnBrowseOutput.Tag = "...";
            this.btnBrowseOutput.Text = "...";
            this.btnBrowseOutput.UseVisualStyleBackColor = false;
            this.btnBrowseOutput.Click += new System.EventHandler(this.btnBrowseOutput_Click);
            // 
            // btnBrowseInput
            // 
            this.btnBrowseInput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnBrowseInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBrowseInput.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnBrowseInput.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnBrowseInput.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnBrowseInput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowseInput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseInput.ForeColor = System.Drawing.Color.White;
            this.btnBrowseInput.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBrowseInput.Location = new System.Drawing.Point(398, 18);
            this.btnBrowseInput.Margin = new System.Windows.Forms.Padding(0);
            this.btnBrowseInput.Name = "btnBrowseInput";
            this.btnBrowseInput.Size = new System.Drawing.Size(37, 29);
            this.btnBrowseInput.TabIndex = 201;
            this.btnBrowseInput.Tag = "...";
            this.btnBrowseInput.Text = "...";
            this.btnBrowseInput.UseVisualStyleBackColor = false;
            this.btnBrowseInput.Click += new System.EventHandler(this.btnBrowseInput_Click);
            // 
            // btnToDocx
            // 
            this.btnToDocx.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnToDocx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnToDocx.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnToDocx.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnToDocx.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnToDocx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToDocx.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToDocx.ForeColor = System.Drawing.Color.White;
            this.btnToDocx.Image = global::iTalent.Admin.UI.Properties.Resources.left;
            this.btnToDocx.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToDocx.Location = new System.Drawing.Point(186, 89);
            this.btnToDocx.Margin = new System.Windows.Forms.Padding(0);
            this.btnToDocx.Name = "btnToDocx";
            this.btnToDocx.Size = new System.Drawing.Size(99, 41);
            this.btnToDocx.TabIndex = 200;
            this.btnToDocx.Tag = "Docx";
            this.btnToDocx.Text = "Docx";
            this.btnToDocx.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnToDocx.UseVisualStyleBackColor = false;
            this.btnToDocx.Visible = false;
            this.btnToDocx.Click += new System.EventHandler(this.btnToDocx_Click);
            // 
            // btnToRptx
            // 
            this.btnToRptx.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnToRptx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnToRptx.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnToRptx.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnToRptx.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnToRptx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToRptx.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToRptx.ForeColor = System.Drawing.Color.White;
            this.btnToRptx.Image = global::iTalent.Admin.UI.Properties.Resources.right;
            this.btnToRptx.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToRptx.Location = new System.Drawing.Point(296, 89);
            this.btnToRptx.Margin = new System.Windows.Forms.Padding(0);
            this.btnToRptx.Name = "btnToRptx";
            this.btnToRptx.Size = new System.Drawing.Size(99, 41);
            this.btnToRptx.TabIndex = 186;
            this.btnToRptx.Tag = "Rptx";
            this.btnToRptx.Text = "Rptx";
            this.btnToRptx.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnToRptx.UseVisualStyleBackColor = false;
            this.btnToRptx.Click += new System.EventHandler(this.btnToRptx_Click);
            // 
            // frmConvert
            // 
            this.AcceptButton = this.btnToRptx;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 173);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConvert";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Label lblInputFile;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label lblOutputFile;
        private System.Windows.Forms.Button btnToRptx;
        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnExit;
        private System.Windows.Forms.Button btnToDocx;
        private System.Windows.Forms.Button btnBrowseInput;
        private System.Windows.Forms.Button btnBrowseOutput;
        private System.Windows.Forms.Button btnRevert;
    }
}