﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;

namespace iTalent.Admin.UI.GUI
{
   
    public partial class FrmLogin : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        private readonly Config _config;
        public FrmLogin()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1, labUsername, labPassword, btnSave};
            _baseForm = new BaseForm();
            _config = new Config();
            Issuccess = false;
            txtPassword.Select();
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            try
            {
                Issuccess = _config.CheckLogin(txtPassword.Text.Trim());
                if (Issuccess)
                    Dispose();
                else
                {
                    _baseForm.VietNamMsg = "Mật khẩu không đúng!";
                    _baseForm.EnglishMsg = "Worng Password!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                }
            }
            catch (Exception)
            {
                //
                throw;
            }
        }

        private void btnExit_Click(object sender, System.EventArgs e)
        {
           Environment.Exit(0);
        }

        private void btnSave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }
    }
}
