﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Admin.UI.GUI;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Service;

namespace iTalent.Admin.UI.Language
{
    //public enum Languages
    //{
    //    English,
    //    VietNam
    //}
   
    public class Lang
    {
        private bool _vietNamLanguage;
        private static Lang _instance;
        public static Lang Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Lang {_vietNamLanguage = ICurrentSessionService.VietNamLanguage};
                    _instance.LoadLanguage();
                }
                else
                {
                    _instance._vietNamLanguage = ICurrentSessionService.VietNamLanguage;
                    _instance.LoadLanguage();
                }
                return _instance;
            }
        }

        public Lang()
        {

        }

        //public Lang(Form T)
        //{
        //    FrmActive = T;
        //}

        //private string Key { get; set; }

        private string Value { get
            ;
            set;
        }

        public static Form FrmActive { get; set; }

        public static ICollection<Control> ListControls { get; set; } 
        public static C4FunDataGridView DgView { get; set; }

        //protected Dictionary<string, string> FrmWebCam = new Dictionary<string, string>
        //{
        //    {"lablNameForm", "Webcam@Chụp Hình"} ,
        //     {"btnFace", "Take a photo@Chụp Hình"}
        //};
        protected Dictionary<string, string> FormLogin = new Dictionary<string, string>
        {
            {"c4FunHeaderGroup1", "Confirm@Xác Nhận"},
            {"labUsername", "Username:@Tên Đăng Nhập:"},
            {"labPassword", "Password:@Mật Khẩu:"},
            {"btnSave", "Next@Tiếp Tục"}
        };

        protected Dictionary<string, string> FormConvert = new Dictionary<string, string>
        {
            {"c4FunHeaderGroup1", "Convert Report@Chuyển Đổi Mẫu Báo Cáo"},
            {"lblOutputFile", "OutputFile:@Nơi Lưu:"},
            {"lblInputFile", "InputFile:@Chọn File:"},
            {"btnToRptx", "Rptx@Rptx"}
        };

        protected Dictionary<string, string> FormKeys = new Dictionary<string, string>
        {
            {"c4FunHeaderGroup1", "Generate Key@Cấp Key Báo Cáo"},
            {"labAddress", "Address:@Địa Chỉ:"},
            {"labCellPhone", "CellPhone:@Di Động:"},
            {"labCity", "City:@Thành Phố:"},
            {"labEmail", "Email:@Email:"},
            {"labPassword", "Password:@Mật Khẩu:"},
            {"labID", "ID Agency:@Mã Đại Lý:"},
            {"labName", "Name:@Tên:"},
            {"labPhone", "Phone:@Điện Thoại:"},
            {"btnGenerate", "Generate Key@Cấp SL Báo Cáo"},

            {"btnCopyKey", "Copy Key@Sao Chép"},
            {"labNumberSum", "Number Sum:@SL Tổng:"},

            {"colID", "ID@Mã"},
            {"colAgencyID", "ID@Mã"},
            {"colDateCreate", "DateCreate@Ngày Cấp"},
            {"colNumber", "Number@Số Lượng"},
            {"colKey", "Key@Mã Bản Quyền"}
        };

        protected Dictionary<string, string> FormAgency = new Dictionary<string, string>
        {
            {"c4FunHeaderGroup1", "Agency information@Thông Tin Đại Lý"},
            {"labAddress", "Address:@Địa Chỉ:"},
            {"labCellPhone", "CellPhone:@Di Động:"},
            {"labCity", "City:@Thành Phố:"},
            {"labEmail", "Email:@Email:"},
            {"labPassword", "Password:@Mật Khẩu:"},
            {"labID", "ID Agency:@Mã Đại Lý:"},
            {"labName", "Name:@Tên:"},
            {"labPhone", "Phone:@Điện Thoại:"},
            {"btnLuu", "Save@Lưu"},

            {"btnGenerateKey", ".:GenerateKey@.:Tạo Mã Đại Lý"},


            {"labKeyXacNhan", ".:Key Send To Agnecy@.:Mã Gửi Đại Lý"},
            {"labKey", "Key Verification@Mã Xác Nhận Từ Đại Lý"},
            {
                "labNote", "Note : Pares key register in text box key register.\n" +
                           "Press button Generate key.\n" +
                           "Copy key at textbox key verification and send key to agency."+
                "@Chi chú : Sao chép key mà đại lý gửi về dán vào ô xác nhận key.\n" +
                           "Nhấn nút xác nhân.\n" +
                           "Sao chép lại key trong ô mã gửi đại lý và gửi lại cho đại lý đó!"
            }
        };
        protected Dictionary<string, string> FormConfig = new Dictionary<string, string>
        {
            {"c4FunHeaderGroup1", "Option@Tùy Chọn"},
            {"labLang", "Language:@Ngôn Ngữ:"},
            {"raVietNam", "Viet Nam@Tiếng Việt"},
            {"raEnglish", "English@Tiếng Anh"},
            {"labPassword", "Password Login:@Mật Khẩu Đăng Nhập:"},
            {"btnSave", "Save@Lưu"}
        };

        //protected Dictionary<string, string> FrmSignup = new Dictionary<string, string>
        //{
        //    {"c4FunHeaderGroup1", "User Information@Thông Tin Người Dùng"},
        //    {"labAddress", "Address:@Địa Chỉ:"},
        //    {"labCellPhone", "CellPhone:@Di Động:"},
        //    {"labCity", "City:@Thành Phố:"},
        //    {"labEmail", "Email:@Email:"},
        //    {"labPassword", "Password:@Mật Khẩu:"},
        //    {"labID", "ID Agency:@Mã Đại Lý:"},
        //    {"labName", "Name:@Tên:"},
        //    {"labPhone", "Phone:@Điện Thoại:"},
        //    {"btnLuu", "Register@Đăng Ký"}
        //};

        //protected Dictionary<string, string> FrmCustomer = new Dictionary<string, string>
        //{
        //    {"c4FunHeaderGroup1", "Customer Information@Thông Tin Khách Hàng"},
        //    {"labName", "Name:@Tên:"},
        //    {"labSex", "Gender:@Giới Tính:"},
        //    {"labParent", "Parent:@Cha/Mẹ:"},
        //    {"labBirthday", "Birthday:@Ngày Sinh:"},
        //    {"labCity", "City:@Thành Phố:"},
        //    {"labAddress", "Address:@Địa Chỉ:"},
        //    {"labEmail", "Email:@Email:"},
        //    {"labMobile", "Mobile:@Di Động:"},
        //    {"Phone", "Telephone:@Điện Thoại:"},
        //    {"labZip", "Zippostal:@Mã Bưu Điện:"},
        //    {"labEntity", "Entity:@Đối Tượng:"},
        //    {"labNote", "Remark:@Ghi Chú:"},
        //    {"btnScaner", "Scanner@Lấy Vân Tay"},
        //    {"btnLuu", "Save@Lưu"},
        //    {"rdMale", "Male@Nam"},
        //    {"rdFemale", "Female@Nữ"},
        //     {"btnCapture", "Capture@Chụp Hình"}
        //};
        //protected Dictionary<string, string> FrmImportCustomer = new Dictionary<string, string>
        //{
        //    {"c4FunHeaderGroup1", "Import Customer Information@Nhập Thông Tin Khách Hàng"},
        //     {"labChoice", "Choice file .dat:@Chọn tập tin .dat:"},
        //    {"labName", "Name:@Tên:"},
        //    {"labSex", "Gender:@Giới Tính:"},
        //    {"labParent", "Parent:@Cha/Mẹ:"},
        //    {"labBirthday", "Birthday:@Ngày Sinh:"},
        //    {"labCity", "City:@Thành Phố:"},
        //    {"labAddress", "Address:@Địa Chỉ:"},
        //    {"labEmail", "Email:@Email:"},
        //    {"labMobile", "Mobile:@Di Động:"},
        //    {"Phone", "Phone:@Điện Thoại:"},
        //    {"labZip", "Zippostal:@Mã Bưu Điện:"},
        //    {"labEntity", "Entity:@Đối Tượng:"},
        //    {"labNote", "Remark:@Ghi Chú:"},
        //    {"btnScaner", "Scanner@Lấy Vân Tay"},
        //    {"btnLuu", "Save@Lưu"},
        //    {"rdMale", "Male@Nam"},
        //    {"rdFemale", "Female@Nữ"},
        //     {"btnCapture", "Capture@Chụp Hình"}
        //};

        protected Dictionary<string, string> FrmMain = new Dictionary<string, string>
        {
            //{"btnRefresh", "@Tải Lại"},
            {"btnAdd", "Add@Thêm"},
            {"btnEdit", "Edit@Sửa"},
            {"btnExport", "Export@Xuất File"},
            {"btnConfig", "Option@Tùy Chọn"},
            {"labSearch", "Search:@Tìm Kiếm:"},

            {"btnCreateKey", "Generate Key@Cấp SL Báo Cáo"},
            {"btnConvertTool", "Convert Report@Chuyển Đổi Báo Cáo"},

            {"colID", "ID@Mã"},
            {"colSeckey", "ID@Mã"},
            {"colName", "Agnecy@Đại Lý"},
            {"colTel", "Tel@Điện Thoại"},
            {"colMobile", "Mobile@Di Động"},
            {"colEmail", "Email@Email"},
            {"colAddress", "Address@Địa Chỉ"},
            {"colCity", "City@Thành Phố"}
        };

        protected void LoadLanguage()
        {
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                switch (FrmActive.Name)
                {
                    case "FrmLogin":
                        dictionary = FormLogin;
                        break;
                    case "FrmConfig":
                        dictionary = FormConfig;
                        break;
                    case "FrmKeys":
                        dictionary = FormKeys;
                        break;
                    case "FrmAgency":
                        dictionary = FormAgency;
                        break;
                    case "frmConvert":
                        dictionary = FormConvert;
                        break;
                    case "frmMain":
                        dictionary = FrmMain;
                        break;

                    //case "FrmCustomer":
                    //    dictionary = FrmCustomer;
                    //    break;

                    //case "FrmWebCam":
                    //    dictionary = FrmWebCam;
                    //    break;

                    //case "FrmImportCustomer":
                    //    dictionary = FrmImportCustomer;
                    //    break;
                }

                foreach (var obj in ListControls.Where(obj => dictionary.ContainsKey(obj.Name)))
                {
                    try
                    {
                        string s1;
                        dictionary.TryGetValue(obj.Name, out s1);
                        Value = s1;
                        if (obj is C4FunHeaderGroup)
                        {
                            (obj as C4FunHeaderGroup).ValuesPrimary.Heading = SplitString;
                            continue;

                        }

                        obj.Text = SplitString;
                    }
                    catch
                    { }
                }

                if (DgView == null) return;
                if (DgView.Columns.Count == 0) return;

                foreach (
                    DataGridViewTextBoxColumn col in
                        DgView.Columns.Cast<DataGridViewTextBoxColumn>().Where(col => dictionary.ContainsKey(col.Name)))
                {
                    try
                    {
                        string s1;
                        dictionary.TryGetValue(col.Name, out s1);
                        Value = s1;
                        col.HeaderText = SplitString;
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
                
            }
        }
        private string SplitString 
        {
            get
            {
                try
                {
                    string[] v = Value.Split('@');
                    return _vietNamLanguage ? v[1] : v[0];
                }
                catch
                {
                    return "";
                }
            }
        }
    }
}
