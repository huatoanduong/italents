﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace iTalent.Utils
{
    public static partial class ExtendMethod
    {
        public static string ToDbFormatString(this DateTime time)
        {
            return time.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static DateTime ToDateTime(this string input, string strFomatter)
        {
            DateTime obj;
            obj = DateTime.ParseExact(input, strFomatter,
                CultureInfo.CurrentCulture);
            return obj;
        }

        public static int ToInt(this string input)
        {
            return int.Parse(input);
        }

        public static decimal ToDecimal(this string input)
        {
            return decimal.Parse(input);
        }

        public static string ToUnsign(this string input)
        {
            if (input == null) return null;
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            var temp = input.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string ToSpaceIfBlank(this string input)
        {
            if (input.IsBlank())
            {
                return input.ToDefaultIfBlank(" ");
            }
            return input;
        }

        public static string ToNullIfEqual(this string input, string compareString)
        {
            if (input == compareString)
            {
                return null;
            }
            return input;
        }

        public static string ToDefaultIfBlank(this string input, string defaultString = "")
        {
            if (input.IsBlank())
            {
                return defaultString;
            }
            return input;
        }
    }
}