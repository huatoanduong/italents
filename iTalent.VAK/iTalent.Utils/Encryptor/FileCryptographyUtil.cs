﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Util.Pattern.Encryptor
{
    public class FileCryptographyUtil
    {
        ///<summary>
        /// Steve Lydford - 12/05/2008.
        ///
        /// Encrypts a file using Rijndael algorithm.
        ///</summary>
        ///<param name="inputFile"></param>
        ///<param name="outputFile"></param>
        public static void EncryptFile(string inputFile, string outputFile, string password)
        {
            try
            {
                //string password = @"myKey123"; // Your Key Here
                //UnicodeEncoding UE = new UnicodeEncoding();
                //byte[] passBytes = UE.GetBytes(password);
                //byte[] key = GetBytes(passBytes);
                byte[] key = GetBytes(password);
                byte[] ivKey = new byte[key.Length];
                Array.Copy(key, ivKey, key.Length);

                string cryptFile = outputFile;
                using (FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create))
                {
                    using (RijndaelManaged RMCrypto = new RijndaelManaged())
                    {
                        //CryptoStream cs = new CryptoStream(fsCrypt,
                        //    RMCrypto.CreateEncryptor(key, key),
                        //    CryptoStreamMode.Write);

                        RMCrypto.KeySize = key.Length * 8;
                        RMCrypto.Key = key;
                        RMCrypto.BlockSize = ivKey.Length * 8;
                        RMCrypto.IV = ivKey;

                        using (ICryptoTransform icrypto = RMCrypto.CreateEncryptor())
                        {
                            using (CryptoStream cs = new CryptoStream(fsCrypt, icrypto, CryptoStreamMode.Write))
                            {
                                using (FileStream fsIn = new FileStream(inputFile, FileMode.Open))
                                {
                                    int data;
                                    while ((data = fsIn.ReadByte()) != -1)
                                        cs.WriteByte((byte) data);


                                    fsIn.Close();
                                    cs.Close();
                                    fsCrypt.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        ///<summary>
        /// Steve Lydford - 12/05/2008.
        ///
        /// Decrypts a file using Rijndael algorithm.
        ///</summary>
        ///<param name="inputFile"></param>
        ///<param name="outputFile"></param>
        public static void DecryptFile(string inputFile, string outputFile, string password)
        {
            try
            {

                //string password = @"myKey123"; // Your Key Here

                byte[] key = GetBytes(password);
                byte[] ivKey = new byte[key.Length];
                Array.Copy(key, ivKey, key.Length);

                using (FileStream fsCrypt = new FileStream(inputFile, FileMode.Open))
                {
                    using (RijndaelManaged RMCrypto = new RijndaelManaged())
                    {
                        //CryptoStream cs = new CryptoStream(fsCrypt,
                        //    RMCrypto.CreateDecryptor(key, key),
                        //    CryptoStreamMode.Read);

                        RMCrypto.KeySize = key.Length * 8;
                        RMCrypto.Key = key;
                        RMCrypto.BlockSize = ivKey.Length * 8;
                        RMCrypto.IV = ivKey;

                        using (ICryptoTransform icrypto = RMCrypto.CreateDecryptor())
                        {
                            using (CryptoStream cs = new CryptoStream(fsCrypt, icrypto, CryptoStreamMode.Read))
                            {
                                using (FileStream fsOut = new FileStream(outputFile, FileMode.Create))
                                {
                                    int data;
                                    while ((data = cs.ReadByte()) != -1)
                                        fsOut.WriteByte((byte) data);

                                    fsOut.Close();
                                    cs.Close();
                                    fsCrypt.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        protected static byte[] GetBytes(string password)
        {
            byte[] resBytes = null;

            byte[] Salt = Encoding.Unicode.GetBytes(password);
            Rfc2898DeriveBytes pwdGen = new Rfc2898DeriveBytes(password, Salt, 10000);

            resBytes = pwdGen.GetBytes(256/8);

            return resBytes;
        }

        protected static byte[] GetBytes(byte[] input)
        {
            int keyLeng1 = 128/8;
            int keyLeng2 = 192/8;
            int keyLeng3 = 256/8;
            int returnLeng = keyLeng1;

            byte[] resBytes = null;

            int inputLeng = input.Length;

            resBytes = new byte[returnLeng];
            Array.Copy(input, resBytes, returnLeng);
            return resBytes;
        }
    }
}