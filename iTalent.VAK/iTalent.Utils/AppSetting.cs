﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;
using Microsoft.Win32;

namespace iTalent.Utils
{
    public class AppSetting
    {
        private static AppSetting _aps;
        public static string ConfigFileName;
        private static XmlDocument _xml = new XmlDocument();
        private static XmlNode _appSettingsElement;
        private static XmlElement _newElement;
        private static string _mAppName = "";
        private readonly CultureInfo _cultureInfo;

        public AppSetting()
        {
            _cultureInfo = new CultureInfo("vi-VN")
            {
                NumberFormat =
                {
                    CurrencyDecimalDigits = 0,
                    CurrencySymbol = string.Empty,
                    CurrencyGroupSeparator = ".",
                    CurrencyDecimalSeparator = ","
                },
                DateTimeFormat = {ShortTimePattern = "", LongTimePattern = ""}
            };
            //System.Windows.Forms.MessageBox.Show(CultureInfo.NumberFormat.CurrencyDecimalDigits.ToString());
            // Đặt lại số lẻ sau dấu chấm thập phân là không có số lẻ
            // Đặt lại ký tự tiền tệ là không có

            //if (m_AppName == "")
            //    configFileName = Environment.CurrentDirectory + "\\" + AppDomain.CurrentDomain.FriendlyName + ".config";
            //else
            //    configFileName = Environment.CurrentDirectory + "\\" + m_AppName + ".config";

            var path = Path.GetPathRoot(Environment.SystemDirectory) + @"FConfig";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            if (_mAppName == "")
                ConfigFileName = path + "\\" + AppDomain.CurrentDomain.FriendlyName.Replace(".vshost.", ".") + ".config";
            else
                ConfigFileName = path + "\\" + _mAppName + ".config";

            if (!File.Exists(ConfigFileName))
            {
                File.Move(
                    Directory.GetCurrentDirectory() + "\\" +
                    AppDomain.CurrentDomain.FriendlyName.Replace(".vshost.", ".") + ".config", ConfigFileName);
            }

            if (!File.Exists(ConfigFileName))
            {
                try
                {
                    //Get default file config and write one
                    const string defaultConfig = "";
                    //string defaultConfig = getDefaultConfig();
                    WriteFile(ConfigFileName, defaultConfig);
                }
                catch
                {
                    // no configuration file exists
                    // and can't write a default one
                    throw new ApplicationException("Can't create configuration file for application (" +
                                                   Environment.CurrentDirectory + "\\" +
                                                   AppDomain.CurrentDomain.FriendlyName);
                }
            }
            try
            {
                // Create a new XmlDocument object
                _xml = new XmlDocument();

                // load the configuration file
                _xml.Load(ConfigFileName);

                // get the <appSettings> element
                _appSettingsElement = _xml.SelectSingleNode("//configuration/appSettings");

                // if there's no <appSettings> section, create one.
                if (_appSettingsElement == null)
                {
                    _appSettingsElement = _xml.CreateElement("appSettings");
                    if (_xml.DocumentElement != null) _xml.DocumentElement.AppendChild(_appSettingsElement);
                }

                //DATETIME_FORMAT = this.getConfigValue("DATETIME_FORMAT");
            }
            catch (Exception ex)
            {
                // handle Load errors
                throw new ApplicationException("Unable to load the configuration " + ConfigFileName +
                                               " file for this application.", ex);
            }
            //}
            //else
            //{
            //    // no configuration file exists
            //    // you might want to alter this method to  create one
            //    throw new ApplicationException("This application (" + Environment.CurrentDirectory + "\\" + AppDomain.CurrentDomain.FriendlyName + ".config" + " has no configuration file.");

            //}
        }

        public CultureInfo GetCulture()
        {
            return _cultureInfo;
        }

        public void SetValue(string key, string val)
        {
            // create a new <add> element
            _newElement = _xml.CreateElement("add");
            var s = GetValue(key);
            if (s == null)
            {
                // set its attributes
                _newElement.SetAttribute("key", key);
                //val = Security.Encrypt(val, "khaihoanproducts","takeru");
                _newElement.SetAttribute("value", val);

                // append it to the <appSettings> section
                _appSettingsElement.AppendChild(_newElement);
            }
            else
            {
                var appsetting = (XmlElement) (_appSettingsElement.SelectSingleNode("add[@key='" + key + "']"));
                if (appsetting != null)
                {
                    //val = Security.Encrypt(val, "khaihoanproducts","takeru");
                    appsetting.SetAttribute("value", val);
                }
            }
            try
            {
                _xml.Save(ConfigFileName);
            }
            catch
            {
                Thread.Sleep(3000);
                _xml.Save(ConfigFileName);
            }
        }

        public string GetValue(string aKey)
        {
            var appsetting = (XmlElement) (_appSettingsElement.SelectSingleNode("add[@key='" + aKey + "']"));
            if (appsetting != null)
            {
                //return Security.Decrypt(appsetting.GetAttribute("value"), "khaihoan", "hontre");
                return (appsetting.GetAttribute("value"));
            }
            return null; //String.Empty ;
        }

        public void Remove(string aKey)
        {
            var el = _GetSettingsElement(aKey);
            if (el != null)
            {
                if (el.ParentNode != null) el.ParentNode.RemoveChild(el);
                //dirty = True;
            }
            _xml.Save(ConfigFileName);
        }

        private XmlElement _GetSettingsElement(string aKey)
        {
            return (XmlElement) (_appSettingsElement.SelectSingleNode("add[@key='" + aKey + "']"));
        }

        public static AppSetting getSetting(string appName)
        {
            _mAppName = appName;

            if (_aps == null)
            {
                _aps = new AppSetting();
                return _aps;
            }
            return _aps;
        }

        public static AppSetting getSetting()
        {
            if (_aps == null)
            {
                _aps = new AppSetting();
                return _aps;
            }
            return _aps;
        }

        public bool ExistKey(string aKey)
        {
            try
            {
                var appsetting = (XmlElement) (_appSettingsElement.SelectSingleNode("add[@key='" + aKey + "']"));
                if (appsetting == null)
                    return false;

                return true;
            }
            catch
            {
                return false;
            }
        }

        //private string getDefaultConfig()
        //{
        //    return AnimalCare.Resource1.DefaultConfigString;
        //}

        private void WriteFile(string fileName, string content)
        {
            TextWriter tw = new StreamWriter(fileName, false);

            // write a line of text to the file
            tw.WriteLine(content);

            // close the stream
            tw.Close();
        }
    }

    public class RegSetting
    {
        [DefaultValue(@"Software\toro\Ftalk")]
        private string _cRegistryKey = @"Software\toro\Ftalk";

        public string CRegistryKey
        {
            get { return _cRegistryKey; }
            set { _cRegistryKey = value; }
        }

        // set property cho _RegistryKey truoc khi goi 02 ham duoi day
        public string GetValue(string aKey)
        {
            string avalue = null;
            var regKey = Registry.LocalMachine.OpenSubKey(CRegistryKey);
            if (regKey != null)
            {
                avalue = (string) regKey.GetValue(aKey);
                regKey.Close();
            }

            return avalue;
        }

        public void SetValue(string aKey, string val)
        {
            //RegistryKey regKey = Registry.CurrentUser.CreateSubKey(_RegistryKey);
            var regKey = Registry.LocalMachine.CreateSubKey(CRegistryKey);
            if (regKey != null)
            {
                regKey.SetValue(aKey, val);
                regKey.Close();
            }
        }
    }
}