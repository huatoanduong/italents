﻿using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace iTalent.NGF.GUI
{
    public partial class FrmZoom : Form
    {
        public string Rc { get; set; }
        
        public FrmZoom(PictureBox ptb,TextBox txtRc)
        {
            InitializeComponent();
            imgZoom.Image = ptb.Image;
            txtRC.Text = txtRc.Text;
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            Rc = txtRC.Text;
            Close();
        }

        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            const string str = "0123456789";
            if (Strings.InStr(str, e.KeyChar.ToString()) == 0)
            {
                e.Handled = true;
            }
            switch (Convert.ToInt32(e.KeyChar))
            {
                case 8:
                    e.Handled = false;
                    break;

                case 190:
                    e.Handled = false;
                    break;

                case 46:
                    e.Handled = false;
                    break;
            }
        }

    }
}
