﻿namespace iTalent.NGF.GUI
{
    partial class FrmWebCam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof (FrmWebCam));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLHand = new System.Windows.Forms.Button();
            this.btnFace = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.lablNameForm = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.labDiaChiEmailDienThoai = new System.Windows.Forms.Label();
            this.picOutput = new System.Windows.Forms.PictureBox();
            this.btnRHand = new System.Windows.Forms.Button();
            this.picRHand = new System.Windows.Forms.PictureBox();
            this.picFace = new System.Windows.Forms.PictureBox();
            this.picLHand = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.pnlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.picOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picRHand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picFace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picLHand)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.picOutput);
            this.panel1.Controls.Add(this.btnRHand);
            this.panel1.Controls.Add(this.btnLHand);
            this.panel1.Controls.Add(this.btnFace);
            this.panel1.Controls.Add(this.picRHand);
            this.panel1.Controls.Add(this.picFace);
            this.panel1.Controls.Add(this.picLHand);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(681, 412);
            this.panel1.TabIndex = 0;
            // 
            // btnLHand
            // 
            this.btnLHand.AutoSize = true;
            this.btnLHand.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))), ((int) (((byte) (120)))),
                ((int) (((byte) (192)))));
            this.btnLHand.FlatAppearance.BorderSize = 0;
            this.btnLHand.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))),
                ((int) (((byte) (37)))), ((int) (((byte) (54)))));
            this.btnLHand.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (5)))),
                ((int) (((byte) (92)))), ((int) (((byte) (145)))));
            this.btnLHand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLHand.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnLHand.ForeColor = System.Drawing.Color.White;
            this.btnLHand.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLHand.Location = new System.Drawing.Point(124, 453);
            this.btnLHand.Name = "btnLHand";
            this.btnLHand.Size = new System.Drawing.Size(105, 54);
            this.btnLHand.TabIndex = 1;
            this.btnLHand.Text = "1.Tay Trái (F2)";
            this.btnLHand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLHand.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLHand.UseVisualStyleBackColor = false;
            this.btnLHand.Visible = false;
            this.btnLHand.Click += new System.EventHandler(this.btnLHand_Click);
            // 
            // btnFace
            // 
            this.btnFace.AutoSize = true;
            this.btnFace.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))), ((int) (((byte) (120)))),
                ((int) (((byte) (192)))));
            this.btnFace.FlatAppearance.BorderSize = 0;
            this.btnFace.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))),
                ((int) (((byte) (37)))), ((int) (((byte) (54)))));
            this.btnFace.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (5)))),
                ((int) (((byte) (92)))), ((int) (((byte) (145)))));
            this.btnFace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFace.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnFace.ForeColor = System.Drawing.Color.White;
            this.btnFace.Location = new System.Drawing.Point(461, 251);
            this.btnFace.Name = "btnFace";
            this.btnFace.Size = new System.Drawing.Size(175, 50);
            this.btnFace.TabIndex = 1;
            this.btnFace.Text = "^^ Chụp Mặt";
            this.btnFace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFace.UseVisualStyleBackColor = false;
            this.btnFace.Click += new System.EventHandler(this.btnFace_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (5)))), ((int) (((byte) (92)))),
                ((int) (((byte) (145)))));
            this.pnlMenu.Controls.Add(this.lablNameForm);
            this.pnlMenu.Controls.Add(this.btnThoat);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(681, 50);
            this.pnlMenu.TabIndex = 67;
            // 
            // lablNameForm
            // 
            this.lablNameForm.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))), ((int) (((byte) (120)))),
                ((int) (((byte) (192)))));
            this.lablNameForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lablNameForm.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lablNameForm.ForeColor = System.Drawing.Color.White;
            this.lablNameForm.Location = new System.Drawing.Point(0, 0);
            this.lablNameForm.Name = "lablNameForm";
            this.lablNameForm.Size = new System.Drawing.Size(628, 50);
            this.lablNameForm.TabIndex = 10;
            this.lablNameForm.Text = ".: CHỤP HÌNH";
            this.lablNameForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))), ((int) (((byte) (120)))),
                ((int) (((byte) (192)))));
            this.btnThoat.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThoat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnThoat.FlatAppearance.BorderSize = 0;
            this.btnThoat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))),
                ((int) (((byte) (37)))), ((int) (((byte) (54)))));
            this.btnThoat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))),
                ((int) (((byte) (120)))), ((int) (((byte) (192)))));
            this.btnThoat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoat.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnThoat.ForeColor = System.Drawing.Color.White;
            this.btnThoat.Image = Properties.Resources.close;
            this.btnThoat.Location = new System.Drawing.Point(628, 0);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(0);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(53, 50);
            this.btnThoat.TabIndex = 15;
            this.btnThoat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (5)))), ((int) (((byte) (92)))),
                ((int) (((byte) (145)))));
            this.pnlInfo.Controls.Add(this.labDiaChiEmailDienThoai);
            this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlInfo.Location = new System.Drawing.Point(0, 462);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(681, 15);
            this.pnlInfo.TabIndex = 83;
            // 
            // labDiaChiEmailDienThoai
            // 
            this.labDiaChiEmailDienThoai.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (5)))),
                ((int) (((byte) (92)))), ((int) (((byte) (145)))));
            this.labDiaChiEmailDienThoai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labDiaChiEmailDienThoai.Font = new System.Drawing.Font("Segoe UI", 8.25F,
                System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.labDiaChiEmailDienThoai.ForeColor = System.Drawing.Color.White;
            this.labDiaChiEmailDienThoai.Location = new System.Drawing.Point(0, 0);
            this.labDiaChiEmailDienThoai.Margin = new System.Windows.Forms.Padding(0);
            this.labDiaChiEmailDienThoai.Name = "labDiaChiEmailDienThoai";
            this.labDiaChiEmailDienThoai.Size = new System.Drawing.Size(681, 15);
            this.labDiaChiEmailDienThoai.TabIndex = 3;
            this.labDiaChiEmailDienThoai.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picOutput
            // 
            this.picOutput.BackColor = System.Drawing.Color.Black;
            this.picOutput.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picOutput.Location = new System.Drawing.Point(18, 6);
            this.picOutput.Name = "picOutput";
            this.picOutput.Size = new System.Drawing.Size(383, 400);
            this.picOutput.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutput.TabIndex = 0;
            this.picOutput.TabStop = false;
            // 
            // btnRHand
            // 
            this.btnRHand.AutoSize = true;
            this.btnRHand.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))), ((int) (((byte) (120)))),
                ((int) (((byte) (192)))));
            this.btnRHand.FlatAppearance.BorderSize = 0;
            this.btnRHand.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (0)))),
                ((int) (((byte) (37)))), ((int) (((byte) (54)))));
            this.btnRHand.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int) (((byte) (5)))),
                ((int) (((byte) (92)))), ((int) (((byte) (145)))));
            this.btnRHand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRHand.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnRHand.ForeColor = System.Drawing.Color.White;
            this.btnRHand.Image = Properties.Resources.right;
            this.btnRHand.Location = new System.Drawing.Point(124, 391);
            this.btnRHand.Name = "btnRHand";
            this.btnRHand.Size = new System.Drawing.Size(141, 54);
            this.btnRHand.TabIndex = 1;
            this.btnRHand.Text = "2.Tay Phải (F3)";
            this.btnRHand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRHand.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnRHand.UseVisualStyleBackColor = false;
            this.btnRHand.Visible = false;
            this.btnRHand.Click += new System.EventHandler(this.btnRHand_Click);
            // 
            // picRHand
            // 
            this.picRHand.BackColor = System.Drawing.Color.Black;
            this.picRHand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picRHand.Location = new System.Drawing.Point(235, 451);
            this.picRHand.Name = "picRHand";
            this.picRHand.Size = new System.Drawing.Size(67, 63);
            this.picRHand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRHand.TabIndex = 0;
            this.picRHand.TabStop = false;
            this.picRHand.Visible = false;
            // 
            // picFace
            // 
            this.picFace.BackColor = System.Drawing.Color.Black;
            this.picFace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picFace.Location = new System.Drawing.Point(435, 9);
            this.picFace.Name = "picFace";
            this.picFace.Size = new System.Drawing.Size(228, 236);
            this.picFace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picFace.TabIndex = 0;
            this.picFace.TabStop = false;
            this.picFace.Click += new System.EventHandler(this.btnFace_Click);
            // 
            // picLHand
            // 
            this.picLHand.BackColor = System.Drawing.Color.Black;
            this.picLHand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picLHand.Location = new System.Drawing.Point(18, 391);
            this.picLHand.Name = "picLHand";
            this.picLHand.Size = new System.Drawing.Size(100, 115);
            this.picLHand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLHand.TabIndex = 0;
            this.picLHand.TabStop = false;
            this.picLHand.Visible = false;
            // 
            // FrmWebCam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 477);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.pnlInfo);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int) (((byte) (51)))), ((int) (((byte) (51)))),
                ((int) (((byte) (51)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.Name = "FrmWebCam";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmWebCam_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlMenu.ResumeLayout(false);
            this.pnlInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.picOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picRHand)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picFace)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picLHand)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnRHand;
        private System.Windows.Forms.Button btnLHand;
        private System.Windows.Forms.Button btnFace;
        private System.Windows.Forms.PictureBox picRHand;
        private System.Windows.Forms.PictureBox picFace;
        private System.Windows.Forms.PictureBox picLHand;
        private System.Windows.Forms.PictureBox picOutput;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Label lablNameForm;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label labDiaChiEmailDienThoai;
    }
}