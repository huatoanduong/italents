﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.DAL.Commons;
using iTalent.DAL.FlashForm;
using iTalent.DAL.Services;
using iTalent.Entities;
using iTalent.Ulits;

namespace iTalent.UI.GUI
{
    public partial class FrmMain : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly Config _config;
        private readonly BaseForm _baseForm;
        private string _dirFile;
        public FICustomer CurrCustomer { get; set; }
        public ICollection<FICustomer> ListCustomers { get; set; }

        public FrmMain()
        {
            InitializeComponent();
            _config = new Config();
            BaseForm.Frm = this;
            BaseForm.DgView = dgvCustomer;
            BaseForm.ListconControls = new List<Control>
            {
                btnAdd,btnConfig,btnDelete,btnEdit,btnExport,btnRefresh,labFromDate,labSearch,labToDate,rbAddress,rbID,rbName,rbParent,rbTel
            };
            _baseForm = new BaseForm();
        }

        private void LoadAll()
        {
            try
            {
                Hide();
                FrmFlash.ShowSplash();
                Application.DoEvents();

                if (_config.LocalSaveImages == "")
                {
                    FrmConfig frmC = new FrmConfig();
                    frmC.ShowDialog();
                }
                _baseForm.CheckDevice(true);
               
                IDbConnectUtil connect = new DbConnectUtil();

                CheckRegisted();

                string sourceDirName = ICurrentSessionService.DesDirNameSource;
                if (!Directory.Exists(sourceDirName))
                    Directory.CreateDirectory(sourceDirName);


                string sourceDirNameTemp = ICurrentSessionService.DesDirNameTemp;
                if (!Directory.Exists(sourceDirNameTemp))
                    Directory.CreateDirectory(sourceDirNameTemp);

                string sourceTemplate = ICurrentSessionService.DesDirNameTemplate;
                if (!Directory.Exists(sourceTemplate))
                    Directory.CreateDirectory(sourceTemplate);

                LoadData();
                WindowState = FormWindowState.Maximized;
                _dirFile = sourceDirName;

                dgvCustomer.AutoGenerateColumns = false;
                FrmFlash.CloseSplash();
                Activate();
                Show();
            }
            catch (Exception ex)
            {
                FrmFlash.CloseSplash();
                Activate();
                Show();
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }

        private void LoadData()
        {
            try
            {
                
                using (ICustomerService service = new CustomerService())
                {
                    dgvCustomer.AutoGenerateColumns = false;
                    //dgvCustomer.DataSource = null;
                    dgvCustomer.SuspendLayout();
                 //   dgvCustomer.Rows.Clear();
                    dgvCustomer.DataSource = service.LoadAll();
                    dgvCustomer.Refresh();

                    dgvCustomer.ResumeLayout();

                }
                
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }

        private void LoadData(ICollection<FICustomer> listCustomers)
        {
            try
            {
                dgvCustomer.AutoGenerateColumns = false;
                dgvCustomer.SuspendLayout();
                dgvCustomer.DataSource = listCustomers;
                dgvCustomer.Refresh();
                dgvCustomer.ResumeLayout();
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }

        private void CheckRegisted()
        {
            try
            {
                using (IAgencyService service = new AgencyService())
                {
                    ICollection<FIAgency> listAgencies = service.LoadAll();
                    if (listAgencies != null && listAgencies.Count > 0)
                    {
                        //FrmLogin frm = new FrmLogin();
                        //frm.ShowDialog(this);
                        //if (!frm.Issuccess)
                        //    Environment.Exit(0);

                        bool kq = service.Login("ADMIN01", "123456");
                        if (!kq)
                        {
                            _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        _baseForm.VietNamMsg = @"Để sử dụng hệ thống bạn phải đăng ký trước!";
                        _baseForm.EnglishMsg = @"You need to register your information before using the system!";
                        _baseForm.ShowMessage(IconMessageBox.Information);

                        FrmSignup frm = new FrmSignup();
                        frm.ShowDialog(this);
                        if (!frm.Issuccess)
                            Environment.Exit(0);
                    }
                }
                   
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning,ex.Message);
                Environment.Exit(0);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmCustomer customer = new FrmCustomer();
            customer.ShowDialog();
            if(customer.Issuccess)
                LoadData();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            Hide();
            FrmConfig frm = new FrmConfig();
            frm.ShowDialog(this);
            Show();

        }

        private void dgvCustomer_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                using (ICustomerService service = new CustomerService())
                {
                    long id = long.Parse(dgvCustomer.SelectedRows[0].Cells[0].Value.ToString());
                    CurrCustomer = service.Find(id);
                    FrmCustomer customer = new FrmCustomer(CurrCustomer);
                    customer.ShowDialog();
                    if(customer.Issuccess)
                        LoadData();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                //Environment.Exit(0);
            }
        }

        private void dgvCustomer_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                using (ICustomerService service = new CustomerService())
                {
                    long id = long.Parse(dgvCustomer.SelectedRows[0].Cells[0].Value.ToString());
                    CurrCustomer = service.Find(id);
                   
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                //Environment.Exit(0);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            FrmCustomer customer = new FrmCustomer(CurrCustomer);
            customer.ShowDialog();
            if (customer.Issuccess)
                LoadData();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                ICurrentSessionService.VietNamLanguage
                    ? "Bạn có muốn xóa khách hàng này không?"
                    : "Do you want delete this customer?",
                ICurrentSessionService.VietNamLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) != DialogResult.Yes) return;

            using (ICustomerService service = new CustomerService())
            {
                long id = long.Parse(dgvCustomer.SelectedRows[0].Cells[0].Value.ToString());
                CurrCustomer = service.Find(id);
                if (CurrCustomer != null)
                {
                    string oldNameCustomer = CurrCustomer.Name;
                    string reportid = CurrCustomer.ReportID;

                    bool kq = service.Delete(CurrCustomer);
                    if (!kq)
                    {
                        _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                    }
                    else
                    {
                        var dirFolderOld = _dirFile + @"\" + reportid + "_" +
                                           ConvertUtil.ConverToUnsign(oldNameCustomer).Replace(" ", "").ToUpper();
                        Directory.Delete(dirFolderOld, true);
                        _baseForm.EnglishMsg = "Delete Successfully!";
                        _baseForm.VietNamMsg = "Xóa Thành Công";
                        _baseForm.ShowMessage(IconMessageBox.Information);
                        LoadData();
                    }
                }
                else
                {
                    _baseForm.EnglishMsg = "Please choice customer to delete!";
                    _baseForm.VietNamMsg = "Chọn một khách hàng để xóa!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            var folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK) return;
            try
            {
                Hide();
                FrmFlash.ShowSplash();
                Application.DoEvents();

                var colremoves = new List<string>
                {
                    "ID",
                    "AgencyID",
                    "FIAgency",
                    "FIFingerAnalysis",
                    "FIFingerRecord",
                    "FIMQChart",
                    "FIPrintRecord"
                };

                using (ICustomerService service = new CustomerService())
                {
                    ListCustomers = service.LoadAll();
                }

                var dtTable = ConvertUtil.ToDataTable(ListCustomers, colremoves);
                var exportExcel = new ExportExcel();
                 
                exportExcel.ExportDanhSachKhachHang(DateTimeUtil.GetCurrentTime().ToString("dd/MM/yyyy"), dtTable,
                    folderBrowserDialog.SelectedPath + "\\Customers_" +
                    DateTimeUtil.GetCurrentTime().ToString("ddMMyyHHmmss") + ".xls");

                _baseForm.ShowMessage(IconMessageBox.Warning,exportExcel.ErrMsg);
                
                FrmFlash.CloseSplash();
                Activate();
                Show();
            }
            catch(Exception ex)
            {
                FrmFlash.CloseSplash();
                Activate();
                Show();

                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                //MessageBox.Show(@"Please uncheck 'Open file after export' if excel is not installed on your computer",
                //    @"Error opening file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSearch2_Click(object sender, EventArgs e)
        {
            using (ICustomerService service = new CustomerService())
            {
                var customers = service.Search(dtpTuNgay.Value,dtpDenNgay.Value);
                LoadData(customers);
            }
        }
    }
}
