﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Utils;
using VAKFomula.Entity;

namespace iTalent.NGF.GUI
{

    public partial class frmConvert : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        private List<FIAgency> FIAgencyList = null;

        public frmConvert()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1, lblOutputFile, lblInputFile, btnToRptx};
            _baseForm = new BaseForm();

            Issuccess = false;
            txtInput.Select();
        }

        private void frmConvert_Load(object sender, EventArgs e)
        {
            LoadAgency();
        }

        private void btnToRptx_Click(object sender, System.EventArgs e)
        {
            ConvertToRptx();
        }

        private void btnToDocx_Click(object sender, EventArgs e)
        {
            ConvertToDocx();
        }

        private void btnExit_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnBrowseInput_Click(object sender, EventArgs e)
        {
            txtInput.Text = getBrowerPath("*.*");
        }

        private void btnBrowseOutput_Click(object sender, EventArgs e)
        {
            txtOutput.Text = getSavepath(txtInput.Text, "*.*");
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            string s = txtInput.Text;
            txtInput.Text = txtOutput.Text;
            txtOutput.Text = s;
        }

        private void LoadAgency()
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFIAgencyService service = new FIAgencyService(unitOfWork);
                FIAgencyList = service.FindAll();
                cbbAgency.DataSource = null;
                cbbAgency.DisplayMember = "Name";
                cbbAgency.ValueMember = "ID";

                if (FIAgencyList != null && FIAgencyList.Count > 0)
                {
                    cbbAgency.SelectedIndex = 0;
                }
            }
        }

        private string getBrowerPath(string filter)
        {
            string path = "";
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.CheckFileExists = true;
            //dialog.Filter = filter;
            dialog.Multiselect = false;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path = dialog.FileName;
            }

            return path;
        }

        private string getSavepath(string inputPath, string filter)
        {
            string path = "";

            SaveFileDialog dialog = new SaveFileDialog();

            try
            {
                dialog.InitialDirectory = Path.GetDirectoryName(inputPath);
                dialog.FileName = Path.ChangeExtension(Path.GetFileName(inputPath), ".rptx");
            }
            catch
            {
            }
            //dialog.Filter = filter;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path = dialog.FileName;
            }

            return path;
        }

        private bool ValidateInput()
        {
            string inputPath = txtInput.Text;
            string outputPath = txtOutput.Text;
            string errMsg;

            if (inputPath.IsBlank())
            {
                errMsg = ICurrentSessionService.VietNamLanguage
                    ? "Vui lòng chọn file nhập"
                    : "Please choose the input file";
                MessageBox.Show(errMsg);
                return false;
            }

            if (outputPath.IsBlank())
            {
                errMsg = ICurrentSessionService.VietNamLanguage
                    ? "Vui lòng chọn file xuất"
                    : "Please choose the output file";
                MessageBox.Show(errMsg);
                return false;
            }

            if (!File.Exists(inputPath))
            {
                errMsg = ICurrentSessionService.VietNamLanguage
                    ? "Không tìm thấy file đưa vào. Vui lòng chọn lại"
                    : "Cannot file input file. Please check again";
                MessageBox.Show(errMsg);
                return false;
            }

            if (File.Exists(outputPath))
            {
                errMsg = ICurrentSessionService.VietNamLanguage
                    ? "Tên file xuất ra đã tồn tại. Vui lòng chọn đường dẫn khác"
                    : "Output file is already exist. Please choose another path";
                MessageBox.Show(errMsg);
                return false;
            }

            if (cbbAgency.SelectedIndex < 0)
            {
                errMsg = ICurrentSessionService.VietNamLanguage
                    ? "Vui lòng chọn một đại lý"
                    : "Please select an agency";
                MessageBox.Show(errMsg);
                return false;
            }

            return true;
        }

        private void ConvertToDocx()
        {
            string inputPath = txtInput.Text;
            string outputPath = txtOutput.Text;
            string errMsg;

            if (!ValidateInput())
            {
                return;
            }

            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFileConverterService service = new FileConverterService(unitOfWork);
                bool res = service.FromRptxToDocx(inputPath, outputPath);
                if (!res)
                {
                    MessageBox.Show(service.ErrMsg);
                }
                else
                {
                    errMsg = ICurrentSessionService.VietNamLanguage
                        ? "Chuyển đổi thành công!"
                        : "Successfully convert!";
                    MessageBox.Show(errMsg);
                }
            }
        }

        private void ConvertToRptx()
        {
            string inputPath = txtInput.Text;
            string outputPath = txtOutput.Text;
            string errMsg;

            if (!ValidateInput())
            {
                return;
            }
            int agencyId = (int) cbbAgency.SelectedValue;
            string templateName = radReport1.Checked
                ? @"Adult"
                : @"Child";

            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFileConverterService service = new FileConverterService(unitOfWork);
                bool res = service.FromDocxToRptx(agencyId, templateName, inputPath, outputPath);
                if (!res)
                {
                    MessageBox.Show(service.ErrMsg);
                }
                else
                {
                    errMsg = ICurrentSessionService.VietNamLanguage
                        ? "Chuyển đổi thành công!"
                        : "Successfully convert!";
                    MessageBox.Show(errMsg);
                }
            }

        }

    }
}