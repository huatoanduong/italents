﻿namespace iTalent.NGF.GUI
{
    partial class FrmCountFinger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCountFinger));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnExit = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.ptbRight = new System.Windows.Forms.PictureBox();
            this.ptbCenter = new System.Windows.Forms.PictureBox();
            this.ptbLeft = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnCenter = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbLeft)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.btnExit});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.ptbRight);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.ptbCenter);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.ptbLeft);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.panel1);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(798, 335);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 153;
            this.c4FunHeaderGroup1.Tag = "";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Count Fingerprint";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // btnExit
            // 
            this.btnExit.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnExit.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // ptbRight
            // 
            this.ptbRight.BackColor = System.Drawing.Color.White;
            this.ptbRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbRight.Dock = System.Windows.Forms.DockStyle.Left;
            this.ptbRight.Location = new System.Drawing.Point(530, 49);
            this.ptbRight.Name = "ptbRight";
            this.ptbRight.Size = new System.Drawing.Size(265, 247);
            this.ptbRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbRight.TabIndex = 193;
            this.ptbRight.TabStop = false;
            this.ptbRight.Click += new System.EventHandler(this.ptbRight_Click);
            // 
            // ptbCenter
            // 
            this.ptbCenter.BackColor = System.Drawing.Color.White;
            this.ptbCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.ptbCenter.Location = new System.Drawing.Point(265, 49);
            this.ptbCenter.Name = "ptbCenter";
            this.ptbCenter.Size = new System.Drawing.Size(265, 247);
            this.ptbCenter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbCenter.TabIndex = 192;
            this.ptbCenter.TabStop = false;
            this.ptbCenter.Click += new System.EventHandler(this.ptbCenter_Click);
            // 
            // ptbLeft
            // 
            this.ptbLeft.BackColor = System.Drawing.Color.White;
            this.ptbLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.ptbLeft.Location = new System.Drawing.Point(0, 49);
            this.ptbLeft.Name = "ptbLeft";
            this.ptbLeft.Size = new System.Drawing.Size(265, 247);
            this.ptbLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbLeft.TabIndex = 191;
            this.ptbLeft.TabStop = false;
            this.ptbLeft.Click += new System.EventHandler(this.ptbLeft_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRight);
            this.panel1.Controls.Add(this.btnCenter);
            this.panel1.Controls.Add(this.btnLeft);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(796, 49);
            this.panel1.TabIndex = 190;
            // 
            // btnRight
            // 
            this.btnRight.BackColor = System.Drawing.Color.Silver;
            this.btnRight.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRight.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRight.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRight.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRight.Location = new System.Drawing.Point(530, 0);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(265, 49);
            this.btnRight.TabIndex = 189;
            this.btnRight.Tag = "";
            this.btnRight.Text = "Right";
            this.btnRight.UseVisualStyleBackColor = false;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnCenter
            // 
            this.btnCenter.BackColor = System.Drawing.Color.Silver;
            this.btnCenter.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCenter.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCenter.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnCenter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnCenter.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCenter.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCenter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCenter.Location = new System.Drawing.Point(265, 0);
            this.btnCenter.Name = "btnCenter";
            this.btnCenter.Size = new System.Drawing.Size(265, 49);
            this.btnCenter.TabIndex = 188;
            this.btnCenter.Tag = "";
            this.btnCenter.Text = "Center";
            this.btnCenter.UseVisualStyleBackColor = false;
            this.btnCenter.Click += new System.EventHandler(this.btnCenter_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.BackColor = System.Drawing.Color.Silver;
            this.btnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLeft.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLeft.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeft.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeft.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLeft.Location = new System.Drawing.Point(0, 0);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(265, 49);
            this.btnLeft.TabIndex = 187;
            this.btnLeft.Tag = "";
            this.btnLeft.Text = "Left";
            this.btnLeft.UseVisualStyleBackColor = false;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // FrmCountFinger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 335);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCountFinger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmCountFinger_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbLeft)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnExit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.PictureBox ptbRight;
        private System.Windows.Forms.PictureBox ptbCenter;
        private System.Windows.Forms.PictureBox ptbLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnCenter;
    }
}