﻿namespace VAKFomula.Model
{
    public class FingerRecordModel
    {
        public int L1L { get; set; }
        public int L1R { get; set; }
        public int Max_L1L_L1R { get { return FingerRCCountCompare(L1L, L1R); } }

        public int L2L { get; set; }
        public int L2R { get; set; }
        public int Max_L2L_L2R { get { return FingerRCCountCompare(L2L, L2R); } }

        public int L3L { get; set; }
        public int L3R { get; set; }
        public int Max_L3L_L3R { get { return FingerRCCountCompare(L3L, L3R); } }

        public int L4L { get; set; }
        public int L4R { get; set; }
        public int Max_L4L_L4R { get { return FingerRCCountCompare(L4L, L4R); } }

        public int L5L { get; set; }
        public int L5R { get; set; }
        public int Max_L5L_L5R { get { return FingerRCCountCompare(L5L, L5R); } }

        public int R1L { get; set; }
        public int R1R { get; set; }
        public int Max_R1L_R1R { get { return FingerRCCountCompare(R1L, R1R); } }

        public int R2L { get; set; }
        public int R2R { get; set; }
        public int Max_R2L_R2R { get { return FingerRCCountCompare(R2L, R2R); } }

        public int R3L { get; set; }
        public int R3R { get; set; }
        public int Max_R3L_R3R { get { return FingerRCCountCompare(R3L, R3R); } }

        public int R4L { get; set; }
        public int R4R { get; set; }
        public int Max_R4L_R4R { get { return FingerRCCountCompare(R4L, R4R); } }

        public int R5L { get; set; }
        public int R5R { get; set; }
        public int Max_R5L_R5R { get { return FingerRCCountCompare(R5L, R5R); } }

        //R_ATD
        public double ATDR { get; set; }
        //L_ATD
        public double ATDL { get; set; }
        //
        public double SUML { get { return SUM_L(); } }
        public double SUMR { get { return SUM_R(); } }

        public int TFRC { get { return SUM_L() + SUM_R(); } }

        public string L1T { get; set; }
        public string L2T { get; set; }
        public string L3T { get; set; }
        public string L4T { get; set; }
        public string L5T { get; set; }
        public string R1T { get; set; }
        public string R2T { get; set; }
        public string R3T { get; set; }
        public string R4T { get; set; }
        public string R5T { get; set; }

        private int FingerRCCountCompare(int L, int R)
        {
            if (L < R)
            {
                return R;
            }
            if (L > R)
            {
                return L;
            }
            return L;
        }

        private int SUM_L()
        {
            return Max_L1L_L1R + Max_L2L_L2R + Max_L3L_L3R + Max_L4L_L4R + Max_L5L_L5R;
        }
        private int SUM_R()
        {
            return Max_R1L_R1R + Max_R2L_R2R + Max_R3L_R3R + Max_R4L_R4R + Max_R5L_R5R;
        }
    }
}
