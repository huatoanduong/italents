﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace iTalent.UI.GUI
{
    public class iCam
    {
        // Fields
        private const short SWP_NOMOVE = 2;
        private const short SWP_NOZORDER = 4;
        private const long WM_CAP_COPY = 0x41eL;
        private const int WM_CAP_DRIVER_CONNECT = 0x40a;
        private const int WM_CAP_DRIVER_DISCONNECT = 0x40b;
        private const long WM_CAP_FILE_SET_CAPTURE_FILEA = 0x414L;
        private const long WM_CAP_GET_FRAME = 0x43cL;
        private const long WM_CAP_SEQUENCE = 0x43eL;
        private const int WM_CAP_SET_PREVIEW = 0x432;
        private const int WM_CAP_SET_PREVIEWRATE = 0x434;
        private const long WM_CAP_SET_SEQUENCE_SETUP = 0x440L;
        private const int WM_CAP_SET_VIDEOFORMAT = 0x42d;
        private const long WM_CAP_START = 0x400L;
        private const long WM_CAP_STOP = 0x444L;
        private const short WM_USER = 0x400;
        private const int WS_CHILD = 0x40000000;
        private const int WS_VISIBLE = 0x10000000;
        private int CamFrameRate = 15;
        private int OutputHeight = 240;
        private int OutputWidth = 360;
        private int hHwnd;
        private string iDevice;
        public bool iRunning;
        //private int lwndC;

        // Methods
        [DllImport("GDI32.DLL", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc,
            int nXSrc, int nYSrc, int dwRop);

        [DllImport("avicap32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int capCreateCaptureWindowA(
            [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszWindowName, int dwStyle, int x, int y, int nWidth,
            short nHeight, int hWndParent, int nID);

        [DllImport("avicap32.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern bool capGetDriverDescriptionA(short wDriver,
            [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszName, int cbName,
            [MarshalAs(UnmanagedType.VBByRefStr)] ref string lpszVer, int cbVer);

        public bool closeCam()
        {
            bool flag = false;
            if (iRunning)
            {
                string lParam = Conversions.ToString(0);
                flag = SendMessage(hHwnd, 0x40b, 0, ref lParam) > 0;
                iRunning = false;
            }
            return flag;
        }

        public Bitmap copyFrame(PictureBox src, RectangleF rect)
        {
            Bitmap bitmap = null;
            if (iRunning)
            {
                Graphics g = src.CreateGraphics();
                Bitmap image = new Bitmap(src.Width, src.Height, g);
                Graphics graphics = Graphics.FromImage(image);
                IntPtr hdc = g.GetHdc();
                IntPtr hdcDest = graphics.GetHdc();
                BitBlt(hdcDest, 0, 0, (int) Math.Round(rect.Width), (int) Math.Round(rect.Height), hdc,
                    (int) Math.Round(rect.X), (int) Math.Round(rect.Y), 0xcc0020);
                bitmap = (Bitmap) image.Clone();
                g.ReleaseHdc(hdc);
                graphics.ReleaseHdc(hdcDest);
                g.Dispose();
                graphics.Dispose();
                return bitmap;
            }
            MessageBox.Show("Camera Is Not Running!");
            return bitmap;
        }

        public int FPS()
        {
            return (int) Math.Round(1000.0/CamFrameRate);
        }

        public void initCam(int parentH)
        {
            if (iRunning)
            {
                MessageBox.Show("Camera Is Already Running");
            }
            else
            {
                hHwnd = capCreateCaptureWindowA(ref iDevice, 0x50000000, 0, 0, OutputWidth, (short) OutputHeight,
                    parentH, 0);
                if (!setCam())
                {
                    MessageBox.Show("Error setting Up Camera");
                }
            }
        }

        public void resetCam()
        {
            if (iRunning)
            {
                closeCam();
                Application.DoEvents();
                if (!setCam())
                {
                    MessageBox.Show("Errror Setting/Re-Setting Camera");
                }
            }
        }

        [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true,
            ExactSpelling = true)]
        private static extern int SendMessage(int hwnd, int wMsg, short wParam,
            [MarshalAs(UnmanagedType.VBByRefStr)] ref string lParam);

        private bool setCam()
        {
            string lParam = Conversions.ToString(0);
            if (SendMessage(hHwnd, 0x40a, Conversions.ToShort(iDevice), ref lParam) == 1)
            {
                SendMessage(hHwnd, 0x434, (short) CamFrameRate, ref lParam);
                SendMessage(hHwnd, 0x432, 1, ref lParam);
                iRunning = true;
                return true;
            }
            iRunning = false;
            return false;
        }

        public void setFrameRate(long iRate)
        {
            CamFrameRate = (int) Math.Round(1000.0/iRate);
            resetCam();
        }
    }
}