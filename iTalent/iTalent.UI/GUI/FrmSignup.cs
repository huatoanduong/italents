﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.DAL.Commons;
using iTalent.DAL.Services;
using iTalent.Entities;

namespace iTalent.UI.GUI
{
    public partial class FrmSignup : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        //private string _seckey;
        //private string _serial;
        //private string _productkey;

        public FrmSignup()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1,labAddress,labCellPhone,labCity,labEmail,labID,labName,labPhone,labPassword,btnLuu};
            _baseForm = new BaseForm();
            _baseForm.CheckDevice(true);
            txtTen.Select();
            //MessageBox.Show(_baseForm.ProductKey);

        }
        //private bool CheckDevice()
        //{
        //    //if (!RuntimePolicyHelper.LegacyV2RuntimeEnabledSuccessfully) return false;
        //    Fingerprint.VietNamLanguage = 
        //    using (var fingerprint = new Fingerprint(false))
        //    {
        //        bool kq = fingerprint.CheckRegisted;
        //        _seckey = fingerprint.Seckey;
        //        _serial = fingerprint.Serial;
        //        _productkey = fingerprint.ProductKey;
        //        if (!fingerprint.Isconnect)
        //            MessageBox.Show(fingerprint.Message);

        //        return fingerprint.Isconnect;
        //    }
        //}

        private void Register()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtTen.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập tên!";
                    _baseForm.VietNamMsg = @"Please input your name!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                if (string.IsNullOrWhiteSpace(txtEmail.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập email!";
                    _baseForm.VietNamMsg = @"Please input your email!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                using (IAgencyService service = new AgencyService())
                {
                    FIAgency objAgency = service.CreateEntity();
                    objAgency.Name = txtTen.Text;
                    objAgency.Username = "AC50015";
                    objAgency.Password = "AC50015"; 
                    objAgency.Email = txtEmail.Text;
                    objAgency.PhoneNo = txtDienThoai.Text;
                    objAgency.MobileNo = txtDiDong.Text;
                    objAgency.City = txtThanhPho.Text;

                    objAgency.Address1 = txtDiaChi.Text;
                    objAgency.SaveImages = ICurrentSessionService.DirSaveImages;
                    objAgency.SecKey = _baseForm.Seckey;
                    objAgency.ProductKey = _baseForm.ProductKey;
                    objAgency.SerailKey = _baseForm.SerailKey;

                    Issuccess = service.Add(objAgency);
                    if (Issuccess)
                    {
                        Issuccess = service.Login("AC50015", "AC50015");
                        if (!Issuccess)
                        {
                            _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                        }
                        else
                        {
                            _baseForm.VietNamMsg = ".: Đăng ký thành công!\nBạn hãy chạy lại chương trình.";
                            _baseForm.EnglishMsg = ".: Register successfully!\nPlease open again.";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        Issuccess = false;
                        _baseForm.ShowMessage(IconMessageBox.Warning, service.ErrMsg);
                    }
                    base.Dispose();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                Environment.Exit(0);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            base.Dispose();
        }

        private void buttonSpecHeaderGroup1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            Register();
        }
    }
}
