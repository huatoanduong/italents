﻿namespace iTalent.UI.GUI
{
    partial class FrmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConfig));
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.txtSaveImages = new System.Windows.Forms.TextBox();
            this.labSaveImage = new System.Windows.Forms.Label();
            this.buttonSpecHeaderGroup1 = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnChoice = new System.Windows.Forms.Button();
            this.labLang = new System.Windows.Forms.Label();
            this.raEnglish = new System.Windows.Forms.RadioButton();
            this.raVietNam = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            // 
            // txtSaveImages
            // 
            this.txtSaveImages.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSaveImages.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtSaveImages.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaveImages.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtSaveImages.Location = new System.Drawing.Point(153, 53);
            this.txtSaveImages.Name = "txtSaveImages";
            this.txtSaveImages.Size = new System.Drawing.Size(406, 29);
            this.txtSaveImages.TabIndex = 187;
            // 
            // labSaveImage
            // 
            this.labSaveImage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labSaveImage.BackColor = System.Drawing.Color.Transparent;
            this.labSaveImage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSaveImage.Location = new System.Drawing.Point(18, 57);
            this.labSaveImage.Name = "labSaveImage";
            this.labSaveImage.Size = new System.Drawing.Size(129, 21);
            this.labSaveImage.TabIndex = 194;
            this.labSaveImage.Tag = "Nơi Lưu Vân Tay :";
            this.labSaveImage.Text = "Save Images :";
            this.labSaveImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonSpecHeaderGroup1
            // 
            this.buttonSpecHeaderGroup1.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.buttonSpecHeaderGroup1.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.buttonSpecHeaderGroup1.Click += new System.EventHandler(this.buttonSpecHeaderGroup1_Click);
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.buttonSpecHeaderGroup1});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnChoice);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labLang);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.raEnglish);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.raVietNam);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtSaveImages);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labSaveImage);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnSave);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(611, 173);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 152;
            this.c4FunHeaderGroup1.Tag = "Tùy Chọn";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Option";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.UI.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // btnChoice
            // 
            this.btnChoice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnChoice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnChoice.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnChoice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnChoice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnChoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChoice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChoice.ForeColor = System.Drawing.Color.White;
            this.btnChoice.Image = global::iTalent.UI.Properties.Resources.BrowseFolder;
            this.btnChoice.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnChoice.Location = new System.Drawing.Point(562, 46);
            this.btnChoice.Margin = new System.Windows.Forms.Padding(0);
            this.btnChoice.Name = "btnChoice";
            this.btnChoice.Size = new System.Drawing.Size(41, 36);
            this.btnChoice.TabIndex = 198;
            this.btnChoice.Tag = "";
            this.btnChoice.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnChoice.UseVisualStyleBackColor = false;
            this.btnChoice.Click += new System.EventHandler(this.btnChoice_Click);
            // 
            // labLang
            // 
            this.labLang.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labLang.BackColor = System.Drawing.Color.Transparent;
            this.labLang.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labLang.Location = new System.Drawing.Point(31, 20);
            this.labLang.Name = "labLang";
            this.labLang.Size = new System.Drawing.Size(116, 21);
            this.labLang.TabIndex = 197;
            this.labLang.Tag = "Ngôn Ngữ :";
            this.labLang.Text = "Language :";
            this.labLang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // raEnglish
            // 
            this.raEnglish.BackColor = System.Drawing.Color.Transparent;
            this.raEnglish.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.raEnglish.Image = global::iTalent.UI.Properties.Resources.UnitedKingdom;
            this.raEnglish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.raEnglish.Location = new System.Drawing.Point(327, 13);
            this.raEnglish.Name = "raEnglish";
            this.raEnglish.Size = new System.Drawing.Size(156, 34);
            this.raEnglish.TabIndex = 196;
            this.raEnglish.TabStop = true;
            this.raEnglish.Tag = "Tiếng Anh";
            this.raEnglish.Text = "English";
            this.raEnglish.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.raEnglish.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.raEnglish.UseVisualStyleBackColor = false;
            this.raEnglish.Click += new System.EventHandler(this.raEnglish_Click);
            // 
            // raVietNam
            // 
            this.raVietNam.BackColor = System.Drawing.Color.Transparent;
            this.raVietNam.Checked = true;
            this.raVietNam.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.raVietNam.Image = global::iTalent.UI.Properties.Resources.vietnam;
            this.raVietNam.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.raVietNam.Location = new System.Drawing.Point(165, 13);
            this.raVietNam.Name = "raVietNam";
            this.raVietNam.Size = new System.Drawing.Size(156, 34);
            this.raVietNam.TabIndex = 195;
            this.raVietNam.TabStop = true;
            this.raVietNam.Tag = "Tiếng Việt";
            this.raVietNam.Text = "VietNam";
            this.raVietNam.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.raVietNam.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.raVietNam.UseVisualStyleBackColor = false;
            this.raVietNam.CheckedChanged += new System.EventHandler(this.raVietNam_CheckedChanged);
            this.raVietNam.Click += new System.EventHandler(this.raVietNam_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Image = global::iTalent.UI.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(464, 85);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(95, 41);
            this.btnSave.TabIndex = 186;
            this.btnSave.Tag = "Lưu";
            this.btnSave.Text = "Save";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 173);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmConfig";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private System.Windows.Forms.TextBox txtSaveImages;
        private System.Windows.Forms.Label labSaveImage;
        private System.Windows.Forms.Button btnSave;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup buttonSpecHeaderGroup1;
        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private System.Windows.Forms.RadioButton raEnglish;
        private System.Windows.Forms.RadioButton raVietNam;
        private System.Windows.Forms.Label labLang;
        private System.Windows.Forms.Button btnChoice;
    }
}