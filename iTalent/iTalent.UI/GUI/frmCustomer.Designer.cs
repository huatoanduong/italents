﻿namespace iTalent.UI.GUI
{
    partial class FrmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomer));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.prbRightHand = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ptbR4L = new System.Windows.Forms.PictureBox();
            this.ptbR5R = new System.Windows.Forms.PictureBox();
            this.ptbR4R = new System.Windows.Forms.PictureBox();
            this.ptbR4F = new System.Windows.Forms.PictureBox();
            this.ptbR5F = new System.Windows.Forms.PictureBox();
            this.ptbR5L = new System.Windows.Forms.PictureBox();
            this.ptbR2L = new System.Windows.Forms.PictureBox();
            this.ptbR3R = new System.Windows.Forms.PictureBox();
            this.ptbR1F = new System.Windows.Forms.PictureBox();
            this.ptbR2R = new System.Windows.Forms.PictureBox();
            this.ptbR2F = new System.Windows.Forms.PictureBox();
            this.ptbR1R = new System.Windows.Forms.PictureBox();
            this.ptbR3F = new System.Windows.Forms.PictureBox();
            this.ptbR3L = new System.Windows.Forms.PictureBox();
            this.ptbR1L = new System.Windows.Forms.PictureBox();
            this.grbLeftHand = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labIndex = new System.Windows.Forms.Label();
            this.labMiddle = new System.Windows.Forms.Label();
            this.labRing = new System.Windows.Forms.Label();
            this.labLittle = new System.Windows.Forms.Label();
            this.ptbL5L = new System.Windows.Forms.PictureBox();
            this.ptbL4R = new System.Windows.Forms.PictureBox();
            this.ptbL5R = new System.Windows.Forms.PictureBox();
            this.ptbL5F = new System.Windows.Forms.PictureBox();
            this.ptbL4F = new System.Windows.Forms.PictureBox();
            this.ptbL4L = new System.Windows.Forms.PictureBox();
            this.ptbL2L = new System.Windows.Forms.PictureBox();
            this.ptbL3R = new System.Windows.Forms.PictureBox();
            this.ptbL3F = new System.Windows.Forms.PictureBox();
            this.ptbL3L = new System.Windows.Forms.PictureBox();
            this.ptbL1F = new System.Windows.Forms.PictureBox();
            this.ptbL1L = new System.Windows.Forms.PictureBox();
            this.ptbL1R = new System.Windows.Forms.PictureBox();
            this.ptbL2F = new System.Windows.Forms.PictureBox();
            this.ptbL2R = new System.Windows.Forms.PictureBox();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.btnCapture = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labReportId = new System.Windows.Forms.Label();
            this.labMa = new System.Windows.Forms.Label();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnScaner = new System.Windows.Forms.Button();
            this.labName = new System.Windows.Forms.Label();
            this.labParent = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.labCity = new System.Windows.Forms.Label();
            this.txtTenChaMe = new System.Windows.Forms.TextBox();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.labAddress = new System.Windows.Forms.Label();
            this.labMobile = new System.Windows.Forms.Label();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.labSex = new System.Windows.Forms.Label();
            this.labBirthday = new System.Windows.Forms.Label();
            this.labNote = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.labPhone = new System.Windows.Forms.Label();
            this.labEmail = new System.Windows.Forms.Label();
            this.cbxGioiTinh = new System.Windows.Forms.ComboBox();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            this.prbRightHand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR4L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR5R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR4R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR4F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR5F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR5L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR2L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR3R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR1F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR2R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR2F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR1R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR3F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR3L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR1L)).BeginInit();
            this.grbLeftHand.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL5L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL4R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL5R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL5F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL4F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL4L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL2L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL3R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL3F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL3L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL1F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL1L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL1R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL2F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL2R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.btnClose});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.prbRightHand);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.grbLeftHand);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.rdFemale);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.rdMale);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnCapture);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox2);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labReportId);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labMa);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnLuu);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnScaner);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labName);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labParent);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiaChi);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiDong);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labCity);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTenChaMe);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.dtpNgaySinh);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtGhiChu);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labAddress);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labMobile);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtThanhPho);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTen);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labSex);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labBirthday);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labNote);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDienThoai);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labPhone);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.cbxGioiTinh);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(916, 689);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 150;
            this.c4FunHeaderGroup1.Tag = "Thông Tin Khách Hàng";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Customer Information";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.UI.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // prbRightHand
            // 
            this.prbRightHand.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.prbRightHand.BackColor = System.Drawing.Color.Transparent;
            this.prbRightHand.Controls.Add(this.label1);
            this.prbRightHand.Controls.Add(this.label2);
            this.prbRightHand.Controls.Add(this.label3);
            this.prbRightHand.Controls.Add(this.label5);
            this.prbRightHand.Controls.Add(this.label6);
            this.prbRightHand.Controls.Add(this.ptbR4L);
            this.prbRightHand.Controls.Add(this.ptbR5R);
            this.prbRightHand.Controls.Add(this.ptbR4R);
            this.prbRightHand.Controls.Add(this.ptbR4F);
            this.prbRightHand.Controls.Add(this.ptbR5F);
            this.prbRightHand.Controls.Add(this.ptbR5L);
            this.prbRightHand.Controls.Add(this.ptbR2L);
            this.prbRightHand.Controls.Add(this.ptbR3R);
            this.prbRightHand.Controls.Add(this.ptbR1F);
            this.prbRightHand.Controls.Add(this.ptbR2R);
            this.prbRightHand.Controls.Add(this.ptbR2F);
            this.prbRightHand.Controls.Add(this.ptbR1R);
            this.prbRightHand.Controls.Add(this.ptbR3F);
            this.prbRightHand.Controls.Add(this.ptbR3L);
            this.prbRightHand.Controls.Add(this.ptbR1L);
            this.prbRightHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.prbRightHand.Location = new System.Drawing.Point(468, 300);
            this.prbRightHand.Name = "prbRightHand";
            this.prbRightHand.Size = new System.Drawing.Size(443, 364);
            this.prbRightHand.TabIndex = 229;
            this.prbRightHand.TabStop = false;
            this.prbRightHand.Text = "RIGHT HAND";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(9, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 21);
            this.label1.TabIndex = 228;
            this.label1.Tag = "Ghi Chú :";
            this.label1.Text = "Thumb";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(95, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 21);
            this.label2.TabIndex = 227;
            this.label2.Tag = "Ghi Chú :";
            this.label2.Text = "Index";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(181, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 21);
            this.label3.TabIndex = 226;
            this.label3.Tag = "Ghi Chú :";
            this.label3.Text = "Middle";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(267, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 21);
            this.label5.TabIndex = 225;
            this.label5.Tag = "Ghi Chú :";
            this.label5.Text = "Ring";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(353, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 21);
            this.label6.TabIndex = 220;
            this.label6.Tag = "Ghi Chú :";
            this.label6.Text = "Little";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ptbR4L
            // 
            this.ptbR4L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR4L.Location = new System.Drawing.Point(267, 160);
            this.ptbR4L.Name = "ptbR4L";
            this.ptbR4L.Size = new System.Drawing.Size(80, 90);
            this.ptbR4L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR4L.TabIndex = 221;
            this.ptbR4L.TabStop = false;
            this.ptbR4L.Tag = "R4L";
            // 
            // ptbR5R
            // 
            this.ptbR5R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR5R.Location = new System.Drawing.Point(353, 256);
            this.ptbR5R.Name = "ptbR5R";
            this.ptbR5R.Size = new System.Drawing.Size(80, 90);
            this.ptbR5R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR5R.TabIndex = 224;
            this.ptbR5R.TabStop = false;
            this.ptbR5R.Tag = "R5R";
            // 
            // ptbR4R
            // 
            this.ptbR4R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR4R.Location = new System.Drawing.Point(267, 256);
            this.ptbR4R.Name = "ptbR4R";
            this.ptbR4R.Size = new System.Drawing.Size(80, 90);
            this.ptbR4R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR4R.TabIndex = 223;
            this.ptbR4R.TabStop = false;
            this.ptbR4R.Tag = "R4R";
            // 
            // ptbR4F
            // 
            this.ptbR4F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR4F.Location = new System.Drawing.Point(267, 64);
            this.ptbR4F.Name = "ptbR4F";
            this.ptbR4F.Size = new System.Drawing.Size(80, 90);
            this.ptbR4F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR4F.TabIndex = 219;
            this.ptbR4F.TabStop = false;
            this.ptbR4F.Tag = "R4F";
            // 
            // ptbR5F
            // 
            this.ptbR5F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR5F.Location = new System.Drawing.Point(353, 64);
            this.ptbR5F.Name = "ptbR5F";
            this.ptbR5F.Size = new System.Drawing.Size(80, 90);
            this.ptbR5F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR5F.TabIndex = 220;
            this.ptbR5F.TabStop = false;
            this.ptbR5F.Tag = "R5F";
            // 
            // ptbR5L
            // 
            this.ptbR5L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR5L.Location = new System.Drawing.Point(353, 160);
            this.ptbR5L.Name = "ptbR5L";
            this.ptbR5L.Size = new System.Drawing.Size(80, 90);
            this.ptbR5L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR5L.TabIndex = 222;
            this.ptbR5L.TabStop = false;
            this.ptbR5L.Tag = "R5L";
            // 
            // ptbR2L
            // 
            this.ptbR2L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR2L.Location = new System.Drawing.Point(95, 160);
            this.ptbR2L.Name = "ptbR2L";
            this.ptbR2L.Size = new System.Drawing.Size(80, 90);
            this.ptbR2L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR2L.TabIndex = 214;
            this.ptbR2L.TabStop = false;
            this.ptbR2L.Tag = "R2L";
            // 
            // ptbR3R
            // 
            this.ptbR3R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR3R.Location = new System.Drawing.Point(181, 256);
            this.ptbR3R.Name = "ptbR3R";
            this.ptbR3R.Size = new System.Drawing.Size(80, 90);
            this.ptbR3R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR3R.TabIndex = 218;
            this.ptbR3R.TabStop = false;
            this.ptbR3R.Tag = "R3R";
            // 
            // ptbR1F
            // 
            this.ptbR1F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR1F.Location = new System.Drawing.Point(9, 64);
            this.ptbR1F.Name = "ptbR1F";
            this.ptbR1F.Size = new System.Drawing.Size(80, 90);
            this.ptbR1F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR1F.TabIndex = 210;
            this.ptbR1F.TabStop = false;
            this.ptbR1F.Tag = "R1F";
            // 
            // ptbR2R
            // 
            this.ptbR2R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR2R.Location = new System.Drawing.Point(95, 256);
            this.ptbR2R.Name = "ptbR2R";
            this.ptbR2R.Size = new System.Drawing.Size(80, 90);
            this.ptbR2R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR2R.TabIndex = 217;
            this.ptbR2R.TabStop = false;
            this.ptbR2R.Tag = "R2R";
            // 
            // ptbR2F
            // 
            this.ptbR2F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR2F.Location = new System.Drawing.Point(95, 64);
            this.ptbR2F.Name = "ptbR2F";
            this.ptbR2F.Size = new System.Drawing.Size(80, 90);
            this.ptbR2F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR2F.TabIndex = 211;
            this.ptbR2F.TabStop = false;
            this.ptbR2F.Tag = "R2F";
            // 
            // ptbR1R
            // 
            this.ptbR1R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR1R.Location = new System.Drawing.Point(9, 256);
            this.ptbR1R.Name = "ptbR1R";
            this.ptbR1R.Size = new System.Drawing.Size(80, 90);
            this.ptbR1R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR1R.TabIndex = 216;
            this.ptbR1R.TabStop = false;
            this.ptbR1R.Tag = "R1R";
            // 
            // ptbR3F
            // 
            this.ptbR3F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR3F.Location = new System.Drawing.Point(181, 64);
            this.ptbR3F.Name = "ptbR3F";
            this.ptbR3F.Size = new System.Drawing.Size(80, 90);
            this.ptbR3F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR3F.TabIndex = 212;
            this.ptbR3F.TabStop = false;
            this.ptbR3F.Tag = "R3F";
            // 
            // ptbR3L
            // 
            this.ptbR3L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR3L.Location = new System.Drawing.Point(181, 160);
            this.ptbR3L.Name = "ptbR3L";
            this.ptbR3L.Size = new System.Drawing.Size(80, 90);
            this.ptbR3L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR3L.TabIndex = 215;
            this.ptbR3L.TabStop = false;
            this.ptbR3L.Tag = "R3L";
            // 
            // ptbR1L
            // 
            this.ptbR1L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbR1L.Location = new System.Drawing.Point(9, 160);
            this.ptbR1L.Name = "ptbR1L";
            this.ptbR1L.Size = new System.Drawing.Size(80, 90);
            this.ptbR1L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbR1L.TabIndex = 213;
            this.ptbR1L.TabStop = false;
            this.ptbR1L.Tag = "R1L";
            // 
            // grbLeftHand
            // 
            this.grbLeftHand.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.grbLeftHand.BackColor = System.Drawing.Color.Transparent;
            this.grbLeftHand.Controls.Add(this.label4);
            this.grbLeftHand.Controls.Add(this.labIndex);
            this.grbLeftHand.Controls.Add(this.labMiddle);
            this.grbLeftHand.Controls.Add(this.labRing);
            this.grbLeftHand.Controls.Add(this.labLittle);
            this.grbLeftHand.Controls.Add(this.ptbL5L);
            this.grbLeftHand.Controls.Add(this.ptbL4R);
            this.grbLeftHand.Controls.Add(this.ptbL5R);
            this.grbLeftHand.Controls.Add(this.ptbL5F);
            this.grbLeftHand.Controls.Add(this.ptbL4F);
            this.grbLeftHand.Controls.Add(this.ptbL4L);
            this.grbLeftHand.Controls.Add(this.ptbL2L);
            this.grbLeftHand.Controls.Add(this.ptbL3R);
            this.grbLeftHand.Controls.Add(this.ptbL3F);
            this.grbLeftHand.Controls.Add(this.ptbL3L);
            this.grbLeftHand.Controls.Add(this.ptbL1F);
            this.grbLeftHand.Controls.Add(this.ptbL1L);
            this.grbLeftHand.Controls.Add(this.ptbL1R);
            this.grbLeftHand.Controls.Add(this.ptbL2F);
            this.grbLeftHand.Controls.Add(this.ptbL2R);
            this.grbLeftHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.grbLeftHand.Location = new System.Drawing.Point(4, 300);
            this.grbLeftHand.Name = "grbLeftHand";
            this.grbLeftHand.Size = new System.Drawing.Size(443, 364);
            this.grbLeftHand.TabIndex = 219;
            this.grbLeftHand.TabStop = false;
            this.grbLeftHand.Text = "LEFT HAND";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(353, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 21);
            this.label4.TabIndex = 228;
            this.label4.Tag = "Ghi Chú :";
            this.label4.Text = "Thumb";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labIndex
            // 
            this.labIndex.BackColor = System.Drawing.Color.Transparent;
            this.labIndex.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labIndex.ForeColor = System.Drawing.Color.Navy;
            this.labIndex.Location = new System.Drawing.Point(267, 40);
            this.labIndex.Name = "labIndex";
            this.labIndex.Size = new System.Drawing.Size(80, 21);
            this.labIndex.TabIndex = 227;
            this.labIndex.Tag = "Ghi Chú :";
            this.labIndex.Text = "Index";
            this.labIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labMiddle
            // 
            this.labMiddle.BackColor = System.Drawing.Color.Transparent;
            this.labMiddle.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labMiddle.ForeColor = System.Drawing.Color.Navy;
            this.labMiddle.Location = new System.Drawing.Point(181, 40);
            this.labMiddle.Name = "labMiddle";
            this.labMiddle.Size = new System.Drawing.Size(80, 21);
            this.labMiddle.TabIndex = 226;
            this.labMiddle.Tag = "Ghi Chú :";
            this.labMiddle.Text = "Middle";
            this.labMiddle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labRing
            // 
            this.labRing.BackColor = System.Drawing.Color.Transparent;
            this.labRing.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labRing.ForeColor = System.Drawing.Color.Navy;
            this.labRing.Location = new System.Drawing.Point(95, 40);
            this.labRing.Name = "labRing";
            this.labRing.Size = new System.Drawing.Size(80, 21);
            this.labRing.TabIndex = 225;
            this.labRing.Tag = "Ghi Chú :";
            this.labRing.Text = "Ring";
            this.labRing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labLittle
            // 
            this.labLittle.BackColor = System.Drawing.Color.Transparent;
            this.labLittle.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labLittle.ForeColor = System.Drawing.Color.Navy;
            this.labLittle.Location = new System.Drawing.Point(9, 40);
            this.labLittle.Name = "labLittle";
            this.labLittle.Size = new System.Drawing.Size(80, 21);
            this.labLittle.TabIndex = 220;
            this.labLittle.Tag = "Ghi Chú :";
            this.labLittle.Text = "Little";
            this.labLittle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ptbL5L
            // 
            this.ptbL5L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL5L.Location = new System.Drawing.Point(9, 160);
            this.ptbL5L.Name = "ptbL5L";
            this.ptbL5L.Size = new System.Drawing.Size(80, 90);
            this.ptbL5L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL5L.TabIndex = 221;
            this.ptbL5L.TabStop = false;
            this.ptbL5L.Tag = "L5L";
            // 
            // ptbL4R
            // 
            this.ptbL4R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL4R.Location = new System.Drawing.Point(95, 256);
            this.ptbL4R.Name = "ptbL4R";
            this.ptbL4R.Size = new System.Drawing.Size(80, 90);
            this.ptbL4R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL4R.TabIndex = 224;
            this.ptbL4R.TabStop = false;
            this.ptbL4R.Tag = "L4R";
            // 
            // ptbL5R
            // 
            this.ptbL5R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL5R.Location = new System.Drawing.Point(9, 256);
            this.ptbL5R.Name = "ptbL5R";
            this.ptbL5R.Size = new System.Drawing.Size(80, 90);
            this.ptbL5R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL5R.TabIndex = 223;
            this.ptbL5R.TabStop = false;
            this.ptbL5R.Tag = "L5R";
            // 
            // ptbL5F
            // 
            this.ptbL5F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL5F.Location = new System.Drawing.Point(9, 64);
            this.ptbL5F.Name = "ptbL5F";
            this.ptbL5F.Size = new System.Drawing.Size(80, 90);
            this.ptbL5F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL5F.TabIndex = 219;
            this.ptbL5F.TabStop = false;
            this.ptbL5F.Tag = "L5F";
            // 
            // ptbL4F
            // 
            this.ptbL4F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL4F.Location = new System.Drawing.Point(95, 64);
            this.ptbL4F.Name = "ptbL4F";
            this.ptbL4F.Size = new System.Drawing.Size(80, 90);
            this.ptbL4F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL4F.TabIndex = 220;
            this.ptbL4F.TabStop = false;
            this.ptbL4F.Tag = "L4F";
            // 
            // ptbL4L
            // 
            this.ptbL4L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL4L.Location = new System.Drawing.Point(95, 160);
            this.ptbL4L.Name = "ptbL4L";
            this.ptbL4L.Size = new System.Drawing.Size(80, 90);
            this.ptbL4L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL4L.TabIndex = 222;
            this.ptbL4L.TabStop = false;
            this.ptbL4L.Tag = "L4L";
            // 
            // ptbL2L
            // 
            this.ptbL2L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL2L.Location = new System.Drawing.Point(267, 160);
            this.ptbL2L.Name = "ptbL2L";
            this.ptbL2L.Size = new System.Drawing.Size(80, 90);
            this.ptbL2L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL2L.TabIndex = 214;
            this.ptbL2L.TabStop = false;
            this.ptbL2L.Tag = "L2L";
            // 
            // ptbL3R
            // 
            this.ptbL3R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL3R.Location = new System.Drawing.Point(181, 256);
            this.ptbL3R.Name = "ptbL3R";
            this.ptbL3R.Size = new System.Drawing.Size(80, 90);
            this.ptbL3R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL3R.TabIndex = 218;
            this.ptbL3R.TabStop = false;
            this.ptbL3R.Tag = "L3R";
            // 
            // ptbL3F
            // 
            this.ptbL3F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL3F.Location = new System.Drawing.Point(181, 64);
            this.ptbL3F.Name = "ptbL3F";
            this.ptbL3F.Size = new System.Drawing.Size(80, 90);
            this.ptbL3F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL3F.TabIndex = 212;
            this.ptbL3F.TabStop = false;
            this.ptbL3F.Tag = "L3F";
            // 
            // ptbL3L
            // 
            this.ptbL3L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL3L.Location = new System.Drawing.Point(181, 160);
            this.ptbL3L.Name = "ptbL3L";
            this.ptbL3L.Size = new System.Drawing.Size(80, 90);
            this.ptbL3L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL3L.TabIndex = 215;
            this.ptbL3L.TabStop = false;
            this.ptbL3L.Tag = "L3L";
            // 
            // ptbL1F
            // 
            this.ptbL1F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL1F.Location = new System.Drawing.Point(353, 64);
            this.ptbL1F.Name = "ptbL1F";
            this.ptbL1F.Size = new System.Drawing.Size(80, 90);
            this.ptbL1F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL1F.TabIndex = 210;
            this.ptbL1F.TabStop = false;
            this.ptbL1F.Tag = "L1F";
            // 
            // ptbL1L
            // 
            this.ptbL1L.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL1L.Location = new System.Drawing.Point(353, 160);
            this.ptbL1L.Name = "ptbL1L";
            this.ptbL1L.Size = new System.Drawing.Size(80, 90);
            this.ptbL1L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL1L.TabIndex = 213;
            this.ptbL1L.TabStop = false;
            this.ptbL1L.Tag = "L1L";
            // 
            // ptbL1R
            // 
            this.ptbL1R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL1R.Location = new System.Drawing.Point(353, 256);
            this.ptbL1R.Name = "ptbL1R";
            this.ptbL1R.Size = new System.Drawing.Size(80, 90);
            this.ptbL1R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL1R.TabIndex = 216;
            this.ptbL1R.TabStop = false;
            this.ptbL1R.Tag = "L1R";
            // 
            // ptbL2F
            // 
            this.ptbL2F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL2F.Location = new System.Drawing.Point(267, 64);
            this.ptbL2F.Name = "ptbL2F";
            this.ptbL2F.Size = new System.Drawing.Size(80, 90);
            this.ptbL2F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL2F.TabIndex = 211;
            this.ptbL2F.TabStop = false;
            this.ptbL2F.Tag = "L2F";
            // 
            // ptbL2R
            // 
            this.ptbL2R.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ptbL2R.Location = new System.Drawing.Point(267, 256);
            this.ptbL2R.Name = "ptbL2R";
            this.ptbL2R.Size = new System.Drawing.Size(80, 90);
            this.ptbL2R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbL2R.TabIndex = 217;
            this.ptbL2R.TabStop = false;
            this.ptbL2R.Tag = "L2R";
            // 
            // rdFemale
            // 
            this.rdFemale.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rdFemale.AutoSize = true;
            this.rdFemale.BackColor = System.Drawing.Color.Transparent;
            this.rdFemale.Location = new System.Drawing.Point(521, 37);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(59, 17);
            this.rdFemale.TabIndex = 209;
            this.rdFemale.Text = "Female";
            this.rdFemale.UseVisualStyleBackColor = false;
            // 
            // rdMale
            // 
            this.rdMale.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rdMale.AutoSize = true;
            this.rdMale.BackColor = System.Drawing.Color.Transparent;
            this.rdMale.Checked = true;
            this.rdMale.Location = new System.Drawing.Point(452, 37);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(48, 17);
            this.rdMale.TabIndex = 208;
            this.rdMale.TabStop = true;
            this.rdMale.Text = "Male";
            this.rdMale.UseVisualStyleBackColor = false;
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCapture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCapture.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCapture.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnCapture.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnCapture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCapture.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapture.ForeColor = System.Drawing.Color.White;
            this.btnCapture.Image = global::iTalent.UI.Properties.Resources.webcam_32;
            this.btnCapture.Location = new System.Drawing.Point(252, 244);
            this.btnCapture.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(157, 41);
            this.btnCapture.TabIndex = 207;
            this.btnCapture.Tag = "Lấy Hình";
            this.btnCapture.Text = " Capture";
            this.btnCapture.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCapture.UseVisualStyleBackColor = false;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::iTalent.UI.Properties.Resources.logo3;
            this.pictureBox2.Location = new System.Drawing.Point(616, 46);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(294, 104);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 206;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::iTalent.UI.Properties.Resources.Logo_wellgen;
            this.pictureBox1.Location = new System.Drawing.Point(616, 171);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(294, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 205;
            this.pictureBox1.TabStop = false;
            // 
            // labReportId
            // 
            this.labReportId.AutoSize = true;
            this.labReportId.BackColor = System.Drawing.Color.Transparent;
            this.labReportId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labReportId.Location = new System.Drawing.Point(45, 6);
            this.labReportId.Name = "labReportId";
            this.labReportId.Size = new System.Drawing.Size(70, 21);
            this.labReportId.TabIndex = 151;
            this.labReportId.Tag = "";
            this.labReportId.Text = "ReportId";
            this.labReportId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labReportId.Visible = false;
            // 
            // labMa
            // 
            this.labMa.AutoSize = true;
            this.labMa.BackColor = System.Drawing.Color.Transparent;
            this.labMa.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMa.Location = new System.Drawing.Point(14, 6);
            this.labMa.Name = "labMa";
            this.labMa.Size = new System.Drawing.Size(25, 21);
            this.labMa.TabIndex = 150;
            this.labMa.Tag = "0";
            this.labMa.Text = "ID";
            this.labMa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labMa.Visible = false;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnLuu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLuu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnLuu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.ForeColor = System.Drawing.Color.White;
            this.btnLuu.Image = global::iTalent.UI.Properties.Resources.save;
            this.btnLuu.Location = new System.Drawing.Point(437, 244);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(157, 41);
            this.btnLuu.TabIndex = 149;
            this.btnLuu.Tag = "Lưu Thông Tin";
            this.btnLuu.Text = "Save Info";
            this.btnLuu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnScaner
            // 
            this.btnScaner.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnScaner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnScaner.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnScaner.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnScaner.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnScaner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScaner.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScaner.ForeColor = System.Drawing.Color.Black;
            this.btnScaner.Image = global::iTalent.UI.Properties.Resources.hand_icon__1_;
            this.btnScaner.Location = new System.Drawing.Point(72, 244);
            this.btnScaner.Margin = new System.Windows.Forms.Padding(0);
            this.btnScaner.Name = "btnScaner";
            this.btnScaner.Size = new System.Drawing.Size(157, 41);
            this.btnScaner.TabIndex = 148;
            this.btnScaner.Tag = "Lấy Vân Tay (F2)";
            this.btnScaner.Text = "Scan (F2)";
            this.btnScaner.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnScaner.UseVisualStyleBackColor = false;
            this.btnScaner.Click += new System.EventHandler(this.btnScaner_Click);
            // 
            // labName
            // 
            this.labName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(4, 33);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(107, 21);
            this.labName.TabIndex = 134;
            this.labName.Tag = "Tên Khách Hàng :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labParent
            // 
            this.labParent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labParent.BackColor = System.Drawing.Color.Transparent;
            this.labParent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labParent.Location = new System.Drawing.Point(12, 67);
            this.labParent.Name = "labParent";
            this.labParent.Size = new System.Drawing.Size(98, 21);
            this.labParent.TabIndex = 141;
            this.labParent.Tag = "Tên Cha/Mẹ:";
            this.labParent.Text = "Parent :";
            this.labParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(113, 99);
            this.txtDiaChi.MaxLength = 500;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(481, 29);
            this.txtDiaChi.TabIndex = 126;
            // 
            // txtDiDong
            // 
            this.txtDiDong.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(113, 168);
            this.txtDiDong.MaxLength = 20;
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.Size = new System.Drawing.Size(171, 29);
            this.txtDiDong.TabIndex = 129;
            // 
            // labCity
            // 
            this.labCity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(4, 137);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(107, 21);
            this.labCity.TabIndex = 136;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTenChaMe
            // 
            this.txtTenChaMe.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtTenChaMe.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChaMe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTenChaMe.Location = new System.Drawing.Point(113, 64);
            this.txtTenChaMe.MaxLength = 250;
            this.txtTenChaMe.Name = "txtTenChaMe";
            this.txtTenChaMe.Size = new System.Drawing.Size(215, 29);
            this.txtTenChaMe.TabIndex = 124;
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpNgaySinh.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CustomFormat = "MM/dd/yyyy";
            this.dtpNgaySinh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(432, 64);
            this.dtpNgaySinh.MaxDate = new System.DateTime(5000, 12, 31, 0, 0, 0, 0);
            this.dtpNgaySinh.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(162, 29);
            this.dtpNgaySinh.TabIndex = 125;
            this.dtpNgaySinh.Value = new System.DateTime(2015, 9, 13, 0, 0, 0, 0);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtGhiChu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtGhiChu.Location = new System.Drawing.Point(113, 203);
            this.txtGhiChu.MaxLength = 500;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(481, 29);
            this.txtGhiChu.TabIndex = 133;
            // 
            // labAddress
            // 
            this.labAddress.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(8, 99);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(103, 21);
            this.labAddress.TabIndex = 135;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labMobile
            // 
            this.labMobile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labMobile.AutoSize = true;
            this.labMobile.BackColor = System.Drawing.Color.Transparent;
            this.labMobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMobile.Location = new System.Drawing.Point(46, 171);
            this.labMobile.Name = "labMobile";
            this.labMobile.Size = new System.Drawing.Size(65, 21);
            this.labMobile.TabIndex = 138;
            this.labMobile.Tag = "Di Động :";
            this.labMobile.Text = "Mobile :";
            this.labMobile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(113, 134);
            this.txtThanhPho.MaxLength = 20;
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.Size = new System.Drawing.Size(171, 29);
            this.txtThanhPho.TabIndex = 127;
            // 
            // txtTen
            // 
            this.txtTen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(113, 29);
            this.txtTen.MaxLength = 250;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(215, 29);
            this.txtTen.TabIndex = 123;
            // 
            // labSex
            // 
            this.labSex.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labSex.BackColor = System.Drawing.Color.Transparent;
            this.labSex.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSex.Location = new System.Drawing.Point(334, 33);
            this.labSex.Name = "labSex";
            this.labSex.Size = new System.Drawing.Size(91, 21);
            this.labSex.TabIndex = 142;
            this.labSex.Tag = "Giới Tính :";
            this.labSex.Text = "Sex :";
            this.labSex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labBirthday
            // 
            this.labBirthday.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labBirthday.BackColor = System.Drawing.Color.Transparent;
            this.labBirthday.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labBirthday.Location = new System.Drawing.Point(334, 67);
            this.labBirthday.Name = "labBirthday";
            this.labBirthday.Size = new System.Drawing.Size(92, 21);
            this.labBirthday.TabIndex = 144;
            this.labBirthday.Tag = "Ngày Sinh :";
            this.labBirthday.Text = "Birthday :";
            this.labBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labNote
            // 
            this.labNote.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labNote.BackColor = System.Drawing.Color.Transparent;
            this.labNote.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNote.Location = new System.Drawing.Point(4, 206);
            this.labNote.Name = "labNote";
            this.labNote.Size = new System.Drawing.Size(106, 21);
            this.labNote.TabIndex = 147;
            this.labNote.Tag = "Ghi Chú :";
            this.labNote.Text = "Remark :";
            this.labNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(351, 134);
            this.txtEmail.MaxLength = 250;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(243, 29);
            this.txtEmail.TabIndex = 128;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDienThoai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(387, 168);
            this.txtDienThoai.MaxLength = 20;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(207, 29);
            this.txtDienThoai.TabIndex = 130;
            // 
            // labPhone
            // 
            this.labPhone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(290, 171);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(91, 21);
            this.labPhone.TabIndex = 137;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Phone :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labEmail
            // 
            this.labEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labEmail.AutoSize = true;
            this.labEmail.BackColor = System.Drawing.Color.White;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(290, 137);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(55, 21);
            this.labEmail.TabIndex = 139;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxGioiTinh
            // 
            this.cbxGioiTinh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxGioiTinh.DropDownWidth = 120;
            this.cbxGioiTinh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxGioiTinh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.cbxGioiTinh.FormattingEnabled = true;
            this.cbxGioiTinh.ItemHeight = 21;
            this.cbxGioiTinh.Items.AddRange(new object[] {
            "Nam",
            "Nữ",
            "None"});
            this.cbxGioiTinh.Location = new System.Drawing.Point(18, 33);
            this.cbxGioiTinh.Name = "cbxGioiTinh";
            this.cbxGioiTinh.Size = new System.Drawing.Size(97, 29);
            this.cbxGioiTinh.TabIndex = 143;
            this.cbxGioiTinh.Tag = "Nữ";
            this.cbxGioiTinh.Visible = false;
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(250, 250);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // FrmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 689);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCustomer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StateCommon.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)(((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.Tag = ".:THÔNG TIN KHÁCH HÀNG";
            this.Load += new System.EventHandler(this.FrmCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            this.prbRightHand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbR4L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR5R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR4R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR4F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR5F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR5L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR2L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR3R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR1F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR2R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR2F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR1R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR3F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR3L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbR1L)).EndInit();
            this.grbLeftHand.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbL5L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL4R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL5R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL5F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL4F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL4L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL2L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL3R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL3F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL3L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL1F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL1L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL1R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL2F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbL2R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.Button btnScaner;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labParent;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.TextBox txtTenChaMe;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.Label labMobile;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label labSex;
        private System.Windows.Forms.Label labBirthday;
        private System.Windows.Forms.Label labNote;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.ComboBox cbxGioiTinh;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private System.Windows.Forms.Label labMa;
        private System.Windows.Forms.Label labReportId;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.GroupBox grbLeftHand;
        private System.Windows.Forms.PictureBox ptbL5L;
        private System.Windows.Forms.PictureBox ptbL4R;
        private System.Windows.Forms.PictureBox ptbL5R;
        private System.Windows.Forms.PictureBox ptbL5F;
        private System.Windows.Forms.PictureBox ptbL4F;
        private System.Windows.Forms.PictureBox ptbL4L;
        private System.Windows.Forms.PictureBox ptbL2L;
        private System.Windows.Forms.PictureBox ptbL3R;
        private System.Windows.Forms.PictureBox ptbL1F;
        private System.Windows.Forms.PictureBox ptbL2R;
        private System.Windows.Forms.PictureBox ptbL2F;
        private System.Windows.Forms.PictureBox ptbL1R;
        private System.Windows.Forms.PictureBox ptbL3F;
        private System.Windows.Forms.PictureBox ptbL3L;
        private System.Windows.Forms.PictureBox ptbL1L;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labIndex;
        private System.Windows.Forms.Label labMiddle;
        private System.Windows.Forms.Label labRing;
        private System.Windows.Forms.Label labLittle;
        private System.Windows.Forms.GroupBox prbRightHand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox ptbR4L;
        private System.Windows.Forms.PictureBox ptbR5R;
        private System.Windows.Forms.PictureBox ptbR4R;
        private System.Windows.Forms.PictureBox ptbR4F;
        private System.Windows.Forms.PictureBox ptbR5F;
        private System.Windows.Forms.PictureBox ptbR5L;
        private System.Windows.Forms.PictureBox ptbR2L;
        private System.Windows.Forms.PictureBox ptbR3R;
        private System.Windows.Forms.PictureBox ptbR1F;
        private System.Windows.Forms.PictureBox ptbR2R;
        private System.Windows.Forms.PictureBox ptbR2F;
        private System.Windows.Forms.PictureBox ptbR1R;
        private System.Windows.Forms.PictureBox ptbR3F;
        private System.Windows.Forms.PictureBox ptbR3L;
        private System.Windows.Forms.PictureBox ptbR1L;
        private System.Windows.Forms.ImageList imageList;
    }
}