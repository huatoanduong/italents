﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace iTalent.Utils
{
    public class ConvertUtil
    {
        private const string StrDisplayedFormatDatetime = "dd/MM/yyyy";
        private const string StrStoredFormatDatetime = "yyyyMMdd";

        public static char ThousandSeparator =
            Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);

        public static char DecimalSeparator =
            Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

        public static DateTime DefaultDateTime = new DateTime(1990, 1, 1, 0, 0, 0);
        public static int DefaultInt = 0;

        /// <summary>
        ///     To string Numeric
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string ToNumericString(long? number)
        {
            return (number != null) ? ((long) number).ToString("N0") : "0";
        }

        public static string ToPlainString(long? number)
        {
            return (number != null) ? ((long) number).ToString() : "0";
        }

        public static string ToDisplayedString(DateTime? time)
        {
            var timeNotNull = time ?? DefaultDateTime;
            return timeNotNull.ToString(StrDisplayedFormatDatetime);
        }

        public static DateTime ToDateTime(string strTime, DateTime defaultValue)
        {
            return ToDateTime(strTime, StrDisplayedFormatDatetime, defaultValue);
        }

        public static DateTime ToDateTime(string strTime, string strFomatter, DateTime defaultValue)
        {
            DateTime obj;
            try
            {
                obj = DateTime.ParseExact(strTime, strFomatter,
                    CultureInfo.CurrentCulture);
            }
            catch (Exception)
            {
                obj = defaultValue;
            }
            return obj;
        }

        public static string ToStoredString(DateTime? time)
        {
            var timeNotNull = time ?? DefaultDateTime;
            return timeNotNull.ToString(StrStoredFormatDatetime);
        }

        public static int ToInt(string strNumber)
        {
            if (strNumber == null)
            {
                return 0;
            }
            strNumber = strNumber.Replace(ThousandSeparator.ToString(), "");
            if (strNumber == "")
            {
                return 0;
            }
            return int.Parse(strNumber);
        }

        public static long ToLong(string strNumber)
        {
            if (strNumber == null)
            {
                return 0;
            }
            strNumber = strNumber.Replace(ThousandSeparator.ToString(), "");
            if (strNumber == "")
            {
                return 0;
            }
            return long.Parse(strNumber);
        }

        public static decimal ToDecimal(string strNumber)
        {
            if (strNumber == null)
            {
                return 0;
            }
            strNumber = strNumber.Replace(ThousandSeparator.ToString(), "");
            if (strNumber == "")
            {
                return 0;
            }
            return decimal.Parse(strNumber);
        }

        public static DateTime ToDefault(DateTime? value)
        {
            return value ?? DefaultDateTime;
        }

        public static long ToDefault(long? value)
        {
            return value ?? DefaultInt;
        }

        public static int ToDefault(int? value)
        {
            return value ?? DefaultInt;
        }

        public static string ConverToUnsign(string s)
        {
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            var temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        /*Converts List To DataTable*/

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            var properties =
                TypeDescriptor.GetProperties(typeof (T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (var item in data)
            {
                var row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;

                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ToDataTable<T>(ICollection<T> data, IList<string> columnRemoves)
        {
            var properties =
                TypeDescriptor.GetProperties(typeof (T));
            var table = new DataTable();
            foreach (var prop in properties.Cast<PropertyDescriptor>().Where(prop => !columnRemoves.Contains(prop.Name))
                )
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            foreach (var item in data)
            {
                var row = table.NewRow();
                foreach (
                    var prop in properties.Cast<PropertyDescriptor>().Where(prop => !columnRemoves.Contains(prop.Name)))
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        /*Converts DataTable To List*/

        public static List<TSource> ToList<TSource>(DataTable dataTable) where TSource : new()
        {
            var dataList = new List<TSource>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            var objFieldNames = (from PropertyInfo aProp in typeof (TSource).GetProperties(flags)
                select new {aProp.Name, Type = Nullable.GetUnderlyingType(aProp.PropertyType) ?? aProp.PropertyType})
                .ToList();
            var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                select new {Name = aHeader.ColumnName, Type = aHeader.DataType}).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

            foreach (var dataRow in dataTable.AsEnumerable().ToList())
            {
                var aTSource = new TSource();
                foreach (var aField in commonFields)
                {
                    var propertyInfos = aTSource.GetType().GetProperty(aField.Name);
                    var value = (dataRow[aField.Name] == DBNull.Value) ? null : dataRow[aField.Name];
                        //if database field is nullable
                    propertyInfos.SetValue(aTSource, value, null);
                }
                dataList.Add(aTSource);
            }
            return dataList;
        }
    }
}