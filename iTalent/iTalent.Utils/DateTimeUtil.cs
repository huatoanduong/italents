using System;

namespace iTalent.Utils
{
    public enum Quarter
    {
        First = 1,
        Second,
        Third,
        Fourth
    }

    public enum Month
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

    public class DateTimeUtil
    {
        public static DateTime GetStartOfQuarter(int year, Quarter qtr)
        {
            DateTime result;
            if (qtr == Quarter.First)
            {
                result = new DateTime(year, 1, 1, 0, 0, 0, 0);
            }
            else
            {
                if (qtr == Quarter.Second)
                {
                    result = new DateTime(year, 4, 1, 0, 0, 0, 0);
                }
                else
                {
                    result = qtr == Quarter.Third
                        ? new DateTime(year, 7, 1, 0, 0, 0, 0)
                        : new DateTime(year, 10, 1, 0, 0, 0, 0);
                }
            }
            return result;
        }

        public static DateTime GetEndOfQuarter(int year, Quarter qtr)
        {
            DateTime result;
            if (qtr == Quarter.First)
            {
                result = new DateTime(year, 3, DateTime.DaysInMonth(year, 3), 23, 59, 59, 999);
            }
            else
            {
                if (qtr == Quarter.Second)
                {
                    result = new DateTime(year, 6, DateTime.DaysInMonth(year, 6), 23, 59, 59, 999);
                }
                else
                {
                    result = qtr == Quarter.Third
                        ? new DateTime(year, 9, DateTime.DaysInMonth(year, 9), 23, 59, 59, 999)
                        : new DateTime(year, 12, DateTime.DaysInMonth(year, 12), 23, 59, 59, 999);
                }
            }
            return result;
        }

        public static Quarter GetQuarter(Month month)
        {
            Quarter result;
            if (month <= Month.March)
            {
                result = Quarter.First;
            }
            else
            {
                if (month >= Month.April && month <= Month.June)
                {
                    result = Quarter.Second;
                }
                else
                {
                    if (month >= Month.July && month <= Month.September)
                    {
                        result = Quarter.Third;
                    }
                    else
                    {
                        result = Quarter.Fourth;
                    }
                }
            }
            return result;
        }

        public static DateTime GetEndOfLastQuarter()
        {
            var endOfQuarter = DateTime.Now.Month <= 3
                ? GetEndOfQuarter(DateTime.Now.Year - 1, GetQuarter(Month.December))
                : GetEndOfQuarter(DateTime.Now.Year, GetQuarter((Month) DateTime.Now.Month));
            return endOfQuarter;
        }

        public static DateTime GetStartOfLastQuarter()
        {
            var startOfQuarter = DateTime.Now.Month <= 3
                ? GetStartOfQuarter(DateTime.Now.Year - 1, GetQuarter(Month.December))
                : GetStartOfQuarter(DateTime.Now.Year, GetQuarter((Month) DateTime.Now.Month));
            return startOfQuarter;
        }

        public static DateTime GetStartOfCurrentQuarter()
        {
            return GetStartOfQuarter(DateTime.Now.Year, GetQuarter((Month) DateTime.Now.Month));
        }

        public static DateTime GetEndOfCurrentQuarter()
        {
            return GetEndOfQuarter(DateTime.Now.Year, GetQuarter((Month) DateTime.Now.Month));
        }

        public static DateTime GetStartOfLastWeek()
        {
            var num = (int) (DateTime.Now.DayOfWeek + 7);
            var dateTime = DateTime.Now.Subtract(TimeSpan.FromDays(num));
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfLastWeek()
        {
            var dateTime = GetStartOfLastWeek().AddDays(6.0);
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 999);
        }

        public static DateTime GetStartOfCurrentWeek()
        {
            var dayOfWeek = (int) DateTime.Now.DayOfWeek;
            var dateTime = DateTime.Now.Subtract(TimeSpan.FromDays(dayOfWeek));
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfCurrentWeek()
        {
            var dateTime = GetStartOfCurrentWeek().AddDays(6.0);
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, 999);
        }

        public static DateTime GetStartOfMonth(int month, int year)
        {
            return new DateTime(year, month, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfMonth(int month, int year)
        {
            return new DateTime(year, month, DateTime.DaysInMonth(year, month), 23, 59, 59, 999);
        }

        public static DateTime GetStartOfLastMonth()
        {
            var startOfMonth = DateTime.Now.Month == 1
                ? GetStartOfMonth(12, DateTime.Now.Year - 1)
                : GetStartOfMonth(DateTime.Now.Month - 1, DateTime.Now.Year);
            return startOfMonth;
        }

        public static DateTime GetEndOfLastMonth()
        {
            var endOfMonth = DateTime.Now.Month == 1
                ? GetEndOfMonth(12, DateTime.Now.Year - 1)
                : GetEndOfMonth(DateTime.Now.Month - 1, DateTime.Now.Year);
            return endOfMonth;
        }

        public static DateTime GetStartOfCurrentMonth()
        {
            return GetStartOfMonth(DateTime.Now.Month, DateTime.Now.Year);
        }

        public static DateTime GetEndOfCurrentMonth()
        {
            return GetEndOfMonth(DateTime.Now.Month, DateTime.Now.Year);
        }

        public static DateTime GetStartOfYear(int year)
        {
            return new DateTime(year, 1, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfYear(int year)
        {
            return new DateTime(year, 12, DateTime.DaysInMonth(year, 12), 23, 59, 59, 999);
        }

        public static DateTime GetStartOfLastYear()
        {
            return GetStartOfYear(DateTime.Now.Year - 1);
        }

        public static DateTime GetEndOfLastYear()
        {
            return GetEndOfYear(DateTime.Now.Year - 1);
        }

        public static DateTime GetStartOfCurrentYear()
        {
            return GetStartOfYear(DateTime.Now.Year);
        }

        public static DateTime GetEndOfCurrentYear()
        {
            return GetEndOfYear(DateTime.Now.Year);
        }

        public static DateTime GetStartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }

        public static DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }

        public static DateTime ConvertDate(string date)
        {
            return DateTime.Parse(date);
        }
    }
}