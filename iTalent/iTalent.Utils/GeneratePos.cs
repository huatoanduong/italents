﻿namespace iTalent.Utils
{
    public static class GeneratePos
    {
        public static string KeyGenerate()
        {
            return HardwareInfo.GetKeyB();
        }

        public static bool CheckKey(string keyDe)
        {
            string keyA = HardwareInfo.GetKeyA();
            string keyPos = HardwareInfo.GetKeyPos();
            return (keyA + keyDe) == keyPos;
        }

        public static string GetKeyDe(string keyEn)
        {
            return HardwareInfo.KeyB_De(keyEn);
        }
    }
}
