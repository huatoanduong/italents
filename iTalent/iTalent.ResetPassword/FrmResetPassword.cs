﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTalent.DAL.Services;
using iTalent.Entities;

namespace iTalent.ResetPassword
{
    public partial class FrmResetPassword : Form
    {
        public FrmResetPassword()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text == "")
                {
                    MessageBox.Show(@"Please input password!");
                    return;
                }

                using (IAgencyService service = new AgencyService())
                {
                    FIAgency objAgency = service.Find(1);
                    if (objAgency == null)
                    {
                        MessageBox.Show(@"Not found!");
                        return;
                    }

                    objAgency.Username = txtPassword.Text;
                    objAgency.Password = txtPassword.Text;

                    bool kq = service.Update(objAgency);

                    MessageBox.Show(kq ? @"Successfully!" : service.ErrMsg);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
