//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace iTalent.Entities
{
    public partial class FIDevice : IIdentityObject
    {
        public long ID { get; set; }
        public long AgencyID { get; set; }
        public string Devicename { get; set; }
        public Nullable<System.DateTime> ActiveDate { get; set; }
        public string ProductKey { get; set; }
        public string SerailKey { get; set; }
        public string SecKey { get; set; }
        public string Max { get; set; }
        public string Min { get; set; }
    
        public virtual FIAgency FIAgency { get; set; }
    }
}
