//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace iTalent.Entities
{
    public partial class FITemplate : IIdentityObject
    {
        public FITemplate()
        {
            this.FIPrintRecord = new HashSet<FIPrintRecord>();
        }
    
        public long ID { get; set; }
        public long AgencyID { get; set; }
        public string Name { get; set; }
        public string TemplatePermit { get; set; }
        public Nullable<decimal> TemplatePoint { get; set; }
        public string TemplateFileName { get; set; }
    
        public virtual FIAgency FIAgency { get; set; }
        public virtual ICollection<FIPrintRecord> FIPrintRecord { get; set; }
    }
}
