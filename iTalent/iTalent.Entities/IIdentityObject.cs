﻿namespace iTalent.Entities
{
    public interface IIdentityObject
    {
        long ID { get; set; }
    }
}