//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace iTalent.Entities
{
    public partial class FIPrintRecord : IIdentityObject
    {
        public long ID { get; set; }
        public Nullable<long> AgencyID { get; set; }
        public long TemplateID { get; set; }
        public long ReportID { get; set; }
        public Nullable<System.DateTime> PrintDate { get; set; }
        public long PointID { get; set; }
    
        public virtual FICustomer FICustomer { get; set; }
        public virtual FIPoint FIPoint { get; set; }
        public virtual FITemplate FITemplate { get; set; }
    }
}
