﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.DAL.Services;

namespace iTalent.Analyse.UI.GUI
{
   
    public partial class FrmLogin : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        public FrmLogin()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1, labUsername, labPassword, btnSave};
            _baseForm = new BaseForm();
            
            Issuccess = false;
            txtPassword.Select();
        }

        private void btnLuu_Click(object sender, System.EventArgs e)
        {
            using (IAgencyService service = new AgencyService())
            {
                Issuccess = service.Login(txtPassword.Text, txtPassword.Text);
                if (Issuccess)
                    Close();
                else
                {
                    _baseForm.ShowMessage(IconMessageBox.Error,service.ErrMsg);
                }
            }
        }

        private void btnExit_Click(object sender, System.EventArgs e)
        {
           Environment.Exit(0);
        }

        //private void btnSave_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        btnSave.PerformClick();
        //    }
        //}
    }
}
