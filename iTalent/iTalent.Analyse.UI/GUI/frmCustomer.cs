﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.DAL.Commons;
using iTalent.DAL.Services;
using iTalent.Entities;
using iTalent.FingerprintScanDevice;
using iTalent.Utils;

namespace iTalent.Analyse.UI.GUI
{
    public enum ValidStatus
    {
        New = 0, 
        Edit = 1
    }
    public partial class FrmCustomer : C4FunForm
    {
        private FICustomer CurrCustomer { get; set; }
        private readonly BaseForm _baseForm;
        public bool Issuccess { get; set; }
        private string _oldNameCustomer;
        private string _dirFile {get {return ICurrentSessionService.DesDirNameSource; } }
        private bool _isinsert;

        public FrmCustomer()
        {
            InitializeComponent();
           
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control>
            {
                c4FunHeaderGroup1,
                labAddress,
                labBirthday,
                labCity,
                labCity,
                labEmail,
                //labEntity,
                labMobile,
                labName,
                labNote,
                labParent,
                labPhone,
                labSex,
                btnLuu,
                btnScaner,
                btnCapture
            };

            _baseForm = new BaseForm();
            Issuccess = false;
            _isinsert = true;
            //LoadCbxSex();
        }
        public FrmCustomer(FICustomer objCustomer)
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control>
            {
                c4FunHeaderGroup1,
                labAddress,
                labBirthday,
                labCity,
                labCity,
                labEmail,
                //labEntity,
                labMobile,
                labName,
                labNote,
                labParent,
                labPhone,
                labSex,
                btnLuu,
                btnScaner,
                 btnCapture
            };
            _baseForm = new BaseForm();
            CurrCustomer = objCustomer;
            Issuccess = false;
            _isinsert = false;
            //LoadCbxSex();
        }

        //private void LoadCbxSex()
        //{
        //    cbxGioiTinh.Items.Clear();
        //    if (ICurrentSessionService.VietNamLanguage)
        //    {
        //        cbxGioiTinh.Items.Add("None");
        //        cbxGioiTinh.Items.Add("Nam");
        //        cbxGioiTinh.Items.Add("Nữ");
        //    }
        //    else
        //    {
        //        cbxGioiTinh.Items.Add("None");
        //        cbxGioiTinh.Items.Add("Male");
        //        cbxGioiTinh.Items.Add("Female");
        //    }
        //}

        private string GetGender()
        {
            if (ICurrentSessionService.VietNamLanguage)
            {
                return rdFemale.Checked ? "Nữ" : "Nam";
            }
            return rdFemale.Checked ? "Female" : "Male";
        }
        private void SetGender(string sex)
        {
            if (sex == "Nữ" || sex == "Female")
                rdFemale.Checked = true;
            else
            {
                rdMale.Checked = true;
            }
        }

        private void LoadToUi(FICustomer obj)
        {
            try
            {
                if (obj == null)
                {
                    using (ICustomerService service = new CustomerService())
                    {
                        obj = service.CreateEntity();
                        obj.ReportID = ICurrentSessionService.Username + DateTimeUtil.ConvertDate(obj.Date.ToString()).ToString("yyMMddhhmmss");
                    }
                }

                labMa.Text = obj.ID.ToString();
                labReportId.Text = obj.ReportID;

                txtTen.Text = obj.Name;
                _oldNameCustomer = obj.Name;
                
                cbxGioiTinh.Text = obj.Gender;
                SetGender(obj.Gender);//them 
                txtTenChaMe.Text = obj.Parent;
                dtpNgaySinh.Value = DateTimeUtil.ConvertDate(obj.DOB.ToString());
                txtDiaChi.Text = obj.Address1;
                txtThanhPho.Text = obj.City;
                txtEmail.Text = obj.Email;
                txtDiDong.Text = obj.Mobile;
                txtDienThoai.Text = obj.Tel;
         
                //txtZip.Text = obj.ZipPosTalCode;
                //txtTinhTrang.Text = obj.State;
                txtGhiChu.Text = obj.Remark;
                CurrCustomer = obj;
            }
            catch (Exception ex)
            {
               _baseForm.ShowMessage(IconMessageBox.Error,ex.Message);
                Environment.Exit(0);
            }
        }

        private void Insertupdatefile(string dir, string dirtemp, string reportid,
          string nameCustomer, string oldnameCustomer)
        {
            try
            {
                var olddirfolder = dir + @"\" + reportid + "_" + oldnameCustomer;
                var newdirfolder = dir + @"\" + reportid + "_" + nameCustomer;
                var tempfolder = dirtemp + @"\" + reportid;

                Directory.CreateDirectory(newdirfolder);

                if (Directory.Exists(olddirfolder) && olddirfolder != newdirfolder)
                {
                    var d = new DirectoryInfo(olddirfolder);

                    var infos = d.GetFiles();
                    if (!d.GetFiles().Any()) return;
                    foreach (var f in infos)
                    {
                        var fileName = Path.GetFileName(f.Name);
                        string destFile = Path.Combine(newdirfolder, fileName);
                        File.Move(f.FullName, destFile);
                        f.Delete();
                    }
                    d.Delete(true);
                }

                if (Directory.Exists(tempfolder))
                {
                    var d = new DirectoryInfo(tempfolder);

                    var infos = d.GetFiles();
                    if (!d.GetFiles().Any()) return;
                    foreach (var f in infos)
                    {
                        var fileName = Path.GetFileName(f.Name);
                        var destFile = Path.Combine(newdirfolder, fileName);
                        if (!f.Name.ToLower().Contains(".dat") && !f.Name.ToLower().Contains(reportid.ToLower()+"f.bmp"))
                            destFile = ConvertToObject(destFile, reportid);
                        File.Move(f.FullName, destFile);
                    }
                    d.Delete(true);
                }
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

        private void SaveDataFile(FIAgency objAgency, FICustomer objCustomer, string desDirName)
        {
            try
            {
                if (objAgency == null) return;
                if (objCustomer == null) return;

                string name = ConvertUtil.ConverToUnsign(objCustomer.Name).Replace(" ", "").ToUpper();

                if (!Directory.Exists(desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper()))
                    Directory.CreateDirectory(desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper());

                string fileName = desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper() + @"\" + objCustomer.ReportID + "profile.dat";

                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                    fileName = desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper() + @"\" + objCustomer.ReportID + "profile.txt";
                }

                File.Create(fileName).Dispose();
                StreamWriter streamWriter = new StreamWriter(fileName);

                //streamWriter.WriteLine("0|" + objAgency.Username + "|" + objAgency.Name + "|0|||" + objAgency.Address1 + "|||||||" + objAgency.MobileNo + "|X||||||||||" + objAgency.Username + objAgency.Name);
                //streamWriter.WriteLine("1|" + objAgency.Username + "|" + objCustomer.ReportID + "|" + DateTimeUtil.ConvertDate(objCustomer.Date.ToString()).ToString("dd/MM/yyyy") + "|" + objCustomer.Name + "|" + objCustomer.Parent + "|" + objCustomer.Gender + "|" + DateTimeUtil.ConvertDate(objCustomer.DOB.ToString()).ToString("dd/MM/yyyy") + "|" + objCustomer.Address1 + "|" + objCustomer.Address2 + "|" + objCustomer.City + "|" + objCustomer.ZipPosTalCode + "|" + objCustomer.State + "|" + objCustomer.Country + "|" + objCustomer.Tel + "|" + objCustomer.Mobile + "|" + objCustomer.Remark + "|" + objCustomer.Email);

                streamWriter.WriteLine("Name: "+objCustomer.Name);
                streamWriter.WriteLine("Gender: " + objCustomer.Gender);
                streamWriter.WriteLine("Telephone: " + objCustomer.Tel);
                streamWriter.WriteLine("Birthday: " + DateTimeUtil.ConvertDate(objCustomer.DOB.ToString()).ToString("MM/dd/yyyy"));
                streamWriter.WriteLine("Address: " + objCustomer.Address1);
                streamWriter.WriteLine("Email: " + objCustomer.Email);
                streamWriter.WriteLine("Parents/Remarks: " + objCustomer.Parent + "/" + objCustomer.Remark);

                streamWriter.Close();
                File.Move(fileName, Path.ChangeExtension(fileName, ".dat"));
            }
            catch
            {
                _baseForm.EnglishMsg = "Can not create file .dat";
                _baseForm.VietNamMsg = "Không thể tạo file .dat";
               _baseForm.ShowMessage(IconMessageBox.Warning);
                Close();
            }
        }

        private string ConvertToObject(string dirname, string reportid)
        {
            if (dirname.Contains(reportid + "L1F")) return dirname.Replace(reportid + "L1F.bmp", "1_1.b");
            if (dirname.Contains(reportid + "L1L")) return dirname.Replace(reportid + "L1L.bmp", "1_2.b");
            if (dirname.Contains(reportid + "L1R")) return dirname.Replace(reportid + "L1R.bmp", "1_3.b");

            if (dirname.Contains(reportid + "L2F")) return dirname.Replace(reportid + "L2F.bmp", "2_1.b");
            if (dirname.Contains(reportid + "L2L")) return dirname.Replace(reportid + "L2L.bmp", "2_2.b");
            if (dirname.Contains(reportid + "L2R")) return dirname.Replace(reportid + "L2R.bmp", "2_3.b");

            if (dirname.Contains(reportid + "L3F")) return dirname.Replace(reportid + "L3F.bmp", "3_1.b");
            if (dirname.Contains(reportid + "L3L")) return dirname.Replace(reportid + "L3L.bmp", "3_2.b");
            if (dirname.Contains(reportid + "L3R")) return dirname.Replace(reportid + "L3R.bmp", "3_3.b");

            if (dirname.Contains(reportid + "L4F")) return dirname.Replace(reportid + "L4F.bmp", "4_1.b");
            if (dirname.Contains(reportid + "L4L")) return dirname.Replace(reportid + "L4L.bmp", "4_2.b");
            if (dirname.Contains(reportid + "L4R")) return dirname.Replace(reportid + "L4R.bmp", "4_3.b");

            if (dirname.Contains(reportid + "L5F")) return dirname.Replace(reportid + "L5F.bmp", "5_1.b");
            if (dirname.Contains(reportid + "L5L")) return dirname.Replace(reportid + "L5L.bmp", "5_2.b");
            if (dirname.Contains(reportid + "L5R")) return dirname.Replace(reportid + "L5R.bmp", "5_3.b");

            //

            if (dirname.Contains(reportid + "R1F")) return dirname.Replace(reportid + "R1F.bmp", "6_1.b");
            if (dirname.Contains(reportid + "R1L")) return dirname.Replace(reportid + "R1L.bmp", "6_2.b");
            if (dirname.Contains(reportid + "R1R")) return dirname.Replace(reportid + "R1R.bmp", "6_3.b");

            if (dirname.Contains(reportid + "R2F")) return dirname.Replace(reportid + "R2F.bmp", "7_1.b");
            if (dirname.Contains(reportid + "R2L")) return dirname.Replace(reportid + "R2L.bmp", "7_2.b");
            if (dirname.Contains(reportid + "R2R")) return dirname.Replace(reportid + "R2R.bmp", "7_3.b");

            if (dirname.Contains(reportid + "R3F")) return dirname.Replace(reportid + "R3F.bmp", "8_1.b");
            if (dirname.Contains(reportid + "R3L")) return dirname.Replace(reportid + "R3L.bmp", "8_2.b");
            if (dirname.Contains(reportid + "R3R")) return dirname.Replace(reportid + "R3R.bmp", "8_3.b");

            if (dirname.Contains(reportid + "R4F")) return dirname.Replace(reportid + "R4F.bmp", "9_1.b");
            if (dirname.Contains(reportid + "R4L")) return dirname.Replace(reportid + "R4L.bmp", "9_2.b");
            if (dirname.Contains(reportid + "R4R")) return dirname.Replace(reportid + "R4R.bmp", "9_3.b");

            if (dirname.Contains(reportid + "R5F")) return dirname.Replace(reportid + "R5F.bmp", "10_1.b");
            if (dirname.Contains(reportid + "R5L")) return dirname.Replace(reportid + "R5L.bmp", "10_2.b");
            if (dirname.Contains(reportid + "R5R")) return dirname.Replace(reportid + "R5R.bmp", "10_3.b");

            return string.Empty;
        }

        private string ConvertToImage(string dirname, string reportid)
        {
            string dir = "";
            if (dirname.Contains("1_1")) dir = dirname.Replace("1_1.b", reportid + "L1F.bmp");
            if (dirname.Contains("1_2")) dir = dirname.Replace("1_2.b", reportid + "L1L.bmp");
            if (dirname.Contains("1_3")) dir = dirname.Replace("1_3.b", reportid + "L1R.bmp");

            if (dirname.Contains("2_1")) dir = dirname.Replace("2_1.b", reportid + "L2F.bmp");
            if (dirname.Contains("2_2")) dir = dirname.Replace("2_2.b", reportid + "L2L.bmp");
            if (dirname.Contains("2_3")) dir = dirname.Replace("2_3.b", reportid + "L2R.bmp");

            if (dirname.Contains("3_1")) dir = dirname.Replace("3_1.b", reportid + "L3F.bmp");
            if (dirname.Contains("3_2")) dir = dirname.Replace("3_2.b", reportid + "L3L.bmp");
            if (dirname.Contains("3_3")) dir = dirname.Replace("3_3.b", reportid + "L3R.bmp");

            if (dirname.Contains("4_1")) dir = dirname.Replace("4_1.b", reportid + "L4F.bmp");
            if (dirname.Contains("4_2")) dir = dirname.Replace("4_2.b", reportid + "L4L.bmp");
            if (dirname.Contains("4_3")) dir = dirname.Replace("4_3.b", reportid + "L4R.bmp");

            if (dirname.Contains("5_1")) dir = dirname.Replace("5_1.b", reportid + "L5F.bmp");
            if (dirname.Contains("5_2")) dir = dirname.Replace("5_2.b", reportid + "L5L.bmp");
            if (dirname.Contains("5_3")) dir = dirname.Replace("5_3.b", reportid + "L5R.bmp");

            //

            if (dirname.Contains("6_1")) dir = dirname.Replace("6_1.b", reportid + "R1F.bmp");
            if (dirname.Contains("6_2")) dir = dirname.Replace("6_2.b", reportid + "R1L.bmp");
            if (dirname.Contains("6_3")) dir = dirname.Replace("6_3.b", reportid + "R1R.bmp");

            if (dirname.Contains("7_1")) dir = dirname.Replace("7_1.b", reportid + "R2F.bmp");
            if (dirname.Contains("7_2")) dir = dirname.Replace("7_2.b", reportid + "R2L.bmp");
            if (dirname.Contains("7_3")) dir = dirname.Replace("7_3.b", reportid + "R2R.bmp");

            if (dirname.Contains("8_1")) dir = dirname.Replace("8_1.b", reportid + "R3F.bmp");
            if (dirname.Contains("8_2")) dir = dirname.Replace("8_2.b", reportid + "R3L.bmp");
            if (dirname.Contains("8_3")) dir = dirname.Replace("8_3.b", reportid + "R3R.bmp");

            if (dirname.Contains("9_1")) dir = dirname.Replace("9_1.b", reportid + "R4F.bmp");
            if (dirname.Contains("9_2")) dir = dirname.Replace("9_2.b", reportid + "R4L.bmp");
            if (dirname.Contains("9_3")) dir = dirname.Replace("9_3.b", reportid + "R4R.bmp");

            if (dirname.Contains("10_1")) dir = dirname.Replace("10_1.b", reportid + "R5F.bmp");
            if (dirname.Contains("10_2")) dir = dirname.Replace("10_2.b", reportid + "R5L.bmp");
            if (dirname.Contains("10_3")) dir = dirname.Replace("10_3.b", reportid + "R5R.bmp");

            //Image img = Bitmap.FromFile(dir);
            //imageList.Images.Add(Path.GetFileName(dir), img);
            return dir;
        }

       
        private void LoadFinger(string dirFolderOld, bool istemp)
        {
            try
            {
                using (ICustomerService service = new CustomerService())
                {
                    if (labReportId.Text == "") return;
                    imageList.Images.Clear();

                    if (dirFolderOld == "" && !istemp)
                    {
                        long id = long.Parse(labMa.Text);
                        FICustomer obj = service.Find(id);
                        if (obj != null)
                        {
                            labReportId.Text = obj.ReportID;
                            _oldNameCustomer = obj.Name;
                        }

                        dirFolderOld = string.Empty;
                        if (_oldNameCustomer != null)
                        {
                            dirFolderOld = _dirFile + @"\" + labReportId.Text + "_" +
                                           ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                        }
                    }


                    string dirtemp = ICurrentSessionService.DesDirNameLoadTemp + @"\" + labReportId.Text;
                    try
                    {
                        if (Directory.Exists(dirtemp))
                            Directory.Delete(dirtemp);

                        Directory.CreateDirectory(dirtemp);
                    }
                    catch
                    {

                    }

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if (!f.Name.ToLower().Contains(".dat") &&
                                    !f.Name.ToLower().Contains(labReportId.Text.ToLower() + "f.bmp"))
                                {
                                    if (!istemp)
                                    {
                                        destFile = ConvertToImage(destFile, labReportId.Text);
                                    }
                                    File.Copy(f.FullName, destFile, true);

                                    Image img = Bitmap.FromFile(destFile);
                                    imageList.Images.Add(Path.GetFileName(destFile), img);
                                    //img.Dispose();
                                } 
                            }
                            LoadPtb(labReportId.Text);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }
        private void LoadPtb(string reportid)
        {
            try
            {
                ptbL1F.Image = imageList.Images[reportid + "L1F.bmp"];
                ptbL1L.Image = imageList.Images[reportid + "L1L.bmp"];
                ptbL1R.Image = imageList.Images[reportid + "L1R.bmp"];

                ptbL2F.Image = imageList.Images[reportid + "L2F.bmp"];
                ptbL2L.Image = imageList.Images[reportid + "L2L.bmp"];
                ptbL2R.Image = imageList.Images[reportid + "L2R.bmp"];

                ptbL3F.Image = imageList.Images[reportid + "L3F.bmp"];
                ptbL3L.Image = imageList.Images[reportid + "L3L.bmp"];
                ptbL3R.Image = imageList.Images[reportid + "L3R.bmp"];

                ptbL4F.Image = imageList.Images[reportid + "L4F.bmp"];
                ptbL4L.Image = imageList.Images[reportid + "L4L.bmp"];
                ptbL4R.Image = imageList.Images[reportid + "L4R.bmp"];

                ptbL5F.Image = imageList.Images[reportid + "L5F.bmp"];
                ptbL5L.Image = imageList.Images[reportid + "L5L.bmp"];
                ptbL5R.Image = imageList.Images[reportid + "L5R.bmp"];

                ptbR1F.Image = imageList.Images[reportid + "R1F.bmp"];
                ptbR1L.Image = imageList.Images[reportid + "R1L.bmp"];
                ptbR1R.Image = imageList.Images[reportid + "R1R.bmp"];

                ptbR2F.Image = imageList.Images[reportid + "R2F.bmp"];
                ptbR2L.Image = imageList.Images[reportid + "R2L.bmp"];
                ptbR2R.Image = imageList.Images[reportid + "R2R.bmp"];

                ptbR3F.Image = imageList.Images[reportid + "R3F.bmp"];
                ptbR3L.Image = imageList.Images[reportid + "R3L.bmp"];
                ptbR3R.Image = imageList.Images[reportid + "R3R.bmp"];

                ptbR4F.Image = imageList.Images[reportid + "R4F.bmp"];
                ptbR4L.Image = imageList.Images[reportid + "R4L.bmp"];
                ptbR4R.Image = imageList.Images[reportid + "R4R.bmp"];

                ptbR5F.Image = imageList.Images[reportid + "R5F.bmp"];
                ptbR5L.Image = imageList.Images[reportid + "R5L.bmp"];
                ptbR5R.Image = imageList.Images[reportid + "R5R.bmp"];

                imageList.Images.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void LoadPtb(string dirname, string reportid)
        //{
        //    try
        //    {
            
        //        if (dirname.Contains(reportid + "L1F")) ptbL1F.Load(dirname);
        //        if (dirname.Contains(reportid + "L1L")) ptbL1L.Load(dirname);
        //        if (dirname.Contains(reportid + "L1R")) ptbL1R.Load(dirname);

        //        if (dirname.Contains(reportid + "L2F")) ptbL2F.Load(dirname);
        //        if (dirname.Contains(reportid + "L2L")) ptbL2L.Load(dirname);
        //        if (dirname.Contains(reportid + "L2R")) ptbL2R.Load(dirname);

        //        if (dirname.Contains(reportid + "L3F")) ptbL3F.Load(dirname);
        //        if (dirname.Contains(reportid + "L3L")) ptbL3L.Load(dirname);
        //        if (dirname.Contains(reportid + "L3R")) ptbL3R.Load(dirname);

        //        if (dirname.Contains(reportid + "L4F")) ptbL4F.Load(dirname);
        //        if (dirname.Contains(reportid + "L4L")) ptbL4L.Load(dirname);
        //        if (dirname.Contains(reportid + "L4R")) ptbL4R.Load(dirname);

        //        if (dirname.Contains(reportid + "L5F")) ptbL5F.Load(dirname);
        //        if (dirname.Contains(reportid + "L5L")) ptbL5L.Load(dirname);
        //        if (dirname.Contains(reportid + "L5R")) ptbL5R.Load(dirname);

        //        //

        //        if (dirname.Contains(reportid + "R1F")) ptbR1F.Load(dirname);
        //        if (dirname.Contains(reportid + "R1L")) ptbR1L.Load(dirname);
        //        if (dirname.Contains(reportid + "R1R")) ptbR1R.Load(dirname);

        //        if (dirname.Contains(reportid + "R2F")) ptbR2F.Load(dirname);
        //        if (dirname.Contains(reportid + "R2L")) ptbR2L.Load(dirname);
        //        if (dirname.Contains(reportid + "R2R")) ptbR2R.Load(dirname);

        //        if (dirname.Contains(reportid + "R3F")) ptbR3F.Load(dirname);
        //        if (dirname.Contains(reportid + "R3L")) ptbR3L.Load(dirname);
        //        if (dirname.Contains(reportid + "R3R")) ptbR3R.Load(dirname);

        //        if (dirname.Contains(reportid + "R4F")) ptbR4F.Load(dirname);
        //        if (dirname.Contains(reportid + "R4L")) ptbR4L.Load(dirname);
        //        if (dirname.Contains(reportid + "R4R")) ptbR4R.Load(dirname);

        //        if (dirname.Contains(reportid + "R5F")) ptbR5F.Load(dirname);
        //        if (dirname.Contains(reportid + "R5L")) ptbR5L.Load(dirname);
        //        if (dirname.Contains(reportid + "R5R")) ptbR5R.Load(dirname);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    ICurrentSessionService.VietNamLanguage ? "Bạn có muốn thoát không?" : "Do you want to exit?", @"Notice",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
                if (!_isinsert && CurrCustomer != null && !Issuccess)
                {
                    string nameCustomer = ConvertUtil.ConverToUnsign(CurrCustomer.Name).Replace(" ", "").ToUpper();
                    Insertupdatefile(_dirFile, ICurrentSessionService.DesDirNameTemp, CurrCustomer.ReportID,
                        nameCustomer, "");
                }
                base.Dispose();
            }
        }

        //private Image GetImage(string dir)
        //{
        //    using (var m = new MemoryStream())
        //    {
                
        //       //Save(m,ImageFormat.Bmp);

        //       // var img = Image.FromStream(m);

        //       // //TEST
        //       // img.Save("C:\\test.jpg");
        //       // var bytes = PhotoEditor.ConvertImageToByteArray(img);
        //        return img;
        //    }
        //}
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                bool kq;
                using (ICustomerService service = new CustomerService())
                {
                    _oldNameCustomer = txtTen.Text;
                    string nameCustomer = "";
                    long id = long.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    if (obj != null)
                    {
                        _oldNameCustomer = obj.Name;
                        labReportId.Text = obj.ReportID;
                    }

                    //Them Sua Khach Hang Tai Day
                    FICustomer objCustomer = service.CreateEntity();
                    objCustomer.ID = long.Parse(labMa.Text);
                    objCustomer.AgencyID = ICurrentSessionService.UserId;
                    objCustomer.ReportID = labReportId.Text;
                    objCustomer.Name = txtTen.Text;
                    objCustomer.Address1 = txtDiaChi.Text;
                    objCustomer.City = txtThanhPho.Text;
                    objCustomer.DOB = dtpNgaySinh.Value;
                    objCustomer.Email = txtEmail.Text;
                    objCustomer.Parent = txtTenChaMe.Text;
                    objCustomer.Gender = GetGender();
                    objCustomer.Mobile = txtDiDong.Text;
                    objCustomer.Tel = txtDienThoai.Text;
                    objCustomer.ZipPosTalCode = "";//txtZip.Text;
                    objCustomer.Remark = txtGhiChu.Text;
                    objCustomer.State = "";//txtTinhTrang.Text;

                    nameCustomer = ConvertUtil.ConverToUnsign(objCustomer.Name).Replace(" ", "").ToUpper();
                    if (_oldNameCustomer != null)
                        _oldNameCustomer = ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();

                    if (obj != null)
                    {
                        kq = service.Update(objCustomer);
                        if (kq)
                        {
                            Insertupdatefile(_dirFile, ICurrentSessionService.DesDirNameTemp, objCustomer.ReportID,
                                nameCustomer, _oldNameCustomer);
                            SaveDataFile(ICurrentSessionService.CurAgency, objCustomer, _dirFile);

                            _baseForm.EnglishMsg = "Update information Successfully!";
                            _baseForm.VietNamMsg = "Cập Nhật Thành Công!";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            Issuccess = true;
                            CurrCustomer = objCustomer;
                            _isinsert = false;
                        }
                        else
                        {
                            _baseForm.ShowMessage(IconMessageBox.Information, service.ErrMsg);
                        }
                    }
                    else
                    {
                        kq = service.Add(objCustomer);
                        if (kq)
                        {
                            Insertupdatefile(_dirFile, ICurrentSessionService.DesDirNameTemp, objCustomer.ReportID,
                                nameCustomer, "");
                            SaveDataFile(ICurrentSessionService.CurAgency, objCustomer, _dirFile);

                            _baseForm.EnglishMsg = "Add information Successfully!";
                            _baseForm.VietNamMsg = "Thêm Thành Công!";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            Issuccess = true;
                            CurrCustomer = objCustomer;
                            _isinsert = true;
                        }
                        else
                        {
                            _baseForm.ShowMessage(IconMessageBox.Information, service.ErrMsg);
                        }
                    }
                }

                if(kq)
                    btnClose.PerformClick();
                else
                {
                    if(txtTen.Text=="") txtTen.Select();
                    else if (txtEmail.Text == "") txtEmail.Select();
                    else if(txtDienThoai.Text == "") txtDienThoai.Select();
                    else btnLuu.Select();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }

        private void btnScaner_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_baseForm.CheckDevice(true))
                {
                    return;
                }

                //Hide();
                using (ICustomerService service = new CustomerService())
                {
                    long id = long.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    if (labReportId.Text == "")
                        labReportId.Text = ICurrentSessionService.Username + DateTime.Now.ToString("ddMMyyyyHHmmss");

                    if (obj != null)
                    {
                        labReportId.Text = obj.ReportID;
                        _oldNameCustomer = obj.Name;
                    }

                    string dirFolderOld = string.Empty;
                    if (_oldNameCustomer != null)
                    {
                        dirFolderOld = _dirFile + @"\" + labReportId.Text + "_" +
                                       ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                    }

                    string dirtemp = ICurrentSessionService.DesDirNameTemp + @"\" + labReportId.Text;

                    Directory.CreateDirectory(dirtemp);

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if (!f.Name.ToLower().Contains(".dat") && !f.Name.ToLower().Contains(labReportId.Text.ToLower() + "f.bmp"))
                                    destFile = ConvertToImage(destFile, labReportId.Text);
                                File.Copy(f.FullName, destFile, true);
                                f.Delete();
                            }
                        }
                        d.Delete(true);
                    }

                    var frm = new FrmScan(labReportId.Text, dirtemp);
                    frm.ShowDialog(this);
                   
                    LoadFinger(dirtemp,true);
                }

                Activate();
                //Show();

            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            LoadToUi(CurrCustomer);
            LoadFinger("",false);
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            try
            {
                Hide();
                using (ICustomerService service = new CustomerService())
                {
                    long id = long.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    if(labReportId.Text=="")
                        labReportId.Text = ICurrentSessionService.Username + DateTime.Now.ToString("ddMMyyyyHHmmss");
                    if (obj != null)
                    {
                        labReportId.Text = obj.ReportID;
                        _oldNameCustomer = obj.Name;
                    }

                    string dirFolderOld = string.Empty;
                    if (_oldNameCustomer != null)
                    {
                        dirFolderOld = _dirFile + @"\" + labReportId.Text + "_" +
                                       ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                    }

                    string dirtemp = ICurrentSessionService.DesDirNameTemp + @"\" + labReportId.Text;

                    Directory.CreateDirectory(dirtemp);

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if (!f.Name.ToLower().Contains(".dat") && !f.Name.ToLower().Contains(labReportId.Text.ToLower() + "f.bmp"))
                                    destFile = ConvertToImage(destFile, labReportId.Text);
                                File.Copy(f.FullName, destFile, true);
                                f.Delete();
                            }
                        }
                        d.Delete(true);
                    }

                    var frm = new FrmWebCam(labReportId.Text, dirtemp);
                    frm.ShowDialog(this);
                }

                Activate();
                Show();
            }
            catch
            {
                //
            }
        }

        private void FrmCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (
            //    MessageBox.Show(
            //        ICurrentSessionService.VietNamLanguage ? "Bạn có muốn thoát không?" : "Do you want to exit?", "Note",
            //        MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes && Issuccess==false)
            //{
            //    this.Close();
            //}
            //else
            //{
            //    return;
            //}
            //_baseForm.ShowMessage(IconMessageBox.Question,ICurrentSessionService.VietNamLanguage? "Bạn có muốn thoát không?": "Do you want to exit?");
        }
    }
}
