﻿namespace iTalent.UI.GUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnConfig = new System.Windows.Forms.Button();
            this.pnlSearchBy = new System.Windows.Forms.Panel();
            this.labSearch = new C4FunComponent.Toolkit.C4FunLabel();
            this.rbID = new System.Windows.Forms.RadioButton();
            this.rbName = new System.Windows.Forms.RadioButton();
            this.rbParent = new System.Windows.Forms.RadioButton();
            this.rbTel = new System.Windows.Forms.RadioButton();
            this.rbAddress = new System.Windows.Forms.RadioButton();
            this.labFromDate = new System.Windows.Forms.Label();
            this.labToDate = new System.Windows.Forms.Label();
            this.dtpDenNgay = new System.Windows.Forms.DateTimePicker();
            this.dtpTuNgay = new System.Windows.Forms.DateTimePicker();
            this.btnSearch1 = new System.Windows.Forms.Button();
            this.btnSearch2 = new System.Windows.Forms.Button();
            this.pnlFunction = new C4FunComponent.Toolkit.C4FunPanel();
            this.txtSearch = new C4FunComponent.Toolkit.C4FunTextBox();
            this.colRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZipPostalCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAddress2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMobile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colParent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReportID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAgencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCustomer = new C4FunComponent.Toolkit.C4FunDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlMenu.SuspendLayout();
            this.pnlSearchBy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFunction)).BeginInit();
            this.pnlFunction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(850, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(263, 114);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.pnlMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMenu.Controls.Add(this.btnConfig);
            this.pnlMenu.Controls.Add(this.btnExport);
            this.pnlMenu.Controls.Add(this.btnDelete);
            this.pnlMenu.Controls.Add(this.btnEdit);
            this.pnlMenu.Controls.Add(this.btnAdd);
            this.pnlMenu.Controls.Add(this.btnRefresh);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(850, 53);
            this.pnlMenu.TabIndex = 157;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRefresh.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRefresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnRefresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.White;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRefresh.Location = new System.Drawing.Point(0, 0);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(125, 51);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Tag = " Tải Lại";
            this.btnRefresh.Text = " Refresh";
            this.btnRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRefresh.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.Location = new System.Drawing.Point(125, 0);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(125, 51);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Tag = "Thêm Mới";
            this.btnAdd.Text = "New";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnEdit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.White;
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.Location = new System.Drawing.Point(250, 0);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(0);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(125, 51);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.Tag = "Sửa";
            this.btnEdit.Text = " Edit";
            this.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.Location = new System.Drawing.Point(375, 0);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(125, 51);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Tag = " Xóa";
            this.btnDelete.Text = " Delete";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnExport.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.White;
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExport.Location = new System.Drawing.Point(500, 0);
            this.btnExport.Margin = new System.Windows.Forms.Padding(0);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(125, 51);
            this.btnExport.TabIndex = 10;
            this.btnExport.Tag = " Xuất File";
            this.btnExport.Text = " Export";
            this.btnExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExport.UseVisualStyleBackColor = true;
            // 
            // btnConfig
            // 
            this.btnConfig.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnConfig.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnConfig.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnConfig.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfig.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfig.ForeColor = System.Drawing.Color.White;
            this.btnConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfig.Location = new System.Drawing.Point(625, 0);
            this.btnConfig.Margin = new System.Windows.Forms.Padding(0);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(125, 51);
            this.btnConfig.TabIndex = 11;
            this.btnConfig.Tag = "Tùy Chỉnh";
            this.btnConfig.Text = "Option";
            this.btnConfig.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfig.UseVisualStyleBackColor = true;
            // 
            // pnlSearchBy
            // 
            this.pnlSearchBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.pnlSearchBy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearchBy.Controls.Add(this.btnSearch2);
            this.pnlSearchBy.Controls.Add(this.btnSearch1);
            this.pnlSearchBy.Controls.Add(this.dtpTuNgay);
            this.pnlSearchBy.Controls.Add(this.dtpDenNgay);
            this.pnlSearchBy.Controls.Add(this.labToDate);
            this.pnlSearchBy.Controls.Add(this.labFromDate);
            this.pnlSearchBy.Controls.Add(this.rbAddress);
            this.pnlSearchBy.Controls.Add(this.rbTel);
            this.pnlSearchBy.Controls.Add(this.rbParent);
            this.pnlSearchBy.Controls.Add(this.rbName);
            this.pnlSearchBy.Controls.Add(this.rbID);
            this.pnlSearchBy.Controls.Add(this.labSearch);
            this.pnlSearchBy.Controls.Add(this.txtSearch);
            this.pnlSearchBy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSearchBy.Location = new System.Drawing.Point(0, 53);
            this.pnlSearchBy.Name = "pnlSearchBy";
            this.pnlSearchBy.Size = new System.Drawing.Size(850, 61);
            this.pnlSearchBy.TabIndex = 158;
            // 
            // labSearch
            // 
            this.labSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.labSearch.Location = new System.Drawing.Point(3, 3);
            this.labSearch.Name = "labSearch";
            this.labSearch.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            this.labSearch.Size = new System.Drawing.Size(92, 26);
            this.labSearch.StateNormal.ShortText.Color1 = System.Drawing.Color.White;
            this.labSearch.StateNormal.ShortText.Color2 = System.Drawing.Color.White;
            this.labSearch.StateNormal.ShortText.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSearch.TabIndex = 1;
            this.labSearch.Tag = "Tìm Kiếm:";
            this.labSearch.Values.Text = "Search By:";
            // 
            // rbID
            // 
            this.rbID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rbID.AutoSize = true;
            this.rbID.Checked = true;
            this.rbID.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.rbID.ForeColor = System.Drawing.Color.White;
            this.rbID.Location = new System.Drawing.Point(101, 6);
            this.rbID.Name = "rbID";
            this.rbID.Size = new System.Drawing.Size(38, 21);
            this.rbID.TabIndex = 2;
            this.rbID.TabStop = true;
            this.rbID.Tag = "Mã";
            this.rbID.Text = "ID";
            this.rbID.UseVisualStyleBackColor = true;
            // 
            // rbName
            // 
            this.rbName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rbName.AutoSize = true;
            this.rbName.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.rbName.ForeColor = System.Drawing.Color.White;
            this.rbName.Location = new System.Drawing.Point(145, 6);
            this.rbName.Name = "rbName";
            this.rbName.Size = new System.Drawing.Size(61, 21);
            this.rbName.TabIndex = 3;
            this.rbName.Tag = "Tên";
            this.rbName.Text = "Name";
            this.rbName.UseVisualStyleBackColor = true;
            // 
            // rbParent
            // 
            this.rbParent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rbParent.AutoSize = true;
            this.rbParent.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.rbParent.ForeColor = System.Drawing.Color.White;
            this.rbParent.Location = new System.Drawing.Point(212, 6);
            this.rbParent.Name = "rbParent";
            this.rbParent.Size = new System.Drawing.Size(63, 21);
            this.rbParent.TabIndex = 4;
            this.rbParent.Tag = "Cha/Mẹ";
            this.rbParent.Text = "Parent";
            this.rbParent.UseVisualStyleBackColor = true;
            // 
            // rbTel
            // 
            this.rbTel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rbTel.AutoSize = true;
            this.rbTel.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.rbTel.ForeColor = System.Drawing.Color.White;
            this.rbTel.Location = new System.Drawing.Point(354, 6);
            this.rbTel.Name = "rbTel";
            this.rbTel.Size = new System.Drawing.Size(42, 21);
            this.rbTel.TabIndex = 5;
            this.rbTel.Tag = "Điện Thoại";
            this.rbTel.Text = "Tel";
            this.rbTel.UseVisualStyleBackColor = true;
            // 
            // rbAddress
            // 
            this.rbAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rbAddress.AutoSize = true;
            this.rbAddress.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.rbAddress.ForeColor = System.Drawing.Color.White;
            this.rbAddress.Location = new System.Drawing.Point(281, 6);
            this.rbAddress.Name = "rbAddress";
            this.rbAddress.Size = new System.Drawing.Size(74, 21);
            this.rbAddress.TabIndex = 6;
            this.rbAddress.Tag = "Địa Chỉ";
            this.rbAddress.Text = "Address";
            this.rbAddress.UseVisualStyleBackColor = true;
            // 
            // labFromDate
            // 
            this.labFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labFromDate.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.labFromDate.ForeColor = System.Drawing.Color.White;
            this.labFromDate.Location = new System.Drawing.Point(506, 6);
            this.labFromDate.Name = "labFromDate";
            this.labFromDate.Size = new System.Drawing.Size(103, 21);
            this.labFromDate.TabIndex = 94;
            this.labFromDate.Tag = "Từ Ngày";
            this.labFromDate.Text = "From Date:";
            this.labFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labToDate
            // 
            this.labToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labToDate.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.labToDate.ForeColor = System.Drawing.Color.White;
            this.labToDate.Location = new System.Drawing.Point(509, 32);
            this.labToDate.Name = "labToDate";
            this.labToDate.Size = new System.Drawing.Size(99, 21);
            this.labToDate.TabIndex = 96;
            this.labToDate.Tag = "Đến Ngày";
            this.labToDate.Text = "To Date:";
            this.labToDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpDenNgay
            // 
            this.dtpDenNgay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDenNgay.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpDenNgay.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpDenNgay.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.dtpDenNgay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDenNgay.Location = new System.Drawing.Point(614, 31);
            this.dtpDenNgay.MaxDate = new System.DateTime(5000, 12, 31, 0, 0, 0, 0);
            this.dtpDenNgay.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpDenNgay.Name = "dtpDenNgay";
            this.dtpDenNgay.Size = new System.Drawing.Size(142, 24);
            this.dtpDenNgay.TabIndex = 95;
            this.dtpDenNgay.Value = new System.DateTime(2015, 6, 1, 12, 0, 0, 0);
            // 
            // dtpTuNgay
            // 
            this.dtpTuNgay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpTuNgay.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpTuNgay.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpTuNgay.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.dtpTuNgay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpTuNgay.Location = new System.Drawing.Point(614, 3);
            this.dtpTuNgay.MaxDate = new System.DateTime(5000, 12, 31, 0, 0, 0, 0);
            this.dtpTuNgay.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpTuNgay.Name = "dtpTuNgay";
            this.dtpTuNgay.Size = new System.Drawing.Size(142, 24);
            this.dtpTuNgay.TabIndex = 97;
            this.dtpTuNgay.Value = new System.DateTime(2015, 6, 1, 12, 0, 0, 0);
            // 
            // btnSearch1
            // 
            this.btnSearch1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSearch1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearch1.FlatAppearance.BorderSize = 0;
            this.btnSearch1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnSearch1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnSearch1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch1.ForeColor = System.Drawing.Color.Transparent;
            this.btnSearch1.Image = global::iTalent.UI.Properties.Resources.search;
            this.btnSearch1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch1.Location = new System.Drawing.Point(460, 10);
            this.btnSearch1.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearch1.Name = "btnSearch1";
            this.btnSearch1.Size = new System.Drawing.Size(40, 37);
            this.btnSearch1.TabIndex = 98;
            this.btnSearch1.Tag = "";
            this.btnSearch1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch1.UseVisualStyleBackColor = false;
            // 
            // btnSearch2
            // 
            this.btnSearch2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearch2.FlatAppearance.BorderSize = 0;
            this.btnSearch2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnSearch2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnSearch2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch2.ForeColor = System.Drawing.Color.Transparent;
            this.btnSearch2.Image = global::iTalent.UI.Properties.Resources.search;
            this.btnSearch2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch2.Location = new System.Drawing.Point(771, 10);
            this.btnSearch2.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearch2.Name = "btnSearch2";
            this.btnSearch2.Size = new System.Drawing.Size(40, 37);
            this.btnSearch2.TabIndex = 99;
            this.btnSearch2.Tag = "";
            this.btnSearch2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch2.UseVisualStyleBackColor = false;
            // 
            // pnlFunction
            // 
            this.pnlFunction.Controls.Add(this.pnlSearchBy);
            this.pnlFunction.Controls.Add(this.pnlMenu);
            this.pnlFunction.Controls.Add(this.pictureBox1);
            this.pnlFunction.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFunction.Location = new System.Drawing.Point(0, 0);
            this.pnlFunction.Name = "pnlFunction";
            this.pnlFunction.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            this.pnlFunction.Size = new System.Drawing.Size(1113, 114);
            this.pnlFunction.TabIndex = 154;
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtSearch.Location = new System.Drawing.Point(101, 28);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(341, 31);
            this.txtSearch.StateCommon.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.txtSearch.StateCommon.Border.Rounding = 8;
            this.txtSearch.StateCommon.Content.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtSearch.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.txtSearch.StateNormal.Border.Rounding = 8;
            this.txtSearch.StateNormal.Content.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtSearch.TabIndex = 0;
            // 
            // colRemark
            // 
            this.colRemark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colRemark.DataPropertyName = "Remark";
            this.colRemark.FillWeight = 93.24875F;
            this.colRemark.HeaderText = "Remark";
            this.colRemark.Name = "colRemark";
            // 
            // colCountry
            // 
            this.colCountry.DataPropertyName = "Country";
            this.colCountry.HeaderText = "Country";
            this.colCountry.Name = "colCountry";
            this.colCountry.Visible = false;
            // 
            // colZipPostalCode
            // 
            this.colZipPostalCode.DataPropertyName = "ZipPosTalCode";
            this.colZipPostalCode.HeaderText = "Postal Code";
            this.colZipPostalCode.Name = "colZipPostalCode";
            this.colZipPostalCode.Visible = false;
            // 
            // colState
            // 
            this.colState.DataPropertyName = "State";
            this.colState.HeaderText = "State";
            this.colState.Name = "colState";
            this.colState.Visible = false;
            // 
            // colCity
            // 
            this.colCity.DataPropertyName = "City";
            this.colCity.HeaderText = "City";
            this.colCity.Name = "colCity";
            this.colCity.Visible = false;
            // 
            // colAddress2
            // 
            this.colAddress2.DataPropertyName = "Address2";
            this.colAddress2.HeaderText = "Address 2";
            this.colAddress2.Name = "colAddress2";
            this.colAddress2.Visible = false;
            // 
            // colAddress
            // 
            this.colAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colAddress.DataPropertyName = "Address1";
            this.colAddress.FillWeight = 93.24875F;
            this.colAddress.HeaderText = "Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.Width = 180;
            // 
            // colEmail
            // 
            this.colEmail.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colEmail.DataPropertyName = "Email";
            this.colEmail.FillWeight = 93.24875F;
            this.colEmail.HeaderText = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.Width = 120;
            // 
            // colMobile
            // 
            this.colMobile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colMobile.DataPropertyName = "Mobile";
            this.colMobile.FillWeight = 93.24875F;
            this.colMobile.HeaderText = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.Width = 95;
            // 
            // colTel
            // 
            this.colTel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colTel.DataPropertyName = "Tel";
            this.colTel.FillWeight = 93.24875F;
            this.colTel.HeaderText = "Tel";
            this.colTel.Name = "colTel";
            this.colTel.Width = 90;
            // 
            // colParent
            // 
            this.colParent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colParent.DataPropertyName = "Parent";
            this.colParent.FillWeight = 93.24875F;
            this.colParent.HeaderText = "Parent";
            this.colParent.Name = "colParent";
            this.colParent.Width = 170;
            // 
            // colDOB
            // 
            this.colDOB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colDOB.DataPropertyName = "DOB";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.colDOB.DefaultCellStyle = dataGridViewCellStyle1;
            this.colDOB.FillWeight = 93.24875F;
            this.colDOB.HeaderText = "Birthday";
            this.colDOB.Name = "colDOB";
            this.colDOB.Width = 90;
            // 
            // colGender
            // 
            this.colGender.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colGender.DataPropertyName = "Gender";
            this.colGender.FillWeight = 93.24875F;
            this.colGender.HeaderText = "Sex";
            this.colGender.Name = "colGender";
            this.colGender.Width = 60;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colName.DataPropertyName = "Name";
            this.colName.FillWeight = 130F;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.Width = 170;
            // 
            // colDate
            // 
            this.colDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colDate.DataPropertyName = "Date";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.colDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.colDate.FillWeight = 93.24875F;
            this.colDate.HeaderText = "Date Created";
            this.colDate.Name = "colDate";
            this.colDate.Width = 120;
            // 
            // colReportID
            // 
            this.colReportID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colReportID.DataPropertyName = "ReportID";
            this.colReportID.FillWeight = 93.24875F;
            this.colReportID.HeaderText = "ID";
            this.colReportID.Name = "colReportID";
            this.colReportID.Width = 80;
            // 
            // colAgencyID
            // 
            this.colAgencyID.DataPropertyName = "AgencyID";
            this.colAgencyID.HeaderText = "AgencyID";
            this.colAgencyID.Name = "colAgencyID";
            this.colAgencyID.Visible = false;
            // 
            // colID
            // 
            this.colID.DataPropertyName = "ID";
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.Visible = false;
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.AllowUserToAddRows = false;
            this.dgvCustomer.AllowUserToDeleteRows = false;
            this.dgvCustomer.AllowUserToResizeColumns = false;
            this.dgvCustomer.AllowUserToResizeRows = false;
            this.dgvCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCustomer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvCustomer.ColumnHeadersHeight = 25;
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colAgencyID,
            this.colReportID,
            this.colDate,
            this.colName,
            this.colGender,
            this.colDOB,
            this.colParent,
            this.colTel,
            this.colMobile,
            this.colEmail,
            this.colAddress,
            this.colAddress2,
            this.colCity,
            this.colState,
            this.colZipPostalCode,
            this.colCountry,
            this.colRemark});
            this.dgvCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCustomer.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCustomer.Location = new System.Drawing.Point(0, 0);
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            this.dgvCustomer.RowHeadersVisible = false;
            this.dgvCustomer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomer.Size = new System.Drawing.Size(1113, 680);
            this.dgvCustomer.StateCommon.BackStyle = C4FunComponent.Toolkit.PaletteBackStyle.GridBackgroundList;
            this.dgvCustomer.StateCommon.HeaderColumn.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgvCustomer.StateCommon.HeaderColumn.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgvCustomer.StateCommon.HeaderColumn.Content.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.dgvCustomer.TabIndex = 153;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 680);
            this.Controls.Add(this.pnlFunction);
            this.Controls.Add(this.dgvCustomer);
            this.Name = "FrmMain";
            this.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2010Black;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iTalent Client";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlMenu.ResumeLayout(false);
            this.pnlSearchBy.ResumeLayout(false);
            this.pnlSearchBy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFunction)).EndInit();
            this.pnlFunction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel pnlSearchBy;
        private System.Windows.Forms.Button btnSearch2;
        private System.Windows.Forms.Button btnSearch1;
        private System.Windows.Forms.DateTimePicker dtpTuNgay;
        private System.Windows.Forms.DateTimePicker dtpDenNgay;
        private System.Windows.Forms.Label labToDate;
        private System.Windows.Forms.Label labFromDate;
        private System.Windows.Forms.RadioButton rbAddress;
        private System.Windows.Forms.RadioButton rbTel;
        private System.Windows.Forms.RadioButton rbParent;
        private System.Windows.Forms.RadioButton rbName;
        private System.Windows.Forms.RadioButton rbID;
        private C4FunComponent.Toolkit.C4FunLabel labSearch;
        private C4FunComponent.Toolkit.C4FunTextBox txtSearch;
        private C4FunComponent.Toolkit.C4FunPanel pnlFunction;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRemark;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZipPostalCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colState;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAddress2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMobile;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colParent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReportID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAgencyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private C4FunComponent.Toolkit.C4FunDataGridView dgvCustomer;
    }
}

