﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.UI.GUI
{
    public partial class FrmConfig : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly Config _config;
        private readonly BaseForm _baseForm;

        public FrmConfig()
        {
            InitializeComponent();
            Issuccess = false;
            _config = new Config();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { c4FunHeaderGroup1, labLang, labSaveImage, btnSave, raVietNam, raEnglish };
            _baseForm = new BaseForm();
        }
        private void LoadThongSo()
        {
            txtSaveImages.Text = _config.LocalSaveImages;
            if (_config.IsVietNamLag) raVietNam.Checked = true;
            else raEnglish.Checked = true;
        }
        private void SaveThongSo()
        {
            _config.LocalSaveImages = txtSaveImages.Text;
            _config.IsVietNamLag = raVietNam.Checked;
            _config.Save();

            Environment.Exit(0);
        }

        private void FrmConfig_Load(object sender, EventArgs e)
        {
            LoadThongSo();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveThongSo();
        }

        private void btnChoice_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            if (fd.ShowDialog() == DialogResult.OK)
            {
                txtSaveImages.Text = fd.SelectedPath;
            }
        }

        private void raVietNam_Click(object sender, EventArgs e)
        {
            if (raVietNam.Checked)
            {
                raVietNam.Text = @"Tiếng Việt";
                raEnglish.Text = @"Tiếng Anh";
                labSaveImage.Text = @"Nơi Lưu Vân Tay:";
                labLang.Text = @"Ngôn Ngữ:";
                btnSave.Text = @"Lưu";
            }
            else
            {
                raVietNam.Text = @"Viet Nam";
                raEnglish.Text = @"English";
                labSaveImage.Text = @"Save Images:";
                labLang.Text = @"Language:";
                btnSave.Text = @"Save";
            }
        }

        private void raEnglish_Click(object sender, EventArgs e)
        {
            if (!raVietNam.Checked)
            {
                //raVietNam.Text = raVietNam.Tag.ToString();
                //raEnglish.Text = raEnglish.Tag.ToString();
            }
        }

        private void raVietNam_CheckedChanged(object sender, EventArgs e)
        {
            if (raVietNam.Checked)
            {
                c4FunHeaderGroup1.ValuesPrimary.Heading = @"Tùy Chọn";
                raVietNam.Text = @"Tiếng Việt";
                raEnglish.Text = @"Tiếng Anh";
                labSaveImage.Text = @"Nơi Lưu Vân Tay:";
                labLang.Text = @"Ngôn Ngữ:";
                btnSave.Text = @"Lưu";
            }
            else
            {
                c4FunHeaderGroup1.ValuesPrimary.Heading = @"Option";
                raVietNam.Text = @"Viet Nam";
                raEnglish.Text = @"English";
                labSaveImage.Text = @"Save Images:";
                labLang.Text = @"Language:";
                btnSave.Text = @"Save";
            }
        }

        private void buttonSpecHeaderGroup1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
