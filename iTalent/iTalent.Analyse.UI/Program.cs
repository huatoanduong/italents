﻿using System;
using System.Windows.Forms;
using iTalent.Analyse.UI.GUI;
using iTalent.DAL.Commons;
using iTalent.Utils;

namespace iTalent.Analyse.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (ISetup intallClass = new Setup())
            {
                var kq = intallClass.Intall();
                if (kq)
                {
                    kq = intallClass.ProcessUnikey();
                    if (kq)
                    {
                        SingleApplication.Run(new FrmMain2());
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }
    }
}
