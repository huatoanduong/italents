﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.Office.Interop.Word;
using Wordprocessing = DocumentFormat.OpenXml.Wordprocessing;
using Header = DocumentFormat.OpenXml.Wordprocessing.Header;
using Run = DocumentFormat.OpenXml.Wordprocessing.Run;
using Text = DocumentFormat.OpenXml.Wordprocessing.Text;
using Cell = DocumentFormat.OpenXml.Spreadsheet.Cell;
using Row = DocumentFormat.OpenXml.Spreadsheet.Row;

namespace iTalent.Analyse.Util
{
    public class MSWordUtil
    {
        protected static string getPlainText(Wordprocessing.Paragraph element)
        {
            StringBuilder builder = new StringBuilder();

            foreach (Text text in element.Descendants<Text>())
            {
                builder.Append(text.Text);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Gross all member inside textbox content to first child element
        /// </summary>
        /// <param name="txtContent"></param>
        protected static void mergeTextbox(TextBoxContent txtContent)
        {
            Run firstRun;
            Text firstText;

            foreach (Wordprocessing.Paragraph paragraph in txtContent.Descendants<Wordprocessing.Paragraph>())
            {
                firstRun = paragraph.GetFirstChild<Run>();
                if (firstRun == null)
                {
                    //return;
                    continue;
                }
                firstText = firstRun.GetFirstChild<Text>();
                if (firstText == null)
                {
                    //return;
                    continue;
                }
                string s = "";
                s = getPlainText(paragraph);
                firstText.Text = s;

                firstRun.RemoveAllChildren<Text>();
                firstRun.AppendChild(firstText);

                paragraph.RemoveAllChildren<Run>();
                paragraph.AppendChild(firstRun);
            }
        }
        
        public static void MergeAllTextboxes(string filename)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(filename, true))
            {
                Wordprocessing.Document document = doc.MainDocumentPart.Document;

                foreach (TextBoxContent content in document.Descendants<TextBoxContent>())
                {
                    mergeTextbox(content);
                }
            }
        }

        protected static void mergeHeader(Header header)
        {
            Run firstRun;
            Text firstText;

            foreach (Wordprocessing.Paragraph paragraph in header.Descendants<Wordprocessing.Paragraph>())
            {
                firstRun = paragraph.GetFirstChild<Run>();
                if (firstRun == null)
                {
                    //return;
                    continue;
                }
                firstText = firstRun.GetFirstChild<Text>();
                if (firstText == null)
                {
                    //return;
                    continue;
                }
                string s = "";
                s = getPlainText(paragraph);
                firstText.Text = s;

                firstRun.RemoveAllChildren<Text>();
                firstRun.AppendChild(firstText);

                paragraph.RemoveAllChildren<Run>();
                paragraph.AppendChild(firstRun);
            }
        }

        public static void MergeAllHeaders(string filename)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(filename, true))
            {
                foreach (Header header in doc.MainDocumentPart.HeaderParts.Select(headerPart => headerPart.Header))
                {
                    mergeHeader(header);
                }
            }
        }

        protected static void ReplaceTextbox(string filename, Dictionary<string, string> dictionary)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(filename, true))
            {
                Wordprocessing.Document document = doc.MainDocumentPart.Document;

                foreach (TextBoxContent textBoxContent in document.Descendants<TextBoxContent>())
                {
                    foreach (Wordprocessing.Paragraph paragraph in textBoxContent.Descendants<Wordprocessing.Paragraph>())
                    {
                        //int paragraphCount = textBoxContent.Descendants<Paragraph>().Count();
                        //int runCount = paragraph.Descendants<Run>().Count();
                        foreach (Run run in paragraph.Descendants<Run>())
                        {
                            foreach (DocumentFormat.OpenXml.Wordprocessing.Text text in run.Descendants<Text>())
                            {
                                if (text != null)
                                {
                                    string s = text.Text;

                                    Regex regexText;
                                    string keyReplace;
                                    foreach (KeyValuePair<string, string> keyValuePair in dictionary)
                                    {
                                        keyReplace = "<%" + keyValuePair.Key + "%>";
                                        regexText = new Regex(keyReplace);
                                        s = regexText.Replace(s, keyValuePair.Value);
                                    }
                                    if (!s.Contains("\r\n"))
                                    {
                                        text.Text = s;
                                    }
                                    else
                                    {
                                        Text currentText = text;
                                        Text newText;
                                        //string[] ss = Regex.Split(s, "\r\n");
                                        string[] ss = s.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                                        int sscount = ss.Length;
                                        int i = 0;
                                        currentText.Text = ss[i++];
                                        while (i < sscount)
                                        {
                                            newText = new Text(ss[i++]);
                                            currentText.InsertAfterSelf(newText);
                                            currentText.InsertAfterSelf(new Wordprocessing.Break());
                                            currentText = newText;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected static void ReplaceHeader(string filename, Dictionary<string, string> dictionary)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(filename, true))
            {
                foreach (HeaderPart headerPart in doc.MainDocumentPart.HeaderParts)
                {
                    Header header = headerPart.Header;

                    foreach (Wordprocessing.Paragraph paragraph in header.Descendants<Wordprocessing.Paragraph>())
                    {
                        //int paragraphCount = textBoxContent.Descendants<Paragraph>().Count();
                        //int runCount = paragraph.Descendants<Run>().Count();
                        foreach (Run run in paragraph.Descendants<Run>())
                        {
                            foreach (DocumentFormat.OpenXml.Wordprocessing.Text text in run.Descendants<Text>())
                            {
                                if (text != null)
                                {
                                    string s = text.Text;

                                    Regex regexText;
                                    string keyReplace;
                                    foreach (KeyValuePair<string, string> keyValuePair in dictionary)
                                    {
                                        keyReplace = "<%" + keyValuePair.Key + "%>";
                                        regexText = new Regex(keyReplace);
                                        s = regexText.Replace(s, keyValuePair.Value);
                                    }
                                    text.Text = s;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected static void ReplaceImage(string filename, Dictionary<string, Image> dictionaryImage)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(filename, true))
            {
                MainDocumentPart mainDocumentPart = doc.MainDocumentPart;
                foreach (KeyValuePair<string, Image> keypaiValuePair in dictionaryImage)
                {
                    foreach (SdtElement controlBlock in mainDocumentPart.Document.Body.Descendants<SdtElement>())
                    {
                        Tag tag = controlBlock.SdtProperties.GetFirstChild<Tag>();
                        if (tag != null)
                        {
                            if (tag.Val == keypaiValuePair.Key)
                            {
                                DocumentFormat.OpenXml.Drawing.Blip blip =
                                    controlBlock.Descendants<DocumentFormat.OpenXml.Drawing.Blip>().FirstOrDefault();
                                ImagePart imagePart = mainDocumentPart.AddImagePart(ImagePartType.Jpeg);
                                Image image = keypaiValuePair.Value;
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    image.Save(stream, ImageFormat.Jpeg);
                                    stream.Position = 0;
                                    imagePart.FeedData(stream);
                                }
                                blip.Embed = mainDocumentPart.GetIdOfPart(imagePart);
                            }
                        }
                    }
                }
            }
        }

        #region Update Chart Value

        // Given a worksheet, a column name, and a row index, 
        // gets the cell at the specified column and 
        private static Cell getCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = worksheet.GetFirstChild<SheetData>().
                                Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().FirstOrDefault(c => string.Compare
                (c.CellReference.Value, columnName + rowIndex, true) == 0);
        }

        // Retrieve the value of a cell, given a file name, sheet name, 
        // and address name.
        protected static string getCellValue(WorkbookPart wbPart, Cell theCell)
        {
            string value = null;

            // If the cell does not exist, return an empty string.
            if (theCell != null)
            {
                value = theCell.InnerText;

                // If the cell represents an integer number, you are done. 
                // For dates, this code returns the serialized value that 
                // represents the date. The code handles strings and 
                // Booleans individually. For shared strings, the code 
                // looks up the corresponding value in the shared string 
                // table. For Booleans, the code converts the value into 
                // the words TRUE or FALSE.
                if (theCell.DataType != null)
                {
                    switch (theCell.DataType.Value)
                    {
                        case CellValues.SharedString:

                            // For shared strings, look up the value in the
                            // shared strings table.
                            var stringTable =
                                wbPart.GetPartsOfType<SharedStringTablePart>()
                                      .FirstOrDefault();

                            // If the shared string table is missing, something 
                            // is wrong. Return the index that is in
                            // the cell. Otherwise, look up the correct text in 
                            // the table.
                            if (stringTable != null)
                            {
                                value = stringTable.SharedStringTable
                                                   .ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "0";
                                    break;
                                default:
                                    value = "1";
                                    break;
                            }
                            break;
                    }
                }
            }

            return value;
        }

        protected static string getCellValue(WorkbookPart wbPart, Worksheet worksheet, string columnName, uint rowIndex)
        {
            Cell theCell = getCell(worksheet, columnName, rowIndex);
            return getCellValue(wbPart, theCell);
        }

        protected static void updateChartData(ChartPart chartPart, WorkbookPart wbPart, Worksheet worksheet,
            Dictionary<string, string> dictionary)
        {
            string axisColumn = "A";
            string varValueColumn = "B";
            string varNameColumn = "C";
            Cell valueCell;
            string name;
            string varValue;
            string varName;

            for (uint i = 2; i < 50; i++)
            {
                try
                {
                    /// Update Chart Worksheet Embbeded Data 

                    varValue = "0";
                    name = getCellValue(wbPart, worksheet, axisColumn, i);
                    if (string.IsNullOrWhiteSpace(name))
                    {
                        return;
                    }
                    valueCell = getCell(worksheet, varValueColumn, i);
                    if (valueCell == null)
                    {
                        continue;
                    }

                    varName = getCellValue(wbPart, worksheet, varNameColumn, i);
                    if (!string.IsNullOrWhiteSpace(varName))
                    {
                        varName = varName.Trim().Replace("<%", "").Replace("%>", "");

                        if (dictionary.ContainsKey(varName))
                        {
                            varValue = dictionary[varName];
                        }
                    }

                    valueCell.CellValue = new CellValue(varValue);
                    // We are updating a numeric chart value
                    valueCell.DataType = new EnumValue<CellValues>(CellValues.Number);

                    ///Update Chart Cache Data (interface in Word)
                    ModifyAreaChart(chartPart, varValueColumn, i, varValue, false);
                    ModifyBarChart(chartPart, varValueColumn, i, varValue, false);
                    ModifyBubleChart(chartPart, varValueColumn, i, varValue, false);
                    ModifyLineChart(chartPart, varValueColumn, i, varValue, false);
                    ModifyPieChart(chartPart, varValueColumn, i, varValue, false);
                    ModifyRadarChart(chartPart, varValueColumn, i, varValue, false);
                    ModifyScatterChart(chartPart, varValueColumn, i, varValue, false);
                    ModifySurfaceChart(chartPart, varValueColumn, i, varValue, false);
                }
                catch (Exception exception)
                {
                }
            }

        }

        #endregion


        #region ModifyChartSimplified

        /// <summary>
        /// Updates the Cached Chart data in the Word document file
        /// </summary>
        /// <param name="cellColumn">Corresponds to the Column that needs to be modified</param>
        /// <param name="intRow">Corresponds to the Row that needs to be modified</param>
        /// <param name="cellValue">New value of the pointed cell</param>
        /// <param name="axisValue">Is the new value an Axis Value?</param>
        protected static void ModifyAreaChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                AreaChartSeries bs1 =
                    c_p.ChartSpace.Descendants<AreaChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifyBarChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                BarChartSeries bs1 =
                    c_p.ChartSpace.Descendants<BarChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifyBubleChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                BubbleChartSeries bs1 =
                    c_p.ChartSpace.Descendants<BubbleChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifyLineChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                LineChartSeries bs1 =
                    c_p.ChartSpace.Descendants<LineChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifyPieChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                PieChartSeries bs1 =
                    c_p.ChartSpace.Descendants<PieChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifyRadarChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                RadarChartSeries bs1 =
                    c_p.ChartSpace.Descendants<RadarChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifyScatterChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                ScatterChartSeries bs1 =
                    c_p.ChartSpace.Descendants<ScatterChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        protected static void ModifySurfaceChart(ChartPart c_p, string cellColumn, uint intRow, string cellValue,
            bool axisValue)
        {
            try
            {
                SurfaceChartSeries bs1 =
                    c_p.ChartSpace.Descendants<SurfaceChartSeries>()
                       .First(s => string.Compare(s.InnerText, "Sheet1!$" + cellColumn + "$1", true) > 0);
                if (axisValue)
                {
                    NumericValue nv1 = bs1.Descendants<NumericValue>().First();
                    nv1.Text = cellValue;
                }
                else
                {
                    // 
                    DocumentFormat.OpenXml.Drawing.Charts.Values v1 =
                        bs1.Descendants<DocumentFormat.OpenXml.Drawing.Charts.Values>().FirstOrDefault();
                    NumericPoint np = v1.Descendants<NumericPoint>().ElementAt((int)intRow - 2);
                    NumericValue nv = np.Descendants<NumericValue>().First();
                    nv.Text = cellValue;
                }
            }
            catch
            {
                // Chart Element is not in a recognizable format. Most likely the defined Chart is incorrect. Ignore the chart creation.
                return;
            }
        }

        #endregion

        public static void ReplaceChart(string filename, Dictionary<string, string> dictionary)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(filename, true))
            {
                Wordprocessing.Document document = doc.MainDocumentPart.Document;

                foreach (ChartPart chartPart in doc.MainDocumentPart.ChartParts)
                {
                    ///Update EmbededPackage
                    using (Stream stream = chartPart.EmbeddedPackagePart.GetStream())
                    {
                        // Open the internal spreadsheet doc for the chart
                        using (SpreadsheetDocument wordSSDoc = SpreadsheetDocument.Open(stream, true))
                        {
                            // Navigate to the sheet where the chart data is located
                            WorkbookPart workBookPart = wordSSDoc.WorkbookPart;
                            Sheet theSheet = workBookPart.Workbook.Descendants<Sheet>()
                                                         .FirstOrDefault(s => s.Name == "Sheet1");
                            if (theSheet != null)
                            {
                                //try
                                //{
                                Worksheet ws = ((WorksheetPart)workBookPart.GetPartById(theSheet.Id)).Worksheet;

                                updateChartData(chartPart, workBookPart, ws, dictionary);

                                //ModifyChartDetailed(cellColumn, intRow, newValue, axisValue);
                                //ModifyChartSimplified(cellColumn, intRow, newValue, axisValue);

                                ws.Save();
                                //}
                                //catch (Exception exception)
                                //{

                                //}
                            }
                        }
                    }


                }

                ///Update cache reference

            }
        }

        /// <summary>
        /// Merge all text in textboxes into 1 line, for searching & replacing
        /// </summary>
        /// <param name="filename"></param>
        public static void MergeFile(string filename)
        {
            MergeAllHeaders(filename);
            MergeAllTextboxes(filename);
        }

        /// <summary>
        /// Replace Headers, Replace Textboxes, Replace Charts
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="dictionary"></param>
        public static void ReplaceAllText(string filename, Dictionary<string, string> dictionary, Dictionary<string, Image> dictionaryImage)
        {
            ReplaceHeader(filename, dictionary);
            ReplaceTextbox(filename, dictionary);
            ReplaceChart(filename, dictionary);
            ReplaceImage(filename, dictionaryImage);
        }

        public static bool ExportPDF2(string filedocx, string filePDF)
        {
            bool result = false;
            Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document wordDocument = null;

            try
            {
                wordDocument = appWord.Documents.Open(filedocx);
                if (wordDocument != null)
                {
                    wordDocument.Activate();
                    object filePdfName = filePDF;
                    wordDocument.SaveAs(filePdfName, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (wordDocument != null)
                {
                    object oMissing = System.Reflection.Missing.Value;
                    object saveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                (  (_Document)  wordDocument).Close(ref saveChanges, ref oMissing, ref oMissing);
                }

                if (appWord != null)
                {
                    appWord.Quit();
                }

                GC.Collect();
                GC.SuppressFinalize(wordDocument);
                GC.SuppressFinalize(appWord);

                Marshal.FinalReleaseComObject(appWord);
            }
            return result;
        }
    
    }
}