using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Analyse.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIAgencyRepository
    {
        int Delete(FIAgency obj);
        int Insert(FIAgency obj);
        int Update(FIAgency obj);
        List<FIAgency> FindListByQuery(string query);
        FIAgency FindByKey(Int32 ID);
        List<FIAgency> FindAll();
        List<FIAgency> FindByCondition(FIAgencyConditionForm condt);
        int DeleteByCondition(FIAgencyConditionForm condt);
    }

    /// <summary>
    /// Summary description for FIAgency.
    /// </summary>
    public class FIAgencyRepository : Repository, IFIAgencyRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FIAgencyRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = "SELECT ID, " +
                    "Username,  " +
                    "Password,  " +
                    "Name,  " +
                    "Point,  " +
                    "Gender,  " +
                    "DOB,  " +
                    "Address1,  " +
                    "Address2,  " +
                    "City,  " +
                    "State,  " +
                    "PostalCode,  " +
                    "Country,  " +
                    "PhoneNo,  " +
                    "MobileNo,  " +
                    "Email,  " +
                    "PassEmail,  " +
                    "SaveImages,  " +
                    "LastActiveDate,  " +
                    "Day_Actived,  " +
                    "Dis_Activated,  " +
                    "ProductKey,  " +
                    "SerailKey,  " +
                    "SecKey FROM FIAgency WHERE (ID = @ID) ";
            _SQLSelectByKey = query;

            query = "UPDATE FIAgency SET " +
                    "Username = @Username, " +
                    "Password = @Password, " +
                    "[Name] = @Name, " +
                    "[Point] = @Point, " +
                    "[Gender] = @Gender, " +
                    "[DOB] = @DOB, " +
                    "[Address1] = @Address1, " +
                    "[Address2] = @Address2, " +
                    "[City] = @City, " +
                    "[State] = @State, " +
                    "[PostalCode] = @PostalCode, " +
                    "[Country] = @Country, " +
                    "[PhoneNo] = @PhoneNo, " +
                    "[MobileNo] = @MobileNo, " +
                    "[Email] = @Email, " +
                    "[PassEmail] = @PassEmail, " +
                    "[SaveImages] = @SaveImages, " +
                    "[LastActiveDate] = @LastActiveDate, " +
                    "[Day_Actived] = @Day_Actived, " +
                    "[Dis_Activated] = @Dis_Activated, " +
                    "[ProductKey] = @ProductKey, " +
                    "[SerailKey] = @SerailKey, " +
                    "[SecKey] = @SecKey" +
                    "WHERE ([ID] = @ID)";
            _SQLUpdate = query;

            query = "INSERT INTO FIAgency ( " +
                    "[Username], " +
                    "[Password], " +
                    "[Name], " +
                    "[Point], " +
                    "[Gender], " +
                    "[DOB], " +
                    "[Address1], " +
                    "[Address2], " +
                    "[City], " +
                    "[State], " +
                    "[PostalCode], " +
                    "[Country], " +
                    "[PhoneNo], " +
                    "[MobileNo], " +
                    "[Email], " +
                    "[PassEmail], " +
                    "[SaveImages], " +
                    "[LastActiveDate], " +
                    "[Day_Actived], " +
                    "[Dis_Activated], " +
                    "[ProductKey], " +
                    "[SerailKey], " +
                    "[SecKey] " +
                    ") VALUES (@Username,  " +
                    "@Password,  " +
                    "@Name,  " +
                    "@Point,  " +
                    "@Gender,  " +
                    "@DOB,  " +
                    "@Address1,  " +
                    "@Address2,  " +
                    "@City,  " +
                    "@State,  " +
                    "@PostalCode,  " +
                    "@Country,  " +
                    "@PhoneNo,  " +
                    "@MobileNo,  " +
                    "@Email,  " +
                    "@PassEmail,  " +
                    "@SaveImages,  " +
                    "@LastActiveDate,  " +
                    "@Day_Actived,  " +
                    "@Dis_Activated,  " +
                    "@ProductKey,  " +
                    "@SerailKey,  " +
                    "@SecKey)";
            _SQLInsert = query;

            query = @"DELETE FROM FIAgency WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT
            [ID],
            [Username],
            [Password],
            [Name],
            [Point],
            [Gender],
            [DOB],
            [Address1],
            [Address2],
            [City],
            [State],
            [PostalCode],
            [Country],
            [PhoneNo],
            [MobileNo],
            [Email],
            [PassEmail],
            [SaveImages],
            [LastActiveDate],
            [Day_Actived],
            [Dis_Activated],
            [ProductKey],
            [SerailKey],
            [SecKey]

		FROM 
			FIAgency";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FIAgency obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FIAgency obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIAgency obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FIAgency> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        protected List<FIAgency> FindListByCommand(IDbCommand command)
        {
            List<FIAgency> list = new List<FIAgency>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FIAgency obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FIAgency();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FIAgency obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["Username"] is DBNull))
                {
                    obj.Username = (string) reader["Username"];
                }
                if (!(reader["Password"] is DBNull))
                {
                    obj.Password = (string) reader["Password"];
                }
                if (!(reader["Name"] is DBNull))
                {
                    obj.Name = (string) reader["Name"];
                }
                if (!(reader["Point"] is DBNull))
                {
                    obj.Point = (Int16) reader["Point"];
                }
                if (!(reader["Gender"] is DBNull))
                {
                    obj.Gender = (string) reader["Gender"];
                }
                if (!(reader["DOB"] is DBNull))
                {
                    obj.DOB = (DateTime) reader["DOB"];
                }
                if (!(reader["Address1"] is DBNull))
                {
                    obj.Address1 = (string) reader["Address1"];
                }
                if (!(reader["Address2"] is DBNull))
                {
                    obj.Address2 = (string) reader["Address2"];
                }
                if (!(reader["City"] is DBNull))
                {
                    obj.City = (string) reader["City"];
                }
                if (!(reader["State"] is DBNull))
                {
                    obj.State = (string) reader["State"];
                }
                if (!(reader["PostalCode"] is DBNull))
                {
                    obj.PostalCode = (string) reader["PostalCode"];
                }
                if (!(reader["Country"] is DBNull))
                {
                    obj.Country = (string) reader["Country"];
                }
                if (!(reader["PhoneNo"] is DBNull))
                {
                    obj.PhoneNo = (string) reader["PhoneNo"];
                }
                if (!(reader["MobileNo"] is DBNull))
                {
                    obj.MobileNo = (string) reader["MobileNo"];
                }
                if (!(reader["Email"] is DBNull))
                {
                    obj.Email = (string) reader["Email"];
                }
                if (!(reader["PassEmail"] is DBNull))
                {
                    obj.PassEmail = (string) reader["PassEmail"];
                }
                if (!(reader["SaveImages"] is DBNull))
                {
                    obj.SaveImages = (string) reader["SaveImages"];
                }
                if (!(reader["LastActiveDate"] is DBNull))
                {
                    obj.LastActiveDate = (DateTime) reader["LastActiveDate"];
                }
                if (!(reader["Day_Actived"] is DBNull))
                {
                    obj.Day_Actived = (Int16) reader["Day_Actived"];
                }
                if (!(reader["Dis_Activated"] is DBNull))
                {
                    obj.Dis_Activated = (string) reader["Dis_Activated"];
                }
                if (!(reader["ProductKey"] is DBNull))
                {
                    obj.ProductKey = (string) reader["ProductKey"];
                }
                if (!(reader["SerailKey"] is DBNull))
                {
                    obj.SerailKey = (string) reader["SerailKey"];
                }
                if (!(reader["SecKey"] is DBNull))
                {
                    obj.SecKey = (string) reader["SecKey"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FIAgency FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FIAgency> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIAgency> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FIAgency> list = FindListByCommand(command);
            return list;
        }

        public List<FIAgency> FindByCondition(FIAgencyConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIAgency " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIAgency> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIAgencyConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIAgency " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIAgencyConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIAgencyConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "Username", obj.Username, DbType.String);
            DalTools.AddDbDataParameter(command, "Password", obj.Password, DbType.String);
            DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
            DalTools.AddDbDataParameter(command, "Point", obj.Point, DbType.Int16);
            DalTools.AddDbDataParameter(command, "Gender", obj.Gender, DbType.String);
            DalTools.AddDbDataParameter(command, "DOB", obj.DOB, DbType.DateTime);
            DalTools.AddDbDataParameter(command, "Address1", obj.Address1, DbType.String);
            DalTools.AddDbDataParameter(command, "Address2", obj.Address2, DbType.String);
            DalTools.AddDbDataParameter(command, "City", obj.City, DbType.String);
            DalTools.AddDbDataParameter(command, "State", obj.State, DbType.String);
            DalTools.AddDbDataParameter(command, "PostalCode", obj.PostalCode, DbType.String);
            DalTools.AddDbDataParameter(command, "Country", obj.Country, DbType.String);
            DalTools.AddDbDataParameter(command, "PhoneNo", obj.PhoneNo, DbType.String);
            DalTools.AddDbDataParameter(command, "MobileNo", obj.MobileNo, DbType.String);
            DalTools.AddDbDataParameter(command, "Email", obj.Email, DbType.String);
            DalTools.AddDbDataParameter(command, "PassEmail", obj.PassEmail, DbType.String);
            DalTools.AddDbDataParameter(command, "SaveImages", obj.SaveImages, DbType.String);
            DalTools.AddDbDataParameter(command, "LastActiveDate", obj.LastActiveDate, DbType.DateTime);
            DalTools.AddDbDataParameter(command, "Day_Actived", obj.Day_Actived, DbType.Int16);
            DalTools.AddDbDataParameter(command, "Dis_Activated", obj.Dis_Activated, DbType.String);
            DalTools.AddDbDataParameter(command, "ProductKey", obj.ProductKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SerailKey", obj.SerailKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SecKey", obj.SecKey, DbType.String);
        }


        protected void FillParamToCommand(IDbCommand command, FIAgency obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "Username", obj.Username, DbType.String);
            DalTools.AddDbDataParameter(command, "Password", obj.Password, DbType.String);
            DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
            DalTools.AddDbDataParameter(command, "Point", obj.Point, DbType.Int16);
            DalTools.AddDbDataParameter(command, "Gender", obj.Gender, DbType.String);
            DalTools.AddDbDataParameter(command, "DOB", obj.DOB, DbType.DateTime);
            DalTools.AddDbDataParameter(command, "Address1", obj.Address1, DbType.String);
            DalTools.AddDbDataParameter(command, "Address2", obj.Address2, DbType.String);
            DalTools.AddDbDataParameter(command, "City", obj.City, DbType.String);
            DalTools.AddDbDataParameter(command, "State", obj.State, DbType.String);
            DalTools.AddDbDataParameter(command, "PostalCode", obj.PostalCode, DbType.String);
            DalTools.AddDbDataParameter(command, "Country", obj.Country, DbType.String);
            DalTools.AddDbDataParameter(command, "PhoneNo", obj.PhoneNo, DbType.String);
            DalTools.AddDbDataParameter(command, "MobileNo", obj.MobileNo, DbType.String);
            DalTools.AddDbDataParameter(command, "Email", obj.Email, DbType.String);
            DalTools.AddDbDataParameter(command, "PassEmail", obj.PassEmail, DbType.String);
            DalTools.AddDbDataParameter(command, "SaveImages", obj.SaveImages, DbType.String);
            DalTools.AddDbDataParameter(command, "LastActiveDate", obj.LastActiveDate, DbType.DateTime);
            DalTools.AddDbDataParameter(command, "Day_Actived", obj.Day_Actived, DbType.Int16);
            DalTools.AddDbDataParameter(command, "Dis_Activated", obj.Dis_Activated, DbType.String);
            DalTools.AddDbDataParameter(command, "ProductKey", obj.ProductKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SerailKey", obj.SerailKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SecKey", obj.SecKey, DbType.String);
        }

        #endregion
    }
}