using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Analyse.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFICustomerRepository
    {
        int Delete(FICustomer obj);
        int Insert(FICustomer obj);
        int Update(FICustomer obj);
        List<FICustomer> FindListByQuery(string query);
        FICustomer FindByKey(Int32 ID);
        List<FICustomer> FindAll();
        List<FICustomer> FindByCondition(FICustomerConditionForm condt);
        int DeleteByCondition(FICustomerConditionForm condt);
    }

	/// <summary>
	/// Summary description for FICustomer.
	/// </summary>
	public class FICustomerRepository : Repository, IFICustomerRepository
	{
        #region Properties

        #endregion
        
        #region Constructor

        public FICustomerRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;	
			this.DbTransaction = unitOfWorkAsync.DbTransaction;			
			InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery
	
	protected override void InitSqlQuery()
	{
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			Date, 
			Name, 
			Gender, 
			DOB, 
			Parent, 
			Tel, 
			Mobile, 
			Email, 
			Address1, 
			Address2, 
			City, 
			State, 
			ZipPosTalCode, 
			Country, 
			Remark FROM FICustomer WHERE (ID = @ID)";
	    _SQLSelectByKey = query;

            query = @"UPDATE FICustomer SET AgencyID = @AgencyID, 
			ReportID = @ReportID, 
			Date = @Date, 
			Name = @Name, 
			Gender = @Gender, 
			DOB = @DOB, 
			Parent = @Parent, 
			Tel = @Tel, 
			Mobile = @Mobile, 
			Email = @Email, 
			Address1 = @Address1, 
			Address2 = @Address2, 
			City = @City, 
			State = @State, 
			ZipPosTalCode = @ZipPosTalCode, 
			Country = @Country, 
			Remark = @Remark WHERE (ID = @ID)";
	    _SQLUpdate      = query;

            query = @"INSERT INTO FICustomer (
            [AgencyID],
            [ReportID],
            [Date],
            [Name],
            [Gender],
            [DOB],
            [Parent],
            [Tel],
            [Mobile],
            [Email],
            [Address1],
            [Address2],
            [City],
            [State],
            [ZipPosTalCode],
            [Country],
            [Remark]
            ) VALUES (
            @AgencyID, 
			@ReportID, 
			@Date, 
			@Name, 
			@Gender, 
			@DOB, 
			@Parent, 
			@Tel, 
			@Mobile, 
			@Email, 
			@Address1, 
			@Address2, 
			@City, 
			@State, 
			@ZipPosTalCode, 
			@Country, 
			@Remark)";
	    _SQLInsert      = query;

            query = @"DELETE FROM FICustomer WHERE (ID = @ID)";
	    _SQLDelete      = query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			Date, 
			Name, 
			Gender, 
			DOB, 
			Parent, 
			Tel, 
			Mobile, 
			Email, 
			Address1, 
			Address2, 
			City, 
			State, 
			ZipPosTalCode, 
			Country, 
			Remark 
		FROM 
			FICustomer";
	    _SQLSelectAll   = query;
	}

        #endregion

        #region Save & Delete

        public int Insert(FICustomer obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Update(FICustomer obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FICustomer obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
				IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
				
                return res;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Command Execute & Read Properties

		public List<FICustomer> FindListByQuery(string query)
		{
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
			return FindListByCommand(command);
		}
		
        protected List<FICustomer> FindListByCommand(IDbCommand command)
        {
            List<FICustomer> list = new List<FICustomer>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {	
				FICustomer obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    	while (reader.Read()) 
						{ 
							obj = new FICustomer();
							ReadProperties(obj, reader);
							list.Add(obj); 
						}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex) { throw ex; }
        }

      private void ReadProperties(FICustomer obj, IDataReader reader)
      {
         try
         {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (decimal)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (string)reader["ReportID"];
		}
                if(!(reader["Date"] is DBNull))
		{
			obj.Date = (DateTime)reader["Date"];
		}
                if(!(reader["Name"] is DBNull))
		{
			obj.Name = (string)reader["Name"];
		}
                if(!(reader["Gender"] is DBNull))
		{
			obj.Gender = (string)reader["Gender"];
		}
                if(!(reader["DOB"] is DBNull))
		{
			obj.DOB = (DateTime)reader["DOB"];
		}
                if(!(reader["Parent"] is DBNull))
		{
			obj.Parent = (string)reader["Parent"];
		}
                if(!(reader["Tel"] is DBNull))
		{
			obj.Tel = (string)reader["Tel"];
		}
                if(!(reader["Mobile"] is DBNull))
		{
			obj.Mobile = (string)reader["Mobile"];
		}
                if(!(reader["Email"] is DBNull))
		{
			obj.Email = (string)reader["Email"];
		}
                if(!(reader["Address1"] is DBNull))
		{
			obj.Address1 = (string)reader["Address1"];
		}
                if(!(reader["Address2"] is DBNull))
		{
			obj.Address2 = (string)reader["Address2"];
		}
                if(!(reader["City"] is DBNull))
		{
			obj.City = (string)reader["City"];
		}
                if(!(reader["State"] is DBNull))
		{
			obj.State = (string)reader["State"];
		}
                if(!(reader["ZipPosTalCode"] is DBNull))
		{
			obj.ZipPosTalCode = (string)reader["ZipPosTalCode"];
		}
                if(!(reader["Country"] is DBNull))
		{
			obj.Country = (string)reader["Country"];
		}
                if(!(reader["Remark"] is DBNull))
		{
			obj.Remark = (string)reader["Remark"];
		}
         }
         catch (Exception ex)
         {
            //throw new DalException("Failed to read properties from DataReader.", ex);
            //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
		throw ex;
         }
      }

        #endregion
      
	public FICustomer FindByKey(Int32 ID)
	{
		IDbCommand command  = DbConnection.CreateCommand();
		command.CommandText = _SQLSelectByKey + ";";
         DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
		List<FICustomer> list =  FindListByCommand(command);

		if (list.Count == 0)
		{
			//throw new Exception("No data was returned"); 
                	return null;
            	}
            	return list[0];
	}

		public List<FICustomer> FindAll()
		{
			IDbCommand command  = DbConnection.CreateCommand();
			command.CommandText = _SQLSelectAll + ";";
			List<FICustomer> list =  FindListByCommand(command);
			return list;
		}

        public List<FICustomer> FindByCondition(FICustomerConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FICustomer " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FICustomer> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FICustomerConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FICustomer " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FICustomerConditionForm condt)
        {
			return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FICustomerConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
         DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
         DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.String);
         DalTools.AddDbDataParameter(command, "Date", obj.Date, DbType.DateTime);
         DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
         DalTools.AddDbDataParameter(command, "Gender", obj.Gender, DbType.String);
         DalTools.AddDbDataParameter(command, "DOB", obj.DOB, DbType.DateTime);
         DalTools.AddDbDataParameter(command, "Parent", obj.Parent, DbType.String);
         DalTools.AddDbDataParameter(command, "Tel", obj.Tel, DbType.String);
         DalTools.AddDbDataParameter(command, "Mobile", obj.Mobile, DbType.String);
         DalTools.AddDbDataParameter(command, "Email", obj.Email, DbType.String);
         DalTools.AddDbDataParameter(command, "Address1", obj.Address1, DbType.String);
         DalTools.AddDbDataParameter(command, "Address2", obj.Address2, DbType.String);
         DalTools.AddDbDataParameter(command, "City", obj.City, DbType.String);
         DalTools.AddDbDataParameter(command, "State", obj.State, DbType.String);
         DalTools.AddDbDataParameter(command, "ZipPosTalCode", obj.ZipPosTalCode, DbType.String);
         DalTools.AddDbDataParameter(command, "Country", obj.Country, DbType.String);
         DalTools.AddDbDataParameter(command, "Remark", obj.Remark, DbType.String);
        }


        protected void FillParamToCommand(IDbCommand command, FICustomer obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
         DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
         DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.String);
         DalTools.AddDbDataParameter(command, "Date", obj.Date, DbType.DateTime);
         DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
         DalTools.AddDbDataParameter(command, "Gender", obj.Gender, DbType.String);
         DalTools.AddDbDataParameter(command, "DOB", obj.DOB, DbType.DateTime);
         DalTools.AddDbDataParameter(command, "Parent", obj.Parent, DbType.String);
         DalTools.AddDbDataParameter(command, "Tel", obj.Tel, DbType.String);
         DalTools.AddDbDataParameter(command, "Mobile", obj.Mobile, DbType.String);
         DalTools.AddDbDataParameter(command, "Email", obj.Email, DbType.String);
         DalTools.AddDbDataParameter(command, "Address1", obj.Address1, DbType.String);
         DalTools.AddDbDataParameter(command, "Address2", obj.Address2, DbType.String);
         DalTools.AddDbDataParameter(command, "City", obj.City, DbType.String);
         DalTools.AddDbDataParameter(command, "State", obj.State, DbType.String);
         DalTools.AddDbDataParameter(command, "ZipPosTalCode", obj.ZipPosTalCode, DbType.String);
         DalTools.AddDbDataParameter(command, "Country", obj.Country, DbType.String);
         DalTools.AddDbDataParameter(command, "Remark", obj.Remark, DbType.String);
        }

        #endregion
   }
}