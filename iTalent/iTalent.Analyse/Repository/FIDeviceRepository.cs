using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Analyse.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIDeviceRepository
    {
        int Delete(FIDevice obj);
        int Insert(FIDevice obj);
        int Update(FIDevice obj);
        List<FIDevice> FindListByQuery(string query);
        FIDevice FindByKey(Int32 ID);
        List<FIDevice> FindAll();
        List<FIDevice> FindByCondition(FIDeviceConditionForm condt);
        int DeleteByCondition(FIDeviceConditionForm condt);
    }

    /// <summary>
    /// Summary description for FIDevice.
    /// </summary>
    public class FIDeviceRepository : Repository, IFIDeviceRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FIDeviceRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			Devicename, 
			ActiveDate, 
			ProductKey, 
			SerailKey, 
			SecKey, 
			Max, 
			Min FROM FIDevice WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FIDevice SET AgencyID = @AgencyID, 
			Devicename = @Devicename, 
			ActiveDate = @ActiveDate, 
			ProductKey = @ProductKey, 
			SerailKey = @SerailKey, 
			SecKey = @SecKey, 
			Max = @Max, 
			Min = @Min WHERE (ID = @ID)";
            _SQLUpdate = query;

            query = @"INSERT INTO FIDevice (
            [AgencyID], 
            [Devicename], 
            [ActiveDate], 
            [ProductKey], 
            [SerailKey], 
            [SecKey], 
            [Max], 
            [Min]
            ) VALUES (
            @AgencyID, 
			@Devicename, 
			@ActiveDate, 
			@ProductKey, 
			@SerailKey, 
			@SecKey, 
			@Max, 
			@Min)";
            _SQLInsert = query;

            query = @"DELETE FROM FIDevice WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			Devicename, 
			ActiveDate, 
			ProductKey, 
			SerailKey, 
			SecKey, 
			Max, 
			Min 
		FROM 
			FIDevice";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FIDevice obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FIDevice obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIDevice obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FIDevice> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        protected List<FIDevice> FindListByCommand(IDbCommand command)
        {
            List<FIDevice> list = new List<FIDevice>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FIDevice obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FIDevice();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FIDevice obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (decimal) reader["AgencyID"];
                }
                if (!(reader["Devicename"] is DBNull))
                {
                    obj.Devicename = (string) reader["Devicename"];
                }
                if (!(reader["ActiveDate"] is DBNull))
                {
                    obj.ActiveDate = (DateTime) reader["ActiveDate"];
                }
                if (!(reader["ProductKey"] is DBNull))
                {
                    obj.ProductKey = (string) reader["ProductKey"];
                }
                if (!(reader["SerailKey"] is DBNull))
                {
                    obj.SerailKey = (string) reader["SerailKey"];
                }
                if (!(reader["SecKey"] is DBNull))
                {
                    obj.SecKey = (string) reader["SecKey"];
                }
                if (!(reader["Max"] is DBNull))
                {
                    obj.Max = (string) reader["Max"];
                }
                if (!(reader["Min"] is DBNull))
                {
                    obj.Min = (string) reader["Min"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FIDevice FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FIDevice> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIDevice> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FIDevice> list = FindListByCommand(command);
            return list;
        }

        public List<FIDevice> FindByCondition(FIDeviceConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIDevice " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIDevice> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIDeviceConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIDevice " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIDeviceConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIDeviceConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Devicename", obj.Devicename, DbType.String);
            DalTools.AddDbDataParameter(command, "ActiveDate", obj.ActiveDate, DbType.DateTime);
            DalTools.AddDbDataParameter(command, "ProductKey", obj.ProductKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SerailKey", obj.SerailKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SecKey", obj.SecKey, DbType.String);
            DalTools.AddDbDataParameter(command, "Max", obj.Max, DbType.String);
            DalTools.AddDbDataParameter(command, "Min", obj.Min, DbType.String);
        }


        protected void FillParamToCommand(IDbCommand command, FIDevice obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Devicename", obj.Devicename, DbType.String);
            DalTools.AddDbDataParameter(command, "ActiveDate", obj.ActiveDate, DbType.DateTime);
            DalTools.AddDbDataParameter(command, "ProductKey", obj.ProductKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SerailKey", obj.SerailKey, DbType.String);
            DalTools.AddDbDataParameter(command, "SecKey", obj.SecKey, DbType.String);
            DalTools.AddDbDataParameter(command, "Max", obj.Max, DbType.String);
            DalTools.AddDbDataParameter(command, "Min", obj.Min, DbType.String);
        }

        #endregion
    }
}