using System;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIPrintRecord.
	/// </summary>
	public class FIPrintRecordConditionForm
	{

      #region Fields

      private Int32 _ID;
      private decimal _AgencyID;
      private decimal _TemplateID;
      private decimal _ReportID;
      private DateTime _PrintDate;
      private decimal _PointID;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public decimal AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetTemplateID;
	public bool IsTemplateIDNullable
      { get { return true;  } }
      public decimal TemplateID
      {
         get { return _TemplateID;  }
         set { 
		_TemplateID = value; 
		IsSetTemplateID = true;
		}
      }
	public bool IsSetReportID;
	public bool IsReportIDNullable
      { get { return true;  } }
      public decimal ReportID
      {
         get { return _ReportID;  }
         set { 
		_ReportID = value; 
		IsSetReportID = true;
		}
      }
	public bool IsSetPrintDate;
	public bool IsPrintDateNullable
      { get { return true;  } }
      public DateTime PrintDate
      {
         get { return _PrintDate;  }
         set { 
		_PrintDate = value; 
		IsSetPrintDate = true;
		}
      }
	public bool IsSetPointID;
	public bool IsPointIDNullable
      { get { return true;  } }
      public decimal PointID
      {
         get { return _PointID;  }
         set { 
		_PointID = value; 
		IsSetPointID = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIPrintRecordConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_decimal;
	IsSetAgencyID = false;
         _TemplateID = EmptyValues.v_decimal;
	IsSetTemplateID = false;
         _ReportID = EmptyValues.v_decimal;
	IsSetReportID = false;
         _PrintDate = EmptyValues.v_DateTime;
	IsSetPrintDate = false;
         _PointID = EmptyValues.v_decimal;
	IsSetPointID = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetTemplateID)
            {
                s += " AND TemplateID = @TemplateID ";
            }
            if (IsSetReportID)
            {
                s += " AND ReportID = @ReportID ";
            }
            if (IsSetPrintDate)
            {
                s += " AND PrintDate = @PrintDate ";
            }
            if (IsSetPointID)
            {
                s += " AND PointID = @PointID ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}