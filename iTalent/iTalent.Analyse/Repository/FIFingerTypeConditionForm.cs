using System;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIFingerType.
	/// </summary>
	public class FIFingerTypeConditionForm
	{

      #region Fields

      private Int32 _ID;
      private string _Name;
      private string _Types;
      private string _Remark;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetName;
	public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { 
		_Name = value; 
		IsSetName = true;
		}
      }
	public bool IsSetTypes;
	public bool IsTypesNullable
      { get { return true;  } }
      public string Types
      {
         get { return _Types;  }
         set { 
		_Types = value; 
		IsSetTypes = true;
		}
      }
	public bool IsSetRemark;
	public bool IsRemarkNullable
      { get { return true;  } }
      public string Remark
      {
         get { return _Remark;  }
         set { 
		_Remark = value; 
		IsSetRemark = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIFingerTypeConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _Name = EmptyValues.v_string;
	IsSetName = false;
         _Types = EmptyValues.v_string;
	IsSetTypes = false;
         _Remark = EmptyValues.v_string;
	IsSetRemark = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetName)
            {
                s += " AND Name = @Name ";
            }
            if (IsSetTypes)
            {
                s += " AND Types = @Types ";
            }
            if (IsSetRemark)
            {
                s += " AND Remark = @Remark ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}