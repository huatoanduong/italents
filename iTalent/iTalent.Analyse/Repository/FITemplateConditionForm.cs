using System;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FITemplate.
	/// </summary>
	public class FITemplateConditionForm
	{

      #region Fields

      private Int32 _ID;
      private decimal _AgencyID;
      private string _Name;
      private string _TemplatePermit;
      private decimal _TemplatePoint;
      private string _TemplateFileName;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public decimal AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetName;
	public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { 
		_Name = value; 
		IsSetName = true;
		}
      }
	public bool IsSetTemplatePermit;
	public bool IsTemplatePermitNullable
      { get { return true;  } }
      public string TemplatePermit
      {
         get { return _TemplatePermit;  }
         set { 
		_TemplatePermit = value; 
		IsSetTemplatePermit = true;
		}
      }
	public bool IsSetTemplatePoint;
	public bool IsTemplatePointNullable
      { get { return true;  } }
      public decimal TemplatePoint
      {
         get { return _TemplatePoint;  }
         set { 
		_TemplatePoint = value; 
		IsSetTemplatePoint = true;
		}
      }
	public bool IsSetTemplateFileName;
	public bool IsTemplateFileNameNullable
      { get { return true;  } }
      public string TemplateFileName
      {
         get { return _TemplateFileName;  }
         set { 
		_TemplateFileName = value; 
		IsSetTemplateFileName = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FITemplateConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_decimal;
	IsSetAgencyID = false;
         _Name = EmptyValues.v_string;
	IsSetName = false;
         _TemplatePermit = EmptyValues.v_string;
	IsSetTemplatePermit = false;
         _TemplatePoint = EmptyValues.v_decimal;
	IsSetTemplatePoint = false;
         _TemplateFileName = EmptyValues.v_string;
	IsSetTemplateFileName = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetName)
            {
                s += " AND Name = @Name ";
            }
            if (IsSetTemplatePermit)
            {
                s += " AND TemplatePermit = @TemplatePermit ";
            }
            if (IsSetTemplatePoint)
            {
                s += " AND TemplatePoint = @TemplatePoint ";
            }
            if (IsSetTemplateFileName)
            {
                s += " AND TemplateFileName = @TemplateFileName ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}