using System;

namespace iTalent.Analyse.Repository
{
   public class DalException : Exception
   {
      public DalException() : base() { }
      public DalException(string message) : base(message) { }
      public DalException(string message, Exception innerException) : base(message, innerException) { }
   }
}