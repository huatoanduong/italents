using System;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FITemplateHistory.
	/// </summary>
	public class FITemplateHistoryConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private string _TemplateName;
      private DateTime _DateCreate;
      private string _KeyEn;
      private string _Remark;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetTemplateName;
	public bool IsTemplateNameNullable
      { get { return true;  } }
      public string TemplateName
      {
         get { return _TemplateName;  }
         set { 
		_TemplateName = value; 
		IsSetTemplateName = true;
		}
      }
	public bool IsSetDateCreate;
	public bool IsDateCreateNullable
      { get { return true;  } }
      public DateTime DateCreate
      {
         get { return _DateCreate;  }
         set { 
		_DateCreate = value; 
		IsSetDateCreate = true;
		}
      }
	public bool IsSetKeyEn;
	public bool IsKeyEnNullable
      { get { return true;  } }
      public string KeyEn
      {
         get { return _KeyEn;  }
         set { 
		_KeyEn = value; 
		IsSetKeyEn = true;
		}
      }
	public bool IsSetRemark;
	public bool IsRemarkNullable
      { get { return true;  } }
      public string Remark
      {
         get { return _Remark;  }
         set { 
		_Remark = value; 
		IsSetRemark = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FITemplateHistoryConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _TemplateName = EmptyValues.v_string;
	IsSetTemplateName = false;
         _DateCreate = EmptyValues.v_DateTime;
	IsSetDateCreate = false;
         _KeyEn = EmptyValues.v_string;
	IsSetKeyEn = false;
         _Remark = EmptyValues.v_string;
	IsSetRemark = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetTemplateName)
            {
                s += " AND TemplateName = @TemplateName ";
            }
            if (IsSetDateCreate)
            {
                s += " AND DateCreate = @DateCreate ";
            }
            if (IsSetKeyEn)
            {
                s += " AND KeyEn = @KeyEn ";
            }
            if (IsSetRemark)
            {
                s += " AND Remark = @Remark ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}