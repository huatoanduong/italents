using System;
using System.Data;
using System.Collections;
using iTalent.Analyse.Repository;

namespace iTalent.Analyse.Entity
{
	/// <summary>
	/// Summary description for FIDevice.
	/// </summary>
	public partial class FIDevice
	{

      private Int32 _ID;
      private decimal _AgencyID;
      private string _Devicename;
      private DateTime _ActiveDate;
      private string _ProductKey;
      private string _SerailKey;
      private string _SecKey;
      private string _Max;
      private string _Min;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public decimal AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsDevicenameNullable
      { get { return true;  } }
      public string Devicename
      {
         get { return _Devicename;  }
         set { _Devicename = value; }
      }
      public bool IsActiveDateNullable
      { get { return true;  } }
      public DateTime ActiveDate
      {
         get { return _ActiveDate;  }
         set { _ActiveDate = value; }
      }
      public bool IsProductKeyNullable
      { get { return true;  } }
      public string ProductKey
      {
         get { return _ProductKey;  }
         set { _ProductKey = value; }
      }
      public bool IsSerailKeyNullable
      { get { return true;  } }
      public string SerailKey
      {
         get { return _SerailKey;  }
         set { _SerailKey = value; }
      }
      public bool IsSecKeyNullable
      { get { return true;  } }
      public string SecKey
      {
         get { return _SecKey;  }
         set { _SecKey = value; }
      }
      public bool IsMaxNullable
      { get { return true;  } }
      public string Max
      {
         get { return _Max;  }
         set { _Max = value; }
      }
      public bool IsMinNullable
      { get { return true;  } }
      public string Min
      {
         get { return _Min;  }
         set { _Min = value; }
      }

      #endregion

      #region Constructors
      public FIDevice()
      {
         Reset();
      }
      public FIDevice(FIDevice obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._Devicename = obj.Devicename;
	this._ActiveDate = obj.ActiveDate;
	this._ProductKey = obj.ProductKey;
	this._SerailKey = obj.SerailKey;
	this._SecKey = obj.SecKey;
	this._Max = obj.Max;
	this._Min = obj.Min;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_decimal;
         _Devicename = EmptyValues.v_string;
         _ActiveDate = EmptyValues.v_DateTime;
         _ProductKey = EmptyValues.v_string;
         _SerailKey = EmptyValues.v_string;
         _SecKey = EmptyValues.v_string;
         _Max = EmptyValues.v_string;
         _Min = EmptyValues.v_string;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (decimal)reader["AgencyID"];
		}
                if(!(reader["Devicename"] is DBNull))
		{
			obj.Devicename = (string)reader["Devicename"];
		}
                if(!(reader["ActiveDate"] is DBNull))
		{
			obj.ActiveDate = (DateTime)reader["ActiveDate"];
		}
                if(!(reader["ProductKey"] is DBNull))
		{
			obj.ProductKey = (string)reader["ProductKey"];
		}
                if(!(reader["SerailKey"] is DBNull))
		{
			obj.SerailKey = (string)reader["SerailKey"];
		}
                if(!(reader["SecKey"] is DBNull))
		{
			obj.SecKey = (string)reader["SecKey"];
		}
                if(!(reader["Max"] is DBNull))
		{
			obj.Max = (string)reader["Max"];
		}
                if(!(reader["Min"] is DBNull))
		{
			obj.Min = (string)reader["Min"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AgencyID"];
//            _Devicename = (reader["Devicename"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Devicename"];
//            _ActiveDate = (reader["ActiveDate"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["ActiveDate"];
//            _ProductKey = (reader["ProductKey"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ProductKey"];
//            _SerailKey = (reader["SerailKey"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["SerailKey"];
//            _SecKey = (reader["SecKey"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["SecKey"];
//            _Max = (reader["Max"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Max"];
//            _Min = (reader["Min"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Min"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, decimal AgencyID, string Devicename, DateTime ActiveDate, string ProductKey, string SerailKey, string SecKey, string Max, string Min)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._Devicename = Devicename;
         this._ActiveDate = ActiveDate;
         this._ProductKey = ProductKey;
         this._SerailKey = SerailKey;
         this._SecKey = SecKey;
         this._Max = Max;
         this._Min = Min;
      }
   }
}