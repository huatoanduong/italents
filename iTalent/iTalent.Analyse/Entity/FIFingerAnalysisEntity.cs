using System;
using System.Data;
using System.Collections;
using iTalent.Analyse.Repository;

namespace iTalent.Analyse.Entity
{
	/// <summary>
	/// Summary description for FIFingerAnalysis.
	/// </summary>
	public partial class FIFingerAnalysis
	{

      private Int32 _ID;
      private decimal _AgencyID;
      private decimal _ReportID;
      private decimal _LT_BLS;
      private decimal _RT_BLS;
      private decimal _Page_BLS;
      private decimal _SAP;
      private decimal _SBP;
      private decimal _SCP;
      private decimal _SDP;
      private decimal _SER;
      private decimal _TFRC;
      private decimal _EQVP;
      private decimal _IQVP;
      private decimal _AQVP;
      private decimal _CQVP;
      private decimal _L1RCP;
      private decimal _L2RCP;
      private decimal _L3RCP;
      private decimal _L4RCP;
      private decimal _L5RCP;
      private decimal _R1RCP;
      private decimal _R2RCP;
      private decimal _R3RCP;
      private decimal _R4RCP;
      private decimal _R5RCP;
      private decimal _L1RCP1;
      private decimal _L2RCP2;
      private decimal _L3RCP3;
      private decimal _L4RCP4;
      private decimal _L5RCP5;
      private decimal _R1RCP1;
      private decimal _R2RCP2;
      private decimal _R3RCP3;
      private decimal _R4RCP4;
      private decimal _R5RCP5;
      private decimal _ATDR;
      private decimal _ATDL;
      private string _ATDRA;
      private string _ATDRS;
      private string _ATDLA;
      private string _ATDLS;
      private decimal _AC2WP;
      private decimal _AC2UP;
      private decimal _AC2RP;
      private decimal _AC2XP;
      private decimal _AC2SP;
      private decimal _RCAP;
      private decimal _RCHP;
      private decimal _RCVP;
      private decimal _T1AP;
      private decimal _T1BP;
      private decimal _M1Q;
      private decimal _M2Q;
      private decimal _M3Q;
      private decimal _M4Q;
      private decimal _M5Q;
      private decimal _M6Q;
      private decimal _M7Q;
      private decimal _M8Q;
      private decimal _L1RCB;
      private decimal _L2RCB;
      private decimal _L3RCB;
      private decimal _L4RCB;
      private decimal _L5RCB;
      private decimal _R1RCB;
      private decimal _R2RCB;
      private decimal _R3RCB;
      private decimal _R4RCB;
      private decimal _R5RCB;
      private string _Label1;
      private string _Label2;
      private string _Label3;
      private string _Label4;
      private string _Label5;
      private string _Label6;
      private string _L1T;
      private string _L2T;
      private string _L3T;
      private string _L4T;
      private string _L5T;
      private string _R1T;
      private string _R2T;
      private string _R3T;
      private string _R4T;
      private string _R5T;
      private decimal _S1BZ;
      private decimal _S2BZ;
      private decimal _S3BZ;
      private decimal _S4BZ;
      private decimal _S5BZ;
      private decimal _S6BZ;
      private decimal _S7BZ;
      private decimal _S8BZ;
      private decimal _S9BZ;
      private decimal _S10BZ;
      private decimal _S11BZ;
      private decimal _S12BZ;
      private decimal _S13BZ;
      private decimal _S14BZ;
      private decimal _S15BZ;
      private decimal _S16BZ;
      private decimal _S17BZ;
      private decimal _S18BZ;
      private string _S1BZZ;
      private string _S2BZZ;
      private string _S3BZZ;
      private string _S4BZZ;
      private string _S5BZZ;
      private string _S6BZZ;
      private string _S7BZZ;
      private string _S8BZZ;
      private string _S9BZZ;
      private string _S10BZZ;
      private string _S11BZZ;
      private string _S12BZZ;
      private string _S13BZZ;
      private string _S14BZZ;
      private string _S15BZZ;
      private string _S16BZZ;
      private string _S17BZZ;
      private string _S18BZZ;
      private decimal _M1QPR;
      private decimal _M2QPR;
      private decimal _M3QPR;
      private decimal _M4QPR;
      private decimal _M5QPR;
      private decimal _M6QPR;
      private decimal _M7QPR;
      private decimal _M8QPR;
      private decimal _FPType1;
      private decimal _FPType2;
      private decimal _FPType3;
      private decimal _FPType4;
      private decimal _FPType5;
      private decimal _FPType6;
      private decimal _FPType7;
      private decimal _FPType8;
      private decimal _FPType9;
      private decimal _FPType10;
      private decimal _FPType11;
      private decimal _FPType12;
      private decimal _FPType13;
      private decimal _FPType14;
      private decimal _FPType15;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public decimal AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsReportIDNullable
      { get { return true;  } }
      public decimal ReportID
      {
         get { return _ReportID;  }
         set { _ReportID = value; }
      }
      public bool IsLT_BLSNullable
      { get { return true;  } }
      public decimal LT_BLS
      {
         get { return _LT_BLS;  }
         set { _LT_BLS = value; }
      }
      public bool IsRT_BLSNullable
      { get { return true;  } }
      public decimal RT_BLS
      {
         get { return _RT_BLS;  }
         set { _RT_BLS = value; }
      }
      public bool IsPage_BLSNullable
      { get { return true;  } }
      public decimal Page_BLS
      {
         get { return _Page_BLS;  }
         set { _Page_BLS = value; }
      }
      public bool IsSAPNullable
      { get { return true;  } }
      public decimal SAP
      {
         get { return _SAP;  }
         set { _SAP = value; }
      }
      public bool IsSBPNullable
      { get { return true;  } }
      public decimal SBP
      {
         get { return _SBP;  }
         set { _SBP = value; }
      }
      public bool IsSCPNullable
      { get { return true;  } }
      public decimal SCP
      {
         get { return _SCP;  }
         set { _SCP = value; }
      }
      public bool IsSDPNullable
      { get { return true;  } }
      public decimal SDP
      {
         get { return _SDP;  }
         set { _SDP = value; }
      }
      public bool IsSERNullable
      { get { return true;  } }
      public decimal SER
      {
         get { return _SER;  }
         set { _SER = value; }
      }
      public bool IsTFRCNullable
      { get { return true;  } }
      public decimal TFRC
      {
         get { return _TFRC;  }
         set { _TFRC = value; }
      }
      public bool IsEQVPNullable
      { get { return true;  } }
      public decimal EQVP
      {
         get { return _EQVP;  }
         set { _EQVP = value; }
      }
      public bool IsIQVPNullable
      { get { return true;  } }
      public decimal IQVP
      {
         get { return _IQVP;  }
         set { _IQVP = value; }
      }
      public bool IsAQVPNullable
      { get { return true;  } }
      public decimal AQVP
      {
         get { return _AQVP;  }
         set { _AQVP = value; }
      }
      public bool IsCQVPNullable
      { get { return true;  } }
      public decimal CQVP
      {
         get { return _CQVP;  }
         set { _CQVP = value; }
      }
      public bool IsL1RCPNullable
      { get { return true;  } }
      public decimal L1RCP
      {
         get { return _L1RCP;  }
         set { _L1RCP = value; }
      }
      public bool IsL2RCPNullable
      { get { return true;  } }
      public decimal L2RCP
      {
         get { return _L2RCP;  }
         set { _L2RCP = value; }
      }
      public bool IsL3RCPNullable
      { get { return true;  } }
      public decimal L3RCP
      {
         get { return _L3RCP;  }
         set { _L3RCP = value; }
      }
      public bool IsL4RCPNullable
      { get { return true;  } }
      public decimal L4RCP
      {
         get { return _L4RCP;  }
         set { _L4RCP = value; }
      }
      public bool IsL5RCPNullable
      { get { return true;  } }
      public decimal L5RCP
      {
         get { return _L5RCP;  }
         set { _L5RCP = value; }
      }
      public bool IsR1RCPNullable
      { get { return true;  } }
      public decimal R1RCP
      {
         get { return _R1RCP;  }
         set { _R1RCP = value; }
      }
      public bool IsR2RCPNullable
      { get { return true;  } }
      public decimal R2RCP
      {
         get { return _R2RCP;  }
         set { _R2RCP = value; }
      }
      public bool IsR3RCPNullable
      { get { return true;  } }
      public decimal R3RCP
      {
         get { return _R3RCP;  }
         set { _R3RCP = value; }
      }
      public bool IsR4RCPNullable
      { get { return true;  } }
      public decimal R4RCP
      {
         get { return _R4RCP;  }
         set { _R4RCP = value; }
      }
      public bool IsR5RCPNullable
      { get { return true;  } }
      public decimal R5RCP
      {
         get { return _R5RCP;  }
         set { _R5RCP = value; }
      }
      public bool IsL1RCP1Nullable
      { get { return true;  } }
      public decimal L1RCP1
      {
         get { return _L1RCP1;  }
         set { _L1RCP1 = value; }
      }
      public bool IsL2RCP2Nullable
      { get { return true;  } }
      public decimal L2RCP2
      {
         get { return _L2RCP2;  }
         set { _L2RCP2 = value; }
      }
      public bool IsL3RCP3Nullable
      { get { return true;  } }
      public decimal L3RCP3
      {
         get { return _L3RCP3;  }
         set { _L3RCP3 = value; }
      }
      public bool IsL4RCP4Nullable
      { get { return true;  } }
      public decimal L4RCP4
      {
         get { return _L4RCP4;  }
         set { _L4RCP4 = value; }
      }
      public bool IsL5RCP5Nullable
      { get { return true;  } }
      public decimal L5RCP5
      {
         get { return _L5RCP5;  }
         set { _L5RCP5 = value; }
      }
      public bool IsR1RCP1Nullable
      { get { return true;  } }
      public decimal R1RCP1
      {
         get { return _R1RCP1;  }
         set { _R1RCP1 = value; }
      }
      public bool IsR2RCP2Nullable
      { get { return true;  } }
      public decimal R2RCP2
      {
         get { return _R2RCP2;  }
         set { _R2RCP2 = value; }
      }
      public bool IsR3RCP3Nullable
      { get { return true;  } }
      public decimal R3RCP3
      {
         get { return _R3RCP3;  }
         set { _R3RCP3 = value; }
      }
      public bool IsR4RCP4Nullable
      { get { return true;  } }
      public decimal R4RCP4
      {
         get { return _R4RCP4;  }
         set { _R4RCP4 = value; }
      }
      public bool IsR5RCP5Nullable
      { get { return true;  } }
      public decimal R5RCP5
      {
         get { return _R5RCP5;  }
         set { _R5RCP5 = value; }
      }
      public bool IsATDRNullable
      { get { return true;  } }
      public decimal ATDR
      {
         get { return _ATDR;  }
         set { _ATDR = value; }
      }
      public bool IsATDLNullable
      { get { return true;  } }
      public decimal ATDL
      {
         get { return _ATDL;  }
         set { _ATDL = value; }
      }
      public bool IsATDRANullable
      { get { return true;  } }
      public string ATDRA
      {
         get { return _ATDRA;  }
         set { _ATDRA = value; }
      }
      public bool IsATDRSNullable
      { get { return true;  } }
      public string ATDRS
      {
         get { return _ATDRS;  }
         set { _ATDRS = value; }
      }
      public bool IsATDLANullable
      { get { return true;  } }
      public string ATDLA
      {
         get { return _ATDLA;  }
         set { _ATDLA = value; }
      }
      public bool IsATDLSNullable
      { get { return true;  } }
      public string ATDLS
      {
         get { return _ATDLS;  }
         set { _ATDLS = value; }
      }
      public bool IsAC2WPNullable
      { get { return true;  } }
      public decimal AC2WP
      {
         get { return _AC2WP;  }
         set { _AC2WP = value; }
      }
      public bool IsAC2UPNullable
      { get { return true;  } }
      public decimal AC2UP
      {
         get { return _AC2UP;  }
         set { _AC2UP = value; }
      }
      public bool IsAC2RPNullable
      { get { return true;  } }
      public decimal AC2RP
      {
         get { return _AC2RP;  }
         set { _AC2RP = value; }
      }
      public bool IsAC2XPNullable
      { get { return true;  } }
      public decimal AC2XP
      {
         get { return _AC2XP;  }
         set { _AC2XP = value; }
      }
      public bool IsAC2SPNullable
      { get { return true;  } }
      public decimal AC2SP
      {
         get { return _AC2SP;  }
         set { _AC2SP = value; }
      }
      public bool IsRCAPNullable
      { get { return true;  } }
      public decimal RCAP
      {
         get { return _RCAP;  }
         set { _RCAP = value; }
      }
      public bool IsRCHPNullable
      { get { return true;  } }
      public decimal RCHP
      {
         get { return _RCHP;  }
         set { _RCHP = value; }
      }
      public bool IsRCVPNullable
      { get { return true;  } }
      public decimal RCVP
      {
         get { return _RCVP;  }
         set { _RCVP = value; }
      }
      public bool IsT1APNullable
      { get { return true;  } }
      public decimal T1AP
      {
         get { return _T1AP;  }
         set { _T1AP = value; }
      }
      public bool IsT1BPNullable
      { get { return true;  } }
      public decimal T1BP
      {
         get { return _T1BP;  }
         set { _T1BP = value; }
      }
      public bool IsM1QNullable
      { get { return true;  } }
      public decimal M1Q
      {
         get { return _M1Q;  }
         set { _M1Q = value; }
      }
      public bool IsM2QNullable
      { get { return true;  } }
      public decimal M2Q
      {
         get { return _M2Q;  }
         set { _M2Q = value; }
      }
      public bool IsM3QNullable
      { get { return true;  } }
      public decimal M3Q
      {
         get { return _M3Q;  }
         set { _M3Q = value; }
      }
      public bool IsM4QNullable
      { get { return true;  } }
      public decimal M4Q
      {
         get { return _M4Q;  }
         set { _M4Q = value; }
      }
      public bool IsM5QNullable
      { get { return true;  } }
      public decimal M5Q
      {
         get { return _M5Q;  }
         set { _M5Q = value; }
      }
      public bool IsM6QNullable
      { get { return true;  } }
      public decimal M6Q
      {
         get { return _M6Q;  }
         set { _M6Q = value; }
      }
      public bool IsM7QNullable
      { get { return true;  } }
      public decimal M7Q
      {
         get { return _M7Q;  }
         set { _M7Q = value; }
      }
      public bool IsM8QNullable
      { get { return true;  } }
      public decimal M8Q
      {
         get { return _M8Q;  }
         set { _M8Q = value; }
      }
      public bool IsL1RCBNullable
      { get { return true;  } }
      public decimal L1RCB
      {
         get { return _L1RCB;  }
         set { _L1RCB = value; }
      }
      public bool IsL2RCBNullable
      { get { return true;  } }
      public decimal L2RCB
      {
         get { return _L2RCB;  }
         set { _L2RCB = value; }
      }
      public bool IsL3RCBNullable
      { get { return true;  } }
      public decimal L3RCB
      {
         get { return _L3RCB;  }
         set { _L3RCB = value; }
      }
      public bool IsL4RCBNullable
      { get { return true;  } }
      public decimal L4RCB
      {
         get { return _L4RCB;  }
         set { _L4RCB = value; }
      }
      public bool IsL5RCBNullable
      { get { return true;  } }
      public decimal L5RCB
      {
         get { return _L5RCB;  }
         set { _L5RCB = value; }
      }
      public bool IsR1RCBNullable
      { get { return true;  } }
      public decimal R1RCB
      {
         get { return _R1RCB;  }
         set { _R1RCB = value; }
      }
      public bool IsR2RCBNullable
      { get { return true;  } }
      public decimal R2RCB
      {
         get { return _R2RCB;  }
         set { _R2RCB = value; }
      }
      public bool IsR3RCBNullable
      { get { return true;  } }
      public decimal R3RCB
      {
         get { return _R3RCB;  }
         set { _R3RCB = value; }
      }
      public bool IsR4RCBNullable
      { get { return true;  } }
      public decimal R4RCB
      {
         get { return _R4RCB;  }
         set { _R4RCB = value; }
      }
      public bool IsR5RCBNullable
      { get { return true;  } }
      public decimal R5RCB
      {
         get { return _R5RCB;  }
         set { _R5RCB = value; }
      }
      public bool IsLabel1Nullable
      { get { return true;  } }
      public string Label1
      {
         get { return _Label1;  }
         set { _Label1 = value; }
      }
      public bool IsLabel2Nullable
      { get { return true;  } }
      public string Label2
      {
         get { return _Label2;  }
         set { _Label2 = value; }
      }
      public bool IsLabel3Nullable
      { get { return true;  } }
      public string Label3
      {
         get { return _Label3;  }
         set { _Label3 = value; }
      }
      public bool IsLabel4Nullable
      { get { return true;  } }
      public string Label4
      {
         get { return _Label4;  }
         set { _Label4 = value; }
      }
      public bool IsLabel5Nullable
      { get { return true;  } }
      public string Label5
      {
         get { return _Label5;  }
         set { _Label5 = value; }
      }
      public bool IsLabel6Nullable
      { get { return true;  } }
      public string Label6
      {
         get { return _Label6;  }
         set { _Label6 = value; }
      }
      public bool IsL1TNullable
      { get { return true;  } }
      public string L1T
      {
         get { return _L1T;  }
         set { _L1T = value; }
      }
      public bool IsL2TNullable
      { get { return true;  } }
      public string L2T
      {
         get { return _L2T;  }
         set { _L2T = value; }
      }
      public bool IsL3TNullable
      { get { return true;  } }
      public string L3T
      {
         get { return _L3T;  }
         set { _L3T = value; }
      }
      public bool IsL4TNullable
      { get { return true;  } }
      public string L4T
      {
         get { return _L4T;  }
         set { _L4T = value; }
      }
      public bool IsL5TNullable
      { get { return true;  } }
      public string L5T
      {
         get { return _L5T;  }
         set { _L5T = value; }
      }
      public bool IsR1TNullable
      { get { return true;  } }
      public string R1T
      {
         get { return _R1T;  }
         set { _R1T = value; }
      }
      public bool IsR2TNullable
      { get { return true;  } }
      public string R2T
      {
         get { return _R2T;  }
         set { _R2T = value; }
      }
      public bool IsR3TNullable
      { get { return true;  } }
      public string R3T
      {
         get { return _R3T;  }
         set { _R3T = value; }
      }
      public bool IsR4TNullable
      { get { return true;  } }
      public string R4T
      {
         get { return _R4T;  }
         set { _R4T = value; }
      }
      public bool IsR5TNullable
      { get { return true;  } }
      public string R5T
      {
         get { return _R5T;  }
         set { _R5T = value; }
      }
      public bool IsS1BZNullable
      { get { return true;  } }
      public decimal S1BZ
      {
         get { return _S1BZ;  }
         set { _S1BZ = value; }
      }
      public bool IsS2BZNullable
      { get { return true;  } }
      public decimal S2BZ
      {
         get { return _S2BZ;  }
         set { _S2BZ = value; }
      }
      public bool IsS3BZNullable
      { get { return true;  } }
      public decimal S3BZ
      {
         get { return _S3BZ;  }
         set { _S3BZ = value; }
      }
      public bool IsS4BZNullable
      { get { return true;  } }
      public decimal S4BZ
      {
         get { return _S4BZ;  }
         set { _S4BZ = value; }
      }
      public bool IsS5BZNullable
      { get { return true;  } }
      public decimal S5BZ
      {
         get { return _S5BZ;  }
         set { _S5BZ = value; }
      }
      public bool IsS6BZNullable
      { get { return true;  } }
      public decimal S6BZ
      {
         get { return _S6BZ;  }
         set { _S6BZ = value; }
      }
      public bool IsS7BZNullable
      { get { return true;  } }
      public decimal S7BZ
      {
         get { return _S7BZ;  }
         set { _S7BZ = value; }
      }
      public bool IsS8BZNullable
      { get { return true;  } }
      public decimal S8BZ
      {
         get { return _S8BZ;  }
         set { _S8BZ = value; }
      }
      public bool IsS9BZNullable
      { get { return true;  } }
      public decimal S9BZ
      {
         get { return _S9BZ;  }
         set { _S9BZ = value; }
      }
      public bool IsS10BZNullable
      { get { return true;  } }
      public decimal S10BZ
      {
         get { return _S10BZ;  }
         set { _S10BZ = value; }
      }
      public bool IsS11BZNullable
      { get { return true;  } }
      public decimal S11BZ
      {
         get { return _S11BZ;  }
         set { _S11BZ = value; }
      }
      public bool IsS12BZNullable
      { get { return true;  } }
      public decimal S12BZ
      {
         get { return _S12BZ;  }
         set { _S12BZ = value; }
      }
      public bool IsS13BZNullable
      { get { return true;  } }
      public decimal S13BZ
      {
         get { return _S13BZ;  }
         set { _S13BZ = value; }
      }
      public bool IsS14BZNullable
      { get { return true;  } }
      public decimal S14BZ
      {
         get { return _S14BZ;  }
         set { _S14BZ = value; }
      }
      public bool IsS15BZNullable
      { get { return true;  } }
      public decimal S15BZ
      {
         get { return _S15BZ;  }
         set { _S15BZ = value; }
      }
      public bool IsS16BZNullable
      { get { return true;  } }
      public decimal S16BZ
      {
         get { return _S16BZ;  }
         set { _S16BZ = value; }
      }
      public bool IsS17BZNullable
      { get { return true;  } }
      public decimal S17BZ
      {
         get { return _S17BZ;  }
         set { _S17BZ = value; }
      }
      public bool IsS18BZNullable
      { get { return true;  } }
      public decimal S18BZ
      {
         get { return _S18BZ;  }
         set { _S18BZ = value; }
      }
      public bool IsS1BZZNullable
      { get { return true;  } }
      public string S1BZZ
      {
         get { return _S1BZZ;  }
         set { _S1BZZ = value; }
      }
      public bool IsS2BZZNullable
      { get { return true;  } }
      public string S2BZZ
      {
         get { return _S2BZZ;  }
         set { _S2BZZ = value; }
      }
      public bool IsS3BZZNullable
      { get { return true;  } }
      public string S3BZZ
      {
         get { return _S3BZZ;  }
         set { _S3BZZ = value; }
      }
      public bool IsS4BZZNullable
      { get { return true;  } }
      public string S4BZZ
      {
         get { return _S4BZZ;  }
         set { _S4BZZ = value; }
      }
      public bool IsS5BZZNullable
      { get { return true;  } }
      public string S5BZZ
      {
         get { return _S5BZZ;  }
         set { _S5BZZ = value; }
      }
      public bool IsS6BZZNullable
      { get { return true;  } }
      public string S6BZZ
      {
         get { return _S6BZZ;  }
         set { _S6BZZ = value; }
      }
      public bool IsS7BZZNullable
      { get { return true;  } }
      public string S7BZZ
      {
         get { return _S7BZZ;  }
         set { _S7BZZ = value; }
      }
      public bool IsS8BZZNullable
      { get { return true;  } }
      public string S8BZZ
      {
         get { return _S8BZZ;  }
         set { _S8BZZ = value; }
      }
      public bool IsS9BZZNullable
      { get { return true;  } }
      public string S9BZZ
      {
         get { return _S9BZZ;  }
         set { _S9BZZ = value; }
      }
      public bool IsS10BZZNullable
      { get { return true;  } }
      public string S10BZZ
      {
         get { return _S10BZZ;  }
         set { _S10BZZ = value; }
      }
      public bool IsS11BZZNullable
      { get { return true;  } }
      public string S11BZZ
      {
         get { return _S11BZZ;  }
         set { _S11BZZ = value; }
      }
      public bool IsS12BZZNullable
      { get { return true;  } }
      public string S12BZZ
      {
         get { return _S12BZZ;  }
         set { _S12BZZ = value; }
      }
      public bool IsS13BZZNullable
      { get { return true;  } }
      public string S13BZZ
      {
         get { return _S13BZZ;  }
         set { _S13BZZ = value; }
      }
      public bool IsS14BZZNullable
      { get { return true;  } }
      public string S14BZZ
      {
         get { return _S14BZZ;  }
         set { _S14BZZ = value; }
      }
      public bool IsS15BZZNullable
      { get { return true;  } }
      public string S15BZZ
      {
         get { return _S15BZZ;  }
         set { _S15BZZ = value; }
      }
      public bool IsS16BZZNullable
      { get { return true;  } }
      public string S16BZZ
      {
         get { return _S16BZZ;  }
         set { _S16BZZ = value; }
      }
      public bool IsS17BZZNullable
      { get { return true;  } }
      public string S17BZZ
      {
         get { return _S17BZZ;  }
         set { _S17BZZ = value; }
      }
      public bool IsS18BZZNullable
      { get { return true;  } }
      public string S18BZZ
      {
         get { return _S18BZZ;  }
         set { _S18BZZ = value; }
      }
      public bool IsM1QPRNullable
      { get { return true;  } }
      public decimal M1QPR
      {
         get { return _M1QPR;  }
         set { _M1QPR = value; }
      }
      public bool IsM2QPRNullable
      { get { return true;  } }
      public decimal M2QPR
      {
         get { return _M2QPR;  }
         set { _M2QPR = value; }
      }
      public bool IsM3QPRNullable
      { get { return true;  } }
      public decimal M3QPR
      {
         get { return _M3QPR;  }
         set { _M3QPR = value; }
      }
      public bool IsM4QPRNullable
      { get { return true;  } }
      public decimal M4QPR
      {
         get { return _M4QPR;  }
         set { _M4QPR = value; }
      }
      public bool IsM5QPRNullable
      { get { return true;  } }
      public decimal M5QPR
      {
         get { return _M5QPR;  }
         set { _M5QPR = value; }
      }
      public bool IsM6QPRNullable
      { get { return true;  } }
      public decimal M6QPR
      {
         get { return _M6QPR;  }
         set { _M6QPR = value; }
      }
      public bool IsM7QPRNullable
      { get { return true;  } }
      public decimal M7QPR
      {
         get { return _M7QPR;  }
         set { _M7QPR = value; }
      }
      public bool IsM8QPRNullable
      { get { return true;  } }
      public decimal M8QPR
      {
         get { return _M8QPR;  }
         set { _M8QPR = value; }
      }
      public bool IsFPType1Nullable
      { get { return true;  } }
      public decimal FPType1
      {
         get { return _FPType1;  }
         set { _FPType1 = value; }
      }
      public bool IsFPType2Nullable
      { get { return true;  } }
      public decimal FPType2
      {
         get { return _FPType2;  }
         set { _FPType2 = value; }
      }
      public bool IsFPType3Nullable
      { get { return true;  } }
      public decimal FPType3
      {
         get { return _FPType3;  }
         set { _FPType3 = value; }
      }
      public bool IsFPType4Nullable
      { get { return true;  } }
      public decimal FPType4
      {
         get { return _FPType4;  }
         set { _FPType4 = value; }
      }
      public bool IsFPType5Nullable
      { get { return true;  } }
      public decimal FPType5
      {
         get { return _FPType5;  }
         set { _FPType5 = value; }
      }
      public bool IsFPType6Nullable
      { get { return true;  } }
      public decimal FPType6
      {
         get { return _FPType6;  }
         set { _FPType6 = value; }
      }
      public bool IsFPType7Nullable
      { get { return true;  } }
      public decimal FPType7
      {
         get { return _FPType7;  }
         set { _FPType7 = value; }
      }
      public bool IsFPType8Nullable
      { get { return true;  } }
      public decimal FPType8
      {
         get { return _FPType8;  }
         set { _FPType8 = value; }
      }
      public bool IsFPType9Nullable
      { get { return true;  } }
      public decimal FPType9
      {
         get { return _FPType9;  }
         set { _FPType9 = value; }
      }
      public bool IsFPType10Nullable
      { get { return true;  } }
      public decimal FPType10
      {
         get { return _FPType10;  }
         set { _FPType10 = value; }
      }
      public bool IsFPType11Nullable
      { get { return true;  } }
      public decimal FPType11
      {
         get { return _FPType11;  }
         set { _FPType11 = value; }
      }
      public bool IsFPType12Nullable
      { get { return true;  } }
      public decimal FPType12
      {
         get { return _FPType12;  }
         set { _FPType12 = value; }
      }
      public bool IsFPType13Nullable
      { get { return true;  } }
      public decimal FPType13
      {
         get { return _FPType13;  }
         set { _FPType13 = value; }
      }
      public bool IsFPType14Nullable
      { get { return true;  } }
      public decimal FPType14
      {
         get { return _FPType14;  }
         set { _FPType14 = value; }
      }
      public bool IsFPType15Nullable
      { get { return true;  } }
      public decimal FPType15
      {
         get { return _FPType15;  }
         set { _FPType15 = value; }
      }

      #endregion

      #region Constructors
      public FIFingerAnalysis()
      {
         Reset();
      }
      public FIFingerAnalysis(FIFingerAnalysis obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._ReportID = obj.ReportID;
	this._LT_BLS = obj.LT_BLS;
	this._RT_BLS = obj.RT_BLS;
	this._Page_BLS = obj.Page_BLS;
	this._SAP = obj.SAP;
	this._SBP = obj.SBP;
	this._SCP = obj.SCP;
	this._SDP = obj.SDP;
	this._SER = obj.SER;
	this._TFRC = obj.TFRC;
	this._EQVP = obj.EQVP;
	this._IQVP = obj.IQVP;
	this._AQVP = obj.AQVP;
	this._CQVP = obj.CQVP;
	this._L1RCP = obj.L1RCP;
	this._L2RCP = obj.L2RCP;
	this._L3RCP = obj.L3RCP;
	this._L4RCP = obj.L4RCP;
	this._L5RCP = obj.L5RCP;
	this._R1RCP = obj.R1RCP;
	this._R2RCP = obj.R2RCP;
	this._R3RCP = obj.R3RCP;
	this._R4RCP = obj.R4RCP;
	this._R5RCP = obj.R5RCP;
	this._L1RCP1 = obj.L1RCP1;
	this._L2RCP2 = obj.L2RCP2;
	this._L3RCP3 = obj.L3RCP3;
	this._L4RCP4 = obj.L4RCP4;
	this._L5RCP5 = obj.L5RCP5;
	this._R1RCP1 = obj.R1RCP1;
	this._R2RCP2 = obj.R2RCP2;
	this._R3RCP3 = obj.R3RCP3;
	this._R4RCP4 = obj.R4RCP4;
	this._R5RCP5 = obj.R5RCP5;
	this._ATDR = obj.ATDR;
	this._ATDL = obj.ATDL;
	this._ATDRA = obj.ATDRA;
	this._ATDRS = obj.ATDRS;
	this._ATDLA = obj.ATDLA;
	this._ATDLS = obj.ATDLS;
	this._AC2WP = obj.AC2WP;
	this._AC2UP = obj.AC2UP;
	this._AC2RP = obj.AC2RP;
	this._AC2XP = obj.AC2XP;
	this._AC2SP = obj.AC2SP;
	this._RCAP = obj.RCAP;
	this._RCHP = obj.RCHP;
	this._RCVP = obj.RCVP;
	this._T1AP = obj.T1AP;
	this._T1BP = obj.T1BP;
	this._M1Q = obj.M1Q;
	this._M2Q = obj.M2Q;
	this._M3Q = obj.M3Q;
	this._M4Q = obj.M4Q;
	this._M5Q = obj.M5Q;
	this._M6Q = obj.M6Q;
	this._M7Q = obj.M7Q;
	this._M8Q = obj.M8Q;
	this._L1RCB = obj.L1RCB;
	this._L2RCB = obj.L2RCB;
	this._L3RCB = obj.L3RCB;
	this._L4RCB = obj.L4RCB;
	this._L5RCB = obj.L5RCB;
	this._R1RCB = obj.R1RCB;
	this._R2RCB = obj.R2RCB;
	this._R3RCB = obj.R3RCB;
	this._R4RCB = obj.R4RCB;
	this._R5RCB = obj.R5RCB;
	this._Label1 = obj.Label1;
	this._Label2 = obj.Label2;
	this._Label3 = obj.Label3;
	this._Label4 = obj.Label4;
	this._Label5 = obj.Label5;
	this._Label6 = obj.Label6;
	this._L1T = obj.L1T;
	this._L2T = obj.L2T;
	this._L3T = obj.L3T;
	this._L4T = obj.L4T;
	this._L5T = obj.L5T;
	this._R1T = obj.R1T;
	this._R2T = obj.R2T;
	this._R3T = obj.R3T;
	this._R4T = obj.R4T;
	this._R5T = obj.R5T;
	this._S1BZ = obj.S1BZ;
	this._S2BZ = obj.S2BZ;
	this._S3BZ = obj.S3BZ;
	this._S4BZ = obj.S4BZ;
	this._S5BZ = obj.S5BZ;
	this._S6BZ = obj.S6BZ;
	this._S7BZ = obj.S7BZ;
	this._S8BZ = obj.S8BZ;
	this._S9BZ = obj.S9BZ;
	this._S10BZ = obj.S10BZ;
	this._S11BZ = obj.S11BZ;
	this._S12BZ = obj.S12BZ;
	this._S13BZ = obj.S13BZ;
	this._S14BZ = obj.S14BZ;
	this._S15BZ = obj.S15BZ;
	this._S16BZ = obj.S16BZ;
	this._S17BZ = obj.S17BZ;
	this._S18BZ = obj.S18BZ;
	this._S1BZZ = obj.S1BZZ;
	this._S2BZZ = obj.S2BZZ;
	this._S3BZZ = obj.S3BZZ;
	this._S4BZZ = obj.S4BZZ;
	this._S5BZZ = obj.S5BZZ;
	this._S6BZZ = obj.S6BZZ;
	this._S7BZZ = obj.S7BZZ;
	this._S8BZZ = obj.S8BZZ;
	this._S9BZZ = obj.S9BZZ;
	this._S10BZZ = obj.S10BZZ;
	this._S11BZZ = obj.S11BZZ;
	this._S12BZZ = obj.S12BZZ;
	this._S13BZZ = obj.S13BZZ;
	this._S14BZZ = obj.S14BZZ;
	this._S15BZZ = obj.S15BZZ;
	this._S16BZZ = obj.S16BZZ;
	this._S17BZZ = obj.S17BZZ;
	this._S18BZZ = obj.S18BZZ;
	this._M1QPR = obj.M1QPR;
	this._M2QPR = obj.M2QPR;
	this._M3QPR = obj.M3QPR;
	this._M4QPR = obj.M4QPR;
	this._M5QPR = obj.M5QPR;
	this._M6QPR = obj.M6QPR;
	this._M7QPR = obj.M7QPR;
	this._M8QPR = obj.M8QPR;
	this._FPType1 = obj.FPType1;
	this._FPType2 = obj.FPType2;
	this._FPType3 = obj.FPType3;
	this._FPType4 = obj.FPType4;
	this._FPType5 = obj.FPType5;
	this._FPType6 = obj.FPType6;
	this._FPType7 = obj.FPType7;
	this._FPType8 = obj.FPType8;
	this._FPType9 = obj.FPType9;
	this._FPType10 = obj.FPType10;
	this._FPType11 = obj.FPType11;
	this._FPType12 = obj.FPType12;
	this._FPType13 = obj.FPType13;
	this._FPType14 = obj.FPType14;
	this._FPType15 = obj.FPType15;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_decimal;
         _ReportID = EmptyValues.v_decimal;
         _LT_BLS = EmptyValues.v_decimal;
         _RT_BLS = EmptyValues.v_decimal;
         _Page_BLS = EmptyValues.v_decimal;
         _SAP = EmptyValues.v_decimal;
         _SBP = EmptyValues.v_decimal;
         _SCP = EmptyValues.v_decimal;
         _SDP = EmptyValues.v_decimal;
         _SER = EmptyValues.v_decimal;
         _TFRC = EmptyValues.v_decimal;
         _EQVP = EmptyValues.v_decimal;
         _IQVP = EmptyValues.v_decimal;
         _AQVP = EmptyValues.v_decimal;
         _CQVP = EmptyValues.v_decimal;
         _L1RCP = EmptyValues.v_decimal;
         _L2RCP = EmptyValues.v_decimal;
         _L3RCP = EmptyValues.v_decimal;
         _L4RCP = EmptyValues.v_decimal;
         _L5RCP = EmptyValues.v_decimal;
         _R1RCP = EmptyValues.v_decimal;
         _R2RCP = EmptyValues.v_decimal;
         _R3RCP = EmptyValues.v_decimal;
         _R4RCP = EmptyValues.v_decimal;
         _R5RCP = EmptyValues.v_decimal;
         _L1RCP1 = EmptyValues.v_decimal;
         _L2RCP2 = EmptyValues.v_decimal;
         _L3RCP3 = EmptyValues.v_decimal;
         _L4RCP4 = EmptyValues.v_decimal;
         _L5RCP5 = EmptyValues.v_decimal;
         _R1RCP1 = EmptyValues.v_decimal;
         _R2RCP2 = EmptyValues.v_decimal;
         _R3RCP3 = EmptyValues.v_decimal;
         _R4RCP4 = EmptyValues.v_decimal;
         _R5RCP5 = EmptyValues.v_decimal;
         _ATDR = EmptyValues.v_decimal;
         _ATDL = EmptyValues.v_decimal;
         _ATDRA = EmptyValues.v_string;
         _ATDRS = EmptyValues.v_string;
         _ATDLA = EmptyValues.v_string;
         _ATDLS = EmptyValues.v_string;
         _AC2WP = EmptyValues.v_decimal;
         _AC2UP = EmptyValues.v_decimal;
         _AC2RP = EmptyValues.v_decimal;
         _AC2XP = EmptyValues.v_decimal;
         _AC2SP = EmptyValues.v_decimal;
         _RCAP = EmptyValues.v_decimal;
         _RCHP = EmptyValues.v_decimal;
         _RCVP = EmptyValues.v_decimal;
         _T1AP = EmptyValues.v_decimal;
         _T1BP = EmptyValues.v_decimal;
         _M1Q = EmptyValues.v_decimal;
         _M2Q = EmptyValues.v_decimal;
         _M3Q = EmptyValues.v_decimal;
         _M4Q = EmptyValues.v_decimal;
         _M5Q = EmptyValues.v_decimal;
         _M6Q = EmptyValues.v_decimal;
         _M7Q = EmptyValues.v_decimal;
         _M8Q = EmptyValues.v_decimal;
         _L1RCB = EmptyValues.v_decimal;
         _L2RCB = EmptyValues.v_decimal;
         _L3RCB = EmptyValues.v_decimal;
         _L4RCB = EmptyValues.v_decimal;
         _L5RCB = EmptyValues.v_decimal;
         _R1RCB = EmptyValues.v_decimal;
         _R2RCB = EmptyValues.v_decimal;
         _R3RCB = EmptyValues.v_decimal;
         _R4RCB = EmptyValues.v_decimal;
         _R5RCB = EmptyValues.v_decimal;
         _Label1 = EmptyValues.v_string;
         _Label2 = EmptyValues.v_string;
         _Label3 = EmptyValues.v_string;
         _Label4 = EmptyValues.v_string;
         _Label5 = EmptyValues.v_string;
         _Label6 = EmptyValues.v_string;
         _L1T = EmptyValues.v_string;
         _L2T = EmptyValues.v_string;
         _L3T = EmptyValues.v_string;
         _L4T = EmptyValues.v_string;
         _L5T = EmptyValues.v_string;
         _R1T = EmptyValues.v_string;
         _R2T = EmptyValues.v_string;
         _R3T = EmptyValues.v_string;
         _R4T = EmptyValues.v_string;
         _R5T = EmptyValues.v_string;
         _S1BZ = EmptyValues.v_decimal;
         _S2BZ = EmptyValues.v_decimal;
         _S3BZ = EmptyValues.v_decimal;
         _S4BZ = EmptyValues.v_decimal;
         _S5BZ = EmptyValues.v_decimal;
         _S6BZ = EmptyValues.v_decimal;
         _S7BZ = EmptyValues.v_decimal;
         _S8BZ = EmptyValues.v_decimal;
         _S9BZ = EmptyValues.v_decimal;
         _S10BZ = EmptyValues.v_decimal;
         _S11BZ = EmptyValues.v_decimal;
         _S12BZ = EmptyValues.v_decimal;
         _S13BZ = EmptyValues.v_decimal;
         _S14BZ = EmptyValues.v_decimal;
         _S15BZ = EmptyValues.v_decimal;
         _S16BZ = EmptyValues.v_decimal;
         _S17BZ = EmptyValues.v_decimal;
         _S18BZ = EmptyValues.v_decimal;
         _S1BZZ = EmptyValues.v_string;
         _S2BZZ = EmptyValues.v_string;
         _S3BZZ = EmptyValues.v_string;
         _S4BZZ = EmptyValues.v_string;
         _S5BZZ = EmptyValues.v_string;
         _S6BZZ = EmptyValues.v_string;
         _S7BZZ = EmptyValues.v_string;
         _S8BZZ = EmptyValues.v_string;
         _S9BZZ = EmptyValues.v_string;
         _S10BZZ = EmptyValues.v_string;
         _S11BZZ = EmptyValues.v_string;
         _S12BZZ = EmptyValues.v_string;
         _S13BZZ = EmptyValues.v_string;
         _S14BZZ = EmptyValues.v_string;
         _S15BZZ = EmptyValues.v_string;
         _S16BZZ = EmptyValues.v_string;
         _S17BZZ = EmptyValues.v_string;
         _S18BZZ = EmptyValues.v_string;
         _M1QPR = EmptyValues.v_decimal;
         _M2QPR = EmptyValues.v_decimal;
         _M3QPR = EmptyValues.v_decimal;
         _M4QPR = EmptyValues.v_decimal;
         _M5QPR = EmptyValues.v_decimal;
         _M6QPR = EmptyValues.v_decimal;
         _M7QPR = EmptyValues.v_decimal;
         _M8QPR = EmptyValues.v_decimal;
         _FPType1 = EmptyValues.v_decimal;
         _FPType2 = EmptyValues.v_decimal;
         _FPType3 = EmptyValues.v_decimal;
         _FPType4 = EmptyValues.v_decimal;
         _FPType5 = EmptyValues.v_decimal;
         _FPType6 = EmptyValues.v_decimal;
         _FPType7 = EmptyValues.v_decimal;
         _FPType8 = EmptyValues.v_decimal;
         _FPType9 = EmptyValues.v_decimal;
         _FPType10 = EmptyValues.v_decimal;
         _FPType11 = EmptyValues.v_decimal;
         _FPType12 = EmptyValues.v_decimal;
         _FPType13 = EmptyValues.v_decimal;
         _FPType14 = EmptyValues.v_decimal;
         _FPType15 = EmptyValues.v_decimal;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (decimal)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (decimal)reader["ReportID"];
		}
                if(!(reader["LT_BLS"] is DBNull))
		{
			obj.LT_BLS = (decimal)reader["LT_BLS"];
		}
                if(!(reader["RT_BLS"] is DBNull))
		{
			obj.RT_BLS = (decimal)reader["RT_BLS"];
		}
                if(!(reader["Page_BLS"] is DBNull))
		{
			obj.Page_BLS = (decimal)reader["Page_BLS"];
		}
                if(!(reader["SAP"] is DBNull))
		{
			obj.SAP = (decimal)reader["SAP"];
		}
                if(!(reader["SBP"] is DBNull))
		{
			obj.SBP = (decimal)reader["SBP"];
		}
                if(!(reader["SCP"] is DBNull))
		{
			obj.SCP = (decimal)reader["SCP"];
		}
                if(!(reader["SDP"] is DBNull))
		{
			obj.SDP = (decimal)reader["SDP"];
		}
                if(!(reader["SER"] is DBNull))
		{
			obj.SER = (decimal)reader["SER"];
		}
                if(!(reader["TFRC"] is DBNull))
		{
			obj.TFRC = (decimal)reader["TFRC"];
		}
                if(!(reader["EQVP"] is DBNull))
		{
			obj.EQVP = (decimal)reader["EQVP"];
		}
                if(!(reader["IQVP"] is DBNull))
		{
			obj.IQVP = (decimal)reader["IQVP"];
		}
                if(!(reader["AQVP"] is DBNull))
		{
			obj.AQVP = (decimal)reader["AQVP"];
		}
                if(!(reader["CQVP"] is DBNull))
		{
			obj.CQVP = (decimal)reader["CQVP"];
		}
                if(!(reader["L1RCP"] is DBNull))
		{
			obj.L1RCP = (decimal)reader["L1RCP"];
		}
                if(!(reader["L2RCP"] is DBNull))
		{
			obj.L2RCP = (decimal)reader["L2RCP"];
		}
                if(!(reader["L3RCP"] is DBNull))
		{
			obj.L3RCP = (decimal)reader["L3RCP"];
		}
                if(!(reader["L4RCP"] is DBNull))
		{
			obj.L4RCP = (decimal)reader["L4RCP"];
		}
                if(!(reader["L5RCP"] is DBNull))
		{
			obj.L5RCP = (decimal)reader["L5RCP"];
		}
                if(!(reader["R1RCP"] is DBNull))
		{
			obj.R1RCP = (decimal)reader["R1RCP"];
		}
                if(!(reader["R2RCP"] is DBNull))
		{
			obj.R2RCP = (decimal)reader["R2RCP"];
		}
                if(!(reader["R3RCP"] is DBNull))
		{
			obj.R3RCP = (decimal)reader["R3RCP"];
		}
                if(!(reader["R4RCP"] is DBNull))
		{
			obj.R4RCP = (decimal)reader["R4RCP"];
		}
                if(!(reader["R5RCP"] is DBNull))
		{
			obj.R5RCP = (decimal)reader["R5RCP"];
		}
                if(!(reader["L1RCP1"] is DBNull))
		{
			obj.L1RCP1 = (decimal)reader["L1RCP1"];
		}
                if(!(reader["L2RCP2"] is DBNull))
		{
			obj.L2RCP2 = (decimal)reader["L2RCP2"];
		}
                if(!(reader["L3RCP3"] is DBNull))
		{
			obj.L3RCP3 = (decimal)reader["L3RCP3"];
		}
                if(!(reader["L4RCP4"] is DBNull))
		{
			obj.L4RCP4 = (decimal)reader["L4RCP4"];
		}
                if(!(reader["L5RCP5"] is DBNull))
		{
			obj.L5RCP5 = (decimal)reader["L5RCP5"];
		}
                if(!(reader["R1RCP1"] is DBNull))
		{
			obj.R1RCP1 = (decimal)reader["R1RCP1"];
		}
                if(!(reader["R2RCP2"] is DBNull))
		{
			obj.R2RCP2 = (decimal)reader["R2RCP2"];
		}
                if(!(reader["R3RCP3"] is DBNull))
		{
			obj.R3RCP3 = (decimal)reader["R3RCP3"];
		}
                if(!(reader["R4RCP4"] is DBNull))
		{
			obj.R4RCP4 = (decimal)reader["R4RCP4"];
		}
                if(!(reader["R5RCP5"] is DBNull))
		{
			obj.R5RCP5 = (decimal)reader["R5RCP5"];
		}
                if(!(reader["ATDR"] is DBNull))
		{
			obj.ATDR = (decimal)reader["ATDR"];
		}
                if(!(reader["ATDL"] is DBNull))
		{
			obj.ATDL = (decimal)reader["ATDL"];
		}
                if(!(reader["ATDRA"] is DBNull))
		{
			obj.ATDRA = (string)reader["ATDRA"];
		}
                if(!(reader["ATDRS"] is DBNull))
		{
			obj.ATDRS = (string)reader["ATDRS"];
		}
                if(!(reader["ATDLA"] is DBNull))
		{
			obj.ATDLA = (string)reader["ATDLA"];
		}
                if(!(reader["ATDLS"] is DBNull))
		{
			obj.ATDLS = (string)reader["ATDLS"];
		}
                if(!(reader["AC2WP"] is DBNull))
		{
			obj.AC2WP = (decimal)reader["AC2WP"];
		}
                if(!(reader["AC2UP"] is DBNull))
		{
			obj.AC2UP = (decimal)reader["AC2UP"];
		}
                if(!(reader["AC2RP"] is DBNull))
		{
			obj.AC2RP = (decimal)reader["AC2RP"];
		}
                if(!(reader["AC2XP"] is DBNull))
		{
			obj.AC2XP = (decimal)reader["AC2XP"];
		}
                if(!(reader["AC2SP"] is DBNull))
		{
			obj.AC2SP = (decimal)reader["AC2SP"];
		}
                if(!(reader["RCAP"] is DBNull))
		{
			obj.RCAP = (decimal)reader["RCAP"];
		}
                if(!(reader["RCHP"] is DBNull))
		{
			obj.RCHP = (decimal)reader["RCHP"];
		}
                if(!(reader["RCVP"] is DBNull))
		{
			obj.RCVP = (decimal)reader["RCVP"];
		}
                if(!(reader["T1AP"] is DBNull))
		{
			obj.T1AP = (decimal)reader["T1AP"];
		}
                if(!(reader["T1BP"] is DBNull))
		{
			obj.T1BP = (decimal)reader["T1BP"];
		}
                if(!(reader["M1Q"] is DBNull))
		{
			obj.M1Q = (decimal)reader["M1Q"];
		}
                if(!(reader["M2Q"] is DBNull))
		{
			obj.M2Q = (decimal)reader["M2Q"];
		}
                if(!(reader["M3Q"] is DBNull))
		{
			obj.M3Q = (decimal)reader["M3Q"];
		}
                if(!(reader["M4Q"] is DBNull))
		{
			obj.M4Q = (decimal)reader["M4Q"];
		}
                if(!(reader["M5Q"] is DBNull))
		{
			obj.M5Q = (decimal)reader["M5Q"];
		}
                if(!(reader["M6Q"] is DBNull))
		{
			obj.M6Q = (decimal)reader["M6Q"];
		}
                if(!(reader["M7Q"] is DBNull))
		{
			obj.M7Q = (decimal)reader["M7Q"];
		}
                if(!(reader["M8Q"] is DBNull))
		{
			obj.M8Q = (decimal)reader["M8Q"];
		}
                if(!(reader["L1RCB"] is DBNull))
		{
			obj.L1RCB = (decimal)reader["L1RCB"];
		}
                if(!(reader["L2RCB"] is DBNull))
		{
			obj.L2RCB = (decimal)reader["L2RCB"];
		}
                if(!(reader["L3RCB"] is DBNull))
		{
			obj.L3RCB = (decimal)reader["L3RCB"];
		}
                if(!(reader["L4RCB"] is DBNull))
		{
			obj.L4RCB = (decimal)reader["L4RCB"];
		}
                if(!(reader["L5RCB"] is DBNull))
		{
			obj.L5RCB = (decimal)reader["L5RCB"];
		}
                if(!(reader["R1RCB"] is DBNull))
		{
			obj.R1RCB = (decimal)reader["R1RCB"];
		}
                if(!(reader["R2RCB"] is DBNull))
		{
			obj.R2RCB = (decimal)reader["R2RCB"];
		}
                if(!(reader["R3RCB"] is DBNull))
		{
			obj.R3RCB = (decimal)reader["R3RCB"];
		}
                if(!(reader["R4RCB"] is DBNull))
		{
			obj.R4RCB = (decimal)reader["R4RCB"];
		}
                if(!(reader["R5RCB"] is DBNull))
		{
			obj.R5RCB = (decimal)reader["R5RCB"];
		}
                if(!(reader["Label1"] is DBNull))
		{
			obj.Label1 = (string)reader["Label1"];
		}
                if(!(reader["Label2"] is DBNull))
		{
			obj.Label2 = (string)reader["Label2"];
		}
                if(!(reader["Label3"] is DBNull))
		{
			obj.Label3 = (string)reader["Label3"];
		}
                if(!(reader["Label4"] is DBNull))
		{
			obj.Label4 = (string)reader["Label4"];
		}
                if(!(reader["Label5"] is DBNull))
		{
			obj.Label5 = (string)reader["Label5"];
		}
                if(!(reader["Label6"] is DBNull))
		{
			obj.Label6 = (string)reader["Label6"];
		}
                if(!(reader["L1T"] is DBNull))
		{
			obj.L1T = (string)reader["L1T"];
		}
                if(!(reader["L2T"] is DBNull))
		{
			obj.L2T = (string)reader["L2T"];
		}
                if(!(reader["L3T"] is DBNull))
		{
			obj.L3T = (string)reader["L3T"];
		}
                if(!(reader["L4T"] is DBNull))
		{
			obj.L4T = (string)reader["L4T"];
		}
                if(!(reader["L5T"] is DBNull))
		{
			obj.L5T = (string)reader["L5T"];
		}
                if(!(reader["R1T"] is DBNull))
		{
			obj.R1T = (string)reader["R1T"];
		}
                if(!(reader["R2T"] is DBNull))
		{
			obj.R2T = (string)reader["R2T"];
		}
                if(!(reader["R3T"] is DBNull))
		{
			obj.R3T = (string)reader["R3T"];
		}
                if(!(reader["R4T"] is DBNull))
		{
			obj.R4T = (string)reader["R4T"];
		}
                if(!(reader["R5T"] is DBNull))
		{
			obj.R5T = (string)reader["R5T"];
		}
                if(!(reader["S1BZ"] is DBNull))
		{
			obj.S1BZ = (decimal)reader["S1BZ"];
		}
                if(!(reader["S2BZ"] is DBNull))
		{
			obj.S2BZ = (decimal)reader["S2BZ"];
		}
                if(!(reader["S3BZ"] is DBNull))
		{
			obj.S3BZ = (decimal)reader["S3BZ"];
		}
                if(!(reader["S4BZ"] is DBNull))
		{
			obj.S4BZ = (decimal)reader["S4BZ"];
		}
                if(!(reader["S5BZ"] is DBNull))
		{
			obj.S5BZ = (decimal)reader["S5BZ"];
		}
                if(!(reader["S6BZ"] is DBNull))
		{
			obj.S6BZ = (decimal)reader["S6BZ"];
		}
                if(!(reader["S7BZ"] is DBNull))
		{
			obj.S7BZ = (decimal)reader["S7BZ"];
		}
                if(!(reader["S8BZ"] is DBNull))
		{
			obj.S8BZ = (decimal)reader["S8BZ"];
		}
                if(!(reader["S9BZ"] is DBNull))
		{
			obj.S9BZ = (decimal)reader["S9BZ"];
		}
                if(!(reader["S10BZ"] is DBNull))
		{
			obj.S10BZ = (decimal)reader["S10BZ"];
		}
                if(!(reader["S11BZ"] is DBNull))
		{
			obj.S11BZ = (decimal)reader["S11BZ"];
		}
                if(!(reader["S12BZ"] is DBNull))
		{
			obj.S12BZ = (decimal)reader["S12BZ"];
		}
                if(!(reader["S13BZ"] is DBNull))
		{
			obj.S13BZ = (decimal)reader["S13BZ"];
		}
                if(!(reader["S14BZ"] is DBNull))
		{
			obj.S14BZ = (decimal)reader["S14BZ"];
		}
                if(!(reader["S15BZ"] is DBNull))
		{
			obj.S15BZ = (decimal)reader["S15BZ"];
		}
                if(!(reader["S16BZ"] is DBNull))
		{
			obj.S16BZ = (decimal)reader["S16BZ"];
		}
                if(!(reader["S17BZ"] is DBNull))
		{
			obj.S17BZ = (decimal)reader["S17BZ"];
		}
                if(!(reader["S18BZ"] is DBNull))
		{
			obj.S18BZ = (decimal)reader["S18BZ"];
		}
                if(!(reader["S1BZZ"] is DBNull))
		{
			obj.S1BZZ = (string)reader["S1BZZ"];
		}
                if(!(reader["S2BZZ"] is DBNull))
		{
			obj.S2BZZ = (string)reader["S2BZZ"];
		}
                if(!(reader["S3BZZ"] is DBNull))
		{
			obj.S3BZZ = (string)reader["S3BZZ"];
		}
                if(!(reader["S4BZZ"] is DBNull))
		{
			obj.S4BZZ = (string)reader["S4BZZ"];
		}
                if(!(reader["S5BZZ"] is DBNull))
		{
			obj.S5BZZ = (string)reader["S5BZZ"];
		}
                if(!(reader["S6BZZ"] is DBNull))
		{
			obj.S6BZZ = (string)reader["S6BZZ"];
		}
                if(!(reader["S7BZZ"] is DBNull))
		{
			obj.S7BZZ = (string)reader["S7BZZ"];
		}
                if(!(reader["S8BZZ"] is DBNull))
		{
			obj.S8BZZ = (string)reader["S8BZZ"];
		}
                if(!(reader["S9BZZ"] is DBNull))
		{
			obj.S9BZZ = (string)reader["S9BZZ"];
		}
                if(!(reader["S10BZZ"] is DBNull))
		{
			obj.S10BZZ = (string)reader["S10BZZ"];
		}
                if(!(reader["S11BZZ"] is DBNull))
		{
			obj.S11BZZ = (string)reader["S11BZZ"];
		}
                if(!(reader["S12BZZ"] is DBNull))
		{
			obj.S12BZZ = (string)reader["S12BZZ"];
		}
                if(!(reader["S13BZZ"] is DBNull))
		{
			obj.S13BZZ = (string)reader["S13BZZ"];
		}
                if(!(reader["S14BZZ"] is DBNull))
		{
			obj.S14BZZ = (string)reader["S14BZZ"];
		}
                if(!(reader["S15BZZ"] is DBNull))
		{
			obj.S15BZZ = (string)reader["S15BZZ"];
		}
                if(!(reader["S16BZZ"] is DBNull))
		{
			obj.S16BZZ = (string)reader["S16BZZ"];
		}
                if(!(reader["S17BZZ"] is DBNull))
		{
			obj.S17BZZ = (string)reader["S17BZZ"];
		}
                if(!(reader["S18BZZ"] is DBNull))
		{
			obj.S18BZZ = (string)reader["S18BZZ"];
		}
                if(!(reader["M1QPR"] is DBNull))
		{
			obj.M1QPR = (decimal)reader["M1QPR"];
		}
                if(!(reader["M2QPR"] is DBNull))
		{
			obj.M2QPR = (decimal)reader["M2QPR"];
		}
                if(!(reader["M3QPR"] is DBNull))
		{
			obj.M3QPR = (decimal)reader["M3QPR"];
		}
                if(!(reader["M4QPR"] is DBNull))
		{
			obj.M4QPR = (decimal)reader["M4QPR"];
		}
                if(!(reader["M5QPR"] is DBNull))
		{
			obj.M5QPR = (decimal)reader["M5QPR"];
		}
                if(!(reader["M6QPR"] is DBNull))
		{
			obj.M6QPR = (decimal)reader["M6QPR"];
		}
                if(!(reader["M7QPR"] is DBNull))
		{
			obj.M7QPR = (decimal)reader["M7QPR"];
		}
                if(!(reader["M8QPR"] is DBNull))
		{
			obj.M8QPR = (decimal)reader["M8QPR"];
		}
                if(!(reader["FPType1"] is DBNull))
		{
			obj.FPType1 = (decimal)reader["FPType1"];
		}
                if(!(reader["FPType2"] is DBNull))
		{
			obj.FPType2 = (decimal)reader["FPType2"];
		}
                if(!(reader["FPType3"] is DBNull))
		{
			obj.FPType3 = (decimal)reader["FPType3"];
		}
                if(!(reader["FPType4"] is DBNull))
		{
			obj.FPType4 = (decimal)reader["FPType4"];
		}
                if(!(reader["FPType5"] is DBNull))
		{
			obj.FPType5 = (decimal)reader["FPType5"];
		}
                if(!(reader["FPType6"] is DBNull))
		{
			obj.FPType6 = (decimal)reader["FPType6"];
		}
                if(!(reader["FPType7"] is DBNull))
		{
			obj.FPType7 = (decimal)reader["FPType7"];
		}
                if(!(reader["FPType8"] is DBNull))
		{
			obj.FPType8 = (decimal)reader["FPType8"];
		}
                if(!(reader["FPType9"] is DBNull))
		{
			obj.FPType9 = (decimal)reader["FPType9"];
		}
                if(!(reader["FPType10"] is DBNull))
		{
			obj.FPType10 = (decimal)reader["FPType10"];
		}
                if(!(reader["FPType11"] is DBNull))
		{
			obj.FPType11 = (decimal)reader["FPType11"];
		}
                if(!(reader["FPType12"] is DBNull))
		{
			obj.FPType12 = (decimal)reader["FPType12"];
		}
                if(!(reader["FPType13"] is DBNull))
		{
			obj.FPType13 = (decimal)reader["FPType13"];
		}
                if(!(reader["FPType14"] is DBNull))
		{
			obj.FPType14 = (decimal)reader["FPType14"];
		}
                if(!(reader["FPType15"] is DBNull))
		{
			obj.FPType15 = (decimal)reader["FPType15"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AgencyID"];
//            _ReportID = (reader["ReportID"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["ReportID"];
//            _LT_BLS = (reader["LT_BLS"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["LT_BLS"];
//            _RT_BLS = (reader["RT_BLS"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["RT_BLS"];
//            _Page_BLS = (reader["Page_BLS"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["Page_BLS"];
//            _SAP = (reader["SAP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["SAP"];
//            _SBP = (reader["SBP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["SBP"];
//            _SCP = (reader["SCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["SCP"];
//            _SDP = (reader["SDP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["SDP"];
//            _SER = (reader["SER"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["SER"];
//            _TFRC = (reader["TFRC"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["TFRC"];
//            _EQVP = (reader["EQVP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["EQVP"];
//            _IQVP = (reader["IQVP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["IQVP"];
//            _AQVP = (reader["AQVP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AQVP"];
//            _CQVP = (reader["CQVP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["CQVP"];
//            _L1RCP = (reader["L1RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L1RCP"];
//            _L2RCP = (reader["L2RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L2RCP"];
//            _L3RCP = (reader["L3RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L3RCP"];
//            _L4RCP = (reader["L4RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L4RCP"];
//            _L5RCP = (reader["L5RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L5RCP"];
//            _R1RCP = (reader["R1RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R1RCP"];
//            _R2RCP = (reader["R2RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R2RCP"];
//            _R3RCP = (reader["R3RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R3RCP"];
//            _R4RCP = (reader["R4RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R4RCP"];
//            _R5RCP = (reader["R5RCP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R5RCP"];
//            _L1RCP1 = (reader["L1RCP1"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L1RCP1"];
//            _L2RCP2 = (reader["L2RCP2"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L2RCP2"];
//            _L3RCP3 = (reader["L3RCP3"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L3RCP3"];
//            _L4RCP4 = (reader["L4RCP4"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L4RCP4"];
//            _L5RCP5 = (reader["L5RCP5"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L5RCP5"];
//            _R1RCP1 = (reader["R1RCP1"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R1RCP1"];
//            _R2RCP2 = (reader["R2RCP2"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R2RCP2"];
//            _R3RCP3 = (reader["R3RCP3"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R3RCP3"];
//            _R4RCP4 = (reader["R4RCP4"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R4RCP4"];
//            _R5RCP5 = (reader["R5RCP5"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R5RCP5"];
//            _ATDR = (reader["ATDR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["ATDR"];
//            _ATDL = (reader["ATDL"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["ATDL"];
//            _ATDRA = (reader["ATDRA"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ATDRA"];
//            _ATDRS = (reader["ATDRS"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ATDRS"];
//            _ATDLA = (reader["ATDLA"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ATDLA"];
//            _ATDLS = (reader["ATDLS"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ATDLS"];
//            _AC2WP = (reader["AC2WP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AC2WP"];
//            _AC2UP = (reader["AC2UP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AC2UP"];
//            _AC2RP = (reader["AC2RP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AC2RP"];
//            _AC2XP = (reader["AC2XP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AC2XP"];
//            _AC2SP = (reader["AC2SP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AC2SP"];
//            _RCAP = (reader["RCAP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["RCAP"];
//            _RCHP = (reader["RCHP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["RCHP"];
//            _RCVP = (reader["RCVP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["RCVP"];
//            _T1AP = (reader["T1AP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["T1AP"];
//            _T1BP = (reader["T1BP"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["T1BP"];
//            _M1Q = (reader["M1Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M1Q"];
//            _M2Q = (reader["M2Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M2Q"];
//            _M3Q = (reader["M3Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M3Q"];
//            _M4Q = (reader["M4Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M4Q"];
//            _M5Q = (reader["M5Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M5Q"];
//            _M6Q = (reader["M6Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M6Q"];
//            _M7Q = (reader["M7Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M7Q"];
//            _M8Q = (reader["M8Q"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M8Q"];
//            _L1RCB = (reader["L1RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L1RCB"];
//            _L2RCB = (reader["L2RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L2RCB"];
//            _L3RCB = (reader["L3RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L3RCB"];
//            _L4RCB = (reader["L4RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L4RCB"];
//            _L5RCB = (reader["L5RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["L5RCB"];
//            _R1RCB = (reader["R1RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R1RCB"];
//            _R2RCB = (reader["R2RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R2RCB"];
//            _R3RCB = (reader["R3RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R3RCB"];
//            _R4RCB = (reader["R4RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R4RCB"];
//            _R5RCB = (reader["R5RCB"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["R5RCB"];
//            _Label1 = (reader["Label1"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Label1"];
//            _Label2 = (reader["Label2"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Label2"];
//            _Label3 = (reader["Label3"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Label3"];
//            _Label4 = (reader["Label4"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Label4"];
//            _Label5 = (reader["Label5"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Label5"];
//            _Label6 = (reader["Label6"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Label6"];
//            _L1T = (reader["L1T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["L1T"];
//            _L2T = (reader["L2T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["L2T"];
//            _L3T = (reader["L3T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["L3T"];
//            _L4T = (reader["L4T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["L4T"];
//            _L5T = (reader["L5T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["L5T"];
//            _R1T = (reader["R1T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["R1T"];
//            _R2T = (reader["R2T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["R2T"];
//            _R3T = (reader["R3T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["R3T"];
//            _R4T = (reader["R4T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["R4T"];
//            _R5T = (reader["R5T"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["R5T"];
//            _S1BZ = (reader["S1BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S1BZ"];
//            _S2BZ = (reader["S2BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S2BZ"];
//            _S3BZ = (reader["S3BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S3BZ"];
//            _S4BZ = (reader["S4BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S4BZ"];
//            _S5BZ = (reader["S5BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S5BZ"];
//            _S6BZ = (reader["S6BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S6BZ"];
//            _S7BZ = (reader["S7BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S7BZ"];
//            _S8BZ = (reader["S8BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S8BZ"];
//            _S9BZ = (reader["S9BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S9BZ"];
//            _S10BZ = (reader["S10BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S10BZ"];
//            _S11BZ = (reader["S11BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S11BZ"];
//            _S12BZ = (reader["S12BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S12BZ"];
//            _S13BZ = (reader["S13BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S13BZ"];
//            _S14BZ = (reader["S14BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S14BZ"];
//            _S15BZ = (reader["S15BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S15BZ"];
//            _S16BZ = (reader["S16BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S16BZ"];
//            _S17BZ = (reader["S17BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S17BZ"];
//            _S18BZ = (reader["S18BZ"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["S18BZ"];
//            _S1BZZ = (reader["S1BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S1BZZ"];
//            _S2BZZ = (reader["S2BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S2BZZ"];
//            _S3BZZ = (reader["S3BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S3BZZ"];
//            _S4BZZ = (reader["S4BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S4BZZ"];
//            _S5BZZ = (reader["S5BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S5BZZ"];
//            _S6BZZ = (reader["S6BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S6BZZ"];
//            _S7BZZ = (reader["S7BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S7BZZ"];
//            _S8BZZ = (reader["S8BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S8BZZ"];
//            _S9BZZ = (reader["S9BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S9BZZ"];
//            _S10BZZ = (reader["S10BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S10BZZ"];
//            _S11BZZ = (reader["S11BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S11BZZ"];
//            _S12BZZ = (reader["S12BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S12BZZ"];
//            _S13BZZ = (reader["S13BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S13BZZ"];
//            _S14BZZ = (reader["S14BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S14BZZ"];
//            _S15BZZ = (reader["S15BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S15BZZ"];
//            _S16BZZ = (reader["S16BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S16BZZ"];
//            _S17BZZ = (reader["S17BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S17BZZ"];
//            _S18BZZ = (reader["S18BZZ"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["S18BZZ"];
//            _M1QPR = (reader["M1QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M1QPR"];
//            _M2QPR = (reader["M2QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M2QPR"];
//            _M3QPR = (reader["M3QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M3QPR"];
//            _M4QPR = (reader["M4QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M4QPR"];
//            _M5QPR = (reader["M5QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M5QPR"];
//            _M6QPR = (reader["M6QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M6QPR"];
//            _M7QPR = (reader["M7QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M7QPR"];
//            _M8QPR = (reader["M8QPR"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["M8QPR"];
//            _FPType1 = (reader["FPType1"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType1"];
//            _FPType2 = (reader["FPType2"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType2"];
//            _FPType3 = (reader["FPType3"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType3"];
//            _FPType4 = (reader["FPType4"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType4"];
//            _FPType5 = (reader["FPType5"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType5"];
//            _FPType6 = (reader["FPType6"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType6"];
//            _FPType7 = (reader["FPType7"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType7"];
//            _FPType8 = (reader["FPType8"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType8"];
//            _FPType9 = (reader["FPType9"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType9"];
//            _FPType10 = (reader["FPType10"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType10"];
//            _FPType11 = (reader["FPType11"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType11"];
//            _FPType12 = (reader["FPType12"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType12"];
//            _FPType13 = (reader["FPType13"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType13"];
//            _FPType14 = (reader["FPType14"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType14"];
//            _FPType15 = (reader["FPType15"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["FPType15"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, decimal AgencyID, decimal ReportID, decimal LT_BLS, decimal RT_BLS, decimal Page_BLS, decimal SAP, decimal SBP, decimal SCP, decimal SDP, decimal SER, decimal TFRC, decimal EQVP, decimal IQVP, decimal AQVP, decimal CQVP, decimal L1RCP, decimal L2RCP, decimal L3RCP, decimal L4RCP, decimal L5RCP, decimal R1RCP, decimal R2RCP, decimal R3RCP, decimal R4RCP, decimal R5RCP, decimal L1RCP1, decimal L2RCP2, decimal L3RCP3, decimal L4RCP4, decimal L5RCP5, decimal R1RCP1, decimal R2RCP2, decimal R3RCP3, decimal R4RCP4, decimal R5RCP5, decimal ATDR, decimal ATDL, string ATDRA, string ATDRS, string ATDLA, string ATDLS, decimal AC2WP, decimal AC2UP, decimal AC2RP, decimal AC2XP, decimal AC2SP, decimal RCAP, decimal RCHP, decimal RCVP, decimal T1AP, decimal T1BP, decimal M1Q, decimal M2Q, decimal M3Q, decimal M4Q, decimal M5Q, decimal M6Q, decimal M7Q, decimal M8Q, decimal L1RCB, decimal L2RCB, decimal L3RCB, decimal L4RCB, decimal L5RCB, decimal R1RCB, decimal R2RCB, decimal R3RCB, decimal R4RCB, decimal R5RCB, string Label1, string Label2, string Label3, string Label4, string Label5, string Label6, string L1T, string L2T, string L3T, string L4T, string L5T, string R1T, string R2T, string R3T, string R4T, string R5T, decimal S1BZ, decimal S2BZ, decimal S3BZ, decimal S4BZ, decimal S5BZ, decimal S6BZ, decimal S7BZ, decimal S8BZ, decimal S9BZ, decimal S10BZ, decimal S11BZ, decimal S12BZ, decimal S13BZ, decimal S14BZ, decimal S15BZ, decimal S16BZ, decimal S17BZ, decimal S18BZ, string S1BZZ, string S2BZZ, string S3BZZ, string S4BZZ, string S5BZZ, string S6BZZ, string S7BZZ, string S8BZZ, string S9BZZ, string S10BZZ, string S11BZZ, string S12BZZ, string S13BZZ, string S14BZZ, string S15BZZ, string S16BZZ, string S17BZZ, string S18BZZ, decimal M1QPR, decimal M2QPR, decimal M3QPR, decimal M4QPR, decimal M5QPR, decimal M6QPR, decimal M7QPR, decimal M8QPR, decimal FPType1, decimal FPType2, decimal FPType3, decimal FPType4, decimal FPType5, decimal FPType6, decimal FPType7, decimal FPType8, decimal FPType9, decimal FPType10, decimal FPType11, decimal FPType12, decimal FPType13, decimal FPType14, decimal FPType15)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._ReportID = ReportID;
         this._LT_BLS = LT_BLS;
         this._RT_BLS = RT_BLS;
         this._Page_BLS = Page_BLS;
         this._SAP = SAP;
         this._SBP = SBP;
         this._SCP = SCP;
         this._SDP = SDP;
         this._SER = SER;
         this._TFRC = TFRC;
         this._EQVP = EQVP;
         this._IQVP = IQVP;
         this._AQVP = AQVP;
         this._CQVP = CQVP;
         this._L1RCP = L1RCP;
         this._L2RCP = L2RCP;
         this._L3RCP = L3RCP;
         this._L4RCP = L4RCP;
         this._L5RCP = L5RCP;
         this._R1RCP = R1RCP;
         this._R2RCP = R2RCP;
         this._R3RCP = R3RCP;
         this._R4RCP = R4RCP;
         this._R5RCP = R5RCP;
         this._L1RCP1 = L1RCP1;
         this._L2RCP2 = L2RCP2;
         this._L3RCP3 = L3RCP3;
         this._L4RCP4 = L4RCP4;
         this._L5RCP5 = L5RCP5;
         this._R1RCP1 = R1RCP1;
         this._R2RCP2 = R2RCP2;
         this._R3RCP3 = R3RCP3;
         this._R4RCP4 = R4RCP4;
         this._R5RCP5 = R5RCP5;
         this._ATDR = ATDR;
         this._ATDL = ATDL;
         this._ATDRA = ATDRA;
         this._ATDRS = ATDRS;
         this._ATDLA = ATDLA;
         this._ATDLS = ATDLS;
         this._AC2WP = AC2WP;
         this._AC2UP = AC2UP;
         this._AC2RP = AC2RP;
         this._AC2XP = AC2XP;
         this._AC2SP = AC2SP;
         this._RCAP = RCAP;
         this._RCHP = RCHP;
         this._RCVP = RCVP;
         this._T1AP = T1AP;
         this._T1BP = T1BP;
         this._M1Q = M1Q;
         this._M2Q = M2Q;
         this._M3Q = M3Q;
         this._M4Q = M4Q;
         this._M5Q = M5Q;
         this._M6Q = M6Q;
         this._M7Q = M7Q;
         this._M8Q = M8Q;
         this._L1RCB = L1RCB;
         this._L2RCB = L2RCB;
         this._L3RCB = L3RCB;
         this._L4RCB = L4RCB;
         this._L5RCB = L5RCB;
         this._R1RCB = R1RCB;
         this._R2RCB = R2RCB;
         this._R3RCB = R3RCB;
         this._R4RCB = R4RCB;
         this._R5RCB = R5RCB;
         this._Label1 = Label1;
         this._Label2 = Label2;
         this._Label3 = Label3;
         this._Label4 = Label4;
         this._Label5 = Label5;
         this._Label6 = Label6;
         this._L1T = L1T;
         this._L2T = L2T;
         this._L3T = L3T;
         this._L4T = L4T;
         this._L5T = L5T;
         this._R1T = R1T;
         this._R2T = R2T;
         this._R3T = R3T;
         this._R4T = R4T;
         this._R5T = R5T;
         this._S1BZ = S1BZ;
         this._S2BZ = S2BZ;
         this._S3BZ = S3BZ;
         this._S4BZ = S4BZ;
         this._S5BZ = S5BZ;
         this._S6BZ = S6BZ;
         this._S7BZ = S7BZ;
         this._S8BZ = S8BZ;
         this._S9BZ = S9BZ;
         this._S10BZ = S10BZ;
         this._S11BZ = S11BZ;
         this._S12BZ = S12BZ;
         this._S13BZ = S13BZ;
         this._S14BZ = S14BZ;
         this._S15BZ = S15BZ;
         this._S16BZ = S16BZ;
         this._S17BZ = S17BZ;
         this._S18BZ = S18BZ;
         this._S1BZZ = S1BZZ;
         this._S2BZZ = S2BZZ;
         this._S3BZZ = S3BZZ;
         this._S4BZZ = S4BZZ;
         this._S5BZZ = S5BZZ;
         this._S6BZZ = S6BZZ;
         this._S7BZZ = S7BZZ;
         this._S8BZZ = S8BZZ;
         this._S9BZZ = S9BZZ;
         this._S10BZZ = S10BZZ;
         this._S11BZZ = S11BZZ;
         this._S12BZZ = S12BZZ;
         this._S13BZZ = S13BZZ;
         this._S14BZZ = S14BZZ;
         this._S15BZZ = S15BZZ;
         this._S16BZZ = S16BZZ;
         this._S17BZZ = S17BZZ;
         this._S18BZZ = S18BZZ;
         this._M1QPR = M1QPR;
         this._M2QPR = M2QPR;
         this._M3QPR = M3QPR;
         this._M4QPR = M4QPR;
         this._M5QPR = M5QPR;
         this._M6QPR = M6QPR;
         this._M7QPR = M7QPR;
         this._M8QPR = M8QPR;
         this._FPType1 = FPType1;
         this._FPType2 = FPType2;
         this._FPType3 = FPType3;
         this._FPType4 = FPType4;
         this._FPType5 = FPType5;
         this._FPType6 = FPType6;
         this._FPType7 = FPType7;
         this._FPType8 = FPType8;
         this._FPType9 = FPType9;
         this._FPType10 = FPType10;
         this._FPType11 = FPType11;
         this._FPType12 = FPType12;
         this._FPType13 = FPType13;
         this._FPType14 = FPType14;
         this._FPType15 = FPType15;
      }
   }
}