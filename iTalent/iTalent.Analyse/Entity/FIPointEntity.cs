using System;
using System.Data;
using System.Collections;
using iTalent.Analyse.Repository;

namespace iTalent.Analyse.Entity
{
	/// <summary>
	/// Summary description for FIPoint.
	/// </summary>
	public partial class FIPoint
	{

      private Int32 _ID;
      private decimal _AgencyID;
      private decimal _ReportID;
      private DateTime _PointDate;
      private string _PointType;
      private decimal _Point;
      private string _KeyPoint;
      private string _KeyPointAfter;
      private string _AddedBy;
      private Int16 _IsOutDate;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public decimal AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsReportIDNullable
      { get { return true;  } }
      public decimal ReportID
      {
         get { return _ReportID;  }
         set { _ReportID = value; }
      }
      public bool IsPointDateNullable
      { get { return true;  } }
      public DateTime PointDate
      {
         get { return _PointDate;  }
         set { _PointDate = value; }
      }
      public bool IsPointTypeNullable
      { get { return true;  } }
      public string PointType
      {
         get { return _PointType;  }
         set { _PointType = value; }
      }
      public bool IsPointNullable
      { get { return true;  } }
      public decimal Point
      {
         get { return _Point;  }
         set { _Point = value; }
      }
      public bool IsKeyPointNullable
      { get { return true;  } }
      public string KeyPoint
      {
         get { return _KeyPoint;  }
         set { _KeyPoint = value; }
      }
      public bool IsKeyPointAfterNullable
      { get { return true;  } }
      public string KeyPointAfter
      {
         get { return _KeyPointAfter;  }
         set { _KeyPointAfter = value; }
      }
      public bool IsAddedByNullable
      { get { return true;  } }
      public string AddedBy
      {
         get { return _AddedBy;  }
         set { _AddedBy = value; }
      }
      public bool IsIsOutDateNullable
      { get { return true;  } }
      public Int16 IsOutDate
      {
         get { return _IsOutDate;  }
         set { _IsOutDate = value; }
      }

      #endregion

      #region Constructors
      public FIPoint()
      {
         Reset();
      }
      public FIPoint(FIPoint obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._ReportID = obj.ReportID;
	this._PointDate = obj.PointDate;
	this._PointType = obj.PointType;
	this._Point = obj.Point;
	this._KeyPoint = obj.KeyPoint;
	this._KeyPointAfter = obj.KeyPointAfter;
	this._AddedBy = obj.AddedBy;
	this._IsOutDate = obj.IsOutDate;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_decimal;
         _ReportID = EmptyValues.v_decimal;
         _PointDate = EmptyValues.v_DateTime;
         _PointType = EmptyValues.v_string;
         _Point = EmptyValues.v_decimal;
         _KeyPoint = EmptyValues.v_string;
         _KeyPointAfter = EmptyValues.v_string;
         _AddedBy = EmptyValues.v_string;
         _IsOutDate = EmptyValues.v_Int16;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (decimal)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (decimal)reader["ReportID"];
		}
                if(!(reader["PointDate"] is DBNull))
		{
			obj.PointDate = (DateTime)reader["PointDate"];
		}
                if(!(reader["PointType"] is DBNull))
		{
			obj.PointType = (string)reader["PointType"];
		}
                if(!(reader["Point"] is DBNull))
		{
			obj.Point = (decimal)reader["Point"];
		}
                if(!(reader["KeyPoint"] is DBNull))
		{
			obj.KeyPoint = (string)reader["KeyPoint"];
		}
                if(!(reader["KeyPointAfter"] is DBNull))
		{
			obj.KeyPointAfter = (string)reader["KeyPointAfter"];
		}
                if(!(reader["AddedBy"] is DBNull))
		{
			obj.AddedBy = (string)reader["AddedBy"];
		}
                if(!(reader["IsOutDate"] is DBNull))
		{
			obj.IsOutDate = (Int16)reader["IsOutDate"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["AgencyID"];
//            _ReportID = (reader["ReportID"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["ReportID"];
//            _PointDate = (reader["PointDate"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["PointDate"];
//            _PointType = (reader["PointType"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["PointType"];
//            _Point = (reader["Point"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["Point"];
//            _KeyPoint = (reader["KeyPoint"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["KeyPoint"];
//            _KeyPointAfter = (reader["KeyPointAfter"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["KeyPointAfter"];
//            _AddedBy = (reader["AddedBy"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["AddedBy"];
//            _IsOutDate = (reader["IsOutDate"] is DBNull)?DalTools.EmptyValues.v_Int16:(Int16)reader["IsOutDate"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, decimal AgencyID, decimal ReportID, DateTime PointDate, string PointType, decimal Point, string KeyPoint, string KeyPointAfter, string AddedBy, Int16 IsOutDate)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._ReportID = ReportID;
         this._PointDate = PointDate;
         this._PointType = PointType;
         this._Point = Point;
         this._KeyPoint = KeyPoint;
         this._KeyPointAfter = KeyPointAfter;
         this._AddedBy = AddedBy;
         this._IsOutDate = IsOutDate;
      }
   }
}