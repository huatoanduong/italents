using System;
using System.Data;
using System.Collections;
using iTalent.Analyse.Repository;

namespace iTalent.Analyse.Entity
{
	/// <summary>
	/// Summary description for FIFingerType.
	/// </summary>
	public partial class FIFingerType
	{

      private Int32 _ID;
      private string _Name;
      private string _Types;
      private string _Remark;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { _Name = value; }
      }
      public bool IsTypesNullable
      { get { return true;  } }
      public string Types
      {
         get { return _Types;  }
         set { _Types = value; }
      }
      public bool IsRemarkNullable
      { get { return true;  } }
      public string Remark
      {
         get { return _Remark;  }
         set { _Remark = value; }
      }

      #endregion

      #region Constructors
      public FIFingerType()
      {
         Reset();
      }
      public FIFingerType(FIFingerType obj)
      {
	this._ID = obj.ID;
	this._Name = obj.Name;
	this._Types = obj.Types;
	this._Remark = obj.Remark;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _Name = EmptyValues.v_string;
         _Types = EmptyValues.v_string;
         _Remark = EmptyValues.v_string;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["Name"] is DBNull))
		{
			obj.Name = (string)reader["Name"];
		}
                if(!(reader["Types"] is DBNull))
		{
			obj.Types = (string)reader["Types"];
		}
                if(!(reader["Remark"] is DBNull))
		{
			obj.Remark = (string)reader["Remark"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _Name = (reader["Name"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Name"];
//            _Types = (reader["Types"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Types"];
//            _Remark = (reader["Remark"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Remark"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, string Name, string Types, string Remark)
      {
         this._ID = ID;
         this._Name = Name;
         this._Types = Types;
         this._Remark = Remark;
      }
   }
}