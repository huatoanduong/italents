﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IDeviceService : IService
    {
        FIDevice Find(int id);

        bool Add(FIDevice obj);
        bool Update(FIDevice obj);
        bool Delete(FIDevice obj);
    }

    public class FIDeviceService : Pattern.Service, IDeviceService
    {
        public FIDeviceService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FIDevice Find(int id)
        {
            FIDevice obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIDeviceRepository repository = new FIDeviceRepository(this._unitOfWork);
                obj = repository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public bool Add(FIDevice obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIDeviceRepository repository = new FIDeviceRepository(this._unitOfWork);
                FIDevice obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị đã tồn tại"
                        : "Device Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm thiết bị"
                        : "Cannot add new device";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIDevice obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIDeviceRepository repository = new FIDeviceRepository(this._unitOfWork);
                FIDevice obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị không tồn tại"
                        : "Device Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin thiết bị"
                        : "Cannot modify device infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIDevice obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIDeviceRepository repository = new FIDeviceRepository(this._unitOfWork);
                FIDevice obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị không tồn tại"
                        : "Device Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa thiết bị"
                        : "Cannot remove device";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}