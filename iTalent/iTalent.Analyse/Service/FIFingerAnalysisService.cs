﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFIFingerAnalysisService : IService
    {
        FIFingerAnalysis Find(int id);

        bool Add(FIFingerAnalysis obj);
        bool Update(FIFingerAnalysis obj);
        bool Delete(FIFingerAnalysis obj);
    }

    public class FIFingerAnalysisService : Pattern.Service, IFIFingerAnalysisService
    {
        public FIFingerAnalysisService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {

        }

        public FIFingerAnalysis Find(int id)
        {
            FIFingerAnalysis device = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                device = deviceRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                device = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return device;
        }

        public bool Add(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay đã tồn tại"
                        : "Finger analyse Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm phân tích vân tay"
                        : "Cannot add new finger analyse";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay không tồn tại"
                        : "Finger analyse Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin phân tích vân tay"
                        : "Cannot modify finger analyse infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay không tồn tại"
                        : "Finger analyse Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa phân tích vân tay"
                        : "Cannot remove finger analyse";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}