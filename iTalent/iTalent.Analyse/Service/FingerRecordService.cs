﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFingerRecordService : IService
    {
        FIFingerRecord Find(int id);

        bool Add(FIFingerRecord obj);
        bool Update(FIFingerRecord obj);
        bool Delete(FIFingerRecord obj);
    }

    class FingerRecordService : Pattern.Service, IFingerRecordService
    {
        public FingerRecordService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FIFingerRecord Find(int id)
        {
            FIFingerRecord device = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIFingerRecordRepository deviceRepository = new FIFingerRecordRepository(this._unitOfWork);
                device = deviceRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                device = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return device;
        }

        public bool Add(FIFingerRecord obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerRecordRepository deviceRepository = new FIFingerRecordRepository(this._unitOfWork);
                FIFingerRecord obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị đã tồn tại"
                        : "Device Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm thiết bị"
                        : "Cannot add new device";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIFingerRecord obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerRecordRepository deviceRepository = new FIFingerRecordRepository(this._unitOfWork);
                FIFingerRecord obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị không tồn tại"
                        : "Device Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin thiết bị"
                        : "Cannot modify device infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIFingerRecord obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerRecordRepository deviceRepository = new FIFingerRecordRepository(this._unitOfWork);
                FIFingerRecord obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị không tồn tại"
                        : "Device Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa thiết bị"
                        : "Cannot remove device";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}