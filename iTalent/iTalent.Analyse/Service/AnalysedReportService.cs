﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;

namespace iTalent.Analyse.Service
{
    public interface IAnalysedReportService : IService
    {
    }

    public class AnalysedReportService : global::iTalent.Analyse.Pattern.Service, IAnalysedReportService
    {
        public AnalysedReportService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }
    }
}