﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFingerAnalysisService : IService
    {
        FIFingerAnalysis Find(int id);

        bool Add(FIFingerAnalysis obj);
        bool Update(FIFingerAnalysis obj);
        bool Delete(FIFingerAnalysis obj);
    }

    public class FingerAnalysisService : Pattern.Service, IFingerAnalysisService
    {
        public FingerAnalysisService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FIFingerAnalysis Find(int id)
        {
            FIFingerAnalysis fingerAnalysis = null;
            try
            {
                _unitOfWork.OpenConnection();
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                fingerAnalysis = deviceRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                fingerAnalysis = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return fingerAnalysis;
        }

        public bool Add(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerAnalysisRepository fingerAnalysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = fingerAnalysisRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị đã tồn tại"
                        : "Device Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = fingerAnalysisRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm thiết bị"
                        : "Cannot add new device";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerAnalysisRepository fingerAnalysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = fingerAnalysisRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị không tồn tại"
                        : "Device Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = fingerAnalysisRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin thiết bị"
                        : "Cannot modify device infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIFingerAnalysisRepository fingerAnalysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = fingerAnalysisRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã thiết bị không tồn tại"
                        : "Device Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = fingerAnalysisRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa thiết bị"
                        : "Cannot remove device";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}