﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFITemplateService : IService
    {
        FITemplate Find(int id);

        bool Add(FITemplate obj);
        bool Update(FITemplate obj);
        bool Delete(FITemplate obj);

        bool ImportRptx(string filePath, int reportType);
    }

    public class FITemplateService : Pattern.Service, IFITemplateService
    {
        public FITemplateService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FITemplate Find(int id)
        {
            FITemplate obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                IFITemplateRepository repository = new FITemplateRepository(this._unitOfWork);
                obj = repository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public bool Add(FITemplate obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFITemplateRepository repository = new FITemplateRepository(this._unitOfWork);
                FITemplate obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã template đã tồn tại"
                        : "Template Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm template"
                        : "Cannot add new template";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FITemplate obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFITemplateRepository repository = new FITemplateRepository(this._unitOfWork);
                FITemplate obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã template không tồn tại"
                        : "Template Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin template"
                        : "Cannot modify template infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FITemplate obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFITemplateRepository repository = new FITemplateRepository(this._unitOfWork);
                FITemplate obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã template không tồn tại"
                        : "Template Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa template"
                        : "Cannot remove template";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool ImportRptx(string filePath, int reportType)
        {
            bool res = true;
            try
            {
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}
