﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFIAgencyService : IService
    {
        FIAgency Find(int id);
        List<FIAgency> FindAll();

        bool Add(FIAgency obj);
        bool Update(FIAgency obj);
        bool Delete(FIAgency obj);
    }

    public class FIAgencyService : Pattern.Service, IFIAgencyService
    {
        public FIAgencyService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
            
        }

        public FIAgency Find(int id)
        {
            FIAgency obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                IFIAgencyRepository repository = new FIAgencyRepository(this._unitOfWork);
                obj = repository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public List<FIAgency> FindAll()
        {
            try
            {
                _unitOfWork.OpenConnection();
                IFIAgencyRepository repository = new FIAgencyRepository(this._unitOfWork);
                return repository.FindAll();
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
        }

        public bool Add(FIAgency obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIAgencyRepository repository = new FIAgencyRepository(this._unitOfWork);
                FIAgency obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã đại lý đã tồn tại"
                        : "Agency Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm đại lý"
                        : "Cannot add new agency";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIAgency obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIAgencyRepository repository = new FIAgencyRepository(this._unitOfWork);
                FIAgency obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã đại lý không tồn tại"
                        : "Agency Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin đại lý"
                        : "Cannot modify agency infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIAgency obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIAgencyRepository repository = new FIAgencyRepository(this._unitOfWork);
                FIAgency obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã đại lý không tồn tại"
                        : "Agency Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa đại lý"
                        : "Cannot remove agency";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}