﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using iTalent.Analyse.Entity;
using iTalent.DAL.Commons;
using iTalent.DAL.Implements;
using iTalent.DAL.Services;
using iTalent.Utils;
using FIAgency = iTalent.Entities.FIAgency;

//using System.Runtime.InteropServices.WindowsRuntime;

namespace iTalent.Analyse.Service
{
    public interface IAgencyService: IBaseService
    {
        bool Add(FIAgency objAgency);
        bool Update(FIAgency objAgency);
        bool Delete(FIAgency objAgency);

        bool Add(FIAgency objAgency, OleDbTransaction trans);
        bool Update(FIAgency objAgency, OleDbTransaction trans);
        bool Delete(FIAgency objAgency, OleDbTransaction trans);


        long Count();
        long Count(OleDbTransaction trans);

        ICollection<FIAgency> Search(string textSearch);
        
        FIAgency Find(long id);

        bool Login(string username, string password);
        bool Login(string name);
        bool ChangePass(long id, string oldPassword, string newPassword);

        bool ResetPass(long id);

        ICollection<FIAgency> LoadAll();

        FIAgency CreateEntity();
    }

    public class AgencyService: BaseService, IAgencyService
    {
        private readonly IAgencyImplement _implement;
        public AgencyService()
        {
            _implement = new AgencyImplement();
        }
        public FIAgency CreateEntity()
        {
            var obj = new FIAgency
            {
                Country = "VietNam",
                DOB = DateTime.Now.AddYears(-20),
                Day_Actived = 0,
                Dis_Activated = "1",
                Gender = "None",
                Username = "",
                Password = "",
                ID = 0,
                Point = -1,
                ProductKey = "",
                SecKey = "",
                SerailKey = "",
                State = "1"
            };

            return obj;
        }

        public bool Add(FIAgency objAgency)
        {
            try
            {
                objAgency.ID = 0;
                EnsureObjProperties(objAgency, false);
                var tmpObj1 = _implement.Find(objAgency.Username);
                if (tmpObj1 != null)
                {
                    ThrowExceptionWithErrMsg(@"Tên đăng nhập đã có đại lý sử dụng!");
                }

                objAgency.Password = Encrypt(objAgency.Password);
                objAgency.SecKey = Encrypt(objAgency.SecKey);

                return _implement.Add(objAgency);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
        }

        public bool Update(FIAgency objAgency)
        {
            try
            {
                EnsureObjProperties(objAgency, true);
                var tmpObj = _implement.Find(objAgency.ID);
                if (tmpObj == null)
                {
                    ThrowExceptionWithErrMsg("Đại lý không tồn tại!");
                }

                //if (tmpObj != null && objAgency.Username != tmpObj.Username)
                //{
                //    ThrowExceptionWithErrMsg("Không thể thay đổi mã đại lý!");
                //}

                objAgency.Password = Encrypt(objAgency.Password);
                return _implement.Update(objAgency);
            }
            catch (Exception exception)
            {
                ErrMsg = "Có lỗi khi cập nhật!\n" + exception.Message;
                return false;
            }
        }

        public bool Delete(FIAgency objAgency)
        {
            return false;
        }

        public bool Add(FIAgency objAgency, OleDbTransaction trans)
        {

            return _implement.Add(objAgency,trans);
        }

        public bool Update(FIAgency objAgency, OleDbTransaction trans)
        {
            return _implement.Update(objAgency,trans);
        }

        public bool Delete(FIAgency objAgency, OleDbTransaction trans)
        {
            return false;
        }

        public long Count()
        {
            return 0;
        }

        public long Count(OleDbTransaction trans)
        {
            return 0;
        }

        public ICollection<FIAgency> Search(string textSearch)
        {
            return _implement.Search(textSearch);
        }

        public FIAgency Find(long id)
        {
           return _implement.Find(id);
        }

        public bool Login(string username, string password)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(username)
                    || string.IsNullOrWhiteSpace(password))
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage ? @"Vui lòng nhập mật khẩu!" : @"Please input password!");
                }

                password = Encrypt(password);
                FIAgency obj = _implement.Login(username, password);
                if (obj == null)
                {
                    if(VietNameLanguage)
                        ThrowExceptionWithErrMsg(@"Mã đại lý hoặc mật khẩu không đúng!" + "\n" +
                                             " Vui lòng nhập lại mã đại lý và mật khẩu của bạn!");
                    else
                        ThrowExceptionWithErrMsg(@"Incorrect agency ID or password!" + "\n" +
                                            " Please input again!");
                }
                //EnsureObjProperties(obj, true);

                ICurrentSessionService.CurAgency = obj;
                ICurrentSessionService.Username = obj.Username;
                ICurrentSessionService.UserId = obj.ID;
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                return false;
            }
        }

        public bool Login(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage ? @"Vui lòng nhập mật khẩu!" : @"Please input password!");
                }


                FIAgency obj = _implement.FindByName(name);
                if (obj == null)
                {
                    if (VietNameLanguage)
                        ThrowExceptionWithErrMsg(@"Mật khẩu không đúng!" + "\n" +
                                             " Vui lòng nhập lại mật khẩu của bạn!");
                    else
                        ThrowExceptionWithErrMsg(@"Incorrect password!" + "\n" +
                                            " Please input again!");
                }

                //EnsureObjProperties(obj, true);

                ICurrentSessionService.CurAgency = obj;
                ICurrentSessionService.Username = obj.Username;
                ICurrentSessionService.UserId = obj.ID;
                return true;
            }
            catch (Exception ex)
            {
                ErrMsg = ex.Message;
                return false;
            }
        }

        public bool ChangePass(long id, string oldPassword, string newPassword)
        {
            try
            {
                if (id<=0)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                        ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                        : @"Agency's information dosen't exists!");
                }

                if (string.IsNullOrWhiteSpace(oldPassword) || string.IsNullOrWhiteSpace(newPassword))
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                       ? @"Vui lòng nhập mật khẩu cũ và mới!"
                       : @"Please input old password and new password!");
                }

                if (oldPassword == newPassword)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Vui lòng nhập mật khẩu cũ khác với mật khẩu mới!"
                      : @"Please input old password different new password!");
                }

                newPassword = Encrypt(newPassword);
                var obj = _implement.Find(id);
                if (obj == null)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Sai tên đăng nhập hoặc mật khẩu cũ"
                      : @"Incorrect agency ID or old password!");
                }
                EnsureObjProperties(obj, true);
                if (obj != null)
                {
                    obj.Password = newPassword;
                    _implement.ChangePass(id,newPassword);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public bool ResetPass(long id)
        {
            try
            {
                if (id <= 0)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                       ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                       : @"Agency's information dosen't exists!");
                }

                var tmpObj = _implement.Find(id);
                if (tmpObj == null)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                       ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                       : @"Agency's information dosen't exists!");
                }
                EnsureObjProperties(tmpObj, true);
                if (tmpObj != null)
                {
                    _implement.ResetPass(tmpObj.ID);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public ICollection<FIAgency> LoadAll()
        {
            return _implement.LoadAll();
        }

        private const string KeyEncrypt = "friek@ouer048!940#kdpe@";
        private string Encrypt(string message)
        {
            return Security.Encrypt(message, KeyEncrypt);
        }

        public string Decrypt(string message)
        {
            return Security.Decrypt(message, KeyEncrypt);
        }

        public bool EnsureObjProperties(FIAgency obj, bool isUpdate)
        {
            //if (obj.Username.Length < 7 || obj.Username.Length < 0)
            //{
            //    ThrowExceptionWithErrMsg(VietNameLanguage
            //           ? @"Mã đại lý phải đúng 7 ký tự!"
            //           : @"Agency's username must be at least 7 characters long!");
            //}

            if (string.IsNullOrWhiteSpace(obj.Name))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Vui lòng nhập tên!"
                      : @"Please input your name!");
            }

            //if (string.IsNullOrWhiteSpace(obj.Email))
            //{
            //    ThrowExceptionWithErrMsg(VietNameLanguage
            //          ? @"Vui lòng nhập email!"
            //          : @"Please input your email!");
            //}

            //if (string.IsNullOrWhiteSpace(obj.ProductKey))
            //{
            //    ThrowExceptionWithErrMsg("Vui lòng nhập key bản quyền");
            //}

            //if (string.IsNullOrWhiteSpace(obj.SecKey))
            //{
            //    ThrowExceptionWithErrMsg("Vui lòng nhập phần cứng");
            //}

            //if (string.IsNullOrWhiteSpace(obj.SerailKey))
            //{
            //    ThrowExceptionWithErrMsg("Vui lòng nhập phần cứng");
            //}

            //if (string.IsNullOrWhiteSpace(obj.Address1))
            //{
            //    ThrowExceptionWithErrMsg("Vui lòng nhập địa chỉ");
            //}

            //if (string.IsNullOrWhiteSpace(obj.MobileNo))
            //{
            //    ThrowExceptionWithErrMsg("Vui lòng nhập số điện thoại");
            //}

            if (string.IsNullOrWhiteSpace(obj.Username))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Vui lòng nhập tên đăng nhập!"
                      : @"Please input your username!");
            }

            if (string.IsNullOrWhiteSpace(obj.Password))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                     ? @"Vui lòng nhập mật khẩu đăng nhập!"
                     : @"Please input your password!");
            }

            if (string.IsNullOrWhiteSpace(obj.SaveImages))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                    ? @"Vui lòng chọn nơi lưu vân tay!"
                    : @"Please choice your local save images!");
            }


            var idGreater0 = obj.ID > 0;

            if (isUpdate != idGreater0) // XOR here
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                   ? @"Đại lý không tồn tại!"
                   : @"Agency dosen't exists");
            }
            if (!isUpdate)
            {
                var exist = _implement.Find(obj.ID);
                if (exist!=null)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                 ? @"Đại lý đã tồn tại trong hệ thống!"
                 : @"Agency was exists in system!");
                    ThrowExceptionWithErrMsg("");
                }
            }
            return true;
        }
    }
}
