﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFIPrintRecordService : IService
    {
        FIPrintRecord Find(int id);

        bool Add(FIPrintRecord obj);
        bool Update(FIPrintRecord obj);
        bool Delete(FIPrintRecord obj);
    }

    public class FIPrintRecordService : Pattern.Service, IFIPrintRecordService
    {
        public FIPrintRecordService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FIPrintRecord Find(int id)
        {
            FIPrintRecord obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIPrintRecordRepository repository = new FIPrintRecordRepository(this._unitOfWork);
                obj = repository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public bool Add(FIPrintRecord obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIPrintRecordRepository repository = new FIPrintRecordRepository(this._unitOfWork);
                FIPrintRecord obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã FIPrintRecord đã tồn tại"
                        : "FIPrintRecord Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm FIPrintRecord"
                        : "Cannot add new FIPrintRecord";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIPrintRecord obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIPrintRecordRepository repository = new FIPrintRecordRepository(this._unitOfWork);
                FIPrintRecord obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã FIPrintRecord không tồn tại"
                        : "FIPrintRecord Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin FIPrintRecord"
                        : "Cannot modify FIPrintRecord infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIPrintRecord obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIPrintRecordRepository repository = new FIPrintRecordRepository(this._unitOfWork);
                FIPrintRecord obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã FIPrintRecord không tồn tại"
                        : "FIPrintRecord Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa FIPrintRecord"
                        : "Cannot remove FIPrintRecord";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}