﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.DAL.Commons;

namespace iTalent.Analyse.Service
{
    public interface IFIPointService : IService
    {
        FIPoint Find(int id);

        bool Add(FIPoint obj);
        bool Update(FIPoint obj);
        bool Delete(FIPoint obj);
    }

    public class FIPointService : Pattern.Service, IFIPointService
    {
        public FIPointService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FIPoint Find(int id)
        {
            FIPoint obj = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIPointRepository repository = new FIPointRepository(this._unitOfWork);
                obj = repository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                obj = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return obj;
        }

        public bool Add(FIPoint obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIPointRepository repository = new FIPointRepository(this._unitOfWork);
                FIPoint obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã FIPoint đã tồn tại"
                        : "FIPoint Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm FIPoint"
                        : "Cannot add new FIPoint";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIPoint obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIPointRepository repository = new FIPointRepository(this._unitOfWork);
                FIPoint obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã FIPoint không tồn tại"
                        : "FIPoint Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin FIPoint"
                        : "Cannot modify FIPoint infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIPoint obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIPointRepository repository = new FIPointRepository(this._unitOfWork);
                FIPoint obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã FIPoint không tồn tại"
                        : "FIPoint Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa FIPoint"
                        : "Cannot remove FIPoint";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}
