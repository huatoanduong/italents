﻿using System;
using System.Collections.Generic;
using iTalent.DAL.Implements;
using iTalent.DAL.Services;
using iTalent.Entities;
using iTalent.Utils;

namespace iTalent.Analyse.Service
{
    public interface ICustomerService : IBaseService
    {
        bool Add(FICustomer objCustomer);
        bool Update(FICustomer objCustomer);
        bool Delete(FICustomer objCustomer);
        ICollection<FICustomer> Search(string textSearch);
        ICollection<FICustomer> SearchByReportId(string textSearch);
        ICollection<FICustomer> SearchByName(string textSearch);
        ICollection<FICustomer> SearchByParent(string textSearch);
        ICollection<FICustomer> SearchByTel(string textSearch);
        ICollection<FICustomer> SearchByAddress(string textSearch);
        ICollection<FICustomer> Search(DateTime fromDate, DateTime toDate);
        ICollection<FICustomer> Search(string textSearch, DateTime fromDate, DateTime toDate);
        FICustomer Find(long id);
        FICustomer FindByName(string name);
        ICollection<FICustomer> FindByAgency(long agencyId);
        FICustomer FindByReportId(string reportId);
        ICollection<FICustomer> LoadAll();
        FICustomer CreateEntity();
    }
    public class CustomerService:BaseService,ICustomerService
    {
        private readonly ICustomerImplement _implement;
        private readonly IAgencyImplement _agencyImplement;

        public CustomerService()
        {
            _implement = new CustomerImplement();
            _agencyImplement = new AgencyImplement();
        }
        public FICustomer CreateEntity()
        {
            var obj = new FICustomer
            {
                ID = 0,
                DOB = DateTime.Now.AddYears(-20),
                Country = "Việt Nam",
                Date = DateTime.Now,
                Gender = "None",
                Mobile = "",
                Tel = "",
                Parent = "",
                Address1 = "",
                Email = "",
                Remark = "",
                ZipPosTalCode = "",
                State = "None",
                City = "",
                ReportID = "",
                Name = ""
            };

            return obj;
        }
        public bool Add(FICustomer objCustomer)
        {
            try
            {
                objCustomer.ID = 0;
                if (VietNameLanguage)
                {
                    if (objCustomer.Gender.Contains("Male"))
                        objCustomer.Gender = "Nam";
                    if (objCustomer.Gender.Contains("Female"))
                        objCustomer.Gender = "Nữ";
                }
                else
                {
                    if (objCustomer.Gender.Contains("Nam"))
                        objCustomer.Gender = "Male";
                    if (objCustomer.Gender.Contains("Nữ"))
                        objCustomer.Gender = "Female";
                }
                    
                EnsureObjProperties(objCustomer, false);
                objCustomer.Date = DateTimeUtil.GetCurrentTime();

                return _implement.Add(objCustomer);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
        }

        public bool Update(FICustomer objCustomer)
        {
            try
            {
                EnsureObjProperties(objCustomer, true);
                var tmpObj = _implement.Find(objCustomer.ID);
                if (tmpObj == null)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Không thể xác định được khách hàng hoặc không tồn tại khách hàng này!"
                      : @"Customer's information dosen't exists!");
                }

                objCustomer.Date = DateTimeUtil.GetCurrentTime();
                return _implement.Update(objCustomer);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
        }

        public bool Delete(FICustomer objCustomer)
        {
            try
            {
                var tmpObj = _implement.Find(objCustomer.ID);
                if (tmpObj == null)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                        ? @"Không thể xác định được khách hàng hoặc không tồn tại khách hàng này!"
                        : @"Customer's information dosen't exists!");
                }
                return _implement.Delete(objCustomer);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
        }

        public ICollection<FICustomer> Search(string textSearch)
        {
            return _implement.Search(textSearch);
        }

        public ICollection<FICustomer> SearchByReportId(string textSearch)
        {
            return _implement.SearchByReportId(textSearch);
        }

        public ICollection<FICustomer> SearchByName(string textSearch)
        {
            return _implement.SearchByName(textSearch);
        }

        public ICollection<FICustomer> SearchByParent(string textSearch)
        {
            return _implement.SearchByParent(textSearch);
        }

        public ICollection<FICustomer> SearchByTel(string textSearch)
        {
            return _implement.SearchByTel(textSearch);
        }

        public ICollection<FICustomer> SearchByAddress(string textSearch)
        {
            return _implement.SearchByAddress(textSearch);
        }

        public ICollection<FICustomer> Search(DateTime fromDate, DateTime toDate)
        {
            return _implement.Search(fromDate,toDate);
        }

        public ICollection<FICustomer> Search(string textSearch, DateTime fromDate, DateTime toDate)
        {
            return _implement.Search(textSearch,fromDate, toDate);
        }

        public FICustomer Find(long id)
        {
            return _implement.Find(id);
        }

        public FICustomer FindByName(string name)
        {
            return _implement.FindByName(name);
        }

        public ICollection<FICustomer> FindByAgency(long agencyId)
        {
            return _implement.FindByAgency(agencyId);
        }

        public FICustomer FindByReportId(string reportId)
        {
            return _implement.FindByReportId(reportId);
        }

        public ICollection<FICustomer> LoadAll()
        {
            return _implement.LoadAll();
        }

        public bool EnsureObjProperties(FICustomer obj, bool isUpdate)
        {
            if (string.IsNullOrWhiteSpace(obj.Name))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                       ? @"Vui lòng nhập tên khách hàng!"
                       : @"Please input name customer!");
            }
            if (string.IsNullOrWhiteSpace(obj.Email))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                       ? @"Vui lòng nhập email!"
                       : @"Please input email customer!");
            }
            if (string.IsNullOrWhiteSpace(obj.Tel))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                       ? @"Vui lòng nhập số điện thoại!"
                       : @"Please input telephone customer!");
            }
            if (string.IsNullOrWhiteSpace(obj.ReportID))
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Mã report rỗng!"
                      : @"ID report is empty!");
            }

            var idGreater0 = obj.ID > 0;

            if (isUpdate != idGreater0) // XOR here
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Không thể xác định được khách hàng hoặc không tồn tại khách hàng này!"
                      : @"Customer's information dosen't exists!");
            }

            var exist = _agencyImplement.Find(obj.AgencyID);
            if (exist==null)
            {
                ThrowExceptionWithErrMsg(VietNameLanguage
                      ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                      : @"Agency's information dosen't exists!");
            }

            var a = _implement.FindByReportId(obj.ReportID);
            if (a!=null)
            {
                if (!isUpdate)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                     ? @"Mã Report của khách hàng này đã có trong hệ thống!"
                     : @"Customer's information was exists in systems!");
                }
            }

            if (!isUpdate)
            {
                var b = _implement.FindByReportId(obj.ReportID);
                if (b!=null)
                {
                    ThrowExceptionWithErrMsg(VietNameLanguage
                    ? @"Mã Report của khách hàng này đã có trong hệ thống!"
                    : @"Customer's information was exists in systems!");
                }
            }

            return true;
        }
    }
}
