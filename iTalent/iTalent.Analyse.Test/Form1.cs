﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTalent.Analyse.Entity;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Utils;

namespace iTalent.Analyse.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //string dbFile = "FINGERPRINTANALYSIS.mdb";
            //UnitOfWorkFactory.DesDirNameDatabase =
            //    Path.GetFullPath(dbFile);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.OpenConnection();
                FIDeviceService service = new FIDeviceService(unitOfWork);
                FIDevice obj = new FIDevice();

                obj.ID = 1;
                obj.AgencyID = 1;
                obj.Devicename = "B";
                obj.ActiveDate = DateTime.Now;
                obj.ProductKey = "B";
                obj.SerailKey = "B";
                obj.SecKey = "B";
                obj.Max = "B";
                obj.Min = "C";

                bool res = service.Add(obj);
                if (res)
                {
                    MessageBox.Show("Done");
                }
                else
                {
                    MessageBox.Show(service.ErrMsg);
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(UnitOfWorkFactory.DesDirNameDatabase);

            //using (IAgencyService service = new AgencyService())
            //{
            //    iTalent.Entities.FIAgency agency = service.Find(2);
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                FIDeviceService service = new FIDeviceService(unitOfWork);
                FIDevice device = service.Find(1);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtKeyXacNhan.Text = GeneratePos.KeyGenerate();
        }
    }
}