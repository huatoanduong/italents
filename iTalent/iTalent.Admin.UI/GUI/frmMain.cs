﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.DAL.Commons;
using iTalent.DAL.FlashForm;
//using iTalent.Analyse.Entity;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entities;

namespace iTalent.Admin.UI.GUI
{
    public partial class frmMain : C4FunForm
    {
        public bool Issuccess { get; set; }
        private Config _config;
        private BaseForm _baseForm;
        private string _dirFile;
        public FICustomer CurrCustomer { get; set; }
        public ICollection<FICustomer> ListCustomers { get; set; }
        public FIAgency CurrAgency { get; set; }

        public frmMain()
        {
            InitializeComponent();
        }

        #region Methods

        private void LoadAll()
        {
            try
            {
                Hide();

                _config = new Config();
                BaseForm.Frm = this;
                BaseForm.DgView = dgvData;
                BaseForm.ListconControls = new List<Control>
                {
                    btnAdd,
                    btnConfig,
                    btnEdit,
                    btnExport,
                    btnRefresh,
                    labSearch,
                    btnConvertTool,
                    btnCreateKey
                };
                _baseForm = new BaseForm();

                //if (_config.LocalSaveImages == "")
                //{
                //    FrmConfig frmC = new FrmConfig();
                //    frmC.ShowDialog();
                //}



                //IDbConnectUtil connect = new DbConnectUtil();

                //_baseForm.CheckDevice(true);
                //CheckRegisted();

                FrmFlash.ShowSplash();
                Application.DoEvents();

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    string sourceDirName = ICurrentSessionService.DesDirNameSource;
                    if (!Directory.Exists(sourceDirName))
                        Directory.CreateDirectory(sourceDirName);

                    string sourceDirNameTemp = ICurrentSessionService.DesDirNameTemp;
                    if (!Directory.Exists(sourceDirNameTemp))
                        Directory.CreateDirectory(sourceDirNameTemp);

                    string sourceTemplate = ICurrentSessionService.DesDirNameTemplate;
                    if (!Directory.Exists(sourceTemplate))
                        Directory.CreateDirectory(sourceTemplate);

                    WindowState = FormWindowState.Maximized;

                    dgvData.AutoGenerateColumns = false;
                    LoadData();

                    _dirFile = sourceDirName;
                }

                FrmFlash.CloseSplash();
                Show();
                Activate();

            }
            catch (Exception ex)
            {
                FrmFlash.CloseSplash();
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }
        private void LoadData()
        {
            try
            {
                using (IAgencyService service = new AgencyService())
                {
                    dgvData.DataSource = service.LoadAll();
                    dgvData.Refresh();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }
        private void LoadData(ICollection<FIAgency> listAgencies)
        {
            try
            {
                dgvData.AutoGenerateColumns = false;
                dgvData.SuspendLayout();
                dgvData.DataSource = listAgencies;
                dgvData.Refresh();
                dgvData.ResumeLayout();
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }

        #endregion

        #region Events

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (Directory.Exists(ICurrentSessionService.DesDirNameTemp))
                    Directory.Delete(ICurrentSessionService.DesDirNameTemp, true);
            }
            catch
            {
                //
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            //Hide();
            FrmConfig frm = new FrmConfig { StartPosition = FormStartPosition.CenterScreen };
            frm.ShowDialog(this);
          
            if (frm.Issuccess)
                Application.Restart();

            //Show();

        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            var frm = new FrmAgency {StartPosition = FormStartPosition.CenterScreen};
            frm.ShowDialog(this);
            if (frm.Issuccess)
                LoadData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                long id = long.Parse(row.Cells[0].Value.ToString());

                using (IAgencyService service = new AgencyService())
                {
                    CurrAgency = service.Find(id);
                    if (CurrAgency == null)
                    {
                        MessageBox.Show(@"Vui lòng chọn một trong các đại lý để cập nhật!", @"Thông Báo",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        return;
                    }
                }

                var frm = new FrmAgency(CurrAgency);
                frm.ShowDialog(this);
                if (frm.Issuccess)
                    LoadData();
                Activate();
            }
            catch (Exception)
            {
                
            }
        }

        private void btnCreateKey_Click(object sender, EventArgs e)
        {

        }

        private void btnConvertTool_Click(object sender, EventArgs e)
        {
            Hide();
            frmConvert frm = new frmConvert {StartPosition = FormStartPosition.CenterScreen};
            frm.ShowDialog(this);
            Show();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SmartSearch(txtSearch.Text);
        }

        private void dgvData_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                long id = long.Parse(row.Cells[0].Value.ToString());
                using (IAgencyService service = new AgencyService())
                {
                    CurrAgency = service.Find(id);
                }
            }
            catch (Exception)
            {
                CurrAgency = null;
            }
        }

        private void dgvData_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            dgvData.Rows[e.RowIndex].Cells[1].Value = e.RowIndex + 1;
            dgvData.Rows[e.RowIndex].Tag = e.RowIndex+"+"+dgvData.Rows[e.RowIndex].Cells[2].Value+"+"+ dgvData.Rows[e.RowIndex].Cells[3].Value + "+" + dgvData.Rows[e.RowIndex].Cells[4].Value + "+" + dgvData.Rows[e.RowIndex].Cells[5].Value + "+" + dgvData.Rows[e.RowIndex].Cells[6].Value + "+" + dgvData.Rows[e.RowIndex].Cells[7].Value + "+" + dgvData.Rows[e.RowIndex].Cells[8].Value;
        }

        #endregion

        #region TimKiem

        public void SmartSearch(string compare)
        {
            if (compare == "")
            {
                LoadData();
            }

            string compareUnsig = ConverToUnsign1(compare).Replace(" ", "").ToLower();
            string comparenomarl = compare.Replace(" ", "").ToLower();

            foreach (string timKiem in from DataGridViewRow row in dgvData.Rows select row.Tag.ToString().Trim().ToLower())
            {
                //Bo dau
                string bodau = ConverToUnsign1(timKiem);
                int i = int.Parse(bodau.Split('+')[0]);
               // dgvData.Rows[i].Visible = false;
                dgvData.Rows[i].Height = 0; //bodau.Contains(compareUnsig) || timKiem.Contains(comparenomarl);
            }
        }

        public static string ConverToUnsign1(string s)
        {
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string XoaKhoangTrang(string s)
        {
            return s.Where(t => t != 32)
                .Aggregate("", (current, t) => current + t.ToString(CultureInfo.InvariantCulture).Trim());
        }

        #endregion

       
    }
}
