﻿namespace iTalent.Admin.UI.GUI
{
    partial class FrmAgency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAgency));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.buttonSpecHeaderGroup1 = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtPassEmail = new System.Windows.Forms.TextBox();
            this.labPassword = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.labID = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.labEmail = new System.Windows.Forms.Label();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.labCellPhone = new System.Windows.Forms.Label();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.labPhone = new System.Windows.Forms.Label();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.labCity = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.labAddress = new System.Windows.Forms.Label();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.labName = new System.Windows.Forms.Label();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.txtKeyCheck = new System.Windows.Forms.TextBox();
            this.labKeyXacNhan = new System.Windows.Forms.Label();
            this.txtKeyRegister = new System.Windows.Forms.TextBox();
            this.labKey = new System.Windows.Forms.Label();
            this.btnGenerateKey = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.buttonSpecHeaderGroup1});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnGenerateKey);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtKeyCheck);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labKeyXacNhan);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtKeyRegister);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labKey);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox2);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtPassEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labPassword);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtID);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labID);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiDong);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labCellPhone);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDienThoai);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labPhone);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtThanhPho);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labCity);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiaChi);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labAddress);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTen);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labName);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnLuu);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(752, 541);
            this.c4FunHeaderGroup1.StateNormal.Back.Color1 = System.Drawing.Color.Honeydew;
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 150;
            this.c4FunHeaderGroup1.Tag = "Thông Tin Người Dùng";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Agency Information";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.Admin.UI.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "";
            // 
            // buttonSpecHeaderGroup1
            // 
            this.buttonSpecHeaderGroup1.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.buttonSpecHeaderGroup1.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.buttonSpecHeaderGroup1.Click += new System.EventHandler(this.buttonSpecHeaderGroup1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::iTalent.Admin.UI.Properties.Resources.logo3;
            this.pictureBox2.Location = new System.Drawing.Point(395, 329);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(250, 99);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 204;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::iTalent.Admin.UI.Properties.Resources.Logo_wellgen;
            this.pictureBox1.Location = new System.Drawing.Point(395, 223);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 99);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 203;
            this.pictureBox1.TabStop = false;
            // 
            // txtPassEmail
            // 
            this.txtPassEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtPassEmail.Location = new System.Drawing.Point(498, 428);
            this.txtPassEmail.Name = "txtPassEmail";
            this.txtPassEmail.Size = new System.Drawing.Size(76, 29);
            this.txtPassEmail.TabIndex = 201;
            this.txtPassEmail.Visible = false;
            // 
            // labPassword
            // 
            this.labPassword.BackColor = System.Drawing.Color.Transparent;
            this.labPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPassword.Location = new System.Drawing.Point(391, 431);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(101, 21);
            this.labPassword.TabIndex = 202;
            this.labPassword.Tag = "Mật khẩu :";
            this.labPassword.Text = "Password :";
            this.labPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labPassword.Visible = false;
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.White;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.Enabled = false;
            this.txtID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.ForeColor = System.Drawing.Color.Red;
            this.txtID.Location = new System.Drawing.Point(498, 461);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(76, 29);
            this.txtID.TabIndex = 193;
            this.txtID.Text = "0";
            this.txtID.Visible = false;
            // 
            // labID
            // 
            this.labID.BackColor = System.Drawing.Color.Transparent;
            this.labID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labID.Location = new System.Drawing.Point(431, 464);
            this.labID.Name = "labID";
            this.labID.Size = new System.Drawing.Size(61, 21);
            this.labID.TabIndex = 200;
            this.labID.Tag = "Mã :";
            this.labID.Text = "ID :";
            this.labID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labID.Visible = false;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(120, 188);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(250, 29);
            this.txtEmail.TabIndex = 192;
            // 
            // labEmail
            // 
            this.labEmail.BackColor = System.Drawing.Color.Transparent;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(50, 192);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(64, 21);
            this.labEmail.TabIndex = 199;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiDong
            // 
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(120, 223);
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.Size = new System.Drawing.Size(250, 29);
            this.txtDiDong.TabIndex = 191;
            // 
            // labCellPhone
            // 
            this.labCellPhone.BackColor = System.Drawing.Color.Transparent;
            this.labCellPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCellPhone.Location = new System.Drawing.Point(14, 227);
            this.labCellPhone.Name = "labCellPhone";
            this.labCellPhone.Size = new System.Drawing.Size(100, 21);
            this.labCellPhone.TabIndex = 198;
            this.labCellPhone.Tag = "Di Động :";
            this.labCellPhone.Text = "Mobile :";
            this.labCellPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(120, 257);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(250, 29);
            this.txtDienThoai.TabIndex = 190;
            // 
            // labPhone
            // 
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(18, 261);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(96, 21);
            this.labPhone.TabIndex = 197;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Tel :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(120, 399);
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.Size = new System.Drawing.Size(250, 29);
            this.txtThanhPho.TabIndex = 189;
            // 
            // labCity
            // 
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(23, 403);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(93, 21);
            this.labCity.TabIndex = 196;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(120, 292);
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(250, 99);
            this.txtDiaChi.TabIndex = 188;
            // 
            // labAddress
            // 
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(21, 333);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(93, 21);
            this.labAddress.TabIndex = 195;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(120, 153);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(250, 29);
            this.txtTen.TabIndex = 187;
            // 
            // labName
            // 
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(49, 157);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(65, 21);
            this.labName.TabIndex = 194;
            this.labName.Tag = "Tên :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnLuu
            // 
            this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnLuu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLuu.FlatAppearance.BorderSize = 0;
            this.btnLuu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnLuu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.ForeColor = System.Drawing.Color.White;
            this.btnLuu.Image = global::iTalent.Admin.UI.Properties.Resources.save;
            this.btnLuu.Location = new System.Drawing.Point(191, 440);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(179, 50);
            this.btnLuu.TabIndex = 186;
            this.btnLuu.Tag = "Đăng Ký";
            this.btnLuu.Text = "Save";
            this.btnLuu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLuu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtKeyCheck
            // 
            this.txtKeyCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtKeyCheck.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtKeyCheck.ForeColor = System.Drawing.Color.Red;
            this.txtKeyCheck.Location = new System.Drawing.Point(120, 97);
            this.txtKeyCheck.Name = "txtKeyCheck";
            this.txtKeyCheck.ReadOnly = true;
            this.txtKeyCheck.Size = new System.Drawing.Size(250, 50);
            this.txtKeyCheck.TabIndex = 207;
            this.txtKeyCheck.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labKeyXacNhan
            // 
            this.labKeyXacNhan.BackColor = System.Drawing.Color.Transparent;
            this.labKeyXacNhan.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labKeyXacNhan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labKeyXacNhan.Location = new System.Drawing.Point(120, 72);
            this.labKeyXacNhan.Name = "labKeyXacNhan";
            this.labKeyXacNhan.Size = new System.Drawing.Size(250, 21);
            this.labKeyXacNhan.TabIndex = 208;
            this.labKeyXacNhan.Text = ".:Key Verification :";
            // 
            // txtKeyRegister
            // 
            this.txtKeyRegister.BackColor = System.Drawing.Color.White;
            this.txtKeyRegister.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyRegister.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtKeyRegister.Location = new System.Drawing.Point(395, 97);
            this.txtKeyRegister.Multiline = true;
            this.txtKeyRegister.Name = "txtKeyRegister";
            this.txtKeyRegister.Size = new System.Drawing.Size(250, 120);
            this.txtKeyRegister.TabIndex = 205;
            this.txtKeyRegister.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labKey
            // 
            this.labKey.BackColor = System.Drawing.Color.Transparent;
            this.labKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labKey.Location = new System.Drawing.Point(395, 72);
            this.labKey.Name = "labKey";
            this.labKey.Size = new System.Drawing.Size(250, 21);
            this.labKey.TabIndex = 206;
            this.labKey.Text = ".:Key Register :";
            // 
            // btnGenerateKey
            // 
            this.btnGenerateKey.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGenerateKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnGenerateKey.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGenerateKey.FlatAppearance.BorderSize = 0;
            this.btnGenerateKey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnGenerateKey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnGenerateKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateKey.ForeColor = System.Drawing.Color.White;
            this.btnGenerateKey.Image = global::iTalent.Admin.UI.Properties.Resources.keys;
            this.btnGenerateKey.Location = new System.Drawing.Point(648, 97);
            this.btnGenerateKey.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerateKey.Name = "btnGenerateKey";
            this.btnGenerateKey.Size = new System.Drawing.Size(94, 120);
            this.btnGenerateKey.TabIndex = 209;
            this.btnGenerateKey.Tag = "...";
            this.btnGenerateKey.Text = "Generate Key";
            this.btnGenerateKey.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnGenerateKey.UseVisualStyleBackColor = false;
            this.btnGenerateKey.Click += new System.EventHandler(this.btnGenerateKey_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(750, 72);
            this.label1.TabIndex = 210;
            this.label1.Tag = "Tên :";
            this.label1.Text = "Note : Pares key register in text box key register. \r\n           Press button Gen" +
    "erate key.\r\n           Copy key at textbox key verification and send key to agen" +
    "cy.";
            // 
            // FrmAgency
            // 
            this.AcceptButton = this.btnLuu;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 541);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAgency";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StateCommon.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)(((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.Tag = ".:THÔNG TIN KHÁCH HÀNG";
            this.Load += new System.EventHandler(this.FrmAgency_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup buttonSpecHeaderGroup1;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label labID;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.Label labCellPhone;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.TextBox txtPassEmail;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtKeyCheck;
        private System.Windows.Forms.Label labKeyXacNhan;
        private System.Windows.Forms.TextBox txtKeyRegister;
        private System.Windows.Forms.Label labKey;
        private System.Windows.Forms.Button btnGenerateKey;
        private System.Windows.Forms.Label label1;
    }
}