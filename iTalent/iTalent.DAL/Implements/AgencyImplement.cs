﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using iTalent.DAL.Commons;
using iTalent.Entities;


namespace iTalent.DAL.Implements
{
    public interface IAgencyImplement
    {
        bool Add(FIAgency objAgency);
        bool Update(FIAgency objAgency);
        bool Delete(FIAgency objAgency);

        bool Add(FIAgency objAgency,OleDbTransaction trans);
        bool Update(FIAgency objAgency, OleDbTransaction trans);
        bool Delete(FIAgency objAgency, OleDbTransaction trans);
      

        long Count();
        long Count(OleDbTransaction trans);

        ICollection<FIAgency> Search(string textSearch);
      
        FIAgency Find(long id);

        FIAgency Find(string username);

        FIAgency Login(string username, string password);

        FIAgency FindByName(string name);

        FIAgency FindBySeckey(string seckey);

        bool ChangePass(long id, string password);

        bool ResetPass(long id);

        ICollection<FIAgency> LoadAll();

    }

    public class AgencyImplement : DbConnectUtil, IAgencyImplement
    {

        //public AgencyImplement()
        //{

        //}

        /// <summary>
        /// add Fields : Username,Password,Name,Address1,City,SaveImages,ProductKey,,SerailKey,SecKey
        /// </summary>
        /// <param name="objAgency"></param>
        /// <returns></returns>
        public bool Add(FIAgency objAgency)
        {
            try
            {
                string sql =
                    "INSERT INTO FIAgency (Username,[Password],Name,Point,Gender,DOB,Address1,Address2,City,State,PostalCode,Country,PhoneNo,MobileNo,Email,PassEmail,SaveImages,LastActiveDate,Day_Actived,Dis_Activated,ProductKey,SerailKey,SecKey) " +
                    "Values ('"+ objAgency.Username + "', '" + objAgency.Password + "', '" + objAgency.Name + "', 0, 'Nam', '01/01/2015', '" + objAgency.Address1 + "', '', '" + objAgency.City + "', '', '', 'VietNam', '', '', '', '', '" + objAgency.SaveImages + "','01/01/2015',0,'0','" + objAgency.ProductKey + "','" + objAgency.SerailKey + "','" + objAgency.SecKey + "');";
                return ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Update Fields : Name,Address1,City,SaveImages,ProductKey,,SerailKey,SecKey
        /// </summary>
        /// <param name="objAgency"></param>
        /// <returns></returns>
        public bool Update(FIAgency objAgency)
        {
            try
            {
                string sql =
                    "Update FIAgency set Name='" + objAgency.Name +
                    "',Address1='" + objAgency.Address1 + "',City='" + objAgency.City + "',SaveImages='" +
                    objAgency.SaveImages + "',ProductKey='" + objAgency.ProductKey + "',SerailKey='" +
                    objAgency.SerailKey + "',SecKey='" + objAgency.SecKey + "',[Password] ='" + objAgency.Password + "',Username ='" + objAgency.Username + "',MobileNo ='" + objAgency.MobileNo + "',PhoneNo ='" + objAgency.PhoneNo + "',Email ='" + objAgency.Email + "' Where ID = " + objAgency.ID + ";";
                    
                return ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Delete(FIAgency objAgency)
        {
            return false;
        }

        public bool Add(FIAgency objAgency, OleDbTransaction trans)
        {
            try
            {
                string sql =
                    "INSERT INTO FIAgency (Username,[Password],Name,Point,Gender,DOB,Address1,Address2,City,State,PostalCode,Country,PhoneNo,MobileNo,Email,PassEmail,SaveImages,LastActiveDate,Day_Actived,Dis_Activated,ProductKey,SerailKey,SecKey) " +
                    "Values ('" + objAgency.Username + "', '" + objAgency.Password + "', '" + objAgency.Name + "', 0, 'Nam', '"+DateTime.Today+"', '" + objAgency.Address1 + "', '', '" + objAgency.City + "', '', '', 'VietNam', '"+objAgency.PhoneNo+ "', '" + objAgency.MobileNo + "', '" + objAgency.Email + "', '', '" + objAgency.SaveImages + "','01/01/2015',0,'0','" + objAgency.ProductKey + "','" + objAgency.SerailKey + "','" + objAgency.SecKey + "');";
                return ExecuteNonQuery(sql,trans);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Update(FIAgency objAgency, OleDbTransaction trans)
        {
            try
            {
                string sql =
                    "Update FIAgency set Name='" + objAgency.Name +
                    "',Address1='" + objAgency.Address1 + "',City='" + objAgency.City + "',SaveImages='" +
                    objAgency.SaveImages + "',ProductKey='" + objAgency.ProductKey + "',SerailKey='" +
                    objAgency.SerailKey + "',SecKey='" + objAgency.SecKey + "' Where ID = " + objAgency.ID + ";";

                return ExecuteNonQuery(sql,trans);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Delete(FIAgency objAgency, OleDbTransaction trans)
        {
            return false;
        }

        public long Count()
        {
            return 0;
        }

        public long Count(OleDbTransaction trans)
        {
            return 0;
        }
        /// <summary>
        /// By Username,Name
        /// </summary>
        /// <param name="textSearch"></param>
        /// <returns></returns>
        public ICollection<FIAgency> Search(string textSearch)
        {
            string sql = "Select * from FIAGENCY where Username like %'"+textSearch+ "'% OR Name like %'" + textSearch + "'%";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public FIAgency Find(long id)
        {
            string sql = "Select * from FIAGENCY WHERE ID = " + id + "";
            DataTable dt = LoadData(sql);
            return ConvertObjAgency(dt);
        }

        public FIAgency Find(string username)
        {
            string sql = "Select * from FIAGENCY WHERE Username = '" + username + "'";
            DataTable dt = LoadData(sql);
            return ConvertObjAgency(dt);
        }

        public FIAgency Login(string username, string password)
        {
            string sql = "Select * from FIAGENCY WHERE Username = '" + username + "' AND [Password] = '" + password +
                         "'";
            DataTable dt = LoadData(sql);
            return ConvertObjAgency(dt);
        }

        public FIAgency FindByName(string name)
        {
            string sql = "Select * from FIAGENCY WHERE Name = '" + name + "'";
            DataTable dt = LoadData(sql);
            return ConvertObjAgency(dt);
        }

        public FIAgency FindBySeckey(string seckey)
        {
            string sql = "Select * from FIAGENCY WHERE SecKey = '" + seckey + "'";
            DataTable dt = LoadData(sql);
            return ConvertObjAgency(dt);
        }

        public bool ChangePass(long id, string password)
        {
            string sql =
                "Update FIAgency set [Password]='" + password + "' Where ID = " + id + "";
            return ExecuteNonQuery(sql);
        }

        public bool ResetPass(long id)
        {
            string sql =
                "Update FIAgency set [Password] = 'k1a6UDR0uUMzkSZH7m1WFA==' Where ID = " + id + "";
            return ExecuteNonQuery(sql);
        }

        public ICollection<FIAgency> LoadAll()
        {
            string sql = "Select * from FIAGENCY";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }

        private FIAgency ConvertObjAgency(DataTable dtTable)
        {
            try
            {
                if (dtTable == null) return null;
                if (dtTable.Rows.Count == 0) return null;

                return new FIAgency
                {
                    ID = long.Parse(dtTable.Rows[0][0].ToString()),
                    Username = dtTable.Rows[0][1].ToString(),
                    Password = dtTable.Rows[0][2].ToString(),
                    Name = dtTable.Rows[0][3].ToString(),
                    Address1 = dtTable.Rows[0][7].ToString(),
                    City = dtTable.Rows[0][9].ToString(),
                    PhoneNo = dtTable.Rows[0][13].ToString(),
                    MobileNo = dtTable.Rows[0][14].ToString(),
                    Email = dtTable.Rows[0][15].ToString(),
                    PassEmail = dtTable.Rows[0][16].ToString(),
                    SaveImages = dtTable.Rows[0][17].ToString(),
                    //LastActiveDate = DateTime.Parse(dtTable.Rows[0][18].ToString()),
                    //Day_Actived = long.Parse(dtTable.Rows[0][19].ToString()),
                    Dis_Activated = dtTable.Rows[0][20].ToString(),
                    ProductKey = dtTable.Rows[0][21].ToString(),
                    SerailKey = dtTable.Rows[0][22].ToString(),
                    SecKey = dtTable.Rows[0][23].ToString()
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private ICollection<FIAgency> ConvertToList(DataTable dtTable)
        {
            try
            {
                if (dtTable == null) return null;
                if (dtTable.Rows.Count == 0) return null;

                return (from DataRow row in dtTable.Rows
                    select new FIAgency
                    {
                        ID = long.Parse(row[0].ToString()),
                        Username = row[1].ToString(),
                        Password = row[2].ToString(),
                        Name = row[3].ToString(),
                        Address1 = row[7].ToString(),
                        City = row[9].ToString(),
                        PhoneNo = row[13].ToString(),
                        MobileNo = row[14].ToString(),
                        Email = row[15].ToString(),
                        PassEmail = row[16].ToString(),
                        SaveImages = row[17].ToString(),
                        Dis_Activated = row[20].ToString(),
                        ProductKey = row[21].ToString(),
                        SerailKey = row[22].ToString(),
                        SecKey = row[23].ToString()
                    }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
