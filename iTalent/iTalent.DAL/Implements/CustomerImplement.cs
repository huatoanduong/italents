﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using iTalent.DAL.Commons;
using iTalent.Entities;

namespace iTalent.DAL.Implements
{
    public interface ICustomerImplement
    {
        bool Add(FICustomer objCustomer);
        bool Update(FICustomer objCustomer);

        bool Delete(FICustomer objCustomer);

        ICollection<FICustomer> Search(string textSearch);
        ICollection<FICustomer> SearchByName(string textSearch);
        ICollection<FICustomer> SearchByParent(string textSearch);
        ICollection<FICustomer> SearchByTel(string textSearch);
        ICollection<FICustomer> SearchByAddress(string textSearch);

        ICollection<FICustomer> SearchByReportId(string textSearch);

        ICollection<FICustomer> Search(DateTime fromDate,DateTime toDate);
        ICollection<FICustomer> Search(string textSearch, DateTime fromDate, DateTime toDate);

        FICustomer Find(long id);
        ICollection<FICustomer> FindByAgency(long agencyId);
        FICustomer FindByReportId(string reportId);
        FICustomer FindByName(string name);
        ICollection<FICustomer> LoadAll();
    }
    public class CustomerImplement: DbConnectUtil, ICustomerImplement
    {
        

        private FICustomer ConvertObj(DataTable dtTable)
        {
            try
            {
                if (dtTable == null) return null;
                if (dtTable.Rows.Count == 0) return null;

                return new FICustomer
                {
                    ID = long.Parse(dtTable.Rows[0][0].ToString()),
                    AgencyID = long.Parse(dtTable.Rows[0][1].ToString()),
                    ReportID = dtTable.Rows[0][2].ToString(),
                    Date = DateTime.Parse(dtTable.Rows[0][3].ToString()),
                    Name = dtTable.Rows[0][4].ToString(),
                    Gender = dtTable.Rows[0][5].ToString(),
                    DOB = DateTime.Parse(dtTable.Rows[0][6].ToString()),
                    Parent = dtTable.Rows[0][7].ToString(),
                    Tel = dtTable.Rows[0][8].ToString(),
                    Mobile = dtTable.Rows[0][9].ToString(),
                    Email = dtTable.Rows[0][10].ToString(),
                    Address1 = dtTable.Rows[0][11].ToString(),
                    Address2 = "",
                    City = dtTable.Rows[0][13].ToString(),
                    State = dtTable.Rows[0][14].ToString(),
                    ZipPosTalCode = dtTable.Rows[0][15].ToString(),
                    Country = dtTable.Rows[0][16].ToString(),
                    Remark = dtTable.Rows[0][17].ToString()
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private ICollection<FICustomer> ConvertToList(DataTable dtTable)
        {
            try
            {
                if (dtTable == null) return null;
                if (dtTable.Rows.Count == 0) return null;

                return (from DataRow row in dtTable.Rows
                        select new FICustomer
                        {
                            ID = long.Parse(row[0].ToString()),
                            AgencyID = long.Parse(row[1].ToString()),
                            ReportID = row[2].ToString(),
                            Date = DateTime.Parse(row[3].ToString()),
                            Name = row[4].ToString(),
                            Gender = row[5].ToString(),
                            DOB = DateTime.Parse(row[6].ToString()),
                            Parent = row[7].ToString(),
                            Tel = row[8].ToString(),
                            Mobile = row[9].ToString(),
                            Email = row[10].ToString(),
                            Address1 = row[11].ToString(),
                            Address2 = "",
                            City = row[13].ToString(),
                            State = row[14].ToString(),
                            ZipPosTalCode = row[15].ToString(),
                            Country = row[16].ToString(),
                            Remark = row[17].ToString()
                        }).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Add(FICustomer objCustomer)
        {
            try
            {
                string sql =
                    "INSERT INTO [FICustomer]([AgencyID],[ReportID],[Date],[Name],[Gender],[DOB],[Parent],[Tel],[Mobile],[Email],[Address1],[Address2],[City],[State] ,[ZipPosTalCode],[Country],[Remark])"+
               " VALUES("+objCustomer.AgencyID+ ", '" + objCustomer.ReportID + "', '" + objCustomer.Date + "', '" + objCustomer.Name + "', '" + objCustomer.Gender + "', '" + objCustomer.DOB + "', '" + objCustomer.Parent + "', '" + objCustomer.Tel
               + "', '" + objCustomer.Mobile + "','" + objCustomer.Email + "', '" + objCustomer.Address1 + "', '', '" + objCustomer.City + "', '" + objCustomer.State + "', '" + objCustomer.ZipPosTalCode + "', '" + objCustomer.Country + "', '" + objCustomer.Remark+ "');";
                return ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Update(FICustomer objCustomer)
        {
            try
            {
                string sql =
                    "Update FICustomer set AgencyID=" + objCustomer.AgencyID +",Name='" + objCustomer.Name + "',Gender='" + objCustomer.Gender + "',DOB='" + objCustomer.DOB + "',Parent='" + objCustomer.Parent + "',Tel='" + objCustomer.Tel + "',Mobile='" + objCustomer.Mobile + "',Email='" + objCustomer.Email + "',Address1='" + objCustomer.Address1 + "',City='" + objCustomer.City + "',State='" + objCustomer.State + "',ZipPosTalCode='" + objCustomer.ZipPosTalCode + "',Country='" + objCustomer.Country + "',Remark='" + objCustomer.Remark + "' Where ID = " + objCustomer.ID + ";";
                return ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Delete(FICustomer objCustomer)
        {
            try
            {
                string sql = "Delete FROM FICustomer Where ID = " + objCustomer.ID + ";";
                return ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ICollection<FICustomer> Search(string textSearch)
        {
            string sql = "Select * from FICustomer where Name like '%" + textSearch + "%' OR ID = " + textSearch + " OR Tel like '%" + textSearch + "%' OR Parent like '%" + textSearch + "%' OR Address1 like '%" + textSearch + "%' OR DOB like '%" + textSearch + "%'";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public ICollection<FICustomer> SearchByReportId(string textSearch)
        {
            string sql = "Select * from FICustomer where ReportID Like '%" + textSearch + "%'";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public ICollection<FICustomer> SearchByName(string textSearch)
        {
            string sql = "Select * from FICustomer where Name Like '%" + textSearch + "%'";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public ICollection<FICustomer> SearchByParent(string textSearch)
        {
            string sql = "Select * from FICustomer where Parent Like '%" + textSearch + "%'";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public ICollection<FICustomer> SearchByAddress(string textSearch)
        {
            string sql = "Select * from FICustomer where Address1 Like '%" + textSearch + "%'";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public ICollection<FICustomer> SearchByTel(string textSearch)
        {
            string sql = "Select * from FICustomer where Tel Like '%" + textSearch + "%'";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
        public ICollection<FICustomer> Search(DateTime fromDate, DateTime toDate)
        {
            string sql = "Select * from FICustomer where Date >= '"+ fromDate + "' AND Date <= '" + toDate + "'"; 
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }

        public ICollection<FICustomer> Search(string textSearch, DateTime fromDate, DateTime toDate)
        {
            string sql = "Select * from FICustomer where (Date >= '" + fromDate + "' AND Date <= '" + toDate + "') and (Name like '%" + textSearch + "%' OR ID = " + textSearch + " OR Tel like '%" + textSearch + "%' OR Parent like '%" + textSearch + "%' OR Address1 like '%" + textSearch + "%' OR DOB like '%" + textSearch + "%')";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }

        public FICustomer Find(long id)
        {
            string sql = "Select * from FICustomer WHERE ID = " + id + "";
            DataTable dt = LoadData(sql);
            return ConvertObj(dt);
        }

        public ICollection<FICustomer> FindByAgency(long agencyId)
        {
            string sql = "Select * from FICustomer where AgencyID=" + agencyId;
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }

        public FICustomer FindByReportId(string reportId)
        {
            string sql = "Select * from FICustomer WHERE ReportID = '" + reportId + "'";
            DataTable dt = LoadData(sql);
            return ConvertObj(dt);
        }

        public FICustomer FindByName(string name)
        {
            string sql = "Select * from FICustomer WHERE Name = '" + name + "'";
            DataTable dt = LoadData(sql);
            return ConvertObj(dt);
        }

        public ICollection<FICustomer> LoadAll()
        {
            string sql = "Select * from FICustomer";
            DataTable dt = LoadData(sql);
            return ConvertToList(dt);
        }
    }
}
