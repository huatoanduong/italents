﻿using System;
using iTalent.DAL.Commons;

namespace iTalent.DAL.Services
{
    public interface IBaseService : IDisposable
    {
        string ErrMsg { get; set; }
        bool VietNameLanguage { get; }
    }

    public class BaseService: IBaseService
    {
        public BaseService()
        {
            DbConnectUtil = new DbConnectUtil();
        }

        protected DbConnectUtil DbConnectUtil;
        public void Dispose()
        {
            GC.Collect();
            GC.SuppressFinalize(this);
        }

        public string ErrMsg { get; set; }

        public bool VietNameLanguage
        {
            get {return ICurrentSessionService.VietNamLanguage; }
        }

        protected void ThrowExceptionWithErrMsg()
        {
            ThrowExceptionWithErrMsg(ErrMsg);
        }

        protected void ThrowExceptionWithErrMsg(string errMsg)
        {
            throw new Exception(errMsg);
        }
    }
}
