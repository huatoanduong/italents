﻿using System;
using iTalent.Entities;

namespace iTalent.DAL.Factorys
{
    public interface IAgencyEntityFactory
    {
       // FIAgency CreateEntity();
    }

    public class AgencyEntityFactory :  IAgencyEntityFactory
    {
        public FIAgency CreateEntity()
        {
            var obj = new FIAgency
            {
                Country = "VietNam",
                DOB = DateTime.Now.AddYears(-20),
                Day_Actived = 0,
                Dis_Activated = "1",
                Gender = "None",
                Username = "",
                Password = "",
                ID = 0,
                Point = -1,
                ProductKey = "",
                SecKey = "",
                SerailKey = "",
                State = "1"
            };

            return obj;
        }
    }
}