﻿using System;
using iTalent.Entities;

namespace iTalent.DAL.Factorys
{
    public interface ICustomerEntityFactory
    {
    }

    public class CustomerEntityFactory : ICustomerEntityFactory
    {
        public FICustomer CreateEntity()
        {
            var obj = new FICustomer
            {
                ID =0,
                DOB = DateTime.Now.AddYears(-20),
                Country = "Việt Nam",
                Date = DateTime.Now,
                Gender = "None"
            };

            return obj;
        }
    }
}