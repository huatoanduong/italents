﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;

namespace iTalent.DAL.Commons
{
    public interface IDbConnectUtil:IDisposable
    {
        void CheckDb();
    }

    public class DbConnectUtil:IDbConnectUtil
    {
        //private static DbConnectUtil _instance;

        //public static DbConnectUtil Intance
        //{
        //    get
        //    {
        //        if (_instance == null)
        //        {
        //            _instance = new DbConnectUtil();
        //            _instance.CheckDB();
        //        }
        //        else
        //        {
        //            _instance.CheckDB();
        //        }
        //        return _instance;
        //    }
        //}
       
        public DbConnectUtil()
        {
            CheckDb();
        }

        protected OleDbConnection Conn = new OleDbConnection(ConnectionString);
        protected bool OpenConnect()
        {
            try
            {
                if (DbConnection.State == ConnectionState.Open) return false;
                DbConnection.Open();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected bool CloseConnect()
        {
            try
            {
                DbConnection.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ExecuteNonQuery(string sql)
        {
            try
            {
                if (DbConnection.State != ConnectionState.Open)
                    OpenConnect();

                using (OleDbCommand cmd = new OleDbCommand(sql, DbConnection))
                {
                    int i = cmd.ExecuteNonQuery();
                    if (i > 0)
                    {
                        return CloseConnect();
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ExecuteNonQuery(string sql, OleDbTransaction trans)
        {
            try
            {
                if (DbConnection.State != ConnectionState.Open)
                    OpenConnect();

                using (OleDbCommand cmd = new OleDbCommand(sql, DbConnection,trans))
                {
                    int i = cmd.ExecuteNonQuery();
                    if (i > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable LoadData(string sql)
        {
            try
            {
                using (DataTable dt = new DataTable())
                {
                   
                    if (DbConnection.State != ConnectionState.Open)
                        OpenConnect();
                    using (OleDbDataAdapter adapter = new OleDbDataAdapter(sql, DbConnection))
                    {
                        adapter.Fill(dt);
                    }
                    CloseConnect();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected static readonly string SourceDb = ICurrentSessionService.DesDirNameDatabase;// + "\\Database\\FINGERPRINTCLIENT.mdb";
        protected static string ConnectionString {
            get
            {
                //return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + SourceDb +
                //       ";Persist Security Info=True;Jet OLEDB:Database Password=@Zway@!";
                //return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + SourceDb +
                //       ";Persist Security Info=True;Jet OLEDB:Database Password=@Zway@!";
                return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + SourceDb +
                       ";Persist Security Info=True;Jet OLEDB:Database Password=@toro@!";
            }
        }
            public OleDbConnection DbConnection
        {
            get { return Conn; }
            set { Conn = value; }
        }

        public void Dispose()
        {
            DbConnection = null;
        }

        public void CheckDb()
        {
            try
            {
                if (!File.Exists(SourceDb))
                {
                    string dirsrc = Application.StartupPath + @"\iTalent.dll";
                  
                    //Directory.CreateDirectory(SourceDb.)
                    File.Copy(dirsrc, SourceDb, true);
                    //File.Copy("Properties.Resources.FINGERPRINTCLIENT", SourceDb, true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
           
        }
    }
}
