﻿using System;
using System.Data;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;

namespace iTalent.DAL.Commons
{

    public class ExportExcel
    {
        string _errMsg = "";
        readonly string _sheetName;

        public string ErrMsg
        {
            get { return _errMsg; }
            set { _errMsg = value; }
        }

        readonly Application _oExcel;

        public ExportExcel()
        {
            try
            {
                _sheetName = "List Customer";
                _oExcel = new Application {Visible = false};

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                _oExcel.DisplayAlerts = false;
                _oExcel.Application.SheetsInNewWorkbook = 1;

                Workbook oBook = _oExcel.Workbooks.Add(Type.Missing);
                Sheets oSheets = oBook.Worksheets;
                Worksheet oSheet = oSheets.Item[1] as  Worksheet;
                if (oSheet != null) oSheet.Name = _sheetName;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ExportDanhSachKhachHang(string exportdate, DataTable dt, string fileXuat)
        {
            try
            {
                //FileXuat = FilePathXuat;
                ErrMsg = "";
                Application oExcel = new Application {Visible = false, DisplayAlerts = false};

                //Tạo mới một Excel WorkBook 
                oExcel.Application.SheetsInNewWorkbook = 1;

                Workbook book = oExcel.Workbooks.Add(Type.Missing);
                Sheets sheets = book.Worksheets;
                Worksheet sheet = (Worksheet)sheets.Item[1];
                sheet.Name = _sheetName;

                //////////////////////////End of heading/////////////////////////////////
                Range head1 = sheet.Range["A1", "P2"];
                head1.MergeCells = true;
                head1.Value2 = ICurrentSessionService.VietNamLanguage ?"DANH SÁCH KHÁCH HÀNG : ":"LIST CUSTOMERS" + exportdate;
                head1.Font.Bold = true;
                head1.Font.Name = "Times New Roman";
                head1.Font.Size = "20";
                head1.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                head1.VerticalAlignment = XlVAlign.xlVAlignCenter;
                head1.RowHeight = 30.0;
                /////////////////////////////////////////////////////////////////////////////////

                // Tạo tiêu đề cột 
                Range cl1 = sheet.Range["A3", "A3"];
                cl1.Value2 = ICurrentSessionService.VietNamLanguage ? "Mã Report":"ReportID";
                cl1.ColumnWidth = 25.0;

                Range cl2 = sheet.Range["B3", "B3"];
                cl2.Value2 = ICurrentSessionService.VietNamLanguage ? "Ngày Tạo" : "Date Create";
                cl2.ColumnWidth = 15.0;

                Range cl3 = sheet.Range["C3", "C3"];
                cl3.Value2 = ICurrentSessionService.VietNamLanguage ? "Khách Hàng":"Customer";
                cl3.ColumnWidth = 20.0;

                Range cl4 = sheet.Range["D3", "D3"];
                cl4.Value2 = ICurrentSessionService.VietNamLanguage ? "Giới Tính" : "Sex";
                cl4.ColumnWidth = 5.0;

                Range cl5 = sheet.Range["E3", "E3"];
                cl5.Value2 = ICurrentSessionService.VietNamLanguage ? "Ngày Sinh" : "Birthday";
                cl5.ColumnWidth = 15.0;

                Range cl6 = sheet.Range["F3", "F3"];
                cl6.Value2 = ICurrentSessionService.VietNamLanguage ? "Cha/Mẹ" : "Parent";
                cl6.ColumnWidth = 20.0;

                Range cl7 = sheet.Range["G3", "G3"];
                cl7.Value2 = ICurrentSessionService.VietNamLanguage ? "Điện Thoại" : "Tel";
                cl7.ColumnWidth = 10.0;

                Range cl8 = sheet.Range["H3", "H3"];
                cl8.Value2 = ICurrentSessionService.VietNamLanguage ? "Di Động" : "Mobile";
                cl8.ColumnWidth = 10.0;

                Range cl9 = sheet.Range["I3", "I3"];
                cl9.Value2 = "Email";
                cl9.ColumnWidth = 15.0;

                Range cl10 = sheet.Range["J3", "J3"];
                cl10.Value2 = ICurrentSessionService.VietNamLanguage ? "Địa Chỉ" : "Address";
                cl10.ColumnWidth = 20.0;

                Range cl11 = sheet.Range["K3", "K3"];
                cl11.Value2 = ICurrentSessionService.VietNamLanguage ? "Địa Chỉ 2" : "Address 2";
                cl11.ColumnWidth = 0;

                Range cl12 = sheet.Range["L3", "L3"];
                cl12.Value2 = ICurrentSessionService.VietNamLanguage ? "Thành Phố" : "City";
                cl12.ColumnWidth = 10.0;

                Range cl13 = sheet.Range["M3", "M3"];
                cl13.Value2 = ICurrentSessionService.VietNamLanguage ? "Đối Tượng" : "Entity";
                cl13.ColumnWidth = 10.0;

                Range cl14 = sheet.Range["N3", "N3"];
                cl14.Value2 = ICurrentSessionService.VietNamLanguage ? "Mã Bưu Điện" : "Zip Postal";
                cl14.ColumnWidth = 0;

                Range cl15 = sheet.Range["O3", "O3"];
                cl15.Value2 = ICurrentSessionService.VietNamLanguage ? "Quốc Tịch" : "Country";
                cl15.ColumnWidth = 10.0;

                Range cl16 = sheet.Range["P3", "P3"];
                cl16.Value2 = ICurrentSessionService.VietNamLanguage ? "Ghi Chú" : "Note";
                cl16.ColumnWidth = 20.0;

                Range rowHead = sheet.Range["A3", "P3"];
                rowHead.Font.Bold = true;

                // Kẻ viền
                rowHead.Borders.LineStyle = Constants.xlSolid;
                // Thiết lập màu nền
                rowHead.Interior.ColorIndex = 15;
                rowHead.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                rowHead.VerticalAlignment = XlVAlign.xlVAlignCenter;

                //--------------------Không sửa-------------------------------------
                // Tạo mẳng đối tượng để lưu dữ toàn bồ dữ liệu trong DataTable,
                // vì dữ liệu được được gán vào các Cell trong Excel phải thông qua object thuần.
                if (dt != null)
                {
                    object[,] arr = new object[dt.Rows.Count, dt.Columns.Count];

                    //Chuyển dữ liệu từ DataTable vào mảng đối tượng
                    for (int r = 0; r < dt.Rows.Count; r++)
                    {
                        DataRow dr = dt.Rows[r];
                        for (int c = 0; c < dt.Columns.Count; c++)
                        {
                            arr[r, c] = dr[c].ToString();
                        }
                    }

                    //Thiết lập vùng điền dữ liệu
                    const int rowStart = 4;
                    const int columnStart = 1;
                    const int columnWork = 1;

                    int rowEnd = rowStart + dt.Rows.Count - 1;
                    int columnEnd = dt.Columns.Count;

                    // Ô bắt đầu điền dữ liệu
                    Range c1 = (Range)sheet.Cells[rowStart, columnStart];
                    // Ô kết thúc điền dữ liệu
                    Range c2 = (Range)sheet.Cells[rowEnd, columnEnd];
                    // Lấy về vùng điền dữ liệu
                    Range range = sheet.Range[c1, c2];

                    //Điền dữ liệu vào vùng đã thiết lập
                    range.Value2 = arr;

                    // Kẻ viền
                    range.Borders.LineStyle = Constants.xlSolid;
                    // Căn giữa cột STT
                    Range c3 = (Range)sheet.Cells[rowEnd, columnStart];
                    Range c4 = sheet.Range[c1, c3];
                    sheet.Range[c3, c4].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    //chon vung mo khoa cell
                    Range c5 = (Range)sheet.Cells[rowStart, columnWork];

                    sheet.Range[c2, c5].Locked = false;
                    //oSheet.Protect(Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing);
                }
                // Save file
                book.SaveAs(fileXuat, XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //oSheet.SaveAs(@"F:\!Xuat\MatHang.xls", XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing);
                oExcel.Quit();
                _errMsg = ICurrentSessionService.VietNamLanguage ? "Xuất file excel thành công !!!" : "Export file excel successfully!";
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
            }
            finally
            {
                ReleaseObject(_oExcel);
            }
        }

        public void ExportDanhSachDaiLy(string exportdate, DataTable dt, string fileXuat)
        {
            try
            {
                //FileXuat = FilePathXuat;
                ErrMsg = "";
                Application oExcel = new Application { Visible = false, DisplayAlerts = false };

                //Tạo mới một Excel WorkBook 
                oExcel.Application.SheetsInNewWorkbook = 1;

                Workbook book = oExcel.Workbooks.Add(Type.Missing);
                Sheets sheets = book.Worksheets;
                Worksheet sheet = (Worksheet)sheets.Item[1];
                sheet.Name = _sheetName;

                //////////////////////////End of heading/////////////////////////////////
                Range head1 = sheet.Range["A1", "G2"];
                head1.MergeCells = true;
                head1.Value2 = (ICurrentSessionService.VietNamLanguage ? "DANH SÁCH ĐẠI LÝ : ":"LIST AGENCYS") + exportdate;
                head1.Font.Bold = true;
                head1.Font.Name = "Times New Roman";
                head1.Font.Size = "20";
                head1.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                head1.VerticalAlignment = XlVAlign.xlVAlignCenter;
                head1.RowHeight = 30.0;
                /////////////////////////////////////////////////////////////////////////////////

                // Tạo tiêu đề cột 
                Range cl1 = sheet.Range["A3", "A3"];
                cl1.Value2 = ICurrentSessionService.VietNamLanguage ? "Mã Đại Lý" : "Agency ID";
                cl1.ColumnWidth = 10.0;

                Range cl2 = sheet.Range["B3", "B3"];
                cl2.Value2 = ICurrentSessionService.VietNamLanguage ? "Đại Lý" : "Agency";
                cl2.ColumnWidth = 20.0;

                Range cl3 = sheet.Range["C3", "C3"];
                cl3.Value2 = ICurrentSessionService.VietNamLanguage ? "Điện Thoại" : "Tel";
                cl3.ColumnWidth = 10.0;

                Range cl4 = sheet.Range["D3", "D3"];
                cl4.Value2 = ICurrentSessionService.VietNamLanguage ? "Di Động" : "Mobile";
                cl4.ColumnWidth = 10.0;

                Range cl5 = sheet.Range["E3", "E3"];
                cl5.Value2 = "Email";
                cl5.ColumnWidth = 20.0;

                Range cl6 = sheet.Range["F3", "F3"];
                cl6.Value2 = ICurrentSessionService.VietNamLanguage ? "Địa Chỉ" : "Address";
                cl6.ColumnWidth = 20.0;

                Range cl7 = sheet.Range["G3", "G3"];
                cl7.Value2 = ICurrentSessionService.VietNamLanguage ? "Thành Phố" : "City";
                cl7.ColumnWidth = 10.0;

                Range rowHead = sheet.Range["A3", "G3"];
                rowHead.Font.Bold = true;

                // Kẻ viền
                rowHead.Borders.LineStyle = Constants.xlSolid;
                // Thiết lập màu nền
                rowHead.Interior.ColorIndex = 15;
                rowHead.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                rowHead.VerticalAlignment = XlVAlign.xlVAlignCenter;

                //--------------------Không sửa-------------------------------------
                // Tạo mẳng đối tượng để lưu dữ toàn bồ dữ liệu trong DataTable,
                // vì dữ liệu được được gán vào các Cell trong Excel phải thông qua object thuần.
                if (dt != null)
                {
                    object[,] arr = new object[dt.Rows.Count, dt.Columns.Count];

                    //Chuyển dữ liệu từ DataTable vào mảng đối tượng
                    for (int r = 0; r < dt.Rows.Count; r++)
                    {
                        DataRow dr = dt.Rows[r];
                        for (int c = 0; c < dt.Columns.Count; c++)
                        {
                            arr[r, c] = dr[c].ToString();
                        }
                    }

                    //Thiết lập vùng điền dữ liệu
                    const int rowStart = 4;
                    const int columnStart = 1;
                    const int columnWork = 1;

                    int rowEnd = rowStart + dt.Rows.Count - 1;
                    int columnEnd = dt.Columns.Count;

                    // Ô bắt đầu điền dữ liệu
                    Range c1 = (Range)sheet.Cells[rowStart, columnStart];
                    // Ô kết thúc điền dữ liệu
                    Range c2 = (Range)sheet.Cells[rowEnd, columnEnd];
                    // Lấy về vùng điền dữ liệu
                    Range range = sheet.Range[c1, c2];

                    //Điền dữ liệu vào vùng đã thiết lập
                    range.Value2 = arr;

                    // Kẻ viền
                    range.Borders.LineStyle = Constants.xlSolid;
                    // Căn giữa cột STT
                    Range c3 = (Range)sheet.Cells[rowEnd, columnStart];
                    Range c4 = sheet.Range[c1, c3];
                    sheet.Range[c3, c4].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    //chon vung mo khoa cell
                    Range c5 = (Range)sheet.Cells[rowStart, columnWork];

                    sheet.Range[c2, c5].Locked = false;
                    //oSheet.Protect(Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing, Missing);
                }
                // Save file
                book.SaveAs(fileXuat, XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //oSheet.SaveAs(@"F:\!Xuat\MatHang.xls", XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing);
                oExcel.Quit();
                _errMsg = ICurrentSessionService.VietNamLanguage ? "Xuất file excel thành công !!!" : "Export file excel successfully!!!";
            }
            catch (Exception ex)
            {
                _errMsg = ex.Message;
            }
            finally
            {
                ReleaseObject(_oExcel);
            }
        }

        private void ReleaseObject(object obj)
        {
            try
            {
                Marshal.ReleaseComObject(obj);
            }
            catch
            {
                _errMsg = "Exception Occured while releasing object ";
            }
            finally
            {
                GC.Collect();
            }
        }

        public void Dispose()
        {
           //
        }
    }
}
