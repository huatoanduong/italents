﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model
{
    public class CareerModel
    {
        public string Name { get; set; }
        public decimal ChiSo { get; set; }
        public decimal SoLuongSao { get { return Cal_SoLuongSao(); } }
        public decimal SoLuongSaoTamLy { get; set; }
        public decimal Cal_SoLuongSao()
        {
            if (Name == "NGOẠI NGỮ")
            {
                //if (ChiSo > 73.5m && ChiSo <= 84)
                //    return 4;
                //else if (ChiSo > 84)
                //    return 5;
                //else if (ChiSo >= 70 && ChiSo < 73.5m)
                //    return 3;
                //else if (ChiSo < 70 && ChiSo >= 63)
                //    return 2;
                //else if (ChiSo < 63)
                //    return 1;

                if (ChiSo > 84 && ChiSo <= 90)
                    return 4;
                else if (ChiSo > 90)
                    return 5;
                else if (ChiSo >= 73.5m && ChiSo < 84)
                    return 3;
                else if (ChiSo < 73.5m && ChiSo >= 70)
                    return 2;
                else if (ChiSo < 70)
                    return 1;
            }
            //if (Name == "TÂM LÝ HỌC")
            //{
            //    return SoLuongSaoTamLy;
            //}
            if (ChiSo <= 12)
                return 2;
            if (ChiSo > 12 && ChiSo <= 13)
                return 3;
            if (ChiSo > 13 && ChiSo <= 14.5m)
                return 4;
            return 5;
        }

        public CareerModelCollection Owner { get; set; }

        public CareerModel(CareerModelCollection collection)
        {
            Owner = collection;
        }
    }

    public class CareerModelCollection
    {
        private MQModelCollection MQCollection;

        public CareerModel CNTT { get; set; }
        public CareerModel KyThuat { get; set; }
        public CareerModel YTe { get; set; }
        public CareerModel QTDH { get; set; }
        public CareerModel TaiChinh_KeToan { get; set; }
        public CareerModel DuLihNhaHangKhachSan { get; set; }
        public CareerModel QuanLyDieuHanh { get; set; }
        public CareerModel MoiTruong { get; set; }
        public CareerModel ThietKeHoiHoa { get; set; }
        public CareerModel NgheThuat { get; set; }
        public CareerModel AmNhac { get; set; }
        public CareerModel XayDungKienTruc { get; set; }
        public CareerModel TheThao { get; set; }
        public CareerModel XaHoiHoc { get; set; }
        public CareerModel TruyenThong { get; set; }
        public CareerModel NgonNgu { get; set; }
        public CareerModel Luat { get; set; }
        public CareerModel GiaoDuc { get; set; }
        public CareerModel Sale_Marketing { get; set; }
        public CareerModel CheTacThuCong { get; set; }
        public CareerModel ChinhTriNgoaiGiao { get; set; }
        public CareerModel VuTrang { get; set; }
        public CareerModel VanSuTriet { get; set; }
        public CareerModel TamLy { get; set; }
        public CareerModel CNDV { get; set; }
        public CareerModel KHTN { get; set; }
        public CareerModel ThietKe { get; set; }
        public CareerModel GiamSatChatLuong { get; set; }
        public CareerModel DuLich { get; set; }
        public CareerModel DaoTao { get; set; }
        public CareerModel KinhDoanh { get; set; }
        public CareerModel NgoaiNgu { get; set; }
        public List<CareerModel> CareerList { get; set; }

        public CareerModel Top1 { get { return CareerList[0]; } }
        public CareerModel Top2 { get { return CareerList[1]; } }
        public CareerModel Top3 { get { return CareerList[2]; } }
        public CareerModel Top4 { get { return CareerList[3]; } }

        private CareerModel FindByName(string name)
        {
            return CareerList.FirstOrDefault(p => p.Name == name);
        }

        public CareerModelCollection(MQModelCollection mqCollection)
        {
            this.MQCollection = mqCollection;
            GenerateCareerModelList();
            CalculateSoSao();
            Calculate();
        }

        public void GenerateCareerModelList()
        {
            if (CareerList == null)
            {
                CareerList = new List<CareerModel>();
            }
            CareerList.Clear();
            CareerModel model;
            
            //1
            model = new CareerModel(this);
            model.Name = "CÔNG NGHỆ THÔNG TIN";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value + MQCollection.Picture.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Logic16.Value + MQCollection.Picture16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            CNTT = model;

            //2
            model = new CareerModel(this);
            model.Name = "KỸ SƯ";
            model.ChiSo =
                //Math.Round(((MQCollection.Logic.Value + MQCollection.Body.Value + MQCollection.Nature.Value + MQCollection.Picture.Value) / (4 * MQCollection.TotalMQValue)) * 100, 2);
                Math.Round(((MQCollection.Logic16.Value + MQCollection.Body16.Value + MQCollection.Nature16.Value + MQCollection.Picture16.Value) / (4 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            KyThuat = model;

            //3
            model = new CareerModel(this);
            model.Name = "Y KHOA";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value + MQCollection.Body.Value + MQCollection.Nature.Value + MQCollection.Picture.Value) /(4 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Logic16.Value + MQCollection.Body16.Value + MQCollection.Nature16.Value + MQCollection.Picture16.Value) / (4 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            YTe = model;

            //4
            model = new CareerModel(this);
            model.Name = "TÀI CHÍNH";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value + MQCollection.Self.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Logic16.Value - (MQCollection.Logic16.Value * 0.3m) + MQCollection.Self16.Value - (MQCollection.Self16.Value * 0.7m)) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            TaiChinh_KeToan = model;

            //5
            model = new CareerModel(this);
            model.Name = "DU LỊCH, NHÀ HÀNG, KHÁCH SẠN";
            //model.ChiSo = Math.Round(((MQCollection.Nature.Value + MQCollection.Body.Value + MQCollection.Self.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Nature16.Value + MQCollection.Body16.Value + MQCollection.Self16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            DuLich = model;

            //6
            model = new CareerModel(this);
            model.Name = "QUẢN TRỊ, ĐIỀU HÀNH";
            //model.ChiSo = Math.Round(((MQCollection.People.Value + MQCollection.Self.Value) / (2 * MQCollection.TotalMQValue)) *100, 2);
            model.ChiSo = Math.Round(((MQCollection.People16.Value + MQCollection.Self16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            QTDH = model;

            //7
            model = new CareerModel(this);
            model.Name = "MÔI TRƯỜNG SINH THÁI";
            //model.ChiSo = Math.Round(((MQCollection.Nature.Value + MQCollection.Self.Value + MQCollection.Picture.Value + MQCollection.Logic.Value /2) /(4 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Nature16.Value + MQCollection.Self16.Value + MQCollection.Picture16.Value + MQCollection.Logic16.Value / 2) / (4 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            MoiTruong = model;

            //8
            model = new CareerModel(this);
            model.Name = "THIẾT KẾ HỘI HỌA";
            //model.ChiSo = Math.Round(((MQCollection.Body.Value + MQCollection.Picture.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Body16.Value + MQCollection.Picture16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            ThietKe = model;

            //9
            model = new CareerModel(this);
            model.Name = "NGHỆ THUẬT";
            //model.ChiSo = Math.Round(((MQCollection.Body.Value + MQCollection.Music.Value + MQCollection.Picture.Value) / (3 * MQCollection.TotalMQValue)) *100, 2);
            model.ChiSo = Math.Round(((MQCollection.Body16.Value + MQCollection.Music16.Value + MQCollection.Picture16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            NgheThuat = model;

            //10
            model = new CareerModel(this);
            model.Name = "KIẾN TRÚC";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value / 2 + MQCollection.Picture.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Logic16.Value / 2 + MQCollection.Picture16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            XayDungKienTruc = model;

            //11
            model = new CareerModel(this); model.Name = "THỂ THAO & HOẠT ĐỘNG BIỂU DIỄN";
            //model.ChiSo = Math.Round(((MQCollection.Body.Value + MQCollection.Self.Value + MQCollection.Picture.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Body16.Value + MQCollection.Self16.Value + MQCollection.Picture16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            TheThao = model;

            //12
            model = new CareerModel(this);
            model.Name = "XÃ HỘI HỌC";
            //model.ChiSo = Math.Round(((MQCollection.People.Value + MQCollection.Word.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.People16.Value + MQCollection.Word16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            XaHoiHoc = model;

            //13
            model = new CareerModel(this);
            model.Name = "TRUYỀN THÔNG";
            //model.ChiSo = Math.Round(((MQCollection.People.Value + MQCollection.Picture.Value + MQCollection.Logic.Value + MQCollection.Word.Value) / (4 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.People16.Value + MQCollection.Picture16.Value + MQCollection.Logic16.Value + MQCollection.Word16.Value) / (4 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            TruyenThong = model;

            //14
            model = new CareerModel(this);
            model.Name = "NGÔN NGỮ";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.Self.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.Self16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            NgonNgu = model;

            //15
            model = new CareerModel(this);
            model.Name = "LUẬT, CHÍNH TRỊ";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.People.Value + MQCollection.Logic.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.People16.Value + MQCollection.Logic16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            ChinhTriNgoaiGiao = model;

            //16
            model = new CareerModel(this);
            model.Name = "GIÁO DỤC";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.People.Value + MQCollection.Body.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.People16.Value + MQCollection.Body16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            GiaoDuc = model;

            //17
            model = new CareerModel(this);
            model.Name = "KINH TẾ - THƯƠNG MẠI";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.People.Value) / (2 * MQCollection.TotalMQValue)) *100, 2);
            //model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.People16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            model.ChiSo = Math.Round((((MQCollection.People16.Value - (MQCollection.People16.Value * 0.05m)) + MQCollection.Logic16.Value + MQCollection.Nature16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            Sale_Marketing = model;

            //18
            model = new CareerModel(this);
            model.Name = "CHẾ TÁC THỦ CÔNG";
            //model.ChiSo = Math.Round(((MQCollection.Body.Value + MQCollection.Self.Value + MQCollection.Picture.Value) / (3 * MQCollection.TotalMQValue)) *100, 2);
            model.ChiSo = Math.Round(((MQCollection.Body16.Value + MQCollection.Self16.Value + MQCollection.Picture16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            CheTacThuCong = model;

            //19
            model = new CareerModel(this);
            model.Name = "GIÁM SÁT CHẤT LƯỢNG";
            //model.ChiSo = Math.Round(((MQCollection.Nature.Value + MQCollection.Body.Value + MQCollection.Self.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Nature16.Value + MQCollection.Body16.Value + MQCollection.Self16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            GiamSatChatLuong = model;

            //20
            model = new CareerModel(this);
            model.Name = "ĐÀO TẠO";
            //model.ChiSo = Math.Round(((MQCollection.People.Value + MQCollection.Body.Value + MQCollection.Word.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.People16.Value + MQCollection.Body16.Value + MQCollection.Word16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            DaoTao = model;

            //21
            model = new CareerModel(this);
            model.Name = "KINH DOANH TIẾP THỊ / BÁN HÀNG";
            //model.ChiSo = Math.Round(((MQCollection.People.Value) / (1 * MQCollection.TotalMQValue)) * 100, 2);
            //model.ChiSo = Math.Round((((MQCollection.People16.Value - (MQCollection.People16.Value * 0.05m)) + MQCollection.Logic16.Value + MQCollection.Nature16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.People16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            KinhDoanh = model;

            //22
            model = new CareerModel(this);
            model.Name = "NGOẠI NGỮ";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.Body.Value + MQCollection.Self.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            //model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.Body16.Value + MQCollection.Self16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            model.ChiSo = getValueByMQ(MQCollection.Word16.Value);
            CareerList.Add(model);
            NgoaiNgu = model;

            //23
            model = new CareerModel(this);
            model.Name = "ÂM NHẠC";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.Body.Value + MQCollection.Self.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            model.ChiSo = Math.Round(((MQCollection.MusicCareer.Value) / (1 * MQCollection.TotalMQValue16)) * 100, 2);
            CareerList.Add(model);
            AmNhac = model;

            // Self 1+ Inter 2+ Lang 3+ Logic 4 5 Sao va truong hop bat ky trong top 4 ma mot thuoc tinh MQ lon hon Intra thi 3 sao.
            // Self + Inter Hang 2 + Lang Hang 3 4Sao
            // Self + Inter +  Logic 3 Sao
            // Self 2 Sao minimum
            // Self + other member 2Sao.
            //24
            model = new CareerModel(this);
            model.Name = "TÂM LÝ HỌC";
            //if (MQCollection.Self16.Rank == 1 && MQCollection.People16.Rank == 2 && MQCollection.Word16.Rank == 3 && MQCollection.Logic16.Rank == 4)
            //    model.SoLuongSaoTamLy = 5;
            //else if (MQCollection.Self16.Rank == 1 && MQCollection.People16.Rank == 2 && MQCollection.Word16.Rank == 3)
            //    model.SoLuongSaoTamLy = 4;
            //else if (MQCollection.Self16.Rank == 1 && MQCollection.People16.Rank == 2 && MQCollection.Logic16.Rank == 3)
            //    model.SoLuongSaoTamLy = 3;
            //else if (MQCollection.Self16.Rank != 1 && (MQCollection.Self16.Rank == 2 || MQCollection.Self16.Rank == 3 || MQCollection.Self16.Rank == 4)
            //    && (MQCollection.People16.Rank == 1 || MQCollection.People16.Rank == 2 || MQCollection.People16.Rank == 3 || MQCollection.People16.Rank == 4)
            //    && (MQCollection.Word16.Rank == 1 || MQCollection.Word16.Rank == 2 || MQCollection.Word16.Rank == 3 || MQCollection.Word16.Rank == 4)
            //    && (MQCollection.Logic16.Rank == 1 || MQCollection.Logic16.Rank == 2 || MQCollection.Logic16.Rank == 3 || MQCollection.Logic16.Rank == 4))
            //{
            //    model.SoLuongSaoTamLy = 3;
            //}
            //else if (MQCollection.Self16.Rank == 1
            //    && (MQCollection.People16.Rank == 2 || MQCollection.Logic16.Rank == 2 || MQCollection.Word16.Rank == 2))
            //{
            //    model.SoLuongSaoTamLy = 2;
            //}
            //else model.SoLuongSaoTamLy = 2;
            if (MQCollection.Self16.Value / 2 > 7.99m)
                model.ChiSo = Math.Round(((MQCollection.Self16.Value + MQCollection.People16.Value + MQCollection.Word16.Value + MQCollection.Logic16.Value) / (4 * MQCollection.TotalMQValue16)) * 100, 2);
            else model.ChiSo = 12;
            CareerList.Add(model);
            TamLy = model;

            #region Unused
            //model = new CareerModel(this);
            //model.Name = "CÔNG NGHỆ DỊCH VỤ";
            ////model.ChiSo =Math.Round(((MQCollection.Word.Value + MQCollection.People.Value + MQCollection.Self.Value) / (3 * MQCollection.TotalMQValue)) *100, 2);
            //model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.People16.Value + MQCollection.Self16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            //CareerList.Add(model);
            //CNDV = model;

            //model = new CareerModel(this);
            //model.Name = "KHTN";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value + MQCollection.Body.Value + MQCollection.Nature.Value + MQCollection.Picture.Value) / (4 * MQCollection.TotalMQValue)) * 100, 2);
            //model.ChiSo = Math.Round(((MQCollection.Logic16.Value + MQCollection.Body16.Value + MQCollection.Nature16.Value + MQCollection.Picture16.Value) / (4 * MQCollection.TotalMQValue16)) * 100, 2);
            //CareerList.Add(model);
            //KHTN = model;

            //model = new CareerModel(this);
            //model.Name = "XayDungKienTruc";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value + MQCollection.Picture.Value + MQCollection.Nature.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            //CareerList.Add(model);
            //XayDungKienTruc = model;

            // model = new CareerModel(this); model.Name = "VuTrang";
            //model.ChiSo = Math.Round(((MQCollection.Logic.Value + MQCollection.Body.Value + MQCollection.Picture.Value) / (3 * MQCollection.TotalMQValue)) * 100, 2);
            //model.ChiSo = Math.Round(((MQCollection.Logic16.Value + MQCollection.Body16.Value + MQCollection.Picture16.Value) / (3 * MQCollection.TotalMQValue16)) * 100, 2);
            //CareerList.Add(model);
            //VuTrang = model;

            //model = new CareerModel(this); model.Name = "Van_Su_Triet";
            //model.ChiSo = Math.Round(((MQCollection.Word.Value + MQCollection.Self.Value) / (2 * MQCollection.TotalMQValue)) * 100, 2);
            //model.ChiSo = Math.Round(((MQCollection.Word16.Value + MQCollection.Self16.Value) / (2 * MQCollection.TotalMQValue16)) * 100, 2);
            //CareerList.Add(model);
            //VanSuTriet = model;
            #endregion Unused
        }

        public void Calculate()
        {
            CareerList = CareerList.OrderByDescending(p => p.SoLuongSao).ToList();
        }

        public void CalculateSoSao()
        {
            foreach (CareerModel model in this.CareerList)
            {
                model.Cal_SoLuongSao();
            }
        }

        private decimal getValueByMQ(decimal value)
        {
            //if (PIModelCollection.TFRC > 28) { return 95; }
            //if (PIModelCollection.TFRC > 26) { return 90; }
            //if (PIModelCollection.TFRC > 24) { return 85; }
            //if (PIModelCollection.TFRC > 20) { return 80; }
            //if (PIModelCollection.TFRC > 18) { return 75; }
            //if (PIModelCollection.TFRC > 16) { return 70; }
            //if (PIModelCollection.TFRC > 14) { return 65; }
            //if (PIModelCollection.TFRC > 12) { return 60; }
            //if (PIModelCollection.TFRC > 10) { return 55; }
            //if (PIModelCollection.TFRC > 8) { return 50; }
            //return 45;

            if (value > 28) { return 95; }
            if (value > 27.9m) { return 94.75m; }
            if (value > 27.8m) { return 94.5m; }
            if (value > 27.7m) { return 94.25m; }
            if (value > 27.6m) { return 94; }
            if (value > 27.5m) { return 93.75m; }
            if (value > 27.4m) { return 93.5m; }
            if (value > 27.3m) { return 93.25m; }
            if (value > 27.2m) { return 93; }
            if (value > 27.1m) { return 92.75m; }
            if (value > 27) { return 92.5m; }
            if (value > 26.9m) { return 92.25m; }
            if (value > 26.8m) { return 92; }
            if (value > 26.7m) { return 91.75m; }
            if (value > 26.6m) { return 91.5m; }
            if (value > 26.5m) { return 91.25m; }
            if (value > 26.4m) { return 91; }
            if (value > 26) { return 90; }
            if (value > 25.9m) { return 89.75m; }
            if (value > 25.8m) { return 89.5m; }
            if (value > 25.7m) { return 89.25m; }
            if (value > 25.6m) { return 89; }
            if (value > 25.5m) { return 88.75m; }
            if (value > 25.4m) { return 88.5m; }
            if (value > 25.3m) { return 88.25m; }
            if (value > 25.2m) { return 88; }
            if (value > 25.1m) { return 87.75m; }
            if (value > 25) { return 87.75m; }
            if (value > 24.9m) { return 87.25m; }
            if (value > 24.8m) { return 87; }
            if (value > 24.7m) { return 86.75m; }
            if (value > 24.6m) { return 86.5m; }
            if (value > 24.5m) { return 86.25m; }
            if (value > 24.4m) { return 86; }
            if (value > 24.3m) { return 85.75m; }
            if (value > 24.2m) { return 85.5m; }
            if (value > 24.1m) { return 85.25m; }
            if (value > 24) { return 85; }
            if (value > 23.9m) { return 84.875m; }
            if (value > 23.8m) { return 84.75m; }
            if (value > 23.7m) { return 84.625m; }
            if (value > 23.6m) { return 84.5m; }
            if (value > 23.5m) { return 84.375m; }
            if (value > 23.4m) { return 84.25m; }
            if (value > 23.3m) { return 84.125m; }
            if (value > 23.2m) { return 84; }
            if (value > 23.1m) { return 83.875m; }
            if (value > 23) { return 83.75m; }
            if (value > 22.9m) { return 83.625m; }
            if (value > 22.8m) { return 83.5m; }
            if (value > 22.7m) { return 83.375m; }
            if (value > 22.6m) { return 83.25m; }
            if (value > 22.5m) { return 83.125m; }
            if (value > 22.4m) { return 83; }
            if (value > 22.3m) { return 82.875m; }
            if (value > 22.2m) { return 82.75m; }
            if (value > 22.1m) { return 82.625m; }
            if (value > 22) { return 82.5m; }
            if (value > 21.9m) { return 82.375m; }
            if (value > 21.8m) { return 82.25m; }
            if (value > 21.7m) { return 82.125m; }
            if (value > 21.6m) { return 82; }
            if (value > 21.5m) { return 81.875m; }
            if (value > 21.4m) { return 81.75m; }
            if (value > 21.3m) { return 81.625m; }
            if (value > 21.2m) { return 81.5m; }
            if (value > 21.1m) { return 81.375m; }
            if (value > 21) { return 81.25m; }
            if (value > 20.9m) { return 81.125m; }
            if (value > 20.8m) { return 81; }
            if (value > 20.7m) { return 80.875m; }
            if (value > 20.6m) { return 80.75m; }
            if (value > 20.5m) { return 80.625m; }
            if (value > 20.4m) { return 80.5m; }
            if (value > 20.3m) { return 80.375m; }
            if (value > 20.2m) { return 80.25m; }
            if (value > 20.1m) { return 80.125m; }
            if (value > 20) { return 80; }
            if (value > 19.9m) { return 79.75m; }
            if (value > 19.8m) { return 79.5m; }
            if (value > 19.7m) { return 79.25m; }
            if (value > 19.6m) { return 79; }
            if (value > 19.5m) { return 78.75m; }
            if (value > 19.4m) { return 78.5m; }
            if (value > 19.3m) { return 78.25m; }
            if (value > 19.2m) { return 78; }
            if (value > 19.1m) { return 77.75m; }
            if (value > 19) { return 77.5m; }
            if (value > 18.9m) { return 77.25m; }
            if (value > 18.8m) { return 77; }
            if (value > 18.7m) { return 76.75m; }
            if (value > 18.6m) { return 76.5m; }
            if (value > 18.5m) { return 76.25m; }
            if (value > 18.4m) { return 76; }
            if (value > 18.3m) { return 75.75m; }
            if (value > 18.2m) { return 75.5m; }
            if (value > 18.1m) { return 75.25m; }
            if (value > 18) { return 75; }
            if (value > 17.9m) { return 74.75m; }
            if (value > 17.8m) { return 74.5m; }
            if (value > 17.7m) { return 74.25m; }
            if (value > 17.6m) { return 74; }
            if (value > 17.5m) { return 73.75m; }
            if (value > 17.4m) { return 73.5m; }
            if (value > 17.3m) { return 73.25m; }
            if (value > 17.2m) { return 73; }
            if (value > 17.1m) { return 72.75m; }
            if (value > 17) { return 72.5m; }
            if (value > 16.9m) { return 72.25m; }
            if (value > 16.8m) { return 72; }
            if (value > 16.7m) { return 71.75m; }
            if (value > 16.6m) { return 71.5m; }
            if (value > 16.5m) { return 71.25m; }
            if (value > 16.4m) { return 71; }
            if (value > 16.3m) { return 70.75m; }
            if (value > 16.2m) { return 70.5m; }
            if (value > 16.1m) { return 70.25m; }
            if (value > 16) { return 70; }
            if (value > 15.9m) { return 69.75m; }
            if (value > 15.8m) { return 69.5m; }
            if (value > 15.7m) { return 69.25m; }
            if (value > 15.6m) { return 69; }
            if (value > 15.5m) { return 68.75m; }
            if (value > 15.4m) { return 68.5m; }
            if (value > 15.3m) { return 68.25m; }
            if (value > 15.2m) { return 68; }
            if (value > 15.1m) { return 67.75m; }
            if (value > 15) { return 67.5m; }
            if (value > 14.9m) { return 67.25m; }
            if (value > 14.8m) { return 67; }
            if (value > 14.7m) { return 66.75m; }
            if (value > 14.6m) { return 66.5m; }
            if (value > 14.5m) { return 66.25m; }
            if (value > 14.4m) { return 66; }
            if (value > 14.3m) { return 65.75m; }
            if (value > 14.2m) { return 65.5m; }
            if (value > 14.1m) { return 65.25m; }
            if (value > 14) { return 65; }
            if (value > 13.9m) { return 64.75m; }
            if (value > 13.8m) { return 64.5m; }
            if (value > 13.7m) { return 64.25m; }
            if (value > 13.6m) { return 64; }
            if (value > 13.5m) { return 63.75m; }
            if (value > 13.4m) { return 63.5m; }
            if (value > 13.3m) { return 63.25m; }
            if (value > 13.2m) { return 63; }
            if (value > 13.1m) { return 62.75m; }
            if (value > 13) { return 62.5m; }
            if (value > 12.9m) { return 62.25m; }
            if (value > 12.8m) { return 62; }
            if (value > 12.7m) { return 61.75m; }
            if (value > 12.6m) { return 61.5m; }
            if (value > 12.5m) { return 61.25m; }
            if (value > 12.4m) { return 61; }
            if (value > 12.3m) { return 60.75m; }
            if (value > 12.2m) { return 60.5m; }
            if (value > 12.1m) { return 60.25m; }
            if (value > 12) { return 60; }
            if (value > 11.9m) { return 59.75m; }
            if (value > 11.8m) { return 59.5m; }
            if (value > 11.7m) { return 59.25m; }
            if (value > 11.6m) { return 59; }
            if (value > 11.5m) { return 58.75m; }
            if (value > 11.4m) { return 58.5m; }
            if (value > 11.3m) { return 58.25m; }
            if (value > 11.2m) { return 58; }
            if (value > 11.1m) { return 57.75m; }
            if (value > 11) { return 57.5m; }
            if (value > 10.9m) { return 57.25m; }
            if (value > 10.8m) { return 57; }
            if (value > 10.7m) { return 56.75m; }
            if (value > 10.6m) { return 56.5m; }
            if (value > 10.5m) { return 56.25m; }
            if (value > 10.4m) { return 56; }
            if (value > 10.3m) { return 55.75m; }
            if (value > 10.2m) { return 55.5m; }
            if (value > 10.1m) { return 55.25m; }
            if (value > 10) { return 55; }
            if (value > 9.9m) { return 54.75m; }
            if (value > 9.8m) { return 54.5m; }
            if (value > 9.7m) { return 54.25m; }
            if (value > 9.6m) { return 54; }
            if (value > 9.5m) { return 53.75m; }
            if (value > 9.4m) { return 53.5m; }
            if (value > 9.3m) { return 53.25m; }
            if (value > 9.2m) { return 53; }
            if (value > 9.1m) { return 52.75m; }
            if (value > 9) { return 52.5m; }
            if (value > 8.9m) { return 52.25m; }
            if (value > 8.8m) { return 52; }
            if (value > 8.7m) { return 51.75m; }
            if (value > 8.6m) { return 51.5m; }
            if (value > 8.5m) { return 51.25m; }
            if (value > 8.4m) { return 51; }
            if (value > 8.3m) { return 50.75m; }
            if (value > 8.2m) { return 50.5m; }
            if (value > 8.1m) { return 50.25m; }
            if (value > 8) { return 50; }
            if (value > 7.9m) { return 45.75m; }
            if (value > 7.8m) { return 45.5m; }
            if (value > 7.7m) { return 45.25m; }
            if (value > 7.6m) { return 45; }
            if (value > 7.5m) { return 44.75m; }
            if (value > 7.4m) { return 44.5m; }
            if (value > 7.3m) { return 44.25m; }
            if (value > 7.2m) { return 44; }
            if (value > 7.1m) { return 43.75m; }
            if (value > 7) { return 43.5m; }
            if (value > 6.9m) { return 43.25m; }
            if (value > 6.8m) { return 43; }
            if (value > 6.7m) { return 42.75m; }
            if (value > 6.6m) { return 42.5m; }
            if (value > 6.5m) { return 42.25m; }
            if (value > 6.4m) { return 42; }
            if (value > 6.3m) { return 41.75m; }
            if (value > 6.2m) { return 41.5m; }
            if (value > 6.1m) { return 41.25m; }
            if (value > 6) { return 41; }
            if (value > 5.9m) { return 41.75m; }
            if (value > 5.8m) { return 41.5m; }
            if (value > 5.7m) { return 41.25m; }
            if (value > 5.6m) { return 41; }
            if (value > 5.5m) { return 40.75m; }
            if (value > 5.4m) { return 40.5m; }
            if (value > 5.3m) { return 40.25m; }
            if (value > 5.2m) { return 40; }
            if (value > 5.1m) { return 39.75m; }
            if (value > 5) { return 39.5m; }
            if (value > 4.9m) { return 38.75m; }
            if (value > 4.8m) { return 38.5m; }
            if (value > 4.7m) { return 38.25m; }
            if (value > 4.6m) { return 38; }
            if (value > 4.5m) { return 37.75m; }
            if (value > 4.4m) { return 37.5m; }
            if (value > 4.3m) { return 37.25m; }
            if (value > 4.2m) { return 37; }
            if (value > 4.1m) { return 36.75m; }
            if (value > 4) { return 36.5m; }
            if (value > 3.9m) { return 36.25m; }
            if (value > 3.8m) { return 36; }
            if (value > 3.7m) { return 35.75m; }
            if (value > 3.6m) { return 35.5m; }
            if (value > 3.5m) { return 35.25m; }
            return 35;
        }
    }
}