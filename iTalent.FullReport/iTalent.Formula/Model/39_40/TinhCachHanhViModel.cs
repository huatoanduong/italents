﻿using iTalent.Formula.Model.ExcelTemplate;
using System.Collections.Generic;
using System.Linq;
#pragma warning disable 1587

namespace iTalent.Formula.Model._39_40
{
    public class TinhCachHanhViModel
    {
        public int Code { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsAdult { get; set; }

        public IntelligenceModelCollection IntelligenceModel { get; set; }
        public PIModelCollection PICollection { get; set; }
        public DacTinhTiemThucModel DacTinhTiemThucModel { get; set; }
        public DacTinhTuDuyModel DacTinhTuDuyModel { get; set; }


        public TinhCachHanhViModel(PIModelCollection piCollection, IntelligenceModelCollection intelligenceModel,
            DacTinhTiemThucModel tiemThucModel, DacTinhTuDuyModel tuDuyModel,
            ExcelTinhCachHanhViCollection excelTinhCachHanhViCollection, bool isAdult)
        {
            this.PICollection = piCollection;
            this.IntelligenceModel = intelligenceModel;
            this.DacTinhTiemThucModel = tiemThucModel;
            this.DacTinhTuDuyModel = tuDuyModel;
            if (isAdult)
            {
                CalculateValueAdult(excelTinhCachHanhViCollection);
            }
            else
            {
                CalculateValueChild(excelTinhCachHanhViCollection);
            }
        }

        private void CalculateValueChild(ExcelTinhCachHanhViCollection collection)
        {
            int i = 0;
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();
            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);

            List<string> last6FingerTypes = new List<string>();
            last6FingerTypes.Add(PICollection.L3.FingerType);
            last6FingerTypes.Add(PICollection.L4.FingerType);
            last6FingerTypes.Add(PICollection.L5.FingerType);
            last6FingerTypes.Add(PICollection.R3.FingerType);
            last6FingerTypes.Add(PICollection.R4.FingerType);
            last6FingerTypes.Add(PICollection.R5.FingerType);

            List<string> last8FingerTypes = new List<string>();
            last8FingerTypes.Add(PICollection.L2.FingerType);
            last8FingerTypes.Add(PICollection.L3.FingerType);
            last8FingerTypes.Add(PICollection.L4.FingerType);
            last8FingerTypes.Add(PICollection.L5.FingerType);
            last8FingerTypes.Add(PICollection.R2.FingerType);
            last8FingerTypes.Add(PICollection.R3.FingerType);
            last8FingerTypes.Add(PICollection.R4.FingerType);
            last8FingerTypes.Add(PICollection.R5.FingerType);

            #region Unchange
            ///2. AR && AU && UL && OcPhanTich > OcNangDong
            i = 2;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                && fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                && this.IntelligenceModel.OcPhanTich > this.IntelligenceModel.OcNangDong
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///3. AR && AU && UL 
            i = 3;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                && fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///5. AU && RL && WC && UL && DacTinhTuDuy =Tự Mâu Thuẫn 
            i = 5;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                && (this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
                    || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///12. AS && RL && WC && UL && DacTinhTuDuy =Tự Mâu Thuẫn 
            i = 12;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                && (this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
                    || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///6. AU && RL
            i = 6;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///18. Arch && RL
            i = 18;
            if (
                fingerTypes.Count(p => p.StartsWith("A")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///7. AU && WD && UL
            i = 7;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///8. AU && UL
            i = 8;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///10. AT && UL
            i = 10;
            if (
                fingerTypes.Count(p => p.StartsWith("AT")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///1. AE
            i = 1;
            if (
                fingerTypes.Count(p => p.StartsWith("AE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///4. AR
            i = 4;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///9. AU
            i = 9;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///11. AT
            i = 11;
            if (
                fingerTypes.Count(p => p.StartsWith("AT")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///13. >= 3AS
            i = 13;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 3
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///14. AS && OcPhanTich - OcNangDong >= 4
            i = 14;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcPhanTich - this.IntelligenceModel.OcNangDong >= 4)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///15. AS && OcPhanTich - OcNangDong < 4
            i = 15;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcPhanTich > this.IntelligenceModel.OcNangDong)
                && (this.IntelligenceModel.OcPhanTich - this.IntelligenceModel.OcNangDong < 4)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///16. AS && OcNangDong > OcPhanTich
            i = 16;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///17. 1AS
            i = 17;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///19. Arch && WE
            i = 19;
            if (
                fingerTypes.Count(p => p.StartsWith("A")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///20. Arch && WC
            i = 20;
            if (
                fingerTypes.Count(p => p.StartsWith("A")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///21. RL  && WD && Count(WD) >= 2
            i = 21;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WD")) >= 2
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///22. RL && WC && WE
            i = 22;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///23. RL && WC && UL
            i = 23;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///24. RL && WD
            i = 24;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///25. RL && PI1 > TFRC/10
            i = 25;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///26. RL && PI6 > TFRC/10
            i = 26;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///27. RL && PI10 > TFRC/10
            i = 27;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && this.PICollection.R5.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///28. RL
            i = 28;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }



            ///29. WP
            i = 29;
            if (
                fingerTypes.Count(p => p.StartsWith("WP")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///30. WL
            i = 30;
            if (
                fingerTypes.Count(p => p.StartsWith("WL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion Unchange

            #region 4 Ngon Whorl
            ///31. WC && WS && WE && WT
            i = 31;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///32. 3WD && WE
            i = 32;
            if (
                //fingerTypes.Count(p => p.StartsWith("WD")) >= 3
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 3
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///33. W && WD && WE 
            i = 33;
            if (
                //fingerTypes.Count(p => p.StartsWith("W")) >= 3
                //&& fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("W")) >= 3
                && first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///34. 3WE trong L1T || L2T ||R1T || R2T
            i = 34;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 3
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion 4 Ngon Whorl

            #region Sharp UL 10 UL - 9UL
            ///67. 10UL Ngón Cái && Thùy Trước Trán > 23% && OcNangDong > OcPhanTich
            i = 67;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.IntelligenceModel.ThuyTruocTran > 23
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///68. 10UL Ngón Cái && Thùy Trước Trán > 23%
            i = 68;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.IntelligenceModel.ThuyTruocTran > 23
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///69. (10UL && PI1> TFRC/10) 
            ///|| (4UL && PI1 >TFRC/10) 
            ///|| (2UL ngon cai && PI R1 va PI L1 > tfrc/10)
            i = 69;
            if (
                (fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10)
                ||
                (first4FingerTypes.Count(p => p.StartsWith("UL")) >= 4
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///70. (10UL && PI6> TFRC/10) 
            ///|| (4UL && PI16>TFRC/10) 
            ///|| (2UL ngon cai && PI R1 va PI L1 > tfrc/10)
            i = 70;
            if (
                //fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                //&& this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                (fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
                ||
                (first4FingerTypes.Count(p => p.StartsWith("UL")) >= 4
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
               )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///45. WE && UL && PI10 > TFRC/10
            i = 45;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                //&& this.PICollection.R5.PIValue > this.PICollection.TFRC / 10
                fingerTypes.Count(p => p.StartsWith("UL")) >= 9
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                bool IsCheck = false;
                //Mot trong hai ngon cai WE kiem tra PI WE va PI UL > TFRC/10
                if ((this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.R1.FingerType.StartsWith("WE"))
                    && (this.PICollection.L1.PIValue > this.PICollection.TFRC / 10 && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10))
                {
                    IsCheck = true;
                }
                //Ca hai ngon cai deu khong phai WE thi 1 trong 2 ngon cai co PI UL > TFRC /10 
                // thi tim WE  
                // PI(WE) > TFRC/10
                if ((!this.PICollection.L1.FingerType.StartsWith("WE") && !this.PICollection.R1.FingerType.StartsWith("WE"))
                    && (this.PICollection.L1.PIValue > this.PICollection.TFRC / 10 || this.PICollection.R1.PIValue > this.PICollection.TFRC / 10))
                {
                    PIModel model = PICollection.FindByFingerType("WE");
                    if (model != null)
                        if (model.PIValue >= this.PICollection.TFRC / 10)
                        {
                            IsCheck = true;
                        }
                }
                if (IsCheck)
                {
                    ExcelTinhCachHanhVi tmp = collection.Find(i);
                    Code = tmp.Code;
                    Title = tmp.Title;
                    Content = tmp.Content;
                    return;
                }
            }

            ///56. (9UL va WS) if (WS  !4 ngon dau tien PI(WS) >(TFRC/10)
            i = 56;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 9
                && first4FingerTypes.Count(p => p.StartsWith("WS")) < 1
                )
            {
                PIModel model = PICollection.FindByFingerType("WS");
                if (model != null)
                    if (model.PIValue >= this.PICollection.TFRC / 10)
                    {
                        ExcelTinhCachHanhVi tmp = collection.Find(i);
                        Code = tmp.Code;
                        Title = tmp.Title;
                        Content = tmp.Content;
                        return;
                    }
            }

            ///56. (3UL va WS la ngon tro) 
            i = 56;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 3
                && (PICollection.L2.FingerType.StartsWith("WS") || PICollection.R2.FingerType.StartsWith("WS"))
                )
            {
                PIModel model = PICollection.FindByFingerType("WS");
                if (model != null)
                    if (model.PIValue >= this.PICollection.TFRC / 10)
                    {
                        ExcelTinhCachHanhVi tmp = collection.Find(i);
                        Code = tmp.Code;
                        Title = tmp.Title;
                        Content = tmp.Content;
                        return;
                    }
            }
            #endregion Sharp UL 10 UL - 9UL

            #region Chỉ xét 2 ngón cái
            ///35. WC && WE && PI5 > TFRC/10
            i = 35;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC")
                && this.PICollection.R1.FingerType.StartsWith("WE")
                && this.PICollection.L5.PIValue > this.PICollection.TFRC / 10)
                ||
                (this.PICollection.L1.FingerType.StartsWith("WE")
                && this.PICollection.R1.FingerType.StartsWith("WC")
                && this.PICollection.L5.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///36. WE && WD là ngón cái
            i = 36;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WD"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WD")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///37. WC && WE
            i = 37;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                (this.PICollection.L1.FingerType.StartsWith("WC")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WC"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///38. WE  && WS 
            i = 38;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WS"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WS")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///41. 2 WE trong L1T || L2T || R1T || R2T
            i = 41;
            if (this.PICollection.L1.FingerType.StartsWith("WE") && this.PICollection.R1.FingerType.StartsWith("WE"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///42. >=2 WD
            i = 42;
            if (PICollection.L1.FingerType.StartsWith("WD")
                && PICollection.R1.FingerType.StartsWith("WD"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///43. >=2 WC
            i = 43;
            if (PICollection.L1.FingerType.StartsWith("WC")
                && PICollection.R1.FingerType.StartsWith("WC"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///39. WC && WE && UL la 2 ngon cai
            i = 39;
            if (
                (PICollection.L1.FingerType.StartsWith("UL") 
                && PICollection.R1.FingerType.StartsWith("UL")
                && PICollection.L2.FingerType.StartsWith("WC") 
                && PICollection.R2.FingerType.StartsWith("WE"))
                ||
                (PICollection.L1.FingerType.StartsWith("UL")
                && PICollection.R1.FingerType.StartsWith("UL")
                && PICollection.L2.FingerType.StartsWith("WE")
                && PICollection.R2.FingerType.StartsWith("WC"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///40. UL ở 2 ngón cái && WD && WE & 6 ngon con lai UL,
            ///PI(WD) > TFRC/10
            i = 40;
            if (
                this.PICollection.L1.FingerType.StartsWith("UL")
                && this.PICollection.R1.FingerType.StartsWith("UL")
                && last8FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && last8FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && last8FingerTypes.Count(p => p.StartsWith("UL")) >= 6
                )
            {
                PIModel model = PICollection.FindByFingerType("WD");
                if (model != null)
                    if (model.PIValue >= this.PICollection.TFRC / 10)
                    {
                        ExcelTinhCachHanhVi tmp = collection.Find(i);
                        Code = tmp.Code;
                        Title = tmp.Title;
                        Content = tmp.Content;
                        return;
                    }
            }

            ///39. WC && WE && UL la ngon cai L1 or R1
            i = 39;
            if (
                (this.PICollection.L1.FingerType.StartsWith("UL") || this.PICollection.R1.FingerType.StartsWith("UL"))
                && (
                    (this.PICollection.L2.FingerType.StartsWith("WE") && this.PICollection.R2.FingerType.StartsWith("WC"))
                    || (this.PICollection.L2.FingerType.StartsWith("WC") && this.PICollection.R2.FingerType.StartsWith("WE"))
                    )
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///57. WE && UL
            i = 57;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL")
                && (PICollection.L2.FingerType == "WE" && PICollection.R2.FingerType == "WE")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///57. WE && UL
            i = 57;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL")
                && (PICollection.L2.FingerType == "WE" || PICollection.R2.FingerType == "WE")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///58. WS && UL
            i = 58;
            if ((PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "WS")
                || (PICollection.L1.FingerType == "WS" && PICollection.R1.FingerType == "UL")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///59. WC && UL
            i = 59;
            if ((PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "WC")
                || (PICollection.L1.FingerType == "WC" && PICollection.R1.FingerType == "UL")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///60. WD && UL
            i = 60;
            if ((PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "WD")
                || (PICollection.L1.FingerType == "WD" && PICollection.R1.FingerType == "UL")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///69.  (2UL ngon cai && PI R1 va PI L1 > tfrc/10)
            i = 69;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL"
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///70.  (2UL ngon cai && PI R1 va PI L1 > tfrc/10)
            i = 70;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL"
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
               )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///44. WE && (PI1 > TFRC/10) && (PI6 > TFRC/10)
            i = 44;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.R1.FingerType.StartsWith("WE"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///46. WE && NaoPhai > Nao Trai
            i = 46;
            if (
                (PICollection.L1.FingerType.StartsWith("WE") || PICollection.R1.FingerType.StartsWith("WE"))
                && this.IntelligenceModel.NaoPhai > this.IntelligenceModel.NaoTrai
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///47. WE && OcNangDong > OcPhanTich
            i = 47;
            if (
                (PICollection.L1.FingerType.StartsWith("WE") || PICollection.R1.FingerType.StartsWith("WE"))
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///48. WS && OcNangDong > OcPhanTich
            i = 48;
            if (
                (PICollection.L1.FingerType.StartsWith("WS") || PICollection.R1.FingerType.StartsWith("WS"))
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///49. WC && PI1 >TFRC/10
            i = 49;
            if (
                (PICollection.L1.FingerType.StartsWith("WC") || PICollection.R1.FingerType.StartsWith("WC"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///50. WC && PI6>TFRC/10
            i = 50;
            if (
                (PICollection.L1.FingerType.StartsWith("WC") || PICollection.R1.FingerType.StartsWith("WC"))
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///51. WD && PI1 > TFRC/10
            i = 51;
            if (
                (PICollection.L1.FingerType.StartsWith("WD") || PICollection.R1.FingerType.StartsWith("WD"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///52. WD && PI6 > TFRC/10
            i = 52;
            if (
                (PICollection.L1.FingerType.StartsWith("WD") || PICollection.R1.FingerType.StartsWith("WD"))
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///53. WE && PI1 > TFRC/10
            i = 53;
            if (
                (PICollection.L1.FingerType.StartsWith("WE") || PICollection.R1.FingerType.StartsWith("WE"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///54. WT && PI1 > TFRC/10
            i = 54;
            if (
                (PICollection.L1.FingerType.StartsWith("WT") || PICollection.R1.FingerType.StartsWith("WT"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///55. WT && PI6 > TFRC/10
            i = 55;
            if (
                (PICollection.L1.FingerType.StartsWith("WT") || PICollection.R1.FingerType.StartsWith("WT"))
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///61. WE
            i = 61;
            if (
                (PICollection.L1.FingerType.StartsWith("WE") || PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///62. WI
            i = 62;
            if (
                (PICollection.L1.FingerType.StartsWith("WI") || PICollection.R1.FingerType.StartsWith("WI"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///63. WS
            i = 63;
            if (
                (PICollection.L1.FingerType.StartsWith("WS") || PICollection.R1.FingerType.StartsWith("WS"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///64. WT
            i = 64;
            if (
                (PICollection.L1.FingerType.StartsWith("WT") || PICollection.R1.FingerType.StartsWith("WT"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///65. WC
            i = 65;
            if (
                (PICollection.L1.FingerType.StartsWith("WC") || PICollection.R1.FingerType.StartsWith("WC"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///66. WD
            i = 66;
            if (
                (PICollection.L1.FingerType.StartsWith("WD") || PICollection.R1.FingerType.StartsWith("WD"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            /// UL
            i = 71;
            if (
                (PICollection.L1.FingerType.StartsWith("UL") || PICollection.R1.FingerType.StartsWith("UL"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            /// LF
            i = 72;
            if (
                (PICollection.L1.FingerType.StartsWith("LF") || PICollection.R1.FingerType.StartsWith("LF"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            /// WX
            i = 73;
            if (
                (PICollection.L1.FingerType.StartsWith("WX") || PICollection.R1.FingerType.StartsWith("WX"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion Chỉ xét 2 ngón cái

            #region Xet 4 Ngon
            ///35. WC && WE && PI10 > TFRC/10
            i = 35;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& this.PICollection.R5.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.PICollection.L5.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///37. WC && WE
            i = 37;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///41. 2 WE trong L1T || L2T || R1T || R2T
            i = 41;
            if (first4FingerTypes.Count(p => p.StartsWith("WE")) >= 2)
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///42. >=2 WD
            i = 42;
            if (first4FingerTypes.Count(p => p.StartsWith("WD")) >= 2)
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///43. >=2 WC
            i = 43;
            if (first4FingerTypes.Count(p => p.StartsWith("WC")) >= 2)
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///58. WS && UL
            i = 58;
            if ((PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///59. WC && UL
            i = 59;
            if ((PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///60. WD && UL
            i = 60;
            if ((PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///44. WE && (PI1 > TFRC/10) && (PI6 > TFRC/10)
            i = 44;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///47. WE && OcNangDong > OcPhanTich
            i = 47;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///48. WS && OcNangDong > OcPhanTich
            i = 48;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///49. WC && PI1 >TFRC/10
            i = 49;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///50. WC && PI6>TFRC/10
            i = 50;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///51. WD && PI1 > TFRC/10
            i = 51;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///52. WD && PI6 > TFRC/10
            i = 52;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///53. WE && PI1 > TFRC/10
            i = 53;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///54. WT && PI1 > TFRC/10
            i = 54;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///55. WT && PI6 > TFRC/10
            i = 55;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///61. WE
            i = 61;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///62. WI
            i = 62;
            if (
                //fingerTypes.Count(p => p.StartsWith("WI")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WI")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///63. WS
            i = 63;
            if (
                //fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///64. WT
            i = 64;
            if (
                //fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///65. WC
            i = 65;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///66. WD
            i = 66;
            if (
                //fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            /// UL
            i = 71;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            /// LF
            i = 72;
            if (
                fingerTypes.Count(p => p.StartsWith("LF")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            /// WX
            i = 73;
            if (
                fingerTypes.Count(p => p.StartsWith("WX")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion Xet 4 Ngon

        }


        private void CalculateValueAdult(ExcelTinhCachHanhViCollection collection)
        {
            int i = 0;
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();

            List<string> thumpFingerTypes = new List<string>();
            thumpFingerTypes.Add(PICollection.L1.FingerType);
            thumpFingerTypes.Add(PICollection.R1.FingerType);

            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);

            List<string> last6FingerTypes = new List<string>();
            last6FingerTypes.Add(PICollection.L3.FingerType);
            last6FingerTypes.Add(PICollection.L4.FingerType);
            last6FingerTypes.Add(PICollection.L5.FingerType);
            last6FingerTypes.Add(PICollection.R3.FingerType);
            last6FingerTypes.Add(PICollection.R4.FingerType);
            last6FingerTypes.Add(PICollection.R5.FingerType);

            List<string> last8FingerTypes = new List<string>();
            last8FingerTypes.Add(PICollection.L2.FingerType);
            last8FingerTypes.Add(PICollection.L3.FingerType);
            last8FingerTypes.Add(PICollection.L4.FingerType);
            last8FingerTypes.Add(PICollection.L5.FingerType);
            last8FingerTypes.Add(PICollection.R2.FingerType);
            last8FingerTypes.Add(PICollection.R3.FingerType);
            last8FingerTypes.Add(PICollection.R4.FingerType);
            last8FingerTypes.Add(PICollection.R5.FingerType);

            #region Arch
            ///2. AR && AU && UL && OcPhanTich > OcNangDong
            i = 2;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                && fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                && this.IntelligenceModel.OcPhanTich > this.IntelligenceModel.OcNangDong
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///3. AR && AU && UL 
            i = 3;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                && fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///5. AU && RL && WC && UL && DacTinhTuDuy =Tự Mâu Thuẫn 
            i = 5;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                && (this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
                    || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///12. AS && RL && WC && UL && DacTinhTuDuy =Tự Mâu Thuẫn 
            i = 12;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                && (this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
                    || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///6. AU && RL
            i = 6;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///20. Arch && RL
            i = 20;
            if (
                fingerTypes.Count(p => p.StartsWith("A")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///7. AU && WD && UL
            i = 7;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///8. AU && UL
            i = 8;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///10. AT && UL
            i = 10;
            if (
                fingerTypes.Count(p => p.StartsWith("AT")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///1. AE
            i = 1;
            if (
                fingerTypes.Count(p => p.StartsWith("AE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            ///4. AR
            i = 4;
            if (
                fingerTypes.Count(p => p.StartsWith("AR")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///9. AU
            i = 9;
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///11. AT
            i = 11;
            if (
                fingerTypes.Count(p => p.StartsWith("AT")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///13. >= 3AS
            i = 13;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 3
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///14. 2AS
            i = 14;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 2
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///15. AS && OcPhanTich - OcNangDong >= 4
            i = 15;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcPhanTich - this.IntelligenceModel.OcNangDong >= 4)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///16. AS && OcPhanTich - OcNangDong < 4
            i = 16;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcPhanTich > this.IntelligenceModel.OcNangDong)
                && (this.IntelligenceModel.OcPhanTich - this.IntelligenceModel.OcNangDong < 4)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///17. AS && OcNangDong > OcPhanTich
            i = 17;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///18. AS && OcPhanTich > OcNangDong
            i = 18;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                && (this.IntelligenceModel.OcPhanTich > this.IntelligenceModel.OcNangDong)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///19. 1AS
            i = 19;
            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            
            ///21. Arch && WE
            i = 21;
            if (
                fingerTypes.Count(p => p.StartsWith("A")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion Arch

            #region RL
            ///22. RL  && WD && Count(WD) >= 2
            i = 22;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WD")) >= 2
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///23. RL && WC && WE
            i = 23;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///24. RL && WC && UL
            i = 24;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///25. RL && WD
            i = 25;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///26. >=2RL
            i = 26;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 2
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///27. RL && PI1 > TFRC/10
            i = 27;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///28. RL && PI10 > TFRC/10
            i = 28;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                && this.PICollection.R5.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///29. RL
            i = 29;
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion RL

            #region WP
            ///30. WP
            i = 30;
            if (
                fingerTypes.Count(p => p.StartsWith("WP")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion WP

            #region WL
            ///31. WL
            i = 31;
            if (
                fingerTypes.Count(p => p.StartsWith("WL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion WL

            #region 4 Ngón Whorl
            ///32. WC && WS && WE && WT
            i = 32;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion 4 Ngón Whorl

            #region 3 Ngón Whorl
            ///33. WS && WD && WE
            i = 33;
            if (
                //fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///34. W && WD && WE 
            i = 34;
            if (
                //fingerTypes.Count(p => p.StartsWith("W")) >= 3
                //&& fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("W")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1 && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///35. 3WE trong L1T || L2T ||R1T || R2T
            i = 35;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 3
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion 3 Ngón Whorl

            #region Sharp UL 10 UL , 9UL-WE
            ///67. 10UL  && Thùy Trước Trán > 23% && OcNangDong > OcPhanTich
            i = 67;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.IntelligenceModel.ThuyTruocTran > 23
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///68. 10UL Ngón Cái && Thùy Trước Trán > 23%
            i = 68;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.IntelligenceModel.ThuyTruocTran > 23
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///69. (10UL && PI1> TFRC/10) 
            ///|| (4UL && PI1 >TFRC/10) 
            i = 69;
            if (
                (fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10)
                ||
                (first4FingerTypes.Count(p => p.StartsWith("UL")) >= 4
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///70. (10UL && PI6> TFRC/10) 
            ///|| (4UL && PI16>TFRC/10) 
            i = 70;
            if (
                //fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                //&& this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                (fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
                ||
                (first4FingerTypes.Count(p => p.StartsWith("UL")) >= 4
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///45. WE && 9UL 
            ///&& PI(WE) > TFRC/10 && PI(UL ngon cai) > TFRC/10 co truong hop (1 trong 2 ngon UL > TFRC/10) 
            i = 45;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 9
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                bool IsCheck = false;
                //Mot trong hai ngon cai WE kiem tra PI WE va PI UL > TFRC/10
                if ((this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.R1.FingerType.StartsWith("WE"))
                    && (this.PICollection.L1.PIValue > this.PICollection.TFRC / 10 && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10))
                {
                    IsCheck = true;
                }
                //Ca hai ngon cai deu khong phai WE thi 1 trong 2 ngon cai co PI UL > TFRC /10 
                // thi tim WE  
                // PI(WE) > TFRC/10
                if ((!this.PICollection.L1.FingerType.StartsWith("WE") && !this.PICollection.R1.FingerType.StartsWith("WE"))
                    && (this.PICollection.L1.PIValue > this.PICollection.TFRC / 10 || this.PICollection.R1.PIValue > this.PICollection.TFRC / 10))
                {
                    PIModel model = PICollection.FindByFingerType("WE");
                    if (model != null)
                        if (model.PIValue >= this.PICollection.TFRC / 10)
                        {
                            IsCheck = true;
                        }
                }
                if (IsCheck)
                {
                    ExcelTinhCachHanhVi tmp = collection.Find(i);
                    Code = tmp.Code;
                    Title = tmp.Title;
                    Content = tmp.Content;
                    return;
                }
            }

            ///56. (3UL va WS la ngon tro) 
            i = 56;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 3
                && (PICollection.L2.FingerType.StartsWith("WS") || PICollection.R2.FingerType.StartsWith("WS"))
                )
            {
                PIModel model = PICollection.FindByFingerType("WS");
                if (model != null)
                    if (model.PIValue >= this.PICollection.TFRC / 10)
                    {
                        ExcelTinhCachHanhVi tmp = collection.Find(i);
                        Code = tmp.Code;
                        Title = tmp.Title;
                        Content = tmp.Content;
                        return;
                    }
            }

            i = 56;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 9
                && first4FingerTypes.Count(p => p.StartsWith("WS")) < 1
                )
            {
                PIModel model = PICollection.FindByFingerType("WS");
                if (model != null)
                    if (model.PIValue >= this.PICollection.TFRC / 10)
                    {
                        ExcelTinhCachHanhVi tmp = collection.Find(i);
                        Code = tmp.Code;
                        Title = tmp.Title;
                        Content = tmp.Content;
                        return;
                    }
            }
            #endregion Sharp UL 10 UL

            #region Chỉ xét 2 ngón cái
            ///36. WC && WE && PI5 > TFRC/10
            i = 36;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC") 
                && this.PICollection.R1.FingerType.StartsWith("WE") 
                && this.PICollection.L5.PIValue > this.PICollection.TFRC / 10)
                ||
                (this.PICollection.L1.FingerType.StartsWith("WE") 
                && this.PICollection.R1.FingerType.StartsWith("WC")
                && this.PICollection.L5.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            i = 37;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WD"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WD")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            i = 38;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WC"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///39. WE  && WS
            i = 39;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WS"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WS")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///42. 2 WE trong L1T || L2T || R1T || R2T
            i = 42;
            if (
                 (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 && this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///43. >=2 WD
            i = 43;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WD")
                 && this.PICollection.R1.FingerType.StartsWith("WD"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WD")
                 && this.PICollection.R1.FingerType.StartsWith("WD"))
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///40. WC && WE && UL là 2 ngón cái
            i = 40;
            if (
                (this.PICollection.L1.FingerType.StartsWith("UL") && this.PICollection.R1.FingerType.StartsWith("UL"))
                && (
                    (this.PICollection.L2.FingerType.StartsWith("WE") && this.PICollection.R2.FingerType.StartsWith("WC"))
                    || (this.PICollection.L2.FingerType.StartsWith("WC") && this.PICollection.R2.FingerType.StartsWith("WE"))
                    )
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///41. UL ở 2 ngón cái && WD && WE & 6 ngon con lai UL, PI(WD) > TFRC/10
            i = 41;
            if (
                this.PICollection.L1.FingerType.StartsWith("UL")
                && this.PICollection.R1.FingerType.StartsWith("UL")
                && last8FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && last8FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && last8FingerTypes.Count(p => p.StartsWith("UL")) >= 6
                )
            {
                PIModel model = PICollection.FindByFingerType("WD");
                if (model != null)
                    if (model.PIValue >= this.PICollection.TFRC / 10)
                    {
                        ExcelTinhCachHanhVi tmp = collection.Find(i);
                        Code = tmp.Code;
                        Title = tmp.Title;
                        Content = tmp.Content;
                        return;
                    }
            }

            ///57. 2WE ngón trỏ && 2UL ngón cái
            i = 57;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL")
                && (PICollection.L2.FingerType == "WE" && PICollection.R2.FingerType == "WE")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///40. WC && WE && UL là 1 ngón cái L1 or R1
            i = 40;
            if (
                (this.PICollection.L1.FingerType.StartsWith("UL") || this.PICollection.R1.FingerType.StartsWith("UL"))
                && ( 
                    (this.PICollection.L2.FingerType.StartsWith("WE") && this.PICollection.R2.FingerType.StartsWith("WC"))
                    || (this.PICollection.L2.FingerType.StartsWith("WC") && this.PICollection.R2.FingerType.StartsWith("WE"))
                    )
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///57. WE && UL
            i = 57;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "WE")
                || (PICollection.L1.FingerType == "WE" && PICollection.R1.FingerType == "UL")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///58. WS && UL
            i = 58;
            if (
                (PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (PICollection.L2.FingerType == "WS" || PICollection.R2.FingerType == "WS")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///59. WC && UL
            i = 59;
            if (
                (PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (PICollection.L2.FingerType == "WC" || PICollection.R2.FingerType == "WC")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///60. WD && UL
            i = 60;
            if (
                (PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (PICollection.L2.FingerType == "WD" || PICollection.R2.FingerType == "WD")
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///69.  (2UL ngon cai && PI R1 va PI L1 > tfrc/10)
            i = 69;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL"
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///70. (2UL ngon cai && PI R1 va PI L1 > tfrc/10)
            i = 70;
            if (
                (PICollection.L1.FingerType == "UL" && PICollection.R1.FingerType == "UL"
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///44. WE && (PI1 > TFRC/10) && (PI6 > TFRC/10)
            i = 44;
            if (
                (PICollection.L1.FingerType == "WE" || PICollection.R1.FingerType == "WE")
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///46.(L1 or L2) la WE  && NaoPhai > Nao Trai
            i = 46;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.L2.FingerType.StartsWith("WE"))
                && this.IntelligenceModel.NaoPhai > this.IntelligenceModel.NaoTrai
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///47.L1 or R1 la WE && OcNangDong > OcPhanTich
            i = 47;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.R1.FingerType.StartsWith("WE"))
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///48. L1 or R1 la WS && OcNangDong > OcPhanTich
            i = 48;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WS") || this.PICollection.R1.FingerType.StartsWith("WS"))
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///49. WC && PI L1 >TFRC/10
            i = 49;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC") || this.PICollection.R1.FingerType.StartsWith("WC"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///50. WC && PI R1>TFRC/10
            i = 50;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC") || this.PICollection.R1.FingerType.StartsWith("WC"))
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///51. WD && PI L1 > TFRC/10
            i = 51;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WD") || this.PICollection.R1.FingerType.StartsWith("WD"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///52. WD && PI6 > TFRC/10
            i = 52;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WD") || this.PICollection.R1.FingerType.StartsWith("WD"))
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///53. WE && PI1 > TFRC/10
            i = 53;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                (this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.R1.FingerType.StartsWith("WE"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///54. WT && PI1 > TFRC/10
            i = 54;
            if (
                (this.PICollection.L1.FingerType.StartsWith("WT") || this.PICollection.R1.FingerType.StartsWith("WT"))
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///55. WT && PI6 > TFRC/10
            i = 55;
            if (
                 (this.PICollection.L1.FingerType.StartsWith("WT") || this.PICollection.R1.FingerType.StartsWith("WT"))
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///61. WE
            i = 61;
            if (this.PICollection.L1.FingerType.StartsWith("WE") || this.PICollection.R1.FingerType.StartsWith("WE"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///62. WI
            i = 62;
            if (this.PICollection.L1.FingerType.StartsWith("WI") || this.PICollection.R1.FingerType.StartsWith("WI"))
                {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///63. WS
            i = 63;
            if (this.PICollection.L1.FingerType.StartsWith("WS") || this.PICollection.R1.FingerType.StartsWith("WS"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///64. WT
            i = 64;
            if (this.PICollection.L1.FingerType.StartsWith("WT") || this.PICollection.R1.FingerType.StartsWith("WT"))
                {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///65. WC
            i = 65;
            if (this.PICollection.L1.FingerType.StartsWith("WC") || this.PICollection.R1.FingerType.StartsWith("WC"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///66. WD
            i = 66;
            if (this.PICollection.L1.FingerType.StartsWith("WD") || this.PICollection.R1.FingerType.StartsWith("WD"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///71. UL
            i = 71;
            if (PICollection.L1.FingerType.StartsWith("UL") || PICollection.R1.FingerType.StartsWith("UL"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///72. LF
            i = 72;
            if (PICollection.L1.FingerType.StartsWith("LF") || PICollection.R1.FingerType.StartsWith("LF"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///73. WX
            i = 73;
            if (PICollection.L1.FingerType.StartsWith("WX") || PICollection.R1.FingerType.StartsWith("WX"))
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion Chỉ xét 2 ngón cái

            #region Chỉ Xét 4 Ngón
            ///58. WS && UL
            i = 58;
            if (
                (PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (first4FingerTypes.Count(p=>p.StartsWith("WS")) >= 1)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///59. WC && UL
            i = 59;
            if (
                (PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///60. WD && UL
            i = 60;
            if (
                (PICollection.L1.FingerType == "UL" || PICollection.R1.FingerType == "UL")
                && (first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1)
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///36. WC && WE && PI5 > TFRC/10
            i = 36;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& this.PICollection.R5.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.PICollection.L5.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///38. WC && WE
            i = 38;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///42. 2 WE trong L1T || L2T || R1T || R2T
            i = 42;
            if (
                 first4FingerTypes.Count(p => p.StartsWith("WE")) >= 2
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///43. >=2 WD
            i = 43;
            if (
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 2
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }



            ///44. WE && (PI1 > TFRC/10) && (PI6 > TFRC/10)
            i = 44;
            if (
                //fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //&& this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                //&& this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///46.(L1 or L2) la WE  && NaoPhai > Nao Trai
            i = 46;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& this.IntelligenceModel.NaoPhai > this.IntelligenceModel.NaoTrai
                (this.PICollection.L1.FingerName.StartsWith("WE") || this.PICollection.L2.FingerName.StartsWith("WE"))
                && this.IntelligenceModel.NaoPhai > this.IntelligenceModel.NaoTrai
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///47.L1 or R1 la WE && OcNangDong > OcPhanTich
            i = 47;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                (this.PICollection.L1.FingerName.StartsWith("WE") || this.PICollection.R1.FingerName.StartsWith("WE"))
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///48. L1 or R1 la WS && OcNangDong > OcPhanTich
            i = 48;
            if (
                //fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                //&& this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                (this.PICollection.L1.FingerName.StartsWith("WS") || this.PICollection.R1.FingerName.StartsWith("WS"))
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///49. WC && PI L1 >TFRC/10
            i = 49;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& this.PICollection.R5.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///50. WC && PI R1>TFRC/10
            i = 50;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //&& this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///51. WD && PI L1 > TFRC/10
            i = 51;
            if (
                //fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //&& this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///52. WD && PI6 > TFRC/10
            i = 52;
            if (
                //fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //&& this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///53. WE && PI1 > TFRC/10
            i = 53;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                //&& this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///54. WT && PI1 > TFRC/10
            i = 54;
            if (
                //fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                //&& this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///55. WT && PI6 > TFRC/10
            i = 55;
            if (
                //fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                //&& this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                && this.PICollection.R1.PIValue > this.PICollection.TFRC / 10
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///61. WE
            i = 61;
            if (
                //fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///62. WI
            i = 62;
            if (
                //fingerTypes.Count(p => p.StartsWith("WI")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WI")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///63. WS
            i = 63;
            if (
                //fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WS")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///64. WT
            i = 64;
            if (
                //fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WT")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///65. WC
            i = 65;
            if (
                //fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WC")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///66. WD
            i = 66;
            if (
                //fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }


            ///71. UL
            i = 71;
            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///72. LF
            i = 72;
            if (
                fingerTypes.Count(p => p.StartsWith("LF")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }

            ///73. WX
            i = 73;
            if (
                fingerTypes.Count(p => p.StartsWith("WX")) >= 1
                )
            {
                ExcelTinhCachHanhVi tmp = collection.Find(i);
                Code = tmp.Code;
                Title = tmp.Title;
                Content = tmp.Content;
                return;
            }
            #endregion 4 Ngón

        }
    }
}