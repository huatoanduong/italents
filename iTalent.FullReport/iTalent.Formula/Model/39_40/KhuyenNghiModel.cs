﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Formula.Model.ExcelTemplate;

// ReSharper disable All

namespace iTalent.Formula.Model._39_40
{
    public enum KhuyenNghiEnum
    {
        /// Open & Absorb - Open, absorb, early planning
        open_absorb1,

        /// Open & Absorb - Open, absorb, early planning
        open_absorb2,

        /// Open & Absorb - Open, absorb, early planning
        open_absorb3,

        /// Open & Absorb - Open, absorb, early planning
        open_absorb4,

        /// Open & Absorb - Open, absorb, early planning
        open_absorb5,

        /// Open & Absorb - Open, absorb, early planning
        open_absorb6,

        /// Self-conscious - Open, democratic, building of values
        self_conscious1,

        /// Self-conscious - Open, democratic, building of values
        self_conscious2,

        /// Self-conscious - Open, democratic, building of values
        self_conscious3,

        /// Self-conscious - Open, democratic, building of values
        self_conscious4,

        /// Self-conscious - Open, democratic, building of values
        self_conscious5,

        /// Self-conscious - Open, democratic, building of values
        self_conscious6,

        /// Self-conscious - Open, democratic, building of values
        self_conscious7,

        /// Self-conscious - Open, democratic, building of values
        self_conscious8,

        /// Self-conscious - Open, democratic, building of values
        self_conscious9,

        /// Self-contradiction - Open, democratic, support & encouragement.
        self_contradiction,

        /// Ego-centric - Open, democratic, building of values
        ego_centric,

        /// Converse think - Impartial - fair about rewards and punishment, support and encouragement
        converse_think_1,

        /// Converse think - Impartial - fair about rewards and punishment, support and encouragement
        converse_think_2,

        /// Social norm
        social_norm,

        /// Social service - Established role model, group learning
        social_service,
    }

    public class KhuyenNghiModel
    {
        public IntelligenceModelCollection IntelligenceModel { get; set; }
        public PIModelCollection PICollection { get; set; }
        public DacTinhTiemThucModel DacTinhTiemThucModel { get; set; }
        public DacTinhTuDuyModel DacTinhTuDuyModel { get; set; }

        public KhuyenNghiEnum EnumValue { get; set; }
        public string Code { get; set; }
        public string TenNhomVietnamese { get; set; }
        public string TenNhomEnglish { get; set; }
        public string MoTa { get; set; }

        public KhuyenNghiModel(PIModelCollection piCollection, IntelligenceModelCollection intelligenceModel,
            DacTinhTiemThucModel tiemThucModel, DacTinhTuDuyModel tuDuyModel,
            ExcelKhuyenNghiCollection excelKhuyenNghiCollection, bool isAdult)
        {
            this.PICollection = piCollection;
            this.IntelligenceModel = intelligenceModel;
            this.DacTinhTiemThucModel = tiemThucModel;
            this.DacTinhTuDuyModel = tuDuyModel;

            if (isAdult)
            {
                CalculateValueAdult(excelKhuyenNghiCollection);
            }
            else
            {
                CalculateValueChild(excelKhuyenNghiCollection);
            }
        }

        private void CalculateValueChild(ExcelKhuyenNghiCollection collection)
        {
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();
            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);

            List<string> thumbFingerType = new List<string>();
            thumbFingerType.Add(PICollection.L1.FingerType);
            thumbFingerType.Add(PICollection.R1.FingerType);

            /// DacTinhTuDuy = Tự Duy Tự mẫu thuẫn
            /// self_contradiction
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_contradiction");
                EnumValue = KhuyenNghiEnum.self_contradiction;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Cá Nhân
            /// ego_centric
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.ego_centric
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("ego_centric");
                EnumValue = KhuyenNghiEnum.ego_centric;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Phản Biện & 2RL
            /// converse_think_2
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("converse_think_2");
                EnumValue = KhuyenNghiEnum.converse_think_2;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Phản Biện
            /// converse_think_1
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("converse_think_1");
                EnumValue = KhuyenNghiEnum.converse_think_1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTuDuy = Tư Duy Chuẫn Mực Xã Hội
            /// social_norm
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_norm
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("social_norm");
                EnumValue = KhuyenNghiEnum.social_norm;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Cộng đồng Xã Hội
            /// social_service
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_service
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("social_service");
                EnumValue = KhuyenNghiEnum.social_service;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTiemThuc = Hấp thụ như miếng bọt biển
            /// open_absorb1
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.sponge_like_absorption
                || DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.open_absorb_1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb1");
                EnumValue = KhuyenNghiEnum.open_absorb1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTiemThuc = Hấp thụ chọn lựa
            /// open_absorb2
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_absorption
                || DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.open_absorb_2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb2");
                EnumValue = KhuyenNghiEnum.open_absorb2;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTiemThuc = Hấp thụ theo cảm xúc
            /// open_absorb4
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.emotional_absorption
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb4");
                EnumValue = KhuyenNghiEnum.open_absorb4;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTiemThuc = Hấp thụ thụ động && OcNangDong > OcPhanTich
            /// open_absorb5
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb5");
                EnumValue = KhuyenNghiEnum.open_absorb5;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTiemThuc = Hấp thụ thụ động && OcNangDong < OcPhanTich
            /// open_absorb6
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                && this.IntelligenceModel.OcNangDong < this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb5");
                EnumValue = KhuyenNghiEnum.open_absorb5;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTiemThuc = Hấp thụ thụ động
            /// open_absorb3
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb3");
                EnumValue = KhuyenNghiEnum.open_absorb3;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// 3UL+ 1 WE
            /// self_conscious7
            if (
                fingerTypes.Count(p=> p.StartsWith("UL") )>=3
                && fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious7");
                EnumValue = KhuyenNghiEnum.self_conscious7;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// NGÓN CÁI WE
            /// self_conscious3
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 || this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious3");
                EnumValue = KhuyenNghiEnum.self_conscious3;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// WE >= 5
            /// self_conscious4
            if (
                fingerTypes.Count(p => p.StartsWith("WE")) >= 5
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious4");
                EnumValue = KhuyenNghiEnum.self_conscious4;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// Nếu 1 trong 2 ngón cái có WE 
            /// self_conscious1
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 || this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious1");
                EnumValue = KhuyenNghiEnum.self_conscious1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// Nếu 2 ngón cái đều là WC
            /// self_conscious5
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC")
                 && this.PICollection.R1.FingerType.StartsWith("WC"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious5");
                EnumValue = KhuyenNghiEnum.self_conscious5;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Nếu 1 trong 2 ngón cái có WT/WS. Và nếu ngón cái có WE thì ưu tiên cho WT/WS hơn.
            /// self_conscious6
            if (
                (this.PICollection.L1.FingerType.StartsWith("WT")
                 ||this.PICollection.L1.FingerType.StartsWith("WS"))
                 &&
                (this.PICollection.R1.FingerType.StartsWith("WT")
                 || this.PICollection.R1.FingerType.StartsWith("WS"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious6");
                EnumValue = KhuyenNghiEnum.self_conscious6;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// WP – WL
            /// self_conscious8
            if (
                fingerTypes.Count(p => p.StartsWith("WP")) >= 1
                && fingerTypes.Count(p => p.StartsWith("WL")) >= 1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious8");
                EnumValue = KhuyenNghiEnum.self_conscious8;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Nếu 1 trong 2 ngón cái có WC 
            /// self_conscious2
            if (
                this.PICollection.L1.FingerType.StartsWith("WC")
                 || this.PICollection.R1.FingerType.StartsWith("WC")
                 )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious2");
                EnumValue = KhuyenNghiEnum.self_conscious2;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// WD – WC hoặc WC – WI  hoặc WI hoặc WD trên ngón cái
            /// self_conscious9
            if (
                (thumbFingerType.Count(p => p.StartsWith("WD")) >= 1
                 && thumbFingerType.Count(p => p.StartsWith("WC")) >= 1)
                ||
                (thumbFingerType.Count(p => p.StartsWith("WC")) >= 1
                 && thumbFingerType.Count(p => p.StartsWith("WI")) >= 1)
                ||
                (thumbFingerType.Count(p => p.StartsWith("WI")) >= 1
                 && thumbFingerType.Count(p => p.StartsWith("WD")) >= 1)
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious9");
                EnumValue = KhuyenNghiEnum.self_conscious9;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }
            /// DacTinhTuDuy = Tự Duy Tự Ý Thức
            /// self_conscious
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious1");
                EnumValue = KhuyenNghiEnum.self_conscious1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }
        }
        
        private void CalculateValueAdult(ExcelKhuyenNghiCollection collection)
        {
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();
            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);


            /// DacTinhTuDuy = Tự Duy Tự mẫu thuẫn
            /// self_contradiction
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_contradiction");
                EnumValue = KhuyenNghiEnum.self_contradiction;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Cá Nhân
            /// ego_centric
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.ego_centric
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("ego_centric");
                EnumValue = KhuyenNghiEnum.ego_centric;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Phản Biện & 2RL
            /// converse_think_2
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("converse_think_2");
                EnumValue = KhuyenNghiEnum.converse_think_2;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Phản Biện
            /// converse_think_1
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("converse_think_1");
                EnumValue = KhuyenNghiEnum.converse_think_1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTuDuy = Tư Duy Chuẫn Mực Xã Hội
            /// social_norm
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_norm
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("social_norm");
                EnumValue = KhuyenNghiEnum.social_norm;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tư Duy Cộng đồng Xã Hội
            /// social_service
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_service
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("social_service");
                EnumValue = KhuyenNghiEnum.social_service;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }
            
            /// DacTinhTiemThuc = Hấp thụ như miếng bọt biển
            /// open_absorb1
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.sponge_like_absorption
                || DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.open_absorb_1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb1");
                EnumValue = KhuyenNghiEnum.open_absorb1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTiemThuc = Hấp thụ chọn lựa
            /// open_absorb2
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_absorption
                || DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.open_absorb_2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb2");
                EnumValue = KhuyenNghiEnum.open_absorb2;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTiemThuc = Hấp thụ theo cảm xúc
            /// open_absorb4
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.emotional_absorption
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb4");
                EnumValue = KhuyenNghiEnum.open_absorb4;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// DacTinhTiemThuc = Hấp thụ thụ động && OcNangDong > OcPhanTich
            /// open_absorb5
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                && this.IntelligenceModel.OcNangDong > this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb5");
                EnumValue = KhuyenNghiEnum.open_absorb5;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTiemThuc = Hấp thụ thụ động && OcNangDong < OcPhanTich
            /// open_absorb6
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                && this.IntelligenceModel.OcNangDong < this.IntelligenceModel.OcPhanTich
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb5");
                EnumValue = KhuyenNghiEnum.open_absorb5;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTiemThuc = Hấp thụ thụ động
            /// open_absorb3
            if (
                DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("open_absorb3");
                EnumValue = KhuyenNghiEnum.open_absorb3;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// WE && WC
            /// self_conscious7
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                && this.PICollection.R1.FingerType.StartsWith("WC"))
                ||
                (this.PICollection.L1.FingerType.StartsWith("WC")
                && this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious7");
                EnumValue = KhuyenNghiEnum.self_conscious7;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// NGÓN CÁI WE
            /// self_conscious3
            if (
                (this.PICollection.L1.FingerType.StartsWith("WE")
                 || this.PICollection.R1.FingerType.StartsWith("WE"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious3");
                EnumValue = KhuyenNghiEnum.self_conscious3;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// NGÓN CÁI WC
            /// self_conscious4
            if (
                (this.PICollection.L1.FingerType.StartsWith("WC")
                 || this.PICollection.R1.FingerType.StartsWith("WC"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious4");
                EnumValue = KhuyenNghiEnum.self_conscious4;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// WT || WS
            /// self_conscious1
            if (
                fingerTypes.Count(p => p.StartsWith("WT")) >= 1
                || fingerTypes.Count(p => p.StartsWith("WS")) >= 1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious1");
                EnumValue = KhuyenNghiEnum.self_conscious1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// WD
            /// self_conscious5
            if (
                (this.PICollection.L1.FingerType.StartsWith("WD")
                 || this.PICollection.R1.FingerType.StartsWith("WD"))
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious5");
                EnumValue = KhuyenNghiEnum.self_conscious5;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// WE
            /// self_conscious6
            if (
                fingerTypes.Count(p => p.StartsWith("WE")) >= 1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious6");
                EnumValue = KhuyenNghiEnum.self_conscious6;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// WI
            /// self_conscious8
            if (
                fingerTypes.Count(p => p.StartsWith("WI")) >= 1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious8");
                EnumValue = KhuyenNghiEnum.self_conscious8;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// "10 UL, THÙY TRƯỚC TRÁN > 23% || 1 UL ngón cái + 1 Whorl ngón cái" 
            /// self_conscious2
            if (
                (fingerTypes.Count(p => p.StartsWith("UL")) >= 10
                 && this.IntelligenceModel.ThuyTruocTran > 23)
                || (
                    (this.PICollection.L1.FingerType.StartsWith("UL")
                     || this.PICollection.R1.FingerType.StartsWith("UL"))
                    && (this.PICollection.L1.FingerType.StartsWith("W")
                        || this.PICollection.R1.FingerType.StartsWith("W"))
                    )
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious2");
                EnumValue = KhuyenNghiEnum.self_conscious2;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            
            /// WP || WL
            /// self_conscious9
            if (
                fingerTypes.Count(p => p.StartsWith("WP")) >= 1
                || fingerTypes.Count(p => p.StartsWith("WL")) >= 1
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious9");
                EnumValue = KhuyenNghiEnum.self_conscious9;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// DacTinhTuDuy = Tự Duy Tự Ý Thức
            /// self_conscious
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_2
                )
            {
                ExcelKhuyenNghi tmp = collection.Find("self_conscious1");
                EnumValue = KhuyenNghiEnum.self_conscious1;
                Code = tmp.Code;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }
        }

        //private void CalculateValue(ExcelKhuyenNghiCollection collection)
        //{
        //    List<string> fingerTypes = PICollection.PIModelList.Select(p => p.FingerType).ToList();

        //    /// open_absorb1
        //    /// DacTinhTiemThuc = Hấp thụ như miếng bọt biển
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.sponge_like_absorption
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("open_absorb1");
        //        EnumValue = KhuyenNghiEnum.open_absorb1;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }



        //    /// open_absorb2
        //    /// DacTinhTiemThuc = Hấp thụ chọn lựa
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_absorption
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("open_absorb2");
        //        EnumValue = KhuyenNghiEnum.open_absorb2;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }



        //    /// open_absorb3
        //    /// DacTinhTiemThuc = Hấp thụ thụ động
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("open_absorb3");
        //        EnumValue = KhuyenNghiEnum.open_absorb3;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// open_absorb4
        //    /// DacTinhTiemThuc = Hấp thụ theo cảm xúc
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.emotional_absorption
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("open_absorb4");
        //        EnumValue = KhuyenNghiEnum.open_absorb4;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// open_absorb5
        //    /// DacTinhTiemThuc = Hấp thụ thụ động && OcNangDong > OcPhanTich
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
        //        && IntelligenceModel.OcNangDong > IntelligenceModel.OcPhanTich
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("open_absorb5");
        //        EnumValue = KhuyenNghiEnum.open_absorb5;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// open_absorb6
        //    /// DacTinhTiemThuc = Hấp thụ thụ động && OcNangDong < OcPhanTich
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
        //        && IntelligenceModel.OcNangDong < IntelligenceModel.OcPhanTich
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("open_absorb6");
        //        EnumValue = KhuyenNghiEnum.open_absorb6;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious1
        //    /// WT || WS
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("WT")) >= 1
        //        || fingerTypes.Count(p => p.StartsWith("WS")) >= 1
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious1");
        //        EnumValue = KhuyenNghiEnum.self_conscious1;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious2
        //    /// 10 UL, THÙY TRƯỚC TRÁN > 23%)
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("UL")) >= 10
        //        && IntelligenceModel.ThuyTruocTran > 23
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious2");
        //        EnumValue = KhuyenNghiEnum.self_conscious2;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious3
        //    /// NGÓN CÁI WE
        //    if (
        //        PICollection.L1.FingerType.StartsWith("WE")
        //        || PICollection.R1.FingerType.StartsWith("WE")
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious3");
        //        EnumValue = KhuyenNghiEnum.self_conscious3;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious4
        //    /// NGÓN CÁI WC
        //    if (
        //        PICollection.L1.FingerType.StartsWith("WC")
        //        || PICollection.R1.FingerType.StartsWith("WC")
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious4");
        //        EnumValue = KhuyenNghiEnum.self_conscious4;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious5
        //    /// WD
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("WD")) >= 1
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious5");
        //        EnumValue = KhuyenNghiEnum.self_conscious5;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious6
        //    /// WE
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("WE")) >= 1
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious6");
        //        EnumValue = KhuyenNghiEnum.self_conscious6;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious7
        //    /// WE && WC
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("WE")) >= 1
        //        && fingerTypes.Count(p => p.StartsWith("WC")) >= 1
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious7");
        //        EnumValue = KhuyenNghiEnum.self_conscious7;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious8
        //    /// WI
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("WI")) >= 1
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious8");
        //        EnumValue = KhuyenNghiEnum.self_conscious8;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious9
        //    /// WP || WL
        //    if (
        //        fingerTypes.Count(p => p.StartsWith("WP")) >= 1
        //        || fingerTypes.Count(p => p.StartsWith("WL")) >= 1
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_conscious9");
        //        EnumValue = KhuyenNghiEnum.self_conscious9;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_contradiction
        //    /// DacTinhTuDuy = Tự Duy Tự mẫu thuẫn
        //    if (
        //        DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_1
        //        || DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_contradiction_2
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("self_contradiction");
        //        EnumValue = KhuyenNghiEnum.self_contradiction;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// ego_centric
        //    /// DacTinhTuDuy=Tư Duy Cá Nhân
        //    if (
        //        DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.ego_centric
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("ego_centric");
        //        EnumValue = KhuyenNghiEnum.ego_centric;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// converse_think_2
        //    /// DacTinhTuDuy=Tư Duy Phản Biện
        //    if (
        //        DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("converse_think_2");
        //        EnumValue = KhuyenNghiEnum.converse_think_2;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// converse_think_2
        //    /// DacTinhTuDuy = Tư Duy Phản Biện & 2RL
        //    if (
        //        DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
        //        && fingerTypes.Count(p => p.StartsWith("RL")) == 2
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("converse_think_2");
        //        EnumValue = KhuyenNghiEnum.converse_think_2;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// social_norm
        //    /// DacTinhTuDuy = Tư Duy Chuẫn Mực Xã Hội
        //    if (
        //        DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_norm
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("social_norm");
        //        EnumValue = KhuyenNghiEnum.social_norm;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// social_service
        //    /// DacTinhTuDuy = Tư Duy Cộng đồng Xã Hội
        //    if (
        //        DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_service
        //        )
        //    {
        //        ExcelKhuyenNghi tmp = collection.Find("social_service");
        //        EnumValue = KhuyenNghiEnum.social_service;
        //        Code = tmp.Code;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }
        //}
    }
}