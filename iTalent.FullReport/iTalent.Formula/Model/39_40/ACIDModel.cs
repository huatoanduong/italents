﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model._39_40
{
    public class ACIDModel
    {
        public decimal A { get; set; }
        public decimal C { get; set; }
        public decimal I { get; set; }
        public decimal D { get; set; }
    }
}