﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Formula.Model. ExcelTemplate;
// ReSharper disable InconsistentNaming

namespace iTalent.Formula.Model._39_40
{
    public enum Top3In16Type
    {
        INTERPERSONAL,
        PLANNING,
        INTERPERSONAL_PLANNING,
        INTRAPERSONAL,
        CREATION_IMAGINATION,
        LOGICAL_REASONING,
        SPATIAL_MENTAL_IMAGE,
        LANGUAGE_EXPRESSION,
        KINESTHETIC_INTELLIGENCE,
        OPERATION_INTELLIGENCE,
        ARTIST_INTELLIGENCE,
        LANGUAGE_INTERNALIZATION,
        MUSIC_APPRECIATION,
        OBSERVATION_INTELLIGENCE,
        IMAGE_APPRECIATION,
        CONCENTRATION_INTELLIGENCE,
        EQ_INTELLIGENCE,
        TUVUNG_NGONNGU,
    }

    public class Top3In16Model
    {
        public decimal VanDongValue { get; set; }
        public Top3In16Type Type { get; set; }
        public string Code { get; set; }
        public string TenNangLuc { get; set; }
        public bool IsAdult { get { return Owner.IsAdult; } }

        public Top3In16ModelCollection Owner { get; set; }

        public string Description { get { return IsAdult ? AdultDescription : ChildDescription; } }
        private string AdultDescription { get; set; }
        private string ChildDescription { get; set; }

        public Top3In16Model(Top3In16ModelCollection owner, string adultDescription, string childDescription)
        {
            this.Owner = owner;
            this.AdultDescription = adultDescription;
            this.ChildDescription = childDescription;
        }
    }

    public class Top3In16ModelCollection
    {
        public List<Top3In16Model> Collection { get; set; }
        public VanDongModelCollection VanDongModel { get; set; }
        public MQModelCollection MQModelCollection { get; set; }

        public PIModelCollection PICollection { get; set; }

        public Top3In16Model First { get { return Collection[0]; } }
        public Top3In16Model Second { get { return Collection[1]; } }
        public Top3In16Model Third { get { return Collection[2]; } }
        public Top3In16Model Fourth { get { return Collection[3]; } }

        public bool IsAdult { get; set; }

        public Top3In16ModelCollection(PIModelCollection piCollection, VanDongModelCollection vanDongModelCollection, MQModelCollection mqModelCollection,
            ExcelTop3In16Collection excelTop3In16Collection, bool isAdult)
        {
            this.PICollection = piCollection;
            this.VanDongModel = vanDongModelCollection;
            this.MQModelCollection = mqModelCollection;
            IsAdult = isAdult;
            MapVanDongModel(excelTop3In16Collection);
            OrderCollection();
            CalculateValue(excelTop3In16Collection);
        }

        private Top3In16Type MapType(VanDongType vanDongType)
        {
            switch (vanDongType)
            {
                case
                    VanDongType.Interpersonal:
                    return Top3In16Type.INTERPERSONAL;
                case VanDongType.Planning:
                    return Top3In16Type.PLANNING;
                case VanDongType.Intrapersonal:
                    return Top3In16Type.INTRAPERSONAL;
                case VanDongType.Creation:
                    return Top3In16Type.CREATION_IMAGINATION;
                case VanDongType.Logical:
                    return Top3In16Type.LOGICAL_REASONING;
                case VanDongType.Spartial:
                    return Top3In16Type.SPATIAL_MENTAL_IMAGE;
                case VanDongType.Language:
                    return Top3In16Type.LANGUAGE_EXPRESSION;
                case VanDongType.Kinesthetic:
                    return Top3In16Type.KINESTHETIC_INTELLIGENCE;
                case VanDongType.Operation:
                    return Top3In16Type.OPERATION_INTELLIGENCE;
                case VanDongType.Artistic:
                    return Top3In16Type.ARTIST_INTELLIGENCE;
                case VanDongType.LanguageInternalization:
                    return Top3In16Type.LANGUAGE_INTERNALIZATION;
                case VanDongType.Music:
                    return Top3In16Type.MUSIC_APPRECIATION;
                case VanDongType.Observation:
                    return Top3In16Type.OBSERVATION_INTELLIGENCE;
                case VanDongType.ImageAppreciation:
                    return Top3In16Type.IMAGE_APPRECIATION;
                case VanDongType.Concentration:
                    return Top3In16Type.CONCENTRATION_INTELLIGENCE;
                case VanDongType.EQIntelligent:
                    return Top3In16Type.EQ_INTELLIGENCE;
                default:
                    return Top3In16Type.INTERPERSONAL;
            }
        }

        private void MapVanDongModel(ExcelTop3In16Collection excelCollection)
        {
            Collection = new List<Top3In16Model>();
            foreach (VanDongModel vanDongModel in VanDongModel.Collection)
            {
                Top3In16Type type = MapType(vanDongModel.VanDongType);
                string code = type.ToString();
                ExcelTop3In16 excel = excelCollection.Find(code);
                Top3In16Model model = new Top3In16Model(this, excel.AdultDescription, excel.ChildDescription)
                {
                    Type = type,
                    Code = code,
                    TenNangLuc = excel.TenNangLuc,
                    VanDongValue = vanDongModel.Value,
                };
                this.Collection.Add(model);
            }
        }

        private int FindIndex(Top3In16Type type)
        {
            for (int i = 0; i < this.Collection.Count; i++)
            {
                if (this.Collection[i].Type == type)
                {
                    return i;
                }
            }
            return -1;
        }

        private void OrderCollection()
        {
            this.Collection = Collection.OrderByDescending(p => p.VanDongValue).ToList();
        }

        private void CalculateValue(ExcelTop3In16Collection excelCollection)
        {
            List<Top3In16Model> top4 = this.Collection.Take(4).ToList();

            if (top4.Any(p => p.Type == Top3In16Type.INTERPERSONAL)
                && top4.Any(p => p.Type == Top3In16Type.PLANNING))
            {
                Top3In16Type type = Top3In16Type.INTERPERSONAL_PLANNING;
                string code = type.ToString();
                ExcelTop3In16 excel = excelCollection.Find(code);
                Top3In16Model model = new Top3In16Model(this, excel.AdultDescription, excel.ChildDescription)
                {
                    Type = type,
                    Code = code,
                    TenNangLuc = excel.TenNangLuc,
                    VanDongValue = 0,
                };

                this.Collection.RemoveAt(FindIndex(Top3In16Type.INTERPERSONAL));
                this.Collection.RemoveAt(FindIndex(Top3In16Type.PLANNING));
                this.Collection.Insert(0, model);
                return;
            }

            if (top4.All(p => p.Type != Top3In16Type.LANGUAGE_EXPRESSION)
                && top4.All(p => p.Type != Top3In16Type.LANGUAGE_INTERNALIZATION)
                && this.MQModelCollection.Word.Value > 12
                && this.PICollection.R4.PIValue < this.PICollection.TFRC_Average)
            {
                Top3In16Type type = Top3In16Type.TUVUNG_NGONNGU;
                string code = type.ToString();
                ExcelTop3In16 excel = excelCollection.Find(code);
                Top3In16Model model = new Top3In16Model(this, excel.AdultDescription, excel.ChildDescription)
                {
                    Type = type,
                    Code = code,
                    TenNangLuc = excel.TenNangLuc,
                    VanDongValue = 0,
                };

                this.Collection.Insert(2, model);
                return;
            }
        }
    }
}