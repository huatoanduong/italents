﻿using System.Collections.Generic;
using System.Linq;

namespace iTalent.Formula.Model._39_40
{
    public static class TenHanhTrinhCuocSong
    {
        public const string ThongThai_BinhAn = @"THÔNG THÁI & BÌNH AN";
        public const string CongViec_KetQua = @"CÔNG VIỆC - KẾT QUẢ";
        public const string TinhYeu_XaHoi = @"TÌNH YÊU & XÃ HỘI";
    }

    public class HanhTrinhCuocSongModel
    {
        public IntelligenceModelCollection IntelligenceModel { get; set; }
        public PIModelCollection PICollection { get; set; }
        public DacTinhTuDuyModel DacTinhTuDuyModel { get; set; }

        public string[] Value { get; set; }
        public int[] Rank { get; set; }

        public int Rank1 { get { return Rank[0]; } }
        public string Value1 { get { return Value[0]; } }

        public int Rank2 { get { return Rank[1]; } }
        public string Value2 { get { return Value[1]; } }

        public int Rank3 { get { return Rank[2]; } }
        public string Value3 { get { return Value[2]; } }

        private int WhorlCount { get; set; }
        private int ArchCount { get; set; }
        private int LoopCount { get; set; }

        public int HangCongViecKetQua { get; internal set; }
        public int HangTinhYeuVaXaHoi { get; internal set; }
        public int HangThongThaiVaBinhAn { get; internal set; }

        public HanhTrinhCuocSongModel(PIModelCollection piCollection, IntelligenceModelCollection intelligenceModel, DacTinhTuDuyModel dacTinhTuDuyModel)
        {
            this.PICollection = piCollection;
            this.IntelligenceModel = intelligenceModel;
            this.DacTinhTuDuyModel = dacTinhTuDuyModel;

            Rank = new int[3];
            Value = new string[3];

            CountFingerType();

            //CalculateValue();

            Ranking();
        }

        private void CountFingerType()
        {
        }

        private void CalculateValue()
        {
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();

            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);

            List<string> thumbFingerType = new List<string>();
            thumbFingerType.Add(PICollection.L1.FingerType);
            thumbFingerType.Add(PICollection.R1.FingerType);

            /// Tư Duy Tự Ý Thức (Trường hợp 10UL và thùy trước trán > 23%) 

            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_2
                )
            {
                Rank[0] = 1;
                Rank[1] = 1;
                Rank[2] = 2;

                Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                return;
            }

            /// Whorl o ngon cai va UL ngon cai 

            if (
                (PICollection.L1.FingerType.StartsWith("W") || PICollection.R1.FingerType.StartsWith("W"))
                &&
                (PICollection.L1.FingerType.StartsWith("U") || PICollection.R1.FingerType.StartsWith("U")))
            {

                if (fingerTypes.Count(p => p.StartsWith("A")) >= 1)
                {
                    Rank[0] = 1;
                    Rank[1] = 1;
                    Rank[2] = 1;
                }
                else
                {
                    Rank[0] = 1;
                    Rank[1] = 1;
                    Rank[2] = 2;
                }
                Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                return;
            }

            if (
                (PICollection.L1.FingerType.StartsWith("W") || PICollection.R1.FingerType.StartsWith("W"))
                &&
                (!PICollection.L1.FingerType.StartsWith("U") || !PICollection.R1.FingerType.StartsWith("U")))
            {

                if (fingerTypes.Count(p => p.StartsWith("A")) >= 1)
                {
                    Rank[0] = 1;
                    Rank[1] = 2;
                    Rank[2] = 1;

                    Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;
                    Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                    Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                }
                return;
            }

            if (
                ((PICollection.L1.FingerType.StartsWith("A") || PICollection.R1.FingerType.StartsWith("A")) || fingerTypes.Count(x => x.StartsWith("A")) >= 1)
                && ((PICollection.L1.FingerType.StartsWith("U") || PICollection.R1.FingerType.StartsWith("U")))
               )
            {

                Rank[0] = 1;
                Rank[1] = 1;
                Rank[2] = 2;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;
                return;
            }

            if (
                ((PICollection.L1.FingerType.StartsWith("A") || PICollection.R1.FingerType.StartsWith("A")) || fingerTypes.Count(x => x.StartsWith("A")) >= 1)
                && ((PICollection.L1.FingerType.StartsWith("W") || PICollection.R1.FingerType.StartsWith("W")))
               )
            {

                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 1;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;
                return;
            }

            if (
                (PICollection.L1.FingerType.StartsWith("W") || PICollection.R1.FingerType.StartsWith("W"))
                && ((PICollection.L1.FingerType.StartsWith("U") || PICollection.R1.FingerType.StartsWith("U")))
               )
            {

                Rank[0] = 2;
                Rank[1] = 1;
                Rank[2] = 1;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;
                return;
            }

            if (
                ((PICollection.L1.FingerType.StartsWith("A") || PICollection.R1.FingerType.StartsWith("A")) || fingerTypes.Count(x =>x.StartsWith("A")) >= 1))
            {


                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 3;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                if (LoopCount >= WhorlCount)
                {
                    Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                    Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;
                }
                else
                {
                    Value[1] = TenHanhTrinhCuocSong.CongViec_KetQua;
                    Value[2] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                }
                return;
            }


            /// Whorl có 2  Ngón Cái WP và WL

            if (
                fingerTypes.Count(p => p.StartsWith("W")) == 10
                && thumbFingerType.Count(p => p.StartsWith("WP")) >= 1
                && thumbFingerType.Count(p => p.StartsWith("WL")) >= 1
                )
            {
                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 3;

                Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                return;
            }


            /// 10 Whorl

            if (
                fingerTypes.Count(p => p.StartsWith("W")) == 10
                )
            {
                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 3;

                Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;

                if (LoopCount >= ArchCount)
                {
                    Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                    Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                }
                else
                {
                    Value[1] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                    Value[2] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                }

                return;
            }


            /// 10 Arch 

            if (
                fingerTypes.Count(p => p.StartsWith("A")) >= 10
                )
            {
                Rank[0] = 2;
                Rank[1] = 2;
                Rank[2] = 1;

                Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                return;
            }


            /// Tư Duy hấp thụ
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.open_absorb_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.open_absorb_2
                )
            {
                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 3;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                if (WhorlCount >= LoopCount)
                {
                    Value[1] = TenHanhTrinhCuocSong.CongViec_KetQua;
                    Value[2] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                }
                else
                {
                    Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                    Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;
                }

                return;
            }


            /// Tư Duy Chuẩn Mực Xã Hội, Tư Duy Cộng Đồng
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_norm
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.social_service
                )
            {
                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 3;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;

                if (WhorlCount >= ArchCount)
                {
                    Value[1] = TenHanhTrinhCuocSong.CongViec_KetQua;
                    Value[2] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                }
                else
                {
                    Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                    Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;
                }

                return;
            }


            /// Tư Duy Tự Ý Thức, Tư Duy Cá Nhân
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_1
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.self_conscious_2
                || this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.ego_centric
                )
            {
                Rank[0] = 1;
                Rank[1] = 2;
                Rank[2] = 3;

                Value[0] = TenHanhTrinhCuocSong.CongViec_KetQua;

                if (LoopCount >= ArchCount)
                {
                    Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                    Value[2] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                }
                else
                {
                    Value[1] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                    Value[2] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                }

                return;
            }


            /// Tư Duy phản biện
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
                )
            {
                Rank[0] = 1;
                Rank[1] = 1;
                Rank[2] = 2;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                Value[1] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;
                Value[2] = TenHanhTrinhCuocSong.CongViec_KetQua;


                return;
            }


            /// Tư Duy Tự Mâu Thuẫn
            if (
                this.DacTinhTuDuyModel.EnumValue == DacTinhTuDuyEnum.converse_think
                )
            {
                Rank[0] = 2;
                Rank[1] = 2;
                Rank[2] = 2;

                Value[0] = TenHanhTrinhCuocSong.ThongThai_BinhAn;
                Value[1] = TenHanhTrinhCuocSong.CongViec_KetQua;
                Value[2] = TenHanhTrinhCuocSong.TinhYeu_XaHoi;

                return;
            }

        }

        //    private void CalculateValue()
        //    {
        //        if (
        //            (PICollection.L1.FingerType == "UL"
        //            || PICollection.R1.FingerType == "UL")
        //            && IntelligenceModel.ThuyTruocTran>23
        //            )
        //        {
        //            Value = "WORK & RESULTS";
        //            return;
        //        }

        //        List<string> fingerTypes = PICollection.PIModelList.Select(p => p.FingerType).ToList();
        //        string compareTypes ;

        //        compareTypes = @"WT-WS-WE-WP-WL-WX";
        //        if (fingerTypes.Any(p => compareTypes.Contains(p)))
        //        {
        //            Value = @"WORK & RESULTS";
        //            return;
        //        }

        //        compareTypes = @"UL-LF";
        //        if (fingerTypes.Any(p => compareTypes.Contains(p)))
        //        {
        //            Value = @"LOVE & RELATIONS";
        //            return;
        //        }

        //        compareTypes = @"WC/WD/WI";
        //        if (fingerTypes.Any(p => compareTypes.Contains(p)))
        //        {
        //            Value = @"WORK & RESULTS  + LOVE  & RELATIONS";
        //            return;
        //        }

        //        compareTypes = @"WC/WD";
        //        if (fingerTypes.Any(p => p.StartsWith("AU"))
        //            && fingerTypes.Any(p => compareTypes.Contains(p))
        //            )
        //        {
        //            Value = @"WISDOM";
        //            return;
        //        }

        //        if (fingerTypes.Count(p => p.StartsWith("AS")) == 10
        //            || fingerTypes.Count(p => p.StartsWith("AT")) == 10
        //            )
        //        {
        //            Value = @"WORK & RESULTS + WISDOM";
        //            return;
        //        }
        //    }

        private void Ranking()
        {
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();

            string L1 = PICollection.L1.FingerType;
            string R1 = PICollection.R1.FingerType;

            List<string> last8FingerTypes = new List<string>();
            last8FingerTypes.Add(PICollection.L2.FingerType);
            last8FingerTypes.Add(PICollection.L3.FingerType);
            last8FingerTypes.Add(PICollection.L4.FingerType);
            last8FingerTypes.Add(PICollection.L5.FingerType);
            last8FingerTypes.Add(PICollection.R2.FingerType);
            last8FingerTypes.Add(PICollection.R3.FingerType);
            last8FingerTypes.Add(PICollection.R4.FingerType);
            last8FingerTypes.Add(PICollection.R5.FingerType);

            ///1. Chua Whorl Va !WD, WC, WI, WP 
            ///Truong hop L1 la Whorl
            ///Check R1 la Arch hay UL
            #region Unused
            //if (L1.StartsWith("W") && !L1.StartsWith("WD")
            //    && !L1.StartsWith("WC") && !L1.StartsWith("WI") && !L1.StartsWith("WP"))
            //{
            //    HangCongViecKetQua = 1;
            //    if (R1.StartsWith("UL"))
            //    {
            //        HangTinhYeuVaXaHoi = 1;
            //        if (fingerTypes.Count(p => p.StartsWith("A")) >= 1 || fingerTypes.Count(p => p.StartsWith("RL")) >= 1 || fingerTypes.Count(p => p.StartsWith("WP")) >= 1)
            //            HangThongThaiVaBinhAn = 1;
            //        else
            //            HangThongThaiVaBinhAn = 2;
            //    }
            //    else if (R1.StartsWith("RL") || R1.StartsWith("A") || R1.StartsWith("WP"))
            //    {
            //        HangThongThaiVaBinhAn = 1;
            //        HangTinhYeuVaXaHoi = 2;
            //    }
            //    else // Hai ngon cai deu la W
            //    {
            //        //Check 8 ngon con lai co Arch, RL, WP ???
            //        if (fingerTypes.Count(p => p.StartsWith("A")) >= 1 || fingerTypes.Count(p => p.StartsWith("RL")) >= 1 || fingerTypes.Count(p => p.StartsWith("WP")) >= 1)
            //        {
            //            HangThongThaiVaBinhAn = 1;
            //            HangTinhYeuVaXaHoi = 2;
            //        }//Khong co dac biet
            //        else
            //        {
            //            HangTinhYeuVaXaHoi = 2;
            //            HangThongThaiVaBinhAn = 3;
            //        }

            //    }
            //}
            #endregion

            ///Chua Arch o bat ky cho nao
            if ((fingerTypes.Count(p => p.StartsWith("A")) >= 1 || fingerTypes.Count(p => p.StartsWith("RL")) >= 1 || fingerTypes.Count(p => p.StartsWith("WP")) >= 1))
            {
                HangThongThaiVaBinhAn = 1;
                ///Truong hop W -UL.(UL - W)
                if (
                    (L1.StartsWith("W") && R1.StartsWith("UL"))
                    || (R1.StartsWith("W") && L1.StartsWith("UL"))
                   )
                {
                    HangCongViecKetQua = 1;
                    HangTinhYeuVaXaHoi = 1;
                    return;
                }
                ///Truong Hop A - UL (UL-A)
                else if
                    (
                    (L1.StartsWith("A") && R1.StartsWith("UL"))
                    || (R1.StartsWith("A") && L1.StartsWith("UL"))
                    )
                {
                    HangTinhYeuVaXaHoi = 1;
                    HangCongViecKetQua = 2;
                    return;
                }
                ///Truong Hop W - Arch (Arch - W)
                else if
                    (
                    (L1.StartsWith("W") && R1.StartsWith("A"))
                    || (R1.StartsWith("W") && L1.StartsWith("A"))
                    )
                {
                    if (
                    (!L1.StartsWith("WD") && !L1.StartsWith("WC") && !L1.StartsWith("WI") && !L1.StartsWith("WP"))
                    || (!R1.StartsWith("WD") && !R1.StartsWith("WC") && !R1.StartsWith("WI") && !R1.StartsWith("WP"))
                    )
                    {
                        HangCongViecKetQua = 1;
                        HangTinhYeuVaXaHoi = 2;
                    }
                    else
                    {
                        HangCongViecKetQua = 1;
                        HangTinhYeuVaXaHoi = 1;
                    }
                    return;
                }
                ///Truong Hop W - W
                else if
                    (
                    (L1.StartsWith("W") && R1.StartsWith("W"))
                    || (R1.StartsWith("W") && L1.StartsWith("W"))
                    )
                {
                    if (
                    (!L1.StartsWith("WD") && !L1.StartsWith("WC") && !L1.StartsWith("WI") && !L1.StartsWith("WP"))
                    || (!R1.StartsWith("WD") && !R1.StartsWith("WC") && !R1.StartsWith("WI") && !R1.StartsWith("WP"))
                   )
                    {
                        HangCongViecKetQua = 1;
                        HangTinhYeuVaXaHoi = 2;
                    }
                    else
                    {
                        HangCongViecKetQua = 1;
                        HangTinhYeuVaXaHoi = 1;
                    }
                    return;
                }
                ///Truong Hop UL - UL
                else if
                    (
                    (L1.StartsWith("UL") && R1.StartsWith("UL"))
                    || (R1.StartsWith("UL") && L1.StartsWith("UL"))
                    )
                {

                    HangTinhYeuVaXaHoi = 1;
                    HangCongViecKetQua = 2;
                    return;
                }
                ///Truong Hop Arch - Arch
                else if
                    (
                    (L1.StartsWith("A") && R1.StartsWith("A"))
                    || (R1.StartsWith("A") && L1.StartsWith("A"))
                    )
                {
                    //Tinh So Whorl va UL so sanh.
                    WhorlCount = last8FingerTypes.Count(p => p.StartsWith("W"));
                    LoopCount = last8FingerTypes.Count(p => p.StartsWith("U"));
                    if(WhorlCount > LoopCount)
                    {
                        HangCongViecKetQua = 2;
                        HangTinhYeuVaXaHoi = 3;
                    }
                    else if(WhorlCount < LoopCount)
                    {
                        HangCongViecKetQua = 3;
                        HangTinhYeuVaXaHoi = 2;
                    }
                    else
                    {
                        HangCongViecKetQua = 2;
                        HangTinhYeuVaXaHoi = 2;
                    }
                    return;
                }
            }
            ///Truong Hop khong chua Arch o bat ky diem nao.
            else
            {
                ///TRuong Hop W-UL (UL -Whorl)
                if (
                    (L1.StartsWith("W") && R1.StartsWith("UL"))
                    || (R1.StartsWith("W") && L1.StartsWith("UL"))
                   )
                {
                    HangCongViecKetQua = 1;
                    HangTinhYeuVaXaHoi = 1;
                    HangThongThaiVaBinhAn = 2;
                    return;
                }
                ///Truong Hop W - W
                else if
                    (
                    (L1.StartsWith("W") && R1.StartsWith("W"))
                    || (R1.StartsWith("W") && L1.StartsWith("W"))
                    )
                {
                    if (
                    (!L1.StartsWith("WD") && !L1.StartsWith("WC") && !L1.StartsWith("WI") && !L1.StartsWith("WP"))
                    || (!R1.StartsWith("WD") && !R1.StartsWith("WC") && !R1.StartsWith("WI") && !R1.StartsWith("WP"))
                   )
                    {
                        HangCongViecKetQua = 1;
                        HangTinhYeuVaXaHoi = 2;
                        HangThongThaiVaBinhAn = 3;
                    }
                    else
                    {
                        HangCongViecKetQua = 1;
                        HangTinhYeuVaXaHoi = 1;
                        HangThongThaiVaBinhAn = 2;
                    }
                    return;
                }
                ///Truong Hop UL - UL
                else if
                    (
                    (L1.StartsWith("UL") && R1.StartsWith("UL"))
                    || (R1.StartsWith("UL") && L1.StartsWith("UL"))
                    )
                {
                    WhorlCount = last8FingerTypes.Count(p => p.StartsWith("W"));
                    ArchCount = last8FingerTypes.Count(p => p.StartsWith("A"));
                    if (WhorlCount > ArchCount)
                    {
                        HangTinhYeuVaXaHoi = 1;
                        HangCongViecKetQua = 2;
                        HangThongThaiVaBinhAn = 3;
                        return;
                    }
                    else if (WhorlCount < ArchCount)
                    {
                        HangTinhYeuVaXaHoi = 1;
                        HangCongViecKetQua = 3;
                        HangThongThaiVaBinhAn = 2;
                        return;
                    }
                    else
                    {
                        HangCongViecKetQua = 2;
                        HangTinhYeuVaXaHoi = 1;
                        HangThongThaiVaBinhAn = 3;
                        return;
                    }
                }
            }
        }

    }
}