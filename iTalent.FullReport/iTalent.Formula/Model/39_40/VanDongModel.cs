﻿// ReSharper disable InconsistentNaming

using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace iTalent.Formula.Model._39_40
{
    public enum VanDongType
    {
        Interpersonal,
        Planning,
        Intrapersonal,
        Creation,
        Logical,
        Spartial,
        Language,
        Kinesthetic,
        Operation,
        Artistic,
        LanguageInternalization,
        Music,
        Observation,
        ImageAppreciation,
        Concentration,
        EQIntelligent,
    }


    public class VanDongModel
    {
        public VanDongType VanDongType { get; set; }
        public decimal Value { get; set; }
    }

    public class VanDongModelCollection
    {
        public List<VanDongModel> Collection { get; set; }

        #region Properties

        public decimal Interpersonal
        {
            get { return Find(VanDongType.Interpersonal).Value; }
            set { Find(VanDongType.Interpersonal).Value = value; }
        }

        public decimal Planning
        {
            get { return Find(VanDongType.Planning).Value; }
            set { Find(VanDongType.Planning).Value = value; }
        }

        public decimal Intrapersonal
        {
            get { return Find(VanDongType.Intrapersonal).Value; }
            set { Find(VanDongType.Intrapersonal).Value = value; }
        }

        public decimal Creation
        {
            get { return Find(VanDongType.Creation).Value; }
            set { Find(VanDongType.Creation).Value = value; }
        }

        public decimal Logical
        {
            get { return Find(VanDongType.Logical).Value; }
            set { Find(VanDongType.Logical).Value = value; }
        }

        public decimal Spartial
        {
            get { return Find(VanDongType.Spartial).Value; }
            set { Find(VanDongType.Spartial).Value = value; }
        }

        public decimal Language
        {
            get { return Find(VanDongType.Language).Value; }
            set { Find(VanDongType.Language).Value = value; }
        }

        public decimal Kinesthetic
        {
            get { return Find(VanDongType.Kinesthetic).Value; }
            set { Find(VanDongType.Kinesthetic).Value = value; }
        }

        public decimal Operation
        {
            get { return Find(VanDongType.Operation).Value; }
            set { Find(VanDongType.Operation).Value = value; }
        }

        public decimal Artistic
        {
            get { return Find(VanDongType.Artistic).Value; }
            set { Find(VanDongType.Artistic).Value = value; }
        }

        public decimal LanguageInternalization
        {
            get { return Find(VanDongType.LanguageInternalization).Value; }
            set { Find(VanDongType.LanguageInternalization).Value = value; }
        }

        public decimal Music
        {
            get { return Find(VanDongType.Music).Value; }
            set { Find(VanDongType.Music).Value = value; }
        }

        public decimal Observation
        {
            get { return Find(VanDongType.Observation).Value; }
            set { Find(VanDongType.Observation).Value = value; }
        }

        public decimal ImageAppreciation
        {
            get { return Find(VanDongType.ImageAppreciation).Value; }
            set { Find(VanDongType.ImageAppreciation).Value = value; }
        }

        public decimal Concentration
        {
            get { return Find(VanDongType.Concentration).Value; }
            set { Find(VanDongType.Concentration).Value = value; }
        }

        public decimal EQIntelligent
        {
            get { return Find(VanDongType.EQIntelligent).Value; }
            set { Find(VanDongType.EQIntelligent).Value = value; }
        }

        #endregion

        public PIModelCollection PIModelCollection { get; set; }
        public MQModelCollection MQModelCollection { get; set; }

        public VanDongModelCollection(PIModelCollection piCollection, MQModelCollection mqCollection)
        {
            this.PIModelCollection = piCollection;
            this.MQModelCollection = mqCollection;
            this.Collection = new List<VanDongModel>();
            Calculate();
        }

        private VanDongModel Find(VanDongType type)
        {
            VanDongModel model = this.Collection.FirstOrDefault(p => p.VanDongType == type);
            if (model == null)
            {
                model = new VanDongModel()
                {
                    VanDongType = type,
                    Value = 0,
                };
                this.Collection.Add(model);
            }
            return model;
        }

        public void Calculate()
        {
            Calculate_Inter();
            Calculate_Planning();
            Calculate_Intrapersonal();
            Calculate_Creation();
            Calculate_Logical();
            Calculate_Spartial();
            Calculate_Language();
            Calculate_Kinesthetic();
            Calculate_Operation();
            Calculate_Artistic();
            Calculate_LanguageInternalization();
            Calculate_Music();
            Calculate_Observation();
            Calculate_ImageApprec();
            Calculate_Concentration();
            Calculate_EQIntelligent();
        }

        private decimal getValueByMQ(decimal value)
        {
            //if (PIModelCollection.TFRC > 28) { return 95; }
            //if (PIModelCollection.TFRC > 26) { return 90; }
            //if (PIModelCollection.TFRC > 24) { return 85; }
            //if (PIModelCollection.TFRC > 20) { return 80; }
            //if (PIModelCollection.TFRC > 18) { return 75; }
            //if (PIModelCollection.TFRC > 16) { return 70; }
            //if (PIModelCollection.TFRC > 14) { return 65; }
            //if (PIModelCollection.TFRC > 12) { return 60; }
            //if (PIModelCollection.TFRC > 10) { return 55; }
            //if (PIModelCollection.TFRC > 8) { return 50; }
            //return 45;

            if (value > 28) { return 95; }
            if (value > 27.9m) { return 94.75m; }
            if (value > 27.8m) { return 94.5m; }
            if (value > 27.7m) { return 94.25m; }
            if (value > 27.6m) { return 94; }
            if (value > 27.5m) { return 93.75m; }
            if (value > 27.4m) { return 93.5m; }
            if (value > 27.3m) { return 93.25m; }
            if (value > 27.2m) { return 93; }
            if (value > 27.1m) { return 92.75m; }
            if (value > 27) { return 92.5m; }
            if (value > 26.9m) { return 92.25m; }
            if (value > 26.8m) { return 92; }
            if (value > 26.7m) { return 91.75m; }
            if (value > 26.6m) { return 91.5m; }
            if (value > 26.5m) { return 91.25m; }
            if (value > 26.4m) { return 91; }
            if (value > 26) { return 90; }
            if (value > 25.9m) { return 89.75m; }
            if (value > 25.8m) { return 89.5m; }
            if (value > 25.7m) { return 89.25m; }
            if (value > 25.6m) { return 89; }
            if (value > 25.5m) { return 88.75m; }
            if (value > 25.4m) { return 88.5m; }
            if (value > 25.3m) { return 88.25m; }
            if (value > 25.2m) { return 88; }
            if (value > 25.1m) { return 87.75m; }
            if (value > 25) { return 87.75m; }
            if (value > 24.9m) { return 87.25m; }
            if (value > 24.8m) { return 87; }
            if (value > 24.7m) { return 86.75m; }
            if (value > 24.6m) { return 86.5m; }
            if (value > 24.5m) { return 86.25m; }
            if (value > 24.4m) { return 86; }
            if (value > 24.3m) { return 85.75m; }
            if (value > 24.2m) { return 85.5m; }
            if (value > 24.1m) { return 85.25m; }
            if (value > 24) { return 85; }
            if (value > 23.9m) { return 84.875m; }
            if (value > 23.8m) { return 84.75m; }
            if (value > 23.7m) { return 84.625m; }
            if (value > 23.6m) { return 84.5m; }
            if (value > 23.5m) { return 84.375m; }
            if (value > 23.4m) { return 84.25m; }
            if (value > 23.3m) { return 84.125m; }
            if (value > 23.2m) { return 84; }
            if (value > 23.1m) { return 83.875m; }
            if (value > 23) { return 83.75m; }
            if (value > 22.9m) { return 83.625m; }
            if (value > 22.8m) { return 83.5m; }
            if (value > 22.7m) { return 83.375m; }
            if (value > 22.6m) { return 83.25m; }
            if (value > 22.5m) { return 83.125m; }
            if (value > 22.4m) { return 83; }
            if (value > 22.3m) { return 82.875m; }
            if (value > 22.2m) { return 82.75m; }
            if (value > 22.1m) { return 82.625m; }
            if (value > 22) { return 82.5m; }
            if (value > 21.9m) { return 82.375m; }
            if (value > 21.8m) { return 82.25m; }
            if (value > 21.7m) { return 82.125m; }
            if (value > 21.6m) { return 82; }
            if (value > 21.5m) { return 81.875m; }
            if (value > 21.4m) { return 81.75m; }
            if (value > 21.3m) { return 81.625m; }
            if (value > 21.2m) { return 81.5m; }
            if (value > 21.1m) { return 81.375m; }
            if (value > 21) { return 81.25m; }
            if (value > 20.9m) { return 81.125m; }
            if (value > 20.8m) { return 81; }
            if (value > 20.7m) { return 80.875m; }
            if (value > 20.6m) { return 80.75m; }
            if (value > 20.5m) { return 80.625m; }
            if (value > 20.4m) { return 80.5m; }
            if (value > 20.3m) { return 80.375m; }
            if (value > 20.2m) { return 80.25m; }
            if (value > 20.1m) { return 80.125m; }
            if (value > 20) { return 80; }
            if (value > 19.9m) { return 79.75m; }
            if (value > 19.8m) { return 79.5m; }
            if (value > 19.7m) { return 79.25m; }
            if (value > 19.6m) { return 79; }
            if (value > 19.5m) { return 78.75m; }
            if (value > 19.4m) { return 78.5m; }
            if (value > 19.3m) { return 78.25m; }
            if (value > 19.2m) { return 78; }
            if (value > 19.1m) { return 77.75m; }
            if (value > 19) { return 77.5m; }
            if (value > 18.9m) { return 77.25m; }
            if (value > 18.8m) { return 77; }
            if (value > 18.7m) { return 76.75m; }
            if (value > 18.6m) { return 76.5m; }
            if (value > 18.5m) { return 76.25m; }
            if (value > 18.4m) { return 76; }
            if (value > 18.3m) { return 75.75m; }
            if (value > 18.2m) { return 75.5m; }
            if (value > 18.1m) { return 75.25m; }
            if (value > 18) { return 75; }
            if (value > 17.9m) { return 74.75m; }
            if (value > 17.8m) { return 74.5m; }
            if (value > 17.7m) { return 74.25m; }
            if (value > 17.6m) { return 74; }
            if (value > 17.5m) { return 73.75m; }
            if (value > 17.4m) { return 73.5m; }
            if (value > 17.3m) { return 73.25m; }
            if (value > 17.2m) { return 73; }
            if (value > 17.1m) { return 72.75m; }
            if (value > 17) { return 72.5m; }
            if (value > 16.9m) { return 72.25m; }
            if (value > 16.8m) { return 72; }
            if (value > 16.7m) { return 71.75m; }
            if (value > 16.6m) { return 71.5m; }
            if (value > 16.5m) { return 71.25m; }
            if (value > 16.4m) { return 71; }
            if (value > 16.3m) { return 70.75m; }
            if (value > 16.2m) { return 70.5m; }
            if (value > 16.1m) { return 70.25m; }
            if (value > 16) { return 70; }
            if (value > 15.9m) { return 69.75m; }
            if (value > 15.8m) { return 69.5m; }
            if (value > 15.7m) { return 69.25m; }
            if (value > 15.6m) { return 69; }
            if (value > 15.5m) { return 68.75m; }
            if (value > 15.4m) { return 68.5m; }
            if (value > 15.3m) { return 68.25m; }
            if (value > 15.2m) { return 68; }
            if (value > 15.1m) { return 67.75m; }
            if (value > 15) { return 67.5m; }
            if (value > 14.9m) { return 67.25m; }
            if (value > 14.8m) { return 67; }
            if (value > 14.7m) { return 66.75m; }
            if (value > 14.6m) { return 66.5m; }
            if (value > 14.5m) { return 66.25m; }
            if (value > 14.4m) { return 66; }
            if (value > 14.3m) { return 65.75m; }
            if (value > 14.2m) { return 65.5m; }
            if (value > 14.1m) { return 65.25m; }
            if (value > 14) { return 65; }
            if (value > 13.9m) { return 64.75m; }
            if (value > 13.8m) { return 64.5m; }
            if (value > 13.7m) { return 64.25m; }
            if (value > 13.6m) { return 64; }
            if (value > 13.5m) { return 63.75m; }
            if (value > 13.4m) { return 63.5m; }
            if (value > 13.3m) { return 63.25m; }
            if (value > 13.2m) { return 63; }
            if (value > 13.1m) { return 62.75m; }
            if (value > 13) { return 62.5m; }
            if (value > 12.9m) { return 62.25m; }
            if (value > 12.8m) { return 62; }
            if (value > 12.7m) { return 61.75m; }
            if (value > 12.6m) { return 61.5m; }
            if (value > 12.5m) { return 61.25m; }
            if (value > 12.4m) { return 61; }
            if (value > 12.3m) { return 60.75m; }
            if (value > 12.2m) { return 60.5m; }
            if (value > 12.1m) { return 60.25m; }
            if (value > 12) { return 60; }
            if (value > 11.9m) { return 59.75m; }
            if (value > 11.8m) { return 59.5m; }
            if (value > 11.7m) { return 59.25m; }
            if (value > 11.6m) { return 59; }
            if (value > 11.5m) { return 58.75m; }
            if (value > 11.4m) { return 58.5m; }
            if (value > 11.3m) { return 58.25m; }
            if (value > 11.2m) { return 58; }
            if (value > 11.1m) { return 57.75m; }
            if (value > 11) { return 57.5m; }
            if (value > 10.9m) { return 57.25m; }
            if (value > 10.8m) { return 57; }
            if (value > 10.7m) { return 56.75m; }
            if (value > 10.6m) { return 56.5m; }
            if (value > 10.5m) { return 56.25m; }
            if (value > 10.4m) { return 56; }
            if (value > 10.3m) { return 55.75m; }
            if (value > 10.2m) { return 55.5m; }
            if (value > 10.1m) { return 55.25m; }
            if (value > 10) { return 55; }
            if (value > 9.9m) { return 54.75m; }
            if (value > 9.8m) { return 54.5m; }
            if (value > 9.7m) { return 54.25m; }
            if (value > 9.6m) { return 54; }
            if (value > 9.5m) { return 53.75m; }
            if (value > 9.4m) { return 53.5m; }
            if (value > 9.3m) { return 53.25m; }
            if (value > 9.2m) { return 53; }
            if (value > 9.1m) { return 52.75m; }
            if (value > 9) { return 52.5m; }
            if (value > 8.9m) { return 52.25m; }
            if (value > 8.8m) { return 52; }
            if (value > 8.7m) { return 51.75m; }
            if (value > 8.6m) { return 51.5m; }
            if (value > 8.5m) { return 51.25m; }
            if (value > 8.4m) { return 51; }
            if (value > 8.3m) { return 50.75m; }
            if (value > 8.2m) { return 50.5m; }
            if (value > 8.1m) { return 50.25m; }
            if (value > 8) { return 50; }
            if (value > 7.9m) { return 45.75m;}
            if (value > 7.8m) { return 45.5m; }
            if (value > 7.7m) { return 45.25m; }
            if (value > 7.6m) { return 45; }
            if (value > 7.5m) { return 44.75m; }
            if (value > 7.4m) { return 44.5m; }
            if (value > 7.3m) { return 44.25m; }
            if (value > 7.2m) { return 44; }
            if (value > 7.1m) { return 43.75m; }
            if (value > 7) { return 43.5m; }
            if (value > 6.9m) { return 43.25m; }
            if (value > 6.8m) { return 43; }
            if (value > 6.7m) { return 42.75m; }
            if (value > 6.6m) { return 42.5m; }
            if (value > 6.5m) { return 42.25m; }
            if (value > 6.4m) { return 42; }
            if (value > 6.3m) { return 41.75m; }
            if (value > 6.2m) { return 41.5m; }
            if (value > 6.1m) { return 41.25m; }
            if (value > 6) { return 41; }
            if (value > 5.9m) { return 41.75m; }
            if (value > 5.8m) { return 41.5m; }
            if (value > 5.7m) { return 41.25m; }
            if (value > 5.6m) { return 41; }
            if (value > 5.5m) { return 40.75m; }
            if (value > 5.4m) { return 40.5m; }
            if (value > 5.3m) { return 40.25m; }
            if (value > 5.2m) { return 40; }
            if (value > 5.1m) { return 39.75m; }
            if (value > 5) { return 39.5m; }
            if (value > 4.9m) { return 38.75m; }
            if (value > 4.8m) { return 38.5m; }
            if (value > 4.7m) { return 38.25m; }
            if (value > 4.6m) { return 38; }
            if (value > 4.5m) { return 37.75m; }
            if (value > 4.4m) { return 37.5m; }
            if (value > 4.3m) { return 37.25m; }
            if (value > 4.2m) { return 37; }
            if (value > 4.1m) { return 36.75m; }
            if (value > 4) { return 36.5m; }
            if (value > 3.9m) { return 36.25m; }
            if (value > 3.8m) { return 36; }
            if (value > 3.7m) { return 35.75m; }
            if (value > 3.6m) { return 35.5m; }
            if (value > 3.5m) { return 35.25m; }
            return 35;
        }

        private bool isWhorl(string fingerType)
        {
            if (fingerType.StartsWith("W"))
            {
                return true;
            }
            return false;
        }

        private bool isArch(string fingerType)
        {
            if (fingerType.StartsWith("A"))
            {
                return true;
            }
            return false;
        }

        private bool isLoop(string fingerType)
        {
            //UL, RL, LF
            if (fingerType.StartsWith("UL")
                || fingerType.StartsWith("RL")
                || fingerType.StartsWith("LF"))
            {
                return true;
            }
            return false;
        }

        public void Calculate_Inter()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R1;
            MQModel mq1 = MQModelCollection.People16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }
            value = getValueByMQ(mq1.Value);

            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            ss:
            this.Interpersonal = value;
        }

        public void Calculate_Intrapersonal()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L1;
            MQModel mq1 = MQModelCollection.Self16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);

            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            ss:
            this.Intrapersonal = value;
        }

        public void Calculate_Logical()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R2;
            MQModel mq1 = MQModelCollection.Logic16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }
            value = getValueByMQ(mq1.Value);

            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            ss:
            this.Logical = value;
        }

        public void Calculate_Artistic()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R5;
            MQModel mq1 = MQModelCollection.People16;
            MQModel mq2 = MQModelCollection.Body16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }
            value = System.Math.Round((mq1.Value + mq2.Value) / 2, 2);
            value = getValueByMQ(value);

            if (value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            ss:
            this.Artistic = value;
        }

        public void Calculate_Observation()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R5;
            MQModel mq1 = MQModelCollection.Self16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }
            //value = getValueByMQ(mq1.Value);
            value = getValueByMQ(p1.PIValue2);

            //if (p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            //{
            //    value += 3;
            //}
            //else
            //{
            //    value += 1;
            //}

            ss:
            this.Observation = value;
        }

        public void Calculate_Planning()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L2;
            PIModel p2 = PIModelCollection.L1;
            MQModel mq1 = MQModelCollection.Self16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Planning = value;
        }

        public void Calculate_Creation()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L1;
            PIModel p2 = PIModelCollection.L3;
            MQModel mq1 = MQModelCollection.Picture16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Creation = value;
        }

        public void Calculate_Language()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L4;
            PIModel p2 = PIModelCollection.L2;
            MQModel mq1 = MQModelCollection.Word16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Language = value;
        }

        public void Calculate_Kinesthetic()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R3;
            PIModel p2 = PIModelCollection.L3;
            MQModel mq1 = MQModelCollection.Body16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Kinesthetic = value;
        }

        public void Calculate_Operation()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L3;
            PIModel p2 = PIModelCollection.R3;
            MQModel mq1 = MQModelCollection.Body16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Operation = value;
        }

        public void Calculate_LanguageInternalization()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L4;
            PIModel p2 = PIModelCollection.L2;
            MQModel mq1 = MQModelCollection.Word16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.LanguageInternalization = value;
        }

        public void Calculate_Music()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R5;
            PIModel p2 = PIModelCollection.R4;
            MQModel mq1 = MQModelCollection.Music16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Music = value;
        }

        public void Calculate_ImageApprec()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L5;
            PIModel p2 = PIModelCollection.R5;
            MQModel mq1 = MQModelCollection.Picture16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            
            //value = getValueByMQ(mq1.Value);
            value = getValueByMQ(p1.PIValue2);
            //if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            //if (p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            //{
            //    value += 3;
            //}
            //else
            //{
            //    value += 1;
            //}

            goto ss;

            ss:
            this.ImageAppreciation = value;
        }

        public void Calculate_EQIntelligent()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L2;
            PIModel p2 = PIModelCollection.R2;
            MQModel mq1 = MQModelCollection.Logic16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 1.5m;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.EQIntelligent = value;
        }

        public void Calculate_Spartial()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.L5;
            PIModel p2 = PIModelCollection.L2;
            PIModel p3 = PIModelCollection.R2;
            MQModel mq1 = MQModelCollection.Picture16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType)
                || isArch(p3.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 3;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Spartial = value;
        }

        public void Calculate_Concentration()
        {
            decimal value = 0;
            PIModel p1 = PIModelCollection.R4;
            PIModel p2 = PIModelCollection.R5;
            //PIModel p3 = PIModelCollection.L1;
            MQModel mq1 = MQModelCollection.Music16;
            decimal avgTFRC = this.PIModelCollection.TFRC_Average16;

            if (isArch(p1.FingerType)
                || isArch(p2.FingerType))
            //|| isArch(p3.FingerType))
            {
                value = p1.PIValue2 >= 8 ? 70 : 45;
                goto ss;
            }

            value = getValueByMQ(mq1.Value);
            if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20 && p2.PIValue2 > 20)
            {
                value += 3;
            }
            else if (mq1.Value > avgTFRC && p1.PIValue2 > avgTFRC && p1.PIValue2 > 20)
            {
                value += 2;
            }
            else if (mq1.Value > avgTFRC && p2.PIValue2 > avgTFRC && p2.PIValue2 > 20)
            {
                value += 2;
            }
            else
            {
                value += 1;
            }

            goto ss;

            ss:
            this.Concentration = value;
        }
    }
}