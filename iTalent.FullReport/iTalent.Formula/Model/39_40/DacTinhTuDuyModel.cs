﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Formula.Model.ExcelTemplate;
#pragma warning disable 1587

// ReSharper disable InconsistentNaming

namespace iTalent.Formula.Model._39_40
{
    public class DacTinhTuDuyModel
    {
        public IntelligenceModelCollection IntelligenceModel { get; set; }
        public PIModelCollection PICollection { get; set; }
        public DacTinhTiemThucModel DacTinhTiemThucModel { get; set; }

        public DacTinhTuDuyEnum EnumValue { get; set; }
        public string Code { get; set; }
        public string TenNhomVietnamese { get; set; }
        public string TenNhomEnglish { get; set; }
        public string MoTa { get; set; }

        public DacTinhTuDuyModel(PIModelCollection piCollection, IntelligenceModelCollection intelligenceModel,
            DacTinhTiemThucModel tiemThucModel, ExcelDacTinhTuDuyCollection excelDacTinhTuDuyCollection)
        {
            this.PICollection = piCollection;
            this.IntelligenceModel = intelligenceModel;
            this.DacTinhTiemThucModel = tiemThucModel;
            CalculateValue(excelDacTinhTuDuyCollection);
        }

        private void CalculateValue(ExcelDacTinhTuDuyCollection collection)
        {
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();
            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);
            string compareTypes;

            /// Hấp thu mâu thuẫn và phản biện
            /// self_contradiction_1
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.contradictive_absorption_opposition
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("self_contradiction_1");
                EnumValue = DacTinhTuDuyEnum.self_contradiction_1;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Mâu thuẫn phản biện
            /// self_contradiction_2
            /// 
            if (
               this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.contradictive_opposition
               )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("self_contradiction_2");
                EnumValue = DacTinhTuDuyEnum.self_contradiction_2;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Hấp thu như miếng bọt biển
            /// open_absorb_1
            /// 
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.sponge_like_absorption
                || (fingerTypes.Count(p => p.StartsWith("AR")) >= 1)
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("open_absorb_1");
                EnumValue = DacTinhTuDuyEnum.open_absorb_1;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// (Hấp thu chọn lựa || hấp thu theo cảm xúc || hấp thu thụ động)
            /// open_absorb_2
            /// 
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_absorption
                || this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.emotional_absorption
                || this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("open_absorb_2");
                EnumValue = DacTinhTuDuyEnum.open_absorb_2;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// "Phản biện, nổi loạn ||Phản biện cố chấp ||Phản biện có chọn lựa"
            /// converse_think
            /// 
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.revoltive_opposition
                || this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.stubborn_opposition
                || this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_opposition
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("converse_think");
                EnumValue = DacTinhTuDuyEnum.converse_think;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Bướng bỉnh, ngoan cố
            /// ego_centric
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.stubborn_insistence
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("ego_centric");
                EnumValue = DacTinhTuDuyEnum.ego_centric;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// "10 UL, THÙY TRƯỚC TRÁN > 23%|| 1 UL ngón cái + 1 Whorl ngón cái"
            /// self_conscious_2
            if (
                ((fingerTypes.Count(p => p.StartsWith("UL")) == 10
                 && this.IntelligenceModel.ThuyTruocTran > 23)
                || (
                    (this.PICollection.L1.FingerType.StartsWith("UL")
                     || this.PICollection.R1.FingerType.StartsWith("UL"))
                    && (this.PICollection.L1.FingerType.StartsWith("W")
                        || this.PICollection.R1.FingerType.StartsWith("W"))
                    )) || DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_insistence_ul
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("ego_centric");
                EnumValue = DacTinhTuDuyEnum.ego_centric;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Bắt chước đồng đội
            /// social_norm
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.peer_imitation 
                || DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.emotional_imitation
                || (this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_imitation)
                )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("social_norm");
                EnumValue = DacTinhTuDuyEnum.social_norm;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// TỐI THIỂU 1 UL Ở NGÓN CÁI && THÙY TRƯỚC TRÁN > 23 %
            /// self_conscious_1
            if (
                  ((this.PICollection.L1.FingerType.StartsWith("UL")
                     || this.PICollection.R1.FingerType.StartsWith("UL"))
                     && this.IntelligenceModel.ThuyTruocTran>23) || (this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_insistence_tfrc)
                     || (this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_insistence)
                 )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("self_conscious_1");
                EnumValue = DacTinhTuDuyEnum.self_conscious_1;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// Bắt chước theo môi trường
            /// social_service
            if (
                this.DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.environmental_imitation
                 )
            {
                ExcelDacTinhTuDuy tmp = collection.Find("social_service");
                EnumValue = DacTinhTuDuyEnum.social_service;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

        }

        //private void CalculateValue(ExcelDacTinhTuDuyCollection collection)
        //{
        //    List<string> fingerTypes = PICollection.PIModelList.Select(p => p.FingerType).ToList();
        //    string compareTypes;

        //    /// open_absorb_1
        //    /// Hấp thu như miếng bọt biển - sponge_like_absorption
        //    /// && Có Arch
        //    if (
        //        DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.sponge_like_absorption
        //        && fingerTypes.Count(p => p.StartsWith("A")) >= 1
        //        )
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("open_absorb_1");
        //        EnumValue = DacTinhTuDuyEnum.open_absorb_1;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// open_absorb_2
        //    /// (Hấp thu chọn lựa - selective_absorption
        //    /// || hấp thu theo cảm xúc - emotional_absorption
        //    /// || hấp thu thụ động - passive_absorption
        //    /// ) && Có Arch
        //    if (
        //        (DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.selective_absorption
        //         || DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.emotional_absorption
        //         || DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.passive_absorption
        //            )
        //        && fingerTypes.Count(p => p.StartsWith("A")) >= 1
        //        )
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("open_absorb_2");
        //        EnumValue = DacTinhTuDuyEnum.open_absorb_2;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// social_norm
        //    /// 5UL && THÙY CHẨM > 20%
        //    if (fingerTypes.Count(p => p.StartsWith("UL")) == 5
        //        && IntelligenceModel.ThuyCham > 20)
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("social_norm");
        //        EnumValue = DacTinhTuDuyEnum.social_norm;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious_1
        //    /// TỐI THIỂU 1 UL Ở NGÓN CÁI && THÙY TRƯỚC TRÁN > 23%
        //    if (
        //        (PICollection.L1.FingerType.StartsWith("UL")
        //         || PICollection.R1.FingerType.StartsWith("UL"))
        //        && IntelligenceModel.ThuyTruocTran > 23)
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("self_conscious_1");
        //        EnumValue = DacTinhTuDuyEnum.self_conscious_1;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_conscious_2
        //    /// 10 UL, THÙY TRƯỚC TRÁN > 23%
        //    if (fingerTypes.Count(p => p.StartsWith("UL")) == 10
        //        && IntelligenceModel.ThuyTruocTran > 23)
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("self_conscious_2");
        //        EnumValue = DacTinhTuDuyEnum.self_conscious_2;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// ego_centric
        //    /// WD Ở 2 NGÓN CÁI || 
        //    /// TỔNG LÀ 3 WD ||
        //    /// WD Ở 2 NGÓN TRỎ && THUY TRAN > 20 % ||
        //    /// WE Ở 2 NGÓN CÁI && THÙY TRƯỚC TRÁN > 22 %
        //    if (
        //           (PICollection.L1.FingerType.StartsWith("WD")
        //         && PICollection.R1.FingerType.StartsWith("WD"))

        //        || (fingerTypes.Count(p => p.StartsWith("WD")) == 3)

        //        || (PICollection.L2.FingerType.StartsWith("WD")
        //            && PICollection.R2.FingerType.StartsWith("WD")
        //            && IntelligenceModel.ThuyTran > 20)

        //        || (PICollection.L1.FingerType.StartsWith("WE")
        //            && PICollection.R1.FingerType.StartsWith("WE")
        //            && IntelligenceModel.ThuyTruocTran > 22)
        //            )
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("ego_centric");
        //        EnumValue = DacTinhTuDuyEnum.ego_centric;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// converse_think
        //    /// RL
        //    if (fingerTypes.Count(p => p.StartsWith("RL")) >= 1)
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("converse_think");
        //        EnumValue = DacTinhTuDuyEnum.converse_think;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_contradiction_1
        //    /// Mâu thuẫn phản biện - contradictive_opposition
        //    if (DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.contradictive_opposition)
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("self_contradiction_1");
        //        EnumValue = DacTinhTuDuyEnum.self_contradiction_1;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// self_contradiction_2
        //    /// (ARCH  && (WD || WC || WI) && RL)
        //    /// || RL && ARCH"
        //    if (
        //        (
        //            fingerTypes.Count(p => p.StartsWith("WD")) >= 1
        //            && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
        //            && (fingerTypes.Count(p => p.StartsWith("WD")) >= 1
        //                || fingerTypes.Count(p => p.StartsWith("WC")) >= 1
        //                || fingerTypes.Count(p => p.StartsWith("WI")) >= 1)
        //            )

        //        || (fingerTypes.Count(p => p.StartsWith("RL")) >= 1
        //            && fingerTypes.Count(p => p.StartsWith("A")) >= 1)
        //        )
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("self_contradiction_2");
        //        EnumValue = DacTinhTuDuyEnum.self_contradiction_2;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }


        //    /// social_service
        //    /// Bắt chước theo môi trường - environmental_imitation
        //    if (DacTinhTiemThucModel.EnumValue == DacTinhTiemThucEnum.environmental_imitation)
        //    {
        //        ExcelDacTinhTuDuy tmp = collection.Find("social_service");
        //        EnumValue = DacTinhTuDuyEnum.social_service;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //}
    }
}