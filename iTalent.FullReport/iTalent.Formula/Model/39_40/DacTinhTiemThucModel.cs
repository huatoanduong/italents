﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Formula.Model.ExcelTemplate;
#pragma warning disable 1587

// ReSharper disable InconsistentNaming

namespace iTalent.Formula.Model._39_40
{
    public class DacTinhTiemThucModel
    {
        public IntelligenceModelCollection IntelligenceModel { get; set; }
        public PIModelCollection PICollection { get; set; }

        public DacTinhTiemThucEnum EnumValue { get; set; }
        public string Code { get; set; }
        public string TenNhomVietnamese { get; set; }
        public string TenNhomEnglish { get; set; }
        public string MoTa { get; set; }

        public DacTinhTiemThucModel(PIModelCollection piCollection, IntelligenceModelCollection intelligenceModel,
            ExcelDacTinhTiemThucCollection excelDacTinhTiemThucCollection)
        {
            this.PICollection = piCollection;
            this.IntelligenceModel = intelligenceModel;
            CalculateValue(excelDacTinhTiemThucCollection);
        }

        private void CalculateValue(ExcelDacTinhTiemThucCollection collection)
        {
            List<string> fingerTypes = PICollection.Collection.Select(p => p.FingerType).ToList();
            List<string> first4FingerTypes = new List<string>();
            first4FingerTypes.Add(PICollection.L1.FingerType);
            first4FingerTypes.Add(PICollection.L2.FingerType);
            first4FingerTypes.Add(PICollection.R1.FingerType);
            first4FingerTypes.Add(PICollection.R2.FingerType);

            /// contradictive_absorption_opposition
            /// ARCH && RL
            if (fingerTypes.Count(p => p.StartsWith("A")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                //&& (
                //    fingerTypes.Count(p => p.StartsWith("WD")) >= 1
                //    || fingerTypes.Count(p => p.StartsWith("WC")) >= 1
                //    || fingerTypes.Count(p => p.StartsWith("WI")) >= 1
                //    )
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("contradictive_absorption_opposition");
                EnumValue = DacTinhTiemThucEnum.contradictive_absorption_opposition;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// contradictive_opposition
            /// RL && WD
            if (first4FingerTypes.Count(p => p.StartsWith("WD")) >= 1
                && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("contradictive_opposition");
                EnumValue = DacTinhTiemThucEnum.contradictive_opposition;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            //sponge_like_absorption
            /// AS / AU && THUY TRUOC TRAN > 20 % 
            if (
                IntelligenceModel.ThuyTruocTran > 20
                && (
                    fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                    || fingerTypes.Count(p => p.StartsWith("AS")) >= 1
                    ))
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("sponge_like_absorption");
                EnumValue = DacTinhTiemThucEnum.sponge_like_absorption;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// AU
            /// selective_absorption
            if (
                fingerTypes.Count(p => p.StartsWith("AU")) >= 1
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("selective_absorption");
                EnumValue = DacTinhTiemThucEnum.selective_absorption;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// L1 || R1 = UL && AT bất kì
            /// emotional_imitation
            if (
                fingerTypes.Count(p => p.StartsWith("AT")) >= 1
                && (
                    this.PICollection.L1.FingerType.StartsWith("UL")
                    || this.PICollection.R1.FingerType.StartsWith("UL")
                    )
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("emotional_imitation");
                EnumValue = DacTinhTiemThucEnum.emotional_imitation;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// AT
            /// emotional_absorption
            if (
                fingerTypes.Count(p => p.StartsWith("AT")) >= 1)
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("emotional_absorption");
                EnumValue = DacTinhTiemThucEnum.emotional_absorption;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// AS
            /// passive_absorption

            if (
                fingerTypes.Count(p => p.StartsWith("AS")) >= 1)
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("passive_absorption");
                EnumValue = DacTinhTiemThucEnum.passive_absorption;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// > 3RL or
            /// 2 RL + WX or
            /// 2RL + WD(WD IN THUMB)
            /// revoltive_opposition

            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 3
                || (fingerTypes.Count(p => p.StartsWith("RL")) >= 2
                    && fingerTypes.Count(p => p.StartsWith("WX")) >= 1)
                || (fingerTypes.Count(p => p.StartsWith("RL")) >= 2
                    && (this.PICollection.L1.FingerType.StartsWith("WD")
                        || this.PICollection.R1.FingerType.StartsWith("WD")))
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("revoltive_opposition");
                EnumValue = DacTinhTiemThucEnum.revoltive_opposition;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// >= 2RL
            /// stubborn_opposition
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 2
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("stubborn_opposition");
                EnumValue = DacTinhTiemThucEnum.stubborn_opposition;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// 1 RL
            /// selective_opposition
            if (
                fingerTypes.Count(p => p.StartsWith("RL")) >= 1
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("selective_opposition");
                EnumValue = DacTinhTiemThucEnum.selective_opposition;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }

            /// WD Ở 2 NGÓN CÁI ||
            /// TỔNG LÀ 3 WD trong 4 ngón đầu tiên ||
            /// WD Ở 2 NGÓN TRỎ && THUY TRAN > 20 % ||
            /// WE Ở 2 NGÓN CÁI && THÙY TRƯỚC TRÁN > 22 % 
            /// stubborn_insistence
            if (
                (this.PICollection.L1.FingerType.StartsWith("WD")
                 && this.PICollection.R1.FingerType.StartsWith("WD"))
                || first4FingerTypes.Count(p => p.StartsWith("WD")) >= 3
                || (this.PICollection.L2.FingerType.StartsWith("WD")
                    && this.PICollection.R2.FingerType.StartsWith("WD")
                    && IntelligenceModel.ThuyTran > 20)
                || (this.PICollection.L1.FingerType.StartsWith("WE")
                    && this.PICollection.R1.FingerType.StartsWith("WE")
                    && IntelligenceModel.ThuyTruocTran > 22)
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("stubborn_insistence");
                EnumValue = DacTinhTiemThucEnum.stubborn_insistence;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }



            /// TỐI THIỂU 1 UL Ở NGÓN CÁI && 1 Whorl ở ngón cái && 
            /// THÙY TRƯỚC TRÁN > 23 % 
            /// selective_insistence_ul
            if (
                (this.PICollection.L1.FingerType.StartsWith("UL")
                 || this.PICollection.R1.FingerType.StartsWith("UL"))
                &&
                (this.PICollection.L1.FingerType.StartsWith("W")
                 || this.PICollection.R1.FingerType.StartsWith("W"))
                && this.IntelligenceModel.ThuyTruocTran > 23
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("selective_insistence_ul");
                EnumValue = DacTinhTiemThucEnum.selective_insistence_ul;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Các trường hợp còn lại của Whorl
            /// 2 ngón cái đều >= (TFRC / 10)
            /// selective_insistence_tfrc

            if (
                fingerTypes.Count(p => p.StartsWith("W")) >= 1
                && this.PICollection.L1.PIValue > this.PICollection.TFRC_Average
                && this.PICollection.R1.PIValue > this.PICollection.TFRC_Average
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("selective_insistence_tfrc");
                EnumValue = DacTinhTiemThucEnum.selective_insistence_tfrc;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// Các trường hợp còn lại của Whorl
            /// selective_insistence

            if (
                fingerTypes.Count(p => p.StartsWith("W")) >= 1
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("selective_insistence");
                EnumValue = DacTinhTiemThucEnum.selective_insistence;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// >= 5 UL &&
            /// THÙY TRƯỚC TRÁN > 22 % 
            /// selective_imitation

            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 5
                && this.IntelligenceModel.ThuyTruocTran > 22
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("selective_imitation");
                EnumValue = DacTinhTiemThucEnum.selective_imitation;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }


            /// >= 5UL && 
            /// THÙY CHẨM > 20 % 
            /// peer_imitation

            if (
                fingerTypes.Count(p => p.StartsWith("UL")) >= 5
                && this.IntelligenceModel.ThuyCham > 20
                )
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("peer_imitation");
                EnumValue = DacTinhTiemThucEnum.peer_imitation;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }



            /// L1 || L2 || R1 || R2 la UL
            /// environmental_imitation

            if (
                this.PICollection.L1.FingerType.StartsWith("UL")
                || this.PICollection.L2.FingerType.StartsWith("UL")
                || this.PICollection.R1.FingerType.StartsWith("UL")
                || this.PICollection.R2.FingerType.StartsWith("UL"))
            {
                ExcelDacTinhTiemThuc tmp = collection.Find("environmental_imitation");
                EnumValue = DacTinhTiemThucEnum.environmental_imitation;
                Code = tmp.Code;
                TenNhomVietnamese = tmp.TenNhomVietnamese;
                TenNhomEnglish = tmp.TenNhomEnglish;
                MoTa = tmp.MoTa;
                return;
            }
        }

        //private void CalculateValue(ExcelDacTinhTiemThucCollection collection)
        //{
        //    List<string> fingerTypes = PICollection.PIModelList.Select(p => p.FingerType).ToList();
        //    string compareTypes;

        //    /// environmental_imitation
        //    /// UL

        //    if (fingerTypes.Count(p => p.StartsWith("UL")) == 10)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("environmental_imitation");
        //        EnumValue = DacTinhTiemThucEnum.environmental_imitation;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// peer_imitation
        //    /// 5UL && 
        //    /// THÙY CHẨM > 20 %

        //    if (fingerTypes.Count(p => p.StartsWith("UL")) == 5
        //        && IntelligenceModel.ThuyCham > 20)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("peer_imitation");
        //        EnumValue = DacTinhTiemThucEnum.peer_imitation;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// selective_imitation
        //    /// 5 UL &&
        //    /// THÙY TRƯỚC TRÁN > 22 %

        //    if (fingerTypes.Count(p => p.StartsWith("UL")) == 5
        //        && IntelligenceModel.ThuyTruocTran > 22)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("selective_imitation");
        //        EnumValue = DacTinhTiemThucEnum.selective_imitation;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// emotional_imitation
        //    /// UL (4 NGÓN ĐẦU TIÊN) + AT

        //    if (
        //        PICollection.L1.FingerType.StartsWith("UL")
        //        && PICollection.L2.FingerType.StartsWith("UL")
        //        && PICollection.R1.FingerType.StartsWith("UL")
        //        && PICollection.R2.FingerType.StartsWith("UL")
        //        && fingerTypes.Count(p => p.StartsWith("AT")) >= 1
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("emotional_imitation");
        //        EnumValue = DacTinhTiemThucEnum.emotional_imitation;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// stubborn_insistence
        //    /// WD Ở 2 NGÓN CÁI ||
        //    /// TỔNG LÀ 3 WD ||
        //    /// WD Ở 2 NGÓN TRỎ && THUY TRAN > 20 % ||
        //    /// WE Ở 2 NGÓN CÁI && THÙY TRƯỚC TRÁN > 22 %
        //    if (
        //        (PICollection.L1.FingerType.StartsWith("WD")
        //         && PICollection.R1.FingerType.StartsWith("WD"))

        //        || (fingerTypes.Count(p => p.StartsWith("WD")) == 3)

        //        || (PICollection.L2.FingerType.StartsWith("WD")
        //            && PICollection.R2.FingerType.StartsWith("WD")
        //            && IntelligenceModel.ThuyTran > 20)

        //        || (PICollection.L1.FingerType.StartsWith("WE")
        //            && PICollection.R1.FingerType.StartsWith("WE")
        //            && IntelligenceModel.ThuyTruocTran > 22)
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("stubborn_insistence");
        //        EnumValue = DacTinhTiemThucEnum.stubborn_insistence;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// selective_insistence_tfrc

        //    //TODO

        //    /// selective_insistence_ul
        //    /// TỐI THIỂU 1 UL Ở NGÓN CÁI && 
        //    /// THÙY TRƯỚC TRÁN > 23 % "
        //    if (
        //        (PICollection.L1.FingerType.StartsWith("UL")
        //         || PICollection.R1.FingerType.StartsWith("UL"))
        //        && IntelligenceModel.ThuyTruocTran > 23
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("selective_insistence_ul");
        //        EnumValue = DacTinhTiemThucEnum.selective_insistence_ul;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// selective_insistence


        //    /// selective_opposition
        //    /// 1 RL

        //    if (fingerTypes.Count(p => p.StartsWith("RL")) == 1)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("selective_opposition");
        //        EnumValue = DacTinhTiemThucEnum.selective_opposition;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// stubborn_opposition
        //    /// >= 2RL

        //    if (fingerTypes.Count(p => p.StartsWith("RL")) >= 2)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("stubborn_opposition");
        //        EnumValue = DacTinhTiemThucEnum.stubborn_opposition;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// revoltive_opposition
        //    /// > 3RL or
        //    /// 2 RL + WX or
        //    /// 2RL + WD(WD IN THUMB)

        //    if (
        //        fingerTypes.Count(p => p.StartsWith("RL")) > 3

        //        || (fingerTypes.Count(p => p.StartsWith("RL")) == 2
        //            && fingerTypes.Count(p => p.StartsWith("RL")) == 1)

        //        || (fingerTypes.Count(p => p.StartsWith("RL")) == 2
        //            && (PICollection.L1.FingerType.StartsWith("WD")
        //                || PICollection.R1.FingerType.StartsWith("WD")))
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("revoltive_opposition");
        //        EnumValue = DacTinhTiemThucEnum.revoltive_opposition;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// passive_absorption
        //    /// AS

        //    if (fingerTypes.Count(p => p.StartsWith("AS")) >= 1)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("passive_absorption");
        //        EnumValue = DacTinhTiemThucEnum.passive_absorption;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// emotional_absorption
        //    /// AT

        //    if (fingerTypes.Count(p => p.StartsWith("AS")) >= 1)
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("emotional_absorption");
        //        EnumValue = DacTinhTiemThucEnum.emotional_absorption;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// sponge_like_absorption
        //    /// AS/AU && 
        //    /// THUY TRUOC TRAN > 20 %

        //    if (
        //        (fingerTypes.Count(p => p.StartsWith("AS")) >= 1
        //         || fingerTypes.Count(p => p.StartsWith("AU")) >= 1)
        //        && IntelligenceModel.ThuyTruocTran > 20
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("sponge_like_absorption");
        //        EnumValue = DacTinhTiemThucEnum.sponge_like_absorption;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// selective_absorption
        //    /// AU

        //    if (
        //        fingerTypes.Count(p => p.StartsWith("AU")) >= 1
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("selective_absorption");
        //        EnumValue = DacTinhTiemThucEnum.selective_absorption;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// contradictive_opposition
        //    /// RL && Arch

        //    if (
        //        fingerTypes.Count(p => p.StartsWith("RL")) >= 1
        //        && fingerTypes.Count(p => p.StartsWith("A")) >= 1
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("contradictive_opposition");
        //        EnumValue = DacTinhTiemThucEnum.contradictive_opposition;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }

        //    /// contradictive_absorption_opposition
        //    /// ARCH  && (WD||WC||WI) && RL

        //    if (
        //        fingerTypes.Count(p => p.StartsWith("A")) >= 1

        //        && (fingerTypes.Count(p => p.StartsWith("WD")) >= 1
        //            || fingerTypes.Count(p => p.StartsWith("WC")) >= 1
        //            || fingerTypes.Count(p => p.StartsWith("WI")) >= 1)

        //        && fingerTypes.Count(p => p.StartsWith("RL")) >= 1
        //        )
        //    {
        //        ExcelDacTinhTiemThuc tmp = collection.Find("contradictive_absorption_opposition");
        //        EnumValue = DacTinhTiemThucEnum.contradictive_absorption_opposition;
        //        Code = tmp.Code;
        //        TenNhomVietnamese = tmp.TenNhomVietnamese;
        //        TenNhomEnglish = tmp.TenNhomEnglish;
        //        MoTa = tmp.MoTa;
        //        return;
        //    }
        //}

    }
}