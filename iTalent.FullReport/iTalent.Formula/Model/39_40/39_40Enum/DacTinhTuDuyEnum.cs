﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model._39_40
{
    public enum DacTinhTuDuyEnum
    {
        /// Tư duy Hấp thụ
        open_absorb_1,

        /// Tư duy Hấp thụ
        open_absorb_2,

        /// Tư duy Chuẩn mực xã hội
        social_norm,

        /// Tư duy Tự ý thức 1
        self_conscious_1,

        /// Tư duy Tự ý thức 2
        self_conscious_2,

        /// Tư duy Cá nhân
        ego_centric,

        /// Tư duy Phản biện
        converse_think,

        /// Tư duy Tự mâu thuẫn
        self_contradiction_1,

        /// Tư duy Tự mâu thuẫn
        self_contradiction_2,

        /// Tư duy Cộng đồng xã hội
        social_service,
    }
}
