﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model._39_40
{
    public enum DacTinhTiemThucEnum
    {
        /// Bắt chước theo môi trường
        environmental_imitation,

        /// Bắt chước đồng đội
        peer_imitation,

        /// Bắt chước có chọn lựa
        selective_imitation,

        /// Bắt chước theo cảm xúc
        emotional_imitation,

        /// Bướng bỉnh, ngoan cố
        stubborn_insistence,

        /// Cố chấp có chọn lựa
        selective_insistence_tfrc,

        /// Cố chấp có chọn lựa
        selective_insistence_ul,

        /// Cố chấp có chọn lựa
        selective_insistence,

        /// Phản biện có chọn lựa
        selective_opposition,

        /// Phản biện cố chấp
        stubborn_opposition,

        /// Phản biện, nổi loạn
        revoltive_opposition,

        /// Hấp thu thụ động
        passive_absorption,

        /// Hấp thụ theo cảm xúc
        emotional_absorption,

        /// Hấp thu như miếng bọt biển
        sponge_like_absorption,

        /// Hấp thụ chọn lựa
        selective_absorption,

        /// Mâu thuẫn phản biện
        contradictive_opposition,

        /// Hấp thu mâu thuẫn và phản biện
        contradictive_absorption_opposition,
    }
}