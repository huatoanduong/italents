﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model.ExcelTemplate
{
    public class ExcelKhuyenNghi
    {
        public string Code { get; set; }
        public string TenNhomEnglish { get; set; }
        //public string TenNhomVietnamese { get; set; }
        public string MoTa { get; set; }

        public ExcelKhuyenNghiCollection Owner { get; private set; }

        public ExcelKhuyenNghi(ExcelKhuyenNghiCollection owner)
        {
            this.Owner = owner;
        }
    }

    public class ExcelKhuyenNghiCollection
    {
        public List<ExcelKhuyenNghi> Collection { get; set; }

        public ExcelKhuyenNghiCollection()
        {
            Collection = new List<ExcelKhuyenNghi>();
        }

        public void Add(ExcelKhuyenNghi value)
        {
            Collection.Add(value);
        }

        public ExcelKhuyenNghi Find(string code)
        {
            return Collection.FirstOrDefault(p => p.Code == code);
        }
    }
}