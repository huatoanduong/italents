﻿using System.Collections.Generic;
using System.Linq;

namespace iTalent.Formula.Model.ExcelTemplate
{
    public class ExcelNGFValue
    {
        /// <summary>
        /// WS, WC, UL...
        /// </summary>
        public string FingerType { get; set; }

        /// <summary>
        /// L1, L2, R1...
        /// </summary>
        public string FingerName { get; set; }

        public decimal Value { get; set; }

        public ExcelNGFValueCollection Collection { get; private set; }

        public ExcelNGFValue(ExcelNGFValueCollection owner)
        {
            this.Collection = owner;
        }
    }

    /// <summary> 
    /// Cách khai báo lấy giá trị NGF
    /// Collection["RF"].Value
    /// </summary>
    public class ExcelNGFValueCollection
    {
        public List<ExcelNGFValue> Collection { get; set; }

        public ExcelNGFValueCollection()
        {
            Collection = new List<ExcelNGFValue>();
        }

        public void Add(ExcelNGFValue value)
        {
            Collection.Add(value);
        }

        public ExcelNGFValue FindByName(string name)
        {
            return Collection.FirstOrDefault(value => value.FingerName == name);
        }

        public ExcelNGFValue FindByTypeAndName(string type, string name)
        {
            return Collection.FirstOrDefault(value => value.FingerType == type && value.FingerName == name);
        }
    }
}