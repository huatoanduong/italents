﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model.ExcelTemplate
{
    public class ExcelTinhCachHanhVi
    {
        public int Code { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public ExcelTinhCachHanhViCollection Owner { get; private set; }

        public ExcelTinhCachHanhVi(ExcelTinhCachHanhViCollection owner)
        {
            this.Owner = owner;
        }
    }

    public class ExcelTinhCachHanhViCollection
    {
        public List<ExcelTinhCachHanhVi> Collection { get; set; }

        public ExcelTinhCachHanhViCollection()
        {
            Collection = new List<ExcelTinhCachHanhVi>();
        }

        public void Add(ExcelTinhCachHanhVi value)
        {
            Collection.Add(value);
        }

        public ExcelTinhCachHanhVi Find(int code)
        {
            return Collection.FirstOrDefault(p => p.Code == code);
        }
    }
}