﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model.ExcelTemplate
{
    public class ExcelDacTinhTiemThuc
    {
        public string Code { get; set; }
        public string TenNhomEnglish { get; set; }
        public string TenNhomVietnamese { get; set; }
        public string MoTa { get; set; }

        public ExcelDacTinhTiemThucCollection Owner { get; private set; }

        public ExcelDacTinhTiemThuc(ExcelDacTinhTiemThucCollection owner)
        {
            this.Owner = owner;
        }
    }

    public class ExcelDacTinhTiemThucCollection
    {
        public List<ExcelDacTinhTiemThuc> Collection { get; set; }

        public ExcelDacTinhTiemThucCollection()
        {
            Collection = new List<ExcelDacTinhTiemThuc>();
        }

        public void Add(ExcelDacTinhTiemThuc value)
        {
            Collection.Add(value);
        }

        public ExcelDacTinhTiemThuc Find(string code)
        {
            return Collection.FirstOrDefault(p => p.Code == code);
        }
    }
}