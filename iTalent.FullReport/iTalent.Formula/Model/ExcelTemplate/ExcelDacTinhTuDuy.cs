﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model.ExcelTemplate
{
    public class ExcelDacTinhTuDuy
    {
        public string Code { get; set; }
        public string TenNhomEnglish { get; set; }
        public string TenNhomVietnamese { get; set; }
        public string MoTa { get; set; }

        public ExcelDacTinhTuDuyCollection Owner { get; private set; }

        public ExcelDacTinhTuDuy(ExcelDacTinhTuDuyCollection owner)
        {
            this.Owner = owner;
        }
    }

    public class ExcelDacTinhTuDuyCollection
    {
        public List<ExcelDacTinhTuDuy> Collection { get; set; }

        public ExcelDacTinhTuDuyCollection()
        {
            Collection = new List<ExcelDacTinhTuDuy>();
        }

        public void Add(ExcelDacTinhTuDuy value)
        {
            Collection.Add(value);
        }

        public ExcelDacTinhTuDuy Find(string code)
        {
            return Collection.FirstOrDefault(p => p.Code == code);
        }
    }
}