﻿using System.Collections.Generic;
using System.Linq;

namespace iTalent.Formula.Model.ExcelTemplate
{
    public class ExcelHeSoTiemNangModel
    {
        public string FingerName { get; set; }
        public string MoTaNgan { get; set; }
        public string MoTaDai { get; set; }

        public ExcelHeSoTiemNangCollection Collection { get; private set; }

        public ExcelHeSoTiemNangModel(ExcelHeSoTiemNangCollection owner)
        {
            this.Collection = owner;
        }
    }
    
    /// <summary> 
    /// Cách khai báo lấy giá trị NGF
    /// Collection["RF"].Value
    /// </summary>
    public class ExcelHeSoTiemNangCollection
    {
        public List<ExcelHeSoTiemNangModel> Collection { get; set; }

        public ExcelHeSoTiemNangCollection()
        {
            Collection = new List<ExcelHeSoTiemNangModel>();
        }

        public void Add(ExcelHeSoTiemNangModel value)
        {
            Collection.Add(value);
        }

        public ExcelHeSoTiemNangModel FindByName(string name)
        {
            return Collection.FirstOrDefault(value => value.FingerName == name);
        }

    }
}