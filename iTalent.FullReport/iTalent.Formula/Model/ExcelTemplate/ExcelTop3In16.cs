﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model.ExcelTemplate
{
   public class ExcelTop3In16
    {
        public string Code { get; set; }
        public string TenNangLuc { get; set; }
        public string AdultDescription { get; set; }
        public string ChildDescription { get; set; }

    }
    public class ExcelTop3In16Collection
    {
        public List<ExcelTop3In16> Collection { get; set; }

        public ExcelTop3In16Collection()
        {
            Collection = new List<ExcelTop3In16>();
        }

        public void Add(ExcelTop3In16 value)
        {
            Collection.Add(value);
        }

        public ExcelTop3In16 Find(string code)
        {
            return Collection.FirstOrDefault(p => p.Code == code);
        }
    }
}