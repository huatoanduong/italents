﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Entity;
using System.Drawing;
using iTalent.Formula.Model._39_40;
using System.Globalization;
// ReSharper disable InconsistentNaming

namespace iTalent.Formula.Model
{
    public class ReportFieldModel
    {
        public FIAgency Distributor { get; set; }
        public FICustomer Customer { get; set; }

        //Fill to DBase
        public FIFingerAnalysis FingerAnalysis { get; set; }
        public FIMQChart MQ_Chart { get; set; }

        public RidgeCountModelCollection RecordModelCollection { get; set; }
        public CareerModelCollection CareerModelCollection { get; set; }
        public IntelligenceModelCollection IntelligenceModelCollection { get; set; }
        public MQModelCollection MQModelCollection { get; set; }
        public PIModelCollection PIModelCollection { get; set; }
        public Top3NGFModelCollection Top3NgfModelCollection { get; set; }
        public Top3In16ModelCollection Top3VanDongCollection { get; set; }
        public ACIDModel ACIDModel { get; set; }
        public DacTinhTuDuyModel DacTinhTuduyModel { get; set; }
        public DacTinhTiemThucModel DacTinhTiemThucModel { get; set; }
        public HanhTrinhCuocSongModel HanhTrinhCuocSongModel { get; set; }
        public TinhCachHanhViModel TinhCachHanhViModel { get; set; }
        public VanDongModelCollection VanDongModel { get; set; }
        public KhuyenNghiModel KhuyenNghiModel { get; set; }

        public ReportFieldModel(ref FIFingerAnalysis AnalysisEntity, ref List<FIMQChart> ArrayChartEntity
            , RidgeCountModelCollection modelCollectionRidgeCountModelCollection)
        {
            RecordModelCollection = modelCollectionRidgeCountModelCollection;
            GetRidgeCount();
            FingerAnalysis = AnalysisEntity;
        }

        public ReportFieldModel() { }

        public void CalucalteGeneral(ref FIFingerAnalysis AnalysisEntity, ref List<FIMQChart> ArrayChartEntity)
        {
            BuildAnalysisEntity(ref AnalysisEntity);
            BuildChartEntity(ref ArrayChartEntity);
            FingerAnalysis = AnalysisEntity;
        }

        public void CalucalteExport() { GetRidgeCount(); }

        #region ToString method

        protected string ToString(DateTime? value)
        {
            if (value == null)
            {
                return "";
            }
            return ((DateTime) value).ToString("dd/MM/yyyy");
        }

        protected string ToString(decimal? value)
        {
            if (value == null)
            {
                return "";
            }
            return Math.Round((decimal) value, 2).ToString(CultureInfo.InvariantCulture);
        }

        protected string ToString(string value) { return (value != null) ? value : ""; }

        protected string ToString(int? value)
        {
            if (value == null)
            {
                return "";
            }
            return ((int) value).ToString();
        }

        protected string ToString(double? value)
        {
            if (value == null)
            {
                return "";
            }
            return (Math.Round((double) value, 2).ToString());
        }

        #endregion

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            //Distributor			
            dictionary.Add("AGENCY_NAME", ToString(Distributor.Name));

            ////Customer	
            dictionary.Add("REPORT_ID", ToString(Customer.ReportID));
            dictionary.Add("CUS_NAME", ToString(Customer.Name));
            dictionary.Add("CUS_DATE", ToString(Customer.Date));
            dictionary.Add("CUS_SEX", ToString(Customer.Gender));
            dictionary.Add("CUS_BRITHDAY", ToString(Customer.DOB));
            dictionary.Add("CUS_PARENT", ToString(Customer.Parent));
            dictionary.Add("CUS_PHONE", ToString(Customer.Tel));
            dictionary.Add("CUS_EMAIL", ToString(Customer.Email));
            dictionary.Add("CUS_ADDRESS", ToString(Customer.Address1));

            //Fingertype
            dictionary.Add("L1FT", ToString(L1T));
            dictionary.Add("L2FT", ToString(L2T));
            dictionary.Add("L3FT", ToString(L3T));
            dictionary.Add("L4FT", ToString(L4T));
            dictionary.Add("L5FT", ToString(L5T));
            dictionary.Add("R1FT", ToString(R1T));
            dictionary.Add("R2FT", ToString(R2T));
            dictionary.Add("R3FT", ToString(R3T));
            dictionary.Add("R4FT", ToString(R4T));
            dictionary.Add("R5FT", ToString(R5T));

            dictionary.Add("L1PI", ToString(PI1));
            dictionary.Add("L2PI", ToString(PI2));
            dictionary.Add("L3PI", ToString(PI3));
            dictionary.Add("L4PI", ToString(PI4));
            dictionary.Add("L5PI", ToString(PI5));
            dictionary.Add("R1PI", ToString(PI6));
            dictionary.Add("R2PI", ToString(PI7));
            dictionary.Add("R3PI", ToString(PI8));
            dictionary.Add("R4PI", ToString(PI9));
            dictionary.Add("R5PI", ToString(PI10));

            dictionary.Add("L1NGF", ToString(NGF1));
            dictionary.Add("L2NGF", ToString(NGF2));
            dictionary.Add("L3NGF", ToString(NGF3));
            dictionary.Add("L4NGF", ToString(NGF4));
            dictionary.Add("L5NGF", ToString(NGF5));
            dictionary.Add("R1NGF", ToString(NGF6));
            dictionary.Add("R2NGF", ToString(NGF7));
            dictionary.Add("R3NGF", ToString(NGF8));
            dictionary.Add("R4NGF", ToString(NGF9));
            dictionary.Add("R5NGF", ToString(NGF10));

            dictionary.Add("L1HTVH", ToString(ChiSoHapThu1));
            dictionary.Add("L2HTVH", ToString(ChiSoHapThu2));
            dictionary.Add("L3HTVH", ToString(ChiSoHapThu3));
            dictionary.Add("L4HTVH", ToString(ChiSoHapThu4));
            dictionary.Add("L5HTVH", ToString(ChiSoHapThu5));
            dictionary.Add("R1HTVH", ToString(ChiSoHapThu6));
            dictionary.Add("R2HTVH", ToString(ChiSoHapThu7));
            dictionary.Add("R3HTVH", ToString(ChiSoHapThu8));
            dictionary.Add("R4HTVH", ToString(ChiSoHapThu9));
            dictionary.Add("R5HTVH", ToString(ChiSoHapThu10));

            dictionary.Add("LTRCP", ToString(NaoTrai));
            dictionary.Add("RTRCP", ToString(NaoPhai));

            dictionary.Add("TFRC", ToString(TFRC));
            dictionary.Add("AFRC", ToString(AFRC));
            dictionary.Add("SUB_TFRC_AFRC", ToString(MinusFRC));

            dictionary.Add("SAP", ToString(ThuyTruocTran));
            dictionary.Add("SBP", ToString(ThuyTran));
            dictionary.Add("SCP", ToString(ThuyDinh));
            dictionary.Add("SDP", ToString(ThuyThaiDuong));
            dictionary.Add("SER", ToString(ThuyCham));

            dictionary.Add("PCNT", ToString(PhongCachNhanThuc));
            dictionary.Add("PCLL", ToString(PhongCachLapLuan));
            dictionary.Add("PCHH", ToString(PhongCachHocHoi));
            dictionary.Add("PCPL", ToString(PhongCachPhanLoai));

            dictionary.Add("OCND", ToString(OcNangDong));
            dictionary.Add("OCPT", ToString(OcPhanTich));
            dictionary.Add("THIENHUONG", ToString(ThienHuong));

            dictionary.Add("TMNT", ToString(SelfScore));
            dictionary.Add("TMVD", ToString(BodyScore));
            dictionary.Add("TMNN", ToString(WordScore));
            dictionary.Add("TMTN", ToString(NatureScore));
            dictionary.Add("TMTH", ToString(LogicScore));
            dictionary.Add("TMAN", ToString(MusicScore));
            dictionary.Add("TMTG", ToString(PictureScore));
            dictionary.Add("TMXH", ToString(PeopleScore));

            dictionary.Add("TMNT_RE", ToString(SelfXepLoai));
            dictionary.Add("TMVD_RE", ToString(BodyXepLoai));
            dictionary.Add("TMNN_RE", ToString(WordXepLoai));
            dictionary.Add("TMTN_RE", ToString(NatureXepLoai));
            dictionary.Add("TMTH_RE", ToString(LogicXepLoai));
            dictionary.Add("TMAN_RE", ToString(MusicXepLoai));
            dictionary.Add("TMTG_RE", ToString(PictureXepLoai));
            dictionary.Add("TMXH_RE", ToString(PeopleXepLoai));

            dictionary.Add("100EQ", ToString(EQ));
            dictionary.Add("100IQ", ToString(IQ));
            dictionary.Add("100AQ", ToString(AQ));
            dictionary.Add("100CQ", ToString(CQ));

            dictionary.Add("EQVP", ToString(EQ));
            dictionary.Add("IQVP", ToString(IQ));
            dictionary.Add("AQVP", ToString(AQ));
            dictionary.Add("CQVP", ToString(CQ));

            dictionary.Add("RCVP", ToString(OcVisual));
            dictionary.Add("RCHP", ToString(OcAuditory));
            dictionary.Add("RCAP", ToString(OcKinesthic));

            dictionary.Add("L1RCP", ToString(L1RCP));
            dictionary.Add("L2RCP", ToString(L2RCP));
            dictionary.Add("L3RCP", ToString(L3RCP));
            dictionary.Add("L4RCP", ToString(L4RCP));
            dictionary.Add("L5RCP", ToString(L5RCP));
            dictionary.Add("R1RCP", ToString(R1RCP));
            dictionary.Add("R2RCP", ToString(R2RCP));
            dictionary.Add("R3RCP", ToString(R3RCP));
            dictionary.Add("R4RCP", ToString(R4RCP));
            dictionary.Add("R5RCP", ToString(R5RCP));
            dictionary.Add("Language_Internalization", ToString(Language_Internalization));
            dictionary.Add("Language_Ex", ToString(Language_Expression));

            dictionary.Add("TOP1F", ToString(Top1NGFName));
            dictionary.Add("TOP2F", ToString(Top2NGFName));
            dictionary.Add("TOP3F", ToString(Top3NGFName));

            dictionary.Add("TOP1HS", ToString(Top1NGFValue));
            dictionary.Add("TOP2HS", ToString(Top2NGFValue));
            dictionary.Add("TOP3HS", ToString(Top3NGFValue));

            dictionary.Add("TOP1NHOM", ToString(Top1NGFMoTaNgan));
            dictionary.Add("TOP2NHOM", ToString(Top2NGFMoTaNgan));
            dictionary.Add("TOP3NHOM", ToString(Top3NGFMoTaNgan));

            dictionary.Add("TOP1NHANXET", ToString(Top1NGFMoTaNgan));
            dictionary.Add("TOP2NHANXET", ToString(Top2NGFMoTaNgan));
            dictionary.Add("TOP3NHANXET", ToString(Top3NGFMoTaNgan));

            dictionary.Add("TOP1MOTA_TITLE", ToString(Top1NGFMoTaNgan));
            dictionary.Add("TOP2MOTA_TITLE", ToString(Top2NGFMoTaNgan));
            dictionary.Add("TOP3MOTA_TITLE", ToString(Top3NGFMoTaNgan));

            dictionary.Add("TOP1MOTA", ToString(Top1NGFMoTaDai));
            dictionary.Add("TOP2MOTA", ToString(Top2NGFMoTaDai));
            dictionary.Add("TOP3MOTA", ToString(Top3NGFMoTaDai));

            dictionary.Add("EQ", ToString(EQ_Intelligence));
            dictionary.Add("Concentration", ToString(Concentration_Intelligence));
            dictionary.Add("ImageAppreciation", ToString(Image_Appreciation));
            dictionary.Add("Observation", ToString(Observation_Intelligence));
            dictionary.Add("MusicAppreciation", ToString(Music_Appreciation));
            dictionary.Add("Language", ToString(Language_Internalization));
            dictionary.Add("Artist", ToString(Artist_Intelligence));
            dictionary.Add("Operation", ToString(Operation_Intelligence));
            dictionary.Add("Kinesthetic", ToString(Kinesthetic_Intelligence));
            dictionary.Add("LanguageExpression", ToString(Language_Expression));
            dictionary.Add("SpatialMentalImage", ToString(Spatial_Mental_Image));
            dictionary.Add("LogicalReasoning", ToString(Logical_Reasoning));
            dictionary.Add("CreationImagination", ToString(Creation_Imagination));
            dictionary.Add("Intrapersonal", ToString(Intrapersonal_Intelligence));
            dictionary.Add("PlanningJudgment", ToString(Planning_Judgment));
            dictionary.Add("Interpersonal", ToString(Interpersonal_Intelligence));

            dictionary.Add("ChiSo1", ToString(CNTT));
            dictionary.Add("ChiSo2", ToString(KyThuat));
            dictionary.Add("ChiSo3", ToString(YTe));
            dictionary.Add("ChiSo4", ToString(TaiChinhKeToan));

            dictionary.Add("ChiSo5", ToString(DuLich));
            dictionary.Add("ChiSo6", ToString(QTDH));
            dictionary.Add("ChiSo7", ToString(MoiTruong));
            dictionary.Add("ChiSo8", ToString(ThietKeHoiHoa));

            dictionary.Add("ChiSo9", ToString(NgheThuat));
            dictionary.Add("ChiSo10", ToString(XayDungKienTruc));
            dictionary.Add("ChiSo11", ToString(TheThao));
            dictionary.Add("ChiSo12", ToString(XaHoiHoc));
            dictionary.Add("ChiSo13", ToString(TruyenThong));
            dictionary.Add("ChiSo14", ToString(NgonNgu));
            dictionary.Add("ChiSo15", ToString(LuatSu));
            dictionary.Add("ChiSo16", ToString(GiaoDuc));
            dictionary.Add("ChiSo17", ToString(SaleMarketing));
            dictionary.Add("ChiSo18", ToString(CheTacThuCong));
            dictionary.Add("ChiSo19", ToString(GiamSatChatLuong));
            dictionary.Add("ChiSo20", ToString(DaoTao));
            dictionary.Add("ChiSo21", ToString(KinhDoanh));
            dictionary.Add("ChiSo22", ToString(NgoaiNgu));
            dictionary.Add("ChiSo23", ToString(AmNhac));
            dictionary.Add("ChiSo24", ToString(TamLy));

            dictionary.Add("TinhCachHanhViText", ToString(TinhCachHanhViText));

            dictionary.Add("DacTinhTiemThucTitle", ToString(DacTinhTiemThucText));
            dictionary.Add("DacTinhTiemThucMoTa", ToString(DacTinhTiemThucTextMoTa));

            dictionary.Add("DacTinhTuDuyTitle", ToString(DacTinhTuDuyText));
            dictionary.Add("DacTinhTuDuyMoTa", ToString(DacTinhTuDuyTextMoTa));

            dictionary.Add("KhuyenNghiTitle", ToString(KhuyenNghiHoaHopText));
            dictionary.Add("KhuyenNghiMoTa", ToString(KhuyenNghiHoaHopTextMoTa));

            dictionary.Add("Top1_16", ToString(Top_1_16));
            dictionary.Add("Top1_16_Des", ToString(Top_1_16_Des));

            dictionary.Add("RANK_WORK", ToString(GetRankCongViec()));
            dictionary.Add("RANK_LOVE", ToString(GetRankTinhYeuXaHoi()));
            dictionary.Add("RANK_WISDOM", ToString(GetRankThongThai()));       

            dictionary.Add("Top2_16", ToString(Top_2_16));
            dictionary.Add("Top2_16_Des", ToString(Top_2_16_Des));

            dictionary.Add("Top3_16", ToString(Top_3_16));
            dictionary.Add("Top3_16_Des", ToString(Top_3_16_Des));

            dictionary.Add("Top4_16", ToString(Top_4_16));
            dictionary.Add("Top4_16_Des", ToString(Top_4_16_Des));

            dictionary.Add("TOP1_CA", ToString(Top1_CA));
            dictionary.Add("TOP2_CA", ToString(Top2_CA));
            dictionary.Add("TOP3_CA", ToString(Top3_CA));
            dictionary.Add("TOP4_CA", ToString(Top4_CA));
            return dictionary;
        }

        //public Dictionary<string, Image> ToDictionaryImage()
        //{
        //    Dictionary<string, Image> dictionary = new Dictionary<string, Image>();

        //    dictionary.Add("RANK_CONGNGHE", CNTT);
        //    dictionary.Add("RANK_KYSU", KyThuat);
        //    dictionary.Add("RANK_YKHOA", YTe);
        //    dictionary.Add("RANK_QUANTRI", QTKD);
        //    dictionary.Add("RANK_TAICHINH", TaiChinh_KeToan);
        //    dictionary.Add("RANK_GIAMSAT", CNDV);
        //    dictionary.Add("RANK_DULICH", XaHoiHoa_MoiTruong);
        //    dictionary.Add("RANK_THIETKEDOHOA", ThietKe);
        //    dictionary.Add("RANK_AMNHAC", NgheThuat);
        //    dictionary.Add("RANK_KIENTRUC", XayDungKienTruc);
        //    dictionary.Add("RANK_THETHAO", TheThao);
        //    dictionary.Add("RANK_CHETAC", ThuCongMyNghe);
        //    dictionary.Add("RANK_XAHOI", ChinhTri_NgoaiGiao);
        //    dictionary.Add("RANK_PHAPLUAT", VuTrang);
        //    dictionary.Add("RANK_BAOCHI", Van_Su_Triet);
        //    dictionary.Add("RANK_TAMLY", TamLy);
        //    dictionary.Add("RANK_GIAODUC", GiaoDuc);
        //    dictionary.Add("RANK_MOITRUONG", XaHoiHoa_MoiTruong);
        //    dictionary.Add("RANK_TRUYENTHONG", TruyenThong);
        //    dictionary.Add("RANK_KINHTE", Sale_Marketing);
        //    dictionary.Add("RANK_NGONNGU", NgoaiNgu);

        //    return dictionary;
        //}

        private void BuildAnalysisEntity(ref FIFingerAnalysis AnalysisEntity)
        {
            AnalysisEntity.LT_BLS = this.NaoTrai;
            AnalysisEntity.RT_BLS = this.NaoPhai;

            AnalysisEntity.SAP = this.ThuyTruocTran;
            AnalysisEntity.SBP = this.ThuyTran;
            AnalysisEntity.SCP = this.ThuyDinh;
            AnalysisEntity.SDP = this.ThuyThaiDuong;
            AnalysisEntity.SER = this.ThuyCham;

            AnalysisEntity.TFRC = this.TFRC;
            AnalysisEntity.Page_BLS = this.AFRC;

            AnalysisEntity.EQVP = this.EQ;
            AnalysisEntity.IQVP = this.IQ;
            AnalysisEntity.AQVP = this.AQ;
            AnalysisEntity.CQVP = this.CQ;

            AnalysisEntity.L1RCP = this.L1RCP;
            AnalysisEntity.L2RCP = this.L2RCP;
            AnalysisEntity.L3RCP = this.L3RCP;
            AnalysisEntity.L4RCP = this.L4RCP;
            AnalysisEntity.L5RCP = this.L5RCP;
            AnalysisEntity.R1RCP = this.R1RCP;
            AnalysisEntity.R2RCP = this.R2RCP;
            AnalysisEntity.R3RCP = this.R3RCP;
            AnalysisEntity.R4RCP = this.R4RCP;
            AnalysisEntity.R5RCP = this.R5RCP;

            //AnalysisEntity.L1RCP1 = this.L1RCP1;
            //AnalysisEntity.L2RCP2 = this.L2RCP2;
            //AnalysisEntity.L3RCP3 = this.L3RCP3;
            //AnalysisEntity.L4RCP4 = this.L4RCP4;
            //AnalysisEntity.L5RCP5 = this.L5RCP5;
            //AnalysisEntity.R1RCP1 = this.R1RCP1;
            //AnalysisEntity.R2RCP2 = this.R2RCP2;
            //AnalysisEntity.R3RCP3 = this.R3RCP3;
            //AnalysisEntity.R4RCP4 = this.R4RCP4;
            //AnalysisEntity.R5RCP5 = this.R5RCP5;
            //AnalysisEntity.ATDR = (decimal)this.ATDR;
            //AnalysisEntity.ATDL = (decimal)this.ATDL;
            //AnalysisEntity.ATDRA = this.ATDRA;
            //AnalysisEntity.ATDRS = this.ATDRS;
            //AnalysisEntity.ATDLA = this.ATDLA;
            //AnalysisEntity.ATDLS = this.ATDLS;
            AnalysisEntity.AC2WP = this.PhongCachNhanThuc;
            AnalysisEntity.AC2UP = this.PhongCachLapLuan;
            AnalysisEntity.AC2RP = this.PhongCachHocHoi;
            AnalysisEntity.AC2XP = this.PhongCachPhanLoai;
            //AnalysisEntity.AC2SP = this.AC2SP;
            AnalysisEntity.RCAP = this.OcVisual;
            AnalysisEntity.RCHP = this.OcAuditory;
            AnalysisEntity.RCVP = this.OcKinesthic;
            AnalysisEntity.T1AP = this.OcNangDong;
            AnalysisEntity.T1BP = this.OcPhanTich;

            AnalysisEntity.M1Q = this.MQ_Word;
            AnalysisEntity.M2Q = this.MQ_Logic;
            AnalysisEntity.M3Q = this.MQ_Self;
            AnalysisEntity.M4Q = this.MQ_Body;
            AnalysisEntity.M5Q = this.MQ_People;
            AnalysisEntity.M6Q = this.MQ_Music;
            AnalysisEntity.M7Q = this.MQ_Picture;
            AnalysisEntity.M8Q = this.MQ_Nature;

            //AnalysisEntity.L1RCB = this.L1RCB;
            //AnalysisEntity.L2RCB = this.L2RCB;
            //AnalysisEntity.L3RCB = this.L3RCB;
            //AnalysisEntity.L4RCB = this.L4RCB;
            //AnalysisEntity.L5RCB = this.L5RCB;
            //AnalysisEntity.R1RCB = this.R1RCB;
            //AnalysisEntity.R2RCB = this.R2RCB;
            //AnalysisEntity.R3RCB = this.R3RCB;
            //AnalysisEntity.R4RCB = this.R4RCB;
            //AnalysisEntity.R5RCB = this.R5RCB;
            //AnalysisEntity.Label1 = this.Label1;
            //AnalysisEntity.Label2 = this.Label2;
            //AnalysisEntity.Label3 = this.Label3;
            //AnalysisEntity.Label4 = this.Label4;
            //AnalysisEntity.Label5 = this.Label5;
            //AnalysisEntity.Label6 = this.Label6;
            AnalysisEntity.L1T = this.L1T;
            AnalysisEntity.L2T = this.L2T;
            AnalysisEntity.L3T = this.L3T;
            AnalysisEntity.L4T = this.L4T;
            AnalysisEntity.L5T = this.L5T;
            AnalysisEntity.R1T = this.R1T;
            AnalysisEntity.R2T = this.R2T;
            AnalysisEntity.R3T = this.R3T;
            AnalysisEntity.R4T = this.R4T;
            AnalysisEntity.R5T = this.R5T;

            //AnalysisEntity.S1BZ = this.S1BZ;
            //AnalysisEntity.S2BZ = this.S2BZ;
            //AnalysisEntity.S3BZ = this.S3BZ;
            //AnalysisEntity.S4BZ = this.S4BZ;
            //AnalysisEntity.S5BZ = this.S5BZ;
            //AnalysisEntity.S6BZ = this.S6BZ;
            //AnalysisEntity.S7BZ = this.S7BZ;
            //AnalysisEntity.S8BZ = this.S8BZ;
            //AnalysisEntity.S9BZ = this.S9BZ;
            //AnalysisEntity.S10BZ = this.S10BZ;
            AnalysisEntity.S11BZ = this.Top1NGFValue;
            AnalysisEntity.S12BZ = this.Top2NGFValue;
            AnalysisEntity.S13BZ = this.Top3NGFValue;
            //AnalysisEntity.S14BZ = this.A;
            //AnalysisEntity.S15BZ = this.C;
            //AnalysisEntity.S16BZ = this.D;
            //AnalysisEntity.S17BZ = this.I;
            //AnalysisEntity.S18BZ = this.S18BZ;

            //AnalysisEntity.S1BZZ = this.S1BZZ;
            //AnalysisEntity.S2BZZ = this.S2BZZ;
            //AnalysisEntity.S3BZZ = this.S3BZZ;
            //AnalysisEntity.S4BZZ = this.S4BZZ;
            //AnalysisEntity.S5BZZ = this.S5BZZ;
            //AnalysisEntity.S6BZZ = this.S6BZZ;
            //AnalysisEntity.S7BZZ = this.S7BZZ;
            //AnalysisEntity.S8BZZ = this.S8BZZ;
            //AnalysisEntity.S9BZZ = this.S9BZZ;
            //AnalysisEntity.S10BZZ = this.S10BZZ;
            AnalysisEntity.S11BZZ = this.Top1NGFName;
            AnalysisEntity.S12BZZ = this.Top2NGFName;
            AnalysisEntity.S13BZZ = this.Top3NGFName;
            //AnalysisEntity.S14BZZ = this.S14BZZ;
            //AnalysisEntity.S15BZZ = this.S15BZZ;
            //AnalysisEntity.S16BZZ = this.S16BZZ;
            //AnalysisEntity.S17BZZ = this.S17BZZ;
            //AnalysisEntity.S18BZZ = this.S18BZZ;

            AnalysisEntity.M1QPR = this.MQModelCollection.Word.Rank;
            AnalysisEntity.M2QPR = this.MQModelCollection.Logic.Rank;
            AnalysisEntity.M3QPR = this.MQModelCollection.Self.Rank;
            AnalysisEntity.M4QPR = this.MQModelCollection.Body.Rank;
            AnalysisEntity.M5QPR = this.MQModelCollection.People.Rank;
            AnalysisEntity.M6QPR = this.MQModelCollection.Music.Rank;
            AnalysisEntity.M7QPR = this.MQModelCollection.Picture.Rank;
            AnalysisEntity.M8QPR = this.MQModelCollection.Nature.Rank;
        }

        private void BuildChartEntity(ref List<FIMQChart> ArrayChartEntity)
        {
            FIMQChart objChart1 = new FIMQChart
            {
                MQID = "M1Q",
                MQDesc = "语言",
                MQValue = this.MQ_Word
            };
            FIMQChart objChart2 = new FIMQChart
            {
                MQID = "M2Q",
                MQDesc = "数学",
                MQValue = this.MQ_Logic
            };
            FIMQChart objChart3 = new FIMQChart
            {
                MQID = "M3Q",
                MQDesc = "内省",
                MQValue = this.MQ_Self
            };
            FIMQChart objChart4 = new FIMQChart
            {
                MQID = "M4Q",
                MQDesc = "肢体",
                MQValue = this.MQ_Body
            };
            FIMQChart objChart5 = new FIMQChart
            {
                MQID = "M5Q",
                MQDesc = "人际",
                MQValue = this.MQ_People
            };
            FIMQChart objChart6 = new FIMQChart
            {
                MQID = "M6Q",
                MQDesc = "音乐",
                MQValue = this.MQ_Music
            };
            FIMQChart objChart7 = new FIMQChart
            {
                MQID = "M7Q",
                MQDesc = "空间",
                MQValue = this.MQ_Picture
            };
            FIMQChart objChart8 = new FIMQChart
            {
                MQID = "M8Q",
                MQDesc = "自然",
                MQValue = this.MQ_Nature
            };

            ArrayChartEntity.Add(objChart1);
            ArrayChartEntity.Add(objChart2);
            ArrayChartEntity.Add(objChart3);
            ArrayChartEntity.Add(objChart4);
            ArrayChartEntity.Add(objChart5);
            ArrayChartEntity.Add(objChart6);
            ArrayChartEntity.Add(objChart7);
            ArrayChartEntity.Add(objChart8);
        }

        #region Properties

        #region Properties For FingerPrint Ridge Count

        public int L1L { get; private set; }
        public int L1R { get; private set; }
        public string L1T { get; private set; }

        public int L2L { get; private set; }
        public int L2R { get; private set; }
        public string L2T { get; private set; }

        public int L3L { get; private set; }
        public int L3R { get; private set; }
        public string L3T { get; private set; }

        public int L4L { get; private set; }
        public int L4R { get; private set; }
        public string L4T { get; private set; }

        public int L5L { get; private set; }
        public int L5R { get; private set; }
        public string L5T { get; private set; }

        public int R1L { get; private set; }
        public int R1R { get; private set; }
        public string R1T { get; private set; }

        public int R2L { get; private set; }
        public int R2R { get; private set; }
        public string R2T { get; private set; }

        public int R3L { get; private set; }
        public int R3R { get; private set; }
        public string R3T { get; private set; }

        public int R4L { get; private set; }
        public int R4R { get; private set; }
        public string R4T { get; private set; }

        public int R5L { get; private set; }
        public int R5R { get; private set; }
        public string R5T { get; private set; }

        private void GetRidgeCount()
        {
            L1L = RecordModelCollection.L1L;
            L1R = RecordModelCollection.L1R;
            L1T = RecordModelCollection.L1T;

            L2L = RecordModelCollection.L2L;
            L2R = RecordModelCollection.L2R;
            L2T = RecordModelCollection.L2T;

            L3L = RecordModelCollection.L3L;
            L3R = RecordModelCollection.L3R;
            L3T = RecordModelCollection.L3T;

            L4L = RecordModelCollection.L4L;
            L4R = RecordModelCollection.L4R;
            L4T = RecordModelCollection.L4T;

            L5L = RecordModelCollection.L5L;
            L5R = RecordModelCollection.L5R;
            L5T = RecordModelCollection.L5T;

            R1L = RecordModelCollection.R1L;
            R1R = RecordModelCollection.R1R;
            R1T = RecordModelCollection.R1T;

            R2L = RecordModelCollection.R2L;
            R2R = RecordModelCollection.R2R;
            R2T = RecordModelCollection.R2T;

            R3L = RecordModelCollection.R3L;
            R3R = RecordModelCollection.R3R;
            R3T = RecordModelCollection.R3T;

            R4L = RecordModelCollection.R4L;
            R4R = RecordModelCollection.R4R;
            R4T = RecordModelCollection.R4T;

            R5L = RecordModelCollection.R5L;
            R5R = RecordModelCollection.R5R;
            R5T = RecordModelCollection.R5T;
        }

        #endregion

        #region Properties PI Value

        public decimal PI1 { get { return PIModelCollection.L1.PIValue; } }
        public decimal PI2 { get { return PIModelCollection.L2.PIValue; } }
        public decimal PI3 { get { return PIModelCollection.L3.PIValue; } }
        public decimal PI4 { get { return PIModelCollection.L4.PIValue; } }
        public decimal PI5 { get { return PIModelCollection.L5.PIValue; } }
        public decimal PI6 { get { return PIModelCollection.R1.PIValue; } }
        public decimal PI7 { get { return PIModelCollection.R2.PIValue; } }
        public decimal PI8 { get { return PIModelCollection.R3.PIValue; } }
        public decimal PI9 { get { return PIModelCollection.R4.PIValue; } }
        public decimal PI10 { get { return PIModelCollection.R5.PIValue; } }

        public decimal NGF1 { get { return PIModelCollection.L1.NGFValue; } }
        public decimal NGF2 { get { return PIModelCollection.L2.NGFValue; } }
        public decimal NGF3 { get { return PIModelCollection.L3.NGFValue; } }
        public decimal NGF4 { get { return PIModelCollection.L4.NGFValue; } }
        public decimal NGF5 { get { return PIModelCollection.L5.NGFValue; } }
        public decimal NGF6 { get { return PIModelCollection.R1.NGFValue; } }
        public decimal NGF7 { get { return PIModelCollection.R2.NGFValue; } }
        public decimal NGF8 { get { return PIModelCollection.R3.NGFValue; } }
        public decimal NGF9 { get { return PIModelCollection.R4.NGFValue; } }
        public decimal NGF10 { get { return PIModelCollection.R5.NGFValue; } }

        public decimal ChiSoHapThu1 { get { return PIModelCollection.L1.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu2 { get { return PIModelCollection.L2.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu3 { get { return PIModelCollection.L3.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu4 { get { return PIModelCollection.L4.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu5 { get { return PIModelCollection.L5.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu6 { get { return PIModelCollection.R1.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu7 { get { return PIModelCollection.R2.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu8 { get { return PIModelCollection.R3.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu9 { get { return PIModelCollection.R4.ChiSoHapThuViecHoc; } }
        public decimal ChiSoHapThu10 { get { return PIModelCollection.R5.ChiSoHapThuViecHoc; } }

        #endregion Proerpties PI Value

        #region Chỉ số Hại não

        //Chỉ số max đương vân tay
        public decimal TFRC { get { return PIModelCollection.TFRC; } }
        public decimal AFRC { get { return PIModelCollection.AFRC; } }
        public decimal MinusFRC { get { return AFRC - TFRC; } }

        //Não Trái Não Phải
        public decimal NaoTrai { get { return IntelligenceModelCollection.NaoTrai; } }
        public decimal NaoPhai { get { return IntelligenceModelCollection.NaoPhai; } }

        //5 thùy
        public decimal ThuyTruocTran { get { return IntelligenceModelCollection.ThuyTruocTran; } }
        public decimal ThuyTran { get { return IntelligenceModelCollection.ThuyTran; } }
        public decimal ThuyDinh { get { return IntelligenceModelCollection.ThuyDinh; } }
        public decimal ThuyThaiDuong { get { return IntelligenceModelCollection.ThuyThaiDuong; } }
        public decimal ThuyCham { get { return IntelligenceModelCollection.ThuyCham; } }

        //4 Phong cách
        public decimal PhongCachNhanThuc { get { return FingerAnalysis.FPType11; } }
        public decimal PhongCachHocHoi { get { return FingerAnalysis.FPType12; } }
        public decimal PhongCachLapLuan { get { return FingerAnalysis.FPType13; } }
        public decimal PhongCachPhanLoai { get { return FingerAnalysis.FPType14; } }

        //OcNangDong, OcPhanTich
        public decimal OcNangDong { get { return IntelligenceModelCollection.OcNangDong; } }
        public decimal OcPhanTich { get { return IntelligenceModelCollection.OcPhanTich; } }

        public string ThienHuong { get { return SoSanhBoOc(); } }

        //EQ, CQ, IQ, AQ
        public decimal EQ { get { return MQModelCollection.EQ; } }
        public decimal IQ { get { return MQModelCollection.IQ; } }
        public decimal AQ { get { return MQModelCollection.AQ; } }
        public decimal CQ { get { return MQModelCollection.CQ; } }

        //VAK
        public decimal OcVisual { get { return IntelligenceModelCollection.OcVisual; } }
        public decimal OcAuditory { get { return IntelligenceModelCollection.OcAuditory; } }
        public decimal OcKinesthic { get { return IntelligenceModelCollection.OcKinesthic; } }

        #endregion Chỉ số Hại não

        public string SoSanhBoOc()
        {
            string PhongCach = "";
            if (PhongCachNhanThuc >= PhongCachLapLuan && PhongCachNhanThuc >= PhongCachHocHoi &&
                     PhongCachNhanThuc >= PhongCachPhanLoai)
                PhongCach += "Phong Cách Nhận Thức -";
            else if (PhongCachHocHoi >= PhongCachLapLuan && PhongCachHocHoi >= PhongCachNhanThuc &&
               PhongCachHocHoi >= PhongCachPhanLoai)
                PhongCach += "Phong Cách Học Hỏi -";
            else if (PhongCachLapLuan >= PhongCachHocHoi && PhongCachLapLuan >= PhongCachNhanThuc &&
                     PhongCachLapLuan >= PhongCachPhanLoai)
                PhongCach += "Phong Cách Lập Luận -";
            else if (PhongCachPhanLoai >= PhongCachLapLuan && PhongCachPhanLoai >= PhongCachHocHoi &&
                     PhongCachPhanLoai >= PhongCachNhanThuc)
                PhongCach += "Phong Cách Phân Loại -";

            if (OcNangDong >= OcPhanTich)
                PhongCach += "Phong Cách Óc Năng Động";
            else
                PhongCach += "Phong Cách Óc Phân Tích";
            return PhongCach;
        }

        #region % 8 loại Hình thông minh

        public decimal MQ_Word { get { return Math.Round(MQModelCollection.Word.Percent, 2); } }
        public decimal MQ_Logic { get { return Math.Round(MQModelCollection.Logic.Percent, 2); } }
        public decimal MQ_Self { get { return Math.Round(MQModelCollection.Self.Percent, 2); } }
        public decimal MQ_Body { get { return Math.Round(MQModelCollection.Body.Percent, 2); } }
        public decimal MQ_People { get { return Math.Round(MQModelCollection.People.Percent, 2); } }
        public decimal MQ_Music { get { return Math.Round(MQModelCollection.Music.Percent, 2); } }
        public decimal MQ_Picture { get { return Math.Round(MQModelCollection.Picture.Percent, 2); } }
        public decimal MQ_Nature { get { return Math.Round(MQModelCollection.Nature.Percent, 2); } }

        #endregion % 8 loại Hình thông minh

        #region 8 loại Hình A+ , A, A-, B+, B, B-, C, D

        public string WordScore { get { return MQModelCollection.Word.Score; } }
        public string LogicScore { get { return MQModelCollection.Logic.Score; } }
        public string SelfScore { get { return MQModelCollection.Self.Score; } }
        public string BodyScore { get { return MQModelCollection.Body.Score; } }
        public string PeopleScore { get { return MQModelCollection.People.Score; } }
        public string MusicScore { get { return MQModelCollection.Music.Score; } }
        public string PictureScore { get { return MQModelCollection.Picture.Score; } }
        public string NatureScore { get { return MQModelCollection.Nature.Score; } }

        #endregion 8 loại Hình A+ , A, A-, B+, B, B-, C, D

        #region 8 loại Hình Excellent, Good, Average

        public string WordXepLoai { get { return MQModelCollection.Word.XepLoai; } }
        public string LogicXepLoai { get { return MQModelCollection.Logic.XepLoai; } }
        public string SelfXepLoai { get { return MQModelCollection.Self.XepLoai; } }
        public string BodyXepLoai { get { return MQModelCollection.Body.XepLoai; } }
        public string PeopleXepLoai { get { return MQModelCollection.People.XepLoai; } }
        public string MusicXepLoai { get { return MQModelCollection.Music.XepLoai; } }
        public string PictureXepLoai { get { return MQModelCollection.Picture.XepLoai; } }
        public string NatureXepLoai { get { return MQModelCollection.Nature.XepLoai; } }

        #endregion 8 loại Hình Excellent, Good, Average

        #region Top3NGF

        public string Top1NGFName { get { return Top3NgfModelCollection.Top1.FingerName; } }
        public decimal Top1NGFValue { get { return Top3NgfModelCollection.Top1.ChiSoViecHapThu; } }
        public string Top1NGFNhomNangLuc { get { return Top3NgfModelCollection.Top1.NhomNangLuc; } }
        public string Top1NGFMoTaNgan { get { return Top3NgfModelCollection.Top1.MoTaNgan; } }
        public string Top1NGFMoTaDai { get { return Top3NgfModelCollection.Top1.MoTaDai; } }

        public string Top2NGFName { get { return Top3NgfModelCollection.Top2.FingerName; } }
        public decimal Top2NGFValue { get { return Top3NgfModelCollection.Top2.ChiSoViecHapThu; } }
        public string Top2NGFNhomNangLuc { get { return Top3NgfModelCollection.Top2.NhomNangLuc; } }
        public string Top2NGFMoTaNgan { get { return Top3NgfModelCollection.Top2.MoTaNgan; } }
        public string Top2NGFMoTaDai { get { return Top3NgfModelCollection.Top2.MoTaDai; } }

        public string Top3NGFName { get { return Top3NgfModelCollection.Top3.FingerName; } }
        public decimal Top3NGFValue { get { return Top3NgfModelCollection.Top3.ChiSoViecHapThu; } }
        public string Top3NGFNhomNangLuc { get { return Top3NgfModelCollection.Top3.NhomNangLuc; } }
        public string Top3NGFMoTaNgan { get { return Top3NgfModelCollection.Top3.MoTaNgan; } }
        public string Top3NGFMoTaDai { get { return Top3NgfModelCollection.Top3.MoTaDai; } }

        #endregion

        #region Sử dụng CareerModel để tính số sao chèn ảnh.

        public String CNTT { get { return LayHinhAnhSoMatTroi(CareerModelCollection.CNTT); } }
        public String KyThuat { get { return LayHinhAnhSoMatTroi(CareerModelCollection.KyThuat); } }
        public String YTe { get { return LayHinhAnhSoMatTroi(CareerModelCollection.YTe); } }
        public String TaiChinhKeToan { get { return LayHinhAnhSoMatTroi(CareerModelCollection.TaiChinh_KeToan); } }
        public String DuLich { get { return LayHinhAnhSoMatTroi(CareerModelCollection.DuLich); } }
        public String QTDH { get { return LayHinhAnhSoMatTroi(CareerModelCollection.QTDH); } }
        public String MoiTruong { get { return LayHinhAnhSoMatTroi(CareerModelCollection.MoiTruong); } }
        public String ThietKeHoiHoa { get { return LayHinhAnhSoMatTroi(CareerModelCollection.ThietKe); } }
        public String NgheThuat { get { return LayHinhAnhSoMatTroi(CareerModelCollection.NgheThuat); } }
        public String XayDungKienTruc { get { return LayHinhAnhSoMatTroi(CareerModelCollection.XayDungKienTruc); } }
        public String TheThao { get { return LayHinhAnhSoMatTroi(CareerModelCollection.TheThao); } }
        public String XaHoiHoc { get { return LayHinhAnhSoMatTroi(CareerModelCollection.XaHoiHoc); } }
        public String TruyenThong { get { return LayHinhAnhSoMatTroi(CareerModelCollection.TruyenThong); } }
        public String NgonNgu { get { return LayHinhAnhSoMatTroi(CareerModelCollection.NgoaiNgu); } }
        public String LuatSu { get { return LayHinhAnhSoMatTroi(CareerModelCollection.ChinhTriNgoaiGiao); } }
        public String GiaoDuc { get { return LayHinhAnhSoMatTroi(CareerModelCollection.GiaoDuc); } }
        public String SaleMarketing { get { return LayHinhAnhSoMatTroi(CareerModelCollection.Sale_Marketing); } }
        public String CheTacThuCong { get { return LayHinhAnhSoMatTroi(CareerModelCollection.CheTacThuCong); } }
        public String GiamSatChatLuong { get { return LayHinhAnhSoMatTroi(CareerModelCollection.GiamSatChatLuong); } }
        public String DaoTao { get { return LayHinhAnhSoMatTroi(CareerModelCollection.DaoTao); } }
        public String KinhDoanh { get { return LayHinhAnhSoMatTroi(CareerModelCollection.KinhDoanh); } }
        public String NgoaiNgu { get { return LayHinhAnhSoMatTroi(CareerModelCollection.NgoaiNgu); } }
        public String AmNhac { get { return LayHinhAnhSoMatTroi(CareerModelCollection.AmNhac); } }
        public String TamLy { get { return LayHinhAnhSoMatTroi(CareerModelCollection.TamLy); } }
        private String LayHinhAnhSoMatTroi(CareerModel model)
        {
            int soSao = (int) model.SoLuongSao;
            String returnString = null;
            switch (soSao)
            {
                case 1:
                    returnString = @"❤";
                    break;
                case 2:
                    returnString = @"❤ ❤";
                    break;
                case 3:
                    returnString = @"❤ ❤ ❤";
                    break;
                case 4:
                    returnString = @"❤ ❤ ❤ ❤";
                    break;
                case 5:
                    returnString = @"❤ ❤ ❤ ❤ ❤";
                    break;
            }
            return returnString;
        }

        #endregion Sử dụng CareerModel để tính số sao chèn ảnh.

        #region Chart 

        public decimal L1RCP { get { return TFRC != 0 ? Math.Round(PI1/TFRC*100, 2) : 0; } }
        public decimal L2RCP { get { return TFRC != 0 ? Math.Round(PI2/TFRC*100, 2) : 0; } }
        public decimal L3RCP { get { return TFRC != 0 ? Math.Round(PI3/TFRC*100, 2) : 0; } }
        public decimal L4RCP { get { return TFRC != 0 ? Math.Round(PI4/TFRC*100, 2) : 0; } }
        public decimal L5RCP { get { return TFRC != 0 ? Math.Round(PI5/TFRC*100, 2) : 0; } }
        public decimal R1RCP { get { return TFRC != 0 ? Math.Round(PI6/TFRC*100, 2) : 0; } }
        public decimal R2RCP { get { return TFRC != 0 ? Math.Round(PI7/TFRC*100, 2) : 0; } }
        public decimal R3RCP { get { return TFRC != 0 ? Math.Round(PI8/TFRC*100, 2) : 0; } }
        public decimal R4RCP { get { return TFRC != 0 ? Math.Round(PI9/TFRC*100, 2) : 0; } }
        public decimal R5RCP { get { return TFRC != 0 ? Math.Round(PI10/TFRC*100, 2) : 0; } }

        #endregion

        #region 16 Loại vận động

        public decimal EQ_Intelligence { get { return VanDongModel.EQIntelligent; } }
        public decimal Concentration_Intelligence { get { return VanDongModel.Concentration; } }
        public decimal Image_Appreciation { get { return VanDongModel.ImageAppreciation; } }
        public decimal Observation_Intelligence { get { return VanDongModel.Observation; } }
        public decimal Music_Appreciation { get { return VanDongModel.Music; } }
        public decimal Language_Internalization { get { return VanDongModel.LanguageInternalization; } }
        public decimal Artist_Intelligence { get { return VanDongModel.Artistic; } }
        public decimal Operation_Intelligence { get { return VanDongModel.Operation; } }
        public decimal Kinesthetic_Intelligence { get { return VanDongModel.Kinesthetic; } }
        public decimal Language_Expression { get { return VanDongModel.Language; } }
        public decimal Spatial_Mental_Image { get { return VanDongModel.Spartial; } }
        public decimal Logical_Reasoning { get { return VanDongModel.Logical; } }
        public decimal Creation_Imagination { get { return VanDongModel.Creation; } }
        public decimal Intrapersonal_Intelligence { get { return VanDongModel.Intrapersonal; } }
        public decimal Planning_Judgment { get { return VanDongModel.Planning; } }
        public decimal Interpersonal_Intelligence { get { return VanDongModel.Interpersonal; } }

        #endregion 16 Loại vận động

        #region A-C-I-D Test

        //public decimal A { get { return ACIDModel.A; } }
        //public decimal C { get { return ACIDModel.C; } }
        //public decimal I { get { return ACIDModel.I; } }
        //public decimal D { get { return ACIDModel.D; } }

        #endregion A-C-D-I Test

        #region Tính cách hành vi

        public string TinhCachHanhViTitle
        {
            get
            {
                return TinhCachHanhViModel.Title;
                ;
            }
        }

        public string TinhCachHanhViText
        {
            get
            {
                return TinhCachHanhViModel.Content;
                ;
            }
        }

        #endregion Tính cách hành vi

        #region Đặc tính tư duy - tiềm thức

        public string DacTinhTiemThucText { get { return DacTinhTiemThucModel.TenNhomEnglish; } }
        public string DacTinhTiemThucTextMoTa { get { return DacTinhTiemThucModel.MoTa; } }

        public string DacTinhTuDuyText { get { return DacTinhTuduyModel.TenNhomEnglish; } }
        public string DacTinhTuDuyTextMoTa { get { return DacTinhTuduyModel.MoTa; } }

        public string KhuyenNghiHoaHopText { get { return KhuyenNghiModel.TenNhomEnglish; } }
        public string KhuyenNghiHoaHopTextMoTa { get { return KhuyenNghiModel.MoTa; } }

        #endregion Đặc tính tư duy - tiềm thức

        #region Hành Trình Cuộc Sống
        public string GetRankThongThai()
        {
            string rank = "1";
            //if (HanhTrinhCuocSongModel.Value1 == "THÔNG THÁI & BÌNH AN")
            //    rank =  HanhTrinhCuocSongModel.Rank1.ToString();
            //if (HanhTrinhCuocSongModel.Value2 == "THÔNG THÁI & BÌNH AN")
            //    rank = HanhTrinhCuocSongModel.Rank2.ToString();
            //if (HanhTrinhCuocSongModel.Value3== "THÔNG THÁI & BÌNH AN")
            //    rank = HanhTrinhCuocSongModel.Rank3.ToString();
            rank = HanhTrinhCuocSongModel.HangThongThaiVaBinhAn.ToString();
            return rank;
        }
        public string GetRankCongViec()
        {
            string rank = "1";
            //if (HanhTrinhCuocSongModel.Value1 == "CÔNG VIỆC - KẾT QUẢ")
            //    rank = HanhTrinhCuocSongModel.Rank1.ToString();
            //if (HanhTrinhCuocSongModel.Value2 == "CÔNG VIỆC - KẾT QUẢ")
            //    rank = HanhTrinhCuocSongModel.Rank2.ToString();
            //if (HanhTrinhCuocSongModel.Value3 == "CÔNG VIỆC - KẾT QUẢ")
            //    rank = HanhTrinhCuocSongModel.Rank3.ToString();
            rank = HanhTrinhCuocSongModel.HangCongViecKetQua.ToString();
            return rank;
        }
        public string GetRankTinhYeuXaHoi()
        {
            string rank = "1";
            //if (HanhTrinhCuocSongModel.Value1 == "TÌNH YÊU & XÃ HỘI")
            //    rank = HanhTrinhCuocSongModel.Rank1.ToString();
            //if (HanhTrinhCuocSongModel.Value2 == "TÌNH YÊU & XÃ HỘI")
            //    rank = HanhTrinhCuocSongModel.Rank2.ToString();
            //if (HanhTrinhCuocSongModel.Value3 == "TÌNH YÊU & XÃ HỘI")
            //    rank = HanhTrinhCuocSongModel.Rank3.ToString();
            rank = HanhTrinhCuocSongModel.HangTinhYeuVaXaHoi.ToString();
            return rank;
        }
        public int HanhTrinhRank1 { get { return HanhTrinhCuocSongModel.Rank1; } }
        public string HanhTrinhValue1 { get { return HanhTrinhCuocSongModel.Value1; } }

        public int HanhTrinhRank2 { get { return HanhTrinhCuocSongModel.Rank2; } }
        public string HanhTrinhValue2 { get { return HanhTrinhCuocSongModel.Value2; } }

        public int HanhTrinhRank3 { get { return HanhTrinhCuocSongModel.Rank3; } }
        public string HanhTrinhValue3 { get { return HanhTrinhCuocSongModel.Value3; } }

        #endregion Hành Trình Cuộc sống

        #region Top3VanDong16Loai
        public string Top_1_16 { get { return Top3VanDongCollection.First.TenNangLuc; } }
        public string Top_1_16_Des { get { return Top3VanDongCollection.First.Description; } }

        public string Top_2_16 { get { return Top3VanDongCollection.Second.TenNangLuc; } }
        public string Top_2_16_Des { get { return Top3VanDongCollection.Second.Description; } }

        public string Top_3_16 { get { return Top3VanDongCollection.Third.TenNangLuc; } }
        public string Top_3_16_Des { get { return Top3VanDongCollection.Third.Description; } }
        public string Top_4_16 { get { return Top3VanDongCollection.Fourth.TenNangLuc; } }
        public string Top_4_16_Des { get { return Top3VanDongCollection.Fourth.Description; } }
        #endregion

        #region Top4NgheNghiep
        public string Top1_CA { get { return CareerModelCollection.Top1.Name; } }
        public string Top2_CA { get { return CareerModelCollection.Top2.Name; } }
        public string Top3_CA { get { return CareerModelCollection.Top3.Name; } }
        public string Top4_CA { get { return CareerModelCollection.Top4.Name; } }

        #endregion Top4NgheNghiep

        #endregion



    }
}