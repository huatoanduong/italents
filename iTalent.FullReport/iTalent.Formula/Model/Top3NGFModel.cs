﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Formula.Model.ExcelTemplate;

namespace iTalent.Formula.Model
{
    public class Top3NGFModel
    {
        /// <summary>
        /// WS, WC, UL...
        /// </summary>
        public string FingerType { get; set; }

        /// <summary>
        /// L1, L2, R1...
        /// </summary>
        public string FingerName { get; set; }

        public decimal NGFValue { get; set; }

        public decimal PIValue { get; set; }
        public decimal ChiSoViecHapThu { get; set; }
        public string NhomNangLuc { get; set; }

        public string MoTaNgan { get; set; }

        public string MoTaDai { get; set; }
    }

    public class Top3NGFModelCollection
    {
        protected PIModelCollection PICollection { get; set; }
        protected ExcelHeSoTiemNangCollection TiemNangCollection { get; set; }

        public List<Top3NGFModel> Collection { get; set; }

        public Top3NGFModel Top1 { get { return Collection[0]; } }
        public Top3NGFModel Top2 { get { return Collection[1]; } }
        public Top3NGFModel Top3 { get { return Collection[2]; } }

        public Top3NGFModelCollection(PIModelCollection piCollection, ExcelHeSoTiemNangCollection tiemNangCollection)
        {
            this.PICollection = piCollection;
            this.TiemNangCollection = tiemNangCollection;
            MapModel();
            Calculate();
        }

        public void MapModel()
        {
            Collection = new List<Top3NGFModel>();
            Top3NGFModel model;
            ExcelHeSoTiemNangModel tiemNangModel;

            foreach (PIModel piModel in PICollection.Collection)
            {
                tiemNangModel = this.TiemNangCollection.FindByName(piModel.FingerName);
                model = new Top3NGFModel()
                {
                    NGFValue = piModel.NGFValue,
                    PIValue = piModel.PIValue,
                    ChiSoViecHapThu = piModel.ChiSoHapThuViecHoc,
                    FingerType = piModel.FingerType,
                    FingerName = piModel.FingerName,
                    MoTaNgan = (tiemNangModel == null) ? null : tiemNangModel.MoTaNgan,
                    MoTaDai = (tiemNangModel == null) ? null : tiemNangModel.MoTaDai,
                };
                Collection.Add(model);
            }
        }

        public void Calculate()
        {
            Collection = Collection.OrderByDescending(p => p.ChiSoViecHapThu).ThenBy(p => p.PIValue).ToList();
        }

        //public void MapModel()
        //{
        //    List<PIModel> piModels = PICollection.Collection.OrderBy(p => p.Rank).ToList();

        //    if (Collection == null)
        //    {
        //        Collection = new List<Top3NGFModel>();
        //    }
        //    Collection.Clear();
        //    Top3NGFModel model;
        //    PIModel piModel;
        //    ExcelHeSoTiemNangModel tiemNangModel;

        //    piModel = piModels[0];
        //    tiemNangModel = this.TiemNangCollection.FindByName(piModel.FingerName);
        //    model = new Top3NGFModel()
        //            {
        //                NGFValue = piModel.NGFValue,
        //                FingerType = piModel.FingerType,
        //                FingerName = piModel.FingerName,
        //                MoTaNgan = (tiemNangModel == null) ? null : tiemNangModel.MoTaNgan,
        //                MoTaDai = (tiemNangModel == null) ? null : tiemNangModel.MoTaDai,
        //            };
        //    Collection.Add(model);
        //    Top1 = model;

        //    piModel = piModels[1];
        //    tiemNangModel = this.TiemNangCollection.FindByName(piModel.FingerName);
        //    model = new Top3NGFModel()
        //            {
        //                NGFValue = piModel.NGFValue,
        //                FingerType = piModel.FingerType,
        //                FingerName = piModel.FingerName,
        //                MoTaNgan = (tiemNangModel == null) ? "" : tiemNangModel.MoTaNgan,
        //                MoTaDai = (tiemNangModel == null) ? "" : tiemNangModel.MoTaDai,
        //            };
        //    Collection.Add(model);
        //    Top2 = model;

        //    piModel = piModels[2];
        //    tiemNangModel = this.TiemNangCollection.FindByName(piModel.FingerName);
        //    model = new Top3NGFModel()
        //            {
        //                NGFValue = piModel.NGFValue,
        //                FingerType = piModel.FingerType,
        //                FingerName = piModel.FingerName,
        //                MoTaNgan = (tiemNangModel == null) ? "" : tiemNangModel.MoTaNgan,
        //                MoTaDai = (tiemNangModel == null) ? "" : tiemNangModel.MoTaDai,
        //            };
        //    Collection.Add(model);
        //    Top3 = model;
        //}

    }
}