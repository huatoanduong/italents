﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace iTalent.Formula.Model
{
    public class FingerImageModel
    {
        ////Singleton
        //private static FingerImageModel instance;
        ////Lazy Loading Managed
        //public static FingerImageModel Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = new FingerImageModel();
        //            instance.CheckSpecial();
        //        }
        //        else
        //        {
        //            instance.CheckSpecial();
        //        }
        //        return instance;
        //    }
        //}
        public string SaveImagePath { get; set; }
        public string ReportID { get; set; }
        public string UnsignCharCustomerName { get; set; }

        private string Front { get { return "F"; } }
        private string ImagePathMain { get { return SaveImagePath + @"\"; } }

        public Dictionary<string, Image> ToDictionary()
        {
            Dictionary<string, Image> dictionary = new Dictionary<string, Image>();
            dictionary.Add("Face_Pic", Face_image);
            dictionary.Add("L1T", L1FImage);
            dictionary.Add("L2T", L2FImage);
            dictionary.Add("L3T", L3FImage);
            dictionary.Add("L4T", L4FImage);
            dictionary.Add("L5T", L5FImage);

            dictionary.Add("R1T", R1FImage);
            dictionary.Add("R2T", R2FImage);
            dictionary.Add("R3T", R3FImage);
            dictionary.Add("R4T", R4FImage);
            dictionary.Add("R5T", R5FImage);

            return dictionary;
        }
        public Image Face_image { get { return this.GetImageFace("F"); } }
        public Image L1FImage { get { return this.GetImageF("1_1"); } }
        public Image L2FImage { get { return this.GetImageF("2_1"); } }
        public Image L3FImage { get { return this.GetImageF("3_1"); } }
        public Image L4FImage { get { return this.GetImageF("4_1"); } }
        public Image L5FImage { get { return this.GetImageF("5_1"); } }
        public Image R1FImage { get { return this.GetImageF("6_1"); } }
        public Image R2FImage { get { return this.GetImageF("7_1"); } }
        public Image R3FImage { get { return this.GetImageF("8_1"); } }
        public Image R4FImage { get { return this.GetImageF("9_1"); } }
        public Image R5FImage { get { return this.GetImageF("10_1"); } }

        protected Image GetImageF(string FingerType)
        {
            string path = "";
            Image image = new Bitmap(320, 480);
            path = this.ImagePathMain + ReportID + "_" + this.UnsignCharCustomerName + @"\";
            if (!Directory.Exists(path))
            {
                return new Bitmap(320, 480);
            }

            path += FingerType + ".b";
            if (File.Exists(path))
            {
                image = Image.FromFile(path);
            }
            //if (File.Exists(path) && CheckImage(path))
            //{
            //    image = Image.FromFile(path);
            //}
            return image;
        }
        protected Image GetImageFace(string FingerType)
        {
            string path = "";
            Image image = global::iTalent.Formula.Properties.Resources.user;//new Bitmap(320, 480);
            path = this.ImagePathMain + ReportID + "_" + this.UnsignCharCustomerName + @"\";
            if (!Directory.Exists(path))
            {
                //return new Bitmap(320, 480);
                return image;//new Bitmap(global::iTalent.Formula.Properties.Resources.user);
            }
            else
            {
                path += this.ReportID + FingerType + ".bmp";
                if (File.Exists(path))
                {
                    image = Image.FromFile(path);
                }
            }
            return image;
        }

        protected bool CheckImageBit(Image img)
        {
            //Image img = bmp;
            PixelFormat pixel = img.PixelFormat;
            return pixel == PixelFormat.Format24bppRgb || pixel == PixelFormat.Format8bppIndexed;
        }

        protected bool CheckImageKb(string pathImg)
        {
            var fileLength = new FileInfo(pathImg).Length;
            return fileLength <= 250000;
        }

        protected bool CheckImage(string path)
        {
            string name = Path.GetFileName(path);
            if (name != null &&
                (name.Contains("1_1.b") || name.Contains("1_2.b") || name.Contains("1_3.b") || name.Contains("2_1.b") ||
                 name.Contains("2_2.b") || name.Contains("2_3.b") || name.Contains("3_1.b") || name.Contains("3_2.b") ||
                 name.Contains("3_3.b") || name.Contains("4_1.b") || name.Contains("4_2.b") || name.Contains("4_3.b") ||
                 name.Contains("5_1.b") || name.Contains("5_2.b") || name.Contains("5_3.b") || name.Contains("6_1.b") ||
                 name.Contains("6_2.b") || name.Contains("6_3.b") || name.Contains("7_1.b") || name.Contains("7_2.b") ||
                 name.Contains("7_3.b") || name.Contains("8_1.b") || name.Contains("8_2.b") || name.Contains("8_3.b") ||
                 name.Contains("9_1.b") || name.Contains("9_2.b") || name.Contains("9_3.b") || name.Contains("10_1.b") ||
                 name.Contains("10_2.b") || name.Contains("10_3.b")))
            {
                //string p = Path.ChangeExtension(path, ".bmp");
                Image bmp = Image.FromFile(path);
                return CheckImageBit(bmp) && CheckImageKb(path);
            }
            return false;
        }
    }

}
