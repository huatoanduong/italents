﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model
{
    public class MQModel
    {
        public decimal Value { get; set; }
        public int Rank { get; set; }
        public bool IsContainArch { get; set; }
        public decimal Percent { get { return this.Value / Collection.TotalMQValue * 100m; } }

        /// <summary>
        /// A+, A-,.... C,... D
        /// </summary>
        public string Score
        {
            get
            {
                //if (Value > 15) return "A+";
                //if (Value > 13) return "A";
                //if (Value > 12) return "A-";
                //if (Value > 11) return "B+";
                //if (Value > 10) return "B";
                //if (Value > 9) return "B-";
                //if (Value > 7) return "C";
                //return "D";
                decimal valueCompare = Value / 2;
                if (valueCompare > 12.2m) return "A+";
                if (valueCompare > 10.5m) return "A";
                if (valueCompare > 8.99m) return "A-";
                if (valueCompare > 7.99m) return "B+";
                if (valueCompare > 6.5m) return "B";
                if (valueCompare > 5.2m) return "B-";
                if (valueCompare > 4.99m) return "C";
                return "D";
            }
        }

        public string XepLoai
        {
            get
            {
                decimal valueCompare = Value / 2;
                if (valueCompare > 12.2m) return "EXCELLENT +";
                if (valueCompare > 10.5m) return "EXCELLENT"; //"Very Good";
                if (valueCompare > 8.99m) return "VERY GOOD"; //"Good";
                if (valueCompare > 7.99m) return "GOOD"; //"Satisfactory";
                if (valueCompare > 6.5m) return "FAIR"; //"Average";
                if (valueCompare > 5.2m) return "AVERAGE"; //"Not Bad";
                if (valueCompare > 4.99m) return "NOT BAD";// "Normal";
                if (this.IsContainArch == true)
                    return "POTENTIAL";
                else
                    return "WEAK";
                
                //if (Value > 14) return "Excelent".ToUpper();
                //if (Value > 10) return "Good".ToUpper();
                //return "Average".ToUpper();
            }
        }

        public MQModelCollection Collection { get; private set; }

        public MQModel(MQModelCollection owner)
        {
            this.Collection = owner;
        }

        public void Calculate()
        {
            //IsContainArch = false;
        }
    }

    public class MQModelCollection
    {
        private PIModelCollection PIModelCollection { get; set; }

        public List<MQModel> MQModelList { get; set; }

        public MQModel Word { get; set; }
        public MQModel Logic { get; set; }
        public MQModel Self { get; set; }
        public MQModel Body { get; set; }
        public MQModel People { get; set; }
        public MQModel Music { get; set; }
        public MQModel Picture { get; set; }
        public MQModel Nature { get; set; }

        

        //public decimal EQ { get { return Math.Round(((Self.Value + People.Value) / TotalMQValue * 100m),2); } }
        //public decimal IQ { get { return Math.Round(((Word.Value + Logic.Value) / TotalMQValue * 100m), 2); } }
        //public decimal AQ { get { return Math.Round(((Body.Value + Nature.Value) / TotalMQValue * 100m), 2); } }
        //public decimal CQ { get { return Math.Round(((Music.Value + Picture.Value) / TotalMQValue * 100m), 2); } }

        public decimal EQ { get; set; }
        public decimal IQ { get; set; }
        public decimal AQ { get; set; }
        public decimal CQ { get; set; }

        public decimal TotalMQValue { get; set; }

        #region Calculate for 16 Loai Van Dong
        public List<MQModel> MQModelList16 { get; set; }
        public MQModel Word16 { get; set; }
        public MQModel Logic16 { get; set; }
        public MQModel Self16 { get; set; }
        public MQModel Body16 { get; set; }
        public MQModel People16 { get; set; }
        public MQModel Music16 { get; set; }
        public MQModel Picture16 { get; set; }
        public MQModel Nature16 { get; set; }

        public MQModel MusicCareer { get; set; }

        public decimal TotalMQValue16 { get; set; }
        #endregion

        public MQModelCollection(PIModelCollection collection)
        {
            this.PIModelCollection = collection;
            MapModel();
            MapModel16();
            CalculateModel();
            CalculateRank();
            Calculate4Q();
        }

        private void CalculateModel()
        {
            foreach (MQModel model in this.MQModelList)
            {
                model.Calculate();
            }
        }

        private void MapModel()
        {
            if (MQModelList == null)
            {
                MQModelList = new List<MQModel>();
            }
            MQModelList.Clear();

            MQModel model;

            //Word R1-R2 - R4 - R5 - L4   || 0.1*R1 + 0.1*R2 + 0.3*R4 + 0.2*R5 + 0.3*L4
            model = new MQModel(this);
            if ((PIModelCollection.R1.FingerType.StartsWith("A") || PIModelCollection.R2.FingerType.StartsWith("A") || PIModelCollection.R4.FingerType.StartsWith("A")
                || PIModelCollection.R5.FingerType.StartsWith("A") || PIModelCollection.L4.FingerType.StartsWith("A")) //WX Region
                || (PIModelCollection.R1.FingerType.StartsWith("WX") || PIModelCollection.R2.FingerType.StartsWith("WX") || PIModelCollection.R4.FingerType.StartsWith("WX")
                || PIModelCollection.R5.FingerType.StartsWith("WX") || PIModelCollection.L4.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.1m * this.PIModelCollection.R1.PIValue
                + 0.1m * this.PIModelCollection.R2.PIValue
                + 0.3m * this.PIModelCollection.R4.PIValue
                + 0.2m * this.PIModelCollection.R5.PIValue
                + 0.3m * this.PIModelCollection.L4.PIValue), 2);
            MQModelList.Add(model);
            Word = model;

            //Logic R2-R4 - R5 - L2 - L3  || =0.4*R2 + 0.2*R4 + 0.1*R5 + 0.2*L2 + 0.1*L3
            model = new MQModel(this);
            if ((PIModelCollection.R2.FingerType.StartsWith("A") || PIModelCollection.R4.FingerType.StartsWith("A") || PIModelCollection.R5.FingerType.StartsWith("A")
                || PIModelCollection.L2.FingerType.StartsWith("A") || PIModelCollection.L3.FingerType.StartsWith("A")) // WX region
                || (PIModelCollection.R2.FingerType.StartsWith("WX") || PIModelCollection.R4.FingerType.StartsWith("WX") || PIModelCollection.R5.FingerType.StartsWith("WX")
                || PIModelCollection.L2.FingerType.StartsWith("WX") || PIModelCollection.L3.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.4m * this.PIModelCollection.R2.PIValue
                + 0.2m * this.PIModelCollection.R4.PIValue
                + 0.1m * this.PIModelCollection.R5.PIValue
                + 0.2m * this.PIModelCollection.L2.PIValue
                + 0.1m * this.PIModelCollection.L3.PIValue), 2);
            MQModelList.Add(model);
            Logic = model;

            //Self R1-R2 - R4 - L1  || =0.5*R1 + 0.2*R2 + 0.1*R4 + 0.2*L1
            model = new MQModel(this);
            if ((PIModelCollection.R1.FingerType.StartsWith("A") || PIModelCollection.R2.FingerType.StartsWith("A") || PIModelCollection.R4.FingerType.StartsWith("A")
                || PIModelCollection.L1.FingerType.StartsWith("A"))//WX Region
                || (PIModelCollection.R1.FingerType.StartsWith("WX") || PIModelCollection.R2.FingerType.StartsWith("WX") || PIModelCollection.R4.FingerType.StartsWith("WX")
                || PIModelCollection.L1.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.5m * this.PIModelCollection.R1.PIValue
                + 0.2m * this.PIModelCollection.R2.PIValue
                + 0.1m * this.PIModelCollection.R4.PIValue
                + 0.2m * this.PIModelCollection.L1.PIValue),2);
            MQModelList.Add(model);
            Self = model;

            //Body R3-L2 - L3   || =0.4*R3 + 0.2*L2 + 0.4*L3
            model = new MQModel(this);
            if ((PIModelCollection.R3.FingerType.StartsWith("A") || PIModelCollection.L2.FingerType.StartsWith("A") || PIModelCollection.L3.FingerType.StartsWith("A"))
                || (PIModelCollection.R3.FingerType.StartsWith("WX") || PIModelCollection.L2.FingerType.StartsWith("WX") || PIModelCollection.L3.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.4m * this.PIModelCollection.R3.PIValue
                + 0.2m * this.PIModelCollection.L2.PIValue
                + 0.4m * this.PIModelCollection.L3.PIValue),2);
            MQModelList.Add(model);
            Body = model;

            //People R1-R2 - R4 - L1 - L4  || =0.2*R1 + 0.1*R2 + 0.1*R4 + 0.5*L1 + 0.1*L4
            model = new MQModel(this);
            if ((PIModelCollection.R1.FingerType.StartsWith("A") || PIModelCollection.R2.FingerType.StartsWith("A") || PIModelCollection.R4.FingerType.StartsWith("A")
                || PIModelCollection.L1.FingerType.StartsWith("A") || PIModelCollection.L4.FingerType.StartsWith("A"))//WX region
                || (PIModelCollection.R1.FingerType.StartsWith("WX") || PIModelCollection.R2.FingerType.StartsWith("WX") || PIModelCollection.R4.FingerType.StartsWith("WX")
                || PIModelCollection.L1.FingerType.StartsWith("WX") || PIModelCollection.L4.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.2m * this.PIModelCollection.R1.PIValue
                + 0.1m * this.PIModelCollection.R2.PIValue
                + 0.1m * this.PIModelCollection.R4.PIValue
                + 0.5m * this.PIModelCollection.L1.PIValue
                + 0.1m * this.PIModelCollection.L4.PIValue),2);
            MQModelList.Add(model);
            People = model;

            //Music R3-R4 - L2 - L3 - L4  || =0.1*R3 + 0.3*R4 + 0.1*L2 + 0.1*L3 + 0.4*L4
            model = new MQModel(this);
            if ((PIModelCollection.R3.FingerType.StartsWith("A") || PIModelCollection.R4.FingerType.StartsWith("A") || PIModelCollection.L2.FingerType.StartsWith("A")
                || PIModelCollection.L3.FingerType.StartsWith("A") || PIModelCollection.L4.FingerType.StartsWith("A")) // WX region
                || (PIModelCollection.R3.FingerType.StartsWith("WX") || PIModelCollection.R4.FingerType.StartsWith("WX") || PIModelCollection.L2.FingerType.StartsWith("WX")
                || PIModelCollection.L3.FingerType.StartsWith("WX") || PIModelCollection.L4.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.1m * this.PIModelCollection.R3.PIValue
                + 0.3m * this.PIModelCollection.R4.PIValue
                + 0.1m * this.PIModelCollection.L2.PIValue
                + 0.1m * this.PIModelCollection.L3.PIValue
                + 0.4m * this.PIModelCollection.L4.PIValue),2);
            MQModelList.Add(model);
            Music = model;

            //Picture R5-L2 - L5  || =0.3*R5 + 0.4*L2 + 0.3*L5 (Hơi khác trong excel - Conflict cột Finger và công thức Value)- Đã giải quyết
            model = new MQModel(this);
            if ((PIModelCollection.R5.FingerType.StartsWith("A") || PIModelCollection.L2.FingerType.StartsWith("A") || PIModelCollection.L5.FingerType.StartsWith("A"))
                || (PIModelCollection.R5.FingerType.StartsWith("WX") || PIModelCollection.L2.FingerType.StartsWith("WX") || PIModelCollection.L5.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.3m * this.PIModelCollection.R5.PIValue
                + 0.4m * this.PIModelCollection.L2.PIValue
                + 0.3m * this.PIModelCollection.L5.PIValue),2);
            MQModelList.Add(model);
            Picture = model;

            //Nature R1- R3- R5 - L5 - L2 || =0.3*R1 + 0.2 * R3 + 0.3*R5 + 0.2*L5
            //Nature R1- R3- R5 - L5 - L2 || =0.2*R1 + 0.2 * R3 + 0.2*R5 + 0.2*L5 + 0.2 * L2
            model = new MQModel(this);
            //model.Value =
            //    Math.Round((0.5m * this.PIModelCollection.R1.PIValue
            //    + 0.3m * this.PIModelCollection.R3.PIValue
            //    + 0.3m * this.PIModelCollection.R5.PIValue
            //    + 0.2m * this.PIModelCollection.L5.PIValue),2);
            if ((PIModelCollection.R1.FingerType.StartsWith("A") || PIModelCollection.R3.FingerType.StartsWith("A") || PIModelCollection.R5.FingerType.StartsWith("A")
                || PIModelCollection.L5.FingerType.StartsWith("A") || PIModelCollection.L3.FingerType.StartsWith("A")) //WX Region
                || (PIModelCollection.R1.FingerType.StartsWith("WX") || PIModelCollection.R3.FingerType.StartsWith("WX") || PIModelCollection.R5.FingerType.StartsWith("WX")
                || PIModelCollection.L5.FingerType.StartsWith("WX") || PIModelCollection.L3.FingerType.StartsWith("WX")))
                model.IsContainArch = true;
            else model.IsContainArch = false;
            model.Value =
                Math.Round((0.2m * this.PIModelCollection.R1.PIValue
                + 0.2m * this.PIModelCollection.R3.PIValue
                + 0.2m * this.PIModelCollection.R5.PIValue
                + 0.2m * this.PIModelCollection.L5.PIValue
                + 0.2m * this.PIModelCollection.L3.PIValue), 2);
            MQModelList.Add(model);
            Nature = model;

            TotalMQValue = MQModelList.Sum(p => p.Value);
        }

        private void MapModel16()
        {
            if (MQModelList16 == null)
            {
                MQModelList16 = new List<MQModel>();
            }
            MQModelList16.Clear();

            MQModel model16;

            //Word R1-R2 - R4 - R5 - L4   || 0.1*R1 + 0.1*R2 + 0.3*R4 + 0.2*R5 + 0.3*L4
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.1m * this.PIModelCollection.R1.PIValue2
                + 0.1m * this.PIModelCollection.R2.PIValue2
                + 0.3m * this.PIModelCollection.R4.PIValue2
                + 0.2m * this.PIModelCollection.R5.PIValue2
                + 0.3m * this.PIModelCollection.L4.PIValue2), 2);
            MQModelList16.Add(model16);
            Word16 = model16;

            //Logic R2-R4 - R5 - L2 - L3  || =0.4*R2 + 0.2*R4 + 0.1*R5 + 0.2*L2 + 0.1*L3
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.4m * this.PIModelCollection.R2.PIValue2
                + 0.2m * this.PIModelCollection.R4.PIValue2
                + 0.1m * this.PIModelCollection.R5.PIValue2
                + 0.2m * this.PIModelCollection.L2.PIValue2
                + 0.1m * this.PIModelCollection.L3.PIValue2), 2);
            MQModelList16.Add(model16);
            Logic16 = model16;

            //Self R1-R2 - R4 - L1  || =0.5*R1 + 0.2*R2 + 0.1*R4 + 0.2*L1
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.5m * this.PIModelCollection.R1.PIValue2
                + 0.2m * this.PIModelCollection.R2.PIValue2
                + 0.1m * this.PIModelCollection.R4.PIValue2
                + 0.2m * this.PIModelCollection.L1.PIValue2), 2);
            MQModelList16.Add(model16);
            Self16 = model16;

            //Body R3-L2 - L3   || =0.4*R3 + 0.2*L2 + 0.4*L3
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.4m * this.PIModelCollection.R3.PIValue2
                + 0.2m * this.PIModelCollection.L2.PIValue2
                + 0.4m * this.PIModelCollection.L3.PIValue2), 2);
            MQModelList16.Add(model16);
            Body16 = model16;

            //People R1-R2 - R4 - L1 - L4  || =0.2*R1 + 0.1*R2 + 0.1*R4 + 0.5*L1 + 0.1*L4
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.2m * this.PIModelCollection.R1.PIValue2
                + 0.1m * this.PIModelCollection.R2.PIValue2
                + 0.1m * this.PIModelCollection.R4.PIValue2
                + 0.5m * this.PIModelCollection.L1.PIValue2
                + 0.1m * this.PIModelCollection.L4.PIValue2), 2);
            MQModelList16.Add(model16);
            People16 = model16;

            //Music R3-R4 - L2 - L3 - L4  || =0.1*R3 + 0.3*R4 + 0.1*L2 + 0.1*L3 + 0.4*L4
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.1m * this.PIModelCollection.R3.PIValue2
                + 0.3m * this.PIModelCollection.R4.PIValue2
                + 0.1m * this.PIModelCollection.L2.PIValue2
                + 0.1m * this.PIModelCollection.L3.PIValue2
                + 0.4m * this.PIModelCollection.L4.PIValue2), 2);
            MQModelList16.Add(model16);
            Music16 = model16;

            //Picture R5-L2 - L5  || =0.3*R5 + 0.4*L2 + 0.3*L5 (Hơi khác trong excel - Conflict cột Finger và công thức Value)- Đã giải quyết
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.3m * this.PIModelCollection.R5.PIValue2
                + 0.4m * this.PIModelCollection.L2.PIValue2
                + 0.3m * this.PIModelCollection.L5.PIValue2), 2);
            MQModelList16.Add(model16);
            Picture16 = model16;

            //Nature R1- R3- R5 - L5 - L2 || =0.3*R1 + 0.2 * R3 + 0.3*R5 + 0.2*L5
            //Nature R1- R3- R5 - L5 - L2 || =0.2*R1 + 0.2 * R3 + 0.2*R5 + 0.2*L5 + 0.2 * L2
            model16 = new MQModel(this);
            model16.Value =
                Math.Round((0.2m * this.PIModelCollection.R1.PIValue2
                + 0.2m * this.PIModelCollection.R3.PIValue2
                + 0.2m * this.PIModelCollection.R5.PIValue2
                + 0.2m * this.PIModelCollection.L5.PIValue2
                + 0.2m * this.PIModelCollection.L3.PIValue2), 2);
            MQModelList16.Add(model16);
            Nature16 = model16;

            TotalMQValue16 = MQModelList16.Sum(p => p.Value);

            //Music Career
            model16 = new MQModel(this);
            if ((PIModelCollection.R4.FingerType.StartsWith("A")
                || PIModelCollection.L4.FingerType.StartsWith("A")) // WX region
                || (PIModelCollection.R4.FingerType.StartsWith("WX")
                || PIModelCollection.L3.FingerType.StartsWith("WX")))
                model16.IsContainArch = true;
            else model16.IsContainArch = false;
            model16.Value = Math.Round((0.5m * this.PIModelCollection.R4.PIValue + 0.5m * this.PIModelCollection.L4.PIValue), 2);
            MQModelList.Add(model16);
            MusicCareer = model16;
        }

        public void CalculateRank()
        {
            List<MQModel> sortedList = this.MQModelList.OrderByDescending(p => p.Value).ToList();

            int rank = 1;
            sortedList[0].Rank = 1;
            for (int i = 1; i < sortedList.Count - 1; i++)
            {
                if (sortedList[i].Value != sortedList[i - 1].Value)
                {
                    rank = i + 1;
                }
                sortedList[i].Rank = rank;
            }
        }

        public void Calculate4Q()
        {
            decimal EQTemp = 1.0m;
            decimal IQTemp = 1.0m;
            decimal CQTemp = 1.0m;
            decimal AQTemp = 1.0m;

            EQ = (PIModelCollection.L1.PIValue + PIModelCollection.R1.PIValue) / PIModelCollection.TFRC * 100 + 9.0m;
            IQ = (PIModelCollection.L2.PIValue + PIModelCollection.R2.PIValue) / PIModelCollection.TFRC * 100 + 8.0m;
            CQ = (PIModelCollection.L5.PIValue + PIModelCollection.R5.PIValue) / PIModelCollection.TFRC * 100 + 8.0m;
            AQ = (PIModelCollection.L3.PIValue + PIModelCollection.R3.PIValue) / PIModelCollection.TFRC * 100 + 8.0m;

            if (EQ <= 20)
                EQ = 20;
            if (IQ <= 20)
                IQ = 20;
            if (CQ <= 20)
                CQ = 20;
            if (AQ <= 20)
                AQ = 20;

            decimal TotalTemp = EQ + IQ + AQ + CQ + EQTemp + CQTemp + AQTemp + IQTemp;
            TotalTemp /= 100;
            EQ /= TotalTemp;
            IQ /= TotalTemp;
            CQ /= TotalTemp;
            AQ /= TotalTemp;

            decimal SoDu = 0;
            decimal SoDuPhanRa = 0;

            SoDu = 100 - EQ - IQ - CQ - AQ;
            SoDuPhanRa = SoDu / 4;

            IQ += SoDuPhanRa;
            CQ += SoDuPhanRa;
            AQ += SoDuPhanRa;
            EQ = 100 - IQ - CQ - AQ;

            Tuple<string, decimal>[] mysortedList = new Tuple<string, decimal>[4];
            mysortedList[0] = new Tuple<string, decimal>("EQ", EQ);
            mysortedList[1] = new Tuple<string, decimal>("IQ", IQ);
            mysortedList[2] = new Tuple<string, decimal>("CQ", CQ);
            mysortedList[3] = new Tuple<string, decimal>("AQ", AQ);
            var result = mysortedList.OrderByDescending(x => x.Item2);
            string MinIn4QName = result.LastOrDefault().Item1; //mysortedList.LastOrDefault().Key;
            decimal MinIn4QValue = result.LastOrDefault().Item2; //mysortedList.LastOrDefault().Value;

            string SecondIn4QName = result.ElementAt(1).Item1; //mysortedList.ElementAt(1).Key;
            decimal SecondIn4QValue = result.ElementAt(1).Item2; //mysortedList.ElementAt(1).Value;

            string ThirdIn4QName = result.ElementAt(2).Item1; //mysortedList.ElementAt(2).Key;
            decimal ThirdIn4QValue = result.ElementAt(2).Item2;//mysortedList.ElementAt(2).Value;

            string MaxIn4QName = result.FirstOrDefault().Item1;  //mysortedList.FirstOrDefault().Key;
            decimal MaxIn4QValue = result.FirstOrDefault().Item2; //mysortedList.FirstOrDefault().Value;

            if(MinIn4QValue < 20)
            {
                if (MinIn4QValue >= 18.5m)
                {
                    MaxIn4QValue -= 0.5m;
                    MinIn4QValue += 0.5m;

                    if (MinIn4QValue >= 20)
                        goto label20;

                    if (SecondIn4QValue - 1 >= 20)
                    {
                        SecondIn4QValue -= 0.5m;
                        MinIn4QValue += 0.5m;
                        if(MinIn4QValue >= 20)
                            goto label20;
                    }

                    else
                    {
                        MaxIn4QValue -= 0.25m;
                        MinIn4QValue += 0.25m;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }

                    if (ThirdIn4QValue - 1 >= 20)
                    {
                        ThirdIn4QValue -= 0.5m;
                        MinIn4QValue += 0.5m;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }
                    else
                    {
                        MaxIn4QValue -= 0.25m;
                        MinIn4QValue += 0.25m;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }

                    if (MinIn4QValue < 20)
                    {
                        MaxIn4QValue -= 0.5m;
                        MinIn4QValue += 0.5m;
                    }
                }
                else if (MinIn4QValue >= 17 && MinIn4QValue < 18.5m)
                {
                    MaxIn4QValue -= 1;
                    MinIn4QValue += 1;
                    if (MinIn4QValue >= 20)
                        goto label20;

                    if (SecondIn4QValue - 1 >= 20)
                    {
                        SecondIn4QValue -= 1;
                        MinIn4QValue += 1;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }
                    
                    else
                    {
                        MaxIn4QValue -= 0.75m;
                        MinIn4QValue += 0.75m;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }
                    
                    if (ThirdIn4QValue - 1 >= 20)
                    {
                        ThirdIn4QValue -= 1;
                        MinIn4QValue += 1;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }
                    else
                    {
                        MaxIn4QValue -= 0.75m;
                        MinIn4QValue += 0.75m;
                        if (MinIn4QValue >= 20)
                            goto label20;
                    }

                    if(MinIn4QValue < 20)
                    {
                        MaxIn4QValue -= 0.5m;
                        MinIn4QValue += 0.5m;
                    }
                }

             label20:
                if (MinIn4QName == "IQ") IQ = MinIn4QValue;
                else if (MinIn4QName == "EQ") EQ = MinIn4QValue;
                else if (MinIn4QName == "CQ") CQ = MinIn4QValue;
                else AQ = MinIn4QValue;

                if (SecondIn4QName == "IQ") IQ = SecondIn4QValue;
                else if (SecondIn4QName == "EQ") EQ = SecondIn4QValue;
                else if (SecondIn4QName == "CQ") CQ = SecondIn4QValue;
                else AQ = SecondIn4QValue;

                if (ThirdIn4QName == "IQ") IQ = ThirdIn4QValue;
                else if (ThirdIn4QName == "EQ") EQ = ThirdIn4QValue;
                else if (ThirdIn4QName == "CQ") CQ = ThirdIn4QValue;
                else AQ = ThirdIn4QValue;

                if (MaxIn4QName == "IQ") IQ = MaxIn4QValue;
                else if (MaxIn4QName == "EQ") EQ = MaxIn4QValue;
                else if (MaxIn4QName == "CQ") CQ = MaxIn4QValue;
                else AQ = MaxIn4QValue;
            }
            
            IQ = Math.Round(IQ, 2);
            CQ = Math.Round(CQ, 2);
            AQ = Math.Round(AQ, 2);
            EQ = 100 - IQ - CQ - AQ;
            EQ = Math.Round(EQ, 2);

        }
    }
}