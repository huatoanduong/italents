﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTalent.Formula.Model
{
    public class IntelligenceModelCollection
    {
        private PIModelCollection PICollection;
        public decimal NaoTrai { get; set; }
        public decimal NaoPhai { get; set; }

        public decimal ThuyTruocTran { get; set; }
        public decimal ThuyTran { get; set; }
        public decimal ThuyDinh { get; set; }
        public decimal ThuyThaiDuong { get; set; }
        public decimal ThuyCham { get; set; }

        public decimal OcNangDong { get; set; }
        public decimal OcPhanTich { get; set; }
        public decimal OcKinesthic { get; set; }
        public decimal OcAuditory { get; set; }
        public decimal OcVisual { get; set; }

        public IntelligenceModelCollection(PIModelCollection collection)
        {
            PICollection = collection;
        }

        public void Calculate()
        {
            PIModel PI1;
            PIModel PI2;
            PIModel PI3;
            PIModel PI4;
            PIModel PI5;
            decimal Value1;
            decimal Value2;

            PI1 = PICollection.L1;
            PI2 = PICollection.L2;
            PI3 = PICollection.L3;
            PI4 = PICollection.L4;
            PI5 = PICollection.L5;
            decimal SumPILeft = Math.Round(PI1.PIValue + PI2.PIValue + PI3.PIValue + PI4.PIValue + PI5.PIValue, 2);
            NaoPhai = SumPILeft;
            NaoPhai = Math.Round(SumPILeft / PICollection.TFRC * 100, 2);

            PI1 = PICollection.R1;
            PI2 = PICollection.R2;
            PI3 = PICollection.R3;
            PI4 = PICollection.R4;
            PI5 = PICollection.R5;
            decimal SumPIRight = Math.Round(PI1.PIValue + PI2.PIValue + PI3.PIValue + PI4.PIValue + PI5.PIValue, 2);
            NaoTrai = SumPIRight;
            NaoTrai = 100 - NaoPhai;

            PI1 = PICollection.L1;
            PI2 = PICollection.R1;
            if (PI1.FingerType == "UL")
                Value1 = Math.Max(PI1.FRCL, PI1.FRCR);
            else
                Value1 = (PI1.FRCL + PI1.FRCR) / 2;
            if (PI2.FingerType == "UL")
                Value2 = Math.Max(PI2.FRCL, PI2.FRCR);
            else
                Value2 = (PI2.FRCL + PI2.FRCR) / 2;
            //ThuyTruocTran = PICollection.TFRC != 0
            //    ? Math.Round((PI1.PIValue + PI2.PIValue) / PICollection.TFRC * 100, 2)
            //    : 0;
            ThuyTruocTran = PICollection.TFRC != 0
                ? Math.Round((Value1 + Value2) / PICollection.TFRC * 100, 2)
                : 0;

            PI1 = PICollection.L2;
            PI2 = PICollection.R2;
            if (PI1.FingerType == "UL")
                Value1 = Math.Max(PI1.FRCL, PI1.FRCR);
            else
                Value1 = (PI1.FRCL + PI1.FRCR) / 2;
            if (PI2.FingerType == "UL")
                Value2 = Math.Max(PI2.FRCL, PI2.FRCR);
            else
                Value2 = (PI2.FRCL + PI2.FRCR) / 2;
            //ThuyTran = PICollection.TFRC != 0 ? Math.Round((PI1.PIValue + PI2.PIValue) / PICollection.TFRC * 100, 2) : 0;
            ThuyTran = PICollection.TFRC != 0 
                ? Math.Round((Value1 + Value2)/ PICollection.TFRC * 100, 2) 
                : 0;

            PI1 = PICollection.L3;
            PI2 = PICollection.R3;
            if (PI1.FingerType == "UL")
                Value1 = Math.Max(PI1.FRCL, PI1.FRCR);
            else
                Value1 = (PI1.FRCL + PI1.FRCR) / 2;
            if (PI2.FingerType == "UL")
                Value2 = Math.Max(PI2.FRCL, PI2.FRCR);
            else
                Value2 = (PI2.FRCL + PI2.FRCR) / 2;
            //ThuyDinh = PICollection.TFRC != 0 ? Math.Round((PI1.PIValue + PI2.PIValue) / PICollection.TFRC * 100, 2) : 0;
            ThuyDinh = PICollection.TFRC != 0
               ? Math.Round((Value1 + Value2) / PICollection.TFRC * 100, 2)
               : 0;

            PI1 = PICollection.L4;
            PI2 = PICollection.R4;
            if (PI1.FingerType == "UL")
                Value1 = Math.Max(PI1.FRCL, PI1.FRCR);
            else
                Value1 = (PI1.FRCL + PI1.FRCR) / 2;
            if (PI2.FingerType == "UL")
                Value2 = Math.Max(PI2.FRCL, PI2.FRCR);
            else
                Value2 = (PI2.FRCL + PI2.FRCR) / 2;
            //ThuyThaiDuong = PICollection.TFRC != 0
            //    ? Math.Round((PI1.PIValue + PI2.PIValue) / PICollection.TFRC * 100, 2)
            //    : 0;
            ThuyThaiDuong = PICollection.TFRC != 0
               ? Math.Round((Value1 + Value2) / PICollection.TFRC * 100, 2)
               : 0;

            PI1 = PICollection.L5;
            PI2 = PICollection.R5;
            if (PI1.FingerType == "UL")
                Value1 = Math.Max(PI1.FRCL, PI1.FRCR);
            else
                Value1 = (PI1.FRCL + PI1.FRCR) / 2;
            if (PI2.FingerType == "UL")
                Value2 = Math.Max(PI2.FRCL, PI2.FRCR);
            //ThuyCham = PICollection.TFRC != 0 ? Math.Round((PI1.PIValue + PI2.PIValue) / PICollection.TFRC * 100, 2) : 0;
            ThuyCham = PICollection.TFRC != 0
               ? Math.Round((Value1 + Value2) / PICollection.TFRC * 100, 2)
               : 0;

            decimal ThuyChenhLech = 100 - ThuyDinh - ThuyThaiDuong - ThuyTran - ThuyTruocTran - ThuyCham;
            if(ThuyChenhLech > 0)
            {
                decimal HeSoChenhLech = ThuyChenhLech / 5;
                ThuyDinh += HeSoChenhLech;
                ThuyThaiDuong += HeSoChenhLech;
                ThuyTran += HeSoChenhLech;
                ThuyTruocTran += HeSoChenhLech;
                ThuyCham += HeSoChenhLech;

                ThuyTruocTran = Math.Round(ThuyTruocTran, 2);
                ThuyDinh = Math.Round(ThuyDinh, 2);
                ThuyThaiDuong = Math.Round(ThuyThaiDuong, 2);
                ThuyCham = Math.Round(ThuyCham, 2);
                ThuyTran = 100 - ThuyTruocTran - ThuyDinh - ThuyThaiDuong - ThuyCham;
            }
            else
            {
                decimal HeSoChenhLech = ThuyChenhLech / 5;
                ThuyDinh += HeSoChenhLech;
                ThuyThaiDuong += HeSoChenhLech;
                ThuyTran += HeSoChenhLech;
                ThuyTruocTran += HeSoChenhLech;
                ThuyCham += HeSoChenhLech;

                ThuyTruocTran = Math.Round(ThuyTruocTran, 2);
                ThuyDinh = Math.Round(ThuyDinh, 2);
                ThuyThaiDuong = Math.Round(ThuyThaiDuong, 2);
                ThuyCham = Math.Round(ThuyCham, 2);
                ThuyTran = 100 - ThuyTruocTran - ThuyDinh - ThuyThaiDuong - ThuyCham;
            }
            if(ThuyDinh + ThuyThaiDuong + ThuyTran + ThuyTruocTran + ThuyCham < 100)
            {
                decimal SoDu = 100 - ThuyDinh - ThuyThaiDuong - ThuyTran - ThuyTruocTran - ThuyCham;
                ThuyTran += SoDu;
            }

            OcNangDong = ((ThuyTruocTran + ThuyTran) != 0)
                ? Math.Round(ThuyTruocTran / (ThuyTruocTran + ThuyTran) * 100, 2)
                : 0;
            OcPhanTich = ((ThuyTruocTran + ThuyTran) != 0)
                ? Math.Round(ThuyTran / (ThuyTruocTran + ThuyTran) * 100, 2)
                : 0;
            OcKinesthic = ((ThuyDinh + ThuyThaiDuong + ThuyCham) != 0)
                ? Math.Round(ThuyDinh / (ThuyDinh + ThuyThaiDuong + ThuyCham) * 100, 2)
                : 0;
            OcAuditory = ((ThuyDinh + ThuyThaiDuong + ThuyCham) != 0)
                ? Math.Round(ThuyThaiDuong / (ThuyDinh + ThuyThaiDuong + ThuyCham) * 100, 2)
                : 0;
            //OcVisual = ((ThuyDinh + ThuyThaiDuong + ThuyCham) != 0)
            //    ? Math.Round(ThuyCham / (ThuyDinh + ThuyThaiDuong + ThuyCham) * 100, 2)
            //    : 0;
            OcVisual = 100 - OcAuditory - OcKinesthic;
        }
    }
}