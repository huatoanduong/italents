﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Entity;
using iTalent.Formula.Model.ExcelTemplate;
// ReSharper disable InconsistentNaming
// ReSharper disable MergeConditionalExpression

namespace iTalent.Formula.Model
{
    public class PIModel
    {
        /// <summary>
        /// WS, WC, UL...
        /// </summary>
        public string FingerType { get; set; }

        /// <summary>
        /// L1, L2, R1...
        /// </summary>
        public string FingerName { get; set; }

        public decimal NGFValue { get; set; }
        public decimal FRCL { get; set; }
        public decimal FRCR { get; set; }

        public decimal ChiSoHapThuViecHoc { get { return PIValue * NGFValue; } }

        public decimal Sum_FRC { get { return FRCL + FRCR; } }

        public decimal XValue { get; set; }

        //Insert more for Calculate 16 Van Dong
        public decimal YValue { get; set; }

        public decimal PIValue { get; set; }
        public decimal PIValue2 { get; set; }

        public int Rank { get; set; }

        public bool IsSet { get; set; }

        public PIModelCollection Owner { get; private set; }

        public void CalculatePI()
        {
            decimal tmpNGF = (NGFValue - 1) / 10;
            if (FingerType == "UL")
                PIValue = Math.Round(XValue * NGFValue, 2);
            else if (FingerType == "WX")
                PIValue = Math.Round(XValue * NGFValue, 2);
            else
                PIValue = Math.Round(XValue * tmpNGF + XValue, 2);
        }

        public void CalculatePI2()
        {
            decimal tmpNGF = (NGFValue - 1) / 10;
            if (FingerType == "UL" && (FingerName == "L5" || FingerName == "R5"))
                PIValue2 = Math.Round(YValue * 1.2m, 2);
            else if (FingerType == "WX")
            {
                PIValue2 = Math.Round(YValue * NGFValue, 2);
            }
            else
                PIValue2 = Math.Round(YValue * tmpNGF + YValue, 2);
        }

        public PIModel(PIModelCollection owner)
        {
            this.Owner = owner;
        }
    }

    /// <summary>
    /// Dùng để tính các giá trị PI
    /// </summary>
    public class PIModelCollection
    {
        public List<PIModel> Collection { get; set; }

        public RidgeCountModelCollection RidgeCountModelCollection { get; set; }
        //public ExcelNGFValueCollection ExcelNgfCollection { get; set; }

        public decimal AFRC { get; set; }
        public decimal TFRC { get; set; }
        public decimal TFRC16 { get; set; }
        public decimal TFRC_Average { get { return TFRC/10m; } }
        public decimal TFRC_Average16 { get { return TFRC16 / 10m; } }
        public decimal FRC_Minus { get { return AFRC - TFRC; } }

        public PIModel L1 { get; set; }
        public PIModel L2 { get; set; }
        public PIModel L3 { get; set; }
        public PIModel L4 { get; set; }
        public PIModel L5 { get; set; }

        public PIModel R1 { get; set; }
        public PIModel R2 { get; set; }
        public PIModel R3 { get; set; }
        public PIModel R4 { get; set; }
        public PIModel R5 { get; set; }

        public PIModelCollection(RidgeCountModelCollection ridgeCountModelCollection, ExcelNGFValueCollection ngfCollection)
        {
            Collection = new List<PIModel>();
            this.RidgeCountModelCollection = ridgeCountModelCollection;
            //this.ExcelNgfCollection = collection;
            MapFromRidgeCount();
            MapFromExcelNgf(ngfCollection);
            CalculateXValue();
            CalculateYValue();
            CalculatePI();
            CalculatePI2();
            CalculateRank();
        }

        public PIModelCollection(RidgeCountModelCollection ridgeCountModelCollection, FIFingerAnalysis analysis)
        {
            Collection = new List<PIModel>();
            this.RidgeCountModelCollection = ridgeCountModelCollection;
            MapFromRidgeCount();
            MapFromAnalysis(analysis);
            CalculateYValue();
            CalculatePI2();
            CalculateRank();
        }

        private void MapFromRidgeCount()
        {
            Collection = new List<PIModel>();
            PIModel model;

            model = new PIModel(this)
            {
                FingerName = "L1",
                FingerType = RidgeCountModelCollection.L1T,
                FRCL = RidgeCountModelCollection.L1L,
                FRCR = RidgeCountModelCollection.L1R,
            };
            Collection.Add(model);
            this.L1 = model;

            model = new PIModel(this)
            {
                FingerName = "L2",
                FingerType = RidgeCountModelCollection.L2T,
                FRCL = RidgeCountModelCollection.L2L,
                FRCR = RidgeCountModelCollection.L2R,
            };
            Collection.Add(model);
            this.L2 = model;

            model = new PIModel(this)
            {
                FingerName = "L3",
                FingerType = RidgeCountModelCollection.L3T,
                FRCL = RidgeCountModelCollection.L3L,
                FRCR = RidgeCountModelCollection.L3R,
            };
            Collection.Add(model);
            this.L3 = model;

            model = new PIModel(this)
            {
                FingerName = "L4",
                FingerType = RidgeCountModelCollection.L4T,
                FRCL = RidgeCountModelCollection.L4L,
                FRCR = RidgeCountModelCollection.L4R,
            };
            Collection.Add(model);
            this.L4 = model;

            model = new PIModel(this)
            {
                FingerName = "L5",
                FingerType = RidgeCountModelCollection.L5T,
                FRCL = RidgeCountModelCollection.L5L,
                FRCR = RidgeCountModelCollection.L5R,
            };
            Collection.Add(model);
            this.L5 = model;

            model = new PIModel(this)
            {
                FingerName = "R1",
                FingerType = RidgeCountModelCollection.R1T,
                FRCL = RidgeCountModelCollection.R1L,
                FRCR = RidgeCountModelCollection.R1R,
            };
            Collection.Add(model);
            this.R1 = model;

            model = new PIModel(this)
            {
                FingerName = "R2",
                FingerType = RidgeCountModelCollection.R2T,
                FRCL = RidgeCountModelCollection.R2L,
                FRCR = RidgeCountModelCollection.R2R,
            };
            Collection.Add(model);
            this.R2 = model;

            model = new PIModel(this)
            {
                FingerName = "R3",
                FingerType = RidgeCountModelCollection.R3T,
                FRCL = RidgeCountModelCollection.R3L,
                FRCR = RidgeCountModelCollection.R3R,
            };
            Collection.Add(model);
            this.R3 = model;

            model = new PIModel(this)
            {
                FingerName = "R4",
                FingerType = RidgeCountModelCollection.R4T,
                FRCL = RidgeCountModelCollection.R4L,
                FRCR = RidgeCountModelCollection.R4R,
            };
            Collection.Add(model);
            this.R4 = model;

            model = new PIModel(this)
            {
                FingerName = "R5",
                FingerType = RidgeCountModelCollection.R5T,
                FRCL = RidgeCountModelCollection.R5L,
                FRCR = RidgeCountModelCollection.R5R,
            };
            Collection.Add(model);
            this.R5 = model;
        }

        private void MapFromExcelNgf(ExcelNGFValueCollection collection)
        {
            //PIModelList = new List<PIModel>();
            PIModel model;
            ExcelNGFValue ngfValue;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.L1T, "L1");
            this.L1.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.L2T, "L2");
            this.L2.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.L3T, "L3");
            this.L3.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.L4T, "L4");
            this.L4.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.L5T, "L5");
            this.L5.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.R1T, "R1");
            this.R1.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.R2T, "R2");
            this.R2.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.R3T, "R3");
            this.R3.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.R4T, "R4");
            this.R4.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;

            ngfValue = collection.FindByTypeAndName(RidgeCountModelCollection.R5T, "R5");
            this.R5.NGFValue = (ngfValue == null) ? 0 : ngfValue.Value;
        }

        private void MapFromAnalysis(FIFingerAnalysis analysis)
        {
            L1.PIValue = analysis.FPType1;
            L2.PIValue = analysis.FPType2;
            L3.PIValue = analysis.FPType3;
            L4.PIValue = analysis.FPType4;
            L5.PIValue = analysis.FPType5;

            R1.PIValue = analysis.FPType6;
            R2.PIValue = analysis.FPType7;
            R3.PIValue = analysis.FPType8;
            R4.PIValue = analysis.FPType9;
            R5.PIValue = analysis.FPType10;


            L1.NGFValue = analysis.S1BZ;
            L2.NGFValue = analysis.S2BZ;
            L3.NGFValue = analysis.S3BZ;
            L4.NGFValue = analysis.S4BZ;
            L5.NGFValue = analysis.S5BZ;

            R1.NGFValue = analysis.S6BZ;
            R2.NGFValue = analysis.S7BZ;
            R3.NGFValue = analysis.S8BZ;
            R4.NGFValue = analysis.S9BZ;
            R5.NGFValue = analysis.S10BZ;

            TFRC = analysis.TFRC;
            AFRC = analysis.Page_BLS;
        }

        public void CalculateXValue()
        {
            foreach (PIModel model in Collection)
            {
                model.IsSet = false;
            }

            if (Collection.Count(p => p.FingerType.StartsWith("AS")) >= 10)
            {
                foreach (PIModel model in Collection)
                {
                    model.XValue = 0;
                    model.IsSet = true;
                }
            }

            if (Collection.Count(p=> p.FingerType.StartsWith("A")) >= 10)
            {
                foreach (PIModel model in Collection)
                {
                    model.XValue = 12m;
                    model.IsSet = true;
                }
            }

            int CountingUL = Collection.Count(p => p.FingerType.StartsWith("UL"));
            int CountingWhorl = Collection.Count(p => p.FingerType.StartsWith("W"));
            int CountingAS = Collection.Count(p => p.FingerType.StartsWith("AS"));

            bool noWhorl = Collection.All(p => !p.FingerType.StartsWith("W"));

            foreach (PIModel model in Collection)
            {
                if (model.IsSet)
                {
                    continue;
                }

                if (model.FingerType.StartsWith("LF") || model.FingerType.StartsWith("RL"))
                {
                    model.XValue = Math.Max(model.FRCL, model.FRCR);
                }
                else if (model.FingerType == "UL")
                {
                    model.XValue = Math.Max(model.FRCL, model.FRCR);
                    if (model.FingerName == "L5")
                    {
                        model.XValue = model.XValue;
                    }
                    else if (model.FingerName == "R5")
                    {
                        model.XValue = model.XValue;
                    }
                    else
                    {
                        model.XValue = model.XValue;//+ CountingUL;
                    }
                }
                else if (model.FingerType.StartsWith("WX"))
                {
                    //model.XValue = 12m;
                    model.XValue = (model.FRCL + model.FRCR);
                }
                else if (model.FingerType.StartsWith("AR") || model.FingerType.StartsWith("AE"))
                {
                    model.XValue = CountingUL * 0.75m + 6.5m;
                }
                else if (model.FingerType.StartsWith("AU"))
                {
                    model.XValue = CountingUL * 0.75m + 6.5m;
                }
                else if (model.FingerType.StartsWith("AS") && noWhorl)
                {
                    if (CountingAS >= 1)
                        model.XValue = 8.3333m;
                    else if (CountingAS >= 2)
                        model.XValue = 7.5m;
                    else if (CountingAS >= 3)
                        model.XValue = 6.5m;
                    else if (CountingAS >= 4)
                        model.XValue = 5.5m;
                    else if (CountingAS >= 5)
                        model.XValue = 4.5m;
                    else if (CountingAS >= 6)
                        model.XValue = 3.5m;
                    else if (CountingAS >= 7)
                        model.XValue = 2.5m;
                }
                else if (model.FingerType.StartsWith("AS") && !noWhorl)
                {
                    if (CountingAS >= 1)
                        model.XValue = 8.3333m;
                    else if (CountingAS >= 2)
                        model.XValue = 7.5m;
                    else if (CountingAS >= 3)
                        model.XValue = 6.5m;
                    else if (CountingAS >= 4)
                        model.XValue = 5.5m;
                    else if (CountingAS >= 5)
                        model.XValue = 4.5m;
                    else if (CountingAS >= 6)
                        model.XValue = 3.5m;
                    else if (CountingAS >= 7)
                        model.XValue = 2.5m;

                    model.XValue = model.XValue + (CountingWhorl * 0.1m);
                }
                else if (model.FingerType.StartsWith("AT"))
                {
                    model.XValue = 10m;
                }
                //else if (model.FingerType.StartsWith("W"))
                //{
                //    model.XValue = Math.Max(model.FRCL, model.FRCR);
                //}
                else
                {
                    model.XValue = model.Sum_FRC / 2;
                }
                model.IsSet = true;
            }
        }

        public void CalculatePI()
        {
            foreach (PIModel model in Collection)
            {
                model.CalculatePI();
            }

            //Tính lại PI ngón cái
            //if (L1.PIValue > 20 && R1.PIValue > 20)
            //{
            //    if (L1.PIValue >= R1.PIValue)
            //    {
            //        L1.PIValue += 1;
            //    }
            //    else
            //    {
            //        R1.PIValue += 1;
            //    }
            //}
            //else //(L1.PIValue < 20 || R1.PIValue < 20)
            //{
            //    if (L1.PIValue <= R1.PIValue)
            //    {
            //        L1.PIValue -= 1;
            //    }
            //    else
            //    {
            //        R1.PIValue -= 1;
            //    }
            //}

            //Tính lại PI ngón trỏ
            //if (L2.PIValue > 20 && R2.PIValue > 20)
            //{
            //    if (L2.PIValue >= R2.PIValue)
            //    {
            //        L2.PIValue += 1.5m;
            //    }
            //    else
            //    {
            //        R2.PIValue += 1.5m;
            //    }
            //}
            //else //(L2.PIValue < 20 || R2.PIValue < 20)
            //{
            //    if (L2.PIValue <= R2.PIValue)
            //    {
            //        L2.PIValue -= 1.5m;
            //    }
            //    else
            //    {
            //        R2.PIValue -= 1.5m;
            //    }
            //}

           // AFRC = Math.Round(PIModelList.Sum(p => p.NGFValue * p.Sum_FRC), 2);

            bool noWhorl = Collection.All(p => !p.FingerType.StartsWith("W"));

            if (noWhorl)
            {
                TFRC = Math.Round(Collection.Sum(p => p.PIValue), 2);
                AFRC = TFRC;
            }
            else if (
                Collection.Count(p => p.FingerType.StartsWith("RL")) >= 10
                || Collection.Count(p => p.FingerType.StartsWith("LF")) >= 10
                || Collection.Count(p => p.FingerType.StartsWith("UL")) >= 10
                || Collection.Count(p => p.FingerType.StartsWith("WX")) >= 10
                )
            {
                TFRC = Math.Round(Collection.Sum(p => p.PIValue), 2);
                AFRC = TFRC;
            }
            else if (
                Collection.Count(p => p.FingerType.StartsWith("A")) >= 10
                )
            {
                TFRC = Math.Round(Collection.Sum(p => p.PIValue), 2);
                AFRC = TFRC;
            }
            else
            {
                //AFRC = Math.Round(Collection.Sum(p => p.Sum_FRC), 2);
                TFRC = Math.Round(Collection.Sum(p => p.PIValue), 2);
                foreach (PIModel model in Collection)
                {
                    if (model.FingerType.StartsWith("W"))
                        AFRC += model.FRCL + model.FRCR;
                    //AFRC += (model.FRCL * model.NGFValue) + (model.FRCR * model.NGFValue);
                    else
                        AFRC += model.PIValue;
                }
            }
        }

        public void CalculateYValue()
        {
            foreach (PIModel model in Collection)
            {
                model.IsSet = false;
            }

            if (Collection.Count(p => p.FingerType.StartsWith("AS")) >= 10)
            {
                foreach (PIModel model in Collection)
                {
                    model.YValue = 0;
                    model.IsSet = true;
                }
            }

            if (Collection.Count(p => p.FingerType.StartsWith("A")) >= 10)
            {
                foreach (PIModel model in Collection)
                {
                    model.YValue = 12m;
                    model.IsSet = true;
                }
            }

            int CountingUL = Collection.Count(p => p.FingerType.StartsWith("UL"));
            int CountingWhorl = Collection.Count(p => p.FingerType.StartsWith("W"));
            int CountingAS = Collection.Count(p => p.FingerType.StartsWith("AS"));

            bool noWhorl = Collection.All(p => !p.FingerType.StartsWith("W"));

            foreach (PIModel model in Collection)
            {
                if (model.IsSet)
                {
                    continue;
                }

                if (model.FingerType.StartsWith("LF") || model.FingerType.StartsWith("RL"))
                {
                    model.YValue = Math.Max(model.FRCL, model.FRCR);
                }
                else if (model.FingerType == "UL")
                {
                    model.YValue = Math.Max(model.FRCL, model.FRCR);
                    if (model.FingerName == "L5")
                    {
                        model.YValue = model.YValue;
                    }
                    else if (model.FingerName == "R5")
                    {
                        model.YValue = model.YValue;
                    }
                    else
                    {
                        model.YValue = model.YValue + CountingUL;
                    }
                }
                else if (model.FingerType.StartsWith("WX"))
                {
                    model.YValue = model.FRCL + model.FRCR;
                }
                else if (model.FingerType.StartsWith("AR") || model.FingerType.StartsWith("AE"))
                {
                    model.YValue = CountingUL * 0.75m + 6.5m;
                }
                else if (model.FingerType.StartsWith("AU"))
                {
                    model.YValue = CountingUL * 0.75m + 6.5m;
                }
                else if (model.FingerType.StartsWith("AS") && noWhorl)
                {
                    if (CountingAS >= 1)
                        model.YValue = 8.3333m;
                    else if (CountingAS >= 2)
                        model.YValue = 7.5m;
                    else if (CountingAS >= 3)
                        model.YValue = 6.5m;
                    else if (CountingAS >= 4)
                        model.YValue = 5.5m;
                    else if (CountingAS >= 5)
                        model.YValue = 4.5m;
                    else if (CountingAS >= 6)
                        model.YValue = 3.5m;
                    else if (CountingAS >= 7)
                        model.YValue = 2.5m;
                }
                else if (model.FingerType.StartsWith("AS") && !noWhorl)
                {
                    if (CountingAS >= 1)
                        model.YValue = 8.3333m;
                    else if (CountingAS >= 2)
                        model.YValue = 7.5m;
                    else if (CountingAS >= 3)
                        model.YValue = 6.5m;
                    else if (CountingAS >= 4)
                        model.YValue = 5.5m;
                    else if (CountingAS >= 5)
                        model.YValue = 4.5m;
                    else if (CountingAS >= 6)
                        model.YValue = 3.5m;
                    else if (CountingAS >= 7)
                        model.YValue = 2.5m;

                    model.YValue = model.YValue + (CountingWhorl * 0.1m);
                }
                else if (model.FingerType.StartsWith("AT"))
                {
                    model.YValue = 10m;
                }
                else if (model.FingerType.StartsWith("W"))
                {
                    model.YValue = Math.Max(model.FRCL, model.FRCR);
                }
                else
                {
                    model.YValue = model.Sum_FRC / 2;
                }
                model.IsSet = true;
            }
        }

        public void CalculatePI2()
        {
            foreach (PIModel model in Collection)
            {
                model.CalculatePI2();
            }

            //Tính lại PI ngón trỏ
            if (L2.PIValue2 > 20 && R2.PIValue2 > 20)
            {
                if (L2.PIValue2 >= R2.PIValue2)
                {
                    L2.PIValue2 += 1.5m;
                }
                else
                {
                    R2.PIValue2 += 1.5m;
                }
            }
            else //(L2.PIValue < 20 || R2.PIValue < 20)
            {
                if (L2.PIValue2 <= R2.PIValue2)
                {
                    L2.PIValue2 -= 1.5m;
                }
                else
                {
                    R2.PIValue2 -= 1.5m;
                }
            }
            bool noWhorl = Collection.All(p => !p.FingerType.StartsWith("W"));

            if (noWhorl)
            {
                TFRC16 = Math.Round(Collection.Sum(p => p.PIValue2), 2);
            }
            else if (
                Collection.Count(p => p.FingerType.StartsWith("RL")) >= 10
                || Collection.Count(p => p.FingerType.StartsWith("LF")) >= 10
                || Collection.Count(p => p.FingerType.StartsWith("UL")) >= 10
                || Collection.Count(p => p.FingerType.StartsWith("WX")) >= 10
                )
            {
                TFRC16 = Math.Round(Collection.Sum(p => p.PIValue2), 2);
            }
            else if (
                Collection.Count(p => p.FingerType.StartsWith("A")) >= 10
                )
            {
                TFRC16 = Math.Round(Collection.Sum(p => p.PIValue2), 2);
            }
            else
            {
                TFRC16 = Math.Round(Collection.Sum(p => p.PIValue2), 2);
            }
        }

        public void CalculateRank()
        {
            List<PIModel> sortedList = this.Collection.OrderByDescending(p => p.PIValue).ToList();

            int rank = 1;
            sortedList[0].Rank = 1;
            for (int i = 1; i < sortedList.Count; i++)
            {
                if (sortedList[i].PIValue != sortedList[i - 1].PIValue)
                {
                    rank = i + 1;
                }
                sortedList[i].Rank = rank;
            }
        }

        public PIModel FindByFingerName(string name)
        {
            return Collection.FirstOrDefault(p => p.FingerName == name);
        }

        public PIModel FindByFingerType(string fingertype)
        {
            return Collection.FirstOrDefault(p => p.FingerType == fingertype);
        }

        public void MapToFingerAnalyse(FIFingerAnalysis analysis)
        {
            analysis.FPType1 = Math.Round(L1.PIValue, 2);
            analysis.FPType2 = Math.Round(L2.PIValue, 2);
            analysis.FPType3 = Math.Round(L3.PIValue, 2);
            analysis.FPType4 = Math.Round(L4.PIValue, 2);
            analysis.FPType5 = Math.Round(L5.PIValue, 2);

            analysis.FPType6 = Math.Round(R1.PIValue, 2);
            analysis.FPType7 = Math.Round(R2.PIValue, 2);
            analysis.FPType8 = Math.Round(R3.PIValue, 2);
            analysis.FPType9 = Math.Round(R4.PIValue, 2);
            analysis.FPType10 = Math.Round(R5.PIValue, 2);


            analysis.S1BZ = Math.Round(L1.NGFValue, 2);
            analysis.S2BZ = Math.Round(L2.NGFValue, 2);
            analysis.S3BZ = Math.Round(L3.NGFValue, 2);
            analysis.S4BZ = Math.Round(L4.NGFValue, 2);
            analysis.S5BZ = Math.Round(L5.NGFValue, 2);

            analysis.S6BZ = Math.Round(R1.NGFValue, 2);
            analysis.S7BZ = Math.Round(R2.NGFValue, 2);
            analysis.S8BZ = Math.Round(R3.NGFValue, 2);
            analysis.S9BZ = Math.Round(R4.NGFValue, 2);
            analysis.S10BZ = Math.Round(R5.NGFValue, 2);

            analysis.TFRC = Math.Round(TFRC, 2);
            analysis.Page_BLS = Math.Round(AFRC, 2);
        }
    }
}