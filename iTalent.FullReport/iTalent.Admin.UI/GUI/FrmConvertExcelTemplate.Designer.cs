﻿namespace iTalent.Admin.UI.GUI
{
    partial class FrmConvertExcelTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConvertExcelTemplate));
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.txtInputNGFtemplate = new System.Windows.Forms.TextBox();
            this.lblInputFile = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.lblOutputFile = new System.Windows.Forms.Label();
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnExit = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.btnInputKhuyenNghi_Kid = new System.Windows.Forms.Button();
            this.txtInputKhuyenNghi_Kid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnInputHanhVi_Kid = new System.Windows.Forms.Button();
            this.txtInputHanhVi_Kid = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnInputKhuyenNghi_Adult = new System.Windows.Forms.Button();
            this.txtInputKhuyenNghi_Adult = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnInputHanhVi_Adult = new System.Windows.Forms.Button();
            this.txtInputHanhVi_Adult = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnInputTop3 = new System.Windows.Forms.Button();
            this.txtInputTop3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnInputDacTinhTiemThuc = new System.Windows.Forms.Button();
            this.txtInputDacTinhTiemThuc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnInputDacTinhTuDuy = new System.Windows.Forms.Button();
            this.txtInputDacTinhTuDuy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnInputHeSoTiemNang = new System.Windows.Forms.Button();
            this.txtInputHeSoTiemNang = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowseOutput = new System.Windows.Forms.Button();
            this.btnInputNGFTemplate = new System.Windows.Forms.Button();
            this.btnToApk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            // 
            // txtInputNGFtemplate
            // 
            this.txtInputNGFtemplate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputNGFtemplate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputNGFtemplate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputNGFtemplate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputNGFtemplate.Location = new System.Drawing.Point(203, 17);
            this.txtInputNGFtemplate.Name = "txtInputNGFtemplate";
            this.txtInputNGFtemplate.ReadOnly = true;
            this.txtInputNGFtemplate.Size = new System.Drawing.Size(494, 29);
            this.txtInputNGFtemplate.TabIndex = 192;
            // 
            // lblInputFile
            // 
            this.lblInputFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblInputFile.BackColor = System.Drawing.Color.Transparent;
            this.lblInputFile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputFile.Location = new System.Drawing.Point(7, 20);
            this.lblInputFile.Name = "lblInputFile";
            this.lblInputFile.Size = new System.Drawing.Size(190, 21);
            this.lblInputFile.TabIndex = 199;
            this.lblInputFile.Tag = "File Nhập :";
            this.lblInputFile.Text = "1.NGFTemplate";
            this.lblInputFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOutput
            // 
            this.txtOutput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtOutput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtOutput.Location = new System.Drawing.Point(203, 350);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.Size = new System.Drawing.Size(494, 29);
            this.txtOutput.TabIndex = 187;
            // 
            // lblOutputFile
            // 
            this.lblOutputFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOutputFile.BackColor = System.Drawing.Color.Transparent;
            this.lblOutputFile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutputFile.Location = new System.Drawing.Point(9, 353);
            this.lblOutputFile.Name = "lblOutputFile";
            this.lblOutputFile.Size = new System.Drawing.Size(188, 21);
            this.lblOutputFile.TabIndex = 194;
            this.lblOutputFile.Tag = "File Xuất :";
            this.lblOutputFile.Text = "Output File :";
            this.lblOutputFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.btnExit});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputKhuyenNghi_Kid);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputKhuyenNghi_Kid);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label8);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputHanhVi_Kid);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputHanhVi_Kid);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label7);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputKhuyenNghi_Adult);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputKhuyenNghi_Adult);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label6);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputHanhVi_Adult);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputHanhVi_Adult);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label5);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputTop3);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputTop3);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label4);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputDacTinhTiemThuc);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputDacTinhTiemThuc);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label3);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputDacTinhTuDuy);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputDacTinhTuDuy);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label2);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputHeSoTiemNang);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputHeSoTiemNang);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnBrowseOutput);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnInputNGFTemplate);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnToApk);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtInputNGFtemplate);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.lblInputFile);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtOutput);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.lblOutputFile);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(763, 493);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 151;
            this.c4FunHeaderGroup1.Tag = "Chuyển Đổi";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Convert File Excel";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.Admin.UI.Properties.Resources.refresh_32;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "";
            // 
            // btnExit
            // 
            this.btnExit.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnExit.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnInputKhuyenNghi_Kid
            // 
            this.btnInputKhuyenNghi_Kid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputKhuyenNghi_Kid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputKhuyenNghi_Kid.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputKhuyenNghi_Kid.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputKhuyenNghi_Kid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputKhuyenNghi_Kid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputKhuyenNghi_Kid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputKhuyenNghi_Kid.ForeColor = System.Drawing.Color.White;
            this.btnInputKhuyenNghi_Kid.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputKhuyenNghi_Kid.Location = new System.Drawing.Point(701, 297);
            this.btnInputKhuyenNghi_Kid.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputKhuyenNghi_Kid.Name = "btnInputKhuyenNghi_Kid";
            this.btnInputKhuyenNghi_Kid.Size = new System.Drawing.Size(37, 29);
            this.btnInputKhuyenNghi_Kid.TabIndex = 226;
            this.btnInputKhuyenNghi_Kid.Tag = "...";
            this.btnInputKhuyenNghi_Kid.Text = "...";
            this.btnInputKhuyenNghi_Kid.UseVisualStyleBackColor = false;
            this.btnInputKhuyenNghi_Kid.Click += new System.EventHandler(this.btnInputKhuyenNghi_Kid_Click);
            // 
            // txtInputKhuyenNghi_Kid
            // 
            this.txtInputKhuyenNghi_Kid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputKhuyenNghi_Kid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputKhuyenNghi_Kid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputKhuyenNghi_Kid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputKhuyenNghi_Kid.Location = new System.Drawing.Point(203, 297);
            this.txtInputKhuyenNghi_Kid.Name = "txtInputKhuyenNghi_Kid";
            this.txtInputKhuyenNghi_Kid.ReadOnly = true;
            this.txtInputKhuyenNghi_Kid.Size = new System.Drawing.Size(494, 29);
            this.txtInputKhuyenNghi_Kid.TabIndex = 224;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 300);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(190, 21);
            this.label8.TabIndex = 225;
            this.label8.Tag = "File Nhập :";
            this.label8.Text = "9.KhuyenNghi_Kid";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputHanhVi_Kid
            // 
            this.btnInputHanhVi_Kid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputHanhVi_Kid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputHanhVi_Kid.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputHanhVi_Kid.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputHanhVi_Kid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputHanhVi_Kid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputHanhVi_Kid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputHanhVi_Kid.ForeColor = System.Drawing.Color.White;
            this.btnInputHanhVi_Kid.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputHanhVi_Kid.Location = new System.Drawing.Point(701, 262);
            this.btnInputHanhVi_Kid.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputHanhVi_Kid.Name = "btnInputHanhVi_Kid";
            this.btnInputHanhVi_Kid.Size = new System.Drawing.Size(37, 29);
            this.btnInputHanhVi_Kid.TabIndex = 223;
            this.btnInputHanhVi_Kid.Tag = "...";
            this.btnInputHanhVi_Kid.Text = "...";
            this.btnInputHanhVi_Kid.UseVisualStyleBackColor = false;
            this.btnInputHanhVi_Kid.Click += new System.EventHandler(this.btnInputHanhVi_Kid_Click);
            // 
            // txtInputHanhVi_Kid
            // 
            this.txtInputHanhVi_Kid.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputHanhVi_Kid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputHanhVi_Kid.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputHanhVi_Kid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputHanhVi_Kid.Location = new System.Drawing.Point(203, 262);
            this.txtInputHanhVi_Kid.Name = "txtInputHanhVi_Kid";
            this.txtInputHanhVi_Kid.ReadOnly = true;
            this.txtInputHanhVi_Kid.Size = new System.Drawing.Size(494, 29);
            this.txtInputHanhVi_Kid.TabIndex = 221;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 265);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(190, 21);
            this.label7.TabIndex = 222;
            this.label7.Tag = "File Nhập :";
            this.label7.Text = "8.TinhCach_HanhVi_Kid";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputKhuyenNghi_Adult
            // 
            this.btnInputKhuyenNghi_Adult.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputKhuyenNghi_Adult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputKhuyenNghi_Adult.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputKhuyenNghi_Adult.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputKhuyenNghi_Adult.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputKhuyenNghi_Adult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputKhuyenNghi_Adult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputKhuyenNghi_Adult.ForeColor = System.Drawing.Color.White;
            this.btnInputKhuyenNghi_Adult.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputKhuyenNghi_Adult.Location = new System.Drawing.Point(701, 227);
            this.btnInputKhuyenNghi_Adult.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputKhuyenNghi_Adult.Name = "btnInputKhuyenNghi_Adult";
            this.btnInputKhuyenNghi_Adult.Size = new System.Drawing.Size(37, 29);
            this.btnInputKhuyenNghi_Adult.TabIndex = 220;
            this.btnInputKhuyenNghi_Adult.Tag = "...";
            this.btnInputKhuyenNghi_Adult.Text = "...";
            this.btnInputKhuyenNghi_Adult.UseVisualStyleBackColor = false;
            this.btnInputKhuyenNghi_Adult.Click += new System.EventHandler(this.btnInputKhuyenNghi_Adult_Click);
            // 
            // txtInputKhuyenNghi_Adult
            // 
            this.txtInputKhuyenNghi_Adult.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputKhuyenNghi_Adult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputKhuyenNghi_Adult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputKhuyenNghi_Adult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputKhuyenNghi_Adult.Location = new System.Drawing.Point(203, 227);
            this.txtInputKhuyenNghi_Adult.Name = "txtInputKhuyenNghi_Adult";
            this.txtInputKhuyenNghi_Adult.ReadOnly = true;
            this.txtInputKhuyenNghi_Adult.Size = new System.Drawing.Size(494, 29);
            this.txtInputKhuyenNghi_Adult.TabIndex = 218;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(190, 21);
            this.label6.TabIndex = 219;
            this.label6.Tag = "File Nhập :";
            this.label6.Text = "7.KhuyenNghi_Adult";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputHanhVi_Adult
            // 
            this.btnInputHanhVi_Adult.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputHanhVi_Adult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputHanhVi_Adult.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputHanhVi_Adult.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputHanhVi_Adult.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputHanhVi_Adult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputHanhVi_Adult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputHanhVi_Adult.ForeColor = System.Drawing.Color.White;
            this.btnInputHanhVi_Adult.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputHanhVi_Adult.Location = new System.Drawing.Point(701, 192);
            this.btnInputHanhVi_Adult.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputHanhVi_Adult.Name = "btnInputHanhVi_Adult";
            this.btnInputHanhVi_Adult.Size = new System.Drawing.Size(37, 29);
            this.btnInputHanhVi_Adult.TabIndex = 217;
            this.btnInputHanhVi_Adult.Tag = "...";
            this.btnInputHanhVi_Adult.Text = "...";
            this.btnInputHanhVi_Adult.UseVisualStyleBackColor = false;
            this.btnInputHanhVi_Adult.Click += new System.EventHandler(this.btnInputHanhVi_Adult_Click);
            // 
            // txtInputHanhVi_Adult
            // 
            this.txtInputHanhVi_Adult.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputHanhVi_Adult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputHanhVi_Adult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputHanhVi_Adult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputHanhVi_Adult.Location = new System.Drawing.Point(203, 192);
            this.txtInputHanhVi_Adult.Name = "txtInputHanhVi_Adult";
            this.txtInputHanhVi_Adult.ReadOnly = true;
            this.txtInputHanhVi_Adult.Size = new System.Drawing.Size(494, 29);
            this.txtInputHanhVi_Adult.TabIndex = 215;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(190, 21);
            this.label5.TabIndex = 216;
            this.label5.Tag = "File Nhập :";
            this.label5.Text = "6.TinhCach_HanhVi_Adult";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputTop3
            // 
            this.btnInputTop3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputTop3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputTop3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputTop3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputTop3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputTop3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputTop3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputTop3.ForeColor = System.Drawing.Color.White;
            this.btnInputTop3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputTop3.Location = new System.Drawing.Point(701, 157);
            this.btnInputTop3.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputTop3.Name = "btnInputTop3";
            this.btnInputTop3.Size = new System.Drawing.Size(37, 29);
            this.btnInputTop3.TabIndex = 214;
            this.btnInputTop3.Tag = "...";
            this.btnInputTop3.Text = "...";
            this.btnInputTop3.UseVisualStyleBackColor = false;
            this.btnInputTop3.Click += new System.EventHandler(this.btnInputTop3_Click);
            // 
            // txtInputTop3
            // 
            this.txtInputTop3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputTop3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputTop3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputTop3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputTop3.Location = new System.Drawing.Point(203, 157);
            this.txtInputTop3.Name = "txtInputTop3";
            this.txtInputTop3.ReadOnly = true;
            this.txtInputTop3.Size = new System.Drawing.Size(494, 29);
            this.txtInputTop3.TabIndex = 212;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(190, 21);
            this.label4.TabIndex = 213;
            this.label4.Tag = "File Nhập :";
            this.label4.Text = "5.Top3in16Description";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputDacTinhTiemThuc
            // 
            this.btnInputDacTinhTiemThuc.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputDacTinhTiemThuc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputDacTinhTiemThuc.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputDacTinhTiemThuc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputDacTinhTiemThuc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputDacTinhTiemThuc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputDacTinhTiemThuc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputDacTinhTiemThuc.ForeColor = System.Drawing.Color.White;
            this.btnInputDacTinhTiemThuc.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputDacTinhTiemThuc.Location = new System.Drawing.Point(701, 122);
            this.btnInputDacTinhTiemThuc.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputDacTinhTiemThuc.Name = "btnInputDacTinhTiemThuc";
            this.btnInputDacTinhTiemThuc.Size = new System.Drawing.Size(37, 29);
            this.btnInputDacTinhTiemThuc.TabIndex = 211;
            this.btnInputDacTinhTiemThuc.Tag = "...";
            this.btnInputDacTinhTiemThuc.Text = "...";
            this.btnInputDacTinhTiemThuc.UseVisualStyleBackColor = false;
            this.btnInputDacTinhTiemThuc.Click += new System.EventHandler(this.btnInputDacTinhTiemThuc_Click);
            // 
            // txtInputDacTinhTiemThuc
            // 
            this.txtInputDacTinhTiemThuc.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputDacTinhTiemThuc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputDacTinhTiemThuc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputDacTinhTiemThuc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputDacTinhTiemThuc.Location = new System.Drawing.Point(203, 122);
            this.txtInputDacTinhTiemThuc.Name = "txtInputDacTinhTiemThuc";
            this.txtInputDacTinhTiemThuc.ReadOnly = true;
            this.txtInputDacTinhTiemThuc.Size = new System.Drawing.Size(494, 29);
            this.txtInputDacTinhTiemThuc.TabIndex = 209;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(190, 21);
            this.label3.TabIndex = 210;
            this.label3.Tag = "File Nhập :";
            this.label3.Text = "4.DacTinhTiemThuc";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputDacTinhTuDuy
            // 
            this.btnInputDacTinhTuDuy.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputDacTinhTuDuy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputDacTinhTuDuy.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputDacTinhTuDuy.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputDacTinhTuDuy.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputDacTinhTuDuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputDacTinhTuDuy.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputDacTinhTuDuy.ForeColor = System.Drawing.Color.White;
            this.btnInputDacTinhTuDuy.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputDacTinhTuDuy.Location = new System.Drawing.Point(701, 87);
            this.btnInputDacTinhTuDuy.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputDacTinhTuDuy.Name = "btnInputDacTinhTuDuy";
            this.btnInputDacTinhTuDuy.Size = new System.Drawing.Size(37, 29);
            this.btnInputDacTinhTuDuy.TabIndex = 208;
            this.btnInputDacTinhTuDuy.Tag = "...";
            this.btnInputDacTinhTuDuy.Text = "...";
            this.btnInputDacTinhTuDuy.UseVisualStyleBackColor = false;
            this.btnInputDacTinhTuDuy.Click += new System.EventHandler(this.btnInputDacTinhTuDuy_Click);
            // 
            // txtInputDacTinhTuDuy
            // 
            this.txtInputDacTinhTuDuy.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputDacTinhTuDuy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputDacTinhTuDuy.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputDacTinhTuDuy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputDacTinhTuDuy.Location = new System.Drawing.Point(203, 87);
            this.txtInputDacTinhTuDuy.Name = "txtInputDacTinhTuDuy";
            this.txtInputDacTinhTuDuy.ReadOnly = true;
            this.txtInputDacTinhTuDuy.Size = new System.Drawing.Size(494, 29);
            this.txtInputDacTinhTuDuy.TabIndex = 206;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 21);
            this.label2.TabIndex = 207;
            this.label2.Tag = "File Nhập :";
            this.label2.Text = "3.DacTinhTuDuy";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInputHeSoTiemNang
            // 
            this.btnInputHeSoTiemNang.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputHeSoTiemNang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputHeSoTiemNang.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputHeSoTiemNang.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputHeSoTiemNang.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputHeSoTiemNang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputHeSoTiemNang.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputHeSoTiemNang.ForeColor = System.Drawing.Color.White;
            this.btnInputHeSoTiemNang.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputHeSoTiemNang.Location = new System.Drawing.Point(701, 52);
            this.btnInputHeSoTiemNang.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputHeSoTiemNang.Name = "btnInputHeSoTiemNang";
            this.btnInputHeSoTiemNang.Size = new System.Drawing.Size(37, 29);
            this.btnInputHeSoTiemNang.TabIndex = 205;
            this.btnInputHeSoTiemNang.Tag = "...";
            this.btnInputHeSoTiemNang.Text = "...";
            this.btnInputHeSoTiemNang.UseVisualStyleBackColor = false;
            this.btnInputHeSoTiemNang.Click += new System.EventHandler(this.btnInputHeSoTiemNang_Click);
            // 
            // txtInputHeSoTiemNang
            // 
            this.txtInputHeSoTiemNang.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInputHeSoTiemNang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtInputHeSoTiemNang.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputHeSoTiemNang.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtInputHeSoTiemNang.Location = new System.Drawing.Point(203, 52);
            this.txtInputHeSoTiemNang.Name = "txtInputHeSoTiemNang";
            this.txtInputHeSoTiemNang.ReadOnly = true;
            this.txtInputHeSoTiemNang.Size = new System.Drawing.Size(494, 29);
            this.txtInputHeSoTiemNang.TabIndex = 203;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 21);
            this.label1.TabIndex = 204;
            this.label1.Tag = "File Nhập :";
            this.label1.Text = "2.HeSoTiemNang";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBrowseOutput
            // 
            this.btnBrowseOutput.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnBrowseOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBrowseOutput.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnBrowseOutput.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnBrowseOutput.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnBrowseOutput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowseOutput.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseOutput.ForeColor = System.Drawing.Color.White;
            this.btnBrowseOutput.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBrowseOutput.Location = new System.Drawing.Point(701, 350);
            this.btnBrowseOutput.Margin = new System.Windows.Forms.Padding(0);
            this.btnBrowseOutput.Name = "btnBrowseOutput";
            this.btnBrowseOutput.Size = new System.Drawing.Size(37, 29);
            this.btnBrowseOutput.TabIndex = 202;
            this.btnBrowseOutput.Tag = "...";
            this.btnBrowseOutput.Text = "...";
            this.btnBrowseOutput.UseVisualStyleBackColor = false;
            this.btnBrowseOutput.Click += new System.EventHandler(this.btnBrowseOutput_Click);
            // 
            // btnInputNGFTemplate
            // 
            this.btnInputNGFTemplate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnInputNGFTemplate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnInputNGFTemplate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnInputNGFTemplate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnInputNGFTemplate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnInputNGFTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInputNGFTemplate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputNGFTemplate.ForeColor = System.Drawing.Color.White;
            this.btnInputNGFTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInputNGFTemplate.Location = new System.Drawing.Point(701, 17);
            this.btnInputNGFTemplate.Margin = new System.Windows.Forms.Padding(0);
            this.btnInputNGFTemplate.Name = "btnInputNGFTemplate";
            this.btnInputNGFTemplate.Size = new System.Drawing.Size(37, 29);
            this.btnInputNGFTemplate.TabIndex = 201;
            this.btnInputNGFTemplate.Tag = "...";
            this.btnInputNGFTemplate.Text = "...";
            this.btnInputNGFTemplate.UseVisualStyleBackColor = false;
            this.btnInputNGFTemplate.Click += new System.EventHandler(this.btnBrowseInput_Click);
            // 
            // btnToApk
            // 
            this.btnToApk.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnToApk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnToApk.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnToApk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnToApk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnToApk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnToApk.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToApk.ForeColor = System.Drawing.Color.White;
            this.btnToApk.Image = global::iTalent.Admin.UI.Properties.Resources.right;
            this.btnToApk.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnToApk.Location = new System.Drawing.Point(544, 392);
            this.btnToApk.Margin = new System.Windows.Forms.Padding(0);
            this.btnToApk.Name = "btnToApk";
            this.btnToApk.Size = new System.Drawing.Size(153, 41);
            this.btnToApk.TabIndex = 186;
            this.btnToApk.Tag = "Rptx";
            this.btnToApk.Text = "Convert";
            this.btnToApk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnToApk.UseVisualStyleBackColor = false;
            this.btnToApk.Click += new System.EventHandler(this.btnToRptx_Click);
            // 
            // FrmConvertExcelTemplate
            // 
            this.AcceptButton = this.btnToApk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 493);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConvertExcelTemplate";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private System.Windows.Forms.TextBox txtInputNGFtemplate;
        private System.Windows.Forms.Label lblInputFile;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Label lblOutputFile;
        private System.Windows.Forms.Button btnToApk;
        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnExit;
        private System.Windows.Forms.Button btnInputNGFTemplate;
        private System.Windows.Forms.Button btnBrowseOutput;
        private System.Windows.Forms.Button btnInputKhuyenNghi_Kid;
        private System.Windows.Forms.TextBox txtInputKhuyenNghi_Kid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnInputHanhVi_Kid;
        private System.Windows.Forms.TextBox txtInputHanhVi_Kid;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnInputKhuyenNghi_Adult;
        private System.Windows.Forms.TextBox txtInputKhuyenNghi_Adult;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnInputHanhVi_Adult;
        private System.Windows.Forms.TextBox txtInputHanhVi_Adult;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnInputTop3;
        private System.Windows.Forms.TextBox txtInputTop3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnInputDacTinhTiemThuc;
        private System.Windows.Forms.TextBox txtInputDacTinhTiemThuc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnInputDacTinhTuDuy;
        private System.Windows.Forms.TextBox txtInputDacTinhTuDuy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnInputHeSoTiemNang;
        private System.Windows.Forms.TextBox txtInputHeSoTiemNang;
        private System.Windows.Forms.Label label1;
    }
}