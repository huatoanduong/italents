﻿namespace iTalent.Admin.UI.GUI
{
    partial class FrmKeys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmKeys));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.buttonSpecHeaderGroup1 = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.dgvData = new C4FunComponent.Toolkit.C4FunDataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAgencyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDateCreate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGenerateVIP = new System.Windows.Forms.Button();
            this.btnGeneratePremium = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtNumberOfkey = new System.Windows.Forms.TextBox();
            this.btnCopyKey = new System.Windows.Forms.Button();
            this.labKey = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.labNumber = new System.Windows.Forms.Label();
            this.btnGenerateSimple = new System.Windows.Forms.Button();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSLTongSimple = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSLTongPremium = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labNumberSum = new System.Windows.Forms.Label();
            this.pnlLine = new System.Windows.Forms.Panel();
            this.txtSLTongVIP = new System.Windows.Forms.TextBox();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.labName = new System.Windows.Forms.Label();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.labAddress = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.labID = new System.Windows.Forms.Label();
            this.labCity = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.labPhone = new System.Windows.Forms.Label();
            this.labEmail = new System.Windows.Forms.Label();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.labCellPhone = new System.Windows.Forms.Label();
            this.btnUltimate = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSLTongUltimate = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.buttonSpecHeaderGroup1});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.dgvData);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.panel1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pnlInfo);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(1022, 766);
            this.c4FunHeaderGroup1.StateNormal.Back.Color1 = System.Drawing.Color.Honeydew;
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 151;
            this.c4FunHeaderGroup1.Tag = "Thông Tin Người Dùng";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Agency Keys";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.Admin.UI.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "";
            // 
            // buttonSpecHeaderGroup1
            // 
            this.buttonSpecHeaderGroup1.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.buttonSpecHeaderGroup1.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.buttonSpecHeaderGroup1.Click += new System.EventHandler(this.buttonSpecHeaderGroup1_Click);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToResizeRows = false;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvData.ColumnHeadersHeight = 30;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colAgencyID,
            this.colStt,
            this.colDateCreate,
            this.colNumber,
            this.colKey});
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvData.GridStyles.Style = C4FunComponent.Toolkit.DataGridViewStyle.Sheet;
            this.dgvData.GridStyles.StyleBackground = C4FunComponent.Toolkit.PaletteBackStyle.GridBackgroundSheet;
            this.dgvData.GridStyles.StyleColumn = C4FunComponent.Toolkit.GridStyle.Sheet;
            this.dgvData.GridStyles.StyleDataCells = C4FunComponent.Toolkit.GridStyle.Sheet;
            this.dgvData.GridStyles.StyleRow = C4FunComponent.Toolkit.GridStyle.Sheet;
            this.dgvData.Location = new System.Drawing.Point(364, 145);
            this.dgvData.MultiSelect = false;
            this.dgvData.Name = "dgvData";
            this.dgvData.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            this.dgvData.ReadOnly = true;
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(656, 582);
            this.dgvData.StateCommon.BackStyle = C4FunComponent.Toolkit.PaletteBackStyle.GridBackgroundSheet;
            this.dgvData.StateCommon.DataCell.Content.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvData.StateCommon.HeaderColumn.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgvData.StateCommon.HeaderColumn.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.dgvData.StateCommon.HeaderColumn.Content.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvData.TabIndex = 205;
            this.dgvData.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvData_RowPrePaint);
            this.dgvData.SelectionChanged += new System.EventHandler(this.dgvData_SelectionChanged);
            // 
            // colID
            // 
            this.colID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colID.DataPropertyName = "ID";
            this.colID.Frozen = true;
            this.colID.HeaderText = "ID";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            this.colID.Visible = false;
            this.colID.Width = 5;
            // 
            // colAgencyID
            // 
            this.colAgencyID.DataPropertyName = "AgencyID";
            this.colAgencyID.HeaderText = "AgencyID";
            this.colAgencyID.Name = "colAgencyID";
            this.colAgencyID.ReadOnly = true;
            this.colAgencyID.Visible = false;
            // 
            // colStt
            // 
            this.colStt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colStt.FillWeight = 76.14214F;
            this.colStt.HeaderText = "Stt";
            this.colStt.Name = "colStt";
            this.colStt.ReadOnly = true;
            this.colStt.Width = 30;
            // 
            // colDateCreate
            // 
            this.colDateCreate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colDateCreate.DataPropertyName = "PointDate";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.colDateCreate.DefaultCellStyle = dataGridViewCellStyle1;
            this.colDateCreate.FillWeight = 105.9645F;
            this.colDateCreate.HeaderText = "Date Create";
            this.colDateCreate.Name = "colDateCreate";
            this.colDateCreate.ReadOnly = true;
            this.colDateCreate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.colDateCreate.Width = 120;
            // 
            // colNumber
            // 
            this.colNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colNumber.DataPropertyName = "PointOriginal";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = "0";
            this.colNumber.DefaultCellStyle = dataGridViewCellStyle2;
            this.colNumber.FillWeight = 105.9645F;
            this.colNumber.HeaderText = "Number";
            this.colNumber.Name = "colNumber";
            this.colNumber.ReadOnly = true;
            this.colNumber.Width = 97;
            // 
            // colKey
            // 
            this.colKey.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colKey.DataPropertyName = "KeyOriginal";
            this.colKey.FillWeight = 105.9645F;
            this.colKey.HeaderText = "Key";
            this.colKey.Name = "colKey";
            this.colKey.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnUltimate);
            this.panel1.Controls.Add(this.btnGenerateVIP);
            this.panel1.Controls.Add(this.btnGeneratePremium);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.txtNumberOfkey);
            this.panel1.Controls.Add(this.btnCopyKey);
            this.panel1.Controls.Add(this.labKey);
            this.panel1.Controls.Add(this.txtKey);
            this.panel1.Controls.Add(this.labNumber);
            this.panel1.Controls.Add(this.btnGenerateSimple);
            this.panel1.Controls.Add(this.txtNumber);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(364, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(656, 145);
            this.panel1.TabIndex = 204;
            // 
            // btnGenerateVIP
            // 
            this.btnGenerateVIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnGenerateVIP.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGenerateVIP.FlatAppearance.BorderSize = 0;
            this.btnGenerateVIP.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnGenerateVIP.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnGenerateVIP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateVIP.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateVIP.ForeColor = System.Drawing.Color.White;
            this.btnGenerateVIP.Image = global::iTalent.Admin.UI.Properties.Resources.keys;
            this.btnGenerateVIP.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGenerateVIP.Location = new System.Drawing.Point(394, 12);
            this.btnGenerateVIP.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerateVIP.Name = "btnGenerateVIP";
            this.btnGenerateVIP.Size = new System.Drawing.Size(122, 60);
            this.btnGenerateVIP.TabIndex = 211;
            this.btnGenerateVIP.Tag = "";
            this.btnGenerateVIP.Text = "VIP";
            this.btnGenerateVIP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerateVIP.UseVisualStyleBackColor = false;
            this.btnGenerateVIP.Click += new System.EventHandler(this.btnGenerateVIP_Click);
            // 
            // btnGeneratePremium
            // 
            this.btnGeneratePremium.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnGeneratePremium.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGeneratePremium.FlatAppearance.BorderSize = 0;
            this.btnGeneratePremium.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnGeneratePremium.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnGeneratePremium.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGeneratePremium.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGeneratePremium.ForeColor = System.Drawing.Color.White;
            this.btnGeneratePremium.Image = global::iTalent.Admin.UI.Properties.Resources.keys;
            this.btnGeneratePremium.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGeneratePremium.Location = new System.Drawing.Point(266, 12);
            this.btnGeneratePremium.Margin = new System.Windows.Forms.Padding(0);
            this.btnGeneratePremium.Name = "btnGeneratePremium";
            this.btnGeneratePremium.Size = new System.Drawing.Size(122, 60);
            this.btnGeneratePremium.TabIndex = 210;
            this.btnGeneratePremium.Tag = "";
            this.btnGeneratePremium.Text = "Premium";
            this.btnGeneratePremium.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGeneratePremium.UseVisualStyleBackColor = false;
            this.btnGeneratePremium.Click += new System.EventHandler(this.btnGeneratePremium_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Location = new System.Drawing.Point(0, 76);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(655, 1);
            this.panel2.TabIndex = 209;
            // 
            // txtNumberOfkey
            // 
            this.txtNumberOfkey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtNumberOfkey.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumberOfkey.ForeColor = System.Drawing.Color.Red;
            this.txtNumberOfkey.Location = new System.Drawing.Point(138, 84);
            this.txtNumberOfkey.Name = "txtNumberOfkey";
            this.txtNumberOfkey.ReadOnly = true;
            this.txtNumberOfkey.Size = new System.Drawing.Size(95, 46);
            this.txtNumberOfkey.TabIndex = 208;
            // 
            // btnCopyKey
            // 
            this.btnCopyKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCopyKey.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCopyKey.FlatAppearance.BorderSize = 0;
            this.btnCopyKey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnCopyKey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnCopyKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopyKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopyKey.ForeColor = System.Drawing.Color.White;
            this.btnCopyKey.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCopyKey.Location = new System.Drawing.Point(576, 80);
            this.btnCopyKey.Margin = new System.Windows.Forms.Padding(0);
            this.btnCopyKey.Name = "btnCopyKey";
            this.btnCopyKey.Size = new System.Drawing.Size(72, 59);
            this.btnCopyKey.TabIndex = 207;
            this.btnCopyKey.Tag = "";
            this.btnCopyKey.Text = "Copy Key";
            this.btnCopyKey.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCopyKey.UseVisualStyleBackColor = false;
            this.btnCopyKey.Click += new System.EventHandler(this.btnCopyKey_Click);
            // 
            // labKey
            // 
            this.labKey.BackColor = System.Drawing.Color.Transparent;
            this.labKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.labKey.Location = new System.Drawing.Point(6, 97);
            this.labKey.Name = "labKey";
            this.labKey.Size = new System.Drawing.Size(126, 21);
            this.labKey.TabIndex = 206;
            this.labKey.Tag = "Tên :";
            this.labKey.Text = "Mã Bản Quyền :";
            this.labKey.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKey
            // 
            this.txtKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.ForeColor = System.Drawing.Color.Red;
            this.txtKey.Location = new System.Drawing.Point(236, 80);
            this.txtKey.Multiline = true;
            this.txtKey.Name = "txtKey";
            this.txtKey.ReadOnly = true;
            this.txtKey.Size = new System.Drawing.Size(337, 59);
            this.txtKey.TabIndex = 205;
            // 
            // labNumber
            // 
            this.labNumber.BackColor = System.Drawing.Color.Transparent;
            this.labNumber.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.labNumber.Location = new System.Drawing.Point(2, 2);
            this.labNumber.Name = "labNumber";
            this.labNumber.Size = new System.Drawing.Size(126, 21);
            this.labNumber.TabIndex = 204;
            this.labNumber.Tag = "Tên :";
            this.labNumber.Text = "SL Cấp Báo Cáo :";
            this.labNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnGenerateSimple
            // 
            this.btnGenerateSimple.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnGenerateSimple.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGenerateSimple.FlatAppearance.BorderSize = 0;
            this.btnGenerateSimple.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnGenerateSimple.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnGenerateSimple.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateSimple.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateSimple.ForeColor = System.Drawing.Color.White;
            this.btnGenerateSimple.Image = global::iTalent.Admin.UI.Properties.Resources.keys;
            this.btnGenerateSimple.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGenerateSimple.Location = new System.Drawing.Point(138, 12);
            this.btnGenerateSimple.Margin = new System.Windows.Forms.Padding(0);
            this.btnGenerateSimple.Name = "btnGenerateSimple";
            this.btnGenerateSimple.Size = new System.Drawing.Size(122, 60);
            this.btnGenerateSimple.TabIndex = 186;
            this.btnGenerateSimple.Tag = "";
            this.btnGenerateSimple.Text = "Simple";
            this.btnGenerateSimple.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerateSimple.UseVisualStyleBackColor = false;
            this.btnGenerateSimple.Click += new System.EventHandler(this.btnGenerateSimple_Click);
            // 
            // txtNumber
            // 
            this.txtNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtNumber.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumber.ForeColor = System.Drawing.Color.Red;
            this.txtNumber.Location = new System.Drawing.Point(6, 26);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(110, 46);
            this.txtNumber.TabIndex = 203;
            this.txtNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber_KeyPress);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.Transparent;
            this.pnlInfo.Controls.Add(this.label4);
            this.pnlInfo.Controls.Add(this.txtSLTongUltimate);
            this.pnlInfo.Controls.Add(this.label3);
            this.pnlInfo.Controls.Add(this.txtSLTongSimple);
            this.pnlInfo.Controls.Add(this.label2);
            this.pnlInfo.Controls.Add(this.txtSLTongPremium);
            this.pnlInfo.Controls.Add(this.label1);
            this.pnlInfo.Controls.Add(this.labNumberSum);
            this.pnlInfo.Controls.Add(this.pnlLine);
            this.pnlInfo.Controls.Add(this.txtSLTongVIP);
            this.pnlInfo.Controls.Add(this.txtThanhPho);
            this.pnlInfo.Controls.Add(this.labName);
            this.pnlInfo.Controls.Add(this.txtTen);
            this.pnlInfo.Controls.Add(this.labAddress);
            this.pnlInfo.Controls.Add(this.txtID);
            this.pnlInfo.Controls.Add(this.txtDiaChi);
            this.pnlInfo.Controls.Add(this.labID);
            this.pnlInfo.Controls.Add(this.labCity);
            this.pnlInfo.Controls.Add(this.txtEmail);
            this.pnlInfo.Controls.Add(this.labPhone);
            this.pnlInfo.Controls.Add(this.labEmail);
            this.pnlInfo.Controls.Add(this.txtDienThoai);
            this.pnlInfo.Controls.Add(this.txtDiDong);
            this.pnlInfo.Controls.Add(this.labCellPhone);
            this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlInfo.Location = new System.Drawing.Point(0, 0);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(364, 727);
            this.pnlInfo.TabIndex = 203;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Maroon;
            this.label3.Location = new System.Drawing.Point(14, 468);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 35);
            this.label3.TabIndex = 214;
            this.label3.Tag = "";
            this.label3.Text = "SIMPLE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSLTongSimple
            // 
            this.txtSLTongSimple.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtSLTongSimple.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSLTongSimple.ForeColor = System.Drawing.Color.Maroon;
            this.txtSLTongSimple.Location = new System.Drawing.Point(175, 462);
            this.txtSLTongSimple.Name = "txtSLTongSimple";
            this.txtSLTongSimple.ReadOnly = true;
            this.txtSLTongSimple.Size = new System.Drawing.Size(182, 46);
            this.txtSLTongSimple.TabIndex = 213;
            this.txtSLTongSimple.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(14, 416);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 35);
            this.label2.TabIndex = 212;
            this.label2.Tag = "";
            this.label2.Text = "PREMIUM";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSLTongPremium
            // 
            this.txtSLTongPremium.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtSLTongPremium.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSLTongPremium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtSLTongPremium.Location = new System.Drawing.Point(175, 410);
            this.txtSLTongPremium.Name = "txtSLTongPremium";
            this.txtSLTongPremium.ReadOnly = true;
            this.txtSLTongPremium.Size = new System.Drawing.Size(182, 46);
            this.txtSLTongPremium.TabIndex = 211;
            this.txtSLTongPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(14, 364);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 35);
            this.label1.TabIndex = 210;
            this.label1.Tag = "";
            this.label1.Text = "VIP";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labNumberSum
            // 
            this.labNumberSum.BackColor = System.Drawing.Color.Transparent;
            this.labNumberSum.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNumberSum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.labNumberSum.Location = new System.Drawing.Point(231, 282);
            this.labNumberSum.Name = "labNumberSum";
            this.labNumberSum.Size = new System.Drawing.Size(126, 21);
            this.labNumberSum.TabIndex = 209;
            this.labNumberSum.Tag = "Tên :";
            this.labNumberSum.Text = "SL Cấp Báo Cáo :";
            this.labNumberSum.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlLine
            // 
            this.pnlLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlLine.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlLine.Location = new System.Drawing.Point(363, 0);
            this.pnlLine.Name = "pnlLine";
            this.pnlLine.Size = new System.Drawing.Size(1, 727);
            this.pnlLine.TabIndex = 201;
            // 
            // txtSLTongVIP
            // 
            this.txtSLTongVIP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtSLTongVIP.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSLTongVIP.ForeColor = System.Drawing.Color.Red;
            this.txtSLTongVIP.Location = new System.Drawing.Point(175, 358);
            this.txtSLTongVIP.Name = "txtSLTongVIP";
            this.txtSLTongVIP.ReadOnly = true;
            this.txtSLTongVIP.Size = new System.Drawing.Size(182, 46);
            this.txtSLTongVIP.TabIndex = 208;
            this.txtSLTongVIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.BackColor = System.Drawing.Color.White;
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(107, 252);
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.ReadOnly = true;
            this.txtThanhPho.Size = new System.Drawing.Size(250, 29);
            this.txtThanhPho.TabIndex = 189;
            // 
            // labName
            // 
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(36, 10);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(65, 21);
            this.labName.TabIndex = 194;
            this.labName.Tag = "Tên :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(107, 6);
            this.txtTen.Name = "txtTen";
            this.txtTen.ReadOnly = true;
            this.txtTen.Size = new System.Drawing.Size(250, 29);
            this.txtTen.TabIndex = 187;
            // 
            // labAddress
            // 
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(8, 186);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(93, 21);
            this.labAddress.TabIndex = 195;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.White;
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.Enabled = false;
            this.txtID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.ForeColor = System.Drawing.Color.Red;
            this.txtID.Location = new System.Drawing.Point(143, 560);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(76, 29);
            this.txtID.TabIndex = 193;
            this.txtID.Text = "0";
            this.txtID.Visible = false;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.BackColor = System.Drawing.Color.White;
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(107, 145);
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.ReadOnly = true;
            this.txtDiaChi.Size = new System.Drawing.Size(250, 99);
            this.txtDiaChi.TabIndex = 188;
            // 
            // labID
            // 
            this.labID.BackColor = System.Drawing.Color.Transparent;
            this.labID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labID.Location = new System.Drawing.Point(76, 563);
            this.labID.Name = "labID";
            this.labID.Size = new System.Drawing.Size(61, 21);
            this.labID.TabIndex = 200;
            this.labID.Tag = "Mã :";
            this.labID.Text = "ID :";
            this.labID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labID.Visible = false;
            // 
            // labCity
            // 
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(10, 256);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(93, 21);
            this.labCity.TabIndex = 196;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(107, 41);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(250, 29);
            this.txtEmail.TabIndex = 192;
            // 
            // labPhone
            // 
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(5, 114);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(96, 21);
            this.labPhone.TabIndex = 197;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Tel :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labEmail
            // 
            this.labEmail.BackColor = System.Drawing.Color.Transparent;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(37, 45);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(64, 21);
            this.labEmail.TabIndex = 199;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.BackColor = System.Drawing.Color.White;
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(107, 110);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.ReadOnly = true;
            this.txtDienThoai.Size = new System.Drawing.Size(250, 29);
            this.txtDienThoai.TabIndex = 190;
            // 
            // txtDiDong
            // 
            this.txtDiDong.BackColor = System.Drawing.Color.White;
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(107, 76);
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.ReadOnly = true;
            this.txtDiDong.Size = new System.Drawing.Size(250, 29);
            this.txtDiDong.TabIndex = 191;
            // 
            // labCellPhone
            // 
            this.labCellPhone.BackColor = System.Drawing.Color.Transparent;
            this.labCellPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCellPhone.Location = new System.Drawing.Point(1, 80);
            this.labCellPhone.Name = "labCellPhone";
            this.labCellPhone.Size = new System.Drawing.Size(100, 21);
            this.labCellPhone.TabIndex = 198;
            this.labCellPhone.Tag = "Di Động :";
            this.labCellPhone.Text = "Mobile :";
            this.labCellPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUltimate
            // 
            this.btnUltimate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnUltimate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUltimate.FlatAppearance.BorderSize = 0;
            this.btnUltimate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnUltimate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnUltimate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUltimate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUltimate.ForeColor = System.Drawing.Color.White;
            this.btnUltimate.Image = global::iTalent.Admin.UI.Properties.Resources.keys;
            this.btnUltimate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUltimate.Location = new System.Drawing.Point(522, 12);
            this.btnUltimate.Margin = new System.Windows.Forms.Padding(0);
            this.btnUltimate.Name = "btnUltimate";
            this.btnUltimate.Size = new System.Drawing.Size(122, 60);
            this.btnUltimate.TabIndex = 212;
            this.btnUltimate.Tag = "";
            this.btnUltimate.Text = "Ultimate";
            this.btnUltimate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUltimate.UseVisualStyleBackColor = false;
            this.btnUltimate.Click += new System.EventHandler(this.btnUltimate_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(14, 312);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 35);
            this.label4.TabIndex = 216;
            this.label4.Tag = "";
            this.label4.Text = "ULTIMATE";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSLTongUltimate
            // 
            this.txtSLTongUltimate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtSLTongUltimate.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSLTongUltimate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtSLTongUltimate.Location = new System.Drawing.Point(175, 306);
            this.txtSLTongUltimate.Name = "txtSLTongUltimate";
            this.txtSLTongUltimate.ReadOnly = true;
            this.txtSLTongUltimate.Size = new System.Drawing.Size(182, 46);
            this.txtSLTongUltimate.TabIndex = 215;
            this.txtSLTongUltimate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FrmKeys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 766);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmKeys";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmKeys_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup buttonSpecHeaderGroup1;
        private System.Windows.Forms.Button btnGenerateSimple;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label labID;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.Label labCellPhone;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labNumber;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Label labKey;
        private System.Windows.Forms.TextBox txtKey;
        private C4FunComponent.Toolkit.C4FunDataGridView dgvData;
        private System.Windows.Forms.Panel pnlLine;
        private System.Windows.Forms.Button btnCopyKey;
        private System.Windows.Forms.Label labNumberSum;
        private System.Windows.Forms.TextBox txtSLTongVIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAgencyID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDateCreate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colKey;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtNumberOfkey;
        private System.Windows.Forms.Button btnGenerateVIP;
        private System.Windows.Forms.Button btnGeneratePremium;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSLTongSimple;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSLTongPremium;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUltimate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSLTongUltimate;
    }
}