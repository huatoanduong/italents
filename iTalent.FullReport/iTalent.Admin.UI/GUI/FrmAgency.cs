﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entity;
using iTalent.Utils;

namespace iTalent.Admin.UI.GUI
{
    public partial class FrmAgency : C4FunForm
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        public FIAgency CurrAgency { get; set; }
        public FrmAgency()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> {c4FunHeaderGroup1,labAddress,labCellPhone,labCity,labEmail,labID,labName,labPhone,labPassword, labKeyXacNhan, labKey,labNote, btnGenerateKey, btnLuu };
            _baseForm = new BaseForm();
            btnLuu.Enabled = false;
            txtKeyRegister.Select();
        }

        public FrmAgency(FIAgency objAgency)
        {
            InitializeComponent();
            CurrAgency = objAgency;
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { c4FunHeaderGroup1, labAddress, labCellPhone, labCity, labEmail, labID, labName, labPhone, labPassword,labKeyXacNhan, labKey, labNote, btnGenerateKey, btnLuu };
            _baseForm = new BaseForm();
            btnLuu.Enabled = true;
            txtTen.Select();
        }

        private void LoadToUi(FIAgency obj)
        {
            try
            {
                dtpDateEx.Value = DateTime.Now.AddYears(1);
                //txtKeyCheck.Enabled = false;
                //txtKeyRegister.Visible = false;
                btnGenerateKey.Visible = false;
                //label1.Visible = false;
                //dtpDateEx.Visible = false;
               // btnGeVipPremiumSimple.Visible = false;
                labKey.Visible = false;
                labNote.Visible = false;

                //using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                //{
                //    unitOfWork.OpenConnection();
                //    IFIAgencyService service = new FIAgencyService(unitOfWork);

                if (obj == null)
                {
                    //txtKeyCheck.Enabled = true;
                    //txtKeyRegister.Visible = true;
                    btnGenerateKey.Visible = true;
                    label1.Visible = true;
                    dtpDateEx.Visible = true;
                   // btnGeVipPremiumSimple.Visible = true;
                    labKey.Visible = true;
                    labNote.Visible = true;
                    btnUpdateKeyUltimate.Visible = false;

                    //obj = service.CreateEntity();
                    obj = new FIAgency()
                    {
                        Country = "VietNam",
                        DOB = DateTime.Now.AddYears(-20),
                        Day_Actived = 0,
                        Dis_Activated = "1",
                        Gender = "None",
                        Username = "",
                        Password = "",
                        ID = 0,
                        Point = -1,
                        ProductKey = "",
                        SecKey = "",
                        SerailKey = "",
                        State = "1",
                        LastActiveDate = DateTime.Now
                    };
                    obj.ProductKey = "";
                    obj.SecKey = "";
                    obj.SerailKey = "";
                    obj.SaveImages = @"C:\";
                    obj.Email = "@gmail.com";
                    obj.City = "HCM";
                    obj.Country = "Việt Nam";
                }

                txtID.Text = obj.ID.ToString();
                txtTen.Text = obj.Name;
                txtDiaChi.Text = obj.Address1;
                txtThanhPho.Text = obj.City;
                txtEmail.Text = obj.Email;
                txtDiDong.Text = obj.MobileNo;
                txtDienThoai.Text = obj.PhoneNo;
                txtKeyRegister.Text = obj.ProductKey;
                txtKeySimplePremiumVip.Text = obj.SecKey;

                if (obj.SerailKey != "")
                {
                    txtKeyUltimate.Text = obj.SerailKey.Replace(obj.ProductKey, "");
                    DateTime exDate;
                    using (IInstallLicenseService installLicense = new InstallLicenseService())
                    {
                        string seckey;
                        DateTime geDate;
                        installLicense.GetInfoLicense(txtKeyUltimate.Text, obj.ProductKey, out seckey, out exDate, out geDate);
                    }

                    txtKeySimplePremiumVip.Text = obj.SecKey;
                    dtpDateEx.Value = exDate;
                }
                CurrAgency = obj;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(0);
            }
        }

        private void Register()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtKeyUltimate.Text))
                {

                    _baseForm.VietNamMsg = @"Chưa có mã xác nhận!";
                    _baseForm.EnglishMsg = @"Key verification null!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                if (string.IsNullOrWhiteSpace(txtTen.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập tên!";
                    _baseForm.EnglishMsg = @"Please input your name!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;

                }

                if (string.IsNullOrWhiteSpace(txtEmail.Text))
                {

                    _baseForm.VietNamMsg = @"Vui lòng nhập email!";
                    _baseForm.EnglishMsg = @"Please input your email!";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                    return;
                }

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    bool isAdd = false;
                    int id = ConvertUtil.ToInt(txtID.Text);
                    FIAgency objAgency = service.Find(id);
                    if (objAgency == null)
                    {
                        objAgency = new FIAgency()
                        {
                            Country = "VietNam",
                            DOB = DateTime.Now.AddYears(-20),
                            Day_Actived = 0,
                            Dis_Activated = "1",
                            Gender = "None",
                            Username = "",
                            Password = "",
                            ID = 0,
                            Point = -1,
                            ProductKey = "",
                            SecKey = "",
                            SerailKey = "",
                            State = "1"
                        };
                        isAdd = true;
                    }

                    objAgency.Username = "AC50015";
                    objAgency.Password = "AC50015";
                    objAgency.Name = txtTen.Text;
                    objAgency.Email = txtEmail.Text;
                    objAgency.PhoneNo = txtDienThoai.Text;
                    objAgency.MobileNo = txtDiDong.Text;
                    objAgency.City = txtThanhPho.Text;
                    objAgency.SaveImages = "C:\\";
                    objAgency.Address1 = txtDiaChi.Text;
                    objAgency.SecKey = txtKeySimplePremiumVip.Text;
                    objAgency.ProductKey = txtKeyRegister.Text;
                    objAgency.SerailKey = txtKeyUltimate.Text + txtKeyRegister.Text;

                    Issuccess = isAdd ? service.Add(objAgency) : service.Update(objAgency);

                    //using (IInstallLicenseService installLicense = new InstallLicenseService())
                    //{
                    //    bool ischeck = installLicense.CheckLicense(objAgency.SecKey);
                    //    if(ischeck)
                    //        Issuccess = isAdd ? service.Add(objAgency) : service.Update(objAgency);
                    //}

                    if (!Issuccess)
                    {
                        _baseForm.ShowMessage(IconMessageBox.Warning, service.ErrMsg);
                        return;
                    }
                    base.Dispose();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                Environment.Exit(0);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSpecHeaderGroup1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            Register();
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtKeyRegister.Text == "")
                {
                    _baseForm.VietNamMsg = @"Vui lòng nhập đăng ký Ultimate!";
                    _baseForm.EnglishMsg = @"Please input key register Ultimate!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                    return;
                }

                using (IInstallLicenseService installLicense = new InstallLicenseService())
                {
                    txtKeySimplePremiumVip.Text = installLicense.GetSecKeySection(txtKeyRegister.Text);
                    txtKeyUltimate.Text = installLicense.MakeLicense(txtKeyRegister.Text, txtKeySimplePremiumVip.Text,
                        dtpDateEx.Value);
                }

                string key = txtKeySimplePremiumVip.Text;
                    if (key != "")
                    {
                        using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                        {
                            IFIAgencyService service = new FIAgencyService(unitOfWork);
                            var tmpObj1 = service.FindBySeckey(key);
                            if (tmpObj1 != null)
                            {
                                txtKeyUltimate.Text = "";
                                _baseForm.VietNamMsg =
                                    "Mã xác nhận này đã có đại lý sử dụng!\nVui lòng liên hệ người quản lý.";
                                _baseForm.EnglishMsg = "The key has exists!\nPlease contract admin.";
                                _baseForm.ShowMessage(IconMessageBox.Information);
                                return;
                            }
                        }

                        //txtKeyUltimate.Text = key;
                        txtKeyRegister.Enabled = false;
                        //btnGenerateKey.Enabled = false;
                        btnLuu.Enabled = true;

                        _baseForm.VietNamMsg = "Mã hợp lệ!\nVui lòng điền đầy đủ thông tin đại lý.";
                        _baseForm.EnglishMsg = "The key is valid!\nPlease input information's agency.";
                        _baseForm.ShowMessage(IconMessageBox.Information);
                    }
                
            }
            catch (Exception ex)
            {
                txtKeyUltimate.Text = "";
                _baseForm.VietNamMsg = @"Key Ultimate không hợp lệ!";
                _baseForm.EnglishMsg = @"the key Ultimate is not valid!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void FrmAgency_Load(object sender, EventArgs e)
        {
            LoadToUi(CurrAgency);
        }

        private void btnGeVipPremiumSimple_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtKeyRegister.Text == "")
                {
                    _baseForm.VietNamMsg = @"Vui lòng nhập đăng ký Vip|Premium|Simple!";
                    _baseForm.EnglishMsg = @"Please input key register Vip|Premium|Simple!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                    return;
                }

                txtKeySimplePremiumVip.Text = GeneratePos.GetKeyDe(txtKeyRegister.Text);
                string key = txtKeySimplePremiumVip.Text;
                if (key != "")
                {
                    using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                    {
                        IFIAgencyService service = new FIAgencyService(unitOfWork);
                        var tmpObj1 = service.FindBySeckey(key);
                        if (tmpObj1 != null)
                        {
                            txtKeyUltimate.Text = "";
                            _baseForm.VietNamMsg =
                                "Mã xác nhận này đã có đại lý sử dụng!\nVui lòng liên hệ người quản lý.";
                            _baseForm.EnglishMsg = "The key has exists!\nPlease contract admin.";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            return;
                        }
                    }

                    txtKeyUltimate.Text = key;
                    txtKeyRegister.Enabled = false;
                    btnGenerateKey.Enabled = false;
                    btnLuu.Enabled = true;

                    _baseForm.VietNamMsg = "Mã hợp lệ!\nVui lòng điền đầy đủ thông tin đại lý.";
                    _baseForm.EnglishMsg = "The key is valid!\nPlease input information's agency.";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                }

            }
            catch (Exception ex)
            {
                txtKeyUltimate.Text = "";
                _baseForm.VietNamMsg = @"Key Vip|Premium|Simple không hợp lệ!";
                _baseForm.EnglishMsg = @"the key Vip|Premium|Simple is not valid!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void btnUpdateKeyUltimate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtKeyRegister.Text == "")
                {
                    _baseForm.VietNamMsg = @"Vui lòng nhập đăng ký Ultimate!";
                    _baseForm.EnglishMsg = @"Please input key register Ultimate!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                    return;
                }

                using (IInstallLicenseService installLicense = new InstallLicenseService())
                {
                    txtKeySimplePremiumVip.Text = installLicense.GetSecKeySection(txtKeyRegister.Text);
                    txtKeyUltimate.Text = installLicense.MakeLicense(txtKeyRegister.Text, txtKeySimplePremiumVip.Text, dtpDateEx.Value);
                }

                string key = txtKeySimplePremiumVip.Text;
                if (key != "")
                {
                    using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                    {
                        IFIAgencyService service = new FIAgencyService(unitOfWork);
                        var tmpObj1 = service.FindBySeckey(key);
                        if (tmpObj1 != null)
                        {
                            //if(txtKeyRegister.Text!="")
                            //    tmpObj1.ProductKey = txtKeyRegister.Text;
                            //if (txtRegisterVipPremiumSimple.Text != "")
                            //    tmpObj1.ProductKey = txtRegisterVipPremiumSimple.Text;

                            tmpObj1.SerailKey = txtKeyUltimate.Text+ tmpObj1.ProductKey;
                            //tmpObj1.SecKey = txtKeySimplePremiumVip.Text;

                            //service.Update(tmpObj1);
                            return;
                            //txtKeyUltimate.Text = "";
                            //_baseForm.VietNamMsg =
                            //    "Mã xác nhận này đã có đại lý sử dụng!\nVui lòng liên hệ người quản lý.";
                            //_baseForm.EnglishMsg = "The key has exists!\nPlease contract admin.";
                            //_baseForm.ShowMessage(IconMessageBox.Information);
                            //return;
                        }
                    }

                    //txtKeyUltimate.Text = key;
                    txtKeyRegister.Enabled = false;
                    btnGenerateKey.Enabled = false;
                    btnLuu.Enabled = true;
                    btnUpdateKeyUltimate.Visible = true;//
                    _baseForm.VietNamMsg = "Mã hợp lệ!\nVui lòng điền đầy đủ thông tin đại lý.";
                    _baseForm.EnglishMsg = "The key is valid!\nPlease input information's agency.";
                    _baseForm.ShowMessage(IconMessageBox.Information);
                }
            }
            catch (Exception ex)
            {
                txtKeyUltimate.Text = "";
                _baseForm.VietNamMsg = @"Key Ultimate không hợp lệ!";
                _baseForm.EnglishMsg = @"the key Ultimate is not valid!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }
    }
}
