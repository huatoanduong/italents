﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Admin.UI.FlashForm;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entity;

namespace iTalent.Admin.UI.GUI
{
    public partial class FrmMain : C4FunForm
    {
        public bool Issuccess { get; set; }
        private Config _config;
        private BaseForm _baseForm;
        private string _dirFile;
        public FICustomer CurrCustomer { get; set; }
        public ICollection<FICustomer> ListCustomers { get; set; }
        public FIAgency CurrAgency { get; set; }

        public FrmMain()
        {
            InitializeComponent();
        }

        #region Methods

        private void LoadAll()
        {
            try
            {
                Hide();

                _config = new Config();
                BaseForm.Frm = this;
                BaseForm.DgView = dgvData;
                BaseForm.ListconControls = new List<Control>
                {
                    btnAdd,
                    btnConfig,
                    btnEdit,
                    btnExport,
                    btnRefresh,
                    labSearch,
                    btnConvertTool,
                    btnCreateKey
                };
                _baseForm = new BaseForm();

                if (_config.KeyLogin == null)
                {
                    FrmConfig frmc = new FrmConfig { StartPosition = FormStartPosition.CenterScreen };
                    frmc.ShowDialog(this);

                    if (frmc.Issuccess)
                        Environment.Exit(0);
                }

                FrmLogin frm = new FrmLogin();
                frm.ShowDialog(this);

                bool kq = frm.Issuccess;
                if (!kq)
                {
                    _baseForm.VietNamMsg = "Mật khẩu không đúng!";
                    _baseForm.EnglishMsg = "Worng Password!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                    Environment.Exit(0);
                }

                //FrmFlash.ShowSplash();
                //Application.DoEvents();

                FIAgency objAgency = new FIAgency {SecKey = "ITALENT"};
                ICurrentSessionService.CurAgency = objAgency;

                WindowState = FormWindowState.Maximized;

                dgvData.AutoGenerateColumns = false;
                LoadData();

                _dirFile = "";//sourceDirName;
                //}

                //FrmFlash.CloseSplash();
                Show();
                Activate();
            }
            catch (Exception ex)
            {
                FrmFlash.CloseSplash();
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }
        private void LoadData()
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    dgvData.DataSource = service.FindAll();
                    dgvData.Refresh();
                    txtSearch.Text = "";
                    txtSearch.Select();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }
        private void LoadData(ICollection<FIAgency> listAgencies)
        {
            try
            {
                dgvData.AutoGenerateColumns = false;
                dgvData.SuspendLayout();
                dgvData.DataSource = listAgencies;
                dgvData.Refresh();
                dgvData.ResumeLayout();
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }

        #endregion

        #region Events

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (Directory.Exists(ICurrentSessionService.DesDirNameTemp))
                    Directory.Delete(ICurrentSessionService.DesDirNameTemp, true);
            }
            catch
            {
                //
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            //Hide();
            FrmConfig frm = new FrmConfig { StartPosition = FormStartPosition.CenterScreen };
            frm.ShowDialog(this);
          
            if (frm.Issuccess)
                Application.Restart();

            //Show();

        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            var frm = new FrmAgency {StartPosition = FormStartPosition.CenterScreen};
            frm.ShowDialog(this);
            if (frm.Issuccess)
                LoadData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                int id = int.Parse(row.Cells[0].Value.ToString());

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    CurrAgency = service.Find(id);
                    if (CurrAgency == null)
                    {
                        _baseForm.VietNamMsg = "Vui lòng chọn một trong các đại lý!";
                        _baseForm.EnglishMsg = "please choice one agency!";
                        _baseForm.ShowMessage(IconMessageBox.Warning);
                        return;
                    }
                }

                var frm = new FrmAgency(CurrAgency);
                frm.ShowDialog(this);
                if (frm.Issuccess)
                    LoadData();
                Activate();
            }
            catch (Exception)
            {
                
            }
        }

        private void btnCreateKey_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                int id = int.Parse(row.Cells[0].Value.ToString());

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    CurrAgency = service.Find(id);
                    if (CurrAgency == null)
                    {
                        _baseForm.VietNamMsg = "Vui lòng chọn một trong các đại lý!";
                        _baseForm.EnglishMsg = "please choice one agency!";
                        _baseForm.ShowMessage(IconMessageBox.Warning);
                        return;
                    }
                }

                FrmKeys frm = new FrmKeys(CurrAgency) { StartPosition = FormStartPosition.CenterScreen };
                frm.ShowDialog(this);
                Activate();
            }
            catch (Exception ex)
            {
                //
            }
        }

        private void btnConvertTool_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                int id = int.Parse(row.Cells[0].Value.ToString());

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    CurrAgency = service.Find(id);
                    if (CurrAgency == null)
                    {
                        _baseForm.VietNamMsg = "Vui lòng chọn một trong các đại lý!";
                        _baseForm.EnglishMsg = "please choice one agency!";
                        _baseForm.ShowMessage(IconMessageBox.Warning);
                        return;
                    }
                }

                FrmConvert frm = new FrmConvert(CurrAgency) { StartPosition = FormStartPosition.CenterScreen };
                frm.ShowDialog(this);

                Activate();
            }
            catch (Exception ex)
            {
                //
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SmartSearch(txtSearch.Text);
        }

        private void dgvData_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvData.SelectedRows[0];
                if (row == null) return;

                int id = int.Parse(row.Cells[0].Value.ToString());
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    CurrAgency = service.Find(id);
                }
            }
            catch (Exception)
            {
                CurrAgency = null;
            }
        }

        private void dgvData_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            dgvData.Rows[e.RowIndex].Cells[1].Value = e.RowIndex + 1;
            dgvData.Rows[e.RowIndex].Tag = e.RowIndex+"+"+dgvData.Rows[e.RowIndex].Cells[2].Value+"+"+ dgvData.Rows[e.RowIndex].Cells[3].Value + "+" + dgvData.Rows[e.RowIndex].Cells[4].Value + "+" + dgvData.Rows[e.RowIndex].Cells[5].Value + "+" + dgvData.Rows[e.RowIndex].Cells[6].Value + "+" + dgvData.Rows[e.RowIndex].Cells[7].Value + "+" + dgvData.Rows[e.RowIndex].Cells[8].Value;
        }

        #endregion

        #region TimKiem

        public void SmartSearch(string compare)
        {
            if (compare == "")
            {
                LoadData();
            }

            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.OpenConnection();
                IFIAgencyService service = new FIAgencyService(unitOfWork);
                List<FIAgency> list = service.FindAll();
                if(list==null)return;

                string compareUnsig = ConverToUnsign1(compare).Replace(" ", "").ToLower();
                //string comparenomarl = compare.Replace(" ", "").ToLower();

                List<FIAgency> listsearch = (from obj in list let bodau = ConverToUnsign1(obj.SecKey + "+" + obj.Name + "+" + obj.Address1 + "+" + obj.Email + "+" + obj.PhoneNo + "+" + obj.MobileNo + "+" + obj.City) where bodau.Contains(compareUnsig) select obj).ToList();

                dgvData.DataSource = listsearch;
                dgvData.Refresh();
            }

           
            //foreach (int i in (from DataGridViewRow row in dgvData.Rows select row.Tag.ToString().Trim().ToLower()).Select(ConverToUnsign1).Select(bodau => int.Parse(bodau.Split('+')[0])))
            //{
            //    //dgvData.Rows[i].Visible = false;
            //    dgvData.Rows[i].Dispose();
            //}
            //dgvData.Refresh();
        }

        public static string ConverToUnsign1(string s)
        {
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }

        public static string XoaKhoangTrang(string s)
        {
            return s.Where(t => t != 32)
                .Aggregate("", (current, t) => current + t.ToString(CultureInfo.InvariantCulture).Trim());
        }


        #endregion

        private void btnConvertTemplate_Click(object sender, EventArgs e)
        {
            FrmConvertExcelTemplate frm = new FrmConvertExcelTemplate();
            frm.ShowDialog(this);
        }
    }
}
