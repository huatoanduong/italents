﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using iTalent.Analyse.Commons;

namespace iTalent.FullReport.GUI
{
    public partial class FrmCountFinger : Form
    {
        public string RCount { get; set; }
        private readonly string _dirFile;
        private readonly string _indexF;
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;
        private bool CheckPtb1 { get; set; }
        private bool CheckPtb2 { get; set; }
        private bool CheckPtb3 { get; set; }

        public FrmCountFinger(int index, string dirfile)
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { c4FunHeaderGroup1, btnCenter, btnLeft, btnRight };
            _baseForm = new BaseForm();
            CheckPtb1 = CheckPtb2 = CheckPtb3 = false;
            Issuccess = false;

            _dirFile = dirfile;
            _indexF = index.ToString();

            //RCount = "0";
        }

        private void Call_RC_Count()
        {
            using (var process = new Process())
            {
                foreach (var process3 in Process.GetProcessesByName("Untitled8f32"))
                {
                    process3.Kill();
                }

                var info = new ProcessStartInfo("Untitled8f32.exe");
                process.StartInfo = info;
                process.EnableRaisingEvents = true;
                process.Exited += ExitRc;
                process.Start();
                process.WaitForExit();
            }
        }

        private void Write_RC_file(string fName)
        {
            var str = Path.Combine(_dirFile, fName + ".b");
            var filename = Directory.GetCurrentDirectory() + @"\inbound.txt";
            var writer = new StreamWriter(filename, false, Encoding.ASCII);
            writer.Write(str);
            writer.Close();
        }

        private void ExitRc()
        {
            if (!File.Exists(Directory.GetCurrentDirectory() + @"\outbound.dat")) return;
            string str;
            TextReader reader = new StreamReader(Directory.GetCurrentDirectory() + @"\outbound.dat");
            while (true)
            {
                str = reader.ReadLine();
                break;
            }
            reader.Close();
            RCount = str;

            if (File.Exists(Directory.GetCurrentDirectory() + @"\outbound.dat"))
            {
                File.Delete(Directory.GetCurrentDirectory() + @"\outbound.dat");
            }

            if (File.Exists(Directory.GetCurrentDirectory() + @"\inbound.txt"))
            {
                File.Delete(Directory.GetCurrentDirectory() + @"\inbound.txt");
            }
        }

        private void LayThongSoVanTay(string nameFinger)
        {
            try
            {
                Write_RC_file(nameFinger);
                Call_RC_Count();
                Dispose();
            }
            catch
            {
                //
            }
        }

        private void ExitRc(object sender, EventArgs e)
        {
            ExitRc();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (ptbLeft.Image != null && CheckPtb2)
                LayThongSoVanTay(_indexF+"_2");
            else
            {
                _baseForm.EnglishMsg = "Fingerprint image not correct!";
                _baseForm.VietNamMsg = "Hình vân tay không đúng chuẩn!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void btnCenter_Click(object sender, EventArgs e)
        {
            if (ptbCenter.Image != null && CheckPtb1)
                LayThongSoVanTay(_indexF + "_1");
            else
            {
                _baseForm.EnglishMsg = "Fingerprint image not correct!";
                _baseForm.VietNamMsg = "Hình vân tay không đúng chuẩn!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (ptbRight.Image != null && CheckPtb3)
                LayThongSoVanTay(_indexF + "_3");
            else
            {
                _baseForm.EnglishMsg = "Fingerprint image not correct!";
                _baseForm.VietNamMsg = "Hình vân tay không đúng chuẩn!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void ptbLeft_Click(object sender, EventArgs e)
        {
            if (ptbLeft.Image != null && CheckPtb2)
            {
                btnLeft.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (255)))),
                    ((int) (((byte) (128)))));
                LayThongSoVanTay(_indexF + "_2");
            }
            else
            {
                _baseForm.EnglishMsg = "Fingerprint image not correct!";
                _baseForm.VietNamMsg = "Hình vân tay không đúng chuẩn!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void ptbCenter_Click(object sender, EventArgs e)
        {
            if (ptbCenter.Image != null && CheckPtb1)
            {
                btnCenter.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (255)))),
                    ((int) (((byte) (128)))));
                LayThongSoVanTay(_indexF + "_1");
            }
            else
            {
                _baseForm.EnglishMsg = "Fingerprint image not correct!";
                _baseForm.VietNamMsg = "Hình vân tay không đúng chuẩn!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void ptbRight_Click(object sender, EventArgs e)
        {
            if (ptbRight.Image != null && CheckPtb3)
            {
                btnRight.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (255)))),
                    ((int) (((byte) (128)))));
                LayThongSoVanTay(_indexF + "_3");
            }
            else
            {
                _baseForm.EnglishMsg = "Fingerprint image not correct!";
                _baseForm.VietNamMsg = "Hình vân tay không đúng chuẩn!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void LoadImage()
        {
            ImageChecker imgChecker = new ImageChecker();
            string dirImage = GetImageFP(_dirFile, _indexF+"_1");
            CheckPtb1 = imgChecker.CheckImage(dirImage);
            if (File.Exists(dirImage)) ptbCenter.Image = Image.FromFile(dirImage);

            dirImage = GetImageFP(_dirFile, _indexF+"_2");
            CheckPtb2 = imgChecker.CheckImage(dirImage);
            if (File.Exists(dirImage)) ptbLeft.Image = Image.FromFile(dirImage);

            dirImage = GetImageFP(_dirFile, _indexF + "_3");
            CheckPtb3 = imgChecker.CheckImage(dirImage);
            if (File.Exists(dirImage)) ptbRight.Image = Image.FromFile(dirImage);
        }

        string GetImageFP(string fileSave, string type)
        {
            return Path.Combine(fileSave, type + ".b");
        }

        private void FrmCountFinger_Load(object sender, EventArgs e)
        {
            LoadImage();
        }
    }
}
