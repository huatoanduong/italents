﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entity;
using iTalent.FingerprintScanDevice;
using iTalent.Utils;

namespace iTalent.FullReport.GUI
{
	public partial class frmImportCustomer2 : C4FunForm
	{
		private FICustomer CurrCustomer { get; set; }
		private readonly BaseForm _baseForm;
		public bool Issuccess { get; set; }
		private string _oldNameCustomer;
		private string _parentDirFile;

		private string _dirFile
		{
			get { return ICurrentSessionService.DesDirNameSource; }
		}

		private bool _isinsert;

		public frmImportCustomer2()
		{
			InitializeComponent();
			btnLuu.Enabled = false;
			BaseForm.Frm = this;
			BaseForm.ListconControls = new List<Control>
			{
				c4FunHeaderGroup1,
				labAddress,
				labBirthday,
				labCity,
				labCity,
				labEmail,
				//labEntity,
				labMobile,
				labName,
				labNote,
				labParent,
				labPhone,
				labSex,
				btnLuu,
				btnScaner,
				labChoice
			};

			_baseForm = new BaseForm();
			Issuccess = false;
			_isinsert = true;
			//LoadCbxSex();
		}

		public frmImportCustomer2(FICustomer objCustomer)
		{
			InitializeComponent();
			BaseForm.Frm = this;
			BaseForm.ListconControls = new List<Control>
			{
				c4FunHeaderGroup1,
				labAddress,
				labBirthday,
				labCity,
				labCity,
				labEmail,
				//labEntity,
				labMobile,
				labName,
				labNote,
				labParent,
				labPhone,
				labSex,
				btnLuu,
				btnScaner,
				 btnCapture
			};
			_baseForm = new BaseForm();
			CurrCustomer = objCustomer;
			Issuccess = false;
			_isinsert = false;
			LoadCbxSex();
		}

		private void LoadCbxSex()
		{
			cbxGioiTinh.Items.Clear();
			if (ICurrentSessionService.VietNamLanguage)
			{
				cbxGioiTinh.Items.Add("None");
				cbxGioiTinh.Items.Add("Nam");
				cbxGioiTinh.Items.Add("Nữ");
			}
			else
			{
				cbxGioiTinh.Items.Add("None");
				cbxGioiTinh.Items.Add("Male");
				cbxGioiTinh.Items.Add("Female");
			}
		}

		private string GetGender()
		{
			if (ICurrentSessionService.VietNamLanguage)
			{
				return rdFemale.Checked ? "Nữ" : "Nam";
			}
			return rdFemale.Checked ? "Female" : "Male";
		}

		private void SetGender(string sex)
		{
			if (sex == "Nữ" || sex == "Female")
				rdFemale.Checked = true;
			else
			{
				rdMale.Checked = true;
			}
		}

		private void LoadToUi(FICustomer obj)
		{
			try
			{
				if (obj == null)
				{
                    using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                    {
                        IFICustomerService service = new FICustomerService(unitOfWork);
                        obj = service.CreateEntity();
                        obj.ReportID = ICurrentSessionService.CurAgency.SecKey +
                                       obj.Date.ToString("yyMMddhhmmss");
                    }
				}
				labMa.Text = obj.ID.ToString();
				labReportId.Text = obj.ReportID;

				txtTen.Text = obj.Name;
				_oldNameCustomer = obj.Name;

				cbxGioiTinh.Text = obj.Gender;
				SetGender(obj.Gender); //them 
				txtTenChaMe.Text = obj.Parent;
			    dtpNgaySinh.Value = obj.DOB;
				txtDiaChi.Text = obj.Address1;
				txtThanhPho.Text = obj.City;
				txtEmail.Text = obj.Email;
				txtDiDong.Text = obj.Mobile;
				txtDienThoai.Text = obj.Tel;
				//txtZip.Text = obj.ZipPosTalCode;
				//txtTinhTrang.Text = obj.State;
				txtGhiChu.Text = obj.Remark;
				CurrCustomer = obj;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				Environment.Exit(0);
			}
		}

		private void Insertupdatefile(string dir, string dirtemp, string reportid,
			string nameCustomer, string oldnameCustomer)
		{
			try
			{
				var olddirfolder = dir + @"\" + reportid + "_" + oldnameCustomer;
				var newdirfolder = dir + @"\" + reportid + "_" + nameCustomer;
				var tempfolder = dirtemp + @"\" + reportid;

				Directory.CreateDirectory(newdirfolder);

				if (Directory.Exists(olddirfolder) && olddirfolder != newdirfolder)
				{
					var d = new DirectoryInfo(olddirfolder);

					var infos = d.GetFiles();
					if (!d.GetFiles().Any()) return;
					foreach (var f in infos)
					{
						var fileName = Path.GetFileName(f.Name);
						string destFile = Path.Combine(newdirfolder, fileName);
						File.Move(f.FullName, destFile);
						f.Delete();
					}
					d.Delete(true);
				}

				if (Directory.Exists(tempfolder))
				{
					var d = new DirectoryInfo(tempfolder);
                    
					var infos = d.GetFiles();
					if (!d.GetFiles().Any()) return;
					foreach (var f in infos)
					{
						var fileName = Path.GetFileName(f.Name);
						var destFile = Path.Combine(newdirfolder, fileName);
						if (!f.Name.ToLower().Contains(".dat") &&
							!f.Name.ToLower().Contains(reportid.ToLower() + "f.bmp"))
							destFile = ConvertToObject(destFile, reportid);
						File.Move(f.FullName, destFile);
					}
					d.Delete(true);
				}
			}
			catch (Exception ex)
			{
				// ignored
			}
		}

		private void SaveDataFile(FIAgency objAgency, FICustomer objCustomer, string desDirName)
		{
		    try
		    {
		        if (objAgency == null) return;
		        if (objCustomer == null) return;

		        string name = ConvertUtil.ConverToUnsign(objCustomer.Name).Replace(" ", "").ToUpper();

		        if (!Directory.Exists(desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper()))
		            Directory.CreateDirectory(desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper());

		        string fileName = desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper() + @"\" +
		                          objCustomer.ReportID + "profile.dat";

		        if (File.Exists(fileName))
		        {
		            File.Delete(fileName);
		            fileName = desDirName + @"\" + objCustomer.ReportID + "_" + name.ToUpper() + @"\" +
		                       objCustomer.ReportID + "profile.txt";
		        }

		        File.Create(fileName).Dispose();
		        StreamWriter streamWriter = new StreamWriter(fileName);
		        streamWriter.WriteLine("0|" + objAgency.Username + "|" + objAgency.Name + "|0|||" + objAgency.Address1 +
		                               "|||||||" + objAgency.MobileNo + "|X||||||||||" + objAgency.Username +
		                               objAgency.Name);
		        streamWriter.WriteLine("1|" + objAgency.Username + "|" + objCustomer.ReportID + "|" +
		                               objCustomer.Date.ToString("dd/MM/yyyy") +
		                               "|" + objCustomer.Name + "|" + objCustomer.Parent + "|" + objCustomer.Gender +
		                               "|" + objCustomer.DOB.ToString("dd/MM/yyyy") +
		                               "|" + objCustomer.Address1 + "|" + objCustomer.Address2 + "|" + objCustomer.City +
		                               "|" + objCustomer.ZipPosTalCode + "|" + objCustomer.State + "|" +
		                               objCustomer.Country + "|" + objCustomer.Tel + "|" + objCustomer.Mobile + "|" +
		                               objCustomer.Remark + "|" + objCustomer.Email);
		        streamWriter.Close();
		        File.Move(fileName, Path.ChangeExtension(fileName, ".dat"));
		    }
		    catch
		    {
		        MessageBox.Show(@"Không thể tạo file .dat");
		        Close();
		    }
		}

		private string ConvertToObject(string dirname, string reportid)
		{
			if (dirname.Contains(reportid + "L1F")) return dirname.Replace(reportid + "L1F.bmp", "1_1.b");
			if (dirname.Contains(reportid + "L1L")) return dirname.Replace(reportid + "L1L.bmp", "1_2.b");
			if (dirname.Contains(reportid + "L1R")) return dirname.Replace(reportid + "L1R.bmp", "1_3.b");

			if (dirname.Contains(reportid + "L2F")) return dirname.Replace(reportid + "L2F.bmp", "2_1.b");
			if (dirname.Contains(reportid + "L2L")) return dirname.Replace(reportid + "L2L.bmp", "2_2.b");
			if (dirname.Contains(reportid + "L2R")) return dirname.Replace(reportid + "L2R.bmp", "2_3.b");

			if (dirname.Contains(reportid + "L3F")) return dirname.Replace(reportid + "L3F.bmp", "3_1.b");
			if (dirname.Contains(reportid + "L3L")) return dirname.Replace(reportid + "L3L.bmp", "3_2.b");
			if (dirname.Contains(reportid + "L3R")) return dirname.Replace(reportid + "L3R.bmp", "3_3.b");

			if (dirname.Contains(reportid + "L4F")) return dirname.Replace(reportid + "L4F.bmp", "4_1.b");
			if (dirname.Contains(reportid + "L4L")) return dirname.Replace(reportid + "L4L.bmp", "4_2.b");
			if (dirname.Contains(reportid + "L4R")) return dirname.Replace(reportid + "L4R.bmp", "4_3.b");

			if (dirname.Contains(reportid + "L5F")) return dirname.Replace(reportid + "L5F.bmp", "5_1.b");
			if (dirname.Contains(reportid + "L5L")) return dirname.Replace(reportid + "L5L.bmp", "5_2.b");
			if (dirname.Contains(reportid + "L5R")) return dirname.Replace(reportid + "L5R.bmp", "5_3.b");

			//

			if (dirname.Contains(reportid + "R1F")) return dirname.Replace(reportid + "R1F.bmp", "6_1.b");
			if (dirname.Contains(reportid + "R1L")) return dirname.Replace(reportid + "R1L.bmp", "6_2.b");
			if (dirname.Contains(reportid + "R1R")) return dirname.Replace(reportid + "R1R.bmp", "6_3.b");

			if (dirname.Contains(reportid + "R2F")) return dirname.Replace(reportid + "R2F.bmp", "7_1.b");
			if (dirname.Contains(reportid + "R2L")) return dirname.Replace(reportid + "R2L.bmp", "7_2.b");
			if (dirname.Contains(reportid + "R2R")) return dirname.Replace(reportid + "R2R.bmp", "7_3.b");

			if (dirname.Contains(reportid + "R3F")) return dirname.Replace(reportid + "R3F.bmp", "8_1.b");
			if (dirname.Contains(reportid + "R3L")) return dirname.Replace(reportid + "R3L.bmp", "8_2.b");
			if (dirname.Contains(reportid + "R3R")) return dirname.Replace(reportid + "R3R.bmp", "8_3.b");

			if (dirname.Contains(reportid + "R4F")) return dirname.Replace(reportid + "R4F.bmp", "9_1.b");
			if (dirname.Contains(reportid + "R4L")) return dirname.Replace(reportid + "R4L.bmp", "9_2.b");
			if (dirname.Contains(reportid + "R4R")) return dirname.Replace(reportid + "R4R.bmp", "9_3.b");

			if (dirname.Contains(reportid + "R5F")) return dirname.Replace(reportid + "R5F.bmp", "10_1.b");
			if (dirname.Contains(reportid + "R5L")) return dirname.Replace(reportid + "R5L.bmp", "10_2.b");
			if (dirname.Contains(reportid + "R5R")) return dirname.Replace(reportid + "R5R.bmp", "10_3.b");

			return string.Empty;
		}

		private string ConvertToImage(string dirname, string reportid)
		{
			if (dirname.Contains("1_1")) return dirname.Replace("1_1.b", reportid + "L1F.bmp");
			if (dirname.Contains("1_2")) return dirname.Replace("1_2.b", reportid + "L1L.bmp");
			if (dirname.Contains("1_3")) return dirname.Replace("1_3.b", reportid + "L1R.bmp");

			if (dirname.Contains("2_1")) return dirname.Replace("2_1.b", reportid + "L2F.bmp");
			if (dirname.Contains("2_2")) return dirname.Replace("2_2.b", reportid + "L2L.bmp");
			if (dirname.Contains("2_3")) return dirname.Replace("2_3.b", reportid + "L2R.bmp");

			if (dirname.Contains("3_1")) return dirname.Replace("3_1.b", reportid + "L3F.bmp");
			if (dirname.Contains("3_2")) return dirname.Replace("3_2.b", reportid + "L3L.bmp");
			if (dirname.Contains("3_3")) return dirname.Replace("3_3.b", reportid + "L3R.bmp");

			if (dirname.Contains("4_1")) return dirname.Replace("4_1.b", reportid + "L4F.bmp");
			if (dirname.Contains("4_2")) return dirname.Replace("4_2.b", reportid + "L4L.bmp");
			if (dirname.Contains("4_3")) return dirname.Replace("4_3.b", reportid + "L4R.bmp");

			if (dirname.Contains("5_1")) return dirname.Replace("5_1.b", reportid + "L5F.bmp");
			if (dirname.Contains("5_2")) return dirname.Replace("5_2.b", reportid + "L5L.bmp");
			if (dirname.Contains("5_3")) return dirname.Replace("5_3.b", reportid + "L5R.bmp");

			//

			if (dirname.Contains("6_1")) return dirname.Replace("6_1.b", reportid + "R1F.bmp");
			if (dirname.Contains("6_2")) return dirname.Replace("6_2.b", reportid + "R1L.bmp");
			if (dirname.Contains("6_3")) return dirname.Replace("6_3.b", reportid + "R1R.bmp");

			if (dirname.Contains("7_1")) return dirname.Replace("7_1.b", reportid + "R2F.bmp");
			if (dirname.Contains("7_2")) return dirname.Replace("7_2.b", reportid + "R2L.bmp");
			if (dirname.Contains("7_3")) return dirname.Replace("7_3.b", reportid + "R2R.bmp");

			if (dirname.Contains("8_1")) return dirname.Replace("8_1.b", reportid + "R3F.bmp");
			if (dirname.Contains("8_2")) return dirname.Replace("8_2.b", reportid + "R3L.bmp");
			if (dirname.Contains("8_3")) return dirname.Replace("8_3.b", reportid + "R3R.bmp");

			if (dirname.Contains("9_1")) return dirname.Replace("9_1.b", reportid + "R4F.bmp");
			if (dirname.Contains("9_2")) return dirname.Replace("9_2.b", reportid + "R4L.bmp");
			if (dirname.Contains("9_3")) return dirname.Replace("9_3.b", reportid + "R4R.bmp");

			if (dirname.Contains("10_1")) return dirname.Replace("10_1.b", reportid + "R5F.bmp");
			if (dirname.Contains("10_2")) return dirname.Replace("10_2.b", reportid + "R5L.bmp");
			if (dirname.Contains("10_3")) return dirname.Replace("10_3.b", reportid + "R5R.bmp");


			return string.Empty;
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			if (!_isinsert && CurrCustomer != null && !Issuccess)
			{
				string nameCustomer = ConvertUtil.ConverToUnsign(CurrCustomer.Name).Replace(" ", "").ToUpper();
				Insertupdatefile(_dirFile, ICurrentSessionService.DesDirNameTemp, CurrCustomer.ReportID, nameCustomer,
					"");
			}
			base.Dispose();
		}

		private void btnLuu_Click(object sender, EventArgs e)
		{
			try
			{
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    _oldNameCustomer = txtTen.Text;
                    string nameCustomer = "";
                    int id = int.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    if (obj != null)
                    {
                        _oldNameCustomer = obj.Name;
                        labReportId.Text = obj.ReportID;
                    }

                    //Them Sua Khach Hang Tai Day
                    FICustomer objCustomer = service.CreateEntity();
                    objCustomer.ID = int.Parse(labMa.Text);
                    objCustomer.AgencyID = ICurrentSessionService.UserId;
                    objCustomer.ReportID = labReportId.Text;
                    objCustomer.Name = txtTen.Text;
                    objCustomer.Address1 = txtDiaChi.Text;
                    objCustomer.City = txtThanhPho.Text;
                    objCustomer.DOB = dtpNgaySinh.Value;
                    objCustomer.Email = txtEmail.Text;
                    objCustomer.Gender = GetGender();
                    objCustomer.Mobile = txtDiDong.Text;
                    objCustomer.Tel = txtDienThoai.Text;
                    objCustomer.ZipPosTalCode = ""; //txtZip.Text;
                    objCustomer.Remark = txtGhiChu.Text;
                    objCustomer.State = ""; //txtTinhTrang.Text;

                    nameCustomer = ConvertUtil.ConverToUnsign(objCustomer.Name).Replace(" ", "").ToUpper();
                    if (_oldNameCustomer != null)
                        _oldNameCustomer = ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();

                    if (obj != null)
                    {
                        bool kq = service.Update(objCustomer);
                        if (kq)
                        {
                            Insertupdatefile(_dirFile, ICurrentSessionService.DesDirNameTemp, objCustomer.ReportID,
                                nameCustomer, _oldNameCustomer);
                            SaveDataFile(ICurrentSessionService.CurAgency, objCustomer, _dirFile);

                            _baseForm.EnglishMsg = "Update information Successfully!";
                            _baseForm.VietNamMsg = "Cập Nhật Thành Công!";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            Issuccess = true;
                            CurrCustomer = objCustomer;
                            _isinsert = false;
                        }
                        else
                        {
                            _baseForm.ShowMessage(IconMessageBox.Information, service.ErrMsg);
                        }
                    }
                    else
                    {
                        bool kq = service.Add(objCustomer);
                        if (kq)
                        {
                            Insertupdatefile(_dirFile, ICurrentSessionService.DesDirNameTemp, objCustomer.ReportID,
                                nameCustomer, "");
                            SaveDataFile(ICurrentSessionService.CurAgency, objCustomer, _dirFile);

                            _baseForm.EnglishMsg = "Add information Successfully!";
                            _baseForm.VietNamMsg = "Thêm Thành Công!";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            Issuccess = true;
                            CurrCustomer = objCustomer;
                            _isinsert = true;
                        }
                        else
                        {
                            _baseForm.ShowMessage(IconMessageBox.Information, service.ErrMsg);
                        }
                    }
                }
				btnClose.PerformClick();
			}
			catch (Exception ex)
			{
				_baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
				Environment.Exit(0);
			}
		}

		private void btnScaner_Click(object sender, EventArgs e)
		{
			try
			{
				Hide();
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    int id = int.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    labReportId.Text = ICurrentSessionService.Username + DateTime.Now.ToString("ddMMyyyyHHmmss");
                    if (obj != null)
                    {
                        labReportId.Text = obj.ReportID;
                        _oldNameCustomer = obj.Name;
                    }

                    string dirFolderOld = string.Empty;
                    if (_oldNameCustomer != null)
                    {
                        dirFolderOld = _dirFile + @"\" + labReportId.Text + "_" +
                                       ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                    }

                    string dirtemp = ICurrentSessionService.DesDirNameTemp + @"\" + labReportId.Text;

                    Directory.CreateDirectory(dirtemp);

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if (!f.Name.ToLower().Contains(".dat") &&
                                    !f.Name.ToLower().Contains(labReportId.Text.ToLower() + "f.bmp"))
                                    destFile = ConvertToImage(destFile, labReportId.Text);
                                File.Copy(f.FullName, destFile, true);
                                f.Delete();
                            }
                        }
                        d.Delete(true);
                    }

                    var frm = new FrmScan(labReportId.Text, dirtemp);
                    frm.ShowDialog(this);
                }

				Activate();
				Show();

			}
			catch (Exception ex)
			{
				_baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
				Environment.Exit(0);
			}
		}

		private void FrmCustomer_Load(object sender, EventArgs e)
		{
			LoadToUi(CurrCustomer);
		}

		private void btnCapture_Click(object sender, EventArgs e)
		{
			try
			{
				Hide();
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    int id = int.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    labReportId.Text = ICurrentSessionService.Username + DateTime.Now.ToString("ddMMyyyyHHmmss");
                    if (obj != null)
                    {
                        labReportId.Text = obj.ReportID;
                        _oldNameCustomer = obj.Name;
                    }

                    string dirFolderOld = string.Empty;
                    if (_oldNameCustomer != null)
                    {
                        dirFolderOld = _dirFile + @"\" + labReportId.Text + "_" +
                                       ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                    }

                    string dirtemp = ICurrentSessionService.DesDirNameTemp + @"\" + labReportId.Text;

                    Directory.CreateDirectory(dirtemp);

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if (!f.Name.ToLower().Contains(".dat") &&
                                    !f.Name.ToLower().Contains(labReportId.Text.ToLower() + "f.bmp"))
                                    destFile = ConvertToImage(destFile, labReportId.Text);
                                File.Copy(f.FullName, destFile, true);
                                f.Delete();
                            }
                        }
                        d.Delete(true);
                    }

                    var frm = new FrmWebCam(labReportId.Text, dirtemp);
                    frm.ShowDialog(this);
                }

				Activate();
				Show();
			}
			catch
			{
				//
			}
		}

		public int Split_String_Count(string line)
		{
			return line.Split('|').Length;
		}

		private void btnChoice_Click(object sender, EventArgs e)
		{

			try
			{
				OpenFileDialog dialog = new OpenFileDialog
				{
					Filter = @"File Khách Hàng (*.dat)|*.*",
					FileName = "*.dat",
					Multiselect = false,
					FilterIndex = 0
				};

				if (dialog.ShowDialog() != DialogResult.OK) return;
				txtDirFile.Text = dialog.FileName;
				string dirfile = dialog.FileName;

				if (!File.Exists(dirfile)) return;
				string str = "";
				string line = "";
				FIAgency objAgency = null;
				FICustomer objCustomer = null;
				TextReader reader = new StreamReader(dirfile, Encoding.UTF8);
				while (true)
				{
					if (str == "c")
					{
						line = line + "\n" + reader.ReadLine();
						str = "";
					}
					else
					{
						line = reader.ReadLine();
					}
					if (line == null)
					{
						break;
					}
					int num = Split_String_Count(line);
					if ((num == 25) | (num == 18) | (num == 8))
					{
						string[] strArray = line.Split('|');
						if (strArray[0] == "0")
						{
                            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                            {
                                IFIAgencyService service = new FIAgencyService(unitOfWork);
                                string agencyusername = strArray[1];
                                string name = strArray[2];
                                string address = strArray[6];
                                string phone = strArray[13];
                                if (agencyusername == "") break;
                                objAgency = ICurrentSessionService.CurAgency;
                                if (objAgency == null)
                                {
                                    MessageBox.Show(@"Không tồn tại đại lý :
																Mã : " + agencyusername +
                                                    @".
																Tên đại lý :" + name + @".
																Địa Chỉ:" + address +
                                                    @".
																Điện thoại:" + phone, @"Thông Báo",
                                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    break;

                                }
                                //CurrAgency = objAgency;
                            }

						}
						else if (strArray[0] == "1")
						{
							string reportid = strArray[2];
							if (reportid == "") break;
							if (objAgency == null) break;
							int agencyId = objAgency.ID;

                            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                            {
                                IFICustomerService service = new FICustomerService(unitOfWork);
                                int customerId = 0;
                                var objtemp = service.FindByReportId(reportid);
                                if (objtemp != null)
                                {
                                    customerId = objtemp.ID;
                                }

                                objCustomer = new FICustomer
                                {
                                    ID = customerId,
                                    AgencyID = agencyId,
                                    ReportID = strArray[2],
                                    Date = DateTimeUtil.ConvertDate(strArray[3]),
                                    Name = strArray[4],
                                    Gender = strArray[6],
                                    Parent = strArray[5],
                                    DOB = DateTimeUtil.ConvertDate(strArray[7]),
                                    Address1 = strArray[8],
                                    Address2 = strArray[9],
                                    City = strArray[10],
                                    ZipPosTalCode = strArray[11],
                                    State = strArray[12],
                                    Country = strArray[13],
                                    Tel = strArray[14],
                                    Mobile = strArray[15],
                                    Remark = strArray[16],
                                    Email = strArray[17]
                                };
                                LoadToUi(objCustomer);
                                //btnLuu.Enabled = true;
                            }
						}

						if ((strArray[0] != "2")) continue;
						if (objAgency == null) continue;
						if (objCustomer == null) continue;
						//AddListFingerRecord(objAgency, objCustomer, strArray);
					}
					else
					{
						str = "c";
					}
				}
				reader.Close();
				reader.Dispose();
				_parentDirFile = Directory.GetParent(dirfile).FullName;
				btnLuu.Enabled = true;
			}
			catch (Exception)
			{
				//
				//throw;
			}

		}

		private void AddListFingerRecord(FIAgency objAgency, FICustomer objCustomer, string[] strArray)
		{
			try
			{
				//using (IFingerRecordService service = new FingerRecordService())
				//{
				//    if (objAgency == null) return;
				//    if (objCustomer == null) return;

				//    var objtemp = service.Find(objCustomer.ID, strArray[3]) ?? service.CreateEntity();

				//    objtemp.AgencyID = objAgency.ID;
				//    objtemp.ReportID = objCustomer.ID;
				//    objtemp.Type = strArray[3];
				//    objtemp.ATDPoint = decimal.Parse(strArray[4]);
				//    objtemp.RCCount = decimal.Parse(strArray[5]);
				//    objtemp.FingerType = strArray[6];
				//    objtemp.ScanCheck = strArray[7] != "";
				//    _listRecords.Add(objtemp);
				//}
			}
			catch
			{
				//
			}
		}
	}
}
