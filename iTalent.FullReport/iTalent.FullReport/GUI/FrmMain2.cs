﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entity;
using iTalent.FullReport.FlashForm;
using iTalent.Utils;

namespace iTalent.FullReport.GUI
{
    public partial class FrmMain2 : C4FunForm
    {
        public bool Issuccess { get; set; }
        private Config _config;
        private BaseForm _baseForm;
        private string _dirFile;
        public FICustomer CurrCustomer { get; set; }
        public ICollection<FICustomer> ListCustomers { get; set; }

        public FrmMain2()
        {
            InitializeComponent();
            //ICurrentSessionService.CheckLicense();
        }

        private void LoadAll()
        {
            try
            {
                Hide();
                ICurrentSessionService.Version = "ULTIMATE";
                _config = new Config();
                BaseForm.Frm = this;
                BaseForm.DgView = dgvCustomer;
                BaseForm.ListconControls = new List<Control>
                {
                    btnAdd,
                    btnConfig,
                    btnDelete,
                    btnEdit,
                    btnExport,
                    btnRefresh,
                    labFromDate,
                    labSearch,
                    labToDate,
                    rbAddress,
                    rbID,
                    rbName,
                    rbParent,
                    rbTel,
                    btnImportProfile,
                    btnAnalysis,
                    btnExportReport,
                    btnImportTemlate,
                    btnImportKey,
                };
                _baseForm = new BaseForm();
                
                if (_config.LocalSaveImages == "")
                {
                    FrmConfig frmC = new FrmConfig();
                    frmC.ShowDialog();
                }
                //TODO: Enable again
                //_baseForm.CheckDevice(true);
                CheckRegisted();

             

                //FrmFlash.ShowSplash();
                Application.DoEvents();

                string sourceDirName = ICurrentSessionService.DesDirNameSource;
                if (!Directory.Exists(sourceDirName))
                    Directory.CreateDirectory(sourceDirName);

                string sourceDirNameTemp = ICurrentSessionService.DesDirNameTemp;
                if (!Directory.Exists(sourceDirNameTemp))
                    Directory.CreateDirectory(sourceDirNameTemp);

                string sourceTemplate = ICurrentSessionService.DesDirNameTemplate;
                if (!Directory.Exists(sourceTemplate))
                    Directory.CreateDirectory(sourceTemplate);

                string tempexcel = ICurrentSessionService.TemplateFolderName;
                if (!Directory.Exists(tempexcel))
                {
                    Directory.CreateDirectory(tempexcel);
                    string filepathtemp = Path.Combine(Directory.GetCurrentDirectory(), "template");

                    //Copy tempexcel tai day.
                    var d = new DirectoryInfo(filepathtemp);
                    var infos = d.GetFiles();
                    if (!d.GetFiles().Any()) return;

                    foreach (var f in infos)
                    {
                        var fileName = Path.GetFileName(f.Name);
                        string destFile = Path.Combine(tempexcel, fileName);
                        File.Copy(f.FullName, destFile,true);
                    }
                }
                

                AddTemptale();

                WindowState = FormWindowState.Maximized;

                dgvCustomer.AutoGenerateColumns = false;
                LoadData();

                _dirFile = sourceDirName;
                //FrmFlash.CloseSplash();
                Activate();
                Show();
               
            }
            catch (Exception ex)
            {
                FrmFlash.CloseSplash();
                Activate();
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }

        
        private void SetLisence()
        {
            try
            {
                const int point = 10000;
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIPointModelService service = new FIPointModelService(unitOfWork);

                    PointModel model = new PointModel
                    {
                        AgentId = ICurrentSessionService.CurAgency.ID,
                        PointOriginal = point,
                        Version = ICurrentSessionService.Version
                    };

                    int numkey = service.GetNumberPointLeft(ICurrentSessionService.CurAgency.ID, ICurrentSessionService.Version);
                    if (numkey <= 0)
                    {
                        bool res = service.GenerateNewKey(ref model);

                        if (!res)
                        {
                            _baseForm.ShowMessage(IconMessageBox.Warning, service.ErrMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        private void LoadData()
        {
            try
            {

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    dgvCustomer.DataSource = service.LoadAll();
                    dgvCustomer.Refresh();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }

        private void LoadData(ICollection<FICustomer> listCustomers)
        {
            try
            {
                dgvCustomer.AutoGenerateColumns = false;
                dgvCustomer.SuspendLayout();
                dgvCustomer.DataSource = listCustomers;
                dgvCustomer.Refresh();
                dgvCustomer.ResumeLayout();
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
            }
        }

        private void AddTemptale()
        {
            string tempAdult = Path.Combine(ICurrentSessionService.DesDirNameTemplate, "Adult.rptx");
            string tempChild = Path.Combine(ICurrentSessionService.DesDirNameTemplate, "Children.rptx");

            //if (Directory.Exists(tempAdult) && Directory.Exists(tempChildren)) return;
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFITemplateService service = new FITemplateService(unitOfWork);

                tempAdult = Path.Combine(Directory.GetCurrentDirectory(), "adult.rptx");
                tempChild = Path.Combine(Directory.GetCurrentDirectory(), "children.rptx");

                bool res = service.ImportRptx(tempAdult, 1);
                if (!res)
                {
                    _baseForm.ShowMessage(IconMessageBox.Warning, "ultimate:\n" + service.ErrMsg);
                }

                res = service.ImportRptx(tempChild, 2);
                if (!res)
                {
                    _baseForm.ShowMessage(IconMessageBox.Warning, "ultimate:\n" + service.ErrMsg);
                }
            }
        }

        private void CheckRegisted()
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFIAgencyService service = new FIAgencyService(unitOfWork);
                    ICollection<FIAgency> listAgencies = service.FindAll();
                    if (listAgencies != null && listAgencies.Count > 0)
                    {
                        FrmLogin frm = new FrmLogin();
                        frm.ShowDialog(this);

                        //if (!frm.Issuccess)
                        //    Environment.Exit(0);

                        bool kq = frm.Issuccess; //service.Login("123456", "123456"); //test 
                        if (!kq)
                        {
                            _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                            Environment.Exit(0);
                        }
                        else
                        {
                            //if (!CheckKey())
                            //    Environment.Exit(0);
                            bool check = CheckKey();//GeneratePos.CheckKey(ICurrentSessionService.CurAgency.SecKey);
                            if (!check)
                            {
                                //_baseForm.VietNamMsg = "Key đăng ký không hợp lệ!";
                                //_baseForm.EnglishMsg = "Key is not valid!";
                                //_baseForm.ShowMessage(IconMessageBox.Information);

                                // Environment.Exit(0);

                                _baseForm.VietNamMsg = "Key không hợp lệ!\nVui lòng đăng ký lại!";
                                _baseForm.EnglishMsg = "Key is not valid!\nPlease register again!";
                                _baseForm.ShowMessage(IconMessageBox.Information);

                                FrmSignup frmsigup = new FrmSignup();
                                frmsigup.ShowDialog(this);

                                if (!frmsigup.Issuccess)
                                    Environment.Exit(0);
                                else
                                {
                               
                                    Application.Restart();
                                }
                            }
                            SetLisence();
                        }
                    }
                    else
                    {
                        _baseForm.VietNamMsg = @"Để sử dụng hệ thống bạn phải đăng ký trước!";
                        _baseForm.EnglishMsg = @"You need to register your information before using the system!";
                        _baseForm.ShowMessage(IconMessageBox.Information);

                        FrmSignup frm = new FrmSignup();
                        frm.ShowDialog(this);

                        if (!frm.Issuccess)
                            Environment.Exit(0);
                        else
                        {
                            Application.Restart();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                Environment.Exit(0);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmCustomer customer = new FrmCustomer();
            customer.ShowDialog();
            if (customer.Issuccess)
                LoadData();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            Hide();
            FrmConfig frm = new FrmConfig();
            frm.ShowDialog(this);
            Show();

        }

        private void dgvCustomer_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    int id = int.Parse(dgvCustomer.SelectedRows[0].Cells[0].Value.ToString());
                    CurrCustomer = service.Find(id);
                    FrmCustomer customer = new FrmCustomer(CurrCustomer);
                    customer.ShowDialog();
                    if (customer.Issuccess)
                        LoadData();
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                //Environment.Exit(0);
            }
        }

        private void dgvCustomer_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvCustomer.SelectedRows[0] == null) return;
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    int id = int.Parse(dgvCustomer.SelectedRows[0].Cells[0].Value.ToString());
                    CurrCustomer = service.Find(id);
                }
            }
            catch
            {
                //_baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                //Environment.Exit(0);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            FrmCustomer customer = new FrmCustomer(CurrCustomer);
            customer.ShowDialog();
            if (customer.Issuccess)
                LoadData();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                ICurrentSessionService.VietNamLanguage
                    ? "Bạn có muốn xóa khách hàng này không?"
                    : "Do you want delete this customer?",
                ICurrentSessionService.VietNamLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) != DialogResult.Yes)
                return;

            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFICustomerService service = new FICustomerService(unitOfWork);
                int id = int.Parse(dgvCustomer.SelectedRows[0].Cells[0].Value.ToString());
                CurrCustomer = service.Find(id);
                if (CurrCustomer != null)
                {
                    //string oldNameCustomer = CurrCustomer.Name;
                    //string reportid = CurrCustomer.ReportID;

                    bool kq = service.Delete(CurrCustomer);
                    if (!kq)
                    {
                        _baseForm.ShowMessage(IconMessageBox.Error, service.ErrMsg);
                    }
                    else
                    {
                        //var dirFolderOld = _dirFile + @"\" + reportid + "_" +
                        //                   ConvertUtil.ConverToUnsign(oldNameCustomer).Replace(" ", "").ToUpper();
                        //Directory.Delete(dirFolderOld, true);
                        _baseForm.EnglishMsg = "Delete Successfully!";
                        _baseForm.VietNamMsg = "Xóa Thành Công";
                        _baseForm.ShowMessage(IconMessageBox.Information);
                        LoadData();
                    }
                }
                else
                {
                    _baseForm.EnglishMsg = "Please choose customer to delete!";
                    _baseForm.VietNamMsg = "Chọn một khách hàng để xóa!";
                    _baseForm.ShowMessage(IconMessageBox.Warning);
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            var folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK) return;
            try
            {
                Hide();
                FrmFlash.ShowSplash();
                Application.DoEvents();

                var colremoves = new List<string>
                {
                    "ID",
                    "AgencyID",
                    "FIAgency",
                    "FIFingerAnalysis",
                    "FIFingerRecord",
                    "FIMQChart",
                    "FIPrintRecord"
                };

                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    ListCustomers = service.LoadAll();
                }

                var dtTable = ConvertUtil.ToDataTable(ListCustomers, colremoves);

                CreateExcelFile.CreateExcelDocument(dtTable, folderBrowserDialog.SelectedPath + "\\Customers_" +
                    DateTimeUtil.GetCurrentTime().ToString("ddMMyyHHmmss") + ".xlsx");
               
                //var exportExcel = new ExportExcel();

                //exportExcel.ExportDanhSachKhachHang(DateTimeUtil.GetCurrentTime().ToString("dd/MM/yyyy"), dtTable,
                //    folderBrowserDialog.SelectedPath + "\\Customers_" +
                //    DateTimeUtil.GetCurrentTime().ToString("ddMMyyHHmmss") + ".xls");

                FrmFlash.CloseSplash();
                Activate();
                Show();

                //TODO : thay doi vi tri
                //_baseForm.ShowMessage(IconMessageBox.Information, exportExcel.ErrMsg);
            }
            catch (Exception ex)
            {
                FrmFlash.CloseSplash();
                Activate();
                Show();

                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
                //MessageBox.Show(@"Please uncheck 'Open file after export' if excel is not installed on your computer",
                //    @"Error opening file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSearch2_Click(object sender, EventArgs e)
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IFICustomerService service = new FICustomerService(unitOfWork);
                var customers = service.Search(dtpTuNgay.Value, dtpDenNgay.Value);
                LoadData(customers);
            }
        }

        private void btnSearch1_Click(object sender, EventArgs e)
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    ICollection<FICustomer> customers = new List<FICustomer>();
                    if (rbName.Checked)
                        customers = service.SearchByName(txtSearch.Text);
                    else if (rbAddress.Checked)
                        customers = service.SearchByAddress(txtSearch.Text);
                    else if (rbParent.Checked)
                        customers = service.SearchByParent(txtSearch.Text);
                    else if (rbTel.Checked)
                        customers = service.SearchByTel(txtSearch.Text);
                    else if (rbID.Checked)
                    {
                        customers = service.SearchByReportId(txtSearch.Text);
                        //long id = long.Parse(txtSearch.Text);
                        //FICustomer objCustomer = service.Find(id);
                        //if (objCustomer != null)
                        //{
                        //    customers.Add(objCustomer);
                        //}

                    }

                    if (customers == null)
                    {
                        customers = new List<FICustomer>();
                    }

                    LoadData(customers);
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Warning, ex.Message);
            }
        }

        private void FrmMain2_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (Directory.Exists(ICurrentSessionService.DesDirNameTemp))
                    Directory.Delete(ICurrentSessionService.DesDirNameTemp, true);
            }
            catch
            {
                //
            }
        }

        private void btnImportProfile_Click(object sender, EventArgs e)
        {
            FrmImportCustomer frm = new FrmImportCustomer();
            DialogResult result = frm.ShowDialog(this);
            //if (frm.Issuccess)
            LoadData();
        }

        private void btnImportTemlate_Click(object sender, EventArgs e)
        {
            frmImportTemlate frm = new frmImportTemlate();
            frm.ShowDialog(this);
        }

        private void btnImportKey_Click(object sender, EventArgs e)
        {
            frmImportKey frm = new frmImportKey();
            frm.ShowDialog(this);
        }

        private void btnAnalysis_Click(object sender, EventArgs e)
        {
            if (CurrCustomer != null)
            {
                Hide();
                frmAnalysis frm = new frmAnalysis(CurrCustomer.ID);
                frm.ShowDialog();
                Show();
                Activate();
            }
            else
            {
                _baseForm.EnglishMsg = "Please choose customer to analyse!";
                _baseForm.VietNamMsg = "Chọn một khách hàng để phân tích!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private void btnExportReport_Click(object sender, EventArgs e)
        {
            if (CurrCustomer != null)
            {
                frmExportReport frm = new frmExportReport(CurrCustomer.ID);
                frm.ShowDialog();
            }
            else
            {
                _baseForm.EnglishMsg = "Please choose customer to analyse!";
                _baseForm.VietNamMsg = "Chọn một khách hàng để phân tích!";
                _baseForm.ShowMessage(IconMessageBox.Warning);
            }
        }

        private bool CheckKey()
        {
            try
            {
                using (IInstallLicenseService installLicense = new InstallLicenseService())
                {
                    bool check = installLicense.CheckLicense(ICurrentSessionService.CurAgency.SecKey);
                    if (!check)
                    {
                        _baseForm.VietNamMsg = "Key không hợp lệ! Bạn cần phải đăng ký lại!";
                        _baseForm.EnglishMsg = "Key is not valid! You must register again!";
                        _baseForm.ShowMessage(IconMessageBox.Information);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _baseForm.VietNamMsg = "Key không hợp lệ!";
                _baseForm.EnglishMsg = "Key is not valid!";
                _baseForm.ShowMessage(IconMessageBox.Information);
                return false;
            }
            return true;
        }

        private void btnUpdateKeyLi_Click(object sender, EventArgs e)
        {

        }
    }
}
