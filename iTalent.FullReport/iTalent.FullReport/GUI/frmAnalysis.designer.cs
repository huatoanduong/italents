﻿namespace iTalent.FullReport.GUI
{
    partial class frmAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAnalysis));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("WT", "WT.bmp");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("WS", "WS.bmp");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("WX", "WX.bmp");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("WE", "WE.bmp");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("WC", "WC.bmp");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("WD", "WD.bmp");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("WI", "WI.bmp");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("WP", "WP.bmp");
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("WL", "WL.bmp");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("UL", "UL.bmp");
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("LF", "LF.bmp");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("RL", "RL.bmp");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("AS", "AS.bmp");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("AU", "AU.bmp");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("AT", "AT.bmp");
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("AE", "AE.bmp");
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem("AR", "AR.bmp");
            this.lblShowLeft = new C4FunComponent.Toolkit.C4FunLabel();
            this.lblShowRight = new C4FunComponent.Toolkit.C4FunLabel();
            this.grpAnalysis = new C4FunComponent.Toolkit.C4FunGroupBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.pnl1 = new System.Windows.Forms.Panel();
            this.lblL1P = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.L1F = new System.Windows.Forms.PictureBox();
            this.txtL1P = new System.Windows.Forms.TextBox();
            this.btnL1R = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.L1R = new System.Windows.Forms.PictureBox();
            this.btnL1L = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.L1P = new System.Windows.Forms.PictureBox();
            this.txtL1R = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.L1L = new System.Windows.Forms.PictureBox();
            this.txtL1L = new System.Windows.Forms.TextBox();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.pnl2 = new System.Windows.Forms.Panel();
            this.lblL2P = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.L2F = new System.Windows.Forms.PictureBox();
            this.txtL2P = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.L2R = new System.Windows.Forms.PictureBox();
            this.btnL2R = new System.Windows.Forms.Button();
            this.btnL2L = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.L2P = new System.Windows.Forms.PictureBox();
            this.txtL2R = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.L2L = new System.Windows.Forms.PictureBox();
            this.txtL2L = new System.Windows.Forms.TextBox();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.pnl3 = new System.Windows.Forms.Panel();
            this.lblL3P = new System.Windows.Forms.Label();
            this.GroupBox15 = new System.Windows.Forms.GroupBox();
            this.L3F = new System.Windows.Forms.PictureBox();
            this.btnL3R = new System.Windows.Forms.Button();
            this.txtL3P = new System.Windows.Forms.TextBox();
            this.GroupBox14 = new System.Windows.Forms.GroupBox();
            this.L3R = new System.Windows.Forms.PictureBox();
            this.btnL3L = new System.Windows.Forms.Button();
            this.GroupBox13 = new System.Windows.Forms.GroupBox();
            this.L3P = new System.Windows.Forms.PictureBox();
            this.txtL3R = new System.Windows.Forms.TextBox();
            this.GroupBox12 = new System.Windows.Forms.GroupBox();
            this.L3L = new System.Windows.Forms.PictureBox();
            this.txtL3L = new System.Windows.Forms.TextBox();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.pnl4 = new System.Windows.Forms.Panel();
            this.lblL4P = new System.Windows.Forms.Label();
            this.txtL4L = new System.Windows.Forms.TextBox();
            this.btnL4R = new System.Windows.Forms.Button();
            this.txtL4R = new System.Windows.Forms.TextBox();
            this.btnL4L = new System.Windows.Forms.Button();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.L4F = new System.Windows.Forms.PictureBox();
            this.txtL4P = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.L4R = new System.Windows.Forms.PictureBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.L4P = new System.Windows.Forms.PictureBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.L4L = new System.Windows.Forms.PictureBox();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.pnl5 = new System.Windows.Forms.Panel();
            this.lblL5P = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.L5F = new System.Windows.Forms.PictureBox();
            this.txtL5P = new System.Windows.Forms.TextBox();
            this.btnL5R = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.L5R = new System.Windows.Forms.PictureBox();
            this.btnL5L = new System.Windows.Forms.Button();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.L5P = new System.Windows.Forms.PictureBox();
            this.txtL5R = new System.Windows.Forms.TextBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.L5L = new System.Windows.Forms.PictureBox();
            this.txtL5L = new System.Windows.Forms.TextBox();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.pnl6 = new System.Windows.Forms.Panel();
            this.lblR1P = new System.Windows.Forms.Label();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.R1F = new System.Windows.Forms.PictureBox();
            this.txtR1P = new System.Windows.Forms.TextBox();
            this.btnR1R = new System.Windows.Forms.Button();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.R1R = new System.Windows.Forms.PictureBox();
            this.btnR1L = new System.Windows.Forms.Button();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.R1P = new System.Windows.Forms.PictureBox();
            this.txtR1R = new System.Windows.Forms.TextBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.R1L = new System.Windows.Forms.PictureBox();
            this.txtR1L = new System.Windows.Forms.TextBox();
            this.TabPage7 = new System.Windows.Forms.TabPage();
            this.pnl7 = new System.Windows.Forms.Panel();
            this.lblR2P = new System.Windows.Forms.Label();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.R2F = new System.Windows.Forms.PictureBox();
            this.txtR2P = new System.Windows.Forms.TextBox();
            this.btnR2R = new System.Windows.Forms.Button();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.R2R = new System.Windows.Forms.PictureBox();
            this.btnR2L = new System.Windows.Forms.Button();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.R2P = new System.Windows.Forms.PictureBox();
            this.txtR2R = new System.Windows.Forms.TextBox();
            this.txtR2L = new System.Windows.Forms.TextBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.R2L = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.TabPage8 = new System.Windows.Forms.TabPage();
            this.pnl8 = new System.Windows.Forms.Panel();
            this.lblR3P = new System.Windows.Forms.Label();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.R3F = new System.Windows.Forms.PictureBox();
            this.txtR3P = new System.Windows.Forms.TextBox();
            this.btnR3R = new System.Windows.Forms.Button();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.R3R = new System.Windows.Forms.PictureBox();
            this.btnR3L = new System.Windows.Forms.Button();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.R3P = new System.Windows.Forms.PictureBox();
            this.txtR3R = new System.Windows.Forms.TextBox();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.R3L = new System.Windows.Forms.PictureBox();
            this.txtR3L = new System.Windows.Forms.TextBox();
            this.TabPage9 = new System.Windows.Forms.TabPage();
            this.pnl9 = new System.Windows.Forms.Panel();
            this.lblR4P = new System.Windows.Forms.Label();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.R4F = new System.Windows.Forms.PictureBox();
            this.txtR4P = new System.Windows.Forms.TextBox();
            this.btnR4R = new System.Windows.Forms.Button();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.R4R = new System.Windows.Forms.PictureBox();
            this.btnR4L = new System.Windows.Forms.Button();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.R4P = new System.Windows.Forms.PictureBox();
            this.txtR4R = new System.Windows.Forms.TextBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.R4L = new System.Windows.Forms.PictureBox();
            this.txtR4L = new System.Windows.Forms.TextBox();
            this.TabPage10 = new System.Windows.Forms.TabPage();
            this.pnl10 = new System.Windows.Forms.Panel();
            this.lblR5P = new System.Windows.Forms.Label();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this.R5F = new System.Windows.Forms.PictureBox();
            this.txtR5P = new System.Windows.Forms.TextBox();
            this.btnR5R = new System.Windows.Forms.Button();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this.R5R = new System.Windows.Forms.PictureBox();
            this.btnR5L = new System.Windows.Forms.Button();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.R5P = new System.Windows.Forms.PictureBox();
            this.txtR5R = new System.Windows.Forms.TextBox();
            this.groupBox43 = new System.Windows.Forms.GroupBox();
            this.R5L = new System.Windows.Forms.PictureBox();
            this.txtR5L = new System.Windows.Forms.TextBox();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.pbF = new System.Windows.Forms.PictureBox();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.labReportId = new System.Windows.Forms.Label();
            this.labMa = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.labParent = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.labCity = new System.Windows.Forms.Label();
            this.txtTenChaMe = new System.Windows.Forms.TextBox();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.labAddress = new System.Windows.Forms.Label();
            this.labMobile = new System.Windows.Forms.Label();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.labSex = new System.Windows.Forms.Label();
            this.labBirthday = new System.Windows.Forms.Label();
            this.labNote = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.labPhone = new System.Windows.Forms.Label();
            this.labEmail = new System.Windows.Forms.Label();
            this.c4FunGroupBox1 = new C4FunComponent.Toolkit.C4FunGroupBox();
            this.labDateCreate = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtRATD = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtLATD = new System.Windows.Forms.TextBox();
            this.gbShowResult = new C4FunComponent.Toolkit.C4FunGroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtR1PResult = new System.Windows.Forms.TextBox();
            this.txtR2PResult = new System.Windows.Forms.TextBox();
            this.txtR3PResult = new System.Windows.Forms.TextBox();
            this.txtR4PResult = new System.Windows.Forms.TextBox();
            this.txtR5PResult = new System.Windows.Forms.TextBox();
            this.txtL5PResult = new System.Windows.Forms.TextBox();
            this.txtL4PResult = new System.Windows.Forms.TextBox();
            this.txtL3PResult = new System.Windows.Forms.TextBox();
            this.txtL2PResult = new System.Windows.Forms.TextBox();
            this.txtL1PResult = new System.Windows.Forms.TextBox();
            this.txtR1RResult = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtR1LResult = new System.Windows.Forms.TextBox();
            this.txtR2RResult = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtR2LResult = new System.Windows.Forms.TextBox();
            this.txtR3RResult = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtR3LResult = new System.Windows.Forms.TextBox();
            this.txtR4RResult = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtR4LResult = new System.Windows.Forms.TextBox();
            this.txtR5RResult = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtR5LResult = new System.Windows.Forms.TextBox();
            this.txtL5RResult = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtL5LResult = new System.Windows.Forms.TextBox();
            this.txtL4RResult = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtL4LResult = new System.Windows.Forms.TextBox();
            this.txtL3RResult = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtL3LResult = new System.Windows.Forms.TextBox();
            this.txtL2RResult = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtL2LResult = new System.Windows.Forms.TextBox();
            this.c4FunLabel2 = new C4FunComponent.Toolkit.C4FunLabel();
            this.c4FunLabel1 = new C4FunComponent.Toolkit.C4FunLabel();
            this.txtL1RResult = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtL1LResult = new System.Windows.Forms.TextBox();
            this.picHand = new System.Windows.Forms.PictureBox();
            this.pnlAnalysis = new System.Windows.Forms.Panel();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lvL1 = new System.Windows.Forms.ListView();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.lablNameForm = new System.Windows.Forms.Label();
            this.btnThoat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grpAnalysis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpAnalysis.Panel)).BeginInit();
            this.grpAnalysis.Panel.SuspendLayout();
            this.grpAnalysis.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.pnl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L1F)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L1R)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L1P)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L1L)).BeginInit();
            this.TabPage2.SuspendLayout();
            this.pnl2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L2F)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L2R)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L2P)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L2L)).BeginInit();
            this.TabPage3.SuspendLayout();
            this.pnl3.SuspendLayout();
            this.GroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L3F)).BeginInit();
            this.GroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L3R)).BeginInit();
            this.GroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L3P)).BeginInit();
            this.GroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L3L)).BeginInit();
            this.TabPage4.SuspendLayout();
            this.pnl4.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L4F)).BeginInit();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L4R)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L4P)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L4L)).BeginInit();
            this.TabPage5.SuspendLayout();
            this.pnl5.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L5F)).BeginInit();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L5R)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L5P)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L5L)).BeginInit();
            this.TabPage6.SuspendLayout();
            this.pnl6.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1F)).BeginInit();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1R)).BeginInit();
            this.groupBox26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1P)).BeginInit();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R1L)).BeginInit();
            this.TabPage7.SuspendLayout();
            this.pnl7.SuspendLayout();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R2F)).BeginInit();
            this.groupBox29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R2R)).BeginInit();
            this.groupBox30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R2P)).BeginInit();
            this.groupBox31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R2L)).BeginInit();
            this.TabPage8.SuspendLayout();
            this.pnl8.SuspendLayout();
            this.groupBox32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R3F)).BeginInit();
            this.groupBox33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R3R)).BeginInit();
            this.groupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R3P)).BeginInit();
            this.groupBox35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R3L)).BeginInit();
            this.TabPage9.SuspendLayout();
            this.pnl9.SuspendLayout();
            this.groupBox36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R4F)).BeginInit();
            this.groupBox37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R4R)).BeginInit();
            this.groupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R4P)).BeginInit();
            this.groupBox39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R4L)).BeginInit();
            this.TabPage10.SuspendLayout();
            this.pnl10.SuspendLayout();
            this.groupBox40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R5F)).BeginInit();
            this.groupBox41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R5R)).BeginInit();
            this.groupBox42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R5P)).BeginInit();
            this.groupBox43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.R5L)).BeginInit();
            this.GroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1.Panel)).BeginInit();
            this.c4FunGroupBox1.Panel.SuspendLayout();
            this.c4FunGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult.Panel)).BeginInit();
            this.gbShowResult.Panel.SuspendLayout();
            this.gbShowResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHand)).BeginInit();
            this.pnlAnalysis.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblShowLeft
            // 
            this.lblShowLeft.Location = new System.Drawing.Point(179, 334);
            this.lblShowLeft.Name = "lblShowLeft";
            this.lblShowLeft.Size = new System.Drawing.Size(19, 20);
            this.lblShowLeft.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.lblShowLeft.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblShowLeft.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowLeft.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowLeft.TabIndex = 57;
            this.lblShowLeft.Values.Text = "L";
            // 
            // lblShowRight
            // 
            this.lblShowRight.Location = new System.Drawing.Point(361, 335);
            this.lblShowRight.Name = "lblShowRight";
            this.lblShowRight.Size = new System.Drawing.Size(22, 20);
            this.lblShowRight.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.lblShowRight.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblShowRight.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowRight.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.lblShowRight.TabIndex = 58;
            this.lblShowRight.Values.Text = "R";
            // 
            // grpAnalysis
            // 
            this.grpAnalysis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAnalysis.Location = new System.Drawing.Point(0, 0);
            this.grpAnalysis.Name = "grpAnalysis";
            this.grpAnalysis.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // grpAnalysis.Panel
            // 
            this.grpAnalysis.Panel.Controls.Add(this.tabControl);
            this.grpAnalysis.Size = new System.Drawing.Size(520, 300);
            this.grpAnalysis.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.grpAnalysis.StateCommon.Back.Color2 = System.Drawing.Color.Green;
            this.grpAnalysis.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.grpAnalysis.StateCommon.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.grpAnalysis.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpAnalysis.TabIndex = 59;
            this.grpAnalysis.Tag = "PHÂN TÍCH ";
            this.grpAnalysis.Values.Heading = "FP ANALYSIS";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.TabPage1);
            this.tabControl.Controls.Add(this.TabPage2);
            this.tabControl.Controls.Add(this.TabPage3);
            this.tabControl.Controls.Add(this.TabPage4);
            this.tabControl.Controls.Add(this.TabPage5);
            this.tabControl.Controls.Add(this.TabPage6);
            this.tabControl.Controls.Add(this.TabPage7);
            this.tabControl.Controls.Add(this.TabPage8);
            this.tabControl.Controls.Add(this.TabPage9);
            this.tabControl.Controls.Add(this.TabPage10);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.ItemSize = new System.Drawing.Size(45, 34);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Drawing.Point(20, 10);
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(516, 276);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 155;
            this.tabControl.Tag = "2";
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            // 
            // TabPage1
            // 
            this.TabPage1.AutoScroll = true;
            this.TabPage1.BackColor = System.Drawing.Color.White;
            this.TabPage1.Controls.Add(this.pnl1);
            this.TabPage1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.TabPage1.Location = new System.Drawing.Point(4, 38);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Size = new System.Drawing.Size(508, 234);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Tag = "";
            this.TabPage1.Text = "L1";
            // 
            // pnl1
            // 
            this.pnl1.Controls.Add(this.lblL1P);
            this.pnl1.Controls.Add(this.groupBox1);
            this.pnl1.Controls.Add(this.txtL1P);
            this.pnl1.Controls.Add(this.btnL1R);
            this.pnl1.Controls.Add(this.groupBox2);
            this.pnl1.Controls.Add(this.btnL1L);
            this.pnl1.Controls.Add(this.groupBox3);
            this.pnl1.Controls.Add(this.txtL1R);
            this.pnl1.Controls.Add(this.groupBox4);
            this.pnl1.Controls.Add(this.txtL1L);
            this.pnl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl1.Location = new System.Drawing.Point(0, 0);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(508, 234);
            this.pnl1.TabIndex = 47;
            this.pnl1.Tag = "1";
            // 
            // lblL1P
            // 
            this.lblL1P.AutoSize = true;
            this.lblL1P.Location = new System.Drawing.Point(465, 181);
            this.lblL1P.Name = "lblL1P";
            this.lblL1P.Size = new System.Drawing.Size(25, 15);
            this.lblL1P.TabIndex = 45;
            this.lblL1P.Text = "L1P";
            this.lblL1P.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.L1F);
            this.groupBox1.Location = new System.Drawing.Point(6, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox1.Size = new System.Drawing.Size(120, 160);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Tag = "Ngón Cái";
            this.groupBox1.Text = "THUMP";
            // 
            // L1F
            // 
            this.L1F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L1F.Location = new System.Drawing.Point(1, 17);
            this.L1F.Name = "L1F";
            this.L1F.Size = new System.Drawing.Size(118, 142);
            this.L1F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L1F.TabIndex = 0;
            this.L1F.TabStop = false;
            this.L1F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtL1P
            // 
            this.txtL1P.Location = new System.Drawing.Point(387, 177);
            this.txtL1P.Name = "txtL1P";
            this.txtL1P.Size = new System.Drawing.Size(50, 23);
            this.txtL1P.TabIndex = 40;
            this.txtL1P.Visible = false;
            this.txtL1P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnL1R
            // 
            this.btnL1R.Location = new System.Drawing.Point(346, 174);
            this.btnL1R.Name = "btnL1R";
            this.btnL1R.Size = new System.Drawing.Size(31, 29);
            this.btnL1R.TabIndex = 44;
            this.btnL1R.Tag = "1_3";
            this.btnL1R.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.L1R);
            this.groupBox2.Location = new System.Drawing.Point(258, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox2.Size = new System.Drawing.Size(120, 160);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Tag = "Bên Phải";
            this.groupBox2.Text = "RIGHT";
            // 
            // L1R
            // 
            this.L1R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L1R.Location = new System.Drawing.Point(1, 17);
            this.L1R.Name = "L1R";
            this.L1R.Size = new System.Drawing.Size(118, 142);
            this.L1R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L1R.TabIndex = 1;
            this.L1R.TabStop = false;
            this.L1R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnL1L
            // 
            this.btnL1L.Location = new System.Drawing.Point(218, 174);
            this.btnL1L.Name = "btnL1L";
            this.btnL1L.Size = new System.Drawing.Size(31, 29);
            this.btnL1L.TabIndex = 43;
            this.btnL1L.Tag = "1_2";
            this.btnL1L.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.L1P);
            this.groupBox3.Location = new System.Drawing.Point(384, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox3.Size = new System.Drawing.Size(120, 160);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Tag = "Mẫu Vân Tay";
            this.groupBox3.Text = "FP TYPE";
            // 
            // L1P
            // 
            this.L1P.Location = new System.Drawing.Point(3, 18);
            this.L1P.Name = "L1P";
            this.L1P.Size = new System.Drawing.Size(114, 139);
            this.L1P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L1P.TabIndex = 1;
            this.L1P.TabStop = false;
            // 
            // txtL1R
            // 
            this.txtL1R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL1R.Location = new System.Drawing.Point(259, 174);
            this.txtL1R.MaxLength = 2;
            this.txtL1R.Name = "txtL1R";
            this.txtL1R.Size = new System.Drawing.Size(82, 29);
            this.txtL1R.TabIndex = 42;
            this.txtL1R.Tag = "1_3";
            this.txtL1R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL1R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL1R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.L1L);
            this.groupBox4.Location = new System.Drawing.Point(132, 11);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox4.Size = new System.Drawing.Size(120, 160);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Tag = "Bên Trái";
            this.groupBox4.Text = "LEFT";
            // 
            // L1L
            // 
            this.L1L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L1L.Location = new System.Drawing.Point(1, 17);
            this.L1L.Name = "L1L";
            this.L1L.Size = new System.Drawing.Size(118, 142);
            this.L1L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L1L.TabIndex = 1;
            this.L1L.TabStop = false;
            this.L1L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtL1L
            // 
            this.txtL1L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL1L.Location = new System.Drawing.Point(132, 174);
            this.txtL1L.MaxLength = 2;
            this.txtL1L.Name = "txtL1L";
            this.txtL1L.Size = new System.Drawing.Size(82, 29);
            this.txtL1L.TabIndex = 41;
            this.txtL1L.Tag = "1_2";
            this.txtL1L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL1L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL1L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage2
            // 
            this.TabPage2.BackColor = System.Drawing.Color.White;
            this.TabPage2.Controls.Add(this.pnl2);
            this.TabPage2.Location = new System.Drawing.Point(4, 38);
            this.TabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Size = new System.Drawing.Size(508, 234);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Tag = "";
            this.TabPage2.Text = "L2";
            // 
            // pnl2
            // 
            this.pnl2.Controls.Add(this.lblL2P);
            this.pnl2.Controls.Add(this.groupBox8);
            this.pnl2.Controls.Add(this.txtL2P);
            this.pnl2.Controls.Add(this.groupBox9);
            this.pnl2.Controls.Add(this.btnL2R);
            this.pnl2.Controls.Add(this.btnL2L);
            this.pnl2.Controls.Add(this.groupBox10);
            this.pnl2.Controls.Add(this.txtL2R);
            this.pnl2.Controls.Add(this.groupBox11);
            this.pnl2.Controls.Add(this.txtL2L);
            this.pnl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl2.Location = new System.Drawing.Point(0, 0);
            this.pnl2.Name = "pnl2";
            this.pnl2.Size = new System.Drawing.Size(508, 234);
            this.pnl2.TabIndex = 46;
            this.pnl2.Tag = "2";
            // 
            // lblL2P
            // 
            this.lblL2P.AutoSize = true;
            this.lblL2P.Location = new System.Drawing.Point(438, 179);
            this.lblL2P.Name = "lblL2P";
            this.lblL2P.Size = new System.Drawing.Size(27, 15);
            this.lblL2P.TabIndex = 56;
            this.lblL2P.Text = "L2P";
            this.lblL2P.Visible = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.L2F);
            this.groupBox8.Location = new System.Drawing.Point(8, 11);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox8.Size = new System.Drawing.Size(120, 160);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Tag = "Ngón Trỏ";
            this.groupBox8.Text = "INDEX";
            // 
            // L2F
            // 
            this.L2F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L2F.Location = new System.Drawing.Point(1, 17);
            this.L2F.Name = "L2F";
            this.L2F.Size = new System.Drawing.Size(118, 142);
            this.L2F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L2F.TabIndex = 0;
            this.L2F.TabStop = false;
            this.L2F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtL2P
            // 
            this.txtL2P.Location = new System.Drawing.Point(385, 176);
            this.txtL2P.Name = "txtL2P";
            this.txtL2P.Size = new System.Drawing.Size(47, 23);
            this.txtL2P.TabIndex = 34;
            this.txtL2P.Visible = false;
            this.txtL2P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.L2R);
            this.groupBox9.Location = new System.Drawing.Point(259, 11);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox9.Size = new System.Drawing.Size(119, 160);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Tag = "Bên Phải";
            this.groupBox9.Text = "RIGHT";
            // 
            // L2R
            // 
            this.L2R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L2R.Location = new System.Drawing.Point(1, 17);
            this.L2R.Name = "L2R";
            this.L2R.Size = new System.Drawing.Size(117, 142);
            this.L2R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L2R.TabIndex = 1;
            this.L2R.TabStop = false;
            this.L2R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnL2R
            // 
            this.btnL2R.Location = new System.Drawing.Point(347, 174);
            this.btnL2R.Name = "btnL2R";
            this.btnL2R.Size = new System.Drawing.Size(31, 29);
            this.btnL2R.TabIndex = 38;
            this.btnL2R.Tag = "2_3";
            this.btnL2R.UseVisualStyleBackColor = true;
            // 
            // btnL2L
            // 
            this.btnL2L.Location = new System.Drawing.Point(219, 174);
            this.btnL2L.Name = "btnL2L";
            this.btnL2L.Size = new System.Drawing.Size(31, 29);
            this.btnL2L.TabIndex = 37;
            this.btnL2L.Tag = "2_2";
            this.btnL2L.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.L2P);
            this.groupBox10.Location = new System.Drawing.Point(384, 11);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox10.Size = new System.Drawing.Size(119, 160);
            this.groupBox10.TabIndex = 10;
            this.groupBox10.TabStop = false;
            this.groupBox10.Tag = "Mẫu Vân Tay";
            this.groupBox10.Text = "FP TYPE";
            // 
            // L2P
            // 
            this.L2P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L2P.Location = new System.Drawing.Point(1, 17);
            this.L2P.Name = "L2P";
            this.L2P.Size = new System.Drawing.Size(117, 142);
            this.L2P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L2P.TabIndex = 1;
            this.L2P.TabStop = false;
            // 
            // txtL2R
            // 
            this.txtL2R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL2R.Location = new System.Drawing.Point(259, 174);
            this.txtL2R.MaxLength = 2;
            this.txtL2R.Name = "txtL2R";
            this.txtL2R.Size = new System.Drawing.Size(80, 29);
            this.txtL2R.TabIndex = 42;
            this.txtL2R.Tag = "2_3";
            this.txtL2R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL2R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL2R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.L2L);
            this.groupBox11.Location = new System.Drawing.Point(134, 11);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox11.Size = new System.Drawing.Size(119, 160);
            this.groupBox11.TabIndex = 11;
            this.groupBox11.TabStop = false;
            this.groupBox11.Tag = "Bên Trái";
            this.groupBox11.Text = "LEFT";
            // 
            // L2L
            // 
            this.L2L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L2L.Location = new System.Drawing.Point(1, 17);
            this.L2L.Name = "L2L";
            this.L2L.Size = new System.Drawing.Size(117, 142);
            this.L2L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L2L.TabIndex = 1;
            this.L2L.TabStop = false;
            this.L2L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtL2L
            // 
            this.txtL2L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL2L.Location = new System.Drawing.Point(133, 174);
            this.txtL2L.MaxLength = 2;
            this.txtL2L.Name = "txtL2L";
            this.txtL2L.Size = new System.Drawing.Size(80, 29);
            this.txtL2L.TabIndex = 41;
            this.txtL2L.Tag = "2_2";
            this.txtL2L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL2L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL2L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage3
            // 
            this.TabPage3.BackColor = System.Drawing.Color.White;
            this.TabPage3.Controls.Add(this.pnl3);
            this.TabPage3.Location = new System.Drawing.Point(4, 38);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Size = new System.Drawing.Size(508, 234);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Tag = "";
            this.TabPage3.Text = "L3";
            // 
            // pnl3
            // 
            this.pnl3.Controls.Add(this.lblL3P);
            this.pnl3.Controls.Add(this.GroupBox15);
            this.pnl3.Controls.Add(this.btnL3R);
            this.pnl3.Controls.Add(this.txtL3P);
            this.pnl3.Controls.Add(this.GroupBox14);
            this.pnl3.Controls.Add(this.btnL3L);
            this.pnl3.Controls.Add(this.GroupBox13);
            this.pnl3.Controls.Add(this.txtL3R);
            this.pnl3.Controls.Add(this.GroupBox12);
            this.pnl3.Controls.Add(this.txtL3L);
            this.pnl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl3.Location = new System.Drawing.Point(0, 0);
            this.pnl3.Name = "pnl3";
            this.pnl3.Size = new System.Drawing.Size(508, 234);
            this.pnl3.TabIndex = 45;
            this.pnl3.Tag = "3";
            // 
            // lblL3P
            // 
            this.lblL3P.AutoSize = true;
            this.lblL3P.Location = new System.Drawing.Point(440, 183);
            this.lblL3P.Name = "lblL3P";
            this.lblL3P.Size = new System.Drawing.Size(27, 15);
            this.lblL3P.TabIndex = 45;
            this.lblL3P.Text = "L3P";
            this.lblL3P.Visible = false;
            // 
            // GroupBox15
            // 
            this.GroupBox15.Controls.Add(this.L3F);
            this.GroupBox15.Location = new System.Drawing.Point(5, 11);
            this.GroupBox15.Name = "GroupBox15";
            this.GroupBox15.Padding = new System.Windows.Forms.Padding(1);
            this.GroupBox15.Size = new System.Drawing.Size(120, 160);
            this.GroupBox15.TabIndex = 9;
            this.GroupBox15.TabStop = false;
            this.GroupBox15.Tag = "Ngón Giữa";
            this.GroupBox15.Text = "MIDDLE";
            // 
            // L3F
            // 
            this.L3F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L3F.Location = new System.Drawing.Point(1, 17);
            this.L3F.Name = "L3F";
            this.L3F.Size = new System.Drawing.Size(118, 142);
            this.L3F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L3F.TabIndex = 0;
            this.L3F.TabStop = false;
            this.L3F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // btnL3R
            // 
            this.btnL3R.Location = new System.Drawing.Point(349, 176);
            this.btnL3R.Name = "btnL3R";
            this.btnL3R.Size = new System.Drawing.Size(31, 29);
            this.btnL3R.TabIndex = 44;
            this.btnL3R.Tag = "3_3";
            this.btnL3R.UseVisualStyleBackColor = true;
            // 
            // txtL3P
            // 
            this.txtL3P.Location = new System.Drawing.Point(408, 180);
            this.txtL3P.Name = "txtL3P";
            this.txtL3P.Size = new System.Drawing.Size(26, 23);
            this.txtL3P.TabIndex = 34;
            this.txtL3P.Visible = false;
            this.txtL3P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // GroupBox14
            // 
            this.GroupBox14.Controls.Add(this.L3R);
            this.GroupBox14.Location = new System.Drawing.Point(257, 11);
            this.GroupBox14.Name = "GroupBox14";
            this.GroupBox14.Padding = new System.Windows.Forms.Padding(1);
            this.GroupBox14.Size = new System.Drawing.Size(120, 160);
            this.GroupBox14.TabIndex = 12;
            this.GroupBox14.TabStop = false;
            this.GroupBox14.Tag = "Bên Phải";
            this.GroupBox14.Text = "RIGHT";
            // 
            // L3R
            // 
            this.L3R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L3R.Location = new System.Drawing.Point(1, 17);
            this.L3R.Name = "L3R";
            this.L3R.Size = new System.Drawing.Size(118, 142);
            this.L3R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L3R.TabIndex = 1;
            this.L3R.TabStop = false;
            this.L3R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnL3L
            // 
            this.btnL3L.Location = new System.Drawing.Point(222, 176);
            this.btnL3L.Name = "btnL3L";
            this.btnL3L.Size = new System.Drawing.Size(31, 29);
            this.btnL3L.TabIndex = 43;
            this.btnL3L.Tag = "3_2";
            this.btnL3L.UseVisualStyleBackColor = true;
            // 
            // GroupBox13
            // 
            this.GroupBox13.Controls.Add(this.L3P);
            this.GroupBox13.Location = new System.Drawing.Point(383, 11);
            this.GroupBox13.Name = "GroupBox13";
            this.GroupBox13.Padding = new System.Windows.Forms.Padding(1);
            this.GroupBox13.Size = new System.Drawing.Size(120, 160);
            this.GroupBox13.TabIndex = 10;
            this.GroupBox13.TabStop = false;
            this.GroupBox13.Tag = "Mẫu Vân Tay";
            this.GroupBox13.Text = "FP TYPE";
            // 
            // L3P
            // 
            this.L3P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L3P.Location = new System.Drawing.Point(1, 17);
            this.L3P.Name = "L3P";
            this.L3P.Size = new System.Drawing.Size(118, 142);
            this.L3P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L3P.TabIndex = 1;
            this.L3P.TabStop = false;
            // 
            // txtL3R
            // 
            this.txtL3R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL3R.Location = new System.Drawing.Point(261, 176);
            this.txtL3R.MaxLength = 2;
            this.txtL3R.Name = "txtL3R";
            this.txtL3R.Size = new System.Drawing.Size(80, 29);
            this.txtL3R.TabIndex = 42;
            this.txtL3R.Tag = "3_3";
            this.txtL3R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL3R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL3R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // GroupBox12
            // 
            this.GroupBox12.Controls.Add(this.L3L);
            this.GroupBox12.Location = new System.Drawing.Point(130, 11);
            this.GroupBox12.Name = "GroupBox12";
            this.GroupBox12.Padding = new System.Windows.Forms.Padding(1);
            this.GroupBox12.Size = new System.Drawing.Size(120, 160);
            this.GroupBox12.TabIndex = 11;
            this.GroupBox12.TabStop = false;
            this.GroupBox12.Tag = "Bên Trái";
            this.GroupBox12.Text = "LEFT";
            // 
            // L3L
            // 
            this.L3L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L3L.Location = new System.Drawing.Point(1, 17);
            this.L3L.Name = "L3L";
            this.L3L.Size = new System.Drawing.Size(118, 142);
            this.L3L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L3L.TabIndex = 1;
            this.L3L.TabStop = false;
            this.L3L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtL3L
            // 
            this.txtL3L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL3L.Location = new System.Drawing.Point(133, 176);
            this.txtL3L.MaxLength = 2;
            this.txtL3L.Name = "txtL3L";
            this.txtL3L.Size = new System.Drawing.Size(80, 29);
            this.txtL3L.TabIndex = 41;
            this.txtL3L.Tag = "3_2";
            this.txtL3L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL3L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL3L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage4
            // 
            this.TabPage4.BackColor = System.Drawing.Color.White;
            this.TabPage4.Controls.Add(this.pnl4);
            this.TabPage4.Location = new System.Drawing.Point(4, 38);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Size = new System.Drawing.Size(508, 234);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Tag = "33";
            this.TabPage4.Text = "L4";
            // 
            // pnl4
            // 
            this.pnl4.Controls.Add(this.lblL4P);
            this.pnl4.Controls.Add(this.txtL4L);
            this.pnl4.Controls.Add(this.btnL4R);
            this.pnl4.Controls.Add(this.txtL4R);
            this.pnl4.Controls.Add(this.btnL4L);
            this.pnl4.Controls.Add(this.groupBox16);
            this.pnl4.Controls.Add(this.txtL4P);
            this.pnl4.Controls.Add(this.groupBox17);
            this.pnl4.Controls.Add(this.groupBox18);
            this.pnl4.Controls.Add(this.groupBox19);
            this.pnl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl4.Location = new System.Drawing.Point(0, 0);
            this.pnl4.Name = "pnl4";
            this.pnl4.Size = new System.Drawing.Size(508, 234);
            this.pnl4.TabIndex = 47;
            // 
            // lblL4P
            // 
            this.lblL4P.AutoSize = true;
            this.lblL4P.Location = new System.Drawing.Point(449, 180);
            this.lblL4P.Name = "lblL4P";
            this.lblL4P.Size = new System.Drawing.Size(27, 15);
            this.lblL4P.TabIndex = 49;
            this.lblL4P.Text = "L4P";
            this.lblL4P.Visible = false;
            // 
            // txtL4L
            // 
            this.txtL4L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL4L.Location = new System.Drawing.Point(133, 173);
            this.txtL4L.MaxLength = 2;
            this.txtL4L.Name = "txtL4L";
            this.txtL4L.Size = new System.Drawing.Size(80, 29);
            this.txtL4L.TabIndex = 45;
            this.txtL4L.Tag = "4_2";
            this.txtL4L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL4L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL4L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // btnL4R
            // 
            this.btnL4R.Location = new System.Drawing.Point(344, 174);
            this.btnL4R.Name = "btnL4R";
            this.btnL4R.Size = new System.Drawing.Size(31, 29);
            this.btnL4R.TabIndex = 48;
            this.btnL4R.Tag = "4_3";
            this.btnL4R.UseVisualStyleBackColor = true;
            // 
            // txtL4R
            // 
            this.txtL4R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL4R.Location = new System.Drawing.Point(258, 174);
            this.txtL4R.MaxLength = 2;
            this.txtL4R.Name = "txtL4R";
            this.txtL4R.Size = new System.Drawing.Size(80, 29);
            this.txtL4R.TabIndex = 46;
            this.txtL4R.Tag = "4_3";
            this.txtL4R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL4R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL4R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // btnL4L
            // 
            this.btnL4L.Location = new System.Drawing.Point(219, 174);
            this.btnL4L.Name = "btnL4L";
            this.btnL4L.Size = new System.Drawing.Size(31, 29);
            this.btnL4L.TabIndex = 47;
            this.btnL4L.Tag = "4_2";
            this.btnL4L.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.L4F);
            this.groupBox16.Location = new System.Drawing.Point(8, 11);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox16.Size = new System.Drawing.Size(120, 160);
            this.groupBox16.TabIndex = 9;
            this.groupBox16.TabStop = false;
            this.groupBox16.Tag = "Ngón Nhẫn";
            this.groupBox16.Text = "RING";
            // 
            // L4F
            // 
            this.L4F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L4F.Location = new System.Drawing.Point(1, 17);
            this.L4F.Name = "L4F";
            this.L4F.Size = new System.Drawing.Size(118, 142);
            this.L4F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L4F.TabIndex = 0;
            this.L4F.TabStop = false;
            this.L4F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtL4P
            // 
            this.txtL4P.Location = new System.Drawing.Point(414, 177);
            this.txtL4P.Name = "txtL4P";
            this.txtL4P.Size = new System.Drawing.Size(29, 23);
            this.txtL4P.TabIndex = 40;
            this.txtL4P.Visible = false;
            this.txtL4P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.L4R);
            this.groupBox17.Location = new System.Drawing.Point(258, 11);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox17.Size = new System.Drawing.Size(120, 160);
            this.groupBox17.TabIndex = 12;
            this.groupBox17.TabStop = false;
            this.groupBox17.Tag = "Bên Phải";
            this.groupBox17.Text = "RIGHT";
            // 
            // L4R
            // 
            this.L4R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L4R.Location = new System.Drawing.Point(1, 17);
            this.L4R.Name = "L4R";
            this.L4R.Size = new System.Drawing.Size(118, 142);
            this.L4R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L4R.TabIndex = 1;
            this.L4R.TabStop = false;
            this.L4R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.L4P);
            this.groupBox18.Location = new System.Drawing.Point(383, 11);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox18.Size = new System.Drawing.Size(120, 160);
            this.groupBox18.TabIndex = 10;
            this.groupBox18.TabStop = false;
            this.groupBox18.Tag = "Mẫu Vân Tay ";
            this.groupBox18.Text = "FP TYPE";
            // 
            // L4P
            // 
            this.L4P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L4P.Location = new System.Drawing.Point(1, 17);
            this.L4P.Name = "L4P";
            this.L4P.Size = new System.Drawing.Size(118, 142);
            this.L4P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L4P.TabIndex = 1;
            this.L4P.TabStop = false;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.L4L);
            this.groupBox19.Location = new System.Drawing.Point(133, 11);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox19.Size = new System.Drawing.Size(120, 160);
            this.groupBox19.TabIndex = 11;
            this.groupBox19.TabStop = false;
            this.groupBox19.Tag = "Bên Trái";
            this.groupBox19.Text = "LEFT";
            // 
            // L4L
            // 
            this.L4L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L4L.Location = new System.Drawing.Point(1, 17);
            this.L4L.Name = "L4L";
            this.L4L.Size = new System.Drawing.Size(118, 142);
            this.L4L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L4L.TabIndex = 1;
            this.L4L.TabStop = false;
            this.L4L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // TabPage5
            // 
            this.TabPage5.BackColor = System.Drawing.Color.White;
            this.TabPage5.Controls.Add(this.pnl5);
            this.TabPage5.Location = new System.Drawing.Point(4, 38);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Size = new System.Drawing.Size(508, 234);
            this.TabPage5.TabIndex = 4;
            this.TabPage5.Tag = "";
            this.TabPage5.Text = "L5";
            // 
            // pnl5
            // 
            this.pnl5.Controls.Add(this.lblL5P);
            this.pnl5.Controls.Add(this.groupBox20);
            this.pnl5.Controls.Add(this.txtL5P);
            this.pnl5.Controls.Add(this.btnL5R);
            this.pnl5.Controls.Add(this.groupBox21);
            this.pnl5.Controls.Add(this.btnL5L);
            this.pnl5.Controls.Add(this.groupBox22);
            this.pnl5.Controls.Add(this.txtL5R);
            this.pnl5.Controls.Add(this.groupBox23);
            this.pnl5.Controls.Add(this.txtL5L);
            this.pnl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl5.Location = new System.Drawing.Point(0, 0);
            this.pnl5.Name = "pnl5";
            this.pnl5.Size = new System.Drawing.Size(508, 234);
            this.pnl5.TabIndex = 46;
            // 
            // lblL5P
            // 
            this.lblL5P.AutoSize = true;
            this.lblL5P.Location = new System.Drawing.Point(449, 184);
            this.lblL5P.Name = "lblL5P";
            this.lblL5P.Size = new System.Drawing.Size(27, 15);
            this.lblL5P.TabIndex = 50;
            this.lblL5P.Text = "L5P";
            this.lblL5P.Visible = false;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.L5F);
            this.groupBox20.Location = new System.Drawing.Point(5, 11);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox20.Size = new System.Drawing.Size(120, 160);
            this.groupBox20.TabIndex = 9;
            this.groupBox20.TabStop = false;
            this.groupBox20.Tag = "Ngón Út";
            this.groupBox20.Text = "LITTLE";
            // 
            // L5F
            // 
            this.L5F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L5F.Location = new System.Drawing.Point(1, 17);
            this.L5F.Name = "L5F";
            this.L5F.Size = new System.Drawing.Size(118, 142);
            this.L5F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L5F.TabIndex = 0;
            this.L5F.TabStop = false;
            this.L5F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtL5P
            // 
            this.txtL5P.Location = new System.Drawing.Point(414, 180);
            this.txtL5P.Name = "txtL5P";
            this.txtL5P.Size = new System.Drawing.Size(29, 23);
            this.txtL5P.TabIndex = 40;
            this.txtL5P.Visible = false;
            this.txtL5P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnL5R
            // 
            this.btnL5R.Location = new System.Drawing.Point(343, 175);
            this.btnL5R.Name = "btnL5R";
            this.btnL5R.Size = new System.Drawing.Size(31, 29);
            this.btnL5R.TabIndex = 44;
            this.btnL5R.Tag = "5_3";
            this.btnL5R.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.L5R);
            this.groupBox21.Location = new System.Drawing.Point(257, 11);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox21.Size = new System.Drawing.Size(120, 160);
            this.groupBox21.TabIndex = 12;
            this.groupBox21.TabStop = false;
            this.groupBox21.Tag = "Bên Phải";
            this.groupBox21.Text = "RIGHT";
            // 
            // L5R
            // 
            this.L5R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L5R.Location = new System.Drawing.Point(1, 17);
            this.L5R.Name = "L5R";
            this.L5R.Size = new System.Drawing.Size(118, 142);
            this.L5R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L5R.TabIndex = 1;
            this.L5R.TabStop = false;
            this.L5R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnL5L
            // 
            this.btnL5L.Location = new System.Drawing.Point(217, 175);
            this.btnL5L.Name = "btnL5L";
            this.btnL5L.Size = new System.Drawing.Size(31, 29);
            this.btnL5L.TabIndex = 43;
            this.btnL5L.Tag = "5_2";
            this.btnL5L.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.L5P);
            this.groupBox22.Location = new System.Drawing.Point(383, 11);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox22.Size = new System.Drawing.Size(120, 160);
            this.groupBox22.TabIndex = 10;
            this.groupBox22.TabStop = false;
            this.groupBox22.Tag = "Mẫu Vân Tay ";
            this.groupBox22.Text = "FP TYPE";
            // 
            // L5P
            // 
            this.L5P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L5P.Location = new System.Drawing.Point(1, 17);
            this.L5P.Name = "L5P";
            this.L5P.Size = new System.Drawing.Size(118, 142);
            this.L5P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L5P.TabIndex = 1;
            this.L5P.TabStop = false;
            // 
            // txtL5R
            // 
            this.txtL5R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL5R.Location = new System.Drawing.Point(257, 175);
            this.txtL5R.MaxLength = 2;
            this.txtL5R.Name = "txtL5R";
            this.txtL5R.Size = new System.Drawing.Size(80, 29);
            this.txtL5R.TabIndex = 42;
            this.txtL5R.Tag = "5_3";
            this.txtL5R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL5R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL5R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.L5L);
            this.groupBox23.Location = new System.Drawing.Point(131, 11);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox23.Size = new System.Drawing.Size(120, 160);
            this.groupBox23.TabIndex = 11;
            this.groupBox23.TabStop = false;
            this.groupBox23.Tag = "Bên Trái";
            this.groupBox23.Text = "LEFT";
            // 
            // L5L
            // 
            this.L5L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.L5L.Location = new System.Drawing.Point(1, 17);
            this.L5L.Name = "L5L";
            this.L5L.Size = new System.Drawing.Size(118, 142);
            this.L5L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.L5L.TabIndex = 1;
            this.L5L.TabStop = false;
            this.L5L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtL5L
            // 
            this.txtL5L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL5L.Location = new System.Drawing.Point(131, 175);
            this.txtL5L.MaxLength = 2;
            this.txtL5L.Name = "txtL5L";
            this.txtL5L.Size = new System.Drawing.Size(80, 29);
            this.txtL5L.TabIndex = 41;
            this.txtL5L.Tag = "5_2";
            this.txtL5L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtL5L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtL5L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage6
            // 
            this.TabPage6.BackColor = System.Drawing.Color.White;
            this.TabPage6.Controls.Add(this.pnl6);
            this.TabPage6.Location = new System.Drawing.Point(4, 38);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Size = new System.Drawing.Size(508, 234);
            this.TabPage6.TabIndex = 5;
            this.TabPage6.Tag = "";
            this.TabPage6.Text = "R1";
            // 
            // pnl6
            // 
            this.pnl6.Controls.Add(this.lblR1P);
            this.pnl6.Controls.Add(this.groupBox24);
            this.pnl6.Controls.Add(this.txtR1P);
            this.pnl6.Controls.Add(this.btnR1R);
            this.pnl6.Controls.Add(this.groupBox25);
            this.pnl6.Controls.Add(this.btnR1L);
            this.pnl6.Controls.Add(this.groupBox26);
            this.pnl6.Controls.Add(this.txtR1R);
            this.pnl6.Controls.Add(this.groupBox27);
            this.pnl6.Controls.Add(this.txtR1L);
            this.pnl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl6.Location = new System.Drawing.Point(0, 0);
            this.pnl6.Name = "pnl6";
            this.pnl6.Size = new System.Drawing.Size(508, 234);
            this.pnl6.TabIndex = 47;
            // 
            // lblR1P
            // 
            this.lblR1P.AutoSize = true;
            this.lblR1P.Location = new System.Drawing.Point(443, 182);
            this.lblR1P.Name = "lblR1P";
            this.lblR1P.Size = new System.Drawing.Size(26, 15);
            this.lblR1P.TabIndex = 51;
            this.lblR1P.Text = "R1P";
            this.lblR1P.Visible = false;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.R1F);
            this.groupBox24.Location = new System.Drawing.Point(4, 10);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox24.Size = new System.Drawing.Size(120, 160);
            this.groupBox24.TabIndex = 9;
            this.groupBox24.TabStop = false;
            this.groupBox24.Tag = "Ngón Cái";
            this.groupBox24.Text = "THUMP";
            // 
            // R1F
            // 
            this.R1F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R1F.Location = new System.Drawing.Point(1, 17);
            this.R1F.Name = "R1F";
            this.R1F.Size = new System.Drawing.Size(118, 142);
            this.R1F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R1F.TabIndex = 0;
            this.R1F.TabStop = false;
            this.R1F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtR1P
            // 
            this.txtR1P.Location = new System.Drawing.Point(405, 179);
            this.txtR1P.Name = "txtR1P";
            this.txtR1P.Size = new System.Drawing.Size(32, 23);
            this.txtR1P.TabIndex = 43;
            this.txtR1P.Visible = false;
            this.txtR1P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnR1R
            // 
            this.btnR1R.Location = new System.Drawing.Point(342, 173);
            this.btnR1R.Name = "btnR1R";
            this.btnR1R.Size = new System.Drawing.Size(31, 29);
            this.btnR1R.TabIndex = 44;
            this.btnR1R.Tag = "6_3";
            this.btnR1R.UseVisualStyleBackColor = true;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.R1R);
            this.groupBox25.Location = new System.Drawing.Point(256, 10);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox25.Size = new System.Drawing.Size(120, 160);
            this.groupBox25.TabIndex = 12;
            this.groupBox25.TabStop = false;
            this.groupBox25.Tag = "Bên Phải";
            this.groupBox25.Text = "RIGHT";
            // 
            // R1R
            // 
            this.R1R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R1R.Location = new System.Drawing.Point(1, 17);
            this.R1R.Name = "R1R";
            this.R1R.Size = new System.Drawing.Size(118, 142);
            this.R1R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R1R.TabIndex = 1;
            this.R1R.TabStop = false;
            this.R1R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnR1L
            // 
            this.btnR1L.Location = new System.Drawing.Point(216, 173);
            this.btnR1L.Name = "btnR1L";
            this.btnR1L.Size = new System.Drawing.Size(31, 29);
            this.btnR1L.TabIndex = 43;
            this.btnR1L.Tag = "6_2";
            this.btnR1L.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.R1P);
            this.groupBox26.Location = new System.Drawing.Point(382, 10);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox26.Size = new System.Drawing.Size(120, 160);
            this.groupBox26.TabIndex = 10;
            this.groupBox26.TabStop = false;
            this.groupBox26.Tag = "Mẫu Vân Tay ";
            this.groupBox26.Text = "FP TYPE";
            // 
            // R1P
            // 
            this.R1P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R1P.Location = new System.Drawing.Point(1, 17);
            this.R1P.Name = "R1P";
            this.R1P.Size = new System.Drawing.Size(118, 142);
            this.R1P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R1P.TabIndex = 1;
            this.R1P.TabStop = false;
            // 
            // txtR1R
            // 
            this.txtR1R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR1R.Location = new System.Drawing.Point(256, 173);
            this.txtR1R.MaxLength = 2;
            this.txtR1R.Name = "txtR1R";
            this.txtR1R.Size = new System.Drawing.Size(80, 29);
            this.txtR1R.TabIndex = 42;
            this.txtR1R.Tag = "6_3";
            this.txtR1R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR1R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR1R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.R1L);
            this.groupBox27.Location = new System.Drawing.Point(130, 10);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox27.Size = new System.Drawing.Size(120, 160);
            this.groupBox27.TabIndex = 11;
            this.groupBox27.TabStop = false;
            this.groupBox27.Tag = "Bên Trái";
            this.groupBox27.Text = "LEFT";
            // 
            // R1L
            // 
            this.R1L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R1L.Location = new System.Drawing.Point(1, 17);
            this.R1L.Name = "R1L";
            this.R1L.Size = new System.Drawing.Size(118, 142);
            this.R1L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R1L.TabIndex = 1;
            this.R1L.TabStop = false;
            this.R1L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtR1L
            // 
            this.txtR1L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR1L.Location = new System.Drawing.Point(130, 173);
            this.txtR1L.MaxLength = 2;
            this.txtR1L.Name = "txtR1L";
            this.txtR1L.Size = new System.Drawing.Size(80, 29);
            this.txtR1L.TabIndex = 41;
            this.txtR1L.Tag = "6_2";
            this.txtR1L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR1L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR1L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage7
            // 
            this.TabPage7.BackColor = System.Drawing.Color.White;
            this.TabPage7.Controls.Add(this.pnl7);
            this.TabPage7.Location = new System.Drawing.Point(4, 38);
            this.TabPage7.Name = "TabPage7";
            this.TabPage7.Size = new System.Drawing.Size(508, 234);
            this.TabPage7.TabIndex = 6;
            this.TabPage7.Tag = "";
            this.TabPage7.Text = "R2";
            // 
            // pnl7
            // 
            this.pnl7.Controls.Add(this.lblR2P);
            this.pnl7.Controls.Add(this.groupBox28);
            this.pnl7.Controls.Add(this.txtR2P);
            this.pnl7.Controls.Add(this.btnR2R);
            this.pnl7.Controls.Add(this.groupBox29);
            this.pnl7.Controls.Add(this.btnR2L);
            this.pnl7.Controls.Add(this.groupBox30);
            this.pnl7.Controls.Add(this.txtR2R);
            this.pnl7.Controls.Add(this.txtR2L);
            this.pnl7.Controls.Add(this.groupBox31);
            this.pnl7.Controls.Add(this.groupBox7);
            this.pnl7.Controls.Add(this.groupBox6);
            this.pnl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl7.Location = new System.Drawing.Point(0, 0);
            this.pnl7.Name = "pnl7";
            this.pnl7.Size = new System.Drawing.Size(508, 234);
            this.pnl7.TabIndex = 48;
            this.pnl7.Tag = "7_2";
            // 
            // lblR2P
            // 
            this.lblR2P.AutoSize = true;
            this.lblR2P.Location = new System.Drawing.Point(451, 182);
            this.lblR2P.Name = "lblR2P";
            this.lblR2P.Size = new System.Drawing.Size(28, 15);
            this.lblR2P.TabIndex = 52;
            this.lblR2P.Text = "R2P";
            this.lblR2P.Visible = false;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.R2F);
            this.groupBox28.Location = new System.Drawing.Point(6, 11);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox28.Size = new System.Drawing.Size(120, 160);
            this.groupBox28.TabIndex = 9;
            this.groupBox28.TabStop = false;
            this.groupBox28.Tag = "Ngón Trỏ";
            this.groupBox28.Text = "INDEX";
            // 
            // R2F
            // 
            this.R2F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R2F.Location = new System.Drawing.Point(1, 17);
            this.R2F.Name = "R2F";
            this.R2F.Size = new System.Drawing.Size(118, 142);
            this.R2F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R2F.TabIndex = 0;
            this.R2F.TabStop = false;
            this.R2F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtR2P
            // 
            this.txtR2P.Location = new System.Drawing.Point(416, 178);
            this.txtR2P.Name = "txtR2P";
            this.txtR2P.Size = new System.Drawing.Size(29, 23);
            this.txtR2P.TabIndex = 43;
            this.txtR2P.Visible = false;
            this.txtR2P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnR2R
            // 
            this.btnR2R.Location = new System.Drawing.Point(344, 175);
            this.btnR2R.Name = "btnR2R";
            this.btnR2R.Size = new System.Drawing.Size(31, 29);
            this.btnR2R.TabIndex = 44;
            this.btnR2R.Tag = "7_3";
            this.btnR2R.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.R2R);
            this.groupBox29.Location = new System.Drawing.Point(258, 11);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox29.Size = new System.Drawing.Size(120, 160);
            this.groupBox29.TabIndex = 12;
            this.groupBox29.TabStop = false;
            this.groupBox29.Tag = "Bên Phải";
            this.groupBox29.Text = "RIGHT";
            // 
            // R2R
            // 
            this.R2R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R2R.Location = new System.Drawing.Point(1, 17);
            this.R2R.Name = "R2R";
            this.R2R.Size = new System.Drawing.Size(118, 142);
            this.R2R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R2R.TabIndex = 1;
            this.R2R.TabStop = false;
            // 
            // btnR2L
            // 
            this.btnR2L.Location = new System.Drawing.Point(218, 175);
            this.btnR2L.Name = "btnR2L";
            this.btnR2L.Size = new System.Drawing.Size(31, 29);
            this.btnR2L.TabIndex = 43;
            this.btnR2L.Tag = "7_2";
            this.btnR2L.UseVisualStyleBackColor = true;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.R2P);
            this.groupBox30.Location = new System.Drawing.Point(384, 11);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox30.Size = new System.Drawing.Size(120, 160);
            this.groupBox30.TabIndex = 10;
            this.groupBox30.TabStop = false;
            this.groupBox30.Tag = "Mẫu Vân Tay";
            this.groupBox30.Text = "FP TYPE";
            // 
            // R2P
            // 
            this.R2P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R2P.Location = new System.Drawing.Point(1, 17);
            this.R2P.Name = "R2P";
            this.R2P.Size = new System.Drawing.Size(118, 142);
            this.R2P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R2P.TabIndex = 1;
            this.R2P.TabStop = false;
            // 
            // txtR2R
            // 
            this.txtR2R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR2R.Location = new System.Drawing.Point(259, 175);
            this.txtR2R.MaxLength = 2;
            this.txtR2R.Name = "txtR2R";
            this.txtR2R.Size = new System.Drawing.Size(79, 29);
            this.txtR2R.TabIndex = 42;
            this.txtR2R.Tag = "7_3";
            this.txtR2R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR2R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR2R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // txtR2L
            // 
            this.txtR2L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR2L.Location = new System.Drawing.Point(132, 175);
            this.txtR2L.MaxLength = 2;
            this.txtR2L.Name = "txtR2L";
            this.txtR2L.Size = new System.Drawing.Size(80, 29);
            this.txtR2L.TabIndex = 41;
            this.txtR2L.Tag = "7_2";
            this.txtR2L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR2L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR2L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.R2L);
            this.groupBox31.Location = new System.Drawing.Point(132, 11);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox31.Size = new System.Drawing.Size(120, 160);
            this.groupBox31.TabIndex = 11;
            this.groupBox31.TabStop = false;
            this.groupBox31.Tag = "Bên Trái";
            this.groupBox31.Text = "LEFT";
            // 
            // R2L
            // 
            this.R2L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R2L.Location = new System.Drawing.Point(1, 17);
            this.R2L.Name = "R2L";
            this.R2L.Size = new System.Drawing.Size(118, 142);
            this.R2L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R2L.TabIndex = 1;
            this.R2L.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Location = new System.Drawing.Point(258, 11);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox7.Size = new System.Drawing.Size(120, 160);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Bên Phải";
            // 
            // groupBox6
            // 
            this.groupBox6.Location = new System.Drawing.Point(132, 11);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox6.Size = new System.Drawing.Size(120, 160);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Bên Trái";
            // 
            // TabPage8
            // 
            this.TabPage8.BackColor = System.Drawing.Color.White;
            this.TabPage8.Controls.Add(this.pnl8);
            this.TabPage8.Location = new System.Drawing.Point(4, 38);
            this.TabPage8.Name = "TabPage8";
            this.TabPage8.Size = new System.Drawing.Size(508, 234);
            this.TabPage8.TabIndex = 7;
            this.TabPage8.Tag = "";
            this.TabPage8.Text = "R3";
            // 
            // pnl8
            // 
            this.pnl8.Controls.Add(this.lblR3P);
            this.pnl8.Controls.Add(this.groupBox32);
            this.pnl8.Controls.Add(this.txtR3P);
            this.pnl8.Controls.Add(this.btnR3R);
            this.pnl8.Controls.Add(this.groupBox33);
            this.pnl8.Controls.Add(this.btnR3L);
            this.pnl8.Controls.Add(this.groupBox34);
            this.pnl8.Controls.Add(this.txtR3R);
            this.pnl8.Controls.Add(this.groupBox35);
            this.pnl8.Controls.Add(this.txtR3L);
            this.pnl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl8.Location = new System.Drawing.Point(0, 0);
            this.pnl8.Name = "pnl8";
            this.pnl8.Size = new System.Drawing.Size(508, 234);
            this.pnl8.TabIndex = 49;
            // 
            // lblR3P
            // 
            this.lblR3P.AutoSize = true;
            this.lblR3P.Location = new System.Drawing.Point(450, 184);
            this.lblR3P.Name = "lblR3P";
            this.lblR3P.Size = new System.Drawing.Size(28, 15);
            this.lblR3P.TabIndex = 53;
            this.lblR3P.Text = "R3P";
            this.lblR3P.Visible = false;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.R3F);
            this.groupBox32.Location = new System.Drawing.Point(6, 11);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox32.Size = new System.Drawing.Size(120, 160);
            this.groupBox32.TabIndex = 9;
            this.groupBox32.TabStop = false;
            this.groupBox32.Tag = "Ngón Giữa";
            this.groupBox32.Text = "MIDDLE";
            // 
            // R3F
            // 
            this.R3F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R3F.Location = new System.Drawing.Point(1, 17);
            this.R3F.Name = "R3F";
            this.R3F.Size = new System.Drawing.Size(118, 142);
            this.R3F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R3F.TabIndex = 0;
            this.R3F.TabStop = false;
            this.R3F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtR3P
            // 
            this.txtR3P.Location = new System.Drawing.Point(415, 181);
            this.txtR3P.Name = "txtR3P";
            this.txtR3P.Size = new System.Drawing.Size(29, 23);
            this.txtR3P.TabIndex = 43;
            this.txtR3P.Visible = false;
            this.txtR3P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnR3R
            // 
            this.btnR3R.Location = new System.Drawing.Point(345, 178);
            this.btnR3R.Name = "btnR3R";
            this.btnR3R.Size = new System.Drawing.Size(31, 29);
            this.btnR3R.TabIndex = 44;
            this.btnR3R.Tag = "8_3";
            this.btnR3R.UseVisualStyleBackColor = true;
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this.R3R);
            this.groupBox33.Location = new System.Drawing.Point(258, 13);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox33.Size = new System.Drawing.Size(120, 160);
            this.groupBox33.TabIndex = 12;
            this.groupBox33.TabStop = false;
            this.groupBox33.Tag = "Bên Phải";
            this.groupBox33.Text = "RIGHT";
            // 
            // R3R
            // 
            this.R3R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R3R.Location = new System.Drawing.Point(1, 17);
            this.R3R.Name = "R3R";
            this.R3R.Size = new System.Drawing.Size(118, 142);
            this.R3R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R3R.TabIndex = 1;
            this.R3R.TabStop = false;
            this.R3R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnR3L
            // 
            this.btnR3L.Location = new System.Drawing.Point(218, 177);
            this.btnR3L.Name = "btnR3L";
            this.btnR3L.Size = new System.Drawing.Size(31, 29);
            this.btnR3L.TabIndex = 43;
            this.btnR3L.Tag = "8_2";
            this.btnR3L.UseVisualStyleBackColor = true;
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this.R3P);
            this.groupBox34.Location = new System.Drawing.Point(384, 13);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox34.Size = new System.Drawing.Size(120, 160);
            this.groupBox34.TabIndex = 10;
            this.groupBox34.TabStop = false;
            this.groupBox34.Tag = "Vân Tay Mẫu";
            this.groupBox34.Text = "FP TYPE";
            // 
            // R3P
            // 
            this.R3P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R3P.Location = new System.Drawing.Point(1, 17);
            this.R3P.Name = "R3P";
            this.R3P.Size = new System.Drawing.Size(118, 142);
            this.R3P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R3P.TabIndex = 1;
            this.R3P.TabStop = false;
            // 
            // txtR3R
            // 
            this.txtR3R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR3R.Location = new System.Drawing.Point(259, 178);
            this.txtR3R.MaxLength = 2;
            this.txtR3R.Name = "txtR3R";
            this.txtR3R.Size = new System.Drawing.Size(80, 29);
            this.txtR3R.TabIndex = 42;
            this.txtR3R.Tag = "8_3";
            this.txtR3R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR3R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR3R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.R3L);
            this.groupBox35.Location = new System.Drawing.Point(132, 11);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox35.Size = new System.Drawing.Size(120, 160);
            this.groupBox35.TabIndex = 11;
            this.groupBox35.TabStop = false;
            this.groupBox35.Tag = "Bên Trái";
            this.groupBox35.Text = "LEFT";
            // 
            // R3L
            // 
            this.R3L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R3L.Location = new System.Drawing.Point(1, 17);
            this.R3L.Name = "R3L";
            this.R3L.Size = new System.Drawing.Size(118, 142);
            this.R3L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R3L.TabIndex = 1;
            this.R3L.TabStop = false;
            this.R3L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtR3L
            // 
            this.txtR3L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR3L.Location = new System.Drawing.Point(132, 176);
            this.txtR3L.MaxLength = 2;
            this.txtR3L.Name = "txtR3L";
            this.txtR3L.Size = new System.Drawing.Size(80, 29);
            this.txtR3L.TabIndex = 41;
            this.txtR3L.Tag = "8_2";
            this.txtR3L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR3L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR3L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage9
            // 
            this.TabPage9.BackColor = System.Drawing.Color.White;
            this.TabPage9.Controls.Add(this.pnl9);
            this.TabPage9.Location = new System.Drawing.Point(4, 38);
            this.TabPage9.Name = "TabPage9";
            this.TabPage9.Size = new System.Drawing.Size(508, 234);
            this.TabPage9.TabIndex = 8;
            this.TabPage9.Tag = "";
            this.TabPage9.Text = "R4";
            // 
            // pnl9
            // 
            this.pnl9.Controls.Add(this.lblR4P);
            this.pnl9.Controls.Add(this.groupBox36);
            this.pnl9.Controls.Add(this.txtR4P);
            this.pnl9.Controls.Add(this.btnR4R);
            this.pnl9.Controls.Add(this.groupBox37);
            this.pnl9.Controls.Add(this.btnR4L);
            this.pnl9.Controls.Add(this.groupBox38);
            this.pnl9.Controls.Add(this.txtR4R);
            this.pnl9.Controls.Add(this.groupBox39);
            this.pnl9.Controls.Add(this.txtR4L);
            this.pnl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl9.Location = new System.Drawing.Point(0, 0);
            this.pnl9.Name = "pnl9";
            this.pnl9.Size = new System.Drawing.Size(508, 234);
            this.pnl9.TabIndex = 52;
            // 
            // lblR4P
            // 
            this.lblR4P.AutoSize = true;
            this.lblR4P.Location = new System.Drawing.Point(453, 179);
            this.lblR4P.Name = "lblR4P";
            this.lblR4P.Size = new System.Drawing.Size(28, 15);
            this.lblR4P.TabIndex = 54;
            this.lblR4P.Text = "R4P";
            this.lblR4P.Visible = false;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this.R4F);
            this.groupBox36.Location = new System.Drawing.Point(6, 11);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox36.Size = new System.Drawing.Size(120, 160);
            this.groupBox36.TabIndex = 9;
            this.groupBox36.TabStop = false;
            this.groupBox36.Tag = "Ngón Nhẫn";
            this.groupBox36.Text = "RING";
            // 
            // R4F
            // 
            this.R4F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R4F.Location = new System.Drawing.Point(1, 17);
            this.R4F.Name = "R4F";
            this.R4F.Size = new System.Drawing.Size(118, 142);
            this.R4F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R4F.TabIndex = 0;
            this.R4F.TabStop = false;
            this.R4F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtR4P
            // 
            this.txtR4P.Location = new System.Drawing.Point(418, 176);
            this.txtR4P.Name = "txtR4P";
            this.txtR4P.Size = new System.Drawing.Size(29, 23);
            this.txtR4P.TabIndex = 43;
            this.txtR4P.Visible = false;
            this.txtR4P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnR4R
            // 
            this.btnR4R.Location = new System.Drawing.Point(344, 175);
            this.btnR4R.Name = "btnR4R";
            this.btnR4R.Size = new System.Drawing.Size(31, 29);
            this.btnR4R.TabIndex = 44;
            this.btnR4R.Tag = "9_3";
            this.btnR4R.UseVisualStyleBackColor = true;
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this.R4R);
            this.groupBox37.Location = new System.Drawing.Point(258, 11);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox37.Size = new System.Drawing.Size(120, 160);
            this.groupBox37.TabIndex = 12;
            this.groupBox37.TabStop = false;
            this.groupBox37.Tag = "Bên Phải";
            this.groupBox37.Text = "RIGHT";
            // 
            // R4R
            // 
            this.R4R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R4R.Location = new System.Drawing.Point(1, 17);
            this.R4R.Name = "R4R";
            this.R4R.Size = new System.Drawing.Size(118, 142);
            this.R4R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R4R.TabIndex = 1;
            this.R4R.TabStop = false;
            this.R4R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnR4L
            // 
            this.btnR4L.Location = new System.Drawing.Point(218, 175);
            this.btnR4L.Name = "btnR4L";
            this.btnR4L.Size = new System.Drawing.Size(31, 29);
            this.btnR4L.TabIndex = 43;
            this.btnR4L.Tag = "9_2";
            this.btnR4L.UseVisualStyleBackColor = true;
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.R4P);
            this.groupBox38.Location = new System.Drawing.Point(384, 11);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox38.Size = new System.Drawing.Size(120, 160);
            this.groupBox38.TabIndex = 10;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "FP TYPE";
            // 
            // R4P
            // 
            this.R4P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R4P.Location = new System.Drawing.Point(1, 17);
            this.R4P.Name = "R4P";
            this.R4P.Size = new System.Drawing.Size(118, 142);
            this.R4P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R4P.TabIndex = 1;
            this.R4P.TabStop = false;
            // 
            // txtR4R
            // 
            this.txtR4R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR4R.Location = new System.Drawing.Point(258, 176);
            this.txtR4R.MaxLength = 2;
            this.txtR4R.Name = "txtR4R";
            this.txtR4R.Size = new System.Drawing.Size(80, 29);
            this.txtR4R.TabIndex = 42;
            this.txtR4R.Tag = "9_3";
            this.txtR4R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR4R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR4R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this.R4L);
            this.groupBox39.Location = new System.Drawing.Point(132, 11);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox39.Size = new System.Drawing.Size(120, 160);
            this.groupBox39.TabIndex = 11;
            this.groupBox39.TabStop = false;
            this.groupBox39.Tag = "Bên Trái";
            this.groupBox39.Text = "LEFT";
            // 
            // R4L
            // 
            this.R4L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R4L.Location = new System.Drawing.Point(1, 17);
            this.R4L.Name = "R4L";
            this.R4L.Size = new System.Drawing.Size(118, 142);
            this.R4L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R4L.TabIndex = 1;
            this.R4L.TabStop = false;
            this.R4L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtR4L
            // 
            this.txtR4L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR4L.Location = new System.Drawing.Point(132, 175);
            this.txtR4L.MaxLength = 2;
            this.txtR4L.Name = "txtR4L";
            this.txtR4L.Size = new System.Drawing.Size(80, 29);
            this.txtR4L.TabIndex = 41;
            this.txtR4L.Tag = "9_2";
            this.txtR4L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR4L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR4L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // TabPage10
            // 
            this.TabPage10.BackColor = System.Drawing.Color.White;
            this.TabPage10.Controls.Add(this.pnl10);
            this.TabPage10.Location = new System.Drawing.Point(4, 38);
            this.TabPage10.Name = "TabPage10";
            this.TabPage10.Size = new System.Drawing.Size(508, 234);
            this.TabPage10.TabIndex = 9;
            this.TabPage10.Text = "R5";
            // 
            // pnl10
            // 
            this.pnl10.Controls.Add(this.lblR5P);
            this.pnl10.Controls.Add(this.groupBox40);
            this.pnl10.Controls.Add(this.txtR5P);
            this.pnl10.Controls.Add(this.btnR5R);
            this.pnl10.Controls.Add(this.groupBox41);
            this.pnl10.Controls.Add(this.btnR5L);
            this.pnl10.Controls.Add(this.groupBox42);
            this.pnl10.Controls.Add(this.txtR5R);
            this.pnl10.Controls.Add(this.groupBox43);
            this.pnl10.Controls.Add(this.txtR5L);
            this.pnl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl10.Location = new System.Drawing.Point(0, 0);
            this.pnl10.Name = "pnl10";
            this.pnl10.Size = new System.Drawing.Size(508, 234);
            this.pnl10.TabIndex = 55;
            // 
            // lblR5P
            // 
            this.lblR5P.AutoSize = true;
            this.lblR5P.Location = new System.Drawing.Point(454, 183);
            this.lblR5P.Name = "lblR5P";
            this.lblR5P.Size = new System.Drawing.Size(28, 15);
            this.lblR5P.TabIndex = 55;
            this.lblR5P.Text = "R5P";
            this.lblR5P.Visible = false;
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this.R5F);
            this.groupBox40.Location = new System.Drawing.Point(7, 11);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox40.Size = new System.Drawing.Size(120, 160);
            this.groupBox40.TabIndex = 9;
            this.groupBox40.TabStop = false;
            this.groupBox40.Tag = "Ngón Út";
            this.groupBox40.Text = "LITTLE";
            // 
            // R5F
            // 
            this.R5F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R5F.Location = new System.Drawing.Point(1, 17);
            this.R5F.Name = "R5F";
            this.R5F.Size = new System.Drawing.Size(118, 142);
            this.R5F.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R5F.TabIndex = 0;
            this.R5F.TabStop = false;
            this.R5F.DoubleClick += new System.EventHandler(this.ptbF_DoubleClick);
            // 
            // txtR5P
            // 
            this.txtR5P.Location = new System.Drawing.Point(419, 180);
            this.txtR5P.Name = "txtR5P";
            this.txtR5P.Size = new System.Drawing.Size(29, 23);
            this.txtR5P.TabIndex = 43;
            this.txtR5P.Visible = false;
            this.txtR5P.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // btnR5R
            // 
            this.btnR5R.Location = new System.Drawing.Point(344, 176);
            this.btnR5R.Name = "btnR5R";
            this.btnR5R.Size = new System.Drawing.Size(31, 29);
            this.btnR5R.TabIndex = 44;
            this.btnR5R.Tag = "10_3";
            this.btnR5R.UseVisualStyleBackColor = true;
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this.R5R);
            this.groupBox41.Location = new System.Drawing.Point(258, 11);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox41.Size = new System.Drawing.Size(120, 160);
            this.groupBox41.TabIndex = 12;
            this.groupBox41.TabStop = false;
            this.groupBox41.Tag = "Bên Phải";
            this.groupBox41.Text = "RIGHT";
            // 
            // R5R
            // 
            this.R5R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R5R.Location = new System.Drawing.Point(1, 17);
            this.R5R.Name = "R5R";
            this.R5R.Size = new System.Drawing.Size(118, 142);
            this.R5R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R5R.TabIndex = 1;
            this.R5R.TabStop = false;
            this.R5R.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // btnR5L
            // 
            this.btnR5L.Location = new System.Drawing.Point(219, 175);
            this.btnR5L.Name = "btnR5L";
            this.btnR5L.Size = new System.Drawing.Size(31, 29);
            this.btnR5L.TabIndex = 43;
            this.btnR5L.Tag = "10_2";
            this.btnR5L.UseVisualStyleBackColor = true;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.R5P);
            this.groupBox42.Location = new System.Drawing.Point(383, 11);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox42.Size = new System.Drawing.Size(120, 160);
            this.groupBox42.TabIndex = 10;
            this.groupBox42.TabStop = false;
            this.groupBox42.Tag = "Vân Tay Mẫu";
            this.groupBox42.Text = "FP TYPE";
            // 
            // R5P
            // 
            this.R5P.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R5P.Location = new System.Drawing.Point(1, 17);
            this.R5P.Name = "R5P";
            this.R5P.Size = new System.Drawing.Size(118, 142);
            this.R5P.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R5P.TabIndex = 1;
            this.R5P.TabStop = false;
            // 
            // txtR5R
            // 
            this.txtR5R.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5R.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR5R.Location = new System.Drawing.Point(258, 176);
            this.txtR5R.MaxLength = 2;
            this.txtR5R.Name = "txtR5R";
            this.txtR5R.Size = new System.Drawing.Size(80, 29);
            this.txtR5R.TabIndex = 42;
            this.txtR5R.Tag = "10_3";
            this.txtR5R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR5R.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR5R.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // groupBox43
            // 
            this.groupBox43.Controls.Add(this.R5L);
            this.groupBox43.Location = new System.Drawing.Point(133, 11);
            this.groupBox43.Name = "groupBox43";
            this.groupBox43.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox43.Size = new System.Drawing.Size(120, 160);
            this.groupBox43.TabIndex = 11;
            this.groupBox43.TabStop = false;
            this.groupBox43.Tag = "Bên Trái";
            this.groupBox43.Text = "LEFT";
            // 
            // R5L
            // 
            this.R5L.Dock = System.Windows.Forms.DockStyle.Fill;
            this.R5L.Location = new System.Drawing.Point(1, 17);
            this.R5L.Name = "R5L";
            this.R5L.Size = new System.Drawing.Size(118, 142);
            this.R5L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.R5L.TabIndex = 1;
            this.R5L.TabStop = false;
            this.R5L.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ptb_MouseDoubleClick);
            // 
            // txtR5L
            // 
            this.txtR5L.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5L.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR5L.Location = new System.Drawing.Point(133, 175);
            this.txtR5L.MaxLength = 2;
            this.txtR5L.Name = "txtR5L";
            this.txtR5L.Size = new System.Drawing.Size(80, 29);
            this.txtR5L.TabIndex = 41;
            this.txtR5L.Tag = "10_2";
            this.txtR5L.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtR5L.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtR5L.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.pbF);
            this.GroupBox5.Location = new System.Drawing.Point(134, 449);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Padding = new System.Windows.Forms.Padding(1);
            this.GroupBox5.Size = new System.Drawing.Size(135, 155);
            this.GroupBox5.TabIndex = 181;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Tag = "Gương Mặt";
            this.GroupBox5.Text = "Face";
            // 
            // pbF
            // 
            this.pbF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbF.Location = new System.Drawing.Point(1, 14);
            this.pbF.Name = "pbF";
            this.pbF.Size = new System.Drawing.Size(133, 140);
            this.pbF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbF.TabIndex = 0;
            this.pbF.TabStop = false;
            // 
            // rdFemale
            // 
            this.rdFemale.AutoSize = true;
            this.rdFemale.BackColor = System.Drawing.Color.Transparent;
            this.rdFemale.Enabled = false;
            this.rdFemale.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdFemale.ForeColor = System.Drawing.Color.Navy;
            this.rdFemale.Location = new System.Drawing.Point(195, 82);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(69, 21);
            this.rdFemale.TabIndex = 233;
            this.rdFemale.Tag = "Nữ";
            this.rdFemale.Text = "Female";
            this.rdFemale.UseVisualStyleBackColor = false;
            // 
            // rdMale
            // 
            this.rdMale.AutoSize = true;
            this.rdMale.BackColor = System.Drawing.Color.Transparent;
            this.rdMale.Checked = true;
            this.rdMale.Enabled = false;
            this.rdMale.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdMale.ForeColor = System.Drawing.Color.Navy;
            this.rdMale.Location = new System.Drawing.Point(126, 82);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(55, 21);
            this.rdMale.TabIndex = 232;
            this.rdMale.TabStop = true;
            this.rdMale.Tag = "Nam";
            this.rdMale.Text = "Male";
            this.rdMale.UseVisualStyleBackColor = false;
            // 
            // labReportId
            // 
            this.labReportId.AutoSize = true;
            this.labReportId.BackColor = System.Drawing.Color.Transparent;
            this.labReportId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labReportId.Location = new System.Drawing.Point(642, 601);
            this.labReportId.Name = "labReportId";
            this.labReportId.Size = new System.Drawing.Size(70, 21);
            this.labReportId.TabIndex = 231;
            this.labReportId.Tag = "";
            this.labReportId.Text = "ReportId";
            this.labReportId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labReportId.Visible = false;
            // 
            // labMa
            // 
            this.labMa.AutoSize = true;
            this.labMa.BackColor = System.Drawing.Color.Transparent;
            this.labMa.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMa.Location = new System.Drawing.Point(642, 639);
            this.labMa.Name = "labMa";
            this.labMa.Size = new System.Drawing.Size(25, 21);
            this.labMa.TabIndex = 230;
            this.labMa.Tag = "0";
            this.labMa.Text = "ID";
            this.labMa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labMa.Visible = false;
            // 
            // labName
            // 
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(17, 15);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(107, 21);
            this.labName.TabIndex = 219;
            this.labName.Tag = "Tên Khách Hàng :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labParent
            // 
            this.labParent.BackColor = System.Drawing.Color.Transparent;
            this.labParent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labParent.Location = new System.Drawing.Point(25, 50);
            this.labParent.Name = "labParent";
            this.labParent.Size = new System.Drawing.Size(98, 21);
            this.labParent.TabIndex = 225;
            this.labParent.Tag = "Tên Cha/Mẹ:";
            this.labParent.Text = "Parent :";
            this.labParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Enabled = false;
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(125, 179);
            this.txtDiaChi.MaxLength = 500;
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(216, 75);
            this.txtDiaChi.TabIndex = 213;
            // 
            // txtDiDong
            // 
            this.txtDiDong.Enabled = false;
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(127, 414);
            this.txtDiDong.MaxLength = 20;
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.Size = new System.Drawing.Size(216, 29);
            this.txtDiDong.TabIndex = 216;
            // 
            // labCity
            // 
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(17, 382);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(107, 21);
            this.labCity.TabIndex = 221;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTenChaMe
            // 
            this.txtTenChaMe.Enabled = false;
            this.txtTenChaMe.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChaMe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTenChaMe.Location = new System.Drawing.Point(126, 47);
            this.txtTenChaMe.MaxLength = 250;
            this.txtTenChaMe.Name = "txtTenChaMe";
            this.txtTenChaMe.Size = new System.Drawing.Size(216, 29);
            this.txtTenChaMe.TabIndex = 211;
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CustomFormat = "MM/dd/yyyy";
            this.dtpNgaySinh.Enabled = false;
            this.dtpNgaySinh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(126, 107);
            this.dtpNgaySinh.MaxDate = new System.DateTime(5000, 12, 31, 0, 0, 0, 0);
            this.dtpNgaySinh.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(216, 29);
            this.dtpNgaySinh.TabIndex = 212;
            this.dtpNgaySinh.Value = new System.DateTime(2015, 9, 13, 0, 0, 0, 0);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Enabled = false;
            this.txtGhiChu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtGhiChu.Location = new System.Drawing.Point(125, 260);
            this.txtGhiChu.MaxLength = 500;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(216, 78);
            this.txtGhiChu.TabIndex = 218;
            // 
            // labAddress
            // 
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(20, 182);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(103, 21);
            this.labAddress.TabIndex = 220;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labMobile
            // 
            this.labMobile.AutoSize = true;
            this.labMobile.BackColor = System.Drawing.Color.Transparent;
            this.labMobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMobile.Location = new System.Drawing.Point(60, 418);
            this.labMobile.Name = "labMobile";
            this.labMobile.Size = new System.Drawing.Size(65, 21);
            this.labMobile.TabIndex = 223;
            this.labMobile.Tag = "Di Động :";
            this.labMobile.Text = "Mobile :";
            this.labMobile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.Enabled = false;
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(127, 379);
            this.txtThanhPho.MaxLength = 20;
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.Size = new System.Drawing.Size(216, 29);
            this.txtThanhPho.TabIndex = 214;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Enabled = false;
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(126, 12);
            this.txtTen.MaxLength = 250;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(216, 29);
            this.txtTen.TabIndex = 210;
            // 
            // labSex
            // 
            this.labSex.BackColor = System.Drawing.Color.Transparent;
            this.labSex.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSex.Location = new System.Drawing.Point(32, 81);
            this.labSex.Name = "labSex";
            this.labSex.Size = new System.Drawing.Size(91, 21);
            this.labSex.TabIndex = 226;
            this.labSex.Tag = "Giới Tính :";
            this.labSex.Text = "Sex :";
            this.labSex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labBirthday
            // 
            this.labBirthday.BackColor = System.Drawing.Color.Transparent;
            this.labBirthday.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labBirthday.Location = new System.Drawing.Point(31, 113);
            this.labBirthday.Name = "labBirthday";
            this.labBirthday.Size = new System.Drawing.Size(92, 21);
            this.labBirthday.TabIndex = 228;
            this.labBirthday.Tag = "Ngày Sinh :";
            this.labBirthday.Text = "Birthday :";
            this.labBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labNote
            // 
            this.labNote.BackColor = System.Drawing.Color.Transparent;
            this.labNote.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNote.Location = new System.Drawing.Point(17, 263);
            this.labNote.Name = "labNote";
            this.labNote.Size = new System.Drawing.Size(106, 21);
            this.labNote.TabIndex = 229;
            this.labNote.Tag = "Ghi Chú :";
            this.labNote.Text = "Remark :";
            this.labNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEmail.Enabled = false;
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(126, 144);
            this.txtEmail.MaxLength = 250;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(216, 29);
            this.txtEmail.TabIndex = 215;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtDienThoai.Enabled = false;
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(127, 344);
            this.txtDienThoai.MaxLength = 20;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(216, 29);
            this.txtDienThoai.TabIndex = 217;
            // 
            // labPhone
            // 
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(34, 347);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(91, 21);
            this.labPhone.TabIndex = 222;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Phone :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labEmail
            // 
            this.labEmail.AutoSize = true;
            this.labEmail.BackColor = System.Drawing.Color.White;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(70, 152);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(55, 21);
            this.labEmail.TabIndex = 224;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // c4FunGroupBox1
            // 
            this.c4FunGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.c4FunGroupBox1.Location = new System.Drawing.Point(3, 42);
            this.c4FunGroupBox1.Name = "c4FunGroupBox1";
            this.c4FunGroupBox1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            // 
            // c4FunGroupBox1.Panel
            // 
            this.c4FunGroupBox1.Panel.Controls.Add(this.labDateCreate);
            this.c4FunGroupBox1.Panel.Controls.Add(this.btnCancel);
            this.c4FunGroupBox1.Panel.Controls.Add(this.label19);
            this.c4FunGroupBox1.Panel.Controls.Add(this.btnSave);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtRATD);
            this.c4FunGroupBox1.Panel.Controls.Add(this.label18);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtLATD);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtTen);
            this.c4FunGroupBox1.Panel.Controls.Add(this.GroupBox5);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtGhiChu);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labNote);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtDiDong);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtDiaChi);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtDienThoai);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labCity);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labPhone);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labMobile);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labReportId);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labMa);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtEmail);
            this.c4FunGroupBox1.Panel.Controls.Add(this.rdFemale);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtThanhPho);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labEmail);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labName);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labAddress);
            this.c4FunGroupBox1.Panel.Controls.Add(this.rdMale);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labParent);
            this.c4FunGroupBox1.Panel.Controls.Add(this.txtTenChaMe);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labBirthday);
            this.c4FunGroupBox1.Panel.Controls.Add(this.dtpNgaySinh);
            this.c4FunGroupBox1.Panel.Controls.Add(this.labSex);
            this.c4FunGroupBox1.Size = new System.Drawing.Size(356, 684);
            this.c4FunGroupBox1.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.Red;
            this.c4FunGroupBox1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c4FunGroupBox1.StateCommon.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Near;
            this.c4FunGroupBox1.StateCommon.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunGroupBox1.TabIndex = 234;
            this.c4FunGroupBox1.Tag = "THÔNG TIN KHÁCH HÀNG";
            this.c4FunGroupBox1.Values.Heading = "CUSTOMER INFORMATION";
            // 
            // labDateCreate
            // 
            this.labDateCreate.AutoSize = true;
            this.labDateCreate.Dock = System.Windows.Forms.DockStyle.Left;
            this.labDateCreate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDateCreate.ForeColor = System.Drawing.Color.Red;
            this.labDateCreate.Location = new System.Drawing.Point(0, 0);
            this.labDateCreate.Margin = new System.Windows.Forms.Padding(3);
            this.labDateCreate.Name = "labDateCreate";
            this.labDateCreate.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.labDateCreate.Size = new System.Drawing.Size(162, 24);
            this.labDateCreate.TabIndex = 240;
            this.labDateCreate.Text = "01/06/2015 01:06:07";
            this.labDateCreate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labDateCreate.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Green;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI Semibold", 10.25F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(27, 608);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(153, 42);
            this.btnCancel.TabIndex = 112;
            this.btnCancel.Tag = "HỦY";
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 639);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 21);
            this.label19.TabIndex = 239;
            this.label19.Text = "R ATD :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label19.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 10.25F, System.Drawing.FontStyle.Bold);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(187, 608);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(153, 42);
            this.btnSave.TabIndex = 66;
            this.btnSave.Tag = "LƯU KẾT QUẢ";
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtRATD
            // 
            this.txtRATD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtRATD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRATD.Location = new System.Drawing.Point(73, 636);
            this.txtRATD.MaxLength = 5;
            this.txtRATD.Name = "txtRATD";
            this.txtRATD.Size = new System.Drawing.Size(51, 29);
            this.txtRATD.TabIndex = 238;
            this.txtRATD.Text = "40";
            this.txtRATD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRATD.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(130, 639);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 21);
            this.label18.TabIndex = 235;
            this.label18.Text = "L ATD :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // txtLATD
            // 
            this.txtLATD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtLATD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLATD.Location = new System.Drawing.Point(197, 636);
            this.txtLATD.MaxLength = 5;
            this.txtLATD.Name = "txtLATD";
            this.txtLATD.Size = new System.Drawing.Size(51, 29);
            this.txtLATD.TabIndex = 234;
            this.txtLATD.Text = "40";
            this.txtLATD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLATD.Visible = false;
            // 
            // gbShowResult
            // 
            this.gbShowResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbShowResult.Location = new System.Drawing.Point(0, 300);
            this.gbShowResult.Name = "gbShowResult";
            this.gbShowResult.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2013White;
            // 
            // gbShowResult.Panel
            // 
            this.gbShowResult.Panel.Controls.Add(this.label22);
            this.gbShowResult.Panel.Controls.Add(this.label21);
            this.gbShowResult.Panel.Controls.Add(this.label20);
            this.gbShowResult.Panel.Controls.Add(this.label17);
            this.gbShowResult.Panel.Controls.Add(this.label16);
            this.gbShowResult.Panel.Controls.Add(this.label15);
            this.gbShowResult.Panel.Controls.Add(this.label14);
            this.gbShowResult.Panel.Controls.Add(this.label13);
            this.gbShowResult.Panel.Controls.Add(this.label12);
            this.gbShowResult.Panel.Controls.Add(this.label11);
            this.gbShowResult.Panel.Controls.Add(this.txtR1PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR2PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR3PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR4PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR5PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL5PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL4PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL3PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL2PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL1PResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR1RResult);
            this.gbShowResult.Panel.Controls.Add(this.label10);
            this.gbShowResult.Panel.Controls.Add(this.txtR1LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR2RResult);
            this.gbShowResult.Panel.Controls.Add(this.label9);
            this.gbShowResult.Panel.Controls.Add(this.txtR2LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR3RResult);
            this.gbShowResult.Panel.Controls.Add(this.label8);
            this.gbShowResult.Panel.Controls.Add(this.txtR3LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR4RResult);
            this.gbShowResult.Panel.Controls.Add(this.label7);
            this.gbShowResult.Panel.Controls.Add(this.txtR4LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtR5RResult);
            this.gbShowResult.Panel.Controls.Add(this.label6);
            this.gbShowResult.Panel.Controls.Add(this.txtR5LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL5RResult);
            this.gbShowResult.Panel.Controls.Add(this.label5);
            this.gbShowResult.Panel.Controls.Add(this.txtL5LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL4RResult);
            this.gbShowResult.Panel.Controls.Add(this.label4);
            this.gbShowResult.Panel.Controls.Add(this.txtL4LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL3RResult);
            this.gbShowResult.Panel.Controls.Add(this.label3);
            this.gbShowResult.Panel.Controls.Add(this.txtL3LResult);
            this.gbShowResult.Panel.Controls.Add(this.txtL2RResult);
            this.gbShowResult.Panel.Controls.Add(this.label1);
            this.gbShowResult.Panel.Controls.Add(this.txtL2LResult);
            this.gbShowResult.Panel.Controls.Add(this.c4FunLabel2);
            this.gbShowResult.Panel.Controls.Add(this.c4FunLabel1);
            this.gbShowResult.Panel.Controls.Add(this.txtL1RResult);
            this.gbShowResult.Panel.Controls.Add(this.label2);
            this.gbShowResult.Panel.Controls.Add(this.txtL1LResult);
            this.gbShowResult.Panel.Controls.Add(this.lblShowLeft);
            this.gbShowResult.Panel.Controls.Add(this.lblShowRight);
            this.gbShowResult.Panel.Controls.Add(this.picHand);
            this.gbShowResult.Size = new System.Drawing.Size(520, 384);
            this.gbShowResult.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbShowResult.TabIndex = 235;
            this.gbShowResult.Tag = "KẾT QUẢ";
            this.gbShowResult.Values.Heading = "RESULT";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label22.Location = new System.Drawing.Point(445, 184);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(21, 17);
            this.label22.TabIndex = 111;
            this.label22.Text = "R1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(416, 129);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 17);
            this.label21.TabIndex = 110;
            this.label21.Text = "R2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label20.Location = new System.Drawing.Point(371, 116);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 17);
            this.label20.TabIndex = 109;
            this.label20.Text = "R3";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(324, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 17);
            this.label17.TabIndex = 108;
            this.label17.Text = "R4";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(289, 153);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 17);
            this.label16.TabIndex = 107;
            this.label16.Text = "R5";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label15.Location = new System.Drawing.Point(251, 153);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 17);
            this.label15.TabIndex = 106;
            this.label15.Text = "L5";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label14.Location = new System.Drawing.Point(218, 129);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 17);
            this.label14.TabIndex = 105;
            this.label14.Text = "L4";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label13.Location = new System.Drawing.Point(172, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 17);
            this.label13.TabIndex = 104;
            this.label13.Text = "L3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label12.Location = new System.Drawing.Point(125, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 17);
            this.label12.TabIndex = 103;
            this.label12.Text = "L2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(98, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 17);
            this.label11.TabIndex = 102;
            this.label11.Text = "L1";
            // 
            // txtR1PResult
            // 
            this.txtR1PResult.BackColor = System.Drawing.Color.White;
            this.txtR1PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR1PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtR1PResult.Location = new System.Drawing.Point(439, 215);
            this.txtR1PResult.MaxLength = 2;
            this.txtR1PResult.Name = "txtR1PResult";
            this.txtR1PResult.ReadOnly = true;
            this.txtR1PResult.Size = new System.Drawing.Size(27, 22);
            this.txtR1PResult.TabIndex = 101;
            this.txtR1PResult.Tag = "L1L";
            this.txtR1PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR2PResult
            // 
            this.txtR2PResult.BackColor = System.Drawing.Color.White;
            this.txtR2PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR2PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtR2PResult.Location = new System.Drawing.Point(410, 159);
            this.txtR2PResult.MaxLength = 2;
            this.txtR2PResult.Name = "txtR2PResult";
            this.txtR2PResult.ReadOnly = true;
            this.txtR2PResult.Size = new System.Drawing.Size(27, 22);
            this.txtR2PResult.TabIndex = 100;
            this.txtR2PResult.Tag = "L1L";
            this.txtR2PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR3PResult
            // 
            this.txtR3PResult.BackColor = System.Drawing.Color.White;
            this.txtR3PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR3PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtR3PResult.Location = new System.Drawing.Point(367, 146);
            this.txtR3PResult.MaxLength = 2;
            this.txtR3PResult.Name = "txtR3PResult";
            this.txtR3PResult.ReadOnly = true;
            this.txtR3PResult.Size = new System.Drawing.Size(27, 22);
            this.txtR3PResult.TabIndex = 99;
            this.txtR3PResult.Tag = "L1L";
            this.txtR3PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR4PResult
            // 
            this.txtR4PResult.BackColor = System.Drawing.Color.White;
            this.txtR4PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR4PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtR4PResult.Location = new System.Drawing.Point(324, 157);
            this.txtR4PResult.MaxLength = 2;
            this.txtR4PResult.Name = "txtR4PResult";
            this.txtR4PResult.ReadOnly = true;
            this.txtR4PResult.Size = new System.Drawing.Size(27, 22);
            this.txtR4PResult.TabIndex = 98;
            this.txtR4PResult.Tag = "L1L";
            this.txtR4PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR5PResult
            // 
            this.txtR5PResult.BackColor = System.Drawing.Color.White;
            this.txtR5PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtR5PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtR5PResult.Location = new System.Drawing.Point(292, 184);
            this.txtR5PResult.MaxLength = 2;
            this.txtR5PResult.Name = "txtR5PResult";
            this.txtR5PResult.ReadOnly = true;
            this.txtR5PResult.Size = new System.Drawing.Size(27, 22);
            this.txtR5PResult.TabIndex = 97;
            this.txtR5PResult.Tag = "L1L";
            this.txtR5PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL5PResult
            // 
            this.txtL5PResult.BackColor = System.Drawing.Color.White;
            this.txtL5PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL5PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtL5PResult.Location = new System.Drawing.Point(245, 184);
            this.txtL5PResult.MaxLength = 2;
            this.txtL5PResult.Name = "txtL5PResult";
            this.txtL5PResult.ReadOnly = true;
            this.txtL5PResult.Size = new System.Drawing.Size(27, 22);
            this.txtL5PResult.TabIndex = 96;
            this.txtL5PResult.Tag = "L5P";
            this.txtL5PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL4PResult
            // 
            this.txtL4PResult.BackColor = System.Drawing.Color.White;
            this.txtL4PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL4PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtL4PResult.Location = new System.Drawing.Point(214, 157);
            this.txtL4PResult.MaxLength = 2;
            this.txtL4PResult.Name = "txtL4PResult";
            this.txtL4PResult.ReadOnly = true;
            this.txtL4PResult.Size = new System.Drawing.Size(27, 22);
            this.txtL4PResult.TabIndex = 95;
            this.txtL4PResult.Tag = "L4P";
            this.txtL4PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL3PResult
            // 
            this.txtL3PResult.BackColor = System.Drawing.Color.White;
            this.txtL3PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL3PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtL3PResult.Location = new System.Drawing.Point(171, 146);
            this.txtL3PResult.MaxLength = 2;
            this.txtL3PResult.Name = "txtL3PResult";
            this.txtL3PResult.ReadOnly = true;
            this.txtL3PResult.Size = new System.Drawing.Size(27, 22);
            this.txtL3PResult.TabIndex = 94;
            this.txtL3PResult.Tag = "L3P";
            this.txtL3PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL2PResult
            // 
            this.txtL2PResult.BackColor = System.Drawing.Color.White;
            this.txtL2PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL2PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtL2PResult.Location = new System.Drawing.Point(128, 159);
            this.txtL2PResult.MaxLength = 2;
            this.txtL2PResult.Name = "txtL2PResult";
            this.txtL2PResult.ReadOnly = true;
            this.txtL2PResult.Size = new System.Drawing.Size(27, 22);
            this.txtL2PResult.TabIndex = 93;
            this.txtL2PResult.Tag = "L2P";
            this.txtL2PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL1PResult
            // 
            this.txtL1PResult.BackColor = System.Drawing.Color.White;
            this.txtL1PResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtL1PResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1PResult.ForeColor = System.Drawing.Color.Gray;
            this.txtL1PResult.Location = new System.Drawing.Point(99, 215);
            this.txtL1PResult.MaxLength = 2;
            this.txtL1PResult.Name = "txtL1PResult";
            this.txtL1PResult.ReadOnly = true;
            this.txtL1PResult.Size = new System.Drawing.Size(27, 22);
            this.txtL1PResult.TabIndex = 92;
            this.txtL1PResult.Tag = "L1P";
            this.txtL1PResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR1RResult
            // 
            this.txtR1RResult.BackColor = System.Drawing.Color.White;
            this.txtR1RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR1RResult.Location = new System.Drawing.Point(455, 70);
            this.txtR1RResult.MaxLength = 2;
            this.txtR1RResult.Name = "txtR1RResult";
            this.txtR1RResult.ReadOnly = true;
            this.txtR1RResult.Size = new System.Drawing.Size(27, 29);
            this.txtR1RResult.TabIndex = 91;
            this.txtR1RResult.Tag = "L1L";
            this.txtR1RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(458, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 17);
            this.label10.TabIndex = 90;
            this.label10.Text = "R1";
            // 
            // txtR1LResult
            // 
            this.txtR1LResult.BackColor = System.Drawing.Color.White;
            this.txtR1LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR1LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR1LResult.Location = new System.Drawing.Point(455, 35);
            this.txtR1LResult.MaxLength = 2;
            this.txtR1LResult.Name = "txtR1LResult";
            this.txtR1LResult.ReadOnly = true;
            this.txtR1LResult.Size = new System.Drawing.Size(27, 29);
            this.txtR1LResult.TabIndex = 89;
            this.txtR1LResult.Tag = "L1L";
            this.txtR1LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR2RResult
            // 
            this.txtR2RResult.BackColor = System.Drawing.Color.White;
            this.txtR2RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR2RResult.Location = new System.Drawing.Point(414, 70);
            this.txtR2RResult.MaxLength = 2;
            this.txtR2RResult.Name = "txtR2RResult";
            this.txtR2RResult.ReadOnly = true;
            this.txtR2RResult.Size = new System.Drawing.Size(27, 29);
            this.txtR2RResult.TabIndex = 88;
            this.txtR2RResult.Tag = "L1L";
            this.txtR2RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(417, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 17);
            this.label9.TabIndex = 87;
            this.label9.Text = "R2";
            // 
            // txtR2LResult
            // 
            this.txtR2LResult.BackColor = System.Drawing.Color.White;
            this.txtR2LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR2LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR2LResult.Location = new System.Drawing.Point(414, 35);
            this.txtR2LResult.MaxLength = 2;
            this.txtR2LResult.Name = "txtR2LResult";
            this.txtR2LResult.ReadOnly = true;
            this.txtR2LResult.Size = new System.Drawing.Size(27, 29);
            this.txtR2LResult.TabIndex = 86;
            this.txtR2LResult.Tag = "L1L";
            this.txtR2LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR3RResult
            // 
            this.txtR3RResult.BackColor = System.Drawing.Color.White;
            this.txtR3RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR3RResult.Location = new System.Drawing.Point(373, 70);
            this.txtR3RResult.MaxLength = 2;
            this.txtR3RResult.Name = "txtR3RResult";
            this.txtR3RResult.ReadOnly = true;
            this.txtR3RResult.Size = new System.Drawing.Size(27, 29);
            this.txtR3RResult.TabIndex = 85;
            this.txtR3RResult.Tag = "L1L";
            this.txtR3RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(376, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 17);
            this.label8.TabIndex = 84;
            this.label8.Text = "R3";
            // 
            // txtR3LResult
            // 
            this.txtR3LResult.BackColor = System.Drawing.Color.White;
            this.txtR3LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR3LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR3LResult.Location = new System.Drawing.Point(373, 35);
            this.txtR3LResult.MaxLength = 2;
            this.txtR3LResult.Name = "txtR3LResult";
            this.txtR3LResult.ReadOnly = true;
            this.txtR3LResult.Size = new System.Drawing.Size(27, 29);
            this.txtR3LResult.TabIndex = 83;
            this.txtR3LResult.Tag = "L1L";
            this.txtR3LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR4RResult
            // 
            this.txtR4RResult.BackColor = System.Drawing.Color.White;
            this.txtR4RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR4RResult.Location = new System.Drawing.Point(332, 70);
            this.txtR4RResult.MaxLength = 2;
            this.txtR4RResult.Name = "txtR4RResult";
            this.txtR4RResult.ReadOnly = true;
            this.txtR4RResult.Size = new System.Drawing.Size(27, 29);
            this.txtR4RResult.TabIndex = 82;
            this.txtR4RResult.Tag = "L1L";
            this.txtR4RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(336, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 17);
            this.label7.TabIndex = 81;
            this.label7.Text = "R4";
            // 
            // txtR4LResult
            // 
            this.txtR4LResult.BackColor = System.Drawing.Color.White;
            this.txtR4LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR4LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR4LResult.Location = new System.Drawing.Point(332, 35);
            this.txtR4LResult.MaxLength = 2;
            this.txtR4LResult.Name = "txtR4LResult";
            this.txtR4LResult.ReadOnly = true;
            this.txtR4LResult.Size = new System.Drawing.Size(27, 29);
            this.txtR4LResult.TabIndex = 80;
            this.txtR4LResult.Tag = "L1L";
            this.txtR4LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtR5RResult
            // 
            this.txtR5RResult.BackColor = System.Drawing.Color.White;
            this.txtR5RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR5RResult.Location = new System.Drawing.Point(291, 70);
            this.txtR5RResult.MaxLength = 2;
            this.txtR5RResult.Name = "txtR5RResult";
            this.txtR5RResult.ReadOnly = true;
            this.txtR5RResult.Size = new System.Drawing.Size(27, 29);
            this.txtR5RResult.TabIndex = 79;
            this.txtR5RResult.Tag = "L1L";
            this.txtR5RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(293, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 17);
            this.label6.TabIndex = 78;
            this.label6.Text = "R5";
            // 
            // txtR5LResult
            // 
            this.txtR5LResult.BackColor = System.Drawing.Color.White;
            this.txtR5LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtR5LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtR5LResult.Location = new System.Drawing.Point(291, 35);
            this.txtR5LResult.MaxLength = 2;
            this.txtR5LResult.Name = "txtR5LResult";
            this.txtR5LResult.ReadOnly = true;
            this.txtR5LResult.Size = new System.Drawing.Size(27, 29);
            this.txtR5LResult.TabIndex = 77;
            this.txtR5LResult.Tag = "L1L";
            this.txtR5LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL5RResult
            // 
            this.txtL5RResult.BackColor = System.Drawing.Color.White;
            this.txtL5RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL5RResult.Location = new System.Drawing.Point(250, 70);
            this.txtL5RResult.MaxLength = 2;
            this.txtL5RResult.Name = "txtL5RResult";
            this.txtL5RResult.ReadOnly = true;
            this.txtL5RResult.Size = new System.Drawing.Size(27, 29);
            this.txtL5RResult.TabIndex = 76;
            this.txtL5RResult.Tag = "L1L";
            this.txtL5RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(252, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 17);
            this.label5.TabIndex = 75;
            this.label5.Text = "L5";
            // 
            // txtL5LResult
            // 
            this.txtL5LResult.BackColor = System.Drawing.Color.White;
            this.txtL5LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL5LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL5LResult.Location = new System.Drawing.Point(250, 35);
            this.txtL5LResult.MaxLength = 2;
            this.txtL5LResult.Name = "txtL5LResult";
            this.txtL5LResult.ReadOnly = true;
            this.txtL5LResult.Size = new System.Drawing.Size(27, 29);
            this.txtL5LResult.TabIndex = 74;
            this.txtL5LResult.Tag = "L1L";
            this.txtL5LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL4RResult
            // 
            this.txtL4RResult.BackColor = System.Drawing.Color.White;
            this.txtL4RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL4RResult.Location = new System.Drawing.Point(209, 70);
            this.txtL4RResult.MaxLength = 2;
            this.txtL4RResult.Name = "txtL4RResult";
            this.txtL4RResult.ReadOnly = true;
            this.txtL4RResult.Size = new System.Drawing.Size(27, 29);
            this.txtL4RResult.TabIndex = 73;
            this.txtL4RResult.Tag = "L1L";
            this.txtL4RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(211, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 17);
            this.label4.TabIndex = 72;
            this.label4.Text = "L4";
            // 
            // txtL4LResult
            // 
            this.txtL4LResult.BackColor = System.Drawing.Color.White;
            this.txtL4LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL4LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL4LResult.Location = new System.Drawing.Point(209, 35);
            this.txtL4LResult.MaxLength = 2;
            this.txtL4LResult.Name = "txtL4LResult";
            this.txtL4LResult.ReadOnly = true;
            this.txtL4LResult.Size = new System.Drawing.Size(27, 29);
            this.txtL4LResult.TabIndex = 71;
            this.txtL4LResult.Tag = "L1L";
            this.txtL4LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL3RResult
            // 
            this.txtL3RResult.BackColor = System.Drawing.Color.White;
            this.txtL3RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL3RResult.Location = new System.Drawing.Point(168, 70);
            this.txtL3RResult.MaxLength = 2;
            this.txtL3RResult.Name = "txtL3RResult";
            this.txtL3RResult.ReadOnly = true;
            this.txtL3RResult.Size = new System.Drawing.Size(27, 29);
            this.txtL3RResult.TabIndex = 70;
            this.txtL3RResult.Tag = "L1L";
            this.txtL3RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(170, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 17);
            this.label3.TabIndex = 69;
            this.label3.Text = "L3";
            // 
            // txtL3LResult
            // 
            this.txtL3LResult.BackColor = System.Drawing.Color.White;
            this.txtL3LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL3LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL3LResult.Location = new System.Drawing.Point(168, 35);
            this.txtL3LResult.MaxLength = 2;
            this.txtL3LResult.Name = "txtL3LResult";
            this.txtL3LResult.ReadOnly = true;
            this.txtL3LResult.Size = new System.Drawing.Size(27, 29);
            this.txtL3LResult.TabIndex = 68;
            this.txtL3LResult.Tag = "L1L";
            this.txtL3LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtL2RResult
            // 
            this.txtL2RResult.BackColor = System.Drawing.Color.White;
            this.txtL2RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL2RResult.Location = new System.Drawing.Point(127, 70);
            this.txtL2RResult.MaxLength = 2;
            this.txtL2RResult.Name = "txtL2RResult";
            this.txtL2RResult.ReadOnly = true;
            this.txtL2RResult.Size = new System.Drawing.Size(27, 29);
            this.txtL2RResult.TabIndex = 67;
            this.txtL2RResult.Tag = "L1L";
            this.txtL2RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(129, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 66;
            this.label1.Text = "L2";
            // 
            // txtL2LResult
            // 
            this.txtL2LResult.BackColor = System.Drawing.Color.White;
            this.txtL2LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL2LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL2LResult.Location = new System.Drawing.Point(127, 35);
            this.txtL2LResult.MaxLength = 2;
            this.txtL2LResult.Name = "txtL2LResult";
            this.txtL2LResult.ReadOnly = true;
            this.txtL2LResult.Size = new System.Drawing.Size(27, 29);
            this.txtL2LResult.TabIndex = 65;
            this.txtL2LResult.Tag = "L1L";
            this.txtL2LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // c4FunLabel2
            // 
            this.c4FunLabel2.Location = new System.Drawing.Point(25, 75);
            this.c4FunLabel2.Name = "c4FunLabel2";
            this.c4FunLabel2.Size = new System.Drawing.Size(56, 20);
            this.c4FunLabel2.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.c4FunLabel2.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.c4FunLabel2.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel2.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel2.TabIndex = 64;
            this.c4FunLabel2.Tag = "PHẢI";
            this.c4FunLabel2.Values.Text = "RIGHT";
            // 
            // c4FunLabel1
            // 
            this.c4FunLabel1.Location = new System.Drawing.Point(24, 39);
            this.c4FunLabel1.Name = "c4FunLabel1";
            this.c4FunLabel1.Size = new System.Drawing.Size(47, 20);
            this.c4FunLabel1.StateCommon.ShortText.Color1 = System.Drawing.Color.Red;
            this.c4FunLabel1.StateCommon.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.c4FunLabel1.StateCommon.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel1.StateCommon.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunLabel1.TabIndex = 63;
            this.c4FunLabel1.Tag = "TRÁI";
            this.c4FunLabel1.Values.Text = "LEFT";
            // 
            // txtL1RResult
            // 
            this.txtL1RResult.BackColor = System.Drawing.Color.White;
            this.txtL1RResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1RResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL1RResult.Location = new System.Drawing.Point(86, 70);
            this.txtL1RResult.MaxLength = 2;
            this.txtL1RResult.Name = "txtL1RResult";
            this.txtL1RResult.ReadOnly = true;
            this.txtL1RResult.Size = new System.Drawing.Size(27, 29);
            this.txtL1RResult.TabIndex = 62;
            this.txtL1RResult.Tag = "L1L";
            this.txtL1RResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(90, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 17);
            this.label2.TabIndex = 61;
            this.label2.Text = "L1";
            // 
            // txtL1LResult
            // 
            this.txtL1LResult.BackColor = System.Drawing.Color.White;
            this.txtL1LResult.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtL1LResult.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.txtL1LResult.Location = new System.Drawing.Point(86, 35);
            this.txtL1LResult.MaxLength = 2;
            this.txtL1LResult.Name = "txtL1LResult";
            this.txtL1LResult.ReadOnly = true;
            this.txtL1LResult.Size = new System.Drawing.Size(27, 29);
            this.txtL1LResult.TabIndex = 59;
            this.txtL1LResult.Tag = "L1L";
            this.txtL1LResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // picHand
            // 
            this.picHand.Image = global::iTalent.FullReport.Properties.Resources.doibantay;
            this.picHand.Location = new System.Drawing.Point(84, 105);
            this.picHand.Name = "picHand";
            this.picHand.Size = new System.Drawing.Size(396, 252);
            this.picHand.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHand.TabIndex = 56;
            this.picHand.TabStop = false;
            // 
            // pnlAnalysis
            // 
            this.pnlAnalysis.Controls.Add(this.grpAnalysis);
            this.pnlAnalysis.Controls.Add(this.gbShowResult);
            this.pnlAnalysis.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlAnalysis.Location = new System.Drawing.Point(359, 42);
            this.pnlAnalysis.MaximumSize = new System.Drawing.Size(520, 0);
            this.pnlAnalysis.MinimumSize = new System.Drawing.Size(520, 0);
            this.pnlAnalysis.Name = "pnlAnalysis";
            this.pnlAnalysis.Size = new System.Drawing.Size(520, 684);
            this.pnlAnalysis.TabIndex = 236;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "WT.bmp");
            this.ImageList1.Images.SetKeyName(1, "WS.bmp");
            this.ImageList1.Images.SetKeyName(2, "WX.bmp");
            this.ImageList1.Images.SetKeyName(3, "WE.bmp");
            this.ImageList1.Images.SetKeyName(4, "WC.bmp");
            this.ImageList1.Images.SetKeyName(5, "WD.bmp");
            this.ImageList1.Images.SetKeyName(6, "WI.bmp");
            this.ImageList1.Images.SetKeyName(7, "UL.bmp");
            this.ImageList1.Images.SetKeyName(8, "LF.bmp");
            this.ImageList1.Images.SetKeyName(9, "RL.bmp");
            this.ImageList1.Images.SetKeyName(10, "AS.bmp");
            this.ImageList1.Images.SetKeyName(11, "AT.bmp");
            this.ImageList1.Images.SetKeyName(12, "AU.bmp");
            this.ImageList1.Images.SetKeyName(13, "AE.bmp");
            this.ImageList1.Images.SetKeyName(14, "AR.bmp");
            this.ImageList1.Images.SetKeyName(15, "WP.bmp");
            this.ImageList1.Images.SetKeyName(16, "WL.bmp");
            // 
            // lvL1
            // 
            this.lvL1.BackColor = System.Drawing.Color.Navy;
            this.lvL1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvL1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvL1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvL1.ForeColor = System.Drawing.Color.White;
            this.lvL1.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17});
            this.lvL1.LargeImageList = this.ImageList1;
            this.lvL1.Location = new System.Drawing.Point(879, 42);
            this.lvL1.Margin = new System.Windows.Forms.Padding(0);
            this.lvL1.MultiSelect = false;
            this.lvL1.Name = "lvL1";
            this.lvL1.Size = new System.Drawing.Size(434, 684);
            this.lvL1.SmallImageList = this.ImageList1;
            this.lvL1.TabIndex = 238;
            this.lvL1.TileSize = new System.Drawing.Size(184, 34);
            this.lvL1.UseCompatibleStateImageBehavior = false;
            this.lvL1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDoubleClick);
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.pnlMenu.Controls.Add(this.lablNameForm);
            this.pnlMenu.Controls.Add(this.btnThoat);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMenu.Location = new System.Drawing.Point(3, 3);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(1310, 39);
            this.pnlMenu.TabIndex = 239;
            this.pnlMenu.Visible = false;
            // 
            // lablNameForm
            // 
            this.lablNameForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lablNameForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lablNameForm.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablNameForm.ForeColor = System.Drawing.Color.White;
            this.lablNameForm.Location = new System.Drawing.Point(0, 0);
            this.lablNameForm.Name = "lablNameForm";
            this.lablNameForm.Size = new System.Drawing.Size(1270, 39);
            this.lablNameForm.TabIndex = 10;
            this.lablNameForm.Tag = ".: PHÂN TÍCH";
            this.lablNameForm.Text = ".: FINGERPRINT ANALYSIS";
            this.lablNameForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnThoat
            // 
            this.btnThoat.BackColor = System.Drawing.Color.Transparent;
            this.btnThoat.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThoat.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnThoat.FlatAppearance.BorderSize = 0;
            this.btnThoat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnThoat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnThoat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThoat.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat.ForeColor = System.Drawing.Color.White;
            this.btnThoat.Image = global::iTalent.FullReport.Properties.Resources.close;
            this.btnThoat.Location = new System.Drawing.Point(1270, 0);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(0);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(40, 39);
            this.btnThoat.TabIndex = 15;
            this.btnThoat.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnThoat.UseVisualStyleBackColor = false;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // frmAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1316, 729);
            this.Controls.Add(this.lvL1);
            this.Controls.Add(this.pnlAnalysis);
            this.Controls.Add(this.c4FunGroupBox1);
            this.Controls.Add(this.pnlMenu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1278, 726);
            this.Name = "frmAnalysis";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.PaletteMode = C4FunComponent.Toolkit.PaletteMode.Office2010Black;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = ".: FINGERPRINT ANALYSIS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmAnalysis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grpAnalysis.Panel)).EndInit();
            this.grpAnalysis.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpAnalysis)).EndInit();
            this.grpAnalysis.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.pnl1.ResumeLayout(false);
            this.pnl1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L1F)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L1R)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L1P)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L1L)).EndInit();
            this.TabPage2.ResumeLayout(false);
            this.pnl2.ResumeLayout(false);
            this.pnl2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L2F)).EndInit();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L2R)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L2P)).EndInit();
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L2L)).EndInit();
            this.TabPage3.ResumeLayout(false);
            this.pnl3.ResumeLayout(false);
            this.pnl3.PerformLayout();
            this.GroupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L3F)).EndInit();
            this.GroupBox14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L3R)).EndInit();
            this.GroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L3P)).EndInit();
            this.GroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L3L)).EndInit();
            this.TabPage4.ResumeLayout(false);
            this.pnl4.ResumeLayout(false);
            this.pnl4.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L4F)).EndInit();
            this.groupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L4R)).EndInit();
            this.groupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L4P)).EndInit();
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L4L)).EndInit();
            this.TabPage5.ResumeLayout(false);
            this.pnl5.ResumeLayout(false);
            this.pnl5.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L5F)).EndInit();
            this.groupBox21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L5R)).EndInit();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L5P)).EndInit();
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.L5L)).EndInit();
            this.TabPage6.ResumeLayout(false);
            this.pnl6.ResumeLayout(false);
            this.pnl6.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R1F)).EndInit();
            this.groupBox25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R1R)).EndInit();
            this.groupBox26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R1P)).EndInit();
            this.groupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R1L)).EndInit();
            this.TabPage7.ResumeLayout(false);
            this.pnl7.ResumeLayout(false);
            this.pnl7.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R2F)).EndInit();
            this.groupBox29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R2R)).EndInit();
            this.groupBox30.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R2P)).EndInit();
            this.groupBox31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R2L)).EndInit();
            this.TabPage8.ResumeLayout(false);
            this.pnl8.ResumeLayout(false);
            this.pnl8.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R3F)).EndInit();
            this.groupBox33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R3R)).EndInit();
            this.groupBox34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R3P)).EndInit();
            this.groupBox35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R3L)).EndInit();
            this.TabPage9.ResumeLayout(false);
            this.pnl9.ResumeLayout(false);
            this.pnl9.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R4F)).EndInit();
            this.groupBox37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R4R)).EndInit();
            this.groupBox38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R4P)).EndInit();
            this.groupBox39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R4L)).EndInit();
            this.TabPage10.ResumeLayout(false);
            this.pnl10.ResumeLayout(false);
            this.pnl10.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R5F)).EndInit();
            this.groupBox41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R5R)).EndInit();
            this.groupBox42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R5P)).EndInit();
            this.groupBox43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.R5L)).EndInit();
            this.GroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1.Panel)).EndInit();
            this.c4FunGroupBox1.Panel.ResumeLayout(false);
            this.c4FunGroupBox1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunGroupBox1)).EndInit();
            this.c4FunGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult.Panel)).EndInit();
            this.gbShowResult.Panel.ResumeLayout(false);
            this.gbShowResult.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbShowResult)).EndInit();
            this.gbShowResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHand)).EndInit();
            this.pnlAnalysis.ResumeLayout(false);
            this.pnlMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picHand;
        private C4FunComponent.Toolkit.C4FunLabel lblShowLeft;
        private C4FunComponent.Toolkit.C4FunLabel lblShowRight;
        private C4FunComponent.Toolkit.C4FunGroupBox grpAnalysis;
        private System.Windows.Forms.GroupBox GroupBox5;
        private System.Windows.Forms.PictureBox pbF;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.Label labReportId;
        private System.Windows.Forms.Label labMa;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labParent;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.TextBox txtTenChaMe;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.Label labMobile;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label labSex;
        private System.Windows.Forms.Label labBirthday;
        private System.Windows.Forms.Label labNote;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.Label labEmail;
        private C4FunComponent.Toolkit.C4FunGroupBox c4FunGroupBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtRATD;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtLATD;
        private C4FunComponent.Toolkit.C4FunGroupBox gbShowResult;
        private System.Windows.Forms.Panel pnlAnalysis;
        public System.Windows.Forms.TextBox txtL1LResult;
        public System.Windows.Forms.TextBox txtL1RResult;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtR1PResult;
        public System.Windows.Forms.TextBox txtR2PResult;
        public System.Windows.Forms.TextBox txtR3PResult;
        public System.Windows.Forms.TextBox txtR4PResult;
        public System.Windows.Forms.TextBox txtR5PResult;
        public System.Windows.Forms.TextBox txtL5PResult;
        public System.Windows.Forms.TextBox txtL4PResult;
        public System.Windows.Forms.TextBox txtL3PResult;
        public System.Windows.Forms.TextBox txtL2PResult;
        public System.Windows.Forms.TextBox txtL1PResult;
        public System.Windows.Forms.TextBox txtR1RResult;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtR1LResult;
        public System.Windows.Forms.TextBox txtR2RResult;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtR2LResult;
        public System.Windows.Forms.TextBox txtR3RResult;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtR3LResult;
        public System.Windows.Forms.TextBox txtR4RResult;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtR4LResult;
        public System.Windows.Forms.TextBox txtR5RResult;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtR5LResult;
        public System.Windows.Forms.TextBox txtL5RResult;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtL5LResult;
        public System.Windows.Forms.TextBox txtL4RResult;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtL4LResult;
        public System.Windows.Forms.TextBox txtL3RResult;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtL3LResult;
        public System.Windows.Forms.TextBox txtL2RResult;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtL2LResult;
        private C4FunComponent.Toolkit.C4FunLabel c4FunLabel2;
        private C4FunComponent.Toolkit.C4FunLabel c4FunLabel1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label labDateCreate;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage TabPage1;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.Label lblL1P;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox L1F;
        private System.Windows.Forms.TextBox txtL1P;
        private System.Windows.Forms.Button btnL1R;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox L1R;
        private System.Windows.Forms.Button btnL1L;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox L1P;
        public System.Windows.Forms.TextBox txtL1R;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox L1L;
        public System.Windows.Forms.TextBox txtL1L;
        private System.Windows.Forms.TabPage TabPage2;
        private System.Windows.Forms.Panel pnl2;
        private System.Windows.Forms.Label lblL2P;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.PictureBox L2F;
        private System.Windows.Forms.TextBox txtL2P;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.PictureBox L2R;
        private System.Windows.Forms.Button btnL2R;
        private System.Windows.Forms.Button btnL2L;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.PictureBox L2P;
        public System.Windows.Forms.TextBox txtL2R;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.PictureBox L2L;
        public System.Windows.Forms.TextBox txtL2L;
        private System.Windows.Forms.TabPage TabPage3;
        private System.Windows.Forms.Panel pnl3;
        private System.Windows.Forms.Label lblL3P;
        private System.Windows.Forms.GroupBox GroupBox15;
        private System.Windows.Forms.PictureBox L3F;
        private System.Windows.Forms.Button btnL3R;
        private System.Windows.Forms.TextBox txtL3P;
        private System.Windows.Forms.GroupBox GroupBox14;
        private System.Windows.Forms.PictureBox L3R;
        private System.Windows.Forms.Button btnL3L;
        private System.Windows.Forms.GroupBox GroupBox13;
        private System.Windows.Forms.PictureBox L3P;
        public System.Windows.Forms.TextBox txtL3R;
        private System.Windows.Forms.GroupBox GroupBox12;
        private System.Windows.Forms.PictureBox L3L;
        public System.Windows.Forms.TextBox txtL3L;
        private System.Windows.Forms.TabPage TabPage4;
        private System.Windows.Forms.Panel pnl4;
        private System.Windows.Forms.Label lblL4P;
        public System.Windows.Forms.TextBox txtL4L;
        private System.Windows.Forms.Button btnL4R;
        public System.Windows.Forms.TextBox txtL4R;
        private System.Windows.Forms.Button btnL4L;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.PictureBox L4F;
        private System.Windows.Forms.TextBox txtL4P;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.PictureBox L4R;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.PictureBox L4P;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.PictureBox L4L;
        private System.Windows.Forms.TabPage TabPage5;
        private System.Windows.Forms.Panel pnl5;
        private System.Windows.Forms.Label lblL5P;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.PictureBox L5F;
        private System.Windows.Forms.TextBox txtL5P;
        private System.Windows.Forms.Button btnL5R;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.PictureBox L5R;
        private System.Windows.Forms.Button btnL5L;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.PictureBox L5P;
        public System.Windows.Forms.TextBox txtL5R;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.PictureBox L5L;
        public System.Windows.Forms.TextBox txtL5L;
        private System.Windows.Forms.TabPage TabPage6;
        private System.Windows.Forms.Panel pnl6;
        private System.Windows.Forms.Label lblR1P;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.PictureBox R1F;
        private System.Windows.Forms.TextBox txtR1P;
        private System.Windows.Forms.Button btnR1R;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.PictureBox R1R;
        private System.Windows.Forms.Button btnR1L;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.PictureBox R1P;
        public System.Windows.Forms.TextBox txtR1R;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.PictureBox R1L;
        public System.Windows.Forms.TextBox txtR1L;
        private System.Windows.Forms.TabPage TabPage7;
        private System.Windows.Forms.Panel pnl7;
        private System.Windows.Forms.Label lblR2P;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.PictureBox R2F;
        private System.Windows.Forms.TextBox txtR2P;
        private System.Windows.Forms.Button btnR2R;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.PictureBox R2R;
        private System.Windows.Forms.Button btnR2L;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.PictureBox R2P;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.PictureBox R2L;
        private System.Windows.Forms.TabPage TabPage8;
        private System.Windows.Forms.Panel pnl8;
        private System.Windows.Forms.Label lblR3P;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.PictureBox R3F;
        private System.Windows.Forms.TextBox txtR3P;
        private System.Windows.Forms.Button btnR3R;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.PictureBox R3R;
        private System.Windows.Forms.Button btnR3L;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.PictureBox R3P;
        public System.Windows.Forms.TextBox txtR3R;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.PictureBox R3L;
        public System.Windows.Forms.TextBox txtR3L;
        private System.Windows.Forms.TabPage TabPage9;
        private System.Windows.Forms.Panel pnl9;
        private System.Windows.Forms.Label lblR4P;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.PictureBox R4F;
        private System.Windows.Forms.TextBox txtR4P;
        private System.Windows.Forms.Button btnR4R;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.PictureBox R4R;
        private System.Windows.Forms.Button btnR4L;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.PictureBox R4P;
        public System.Windows.Forms.TextBox txtR4R;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.PictureBox R4L;
        public System.Windows.Forms.TextBox txtR4L;
        private System.Windows.Forms.TabPage TabPage10;
        private System.Windows.Forms.Panel pnl10;
        private System.Windows.Forms.Label lblR5P;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.PictureBox R5F;
        private System.Windows.Forms.TextBox txtR5P;
        private System.Windows.Forms.Button btnR5R;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.PictureBox R5R;
        private System.Windows.Forms.Button btnR5L;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.PictureBox R5P;
        public System.Windows.Forms.TextBox txtR5R;
        private System.Windows.Forms.GroupBox groupBox43;
        private System.Windows.Forms.PictureBox R5L;
        public System.Windows.Forms.TextBox txtR5L;
        private System.Windows.Forms.GroupBox groupBox7;
        public System.Windows.Forms.TextBox txtR2R;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.TextBox txtR2L;
        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.ListView lvL1;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Label lablNameForm;
        private System.Windows.Forms.Button btnThoat;
    }
}