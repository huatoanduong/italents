﻿namespace iTalent.FullReport.GUI
{
    partial class FrmSignup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSignup));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.buttonSpecHeaderGroup1 = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.txtSeckey = new System.Windows.Forms.TextBox();
            this.btnCheckKey = new System.Windows.Forms.Button();
            this.txtKeyDangKy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKeyXacNhan = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtPassEmail = new System.Windows.Forms.TextBox();
            this.labPassword = new System.Windows.Forms.Label();
            this.txtTenDangNhap = new System.Windows.Forms.TextBox();
            this.labID = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.labEmail = new System.Windows.Forms.Label();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.labCellPhone = new System.Windows.Forms.Label();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.labPhone = new System.Windows.Forms.Label();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.labCity = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.labAddress = new System.Windows.Forms.Label();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.labName = new System.Windows.Forms.Label();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.buttonSpecHeaderGroup1});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtSeckey);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnCheckKey);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtKeyDangKy);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label2);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtKeyXacNhan);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.label1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox2);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtPassEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labPassword);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTenDangNhap);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labID);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiDong);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labCellPhone);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDienThoai);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labPhone);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtThanhPho);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labCity);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiaChi);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labAddress);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTen);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labName);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnLuu);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(865, 431);
            this.c4FunHeaderGroup1.StateNormal.Back.Color1 = System.Drawing.Color.Honeydew;
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 0;
            this.c4FunHeaderGroup1.Tag = "Thông Tin Người Dùng";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "User Information";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.FullReport.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // buttonSpecHeaderGroup1
            // 
            this.buttonSpecHeaderGroup1.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.buttonSpecHeaderGroup1.UniqueName = "084F18ED5C964E6DF6836771B98EE8F1";
            this.buttonSpecHeaderGroup1.Click += new System.EventHandler(this.buttonSpecHeaderGroup1_Click);
            // 
            // txtSeckey
            // 
            this.txtSeckey.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSeckey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtSeckey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeckey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtSeckey.Location = new System.Drawing.Point(137, 11);
            this.txtSeckey.Name = "txtSeckey";
            this.txtSeckey.Size = new System.Drawing.Size(250, 29);
            this.txtSeckey.TabIndex = 210;
            this.txtSeckey.Visible = false;
            // 
            // btnCheckKey
            // 
            this.btnCheckKey.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCheckKey.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCheckKey.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCheckKey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnCheckKey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnCheckKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckKey.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckKey.ForeColor = System.Drawing.Color.White;
            this.btnCheckKey.Image = global::iTalent.FullReport.Properties.Resources.Active;
            this.btnCheckKey.Location = new System.Drawing.Point(789, 82);
            this.btnCheckKey.Margin = new System.Windows.Forms.Padding(0);
            this.btnCheckKey.Name = "btnCheckKey";
            this.btnCheckKey.Size = new System.Drawing.Size(61, 64);
            this.btnCheckKey.TabIndex = 209;
            this.btnCheckKey.Tag = "Đăng Ký";
            this.btnCheckKey.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCheckKey.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCheckKey.UseVisualStyleBackColor = false;
            this.btnCheckKey.Click += new System.EventHandler(this.btnCheckKey_Click);
            // 
            // txtKeyDangKy
            // 
            this.txtKeyDangKy.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtKeyDangKy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtKeyDangKy.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyDangKy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtKeyDangKy.Location = new System.Drawing.Point(516, 8);
            this.txtKeyDangKy.Multiline = true;
            this.txtKeyDangKy.Name = "txtKeyDangKy";
            this.txtKeyDangKy.ReadOnly = true;
            this.txtKeyDangKy.Size = new System.Drawing.Size(334, 68);
            this.txtKeyDangKy.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(393, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 21);
            this.label2.TabIndex = 208;
            this.label2.Tag = "Tên :";
            this.label2.Text = "Key Xác Nhận :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKeyXacNhan
            // 
            this.txtKeyXacNhan.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtKeyXacNhan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtKeyXacNhan.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeyXacNhan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtKeyXacNhan.Location = new System.Drawing.Point(516, 82);
            this.txtKeyXacNhan.Multiline = true;
            this.txtKeyXacNhan.Name = "txtKeyXacNhan";
            this.txtKeyXacNhan.Size = new System.Drawing.Size(270, 64);
            this.txtKeyXacNhan.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(393, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 21);
            this.label1.TabIndex = 206;
            this.label1.Tag = "Tên :";
            this.label1.Text = "Key Đăng Ký :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::iTalent.FullReport.Properties.Resources.logo3;
            this.pictureBox2.Location = new System.Drawing.Point(516, 151);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(241, 100);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 204;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::iTalent.FullReport.Properties.Resources.Logo_wellgen;
            this.pictureBox1.Location = new System.Drawing.Point(516, 257);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(241, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 203;
            this.pictureBox1.TabStop = false;
            // 
            // txtPassEmail
            // 
            this.txtPassEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPassEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtPassEmail.Location = new System.Drawing.Point(137, 186);
            this.txtPassEmail.Name = "txtPassEmail";
            this.txtPassEmail.Size = new System.Drawing.Size(250, 29);
            this.txtPassEmail.TabIndex = 4;
            this.txtPassEmail.Visible = false;
            // 
            // labPassword
            // 
            this.labPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labPassword.BackColor = System.Drawing.Color.Transparent;
            this.labPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPassword.Location = new System.Drawing.Point(30, 189);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(101, 21);
            this.labPassword.TabIndex = 202;
            this.labPassword.Tag = "Mật khẩu :";
            this.labPassword.Text = "Password :";
            this.labPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labPassword.Visible = false;
            // 
            // txtTenDangNhap
            // 
            this.txtTenDangNhap.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtTenDangNhap.BackColor = System.Drawing.Color.White;
            this.txtTenDangNhap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDangNhap.Enabled = false;
            this.txtTenDangNhap.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDangNhap.ForeColor = System.Drawing.Color.Red;
            this.txtTenDangNhap.Location = new System.Drawing.Point(137, 186);
            this.txtTenDangNhap.Name = "txtTenDangNhap";
            this.txtTenDangNhap.ReadOnly = true;
            this.txtTenDangNhap.Size = new System.Drawing.Size(250, 29);
            this.txtTenDangNhap.TabIndex = 193;
            this.txtTenDangNhap.Text = "0";
            this.txtTenDangNhap.Visible = false;
            // 
            // labID
            // 
            this.labID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labID.BackColor = System.Drawing.Color.Transparent;
            this.labID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labID.Location = new System.Drawing.Point(69, 189);
            this.labID.Name = "labID";
            this.labID.Size = new System.Drawing.Size(61, 21);
            this.labID.TabIndex = 200;
            this.labID.Tag = "Mã :";
            this.labID.Text = "ID :";
            this.labID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labID.Visible = false;
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(137, 82);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(250, 29);
            this.txtEmail.TabIndex = 1;
            // 
            // labEmail
            // 
            this.labEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labEmail.BackColor = System.Drawing.Color.Transparent;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(69, 86);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(64, 21);
            this.labEmail.TabIndex = 199;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiDong
            // 
            this.txtDiDong.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(137, 117);
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.Size = new System.Drawing.Size(250, 29);
            this.txtDiDong.TabIndex = 2;
            // 
            // labCellPhone
            // 
            this.labCellPhone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labCellPhone.BackColor = System.Drawing.Color.Transparent;
            this.labCellPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCellPhone.Location = new System.Drawing.Point(30, 121);
            this.labCellPhone.Name = "labCellPhone";
            this.labCellPhone.Size = new System.Drawing.Size(100, 21);
            this.labCellPhone.TabIndex = 198;
            this.labCellPhone.Tag = "Di Động :";
            this.labCellPhone.Text = "Mobile :";
            this.labCellPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(137, 151);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(250, 29);
            this.txtDienThoai.TabIndex = 3;
            // 
            // labPhone
            // 
            this.labPhone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(34, 155);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(96, 21);
            this.labPhone.TabIndex = 197;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Tel :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(137, 304);
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.Size = new System.Drawing.Size(250, 29);
            this.txtThanhPho.TabIndex = 6;
            // 
            // labCity
            // 
            this.labCity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(40, 308);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(93, 21);
            this.labCity.TabIndex = 196;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(137, 221);
            this.txtDiaChi.Multiline = true;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(250, 77);
            this.txtDiaChi.TabIndex = 5;
            // 
            // labAddress
            // 
            this.labAddress.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(40, 249);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(93, 21);
            this.labAddress.TabIndex = 195;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTen
            // 
            this.txtTen.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(137, 47);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(250, 29);
            this.txtTen.TabIndex = 0;
            // 
            // labName
            // 
            this.labName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(65, 51);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(65, 21);
            this.labName.TabIndex = 194;
            this.labName.Tag = "Tên :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnLuu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLuu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnLuu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.ForeColor = System.Drawing.Color.White;
            this.btnLuu.Image = global::iTalent.FullReport.Properties.Resources.save;
            this.btnLuu.Location = new System.Drawing.Point(208, 339);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(179, 50);
            this.btnLuu.TabIndex = 9;
            this.btnLuu.Tag = "Đăng Ký";
            this.btnLuu.Text = "Register";
            this.btnLuu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLuu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FrmSignup
            // 
            this.AcceptButton = this.btnLuu;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 431);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSignup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StateCommon.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)(((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.Tag = ".:THÔNG TIN KHÁCH HÀNG";
            this.Load += new System.EventHandler(this.FrmSignup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup buttonSpecHeaderGroup1;
        private System.Windows.Forms.TextBox txtTenDangNhap;
        private System.Windows.Forms.Label labID;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.Label labCellPhone;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.TextBox txtPassEmail;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtKeyDangKy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKeyXacNhan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCheckKey;
        private System.Windows.Forms.TextBox txtSeckey;
    }
}