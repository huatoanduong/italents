﻿namespace iTalent.FullReport.GUI
{
    partial class FrmImportCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImportCustomer));
            this.c4FunHeaderGroup1 = new C4FunComponent.Toolkit.C4FunHeaderGroup();
            this.btnClose = new C4FunComponent.Toolkit.ButtonSpecHeaderGroup();
            this.btnChoice = new System.Windows.Forms.Button();
            this.labChoice = new System.Windows.Forms.Label();
            this.txtDirFile = new System.Windows.Forms.TextBox();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.btnCapture = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labReportId = new System.Windows.Forms.Label();
            this.labMa = new System.Windows.Forms.Label();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnScaner = new System.Windows.Forms.Button();
            this.labName = new System.Windows.Forms.Label();
            this.labParent = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtDiDong = new System.Windows.Forms.TextBox();
            this.labCity = new System.Windows.Forms.Label();
            this.txtTenChaMe = new System.Windows.Forms.TextBox();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.labAddress = new System.Windows.Forms.Label();
            this.labMobile = new System.Windows.Forms.Label();
            this.txtThanhPho = new System.Windows.Forms.TextBox();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.labSex = new System.Windows.Forms.Label();
            this.labBirthday = new System.Windows.Forms.Label();
            this.labNote = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.labPhone = new System.Windows.Forms.Label();
            this.labEmail = new System.Windows.Forms.Label();
            this.cbxGioiTinh = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).BeginInit();
            this.c4FunHeaderGroup1.Panel.SuspendLayout();
            this.c4FunHeaderGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c4FunHeaderGroup1
            // 
            this.c4FunHeaderGroup1.ButtonSpecs.AddRange(new C4FunComponent.Toolkit.ButtonSpecHeaderGroup[] {
            this.btnClose});
            this.c4FunHeaderGroup1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c4FunHeaderGroup1.GroupBackStyle = C4FunComponent.Toolkit.PaletteBackStyle.PanelClient;
            this.c4FunHeaderGroup1.GroupBorderStyle = C4FunComponent.Toolkit.PaletteBorderStyle.ButtonBreadCrumb;
            this.c4FunHeaderGroup1.HeaderVisibleSecondary = false;
            this.c4FunHeaderGroup1.Location = new System.Drawing.Point(0, 0);
            this.c4FunHeaderGroup1.Name = "c4FunHeaderGroup1";
            this.c4FunHeaderGroup1.PaletteMode = C4FunComponent.Toolkit.PaletteMode.ProfessionalSystem;
            // 
            // c4FunHeaderGroup1.Panel
            // 
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnChoice);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labChoice);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDirFile);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.rdFemale);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.rdMale);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnCapture);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox2);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.pictureBox1);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labReportId);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labMa);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnLuu);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.btnScaner);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labName);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labParent);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiaChi);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDiDong);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labCity);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTenChaMe);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.dtpNgaySinh);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtGhiChu);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labAddress);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labMobile);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtThanhPho);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtTen);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labSex);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labBirthday);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labNote);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.txtDienThoai);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labPhone);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.labEmail);
            this.c4FunHeaderGroup1.Panel.Controls.Add(this.cbxGioiTinh);
            this.c4FunHeaderGroup1.Size = new System.Drawing.Size(1008, 413);
            this.c4FunHeaderGroup1.StateNormal.Border.Color1 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.Color2 = System.Drawing.Color.Black;
            this.c4FunHeaderGroup1.StateNormal.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)((((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Bottom) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextH = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.StateNormal.HeaderPrimary.Content.ShortText.TextV = C4FunComponent.Toolkit.PaletteRelativeAlign.Center;
            this.c4FunHeaderGroup1.TabIndex = 150;
            this.c4FunHeaderGroup1.Tag = "Thông Tin Khách Hàng";
            this.c4FunHeaderGroup1.ValuesPrimary.Heading = "Import Customer Information";
            this.c4FunHeaderGroup1.ValuesPrimary.Image = global::iTalent.FullReport.Properties.Resources.user;
            this.c4FunHeaderGroup1.ValuesSecondary.Heading = "s";
            // 
            // btnClose
            // 
            this.btnClose.Checked = C4FunComponent.Toolkit.ButtonCheckState.Checked;
            this.btnClose.Orientation = C4FunComponent.Toolkit.PaletteButtonOrientation.FixedTop;
            this.btnClose.Type = C4FunComponent.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose.UniqueName = "C0168292F1894EBFC89DBF3B08D142F2";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnChoice
            // 
            this.btnChoice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnChoice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnChoice.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnChoice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnChoice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnChoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChoice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChoice.ForeColor = System.Drawing.Color.White;
            this.btnChoice.Image = global::iTalent.FullReport.Properties.Resources.BrowseFolder;
            this.btnChoice.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnChoice.Location = new System.Drawing.Point(555, 31);
            this.btnChoice.Margin = new System.Windows.Forms.Padding(0);
            this.btnChoice.Name = "btnChoice";
            this.btnChoice.Size = new System.Drawing.Size(41, 36);
            this.btnChoice.TabIndex = 212;
            this.btnChoice.Tag = "";
            this.btnChoice.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnChoice.UseVisualStyleBackColor = false;
            this.btnChoice.Click += new System.EventHandler(this.btnChoice_Click);
            // 
            // labChoice
            // 
            this.labChoice.BackColor = System.Drawing.Color.Transparent;
            this.labChoice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labChoice.Location = new System.Drawing.Point(3, 12);
            this.labChoice.Name = "labChoice";
            this.labChoice.Size = new System.Drawing.Size(136, 21);
            this.labChoice.TabIndex = 211;
            this.labChoice.Tag = "Tên Khách Hàng :Choice file customer :";
            this.labChoice.Text = "Choice file .dat :";
            this.labChoice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDirFile
            // 
            this.txtDirFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtDirFile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDirFile.Location = new System.Drawing.Point(113, 36);
            this.txtDirFile.MaxLength = 250;
            this.txtDirFile.Name = "txtDirFile";
            this.txtDirFile.Size = new System.Drawing.Size(443, 29);
            this.txtDirFile.TabIndex = 210;
            // 
            // rdFemale
            // 
            this.rdFemale.AutoSize = true;
            this.rdFemale.BackColor = System.Drawing.Color.Transparent;
            this.rdFemale.Location = new System.Drawing.Point(521, 90);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(59, 17);
            this.rdFemale.TabIndex = 209;
            this.rdFemale.Text = "Female";
            this.rdFemale.UseVisualStyleBackColor = false;
            // 
            // rdMale
            // 
            this.rdMale.AutoSize = true;
            this.rdMale.BackColor = System.Drawing.Color.Transparent;
            this.rdMale.Checked = true;
            this.rdMale.Location = new System.Drawing.Point(452, 90);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(48, 17);
            this.rdMale.TabIndex = 208;
            this.rdMale.TabStop = true;
            this.rdMale.Text = "Male";
            this.rdMale.UseVisualStyleBackColor = false;
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCapture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCapture.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCapture.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnCapture.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnCapture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCapture.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapture.ForeColor = System.Drawing.Color.White;
            this.btnCapture.Image = global::iTalent.FullReport.Properties.Resources.webcam_32;
            this.btnCapture.Location = new System.Drawing.Point(265, 297);
            this.btnCapture.Margin = new System.Windows.Forms.Padding(0);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(157, 41);
            this.btnCapture.TabIndex = 207;
            this.btnCapture.Tag = "Lấy Hình";
            this.btnCapture.Text = " Capture";
            this.btnCapture.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCapture.UseVisualStyleBackColor = false;
            this.btnCapture.Visible = false;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::iTalent.FullReport.Properties.Resources.logo3;
            this.pictureBox2.Location = new System.Drawing.Point(631, 51);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(351, 114);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 206;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::iTalent.FullReport.Properties.Resources.Logo_wellgen;
            this.pictureBox1.Location = new System.Drawing.Point(631, 171);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(351, 114);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 205;
            this.pictureBox1.TabStop = false;
            // 
            // labReportId
            // 
            this.labReportId.AutoSize = true;
            this.labReportId.BackColor = System.Drawing.Color.Transparent;
            this.labReportId.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labReportId.Location = new System.Drawing.Point(137, 353);
            this.labReportId.Name = "labReportId";
            this.labReportId.Size = new System.Drawing.Size(70, 21);
            this.labReportId.TabIndex = 151;
            this.labReportId.Tag = "";
            this.labReportId.Text = "ReportId";
            this.labReportId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labReportId.Visible = false;
            // 
            // labMa
            // 
            this.labMa.AutoSize = true;
            this.labMa.BackColor = System.Drawing.Color.Transparent;
            this.labMa.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMa.Location = new System.Drawing.Point(106, 353);
            this.labMa.Name = "labMa";
            this.labMa.Size = new System.Drawing.Size(25, 21);
            this.labMa.TabIndex = 150;
            this.labMa.Tag = "0";
            this.labMa.Text = "ID";
            this.labMa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labMa.Visible = false;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnLuu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLuu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnLuu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(92)))), ((int)(((byte)(145)))));
            this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.ForeColor = System.Drawing.Color.White;
            this.btnLuu.Image = global::iTalent.FullReport.Properties.Resources.save;
            this.btnLuu.Location = new System.Drawing.Point(450, 297);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(0);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(157, 41);
            this.btnLuu.TabIndex = 149;
            this.btnLuu.Tag = "Lưu Thông Tin";
            this.btnLuu.Text = "Save Info";
            this.btnLuu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnScaner
            // 
            this.btnScaner.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnScaner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnScaner.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnScaner.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(37)))), ((int)(((byte)(54)))));
            this.btnScaner.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(192)))));
            this.btnScaner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnScaner.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScaner.ForeColor = System.Drawing.Color.Black;
            this.btnScaner.Image = global::iTalent.FullReport.Properties.Resources.hand_icon__1_;
            this.btnScaner.Location = new System.Drawing.Point(85, 297);
            this.btnScaner.Margin = new System.Windows.Forms.Padding(0);
            this.btnScaner.Name = "btnScaner";
            this.btnScaner.Size = new System.Drawing.Size(157, 41);
            this.btnScaner.TabIndex = 148;
            this.btnScaner.Tag = "Lấy Vân Tay (F2)";
            this.btnScaner.Text = "Scan (F2)";
            this.btnScaner.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnScaner.UseVisualStyleBackColor = false;
            this.btnScaner.Visible = false;
            this.btnScaner.Click += new System.EventHandler(this.btnScaner_Click);
            // 
            // labName
            // 
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(4, 86);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(107, 21);
            this.labName.TabIndex = 134;
            this.labName.Tag = "Tên Khách Hàng :";
            this.labName.Text = "Name :";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labParent
            // 
            this.labParent.BackColor = System.Drawing.Color.Transparent;
            this.labParent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labParent.Location = new System.Drawing.Point(12, 120);
            this.labParent.Name = "labParent";
            this.labParent.Size = new System.Drawing.Size(98, 21);
            this.labParent.TabIndex = 141;
            this.labParent.Tag = "Tên Cha/Mẹ:";
            this.labParent.Text = "Parent :";
            this.labParent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiaChi.Location = new System.Drawing.Point(113, 152);
            this.txtDiaChi.MaxLength = 500;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(481, 29);
            this.txtDiaChi.TabIndex = 126;
            // 
            // txtDiDong
            // 
            this.txtDiDong.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiDong.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDiDong.Location = new System.Drawing.Point(113, 221);
            this.txtDiDong.MaxLength = 20;
            this.txtDiDong.Name = "txtDiDong";
            this.txtDiDong.Size = new System.Drawing.Size(171, 29);
            this.txtDiDong.TabIndex = 129;
            // 
            // labCity
            // 
            this.labCity.BackColor = System.Drawing.Color.Transparent;
            this.labCity.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCity.Location = new System.Drawing.Point(4, 190);
            this.labCity.Name = "labCity";
            this.labCity.Size = new System.Drawing.Size(107, 21);
            this.labCity.TabIndex = 136;
            this.labCity.Tag = "Thành Phố :";
            this.labCity.Text = "City :";
            this.labCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTenChaMe
            // 
            this.txtTenChaMe.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChaMe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTenChaMe.Location = new System.Drawing.Point(113, 117);
            this.txtTenChaMe.MaxLength = 250;
            this.txtTenChaMe.Name = "txtTenChaMe";
            this.txtTenChaMe.Size = new System.Drawing.Size(215, 29);
            this.txtTenChaMe.TabIndex = 124;
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.dtpNgaySinh.CustomFormat = "MM/dd/yyyy";
            this.dtpNgaySinh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(432, 117);
            this.dtpNgaySinh.MaxDate = new System.DateTime(5000, 12, 31, 0, 0, 0, 0);
            this.dtpNgaySinh.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(162, 29);
            this.dtpNgaySinh.TabIndex = 125;
            this.dtpNgaySinh.Value = new System.DateTime(2015, 9, 13, 0, 0, 0, 0);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtGhiChu.Location = new System.Drawing.Point(113, 256);
            this.txtGhiChu.MaxLength = 500;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(481, 29);
            this.txtGhiChu.TabIndex = 133;
            // 
            // labAddress
            // 
            this.labAddress.BackColor = System.Drawing.Color.Transparent;
            this.labAddress.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAddress.Location = new System.Drawing.Point(8, 152);
            this.labAddress.Name = "labAddress";
            this.labAddress.Size = new System.Drawing.Size(103, 21);
            this.labAddress.TabIndex = 135;
            this.labAddress.Tag = "Địa Chỉ :";
            this.labAddress.Text = "Address :";
            this.labAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labMobile
            // 
            this.labMobile.AutoSize = true;
            this.labMobile.BackColor = System.Drawing.Color.Transparent;
            this.labMobile.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMobile.Location = new System.Drawing.Point(46, 224);
            this.labMobile.Name = "labMobile";
            this.labMobile.Size = new System.Drawing.Size(65, 21);
            this.labMobile.TabIndex = 138;
            this.labMobile.Tag = "Di Động :";
            this.labMobile.Text = "Mobile :";
            this.labMobile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtThanhPho
            // 
            this.txtThanhPho.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThanhPho.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtThanhPho.Location = new System.Drawing.Point(113, 187);
            this.txtThanhPho.MaxLength = 20;
            this.txtThanhPho.Name = "txtThanhPho";
            this.txtThanhPho.Size = new System.Drawing.Size(171, 29);
            this.txtThanhPho.TabIndex = 127;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtTen.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtTen.Location = new System.Drawing.Point(113, 82);
            this.txtTen.MaxLength = 250;
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(215, 29);
            this.txtTen.TabIndex = 123;
            // 
            // labSex
            // 
            this.labSex.BackColor = System.Drawing.Color.Transparent;
            this.labSex.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSex.Location = new System.Drawing.Point(334, 86);
            this.labSex.Name = "labSex";
            this.labSex.Size = new System.Drawing.Size(91, 21);
            this.labSex.TabIndex = 142;
            this.labSex.Tag = "Giới Tính :";
            this.labSex.Text = "Sex :";
            this.labSex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labBirthday
            // 
            this.labBirthday.BackColor = System.Drawing.Color.Transparent;
            this.labBirthday.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labBirthday.Location = new System.Drawing.Point(334, 120);
            this.labBirthday.Name = "labBirthday";
            this.labBirthday.Size = new System.Drawing.Size(92, 21);
            this.labBirthday.TabIndex = 144;
            this.labBirthday.Tag = "Ngày Sinh :";
            this.labBirthday.Text = "Birthday :";
            this.labBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labNote
            // 
            this.labNote.BackColor = System.Drawing.Color.Transparent;
            this.labNote.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNote.Location = new System.Drawing.Point(3, 259);
            this.labNote.Name = "labNote";
            this.labNote.Size = new System.Drawing.Size(107, 21);
            this.labNote.TabIndex = 147;
            this.labNote.Tag = "Ghi Chú :";
            this.labNote.Text = "Remark :";
            this.labNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtEmail.Location = new System.Drawing.Point(351, 187);
            this.txtEmail.MaxLength = 250;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(243, 29);
            this.txtEmail.TabIndex = 128;
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.txtDienThoai.Location = new System.Drawing.Point(387, 221);
            this.txtDienThoai.MaxLength = 20;
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(207, 29);
            this.txtDienThoai.TabIndex = 130;
            // 
            // labPhone
            // 
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labPhone.Location = new System.Drawing.Point(290, 224);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(91, 21);
            this.labPhone.TabIndex = 137;
            this.labPhone.Tag = "Điện Thoại :";
            this.labPhone.Text = "Phone :";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labEmail
            // 
            this.labEmail.AutoSize = true;
            this.labEmail.BackColor = System.Drawing.Color.White;
            this.labEmail.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmail.Location = new System.Drawing.Point(290, 190);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(55, 21);
            this.labEmail.TabIndex = 139;
            this.labEmail.Tag = "Email :";
            this.labEmail.Text = "Email :";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxGioiTinh
            // 
            this.cbxGioiTinh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxGioiTinh.DropDownWidth = 120;
            this.cbxGioiTinh.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxGioiTinh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.cbxGioiTinh.FormattingEnabled = true;
            this.cbxGioiTinh.ItemHeight = 21;
            this.cbxGioiTinh.Items.AddRange(new object[] {
            "Nam",
            "Nữ",
            "None"});
            this.cbxGioiTinh.Location = new System.Drawing.Point(650, 62);
            this.cbxGioiTinh.Name = "cbxGioiTinh";
            this.cbxGioiTinh.Size = new System.Drawing.Size(97, 29);
            this.cbxGioiTinh.TabIndex = 143;
            this.cbxGioiTinh.Tag = "Nữ";
            this.cbxGioiTinh.Visible = false;
            // 
            // FrmImportCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 413);
            this.ControlBox = false;
            this.Controls.Add(this.c4FunHeaderGroup1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmImportCustomer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StateCommon.Border.DrawBorders = ((C4FunComponent.Toolkit.PaletteDrawBorders)(((C4FunComponent.Toolkit.PaletteDrawBorders.Top | C4FunComponent.Toolkit.PaletteDrawBorders.Left) 
            | C4FunComponent.Toolkit.PaletteDrawBorders.Right)));
            this.Tag = ".:THÔNG TIN KHÁCH HÀNG";
            this.Load += new System.EventHandler(this.FrmCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1.Panel)).EndInit();
            this.c4FunHeaderGroup1.Panel.ResumeLayout(false);
            this.c4FunHeaderGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c4FunHeaderGroup1)).EndInit();
            this.c4FunHeaderGroup1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C4FunComponent.Toolkit.C4FunHeaderGroup c4FunHeaderGroup1;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.Button btnScaner;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labParent;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtDiDong;
        private System.Windows.Forms.Label labCity;
        private System.Windows.Forms.TextBox txtTenChaMe;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label labAddress;
        private System.Windows.Forms.Label labMobile;
        private System.Windows.Forms.TextBox txtThanhPho;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label labSex;
        private System.Windows.Forms.Label labBirthday;
        private System.Windows.Forms.Label labNote;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.Label labPhone;
        private System.Windows.Forms.Label labEmail;
        private System.Windows.Forms.ComboBox cbxGioiTinh;
        private C4FunComponent.Toolkit.ButtonSpecHeaderGroup btnClose;
        private System.Windows.Forms.Label labMa;
        private System.Windows.Forms.Label labReportId;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.Label labChoice;
        private System.Windows.Forms.TextBox txtDirFile;
        private System.Windows.Forms.Button btnChoice;
    }
}