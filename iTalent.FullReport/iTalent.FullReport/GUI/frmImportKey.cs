﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;

namespace iTalent.FullReport.GUI
{
    public partial class frmImportKey : Form
    {
        public bool Issuccess { get; set; }
        private readonly BaseForm _baseForm;

        public frmImportKey()
        {
            InitializeComponent();
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control> { c4FunHeaderGroup1, labNumber, labKey, btnSave };
            _baseForm = new BaseForm();

            Issuccess = false;
        }

        private void SetKey()
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIPointModelService service = new FIPointModelService(unitOfWork);
                    string key = txtKey.Text.Trim();
                    bool kq = service.RegisterLicense(key, ICurrentSessionService.Version);
                    if (!kq)
                    {
                        MessageBox.Show(service.ErrMsg, @"Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    int slThem = service.GetNumAddKey(key, ICurrentSessionService.Version);
                    _baseForm.VietNamMsg = "Nhập Key hợp lệ.\nCó : " + slThem + " " + ICurrentSessionService.Version + @" báo cáo được thêm vào hệ thống.";
                    _baseForm.EnglishMsg = "Import key successful!\n " + slThem + " " + ICurrentSessionService.Version + @" report added.";
                    _baseForm.ShowMessage(IconMessageBox.Information);

                    int sl = service.GetNumberPointLeft(ICurrentSessionService.CurAgency.ID, ICurrentSessionService.Version);
                    txtNumber.Text = sl.ToString();
                }

                txtKey.Text = "";
                txtKey.Select();
            }
            catch (Exception ex)
            {
                txtNumber.Text = @"0";
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                //throw;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SetKey();
        }

        private void frmImportKey_Load(object sender, EventArgs e)
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.OpenConnection();
                IFIPointModelService service = new FIPointModelService(unitOfWork);
                int sl = service.GetNumberPointLeft(ICurrentSessionService.CurAgency.ID,ICurrentSessionService.Version);
                txtNumber.Text = sl.ToString();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
