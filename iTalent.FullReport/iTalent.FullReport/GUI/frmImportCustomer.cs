﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entity;
using iTalent.FingerprintScanDevice;
using iTalent.Utils;

namespace iTalent.FullReport.GUI
{
    public partial class FrmImportCustomer : C4FunForm
    {
        private FICustomer CurrCustomer { get; set; }
        private DatFileModel DatFileModel { get; set; }
        private readonly BaseForm _baseForm;
        private string _oldNameCustomer;
        private string DatFilePath { get; set; }

        private string TempDirectoryPath
        {
            get { return ICurrentSessionService.DesDirNameTemp; }
        }

        private string SourceDirPath
        {
            get { return ICurrentSessionService.DesDirNameSource; }
        }

        private bool _isinsert;

        public FrmImportCustomer()
        {
            InitializeComponent();
            btnLuu.Enabled = false;
            BaseForm.Frm = this;
            BaseForm.ListconControls = new List<Control>
                                       {
                                           c4FunHeaderGroup1,
                                           labAddress,
                                           labBirthday,
                                           labCity,
                                           labCity,
                                           labEmail,
                                           //labEntity,
                                           labMobile,
                                           labName,
                                           labNote,
                                           labParent,
                                           labPhone,
                                           labSex,
                                           btnLuu,
                                           btnScaner,
                                           labChoice
                                       };

            _baseForm = new BaseForm();
            _isinsert = true;
            //LoadCbxSex();
        }

        //public FrmImportCustomer(FICustomer objCustomer)
        //{
        //	InitializeComponent();
        //	BaseForm.Frm = this;
        //	BaseForm.ListconControls = new List<Control>
        //	{
        //		c4FunHeaderGroup1,
        //		labAddress,
        //		labBirthday,
        //		labCity,
        //		labCity,
        //		labEmail,
        //		//labEntity,
        //		labMobile,
        //		labName,
        //		labNote,
        //		labParent,
        //		labPhone,
        //		labSex,
        //		btnLuu,
        //		btnScaner,
        //		 btnCapture
        //	};
        //	_baseForm = new BaseForm();
        //	CurrCustomer = objCustomer;
        //	Issuccess = false;
        //	_isinsert = false;
        //	LoadCbxSex();
        //}

        //private void LoadCbxSex()
        //{
        //	cbxGioiTinh.Items.Clear();
        //	if (ICurrentSessionService.VietNamLanguage)
        //	{
        //		cbxGioiTinh.Items.Add("None");
        //		cbxGioiTinh.Items.Add("Nam");
        //		cbxGioiTinh.Items.Add("Nữ");
        //	}
        //	else
        //	{
        //		cbxGioiTinh.Items.Add("None");
        //		cbxGioiTinh.Items.Add("Male");
        //		cbxGioiTinh.Items.Add("Female");
        //	}
        //}

        private string GetGender()
        {
            if (ICurrentSessionService.VietNamLanguage)
            {
                return rdFemale.Checked ? "Nữ" : "Nam";
            }
            return rdFemale.Checked ? "Female" : "Male";
        }

        private void SetGender(string sex)
        {
            if (sex == "Nữ" || sex == "Female")
                rdFemale.Checked = true;
            else
            {
                rdMale.Checked = true;
            }
        }
        
        private string ConvertToObject(string dirname, string reportid)
        {
            if (dirname.Contains(reportid + "L1F")) return dirname.Replace(reportid + "L1F.bmp", "1_1.b");
            if (dirname.Contains(reportid + "L1L")) return dirname.Replace(reportid + "L1L.bmp", "1_2.b");
            if (dirname.Contains(reportid + "L1R")) return dirname.Replace(reportid + "L1R.bmp", "1_3.b");

            if (dirname.Contains(reportid + "L2F")) return dirname.Replace(reportid + "L2F.bmp", "2_1.b");
            if (dirname.Contains(reportid + "L2L")) return dirname.Replace(reportid + "L2L.bmp", "2_2.b");
            if (dirname.Contains(reportid + "L2R")) return dirname.Replace(reportid + "L2R.bmp", "2_3.b");

            if (dirname.Contains(reportid + "L3F")) return dirname.Replace(reportid + "L3F.bmp", "3_1.b");
            if (dirname.Contains(reportid + "L3L")) return dirname.Replace(reportid + "L3L.bmp", "3_2.b");
            if (dirname.Contains(reportid + "L3R")) return dirname.Replace(reportid + "L3R.bmp", "3_3.b");

            if (dirname.Contains(reportid + "L4F")) return dirname.Replace(reportid + "L4F.bmp", "4_1.b");
            if (dirname.Contains(reportid + "L4L")) return dirname.Replace(reportid + "L4L.bmp", "4_2.b");
            if (dirname.Contains(reportid + "L4R")) return dirname.Replace(reportid + "L4R.bmp", "4_3.b");

            if (dirname.Contains(reportid + "L5F")) return dirname.Replace(reportid + "L5F.bmp", "5_1.b");
            if (dirname.Contains(reportid + "L5L")) return dirname.Replace(reportid + "L5L.bmp", "5_2.b");
            if (dirname.Contains(reportid + "L5R")) return dirname.Replace(reportid + "L5R.bmp", "5_3.b");

            //

            if (dirname.Contains(reportid + "R1F")) return dirname.Replace(reportid + "R1F.bmp", "6_1.b");
            if (dirname.Contains(reportid + "R1L")) return dirname.Replace(reportid + "R1L.bmp", "6_2.b");
            if (dirname.Contains(reportid + "R1R")) return dirname.Replace(reportid + "R1R.bmp", "6_3.b");

            if (dirname.Contains(reportid + "R2F")) return dirname.Replace(reportid + "R2F.bmp", "7_1.b");
            if (dirname.Contains(reportid + "R2L")) return dirname.Replace(reportid + "R2L.bmp", "7_2.b");
            if (dirname.Contains(reportid + "R2R")) return dirname.Replace(reportid + "R2R.bmp", "7_3.b");

            if (dirname.Contains(reportid + "R3F")) return dirname.Replace(reportid + "R3F.bmp", "8_1.b");
            if (dirname.Contains(reportid + "R3L")) return dirname.Replace(reportid + "R3L.bmp", "8_2.b");
            if (dirname.Contains(reportid + "R3R")) return dirname.Replace(reportid + "R3R.bmp", "8_3.b");

            if (dirname.Contains(reportid + "R4F")) return dirname.Replace(reportid + "R4F.bmp", "9_1.b");
            if (dirname.Contains(reportid + "R4L")) return dirname.Replace(reportid + "R4L.bmp", "9_2.b");
            if (dirname.Contains(reportid + "R4R")) return dirname.Replace(reportid + "R4R.bmp", "9_3.b");

            if (dirname.Contains(reportid + "R5F")) return dirname.Replace(reportid + "R5F.bmp", "10_1.b");
            if (dirname.Contains(reportid + "R5L")) return dirname.Replace(reportid + "R5L.bmp", "10_2.b");
            if (dirname.Contains(reportid + "R5R")) return dirname.Replace(reportid + "R5R.bmp", "10_3.b");

            return string.Empty;
        }

        private string ConvertToImage(string dirname, string reportid)
        {
            if (dirname.Contains("1_1")) return dirname.Replace("1_1.b", reportid + "L1F.bmp");
            if (dirname.Contains("1_2")) return dirname.Replace("1_2.b", reportid + "L1L.bmp");
            if (dirname.Contains("1_3")) return dirname.Replace("1_3.b", reportid + "L1R.bmp");

            if (dirname.Contains("2_1")) return dirname.Replace("2_1.b", reportid + "L2F.bmp");
            if (dirname.Contains("2_2")) return dirname.Replace("2_2.b", reportid + "L2L.bmp");
            if (dirname.Contains("2_3")) return dirname.Replace("2_3.b", reportid + "L2R.bmp");

            if (dirname.Contains("3_1")) return dirname.Replace("3_1.b", reportid + "L3F.bmp");
            if (dirname.Contains("3_2")) return dirname.Replace("3_2.b", reportid + "L3L.bmp");
            if (dirname.Contains("3_3")) return dirname.Replace("3_3.b", reportid + "L3R.bmp");

            if (dirname.Contains("4_1")) return dirname.Replace("4_1.b", reportid + "L4F.bmp");
            if (dirname.Contains("4_2")) return dirname.Replace("4_2.b", reportid + "L4L.bmp");
            if (dirname.Contains("4_3")) return dirname.Replace("4_3.b", reportid + "L4R.bmp");

            if (dirname.Contains("5_1")) return dirname.Replace("5_1.b", reportid + "L5F.bmp");
            if (dirname.Contains("5_2")) return dirname.Replace("5_2.b", reportid + "L5L.bmp");
            if (dirname.Contains("5_3")) return dirname.Replace("5_3.b", reportid + "L5R.bmp");

            //

            if (dirname.Contains("6_1")) return dirname.Replace("6_1.b", reportid + "R1F.bmp");
            if (dirname.Contains("6_2")) return dirname.Replace("6_2.b", reportid + "R1L.bmp");
            if (dirname.Contains("6_3")) return dirname.Replace("6_3.b", reportid + "R1R.bmp");

            if (dirname.Contains("7_1")) return dirname.Replace("7_1.b", reportid + "R2F.bmp");
            if (dirname.Contains("7_2")) return dirname.Replace("7_2.b", reportid + "R2L.bmp");
            if (dirname.Contains("7_3")) return dirname.Replace("7_3.b", reportid + "R2R.bmp");

            if (dirname.Contains("8_1")) return dirname.Replace("8_1.b", reportid + "R3F.bmp");
            if (dirname.Contains("8_2")) return dirname.Replace("8_2.b", reportid + "R3L.bmp");
            if (dirname.Contains("8_3")) return dirname.Replace("8_3.b", reportid + "R3R.bmp");

            if (dirname.Contains("9_1")) return dirname.Replace("9_1.b", reportid + "R4F.bmp");
            if (dirname.Contains("9_2")) return dirname.Replace("9_2.b", reportid + "R4L.bmp");
            if (dirname.Contains("9_3")) return dirname.Replace("9_3.b", reportid + "R4R.bmp");

            if (dirname.Contains("10_1")) return dirname.Replace("10_1.b", reportid + "R5F.bmp");
            if (dirname.Contains("10_2")) return dirname.Replace("10_2.b", reportid + "R5L.bmp");
            if (dirname.Contains("10_3")) return dirname.Replace("10_3.b", reportid + "R5R.bmp");


            return string.Empty;
        }

        private void SaveData()
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    _oldNameCustomer = txtTen.Text;
                    string nameCustomer = "";
                    int id = int.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    if (obj != null)
                    {
                        _oldNameCustomer = obj.Name;
                        labReportId.Text = obj.ReportID;
                    }

                    //Them Sua Khach Hang Tai Day
                    FICustomer objCustomer = service.CreateEntity();
                    objCustomer.ID = int.Parse(labMa.Text);
                    objCustomer.AgencyID = ICurrentSessionService.UserId;
                    objCustomer.ReportID = labReportId.Text;
                    objCustomer.Name = txtTen.Text;
                    objCustomer.Address1 = txtDiaChi.Text;
                    objCustomer.City = txtThanhPho.Text;
                    objCustomer.DOB = dtpNgaySinh.Value;
                    objCustomer.Email = txtEmail.Text;
                    objCustomer.Gender = GetGender();
                    objCustomer.Mobile = txtDiDong.Text;
                    objCustomer.Tel = txtDienThoai.Text;
                    objCustomer.ZipPosTalCode = ""; //txtZip.Text;
                    objCustomer.Remark = txtGhiChu.Text;
                    objCustomer.State = ""; //txtTinhTrang.Text;

                    nameCustomer = ConvertUtil.ConverToUnsign(objCustomer.Name).Replace(" ", "").ToUpper();
                    if (_oldNameCustomer != null)
                        _oldNameCustomer = ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();

                    if (obj != null)
                    {
                        bool kq = service.Update(objCustomer);
                        if (kq)
                        {
                            //InsertUpdatefile(SourceDirPath, ICurrentSessionService.DesDirNameTemp, objCustomer.ReportID,
                            //    nameCustomer, _oldNameCustomer);
                            //SaveDataFile(ICurrentSessionService.CurAgency, objCustomer, SourceDirPath);

                            _baseForm.EnglishMsg = "Update information Successfully!";
                            _baseForm.VietNamMsg = "Cập Nhật Thành Công!";
                            _baseForm.ShowMessage(IconMessageBox.Information);
                            //Issuccess = true;
                            CurrCustomer = objCustomer;
                            _isinsert = false;
                        }
                        else
                        {
                            _baseForm.ShowMessage(IconMessageBox.Information, service.ErrMsg);
                        }
                    }
                    else
                    {
                        bool kq = service.Add(objCustomer);
                        if (kq)
                        {
                            //InsertUpdatefile(SourceDirPath, ICurrentSessionService.DesDirNameTemp, objCustomer.ReportID,
                            //    nameCustomer, "");
                            //SaveDataFile(ICurrentSessionService.CurAgency, objCustomer, SourceDirPath);

                            //_baseForm.EnglishMsg = "Add information Successfully!";
                            //_baseForm.VietNamMsg = "Thêm Thành Công!";
                            //_baseForm.ShowMessage(IconMessageBox.Information);
                            //Issuccess = true;
                            CurrCustomer = objCustomer;
                            _isinsert = true;
                        }
                        else
                        {
                            _baseForm.ShowMessage(IconMessageBox.Information, service.ErrMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            //SaveData();
            Save();
        }

        private void btnScaner_Click(object sender, EventArgs e)
        {
            try
            {
                Hide();
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    int id = int.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    labReportId.Text = ICurrentSessionService.Username + DateTime.Now.ToString("ddMMyyyyHHmmss");
                    if (obj != null)
                    {
                        labReportId.Text = obj.ReportID;
                        _oldNameCustomer = obj.Name;
                    }

                    string dirFolderOld = string.Empty;
                    if (_oldNameCustomer != null)
                    {
                        dirFolderOld = SourceDirPath + @"\" + labReportId.Text + "_" +
                                       ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                    }

                    string dirtemp = ICurrentSessionService.DesDirNameTemp + @"\" + labReportId.Text;

                    Directory.CreateDirectory(dirtemp);

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if ((f.Name.EndsWith(".b") || f.Name.EndsWith(".bmp")) &&
                                   !f.Name.ToLower().Contains(labReportId.Text.Substring(7).ToLower() + "f.bmp"))
                                    destFile = ConvertToImage(destFile, labReportId.Text);
                                File.Copy(f.FullName, destFile, true);
                                f.Delete();
                            }
                        }
                        d.Delete(true);
                    }

                    var frm = new FrmScan(labReportId.Text, dirtemp);
                    frm.ShowDialog(this);
                }

                Activate();
                Show();

            }
            catch (Exception ex)
            {
                _baseForm.ShowMessage(IconMessageBox.Information, ex.Message);
                Environment.Exit(0);
            }
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            ClearUI();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            try
            {
                Hide();
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService service = new FICustomerService(unitOfWork);
                    int id = int.Parse(labMa.Text);
                    FICustomer obj = service.Find(id);
                    labReportId.Text = ICurrentSessionService.Username + DateTime.Now.ToString("ddMMyyyyHHmmss");
                    if (obj != null)
                    {
                        labReportId.Text = obj.ReportID;
                        _oldNameCustomer = obj.Name;
                    }

                    string dirFolderOld = string.Empty;
                    if (_oldNameCustomer != null)
                    {
                        dirFolderOld = SourceDirPath + @"\" + labReportId.Text + "_" +
                                       ConvertUtil.ConverToUnsign(_oldNameCustomer).Replace(" ", "").ToUpper();
                    }

                    string dirtemp = ICurrentSessionService.DesDirNameTemp + @"\" + labReportId.Text;

                    Directory.CreateDirectory(dirtemp);

                    if (Directory.Exists(dirFolderOld))
                    {
                        var d = new DirectoryInfo(dirFolderOld);
                        var infos = d.GetFiles();
                        if (d.GetFiles().Any())
                        {
                            foreach (var f in infos)
                            {
                                var file = Path.GetFileName(f.Name);
                                string destFile = Path.Combine(dirtemp, file);
                                if (!f.Name.ToLower().Contains(".dat") &&
                                    !f.Name.ToLower().Contains(labReportId.Text.ToLower() + "f.bmp"))
                                    destFile = ConvertToImage(destFile, labReportId.Text);
                                File.Copy(f.FullName, destFile, true);
                                f.Delete();
                            }
                        }
                        d.Delete(true);
                    }

                    var frm = new FrmWebCam(labReportId.Text, dirtemp);
                    frm.ShowDialog(this);
                }

                Activate();
                Show();
            }
            catch
            {
                //
            }
        }

        public int Split_String_Count(string line)
        {
            return line.Split('|').Length;
        }

        private void ClearUI()
        {
            labMa.Text = "";
            labReportId.Text = "";

            txtTen.Text = "";
            _oldNameCustomer = "";

            SetGender(""); //them 
            txtTenChaMe.Text = "";
            dtpNgaySinh.Value = DateTime.Now.AddYears(-20);
            txtDiaChi.Text = "";
            txtThanhPho.Text = "";
            txtEmail.Text = "";
            txtDiDong.Text = "";
            txtDienThoai.Text = "";
            txtGhiChu.Text = "";
            btnLuu.Enabled = false;
        }

        private void LoadUI()
        {
            ClearUI();
            if (DatFileModel != null && this.DatFileModel.FICustomer != null)
            {
                FICustomer customer = this.DatFileModel.FICustomer;
                labMa.Text = customer.ID.ToString();
                if (customer.ReportID == "")
                {
                    customer.ReportID = ICurrentSessionService.CurAgency.SecKey + DateTime.Now.ToString("ddMMyyyyHHmmss");
                    if (DatFileModel.FIAgency.SecKey!="")
                        customer.ReportID = DatFileModel.FIAgency.SecKey + DateTime.Now.ToString("ddMMyyyyHHmmss");
                }

                labReportId.Text = customer.ReportID;

                txtTen.Text = customer.Name;

                SetGender(customer.Gender); //them 
                txtTenChaMe.Text = customer.Parent;
                dtpNgaySinh.Value = customer.DOB;
                txtDiaChi.Text = customer.Address1;
                txtThanhPho.Text = customer.City;
                txtEmail.Text = customer.Email;
                txtDiDong.Text = customer.Mobile;
                txtDienThoai.Text = customer.Tel;
                txtGhiChu.Text = customer.Remark;
            }
            btnLuu.Enabled = DatFileModel != null;
        }

        private void Save()
        {
            GatherUI();
            if (this.DatFileModel != null)
            {
                /// Save to DB and write Dat file
                bool result = true;
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    IFICustomerService customerService = new FICustomerService(unitOfWork);
                    result = customerService.ImportFromDatFile(DatFileModel);
                    if (!result)
                    {
                        MessageBox.Show(customerService.ErrMsg);
                        return;
                    }

                    _baseForm.EnglishMsg = "Import Information Successfully!";
                    _baseForm.VietNamMsg = "Nhập Dữ Liệu Khách Hàng Thành Công!";
                    _baseForm.ShowMessage(IconMessageBox.Information);

                    // Move Files
                    string datDirectory = Path.GetDirectoryName(DatFilePath);
                    string[] files = Directory.GetFiles(datDirectory);

                    FICustomer customer = customerService.FindByReportId(DatFileModel.FICustomer.ReportID);
                    if (customer == null)
                    {
                        return;
                    }

                    IDirectoryService directoryService = new DirectoryService();
                    string desFolderName = directoryService.GetCustomerFolderName(customer);
                    string desDirectory = Path.Combine(ICurrentSessionService.DesDirNameSource, desFolderName);
                    string fileName;

                    if (!Directory.Exists(desDirectory))
                        Directory.CreateDirectory(desDirectory);

                    ImageChecker imgChecker = new ImageChecker();

                    foreach (string file in files.Where(file => file != DatFilePath))
                    {
                        try
                        {
                            fileName = Path.GetFileName(file);
                            bool kq = imgChecker.CheckImage(file);
                            if (kq)
                            {
                                if (fileName != null)
                                    File.Copy(file, Path.Combine(desDirectory, fileName), true);
                            }
                            else
                            {
                                if (fileName != null && !fileName.EndsWith(".b") && !fileName.EndsWith(".dat") && !fileName.EndsWith(".docx") && !fileName.EndsWith(".pdf") && fileName.EndsWith("F.bmp"))
                                    File.Copy(file, Path.Combine(desDirectory, fileName), true);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    //Write dat file
                    IDatFileService datFileService = new DatFileService(unitOfWork);
                    string datDesFileName = directoryService.GetDatFileName(customer);
                    datDesFileName = Path.Combine(desDirectory, datDesFileName);
                    result = datFileService.WriteDatFile(datDesFileName, DatFileModel);
                    if (!result)
                    {
                        MessageBox.Show(datFileService.ErrMsg);
                        return;
                    }

                    if (DatFileModel.FingerRecords.Count == 24)
                    {
                        IFIFingerAnalysisService analysisService = new FIFingerAnalysisService(unitOfWork);
                        bool kq = analysisService.CalculateFingerRecord(customer.ID);
                        if (!kq)
                        {
                            MessageBox.Show(ICurrentSessionService.VietNamLanguage
                                ? "Phân tích vân tay thất bại!"
                                : "Analysed failed!");
                        }
                    }

                    this.DialogResult = DialogResult.OK;
                    //this.Close();
                }
            } 
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            //this.Close();
        }

        private void GatherUI()
        {
            if (DatFileModel != null && this.DatFileModel.FICustomer != null)
            {
                FICustomer customer = this.DatFileModel.FICustomer;

                customer.Name = txtTen.Text;
                customer.Gender = GetGender();
                customer.Parent = txtTenChaMe.Text;
                customer.DOB = dtpNgaySinh.Value;
                customer.Address1 = txtDiaChi.Text;
                customer.City = txtThanhPho.Text;
                customer.Email = txtEmail.Text;
                customer.Mobile = txtDiDong.Text;
                customer.Tel = txtDienThoai.Text;
                customer.Remark = txtGhiChu.Text;
            }
        }

        private void ChangeFile()
        {
            if (!File.Exists(DatFilePath))
            {
                string msg = ICurrentSessionService.VietNamLanguage
                    ? "File không tồn tại"
                    : "File does not exist";
                MessageBox.Show(msg);
                return;
            }
            using (IUnitOfWorkAsync unitOfWorkAsync = UnitOfWorkFactory.MakeUnitOfWork())
            {
                IDatFileService service = new DatFileService(unitOfWorkAsync);
                this.DatFileModel = service.LoadDatFile(DatFilePath);
                if (DatFileModel == null)
                {
                    MessageBox.Show(service.ErrMsg);
                    return;
                }
                LoadUI();
            }
        }
        
        private void btnChoice_Click(object sender, EventArgs e)
        {
            try
            {
                string filter = ICurrentSessionService.VietNamLanguage
                    ? @"File Khách Hàng (*.dat)|*.*"
                    : @"Customer File (*.dat)|*.*";
                OpenFileDialog dialog = new OpenFileDialog
                                        {
                                            Filter = filter,
                                            FileName = "*.dat",
                                            Multiselect = false,
                                            FilterIndex = 0
                                        };

                if (dialog.ShowDialog() != DialogResult.OK) return;
                DatFilePath = txtDirFile.Text = dialog.FileName;

                string profilePath = Path.GetDirectoryName(DatFilePath);
                if (CheckerImages(profilePath))
                {
                    ChangeFile();
                }
                else
                {
                    DatFilePath = txtDirFile.Text = "";
                   ClearUI();
                }
            }
            catch (Exception exception)
            {

            }
        }

        private bool CheckerImages(string path)
        {
            if (Directory.Exists(path))
            {
                ImageChecker imgChecker = new ImageChecker();
                var d = new DirectoryInfo(path);
                var infos = d.GetFiles();
                if (!d.GetFiles().Any()) return false;
                foreach (var f in infos)
                {
                    imgChecker.CheckImage(f.FullName);
                }

                if (imgChecker.ListFPNotCorrect != "")
                {
                    if(imgChecker.ListFPNotCorrect.EndsWith(";"))
                    {
                        imgChecker.ListFPNotCorrect = imgChecker.ListFPNotCorrect.Remove(
                            imgChecker.ListFPNotCorrect.Length - 1, 1);
                    }

                    string msg = ICurrentSessionService.VietNamLanguage
                        ? "File hình ảnh không đúng chuẩn format của iTalent\nBạn có muốn tiếp tục nhập khách hàng này không?\n" + imgChecker.ListFPNotCorrect
                        : "File fingerprint image doesn't have right format in iTalent\nDo you want import customer?\n" + imgChecker.ListFPNotCorrect;
                    if (MessageBox.Show(msg, @"Notice", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                    {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }
        //private bool CheckImageBit(string ProfilePath)
        //{
        //    bool KQ = true;
        //    string HinhDuyet = "";
        //    if (ProfilePath != "")
        //    {
        //        try
        //        {
        //            HinhDuyet = "1_1.b";
        //            KQ = .CheckImage(Path.Combine(ProfilePath, "1_1.b"));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "1_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "1_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "1_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "1_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "2_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "2_1.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "2_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "2_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "2_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "2_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "3_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "3_1.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "3_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "3_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "3_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "3_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "4_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "4_1.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "4_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "4_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "4_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "4_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "5_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "5_1.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "5_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "5_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "5_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "5_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "6_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "6_1.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "6_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "6_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "6_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "6_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "7_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "7_1.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "7_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "7_2.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "7_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "7_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "8_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "8_1.b")));
        //            if (!KQ) goto labelErr;
        //            HinhDuyet = "8_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "8_2.b")));
        //            if (!KQ) goto labelErr;
        //            HinhDuyet = "8_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "8_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "9_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "9_1.b")));
        //            if (!KQ) goto labelErr;
        //            HinhDuyet = "9_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "9_2.b")));
        //            if (!KQ) goto labelErr;
        //            HinhDuyet = "9_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "9_3.b")));
        //            if (!KQ) goto labelErr;

        //            HinhDuyet = "10_1.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "10_1.b")));
        //            if (!KQ) goto labelErr;
        //            HinhDuyet = "10_2.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "10_2.b")));
        //            if (!KQ) goto labelErr;
        //            HinhDuyet = "10_3.b";
        //            KQ = ImageChecker.CheckImageBit(new System.Drawing.Bitmap(Path.Combine(ProfilePath, "10_3.b")));
        //            if (!KQ) goto labelErr;
        //            if (KQ)
        //                return KQ;
        //        }
        //        catch (Exception ex)
        //        {
        //            return KQ = false;
        //        }
        //    labelErr:
        //        string msg = ICurrentSessionService.VietNamLanguage
        //                ? "File hình ảnh không đúng chuẩn format của iTalent " + HinhDuyet
        //                : "File fingerprint image doesn't have right format in " + HinhDuyet;
        //        MessageBox.Show(msg);
        //        return KQ = false;
        //    }
        //    else
        //    {
        //        string msg = ICurrentSessionService.VietNamLanguage
        //                ? "File Image Không Chứa Hình Ảnh"
        //                : "File Image doesn't have fingerprint image file";
        //        MessageBox.Show(msg);
        //        return false;
        //    }
        //}
    }
}
