﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Commons;

namespace iTalent.FullReport.Language
{
    //public enum Languages
    //{
    //    English,
    //    VietNam
    //}

    public class Lang
    {
        private bool _vietNamLanguage;
        private static Lang _instance;

        public static Lang Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Lang {_vietNamLanguage = ICurrentSessionService.VietNamLanguage};
                    _instance.LoadLanguage();
                }
                else
                {
                    _instance._vietNamLanguage = ICurrentSessionService.VietNamLanguage;
                    _instance.LoadLanguage();
                }
                return _instance;
            }
        }

        public Lang()
        {

        }

        //public Lang(Form T)
        //{
        //    FrmActive = T;
        //}

        //private string Key { get; set; }

        private string Value { get
            ; set; }

        public static Form FrmActive { get; set; }

        public static ICollection<Control> ListControls { get; set; }
        public static C4FunDataGridView DgView { get; set; }

        protected Dictionary<string, string> FrmWebCam = new Dictionary<string, string>
                                                         {
                                                             {"lablNameForm", "Webcam@Chụp Hình"},
                                                             {"btnFace", "Take a photo@Chụp Hình"}
                                                         };

        protected Dictionary<string, string> FormLogin = new Dictionary<string, string>
                                                         {
                                                             {"c4FunHeaderGroup1", "Confirm@Xác Nhận"},
                                                             {"labUsername", "Username:@Tên Đăng Nhập:"},
                                                             {"labPassword", "Password:@Mật Khẩu:"},
                                                             {"btnSave", "Next@Tiếp Tục"}
                                                         };
        protected Dictionary<string, string> FormImportKey= new Dictionary<string, string>
                                                         {
                                                             {"c4FunHeaderGroup1", "Import Key Report@Nhập Key Báo Cáo"},
                                                             {"labNumber", "Number Report:@SL Hiện Tại:"},
                                                             {"labKey", "Input Key:@Nhập Key:"},
                                                             {"btnSave", "Save@Xác Nhận"}
                                                         };
        protected Dictionary<string, string> FormCountFinger = new Dictionary<string, string>
                                                         {
                                                             {"c4FunHeaderGroup1", "Choose Fingerprint To Count@Chọn Vân Tay Cần Đếm"},
                                                             {"btnCenter", "Center@Giữa"},
                                                             {"btnLeft", "Left@Trái"},
                                                             {"btnRight", "Right@Phải"}
                                                         };

        protected Dictionary<string, string> FormImportTemplate = new Dictionary<string, string>
                                                         {
                                                             {"c4FunHeaderGroup1", "Import Template Report@Nhập Mẫu Báo Cáo"},
                                                             {"labNumber", "2.Choose Type:@2.Chọn Loại Báo Cáo:"},
                                                             {"labKey", "1.Choose Template:@1.Chọn File Mẫu:"},
                                                             {"btnSave", "Save@Xác Nhận"}
                                                         };
        protected Dictionary<string, string> FormConfig = new Dictionary<string, string>
                                                          {
                                                              {"c4FunHeaderGroup1", "Option@Tùy Chọn"},
                                                              {"labLang", "Language:@Ngôn Ngữ:"},
                                                              {"raVietNam", "Viet Nam@Tiếng Việt"},
                                                              {"raEnglish", "English@Tiếng Anh"},
                                                              {"labSaveImage", "Save Images:@Lưu Hình Tại:"},
                                                              {"btnSave", "Save@Lưu"}

                                                          };

        protected Dictionary<string, string> FrmSignup = new Dictionary<string, string>
                                                         {
                                                             {
                                                                 "c4FunHeaderGroup1",
                                                                 "User Information@Thông Tin Người Dùng"
                                                             },
                                                             {"labAddress", "Address:@Địa Chỉ:"},
                                                             {"labCellPhone", "CellPhone:@Di Động:"},
                                                             {"labCity", "City:@Thành Phố:"},
                                                             {"labEmail", "Email:@Email:"},
                                                             {"labPassword", "Password:@Mật Khẩu:"},
                                                             {"labID", "ID Agency:@Mã Đại Lý:"},
                                                             {"labName", "Name:@Tên:"},
                                                             {"labPhone", "Phone:@Điện Thoại:"},
                                                             {"btnLuu", "Register@Đăng Ký"}
                                                         };

        protected Dictionary<string, string> FrmCustomer = new Dictionary<string, string>
                                                           {
                                                               {
                                                                   "c4FunHeaderGroup1",
                                                                   "Customer Information@Thông Tin Khách Hàng"
                                                               },
                                                               {"labName", "Name:@Tên:"},
                                                               {"labSex", "Gender:@Giới Tính:"},
                                                               {"labParent", "Parent:@Cha/Mẹ:"},
                                                               {"labBirthday", "Birthday:@Ngày Sinh:"},
                                                               {"labCity", "City:@Thành Phố:"},
                                                               {"labAddress", "Address:@Địa Chỉ:"},
                                                               {"labEmail", "Email:@Email:"},
                                                               {"labMobile", "Mobile:@Di Động:"},
                                                               {"Phone", "Telephone:@Điện Thoại:"},
                                                               {"labZip", "Zippostal:@Mã Bưu Điện:"},
                                                               {"labEntity", "Entity:@Đối Tượng:"},
                                                               {"labNote", "Remark:@Ghi Chú:"},
                                                               {"btnScaner", "Scanner@Lấy Vân Tay"},
                                                               {"btnLuu", "Save@Lưu"},
                                                               {"rdMale", "Male@Nam"},
                                                               {"rdFemale", "Female@Nữ"},
                                                               {"btnCapture", "Capture@Chụp Hình"}
                                                           };

        protected Dictionary<string, string> FrmImportCustomer = new Dictionary<string, string>
                                                                 {
                                                                     {
                                                                         "c4FunHeaderGroup1",
                                                                         "Import Customer Information@Nhập Thông Tin Khách Hàng"
                                                                     },
                                                                     {
                                                                         "labChoice",
                                                                         "Choose file .dat:@Chọn tập tin .dat:"
                                                                     },
                                                                     {"labName", "Name:@Tên:"},
                                                                     {"labSex", "Gender:@Giới Tính:"},
                                                                     {"labParent", "Parent:@Cha/Mẹ:"},
                                                                     {"labBirthday", "Birthday:@Ngày Sinh:"},
                                                                     {"labCity", "City:@Thành Phố:"},
                                                                     {"labAddress", "Address:@Địa Chỉ:"},
                                                                     {"labEmail", "Email:@Email:"},
                                                                     {"labMobile", "Mobile:@Di Động:"},
                                                                     {"Phone", "Phone:@Điện Thoại:"},
                                                                     {"labZip", "Zippostal:@Mã Bưu Điện:"},
                                                                     {"labEntity", "Entity:@Đối Tượng:"},
                                                                     {"labNote", "Remark:@Ghi Chú:"},
                                                                     {"btnScaner", "Scanner@Lấy Vân Tay"},
                                                                     {"btnLuu", "Save@Lưu"},
                                                                     {"rdMale", "Male@Nam"},
                                                                     {"rdFemale", "Female@Nữ"},
                                                                     {"btnCapture", "Capture@Chụp Hình"}
                                                                 };

        public Dictionary<string, string> FrmMain = new Dictionary<string, string>
        {
            {"btnRefresh", "Refresh@Tải Lại"},
            {"btnAdd", "Add@Thêm"},
            {"btnEdit", "Edit@Sửa"},
            {"btnDelete", "Delete@Xóa"},
            {"btnExport", "Export@Xuất File"},
            {"btnConfig", "Option@Tùy Chọn"},
            {"labSearch", "Search By:@Tìm Kiếm:"},
            {"rbID", "ID@Mã"},
            {"rbName", "Name@Tên"},
            {"rbParent", "Parent@Cha/Mẹ"},
            {"rbAddress", "Address@Địa Chỉ"},
            {"rbTel", "Tel@Điện Thoại"},
            {"labFromDate", "From Date:@Từ Ngày:"},
            {"labToDate", "To Date:@Đến Ngày:"},
            {"btnImportProfile", "Import Profile@Nhập Khách Hàng"},
            {"colID", "ID@Mã"},
            {"colAgencyID", "ID Agency@Mã Đại Lý"},
            {"colReportID", "ID@Mã"},
            {"colDate", "Date Create@Ngày Tạo"},
            {"colName", "Customer@Khách Hàng"},
            {"colGender", "Gender@Giới Tính"},
            {"colDOB", "DOB@Ngày Sinh"},
            {"colParent", "Parent@Cha/Mẹ"},
            {"colTel", "Tel@Điện Thoại"},
            {"colMobile", "Mobile@Di Động"},
            {"colEmail", "Email@Email"},
            {"colAddress", "Address@Địa Chỉ"},
            {"colAddress2", "Status@Trạng Thái"},
            {"colCity", "City@Thành Phố"},
            {"colState", "Entity@Đối Tượng"},
            {"colZipPostalCode", "ZipPostal@Bưu Điện"},
            {"colCountry", "Country@Quốc Tịch"},
            {"colRemark", "Note@Ghi Chú"},
            {"btnAnalysis", "Fingerprint Analysis@Phân Tích Vân Tay"},
            {"btnExportReport", "Export Report@Xuất Báo Cáo"},
            {"btnImportTemlate", "Import Template@Nhập Mẫu Báo Cáo"},
            {"btnImportKey", "Import Key Report@Nhập Key Báo Cáo"},
            {"btnUpdateKeyLi", "Key Linsence@Bản Quyền"},
        };

        protected void LoadLanguage()
        {
            try
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                switch (FrmActive.Name)
                {
                    case "FrmLogin":
                        dictionary = FormLogin;
                        break;
                    case "frmImportKey":
                        dictionary = FormImportKey;
                        break;
                    case "frmImportTemlate":
                        dictionary = FormImportTemplate;
                        break;
                    case "FrmCountFinger":
                        dictionary = FormCountFinger;
                        break;
                    case "FrmConfig":
                        dictionary = FormConfig;
                        break;
                    case "FrmSignup":
                        dictionary = FrmSignup;
                        break;
                    case "FrmMain2":
                        dictionary = FrmMain;
                        break;

                    case "FrmCustomer":
                        dictionary = FrmCustomer;
                        break;

                    case "FrmWebCam":
                        dictionary = FrmWebCam;
                        break;

                    case "FrmImportCustomer":
                        dictionary = FrmImportCustomer;
                        break;
                }

                foreach (var obj in ListControls.Where(obj => dictionary.ContainsKey(obj.Name)))
                {
                    try
                    {
                        string s1;
                        dictionary.TryGetValue(obj.Name, out s1);
                        Value = s1;
                        if (obj is C4FunHeaderGroup)
                        {
                            (obj as C4FunHeaderGroup).ValuesPrimary.Heading = SplitString;
                            continue;

                        }

                        obj.Text = SplitString;
                    }
                    catch
                    {
                    }
                }

                if (DgView == null) return;
                if (DgView.Columns.Count == 0) return;

                foreach (
                    DataGridViewTextBoxColumn col in
                        DgView.Columns.Cast<DataGridViewTextBoxColumn>().Where(col => dictionary.ContainsKey(col.Name)))
                {
                    try
                    {
                        string s1;
                        dictionary.TryGetValue(col.Name, out s1);
                        Value = s1;
                        col.HeaderText = SplitString;
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {

            }
        }

        private string SplitString
        {
            get
            {
                try
                {
                    string[] v = Value.Split('@');
                    return _vietNamLanguage ? v[1] : v[0];
                }
                catch
                {
                    return "";
                }
            }
        }
    }
}