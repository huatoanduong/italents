﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Entity;

namespace iTalent.Analyse.Model
{
    public class RecordTypeModel
    {
        public FIFingerRecord FingerRecord { get; set; }

        public string FingerType
        {
            get { return FingerRecord.FingerType; }
        }

        public string Type
        {
            get { return FingerRecord.Type; }
        }

        public FingerTypeModel FingerTypeModel { get; set; }
    }

    public class RecordTypeCollection
    {
        private List<FIFingerRecord> fingerRecords;
        private FingerTypeModelCollection typeCollection;

        public FingerTypeModelCollection TypeCollection
        {
            get { return typeCollection; }
            set
            {
                typeCollection = value;
                UpdateProperties();
            }
        }

        public List<FIFingerRecord> FingerRecords
        {
            get { return fingerRecords; }
            set
            {
                fingerRecords = value;
                UpdateProperties();
            }
        }

        private List<RecordTypeModel> compareRecords;

        public List<RecordTypeModel> CompareRecords
        {
            get { return compareRecords; }
        }

        //public FIFingerRecord L1T { get; set; }
        //public FIFingerRecord L2T { get; set; }
        //public FIFingerRecord L3T { get; set; }
        //public FIFingerRecord L4T { get; set; }
        //public FIFingerRecord L5T { get; set; }

        //public FIFingerRecord R1T { get; set; }
        //public FIFingerRecord R2T { get; set; }
        //public FIFingerRecord R3T { get; set; }
        //public FIFingerRecord R4T { get; set; }
        //public FIFingerRecord R5T { get; set; }

        public RecordTypeModel L1T { get; set; }
        public RecordTypeModel L2T { get; set; }
        public RecordTypeModel L3T { get; set; }
        public RecordTypeModel L4T { get; set; }
        public RecordTypeModel L5T { get; set; }

        public RecordTypeModel R1T { get; set; }
        public RecordTypeModel R2T { get; set; }
        public RecordTypeModel R3T { get; set; }
        public RecordTypeModel R4T { get; set; }
        public RecordTypeModel R5T { get; set; }

        public RecordTypeCollection(List<FIFingerRecord> records, FingerTypeModelCollection types)
        {
            this.fingerRecords = records;
            this.typeCollection = types;
            //L1T = new RecordType();
            //L2T = new RecordType();
            //L3T = new RecordType();
            //L4T = new RecordType();
            //L5T = new RecordType();

            //R1T = new RecordType();
            //R2T = new RecordType();
            //R3T = new RecordType();
            //R4T = new RecordType();
            //R5T = new RecordType();
            UpdateProperties();
        }

        private void UpdateProperties()
        {
            List<FIFingerRecord> records;
            records = fingerRecords.Where(p => p.Type.EndsWith("L")).ToList();

            compareRecords = new List<RecordTypeModel>();
            foreach (FIFingerRecord r in records)
            {
                compareRecords.Add(new RecordTypeModel()
                                   {
                                       FingerRecord = r,
                                       FingerTypeModel = TypeCollection.GetByType(r.FingerType)
                                   });
            }

            L1T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L1"));
            L2T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L2"));
            L3T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L3"));
            L4T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L4"));
            L5T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L5"));

            R1T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R1"));
            R2T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R2"));
            R3T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R3"));
            R4T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R4"));
            R5T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R5"));

            //------------------------------------------------------------------

            //L1T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L1"));
            //L2T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L2"));
            //L3T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L3"));
            //L4T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L4"));
            //L5T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L5"));

            //R1T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R1"));
            //R2T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R2"));
            //R3T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R3"));
            //R4T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R4"));
            //R5T = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R5"));

            //--------------------------------------------------------

            //FIFingerRecord record;
            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L1"));
            //L1T.FingerRecord = record;
            //L1T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L2"));
            //L2T.FingerRecord = record;
            //L2T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L3"));
            //L3T.FingerRecord = record;
            //L3T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L4"));
            //L4T.FingerRecord = record;
            //L4T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("L5"));
            //L5T.FingerRecord = record;
            //L5T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R1"));
            //R1T.FingerRecord = record;
            //R1T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R2"));
            //R2T.FingerRecord = record;
            //R2T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R3"));
            //R3T.FingerRecord = record;
            //R3T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R4"));
            //R4T .FingerRecord = record;
            //R4T.TypeValue = TypeCollection.GetByType(record.FingerType);

            //record = compareRecords.FirstOrDefault(p => p.Type.StartsWith("R5"));
            //R5T.FingerRecord = record;
            //R5T.TypeValue = TypeCollection.GetByType(record.FingerType);
        }

        public int CountContainType(string type)
        {
            return CompareRecords.Count(p => p.FingerType.Contains(type));
        }

        public int CountStartWithType(string type)
        {
            return CompareRecords.Count(p => p.FingerType.StartsWith(type));
        }

        public List<RecordTypeModel> GetListNotThumb()
        {
            return compareRecords.Where(p => !p.Type.StartsWith("L1") 
            && !p.Type.StartsWith("R1")
            ).ToList();
        }

        public List<RecordTypeModel> GetListNotContain(string type)
        {
            return compareRecords.Where(p => !p.FingerType.Contains(type)).ToList();
        }

        public List<RecordTypeModel> GetListContain(string type)
        {
            return compareRecords.Where(p => p.FingerType.Contains(type)).ToList();
        }
    }
}