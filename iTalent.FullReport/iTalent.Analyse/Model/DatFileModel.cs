﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using iTalent.Entity;

namespace iTalent.Analyse.Model
{
    public class DatFileModel
    {
        public FICustomer FICustomer { get; set; }
        public FIAgency FIAgency { get; set; }
        public List<FIFingerRecord> FingerRecords { get; set; }
    }
}