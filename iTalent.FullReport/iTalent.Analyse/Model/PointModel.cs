﻿using System;
using iTalent.Entity;

namespace iTalent.Analyse.Model
{
    public class PointModel
    {
        public Int32 ID
        {
            get { return (PointEntity != null) ? PointEntity.ID : -1; }
        }

        protected FIPoint _PointEntity;

        protected Int32 _AgentId;

        //protected string _SecKey;

        public Int32 AgentId
        {
            get { return _AgentId; }
            set { _AgentId = value; }
        }

        public string SecKey { get; set; }
        public string Version { get; set; }
        public bool IsError { get; set; }

        public int PointOriginal { get; set; }
        public int PointCurrent { get; set; }

        //public string ErrMsg { get; set; }

        //public string KeyOriginal
        //{
        //    get { return (_PointEntity == null) ? ErrMsg : _PointEntity.KeyPoint; }
        //}
        //public string KeyCurrent
        //{
        //    get { return (_PointEntity == null) ? ErrMsg : _PointEntity.KeyPointAfter; }
        //}

        public string KeyOriginal { get; set; }
        public string KeyCurrent { get; set; }

        public DateTime? PointDate
        {
            get
            {
                if (_PointEntity != null) return _PointEntity.PointDate;
                return null;
            }
        }

        public FIPoint PointEntity
        {
            get { return _PointEntity; }
            set
            {
                _PointEntity = value;
                _AgentId = (_PointEntity != null) ? _PointEntity.ID : (-1);
            }
        }
    }
}