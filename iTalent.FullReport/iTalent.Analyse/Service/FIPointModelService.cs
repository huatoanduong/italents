﻿using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFIPointModelService : IService
    {
        PointModel Find(int id);

        PointModel FindByOriginalKey(string key);
        PointModel FindByOriginalKey(string key, string version);

        FIPoint CreateEntity();
        List<PointModel> GetAll();

        List<PointModel> FindByAgency(int agencyId);
        List<PointModel> FindByAgency(int agencyId, string version);

        List<PointModel> FindByCustomer(int customerId);
        List<PointModel> FindByCustomer(int customerId, string version);

        PointModel FindFirstUnexpiredKey(int agencyId);
        PointModel FindFirstUnexpiredKey(int agencyId, string version);
        List<FIPoint> FindFirstKeys(int agencyId);

        List<PointModel> FindAllUnexpiredKey(int agencyId);
        List<PointModel> FindAllUnexpiredKey(int agencyId, string version);

        int GetNumberPointLeft(int agencyId);
        int GetNumberPointLeft(int agencyId, string version);
        //int CountAll();

        int GetNumAddKey(string key);
        int GetNumAddKey(string key, string version);

        /// <summary>
        ///     Generate and Save to Database
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="pointValue"></param>
        /// <returns></returns>
        bool GenerateNewKey(ref PointModel model);

        bool RegisterLicense(string key);
        bool RegisterLicense(string key, string version);

        bool SubtractPointValue(int pointId, int subtractValue);
        bool SubtractPointValue(int pointId, int subtractValue, string version);

        bool CheckFirstKey(int agencyId, string version);
    }

    public class FIPointModelService : Pattern.Service, IFIPointModelService
    {
        private readonly IFIAgencyRepository _agencyRepository;
        private readonly IFIPointRepository _pointRepository;
        protected readonly string GenericKeyEnc = @"h/w(s6!+crlHk~5r09+abc";

        public FIPointModelService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
            _agencyRepository = new FIAgencyRepository(unitOfWork);
            _pointRepository = new FIPointRepository(unitOfWork);
        }

        public PointModel Find(int id)
        {
            var point = _pointRepository.FindByKey(id);
            if (point == null)
            {
                return null;
            }
            return MakeModel(point);
        }

        public List<FIPoint> FindFirstKeys(int agencyId)
        {
            return _pointRepository.FindByAgency(agencyId);
        }

        public List<PointModel> FindAllUnexpiredKey(int agencyId)
        {
            var _cond = new FIPointConditionForm { AgencyID = agencyId, IsOutDate = 0 };
            return MakeModel(_pointRepository.FindByCondition(_cond));
        }

        public List<PointModel> FindAllUnexpiredKey(int agencyId, string version)
        {
            var _cond = new FIPointConditionForm { AgencyID = agencyId, IsOutDate = 0 };
            return MakeModel(_pointRepository.FindByCondition(_cond), version);
        }

        public List<PointModel> FindByAgency(int agencyId)
        {
            return MakeModel(_pointRepository.FindByAgency(agencyId));
        }

        public List<PointModel> FindByAgency(int agencyId, string version)
        {
            return MakeModel(_pointRepository.FindByAgency(agencyId), version);
        }

        public List<PointModel> FindByCustomer(int customerId)
        {
            var _cond = new FIPointConditionForm { ReportID = customerId };
            return MakeModel(_pointRepository.FindByCondition(_cond));
        }

        public List<PointModel> FindByCustomer(int customerId, string version)
        {
            var _cond = new FIPointConditionForm { ReportID = customerId };
            return MakeModel(_pointRepository.FindByCondition(_cond), version);
        }

        public PointModel FindByOriginalKey(string key)
        {
            var _cond = new FIPointConditionForm { KeyPoint = key };
            var point = _pointRepository.FindByCondition(_cond)[0];
            if (point == null)
            {
                return null;
            }
            return MakeModel(point);
        }

        public PointModel FindByOriginalKey(string key, string version)
        {
            var _cond = new FIPointConditionForm { KeyPoint = key };
            var point = _pointRepository.FindByCondition(_cond)[0];
            if (point == null)
            {
                return null;
            }
            return MakeModel(point, version);
        }

        public PointModel FindFirstUnexpiredKey(int agencyId)
        {
            var _cond = new FIPointConditionForm { AgencyID = agencyId, IsOutDate = 0 };
            return MakeModel(_pointRepository.FindByCondition(_cond)[0]);
        }

        public PointModel FindFirstUnexpiredKey(int agencyId, string version)
        {
            var _cond = new FIPointConditionForm { AgencyID = agencyId, IsOutDate = 0 };
            return MakeModel(_pointRepository.FindByCondition(_cond)[0], version);
        }

        public bool GenerateNewKey(ref PointModel model)
        {
            try
            {
                if (model == null
                    || model.AgentId <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã Đại lý không tồn tại"
                        : "This agency is not existed";
                    ThrowException(ErrMsg);
                }
                if (model == null
                    || model.PointOriginal <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cấp điểm báo cáo bằng hoặc nhỏ hơn 0"
                        : "Cannot create numeric point equal zero!";
                    ThrowException(ErrMsg);
                }

                var agency = _agencyRepository.FindByKey(model.AgentId);
                if (agency == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã Đại lý không tồn tại"
                        : "This agency is not existed";
                    ThrowException(ErrMsg);
                }
                if (string.IsNullOrEmpty(agency.SecKey))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã Đại lý trống"
                        : "the ID is not null";
                    ThrowException(ErrMsg);
                }

                var dateCreated = DateTime.Now;
                string licKey;
                string licKeyCurrent;
                if (model.Version == "")
                {
                    licKey = makeKey(agency.SecKey, dateCreated, model.PointOriginal, true);
                    licKeyCurrent = makeKey(agency.SecKey, dateCreated, model.PointOriginal, false);
                }
                else
                {
                    licKey = makeKey(agency.SecKey, dateCreated, model.PointOriginal, true, model.Version);
                    licKeyCurrent = makeKey(agency.SecKey, dateCreated, model.PointOriginal, false, model.Version);
                }

                FIPoint tmpPoint = null;

                try
                {
                    tmpPoint = _pointRepository.FindByKeyPoint(licKey);
                }
                catch
                {
                }

                if (tmpPoint != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License Key này đã tồn tại trong dữ liệu"
                        : "This licensekey was existed";
                    ThrowException(ErrMsg);
                }

                var pointEntity = new FIPoint();
                pointEntity.AgencyID = agency.ID;
                pointEntity.ReportID = 0;
                pointEntity.PointDate = dateCreated;
                pointEntity.PointType = "0";
                pointEntity.Point = model.PointOriginal;
                pointEntity.KeyPoint = licKey;
                pointEntity.KeyPointAfter = licKeyCurrent;
                pointEntity.AddedBy = agency.SecKey;
                pointEntity.IsOutDate = 0;

                var res = _pointRepository.Insert(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm mã bản quyền"
                        : "Cannot add new licensekey";
                    ThrowException(ErrMsg);
                }
                model = MakeModel(pointEntity, model.Version);
                model.PointEntity = pointEntity;
                model.PointCurrent = model.PointOriginal;
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public List<PointModel> GetAll()
        {
            return MakeModel(_pointRepository.FindAll());
        }

        public int GetNumAddKey(string key)
        {
            var num = -1;
            try
            {
                var agency = ICurrentSessionService.CurAgency;
                var keyEncrypt = GenericKeyEnc + agency.SecKey;

                string sfront, srear;
                int startIndex, endIndex;

                var key1 = Decrypt(key, keyEncrypt);

                startIndex = key1.IndexOf('!');
                endIndex = key1.IndexOf('#');
                sfront = key1.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = key1.LastIndexOf('!');
                endIndex = key1.LastIndexOf('#');
                srear = key1.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                num = int.Parse(sfront);
            }
            catch (Exception)
            {
                //throw;
            }
            return num;
        }

        public int GetNumAddKey(string key, string version)
        {
            var num = -1;
            try
            {
                var agency = ICurrentSessionService.CurAgency;
                var keyEncrypt = GenericKeyEnc + agency.SecKey + version;

                string sfront, srear;
                int startIndex, endIndex;

                var key1 = Decrypt(key.Replace(version, ""), keyEncrypt);

                startIndex = key1.IndexOf('!');
                endIndex = key1.IndexOf('#');
                sfront = key1.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = key1.LastIndexOf('!');
                endIndex = key1.LastIndexOf('#');
                srear = key1.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                num = int.Parse(sfront);
            }
            catch (Exception)
            {
                //throw;
            }
            return num;
        }

        public int GetNumberPointLeft(int agencyId)
        {
            var i = 0;

            try
            {
                var list = FindAllUnexpiredKey(agencyId);

                i = list.Sum(p => p.PointCurrent);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                i = 0;
            }

            return i;
        }

        public int GetNumberPointLeft(int agencyId, string verion)
        {
            var i = 0;

            try
            {
                var list = FindAllUnexpiredKey(agencyId);
                list = list.Where(p => p.KeyCurrent.ToString().Contains(verion)).ToList();
                i = list.Sum(p => p.PointCurrent);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                i = 0;
            }

            return i;
        }

        public bool RegisterLicense(string key)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Vui lòng nhập License Key"
                        : "Please input license key";
                    ThrowException(ErrMsg);
                }

                //FIPointConditionForm _cond = new FIPointConditionForm { KeyPoint = key };
                FIPoint tmpPoint = null;

                try
                {
                    tmpPoint = _pointRepository.FindByKeyPoint(key);
                }
                catch
                {
                }

                if (tmpPoint != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License Key này đã được nhập!"
                        : "license key has inserted in system!";
                    ThrowException(ErrMsg);
                }

                var pointEntity = parseKey(key);

                var res = _pointRepository.Insert(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm mã bản quyền"
                        : "Cannot add new licensekey";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public bool RegisterLicense(string key, string verion)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Vui lòng nhập License Key"
                        : "Please input license key";
                    ThrowException(ErrMsg);
                }

                //FIPointConditionForm _cond = new FIPointConditionForm { KeyPoint = key };
                FIPoint tmpPoint = null;

                try
                {
                    tmpPoint = _pointRepository.FindByKeyPoint(key);
                }
                catch
                {
                }

                if (tmpPoint != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License Key này đã được nhập!"
                        : "license key has inserted in system!";
                    ThrowException(ErrMsg);
                }

                var pointEntity = parseKey(key, verion);

                var res = _pointRepository.Insert(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm mã bản quyền"
                        : "Cannot add new licensekey";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public bool SubtractPointValue(int pointId, int subtractValue)
        {
            PointModel model = null;
            try
            {
                model = Find(pointId);
                if (model == null || model.IsError)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không tìm thấy license key hợp lệ trong dữ liệu"
                        : "This license key is not existed";
                    ThrowException(ErrMsg);
                }
                if (model.PointCurrent < subtractValue)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License key không còn đủ để sử dụng"
                        : "This license key is not used";
                    ThrowException(ErrMsg);
                }
                var pointAfter = model.PointCurrent - subtractValue;
                var key = makeKey(ICurrentSessionService.CurAgency.SecKey, (DateTime)model.PointDate, pointAfter, false);
                var pointEntity = model.PointEntity;
                pointEntity.KeyPointAfter = key;
                pointEntity.Point = pointAfter;
                short i = 0;
                if (pointAfter <= 0) i = 1;
                pointEntity.IsOutDate = i;

                var res = _pointRepository.Update(pointEntity);
                if (res <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin mã bản quyền"
                        : "Cannot modify FIPoint infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public bool SubtractPointValue(int pointId, int subtractValue, string version)
        {
            PointModel model = null;
            try
            {
                model = Find(pointId, version);
                if (model == null || model.IsError)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không tìm thấy license key hợp lệ trong dữ liệu"
                        : "This license key is not existed";
                    ThrowException(ErrMsg);
                }
                if (model != null && model.PointCurrent < subtractValue)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License key không còn đủ để sử dụng"
                        : "This license key is not used";
                    ThrowException(ErrMsg);
                }
                if (model != null)
                {
                    var pointAfter = model.PointCurrent - subtractValue;
                    if (model.PointDate != null)
                    {
                        var key = makeKey(ICurrentSessionService.CurAgency.SecKey, (DateTime)model.PointDate,
                            pointAfter, false, version);
                        var pointEntity = model.PointEntity;
                        pointEntity.KeyPointAfter = key;
                        pointEntity.Point = pointAfter;
                        short i = 0;
                        if (pointAfter <= 0) i = 1;
                        pointEntity.IsOutDate = i;

                        var res = _pointRepository.Update(pointEntity);
                        if (res <= 0)
                        {
                            ErrMsg = ICurrentSessionService.VietNamLanguage
                                ? "Không thể cập nhật thông tin mã bản quyền"
                                : "Cannot modify FIPoint infomartion";
                            ThrowException(ErrMsg);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        public bool CheckFirstKey(int agencyId, string version)
        {
            bool kq;
            List<FIPoint> lstPoints = FindFirstKeys(agencyId);
            if (lstPoints == null) kq = true;
            else
            {
                lstPoints = lstPoints.Where(s => s.KeyPoint.Contains(version)).ToList();
                kq = lstPoints.Count == 0;
            }
            return kq;
        }

        FIPoint IFIPointModelService.CreateEntity()
        {
            var obj = new FIPoint
            {
                KeyPoint = "",
                ID = 0,
                AddedBy = ICurrentSessionService.CurAgency.SecKey,
                AgencyID = 0,
                Point = 0,
                IsOutDate = 0,
                KeyPointAfter = "",
                PointDate = DateTimeUtil.GetCurrentTime(),
                PointType = "0"
            };
            return obj;
        }

        /// <summary>
        ///     Use internal, not throw Exception
        /// </summary>
        /// <param name="pointEntity"></param>
        /// <returns></returns>
        protected PointModel MakeModel(FIPoint pointEntity)
        {
            PointModel model = null;
            try
            {
                model = makeModel(pointEntity);
            }
            catch (Exception exception)
            {
                var obj = _agencyRepository.FindByKey(pointEntity.AgencyID);
                model = new PointModel
                {
                    SecKey = obj.SecKey,
                    PointEntity = pointEntity,
                    IsError = true,
                    Version = ""
                };
                //model.ErrMsg = exception.Message;
                model.PointCurrent = model.PointOriginal = -1;
                model.KeyOriginal = model.KeyCurrent = exception.Message;
            }
            return model;
        }

        protected PointModel MakeModel(FIPoint pointEntity, string version)
        {
            PointModel model = null;
            try
            {
                model = makeModel(pointEntity, version);
            }
            catch (Exception exception)
            {
                var obj = _agencyRepository.FindByKey(pointEntity.AgencyID);
                model = new PointModel
                {
                    SecKey = obj.SecKey,
                    PointEntity = pointEntity,
                    IsError = true,
                    Version = version
                };
                //model.ErrMsg = exception.Message;
                model.PointCurrent = model.PointOriginal = -1;
                model.KeyOriginal = model.KeyCurrent = exception.Message;
            }
            return model;
        }

        protected List<PointModel> MakeModel(List<FIPoint> pointEntities)
        {
            var list = new List<PointModel>();
            PointModel model;

            if (pointEntities != null)
            {
                foreach (var point in pointEntities)
                {
                    model = MakeModel(point);
                    list.Add(model);
                }
            }

            return list;
        }

        protected List<PointModel> MakeModel(List<FIPoint> pointEntities, string version)
        {
            var list = new List<PointModel>();
            PointModel model;

            if (pointEntities != null)
            {
                foreach (var point in pointEntities)
                {
                    model = MakeModel(point, version);
                    list.Add(model);
                }
            }

            return list;
        }

        protected PointModel makeModel(FIPoint pointEntity)
        {
            PointModel model = null;

            try
            {
                var keyOrigin = pointEntity.KeyPoint;
                var keyCurrent = pointEntity.KeyPointAfter;
                string keyEncrypt;
                var obj = _agencyRepository.FindByKey(pointEntity.AgencyID);
                var version = "";
                if (pointEntity.AgencyID > 0)
                {
                    if (pointEntity.KeyPoint.Contains("SIMPLE"))
                        version = "SIMPLE";
                    else if (pointEntity.KeyPoint.Contains("PREMIUM"))
                        version = "PREMIUM";
                    else if (pointEntity.KeyPoint.Contains("VIP"))
                        version = "VIP";
                    else if (pointEntity.KeyPoint.Contains("ULTIMATE"))
                        version = "ULTIMATE";
                    else
                        version = "";

                    keyEncrypt = GenericKeyEnc + obj.SecKey + version;
                }
                else
                {
                    keyEncrypt = GenericKeyEnc; //ICurrentSessionService.Seckey;
                }

                string sfront, srear;
                int startIndex, endIndex;
                int pointOrigin;
                int pointCurrent;

                keyOrigin = Decrypt(keyOrigin.Replace(version, ""), keyEncrypt);
                keyCurrent = Decrypt(keyCurrent.Replace(version, ""), keyEncrypt);

                if (!keyOrigin.EndsWith("#00")
                    || !keyCurrent.EndsWith("#01"))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "không thể xác định key."
                        : "The key is not modify";
                    ThrowException(ErrMsg);
                }

                startIndex = keyOrigin.IndexOf('!');
                endIndex = keyOrigin.IndexOf('#');
                sfront = keyOrigin.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyOrigin.LastIndexOf('!');
                endIndex = keyOrigin.LastIndexOf('#');
                srear = keyOrigin.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                pointOrigin = int.Parse(sfront);

                startIndex = keyCurrent.IndexOf('!');
                endIndex = keyCurrent.IndexOf('#');
                sfront = keyCurrent.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyCurrent.LastIndexOf('!');
                endIndex = keyCurrent.LastIndexOf('#');
                srear = keyCurrent.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                pointCurrent = int.Parse(sfront);

                model = new PointModel
                {
                    IsError = false,
                    PointEntity = pointEntity,
                    PointCurrent = pointCurrent,
                    PointOriginal = pointOrigin,
                    KeyOriginal = pointEntity.KeyPoint,
                    KeyCurrent = pointEntity.KeyPointAfter
                };
                //if (pointEntity.PointDate != null) model.PointDate = pointEntity.PointDate;
            }
            catch (Exception ex)
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage
                    ? "Mã bản quyền không hợp lệ."
                    : " the key is not valid!";
                ThrowException(ErrMsg);
            }
            return model;
        }

        protected PointModel makeModel(FIPoint pointEntity, string version)
        {
            PointModel model = null;

            try
            {
                var keyOrigin = pointEntity.KeyPoint.Replace(version, "");
                var keyCurrent = pointEntity.KeyPointAfter.Replace(version, "");
                string keyEncrypt;
                var obj = _agencyRepository.FindByKey(pointEntity.AgencyID);

                if (pointEntity.AgencyID > 0)
                    keyEncrypt = GenericKeyEnc + obj.SecKey + version;
                else
                {
                    keyEncrypt = GenericKeyEnc + version; //ICurrentSessionService.Seckey;
                }

                string sfront, srear;
                int startIndex, endIndex;
                int pointOrigin;
                int pointCurrent;

                keyOrigin = Decrypt(keyOrigin, keyEncrypt);
                keyCurrent = Decrypt(keyCurrent, keyEncrypt);

                if (!keyOrigin.EndsWith("#00")
                    || !keyCurrent.EndsWith("#01"))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "không thể xác định key."
                        : "The key is not modify";
                    ThrowException(ErrMsg);
                }

                startIndex = keyOrigin.IndexOf('!');
                endIndex = keyOrigin.IndexOf('#');
                sfront = keyOrigin.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyOrigin.LastIndexOf('!');
                endIndex = keyOrigin.LastIndexOf('#');
                srear = keyOrigin.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                pointOrigin = int.Parse(sfront);

                startIndex = keyCurrent.IndexOf('!');
                endIndex = keyCurrent.IndexOf('#');
                sfront = keyCurrent.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyCurrent.LastIndexOf('!');
                endIndex = keyCurrent.LastIndexOf('#');
                srear = keyCurrent.Substring(startIndex + 1, endIndex - startIndex - 1);
                if (sfront != srear)
                {
                    throw new Exception();
                }
                pointCurrent = int.Parse(sfront);

                model = new PointModel
                {
                    IsError = false,
                    PointEntity = pointEntity,
                    PointCurrent = pointCurrent,
                    PointOriginal = pointOrigin,
                    KeyOriginal = version + pointEntity.KeyPoint,
                    KeyCurrent = version + pointEntity.KeyPointAfter,
                    Version = version
                };
                //if (pointEntity.PointDate != null) model.PointDate = pointEntity.PointDate;
            }
            catch (Exception ex)
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage
                    ? "Mã bản quyền không hợp lệ."
                    : " the key is not valid!";
                ThrowException(ErrMsg);
            }
            return model;
        }

        protected string makeKey(string agentCode, DateTime dateMade, int point, bool isOriginalKey)
        {
            string key = null;

            var keyEncrypt = GenericKeyEnc + agentCode;
            var strPoint = point.ToString();
            key = "!" + strPoint + "#"
                  + agentCode
                  + dateMade.ToString("yyMMddHHmmss")
                  + "!" + strPoint + "#";
            if (isOriginalKey)
            {
                key += "00";
            }
            else
            {
                key += "01";
            }

            key = Encrypt(key, keyEncrypt);

            return key;
        }

        protected string makeKey(string agentCode, DateTime dateMade, int point, bool isOriginalKey, string version)
        {
            string key = null;

            var keyEncrypt = GenericKeyEnc + agentCode + version;
            var strPoint = point.ToString();
            key = "!" + strPoint + "#"
                  + agentCode
                  + dateMade.ToString("yyMMddHHmmss")
                  + "!" + strPoint + "#";
            if (isOriginalKey)
            {
                key += "00";
            }
            else
            {
                key += "01";
            }

            key = version + Encrypt(key, keyEncrypt);

            return key;
        }

        protected FIPoint parseKey(string key)
        {
            FIPoint obj = null;
            try
            {
                var agency = ICurrentSessionService.CurAgency;
                //string keyEncrypt = this.GenericKeyEnc + agency.Username;
                var keyEncrypt = GenericKeyEnc + agency.SecKey;

                string sfront, srear;
                int startIndex, endIndex;
                int pointValue;

                var keyDecrypt = Decrypt(key, keyEncrypt);

                startIndex = keyDecrypt.IndexOf('!');
                endIndex = keyDecrypt.IndexOf('#');
                sfront = keyDecrypt.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyDecrypt.LastIndexOf('!');
                endIndex = keyDecrypt.LastIndexOf('#');
                srear = keyDecrypt.Substring(startIndex + 1, endIndex - startIndex - 1);

                if (sfront != srear
                    || !keyDecrypt.EndsWith("#00"))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License Key trống"
                        : "License key null";
                    ThrowException(ErrMsg);
                }
                pointValue = int.Parse(sfront);
                var keyCurrent = keyDecrypt.Substring(0, keyDecrypt.Length - 3);
                keyCurrent += "#01";
                keyCurrent = Encrypt(keyCurrent, keyEncrypt);

                obj = new FIPoint();
                obj.AgencyID = agency.ID;
                obj.ReportID = 0;
                obj.PointDate = DateTime.Now;
                obj.PointType = "0";
                obj.Point = pointValue;
                obj.KeyPoint = key;
                obj.KeyPointAfter = keyCurrent;
                obj.AddedBy = agency.SecKey;
                short i = 0;
                if (pointValue <= 0) i = 1;
                obj.IsOutDate = i;
            }
            catch
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage
                    ? "License Key không hợp lệ"
                    : "This license key is not existed";
                ThrowException(ErrMsg);
            }
            return obj;
        }

        protected FIPoint parseKey(string key, string version)
        {
            FIPoint obj = null;
            try
            {
                var agency = ICurrentSessionService.CurAgency;
                //string keyEncrypt = this.GenericKeyEnc + agency.Username;
                var keyEncrypt = GenericKeyEnc + agency.SecKey + version;

                string sfront, srear;
                int startIndex, endIndex;
                int pointValue;

                var keyDecrypt = Decrypt(key.Replace(version, ""), keyEncrypt);

                startIndex = keyDecrypt.IndexOf('!');
                endIndex = keyDecrypt.IndexOf('#');
                sfront = keyDecrypt.Substring(startIndex + 1, endIndex - startIndex - 1);
                startIndex = keyDecrypt.LastIndexOf('!');
                endIndex = keyDecrypt.LastIndexOf('#');
                srear = keyDecrypt.Substring(startIndex + 1, endIndex - startIndex - 1);

                if (sfront != srear
                    || !keyDecrypt.EndsWith("#00"))
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License Key trống"
                        : "License key null";
                    ThrowException(ErrMsg);
                }
                pointValue = int.Parse(sfront);
                var keyCurrent = keyDecrypt.Substring(0, keyDecrypt.Length - 3);
                keyCurrent += "#01";
                keyCurrent = version + Encrypt(keyCurrent, keyEncrypt);

                obj = new FIPoint();
                obj.AgencyID = agency.ID;
                obj.ReportID = 0;
                obj.PointDate = DateTime.Now;
                obj.PointType = "0";
                obj.Point = pointValue;
                obj.KeyPoint = key;
                obj.KeyPointAfter = keyCurrent;
                obj.AddedBy = agency.SecKey;
                short i = 0;
                if (pointValue <= 0) i = 1;
                obj.IsOutDate = i;
            }
            catch
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage
                    ? "License Key không hợp lệ"
                    : "This license key is not existed";
                ThrowException(ErrMsg);
            }
            return obj;
        }

        protected string Encrypt(string message, string keyEncrypt)
        {
            return Security.Encrypt(message, keyEncrypt);
        }

        protected string Decrypt(string message, string keyEncrypt)
        {
            return Security.Decrypt(message, keyEncrypt);
        }

        //public int CountAll()
        //{
        //    return _customerRepository.Count();
        //}

        public FIPointModelService CreateEntity()
        {
            throw new NotImplementedException();
        }

        public PointModel Find(int id, string version)
        {
            var point = _pointRepository.FindByKey(id);
            if (point == null)
            {
                return null;
            }
            return MakeModel(point, version);
        }

        public List<PointModel> GetAll(string version)
        {
            return MakeModel(_pointRepository.FindAll(), version);
        }

        protected override void InitRepositories()
        {
        }
    }
}
