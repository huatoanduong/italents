﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml.Drawing.Charts;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Entity;
using iTalent.Utils;

namespace iTalent.Analyse.Service
{
    public interface IFICustomerService : IService
    {
        bool Add(FICustomer objCustomer);
        bool Update(FICustomer objCustomer);
        bool Delete(FICustomer objCustomer);

        bool ImportFromDatFile(DatFileModel datFileModel);

        List<FICustomer> Search(string textSearch);
        List<FICustomer> SearchByReportId(string textSearch);
        List<FICustomer> SearchByName(string textSearch);
        List<FICustomer> SearchByParent(string textSearch);
        List<FICustomer> SearchByTel(string textSearch);
        List<FICustomer> SearchByAddress(string textSearch);
        List<FICustomer> Search(DateTime fromDate, DateTime toDate);
        List<FICustomer> Search(string textSearch, DateTime fromDate, DateTime toDate);
        
        FICustomer Find(Int32 id);
        FICustomer FindByName(string name);
        List<FICustomer> FindByAgency(int agencyId);
        FICustomer FindByReportId(string reportId);
        List<FICustomer> LoadAll();
        FICustomer CreateEntity();
    }

    public class FICustomerService : Pattern.Service, IFICustomerService //BaseService,
    {
        public FICustomerService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {

        }

        public FICustomer CreateEntity()
        {
            var obj = new FICustomer
                      {
                          ID = 0,
                          DOB = DateTime.Now.AddYears(-20),
                          Country = "Việt Nam",
                          Date = DateTime.Now,
                          Gender = "None",
                          Mobile = "",
                          Tel = "",
                          Parent = "",
                          Address1 = "",
                          Email = "",
                          Remark = "",
                          ZipPosTalCode = "",
                          State = "None",
                          City = "",
                          ReportID = "",
                          Name = ""
                      };

            return obj;
        }


        protected string writeDatFile(DatFileModel model)
        {
            string result = "";

            return result;
        }

        public bool Add(FICustomer obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                FICustomer obj2 = repository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã khách hàng này đã tồn tại"
                        : "This customer is already existed";
                    ThrowException(ErrMsg);
                }

                obj.ID = 0;
                if (obj.ReportID == "")
                    obj.ReportID = ICurrentSessionService.CurAgency.SecKey + DateTime.Now.ToString("ddMMyyyyHHmmss");
                obj.Date = DateTimeUtil.GetCurrentTime();
                if (ICurrentSessionService.VietNamLanguage)
                {
                    if (obj.Gender.Contains("Male"))
                        obj.Gender = "Nam";
                    if (obj.Gender.Contains("Female"))
                        obj.Gender = "Nữ";
                }
                else
                {
                    if (obj.Gender.Contains("Nam"))
                        obj.Gender = "Male";
                    if (obj.Gender.Contains("Nữ"))
                        obj.Gender = "Female";
                }
                EnsureObjProperties(obj, false);

                int resCount = repository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm khách hàng"
                        : "The key is not valid";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FICustomer obj)
        {
            bool res = true;
            try
            {
                EnsureObjProperties(obj, true);
                _unitOfWork.OpenConnection();
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                FICustomer obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xác định được khách hàng hoặc không tồn tại khách hàng này!"
                        : "Customer's information dosen't exist!";
                    ThrowException(ErrMsg);
                }

                obj.Date = DateTimeUtil.GetCurrentTime();
                if (ICurrentSessionService.VietNamLanguage)
                {
                    if (obj.Gender.Contains("Male"))
                        obj.Gender = "Nam";
                    if (obj.Gender.Contains("Female"))
                        obj.Gender = "Nữ";
                }
                else
                {
                    if (obj.Gender.Contains("Nam"))
                        obj.Gender = "Male";
                    if (obj.Gender.Contains("Nữ"))
                        obj.Gender = "Female";
                }
                //EnsureObjProperties(obj, false);

                int resCount = repository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật khách hàng"
                        : "Cannot update this customer";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FICustomer obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                FICustomer obj2 = repository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xác định được khách hàng hoặc không tồn tại khách hàng này!"
                        : "Customer's information dosen't exist!";
                    ThrowException(ErrMsg);
                }

                int resCount = repository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa khách hàng"
                        : "Cannot remove customer";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool ImportFromDatFile(DatFileModel datFileModel)
        {
            bool res = true;
            try
            {
                FIAgency agency = datFileModel.FIAgency;
                FICustomer customer = datFileModel.FICustomer;
                List<FIFingerRecord> fingerRecords = datFileModel.FingerRecords;

                IFIAgencyRepository agencyRepository = new FIAgencyRepository(this._unitOfWork);
                IFICustomerRepository customerRepository = new FICustomerRepository(this._unitOfWork);
                IFIFingerRecordRepository recordRepository = new FIFingerRecordRepository(this._unitOfWork);

                FIAgency agency2 = agencyRepository.FindBySeckey(agency.SecKey);
                setDefaultValue(agency);
                if (agency2 == null)
                {
                    agency = ICurrentSessionService.CurAgency;
                }

                if (customer.ReportID == "")
                    customer.ReportID = agency.SecKey + DateTime.Now.ToString("ddMMyyyyHHmmss");

                //agencyRepository.Update(agency);

                FICustomer customer2 = customerRepository.FindByReportId(customer.ReportID);
                setDefaultValue(customer);
                customer.AgencyID = agency.ID;
                if (customer2 == null)
                {
                    customerRepository.Insert(customer);
                }
                else
                {
                    customer.ID = customer2.ID;
                    customerRepository.Update(customer);
                }
                customer = customerRepository.FindByReportId(customer.ReportID);
                int reportId = customer.ID;
                List<FIFingerRecord> fingerRecords2 = recordRepository.FindByRecordId(reportId);
                
                //If there is no fingerRecord in database
                if (fingerRecords2 == null || fingerRecords2.Count == 0)
                {
                    for (int i = 0; i < fingerRecords.Count; i++)
                    {
                        fingerRecords[i].ReportID = reportId;
                        recordRepository.Insert(fingerRecords[i]);
                    }
                }
                //If exist in database and have to have 22 record exactly
                else
                {
                    if (fingerRecords2.Count == 22 && fingerRecords.Count == 22)
                    {
                        for (int i = 0; i < 22; i++)
                        {
                            fingerRecords[i].ID = fingerRecords2[i].ID;
                            recordRepository.Update(fingerRecords[i]);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }

        public List<FICustomer> Search(string textSearch)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where Name like '%" + textSearch +
                                               "%' OR ID = " + textSearch + " OR Tel like '%" + textSearch +
                                               "%' OR Parent like '%" + textSearch + "%' OR Address1 like '%" +
                                               textSearch + "%' OR DOB like '%" + textSearch + "%'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> SearchByReportId(string textSearch)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where ReportID Like '%" + textSearch + "%'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> SearchByName(string textSearch)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where Name Like '%" + textSearch + "%'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> SearchByParent(string textSearch)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where Parent Like '%" + textSearch + "%'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> SearchByTel(string textSearch)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where Tel Like '%" + textSearch + "%'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> SearchByAddress(string textSearch)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where Address1 Like '%" + textSearch + "%'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> Search(DateTime fromDate, DateTime toDate)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where Date >= '" + fromDate + "' AND Date <= '" +
                                               toDate + "'");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public List<FICustomer> Search(string textSearch, DateTime fromDate, DateTime toDate)
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer =
                    repository.FindListByQuery("Select * from FICustomer where (Date >= '" + fromDate +
                                               "' AND Date <= '" + toDate + "') and (Name like '%" + textSearch +
                                               "%' OR ID = " + textSearch + " OR Tel like '%" + textSearch +
                                               "%' OR Parent like '%" + textSearch + "%' OR Address1 like '%" +
                                               textSearch + "%' OR DOB like '%" + textSearch + "%')");
                if (lstCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }
        
        public List<FICustomer> FindByAgency(int agencyId)
        {
            List<FICustomer> lst = null;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                FICustomerConditionForm condition = new FICustomerConditionForm();
                condition.AgencyID = agencyId;
                lst = repository.FindByCondition(condition);
                if (lst == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lst = null;
            }
            return lst;
        }

        public List<FICustomer> LoadAll()
        {
            List<FICustomer> lstCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                lstCustomer = repository.FindAll();
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                lstCustomer = null;
            }
            return lstCustomer;
        }

        public FICustomer Find(int id)
        {
            FICustomer currCustomer;
            try
            {
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                currCustomer = repository.FindByKey(id);
                if (currCustomer == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }

            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                currCustomer = null;
            }
            return currCustomer;
        }

        public FICustomer FindByName(string name)
        {
            FICustomer currCustomer = null;
            try
            {
                List<FICustomer> lst = new List<FICustomer>();
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                FICustomerConditionForm condition = new FICustomerConditionForm();
                condition.Name = name;
                lst = repository.FindByCondition(condition);
                if (lst == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Khách hàng cần tìm không tồn tại."
                        : "Not found.";
                    ThrowException(ErrMsg);
                }
                else
                {
                    currCustomer = lst[0];
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                currCustomer = null;
            }
            return currCustomer;
        }

        public FICustomer FindByReportId(string reportId)
        {
            try
            {
                FICustomer customer;
                IFICustomerRepository repository = new FICustomerRepository(this._unitOfWork);
                customer = repository.FindByReportId(reportId);
                return customer;
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return null;
            }
        }

        public bool EnsureObjProperties(FICustomer obj, bool isUpdate)
        {
            if (string.IsNullOrWhiteSpace(obj.Name))
            {
                base.ThrowException(ICurrentSessionService.VietNamLanguage
                    ? @"Vui lòng nhập tên khách hàng!"
                    : @"Please input name customer!");
            }
            if (string.IsNullOrWhiteSpace(obj.Email))
            {
                base.ThrowException(ICurrentSessionService.VietNamLanguage
                    ? @"Vui lòng nhập email!"
                    : @"Please input email customer!");
            }
            if (string.IsNullOrWhiteSpace(obj.Tel))
            {
                base.ThrowException(ICurrentSessionService.VietNamLanguage
                    ? @"Vui lòng nhập số điện thoại!"
                    : @"Please input telephone customer!");
            }
            if (string.IsNullOrWhiteSpace(obj.ReportID))
            {
                base.ThrowException(ICurrentSessionService.VietNamLanguage
                    ? @"Mã report rỗng!"
                    : @"ID report is empty!");
            }

            var idGreater0 = obj.ID > 0;

            if (isUpdate != idGreater0) // XOR here
            {
                base.ThrowException(ICurrentSessionService.VietNamLanguage
                    ? @"Không thể xác định được khách hàng hoặc không tồn tại khách hàng này!"
                    : @"Customer's information dosen't exists!");
            }
            IFIAgencyRepository repository = new FIAgencyRepository(this._unitOfWork);
            var exist = repository.FindByKey((int) obj.AgencyID);
            if (exist == null)
            {
                base.ThrowException(ICurrentSessionService.VietNamLanguage
                    ? @"Không thể xác định được đại lý hoặc không tồn tại đại lý này!"
                    : @"Agency's information dosen't exists!");
            }

            //var a = _implement.FindByReportId(obj.ReportID);
            var a = FindByReportId(obj.ReportID);
            if (a != null)
            {
                if (!isUpdate)
                {
                    base.ThrowException(ICurrentSessionService.VietNamLanguage
                        ? @"Mã Report của khách hàng này đã có trong hệ thống!"
                        : @"Customer's information was exists in systems!");
                }
            }

            if (!isUpdate)
            {
                //var b = _implement.FindByReportId(obj.ReportID);
                var b = FindByReportId(obj.ReportID);
                if (b != null)
                {
                    base.ThrowException(ICurrentSessionService.VietNamLanguage
                        ? @"Mã Report của khách hàng này đã có trong hệ thống!"
                        : @"Customer's information was exists in systems!");
                }
            }

            return true;
        }

        protected void setDefaultValue(FICustomer customer)
        {
            customer.Gender = customer.Gender.ToDefaultIfBlank("Male");
            customer.Parent = customer.Parent.ToDefaultIfBlank();
            customer.Tel = customer.Tel.ToDefaultIfBlank();
            customer.Mobile = customer.Mobile.ToDefaultIfBlank();
            customer.Email = customer.Email.ToDefaultIfBlank();
            customer.Address1 = customer.Address1.ToDefaultIfBlank();
            customer.Address2 = customer.Address2.ToDefaultIfBlank();
            customer.City = customer.City.ToDefaultIfBlank();
            customer.State = customer.State.ToDefaultIfBlank();
            customer.ZipPosTalCode = customer.ZipPosTalCode.ToDefaultIfBlank();
            customer.Country = customer.Country.ToDefaultIfBlank();
            customer.Remark = customer.Remark.ToDefaultIfBlank();
        }

        protected void setDefaultValue(FIAgency agency)
        {
            agency.Gender = agency.Gender.ToDefaultIfBlank("Male");
            agency.Address1 = agency.Address1.ToDefaultIfBlank();
            agency.Address2 = agency.Address2.ToDefaultIfBlank();
            agency.City = agency.City.ToDefaultIfBlank();
            agency.State = agency.State.ToDefaultIfBlank();
            agency.PostalCode = agency.PostalCode.ToDefaultIfBlank();
            agency.Country = agency.Country.ToDefaultIfBlank();
            agency.PhoneNo = agency.PhoneNo.ToDefaultIfBlank();
            agency.MobileNo = agency.MobileNo.ToDefaultIfBlank();
            agency.Email = agency.Email.ToDefaultIfBlank();
        }
    }
}
