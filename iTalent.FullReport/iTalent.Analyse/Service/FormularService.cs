﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using iTalent.Analyse.Model;
using iTalent.Entity;
using iTalent.Utils;

namespace iTalent.Analyse.Service
{
    public interface IFormularService
    {
        void CalculateRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
            List<FIFingerType> fingerTypes);
    }

    public class FormularService : IFormularService
    {
        protected void calculateFingerRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
            List<FIFingerType> fingerTypes, out FingerTypeModelCollection fingerTypeModel)
        {
            fingerTypeModel = new FingerTypeModelCollection(fingerTypes);
            RecordTypeCollection records = new RecordTypeCollection(fingerRecords, fingerTypeModel);

            if (records.CountStartWithType("W") == 10)
            {
                foreach (RecordTypeModel record in records.CompareRecords)
                {
                    if (record.FingerType.StartsWith("LF"))
                    {
                        fingerTypeModel.NhomLF.Value += 10;
                    }
                    record.FingerTypeModel.Value += 10;
                }
                return;
            }

            decimal totalPercent = 100;
            int totalFinger = 10;
            decimal percentPerFinger;

            bool isWLeft = records.L1T.FingerType.StartsWith("W");
            bool isWRight = records.R1T.FingerType.StartsWith("W");
            List<RecordTypeModel> notThumbList = records.GetListNotThumb();

            if (isWLeft ^ isWRight)
            {
                if (isWLeft)
                {
                    records.L1T.FingerTypeModel.Value += 26;

                    if (records.R1T.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += 10 / 2;
                        fingerTypeModel.CaHeo.Value += 10 / 2;
                    }
                    else
                    {
                        if(records.R1T.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += 10;
                        }
                        records.R1T.FingerTypeModel.Value += 10;
                    }
                }
                if (isWRight)
                {
                    records.R1T.FingerTypeModel.Value += 26;
                    if (records.L1T.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += 10 / 2;
                        fingerTypeModel.CaHeo.Value += 10 / 2;
                    }
                    else
                    {
                        if (records.L1T.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += 10;
                        }
                        records.L1T.FingerTypeModel.Value += 10;
                    }
                }

                percentPerFinger = 8;
                foreach (RecordTypeModel model in notThumbList)
                {
                    if (model.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += percentPerFinger / 2;
                        fingerTypeModel.CaHeo.Value += percentPerFinger / 2;
                    }
                    else
                    {
                        if (model.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += percentPerFinger;
                        }
                        model.FingerTypeModel.Value += percentPerFinger;
                    }
                }
                return;
            }
            else if (isWLeft && isWRight)
            {
                records.L1T.FingerTypeModel.Value += 30;
                records.R1T.FingerTypeModel.Value += 30;

                percentPerFinger = 5;
                foreach (RecordTypeModel model in notThumbList)
                {
                    if (model.FingerType.StartsWith("AR"))
                    {
                        fingerTypeModel.ConCu.Value += percentPerFinger / 2;
                        fingerTypeModel.CaHeo.Value += percentPerFinger / 2;
                    }
                    else
                    {
                        if (model.FingerType.StartsWith("LF"))
                        {
                            fingerTypeModel.NhomLF.Value += percentPerFinger;
                        }
                        model.FingerTypeModel.Value += percentPerFinger;
                    }
                }
                return;
            }

            percentPerFinger = 10;
            foreach (RecordTypeModel model in records.CompareRecords)
            {
                if (model.FingerType.StartsWith("AR"))
                {
                    fingerTypeModel.ConCu.Value += percentPerFinger / 2;
                    fingerTypeModel.CaHeo.Value += percentPerFinger / 2;
                }
                else
                {
                    if (model.FingerType.StartsWith("LF"))
                    {
                        fingerTypeModel.NhomLF.Value += percentPerFinger;
                    }
                    model.FingerTypeModel.Value += percentPerFinger;
                }
            }
            return;
        }

        //protected bool isContainStartWith(List<FIFingerRecord> records, string startString)
        //{
        //    foreach (FIFingerRecord record in records)
        //    {
        //        if (record.FingerType.StartsWith(startString))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //protected int getFingerTypeIndex(string value, List<FIFingerType> types)
        //{
        //    for (int i = 0; i < types.Count; i++)
        //    {
        //        if (!value.IsBlank() && types[i].Types.Contains(value)) { return i; }
        //    }
        //    return -1;
        //}

        protected void updateFingerAnalyse(FIFingerAnalysis fingerAnalysis,
            FingerTypeModelCollection fingerTypeModelCollection)
        {
            //fingerAnalysis.FPType1 = value[0];
            //fingerAnalysis.FPType2 = value[1];
            //fingerAnalysis.FPType3 = value[2];
            //fingerAnalysis.FPType4 = value[3];
            //fingerAnalysis.FPType5 = value[4];
            //fingerAnalysis.FPType6 = value[5];
            //fingerAnalysis.FPType7 = value[6];
            //fingerAnalysis.FPType8 = value[7];

            fingerTypeModelCollection.MapFingerAnalyse(fingerAnalysis);
        }

        public void CalculateRecord(FIFingerAnalysis analysis, List<FIFingerRecord> fingerRecords,
            List<FIFingerType> fingerTypes)
        {
            FingerTypeModelCollection fingerTypeModelCollection;
            calculateFingerRecord(analysis, fingerRecords, fingerTypes, out fingerTypeModelCollection);
            updateFingerAnalyse(analysis, fingerTypeModelCollection);
        }
    }
}