﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Formula.Model;
using iTalent.Formula.Model.ExcelTemplate;
using iTalent.Utils;
using Microsoft.VisualBasic;

namespace iTalent.Analyse.Service
{
    public interface IExcelTemplateService : IErrorProvider
    {
        void EncryptFile(string inputFile, string outputFile);
        void DecryptFile(string inputFile, string outputFile);
        ExcelNGFValueCollection ReadNGFFromExcel(string filePath);
        ExcelHeSoTiemNangCollection ReadHeSoTiemNangFromExcel(string filePath);
        ExcelDacTinhTiemThucCollection ReadDacTinhTiemThucFromExcel(string filePath);
        ExcelDacTinhTuDuyCollection ReadDacTinhTuDuyFromExcel(string filePath);
        ExcelKhuyenNghiCollection ReadKhuyenNghiFromExcel(string filePath);
        ExcelTinhCachHanhViCollection ReadTinhCachHanhViFromExcel(string filePath);
        ExcelTop3In16Collection ReadTop3In16FromExcel(string filePath);
    }

    public class ExcelTemplateService : ErrorProvider, IExcelTemplateService
    {
        public string KeyEncrypt { get { return "<ew?<70jI]9Tj[V"; } }

        private string TempFilename;

        public ExcelTemplateService()
        {

        }

        public void EncryptFile(string inputFile, string outputFile)
        {
            FileEncryptorUtil.EncryptFile(inputFile, outputFile, KeyEncrypt);
        }

        public void DecryptFile(string inputFile, string outputFile)
        {
            FileEncryptorUtil.DecryptFile(inputFile, outputFile, KeyEncrypt);
        }

        private string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private string GetRandomFileName(string directory)
        {
            string fileName;
            do
            {
                fileName = Path.Combine(directory, RandomString(10) + ".tmp");
            } while (File.Exists(fileName));
            return fileName;
        }

        private void MakeDecryptedTempFile(string filePath, string type)
        {
            if (!File.Exists(filePath))
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage
                    ? "Template " + type + " không tồn tại"
                    : "Template file" + type + " does not exist";
                ThrowException(ErrMsg);
            }

            //string directory = Path.GetDirectoryName(filePath);
            string directory = ICurrentSessionService.DesDirNameTemp;
            TempFilename = GetRandomFileName(directory);

            try
            {
                DecryptFile(filePath, TempFilename);
                //File.Copy(filePath, TempFilename);
            }
            catch
            {
                ErrMsg = ICurrentSessionService.VietNamLanguage ? "Template " + type + " không đúng định dạng" : "Decrypt Template" + type + " is not in correct format.";
                ThrowNotCorrectFormatException(ErrMsg);
            }
        }

        private void DeleteDecryptedTempFile()
        {
            if (File.Exists(TempFilename))
            {
                File.Delete(TempFilename);
            }
        }

        private void ThrowNotCorrectFormatException(string ErrMsg)
        {
            //ErrMsg = ICurrentSessionService.VietNamLanguage
            //    ? "Template không đúng định dạng"
            //    : "Template is not in correct format";
            ThrowException(ErrMsg);
        }

        public ExcelNGFValueCollection ReadNGFFromExcel(string filePath)
        {
            ExcelNGFValueCollection collection = null;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "NGF" : "NGF-factor");
                collection = new ExcelNGFValueCollection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template hệ số NGF không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File NGF Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template NGF không đúng định dạng" : "Template is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }

                int columnCount = dt.Columns.Count;
                int rowCount = dt.Rows.Count;
                string fingerName;
                string fingerType;
                ExcelNGFValue value;
                for (int r = 0; r < rowCount; r++)
                {
                    fingerName = dt.Rows[r][0].ToString();
                    for (int c = 1; c < columnCount; c++)
                    {
                        fingerType = dt.Columns[c].ColumnName;
                        value = collection.FindByTypeAndName(fingerType, fingerName);
                        if (value == null)
                        {
                            value = new ExcelNGFValue(collection);
                            collection.Add(value);
                        }
                        value.FingerType = fingerType;
                        value.FingerName = fingerName;
                        value.Value = dt.Rows[r][c].ToString().ToDecimalOrDefault();
                        //value.Value = (value.Value - 1) / 10;
                    }
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }

        public ExcelHeSoTiemNangCollection ReadHeSoTiemNangFromExcel(string filePath)
        {
            ExcelHeSoTiemNangCollection collection = null;
            int RowError = -1;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "Hệ số tiềm năng" : "Potential-factor");
                collection = new ExcelHeSoTiemNangCollection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template hệ số tiềm năng không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File Potential-factor Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template hệ số tiềm năng không đúng định dạng" : "Template is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }

                int columnCount = dt.Columns.Count;
                int rowCount = dt.Rows.Count;
                string fingerName;
                string fingerType;
                ExcelHeSoTiemNangModel value;
                for (int r = 0; r < rowCount; r++)
                {
                    fingerName = dt.Rows[r][0].ToString().Trim();
                    value = collection.FindByName(fingerName);
                    if (value == null)
                    {
                        value = new ExcelHeSoTiemNangModel(collection);
                        collection.Add(value);
                    }
                    value.FingerName = fingerName;
                    value.MoTaNgan = dt.Rows[r][1].ToString();
                    value.MoTaDai = dt.Rows[r][2].ToString();
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }

        public  ExcelDacTinhTiemThucCollection ReadDacTinhTiemThucFromExcel(string filePath)
        {
            ExcelDacTinhTiemThucCollection collection = null;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "Đặc tính tiềm thức" : "Subconscious Character");
                collection = new ExcelDacTinhTiemThucCollection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template đặc tính tiềm thức không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File Subconscious Character Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template đặc tính tiềm thức không đúng định dạng" : "Template Subconscious Character is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }
                
                int rowCount = dt.Rows.Count;
                string code;
                ExcelDacTinhTiemThuc value;
                for (int r = 0; r < rowCount; r++)
                {
                    DataRow row = dt.Rows[r];
                    code = row[1].ToString().ToLower().Trim();
                    value = collection.Find(code);
                    if (value == null)
                    {
                        value = new ExcelDacTinhTiemThuc(collection);
                        collection.Add(value);
                    }
                    value.Code = code;
                    value.TenNhomEnglish = row[2].ToString();
                    //value.TenNhomVietnamese = row[3].ToString();
                    value.MoTa = row[3].ToString();
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }

        public ExcelDacTinhTuDuyCollection ReadDacTinhTuDuyFromExcel(string filePath)
        {
            ExcelDacTinhTuDuyCollection collection = null;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "Đặc tính tư duy" : "Thinking Character");
                collection = new ExcelDacTinhTuDuyCollection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template đặc tính tư duy không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File Thinking Character Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template đặc tính tư duy không đúng định dạng" : "Template Thinking Character is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }

                int rowCount = dt.Rows.Count;
                string code;
                ExcelDacTinhTuDuy value;
                for (int r = 0; r < rowCount; r++)
                {
                    DataRow row = dt.Rows[r];
                    code = row[1].ToString().ToLower().Trim();
                    value = collection.Find(code);
                    if (value == null)
                    {
                        value = new ExcelDacTinhTuDuy(collection);
                        collection.Add(value);
                    }
                    value.Code = code;
                    value.TenNhomEnglish = row[2].ToString();
                    //value.TenNhomVietnamese = row[3].ToString();
                    value.MoTa = row[3].ToString();
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }

        public  ExcelKhuyenNghiCollection ReadKhuyenNghiFromExcel(string filePath)
        {
            ExcelKhuyenNghiCollection collection = null;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "Khuyến Nghị" : "Recommendations");
                collection = new ExcelKhuyenNghiCollection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template khuyến nghị không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File Recommendations Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template khuyến nghị không đúng định dạng" : "Template Recommendations is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }

                int rowCount = dt.Rows.Count;
                string code;
                ExcelKhuyenNghi value;
                for (int r = 0; r < rowCount; r++)
                {
                    DataRow row = dt.Rows[r];
                    code = row[1].ToString().ToLower().Trim();
                    value = collection.Find(code);
                    if (value == null)
                    {
                        value = new ExcelKhuyenNghi(collection);
                        collection.Add(value);
                    }
                    value.Code = code;
                    value.TenNhomEnglish = row[2].ToString();
                    //value.TenNhomVietnamese = row[3].ToString();
                    value.MoTa = row[3].ToString();
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }

        public ExcelTinhCachHanhViCollection ReadTinhCachHanhViFromExcel(string filePath)
        {
            ExcelTinhCachHanhViCollection collection = null;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "Tính cách hành vi" : "Personality and Behavior");
                collection = new ExcelTinhCachHanhViCollection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template tính cách hành vi không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File Personality and Behavior Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template tính cách hành vi không đúng định dạng" : "Template Personality and Behavior is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }

                int rowCount = dt.Rows.Count;
                int code;
                ExcelTinhCachHanhVi value;
                for (int r = 0; r < rowCount; r++)
                {
                    DataRow row = dt.Rows[r];
                    code = row[0].ToString().ToIntOrDefault();
                    value = collection.Find(code);
                    if (value == null)
                    {
                        value = new ExcelTinhCachHanhVi(collection);
                        collection.Add(value);
                    }
                    value.Code = code;
                    value.Title = row[1].ToString();
                    value.Content = "► " + row[2].ToString().Replace("\n","\r\n" + "► ") ;
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }

        public ExcelTop3In16Collection ReadTop3In16FromExcel(string filePath)
        {
            ExcelTop3In16Collection collection = null;
            try
            {
                MakeDecryptedTempFile(filePath, ICurrentSessionService.VietNamLanguage ? "16 nhóm năng lực" : "16 types");
                collection = new ExcelTop3In16Collection();
                DataTable dt = ExcelUtil_Bkp.ReadFromExcel(TempFilename);

                if (dt.Rows.Count <= 1 || dt.Columns.Count <= 1)
                {
                    if (ExcelUtil_Bkp.RowError != 0 && ExcelUtil_Bkp.CellError != 0)
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                                     ? "Template 16 nhóm năng lực không đúng định dạng ở hàng thứ " + ExcelUtil_Bkp.RowError + " và ô thứ " + ExcelUtil_Bkp.CellError + " đang không có giá trị."
                                     : "Template is not in correct format. Data Error null in File 16 types Row " + ExcelUtil_Bkp.RowError + " and Cell " + ExcelUtil_Bkp.CellError;
                    else
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "Template 16 nhóm năng lực không đúng định dạng" : "Template 16 types is not in correct format.";
                    ThrowNotCorrectFormatException(ErrMsg);
                }

                int rowCount = dt.Rows.Count;
                string code;
                ExcelTop3In16 value;
                for (int r = 0; r < rowCount; r++)
                {
                    DataRow row = dt.Rows[r];
                    code = row[1].ToString();
                    value = collection.Find(code);
                    if (value == null)
                    {
                        value = new ExcelTop3In16();
                        collection.Add(value);
                    }
                    value.Code = code;
                    value.TenNangLuc = row[2].ToString();
                    value.AdultDescription = row[3].ToString();
                    value.ChildDescription = row[4].ToString();
                }
            }
            catch (Exception exception)
            {
                collection = null;
                ErrMsg = exception.Message;
            }
            finally
            {
                DeleteDecryptedTempFile();
            }
            return collection;
        }
    }
}