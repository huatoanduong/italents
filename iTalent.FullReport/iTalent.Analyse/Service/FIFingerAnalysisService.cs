﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Model;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Util;
using iTalent.Entity;
using iTalent.Formula.Model;
using iTalent.Formula.Model.ExcelTemplate;
using iTalent.Formula.Model._39_40;
using iTalent.Utils;
#pragma warning disable 1587

namespace iTalent.Analyse.Service
{
    public interface IFIFingerAnalysisService : IService
    {
        FIFingerAnalysis Find(int id);

        FIFingerAnalysis CreateEntity(int agencyId, int reportId);

        bool CalculateFingerRecord(int reportId);

        bool GenerateReport(int reportId, int agencyId, int templateId, string templateFileName,
            string destinationFileName,
            bool isExportPdf, bool isAdult,string agentName);

        bool Add(FIFingerAnalysis obj);
        bool Update(FIFingerAnalysis obj);
        bool Delete(FIFingerAnalysis obj);
    }

    public class FIFingerAnalysisService : Pattern.Service, IFIFingerAnalysisService
    {
        public FIFingerAnalysisService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {

        }

        public FIFingerAnalysis Find(int id)
        {
            FIFingerAnalysis device = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                device = deviceRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                device = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return device;
        }

        public FIFingerAnalysis CreateEntity(int agencyId, int reportId)
        {
            FIFingerAnalysis analysis =
                new FIFingerAnalysis()
                {
                    AgencyID = agencyId,
                    ReportID = reportId,
                };
            return analysis;
        }

        protected List<FIFingerRecord> fingerRecords { get; set; }

        private int GetRcount(string type, out string p)
        {
            foreach (FIFingerRecord record in this.fingerRecords)
            {
                if (type == record.Type)
                {
                    p = record.FingerType;
                    if (type.Contains("ATD"))
                    {
                        return (int) record.ATDPoint;
                    }
                    return (int) record.RCCount;
                }
            }
            p = "";
            return 0;
        }

        private RidgeCountModelCollection GetFingerRecordModel(int reportId)
        {
            try
            {
                IFIFingerRecordRepository _fingerRecordRepository = new FIFingerRecordRepository(this._unitOfWork);
                fingerRecords = _fingerRecordRepository.FindByRecordId(reportId);

                string p;
                RidgeCountModelCollection modelCollection = new RidgeCountModelCollection
                                                            {
                                                                ATDL = GetRcount("LATD", out p),
                                                                ATDR = GetRcount("RATD", out p),
                                                                L1L = GetRcount("L1L", out p),
                                                                L1R = GetRcount("L1R", out p),
                                                                L1T = p,
                                                                L2L = GetRcount("L2L", out p),
                                                                L2R = GetRcount("L2R", out p),
                                                                L2T = p,
                                                                L3L = GetRcount("L3L", out p),
                                                                L3R = GetRcount("L3R", out p),
                                                                L3T = p,
                                                                L4L = GetRcount("L4L", out p),
                                                                L4R = GetRcount("L4R", out p),
                                                                L4T = p,
                                                                L5L = GetRcount("L5L", out p),
                                                                L5R = GetRcount("L5R", out p),
                                                                L5T = p,
                                                                R1L = GetRcount("R1L", out p),
                                                                R1R = GetRcount("R1R", out p),
                                                                R1T = p,
                                                                R2L = GetRcount("R2L", out p),
                                                                R2R = GetRcount("R2R", out p),
                                                                R2T = p,
                                                                R3L = GetRcount("R3L", out p),
                                                                R3R = GetRcount("R3R", out p),
                                                                R3T = p,
                                                                R4L = GetRcount("R4L", out p),
                                                                R4R = GetRcount("R4R", out p),
                                                                R4T = p,
                                                                R5L = GetRcount("R5L", out p),
                                                                R5R = GetRcount("R5R", out p),
                                                                R5T = p
                                                            };

                if (modelCollection.L1T.StartsWith("A")
                    && modelCollection.L2T.StartsWith("A")
                    && modelCollection.L3T.StartsWith("A")
                    && modelCollection.L4T.StartsWith("A")
                    && modelCollection.L5T.StartsWith("A")

                    && modelCollection.R1T.StartsWith("A")
                    && modelCollection.R2T.StartsWith("A")
                    && modelCollection.R3T.StartsWith("A")
                    && modelCollection.R4T.StartsWith("A")
                    && modelCollection.R5T.StartsWith("A"))
                {
                    modelCollection.L1L = 12;
                    modelCollection.L2L = 12;
                    modelCollection.L3L = 12;
                    modelCollection.L4L = 12;
                    modelCollection.L5L = 12;

                    modelCollection.L1R = 12;
                    modelCollection.L2R = 12;
                    modelCollection.L3R = 12;
                    modelCollection.L4R = 12;
                    modelCollection.L5R = 12;

                    modelCollection.R1L = 12;
                    modelCollection.R2L = 12;
                    modelCollection.R3L = 12;
                    modelCollection.R4L = 12;
                    modelCollection.R5L = 12;

                    modelCollection.R1R = 12;
                    modelCollection.R2R = 12;
                    modelCollection.R3R = 12;
                    modelCollection.R4R = 12;
                    modelCollection.R5R = 12;
                }

                if (modelCollection.L1T.StartsWith("WX")
                    && modelCollection.L2T.StartsWith("WX")
                    && modelCollection.L3T.StartsWith("WX")
                    && modelCollection.L4T.StartsWith("WX")
                    && modelCollection.L5T.StartsWith("WX")

                    && modelCollection.R1T.StartsWith("WX")
                    && modelCollection.R2T.StartsWith("WX")
                    && modelCollection.R3T.StartsWith("WX")
                    && modelCollection.R4T.StartsWith("WX")
                    && modelCollection.R5T.StartsWith("WX"))
                {
                    modelCollection.L1L = 12;
                    modelCollection.L2L = 12;
                    modelCollection.L3L = 12;
                    modelCollection.L4L = 12;
                    modelCollection.L5L = 12;

                    modelCollection.L1R = 12;
                    modelCollection.L2R = 12;
                    modelCollection.L3R = 12;
                    modelCollection.L4R = 12;
                    modelCollection.L5R = 12;

                    modelCollection.R1L = 12;
                    modelCollection.R2L = 12;
                    modelCollection.R3L = 12;
                    modelCollection.R4L = 12;
                    modelCollection.R5L = 12;

                    modelCollection.R1R = 12;
                    modelCollection.R2R = 12;
                    modelCollection.R3R = 12;
                    modelCollection.R4R = 12;
                    modelCollection.R5R = 12;
                }

                return modelCollection;
            }
            catch
            {
                return null;
            }
        }

        protected void MapMQChart(List<FIMQChart> source, List<FIMQChart> dest)
        {
            FIMQChart record;
            FIMQChart recordSource;
            for (int i = 0; i < 8; i++)
            {
                recordSource = source[i];
                record = dest.FirstOrDefault(p => (p.ReportID == recordSource.ReportID) && (p.MQID == recordSource.MQID));
                recordSource.MQValue = Math.Round(recordSource.MQValue, 2);
                if (record == null)
                {
                    dest.Add(recordSource);
                }
                else
                {
                    record.MQDesc = recordSource.MQDesc;
                    record.MQValue = recordSource.MQValue;
                }
            }
        }

        public bool CalculateFingerRecord(int reportId)
        {
            bool res = true;

            try
            {
                /// Lấy FingerAnalysis & FingerType & FingerRecord & MQChart tương ứng với ReportId
                /// Tính giá trị cho các nhóm Whorl, Cú, RL, Arch...
                /// Lấy các giá trị PI và HeSoTiemNang từ file mẫu 
                /// Tính các chỉ số PI
                /// Map các chỉ số PI vào FingerAnalysis từ FP1 -> FP10
                /// Insert/Update FingerAnalysis & MQChart
                /// 


                /// Lấy FingerAnalysis & FingerType & FingerRecord & MQChart tương ứng với ReportId
                /// 

                IFIFingerTypeRepository fingerTypeRepository = new FIFingerTypeRepository(this._unitOfWork);
                IFIFingerRecordRepository fingerRecordRepository = new FIFingerRecordRepository(this._unitOfWork);
                IFIFingerAnalysisRepository analysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIMQChartRepository mqChartRepository = new FIMQChartRepository(this._unitOfWork);

                List<FIFingerType> fingerTypes = fingerTypeRepository.FindAll();
                List<FIFingerRecord> fingerRecords = fingerRecordRepository.FindByRecordId(reportId);
                FIFingerAnalysis analysis = analysisRepository.FindByReportId(reportId);
                List<FIMQChart> dbMQCharts = mqChartRepository.FindByRecordId(reportId);

                /// Tính giá trị cho các nhóm Whorl, Cú, RL, Arch...
                /// 

                List<FIMQChart> mqCharts = new List<FIMQChart>();

                if (analysis == null)
                {
                    analysis = CreateEntity(ICurrentSessionService.CurAgency.ID, reportId);
                }

                IFormularService formularService = new FormularService();
                formularService.CalculateRecord(analysis, fingerRecords, fingerTypes);

                RidgeCountModelCollection recordModelCollection = GetFingerRecordModel(reportId);

                /// Lấy các giá trị PI và HeSoTiemNang từ file mẫu
                /// 

                IExcelTemplateService excelTemplateService = new ExcelTemplateService();
                ExcelNGFValueCollection excelNgfValueCollection =
                    excelTemplateService.ReadNGFFromExcel(ICurrentSessionService.NGFExcelPath);

                //HeSoTiemNangExcelCollection heSoTiemNangExcelCollection =
                //    excelTemplateService.ReadHeSoTiemNangFromExcel(ICurrentSessionService.HeSoTiemNangExcelPath);
                ExcelHeSoTiemNangCollection excelHeSoTiemNangCollection;
                excelHeSoTiemNangCollection = excelTemplateService.ReadHeSoTiemNangFromExcel(ICurrentSessionService.HeSoTiemNangExcelPath);

                PIModelCollection piModelCollection = new PIModelCollection(recordModelCollection, excelNgfValueCollection);

                /// Map các chỉ số PI vào FingerAnalysis từ FP1 -> FP10
                /// 

                piModelCollection.MapToFingerAnalyse(analysis);

                IntelligenceModelCollection intelligenceModelCollection = new IntelligenceModelCollection(piModelCollection);
                intelligenceModelCollection.Calculate();

                MQModelCollection mqModelCollection = new MQModelCollection(piModelCollection);

                CareerModelCollection careerModelCollection = new CareerModelCollection(mqModelCollection);
                Top3NGFModelCollection top3NgfModelCollection = new Top3NGFModelCollection(piModelCollection, excelHeSoTiemNangCollection);
                
                //DacTinhTiemThucModel dacTinhTiemThucModel = new DacTinhTiemThucModel();
                //DacTinhTuDuyModel dacTinhTuDuyModel = new DacTinhTuDuyModel(PICollection, intelligenceModelCollection, dacTinhTiemThucModel, )
                //HanhTrinhCuocSongModel hanhTrinhCuocSongModel =
                //    new HanhTrinhCuocSongModel(piModelCollection, intelligenceModelCollection, );
                //TinhCachHanhViModel tinhCachHanhViModel = new TinhCachHanhViModel(PICollection, intelligenceModelCollection);
                VanDongModelCollection vanDongModel = new VanDongModelCollection(piModelCollection, mqModelCollection);

                //CustomerAnalyseModel fieldModel = new CustomerAnalyseModel(ref analysis, ref mqCharts,
                //    recordModelCollection);
                ReportFieldModel fieldModel = new ReportFieldModel(ref analysis, ref mqCharts, recordModelCollection)
                {
                    RecordModelCollection = recordModelCollection,
                    PIModelCollection = piModelCollection,
                    //ACIDModel = acidModel,
                    MQModelCollection = mqModelCollection,
                    CareerModelCollection = careerModelCollection,
                    Top3NgfModelCollection = top3NgfModelCollection,
                    //DacTinhTuduyModel = dacTinhTuDuyModel,
                    //DacTinhTiemThucModel = dacTinhTiemThucModel,
                    //HanhTrinhCuocSongModel = hanhTrinhCuocSongModel,
                    //TinhCachHanhViModel = tinhCachHanhViModel,
                    VanDongModel = vanDongModel,
                    IntelligenceModelCollection = intelligenceModelCollection,
                };
                //fieldModel.Distributor = agency;
                //fieldModel.Customer = customer;
                fieldModel.CalucalteGeneral(ref analysis, ref mqCharts);
                MapMQChart(mqCharts, dbMQCharts);

                //TODO: Tính thêm 1 phần ReportFieldModel

                foreach (FIMQChart chart in dbMQCharts)
                {
                    if (chart.ID <= 0)
                    {
                        mqChartRepository.Insert(chart);
                    }
                    else
                    {
                        mqChartRepository.Update(chart);
                    }
                }


                if (analysis.ID <= 0)
                {
                    res = Add(analysis);
                }
                else
                {
                    res = Update(analysis);
                }
                if (!res)
                {
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }

            return res;
        }

        private void ThrowTemplateFormatError(string templateName)
        {
            string errMsg = "";

            errMsg += ICurrentSessionService.VietNamLanguage
                ? @"File mẫu không đúng định dạng. Tên file mẫu: "
                : @"Template file not in correct format. Template name: ";

            errMsg += templateName;

            throw new Exception(errMsg);
        }

        public bool GenerateReport(int reportId, int agencyId, int templateId, string templateFileName,
            string destinationFileName,
            bool isExportPdf, bool isAdult,string agentName)
        {
            try
            {
                /// Step of Generating Report:
                /// 1.1 Get Template file
                /// 1.2 Check Customer exist
                /// 1.3 Check Agency exist
                /// 1.4 Check License key valid or not
                /// 2. Copy to temporary folder
                /// 3. Convert to Word
                /// 4.1 Select all Analyse by CustomerId
                /// 4.2 Calculate FIFingerAnalysis and List<FIMQChart>
                /// 4.3 Insert/Update FIMQChart/AnalysisEntity
                /// 4.4 Generate Dictionary
                /// 5. Replace text inside Word (By ReportFieldModel)
                /// 6 Copy replaced text file to destinat folder with the name generated
                /// 7.1 Check if destinationFile exist
                /// 7.2 Write FIPrintRecord
                /// 7.3 Subtract point by 1
                /// 7.4 Delete tmpDat & tmpDocx


                /// 1.1 Get Template file

                IFITemplateRepository _templateRepository = new FITemplateRepository(this._unitOfWork);

                FITemplate template = _templateRepository.FindByKey(templateId);
                if (template == null
                    || !File.Exists(templateFileName))
                {
                    ThrowException("Mẫu báo cáo không tồn tại");
                }

                /// 1.2 Check Customer exist

                IFICustomerRepository _customerRepository = new FICustomerRepository(this._unitOfWork);
                FICustomer customer = _customerRepository.FindByKey(reportId);
                if (customer == null)
                {
                    ThrowException("Dữ liệu khách hàng không tồn tại");
                }

                /// 1.3 Check Agency exist

                IFIAgencyRepository _agencyRepository = new FIAgencyRepository(this._unitOfWork);
                FIAgency agency = _agencyRepository.FindByKey(agencyId);
                if (agency == null)
                {
                    agency = ICurrentSessionService.CurAgency;
                    //ThrowException("Dữ liệu đại lý không tồn tại");
                }

                /// 1.4 Check License key valid or not

                IFIPointModelService _pointService = new FIPointModelService(this._unitOfWork);
                List<PointModel> pointModels = _pointService.FindByAgency(ICurrentSessionService.CurAgency.ID);
                PointModel pointModel = pointModels.FirstOrDefault(p => !p.IsError && p.PointCurrent > 0 && p.KeyCurrent.Contains(ICurrentSessionService.Version));
                if (pointModel == null)
                {
                    ThrowException("Không tìm thấy license key còn hiệu lực");
                }

                /// 2. Copy to temporary folder

                //string templateFileName = template.TemplateFileName;
                string tmpFolderPath = ICurrentSessionService.DesDirNameTemp;
                ///It will be full fileName
                string tmpFileName = getRandomFileName(tmpFolderPath, "dat");
                string tmpDatFileName = Path.ChangeExtension(tmpFileName, "dat");
                string tmpDocxFileName = Path.ChangeExtension(tmpFileName, "dattmp");
                string destFileName = destinationFileName;
                bool res;

                Directory.CreateDirectory(Path.GetDirectoryName(destFileName));
                if (File.Exists(tmpDocxFileName))
                {
                    File.Delete(tmpDocxFileName);
                }
                File.Copy(templateFileName, tmpDatFileName);

                /// 3. Convert to Word
                IFileConverterService service = new FileConverterService(this._unitOfWork);
                res = service.FromRptxToDocx(tmpDatFileName, tmpDocxFileName);
                if (!res)
                {
                    ThrowException(service.ErrMsg);
                }


                /// 4.1 Select all Analyse by CustomerId

                RidgeCountModelCollection ridgeCountModelCollection = GetFingerRecordModel(reportId);
                //FIFingerAnalysis fingerAnalysis = new FIFingerAnalysis();
                IFIFingerAnalysisRepository analysisRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis fingerAnalysis = analysisRepository.FindByReportId(reportId);
                List<FIMQChart> mqCharts = new List<FIMQChart>();

                if (fingerAnalysis == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Dữ liệu khách hàng chưa được phân tích"
                        : "Customer data have not been analysed yet";
                    ThrowException(ErrMsg);
                }

                ExcelHeSoTiemNangCollection excelHeSoTiemNangCollection;
                ExcelDacTinhTiemThucCollection excelDacTinhTiemThucCollection;
                ExcelDacTinhTuDuyCollection excelDacTinhTuDuyCollection;
                ExcelKhuyenNghiCollection excelKhuyenNghiCollection;
                ExcelTinhCachHanhViCollection excelTinhCachCollection;
                ExcelTop3In16Collection excelTop3In16Collection;

                IExcelTemplateService excelTemplateService = new ExcelTemplateService();
                excelHeSoTiemNangCollection =
                    excelTemplateService.ReadHeSoTiemNangFromExcel(ICurrentSessionService.HeSoTiemNangExcelPath);
                excelDacTinhTiemThucCollection =
                    excelTemplateService.ReadDacTinhTiemThucFromExcel(ICurrentSessionService.DacTinhTiemThucExcelPath);
                excelDacTinhTuDuyCollection =
                    excelTemplateService.ReadDacTinhTuDuyFromExcel(ICurrentSessionService.DacTinhTuDuyExcelPath);
                excelTop3In16Collection =
                    excelTemplateService.ReadTop3In16FromExcel(ICurrentSessionService.Top3in16DescriptionExcelPath);

                if (isAdult)
                {
                    //Adult
                    excelKhuyenNghiCollection =
                        excelTemplateService.ReadKhuyenNghiFromExcel(ICurrentSessionService.KhuyenNghiExcelPath_Adult);
                    excelTinhCachCollection =
                        excelTemplateService.ReadTinhCachHanhViFromExcel(
                            ICurrentSessionService.TinhCachHanhViExcelPath_Adult);
                }
                else
                {
                    //Adult
                    excelKhuyenNghiCollection =
                        excelTemplateService.ReadKhuyenNghiFromExcel(ICurrentSessionService.KhuyenNghiExcelPath_Kid);
                    excelTinhCachCollection =
                        excelTemplateService.ReadTinhCachHanhViFromExcel(
                            ICurrentSessionService.TinhCachHanhViExcelPath_Kid);
                }

                //Xét các template có đúng format hay không
                string templateName;
                if (excelHeSoTiemNangCollection == null)
                {
                    templateName = ICurrentSessionService.VietNamLanguage
                        ? @"Hệ số tiềm năng"
                        : @"Hệ số tiềm năng";
                    ThrowTemplateFormatError(templateName);
                }
                if(excelDacTinhTiemThucCollection == null)
                {
                    templateName = ICurrentSessionService.VietNamLanguage
                        ? @"Đặc tính tiềm thức"
                        : @"Đặc tính tiềm thức";
                    ThrowTemplateFormatError(templateName);
                }
                if (excelDacTinhTuDuyCollection == null)
                {
                    templateName = ICurrentSessionService.VietNamLanguage
                        ? @"Đặc tính tư duy"
                        : @"Đặc tính tư duy";
                    ThrowTemplateFormatError(templateName);
                }
                if (excelTop3In16Collection == null)
                {
                    templateName = ICurrentSessionService.VietNamLanguage
                        ? @"Top 3 loại vận dộng"
                        : @"Top 3 loại vận dộng";
                    ThrowTemplateFormatError(templateName);
                }
                if (excelKhuyenNghiCollection == null)
                {
                    templateName = ICurrentSessionService.VietNamLanguage
                        ? @"Khuyến nghị"
                        : @"Khuyến nghị";
                    ThrowTemplateFormatError(templateName);
                }
                if (excelTinhCachCollection == null)
                {
                    templateName = ICurrentSessionService.VietNamLanguage
                        ? @"Đặc điểm tính cách"
                        : @"Đặc điểm tính cách";
                    ThrowTemplateFormatError(templateName);
                }

                ///Khai báo các Model

                PIModelCollection piModelCollection = new PIModelCollection(ridgeCountModelCollection, fingerAnalysis);
                piModelCollection.MapToFingerAnalyse(fingerAnalysis);

                IntelligenceModelCollection intelligenceModelCollection = new IntelligenceModelCollection(piModelCollection);
                intelligenceModelCollection.Calculate();

                MQModelCollection mqModelCollection = new MQModelCollection(piModelCollection);
                CareerModelCollection careerModelCollection = new CareerModelCollection(mqModelCollection);
                Top3NGFModelCollection top3NgfModelCollection = new Top3NGFModelCollection(piModelCollection, excelHeSoTiemNangCollection);

                DacTinhTiemThucModel dacTinhTiemThucModel = new DacTinhTiemThucModel(piModelCollection, intelligenceModelCollection, excelDacTinhTiemThucCollection);
                DacTinhTuDuyModel dacTinhTuDuyModel = new DacTinhTuDuyModel(piModelCollection,intelligenceModelCollection, dacTinhTiemThucModel, excelDacTinhTuDuyCollection);
                HanhTrinhCuocSongModel hanhTrinhCuocSongModel = new HanhTrinhCuocSongModel(piModelCollection, intelligenceModelCollection, dacTinhTuDuyModel);
                KhuyenNghiModel khuyenNghiModel = new KhuyenNghiModel(piModelCollection, intelligenceModelCollection, dacTinhTiemThucModel, dacTinhTuDuyModel, excelKhuyenNghiCollection, isAdult);
                TinhCachHanhViModel tinhCachHanhViModel = new TinhCachHanhViModel(piModelCollection, intelligenceModelCollection, dacTinhTiemThucModel, dacTinhTuDuyModel, excelTinhCachCollection, isAdult);
                //ACIDModel acidModel = new ACIDModel();
                VanDongModelCollection vanDongModel = new VanDongModelCollection(piModelCollection, mqModelCollection);
                Top3In16ModelCollection top3In16ModelCollection = new Top3In16ModelCollection(piModelCollection,
                    vanDongModel, mqModelCollection, excelTop3In16Collection, isAdult);

                /// 4.2 Calculate FIFingerAnalysis and List<FIMQChart>
                
                agency.Name = agentName;
                ReportFieldModel fieldModel = new ReportFieldModel
                                              {
                                                  RecordModelCollection = ridgeCountModelCollection,
                                                  PIModelCollection = piModelCollection,
                                                  MQModelCollection = mqModelCollection,
                                                  CareerModelCollection = careerModelCollection,
                                                  Top3NgfModelCollection = top3NgfModelCollection,
                                                  DacTinhTuduyModel = dacTinhTuDuyModel,
                                                  DacTinhTiemThucModel = dacTinhTiemThucModel,
                                                  HanhTrinhCuocSongModel = hanhTrinhCuocSongModel,
                                                  IntelligenceModelCollection = intelligenceModelCollection,
                                                  KhuyenNghiModel = khuyenNghiModel,
                                                  TinhCachHanhViModel = tinhCachHanhViModel,
                                                  VanDongModel = vanDongModel,
                                                  FingerAnalysis = fingerAnalysis,
                                                  Top3VanDongCollection = top3In16ModelCollection,
                                              };

                fieldModel.Distributor = agency;
                fieldModel.Customer = customer;
                fieldModel.CalucalteExport();
                

                FingerImageModel fieldImageModel = new FingerImageModel();
                fieldImageModel.ReportID = customer.ReportID.Substring(7);
                fieldImageModel.UnsignCharCustomerName =
                    ConvertUtil.ConverToUnsign(customer.Name).Replace(" ", "").ToUpper();
                fieldImageModel.SaveImagePath = ICurrentSessionService.DesDirNameSource;
                //fieldImageModel = FingerImageModel.Instance;
                /// 4.3 Insert/Update FIMQChart/AnalysisEntity

                //saveFingerAnalysis(agencyId, customerId, fingerAnalysis);
                //saveMQCharts(agencyId, customerId, mqCharts);

                //if (SaveChanges() <= 0)
                //{
                //    ThrowException("Không thể lưu thông tin");
                //}

                /// 4.4 Generate Dictionary

                //Dictionary<string, object> dictionary = fieldModel.ToDictionary();
                Dictionary<string, string> dictionary = fieldModel.ToDictionary();
                Dictionary<string, Image> dictionaryImage = fieldImageModel.ToDictionary();
                //Dictionary<string, Image> dictionaryImage2 = fieldModel.ToDictionaryImage();
                //foreach (KeyValuePair<string, Image> k in dictionaryImage2)
                //{
                //    dictionaryImage.Add(k.Key, k.Value);
                //}

                /// 5. Replace text inside Word (By ReportFieldModel)

                MSWordUtil.MergeFileContent(tmpDocxFileName);
                MSWordUtil.ReplaceAllTextAndTextbox(tmpDocxFileName, dictionary, dictionaryImage);


                /// 6. Copy replaced text file to destinate folder with the name generated

                if (File.Exists(destFileName))
                {
                    File.Delete(destFileName);
                }
                //if Export to Pdf

                //File.Copy(tmpDocxFileName, destFileName);
                if (isExportPdf)
                {
                    //util.LoadDocument(tmpDocxFileName);
                    //util.ExportToPdf(destFileName);
                    MSWordUtil.ExportPDF2(tmpDocxFileName, destFileName);
                }
                //if Export to Docx
                else
                {

                    File.Copy(tmpDocxFileName, destFileName);
                }
                /////////////////////////////////////////////////////////////
                //Directory.CreateDirectory(Path.GetDirectoryName(destFileName));
                //File.Copy(tmpDocxFileName, destFileName);


                /// 7.1 Check if destinationFile exist



                /// 7.2 Write FIPrintRecord
                int pointId = pointModel.ID;
                writePrintRecord(agencyId, templateId, reportId, pointId);

                /// 7.3 Subtract point by 1

                res = _pointService.SubtractPointValue(pointId, 1, ICurrentSessionService.Version);
                if (!res)
                {
                    ThrowException(_pointService.ErrMsg);
                }

                /// 7.2 Delete tmpDat & tmpDocx
                if (File.Exists(tmpDocxFileName))
                {
                    File.Delete(tmpDocxFileName);
                }
                if (File.Exists(tmpDatFileName))
                {
                    File.Delete(tmpDatFileName);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                return false;
            }
            return true;
        }

        protected string getRandomFileName(string folderPath, string extName)
        {
            string fileName = "";
            string fullFileName;
            string strTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            byte count = 0;
            do
            {
                fileName = Path.ChangeExtension((strTime + (count++).ToString("D2")), extName);
                fullFileName = Path.Combine(folderPath, fileName);
            }
            while (Directory.Exists(fullFileName));
            return fullFileName;
        }

        private void writePrintRecord(int agencyId, int templateId, int customerId, int pointId)
        {
            IFIPrintRecordRepository _printRecordRepository = new FIPrintRecordRepository(this._unitOfWork);
            FIPrintRecord obj = _printRecordRepository.CreateEntity();
            obj.AgencyID = agencyId;
            obj.TemplateID = templateId;
            obj.ReportID = customerId;
            obj.PointID = pointId;
            _printRecordRepository.Insert(obj);
        }

        public bool Add(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay đã tồn tại"
                        : "Finger analyse Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm phân tích vân tay"
                        : "Cannot add new finger analyse";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }

        public bool Update(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay không tồn tại"
                        : "Finger analyse Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin phân tích vân tay"
                        : "Cannot modify finger analyse infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }

        public bool Delete(FIFingerAnalysis obj)
        {
            bool res = true;
            try
            {
                IFIFingerAnalysisRepository deviceRepository = new FIFingerAnalysisRepository(this._unitOfWork);
                FIFingerAnalysis obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã phân tích vân tay không tồn tại"
                        : "Finger analyse Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa phân tích vân tay"
                        : "Cannot remove finger analyse";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            return res;
        }
    }
}