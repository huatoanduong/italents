﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Pattern;
using iTalent.Utils;
#pragma warning disable 1587

namespace iTalent.Analyse.Service
{
    public interface IInstallLicenseService : IErrorProvider, IDisposable
    {
        /// Quy trình:
        /// 1. Client: Gọi hàm GetComputerKey -> computerKey -> Gửi cho Admin
        /// 2. Admin dùng hàm GetSecKeySection để lấy SecKey
        /// 3. Admin dùng hàm MakeLicense (truyền vào computerKey nhận từ Client và secKey) -> licenseKey -> Gửi cho Client
        /// 4. Client dùng hàm RegisterLicense để đăng kí licenseKey
        /// 5. Client: Mỗi khi khởi động chương trình gọi hàm CheckLicense (truyền vào secKey từ DB)



        /// <summary>
        /// Client Sử dụng để lấy Computer của máy cài đặt
        /// Computer Key và Seckey đã mã hóa
        /// </summary>
        /// <returns></returns>
        string GetComputerKey();

        /// <summary>
        /// Client sử dụng để đăng kí License Key vào hệ thống
        /// </summary>
        /// <param name="licenseKey"></param>
        /// <param name="secKey"></param>
        /// <returns></returns>
        bool RegisterLicense(string licenseKey, out string secKey);
        bool GetInfoLicense(string licenseKey, string computerkey, out string secKey,out DateTime exDateTime, out DateTime geDateTime);
        /// <summary>
        /// Client sử dụng để check License Key mỗi lần mở chương trình
        /// </summary>
        /// <param name="secKey"></param>
        /// <returns></returns>
        bool CheckLicense(string secKey);

        /// <summary>
        /// Admin sử dụng để generate ra 1 LicenseKey có hạn sử dụng
        /// </summary>
        /// <param name="computerKey"></param>
        /// <param name="secKey"></param>
        /// <param name="expiredDate"></param>
        /// <returns></returns>
        string MakeLicense(string computerKey, string secKey, DateTime expiredDate);

        /// <summary>
        /// Admin sử dụng để lấy SecKey từ ComputerKey
        /// Seckey này là seckey chưa mã hóa
        /// </summary>
        /// <param name="computerKey"></param>
        /// <returns></returns>
        string GetSecKeySection(string computerKey);
    }

    public class InstallLicenseService : ErrorProvider, IInstallLicenseService
    {
        protected string KeyEncrypt { get { return "0+zbjg}sI{M53K["; } }
        protected string LicenseRegistryPath { get { return @"Software\toro\Ftalk"; } }
        protected string DateTimeFormat { get { return "yyyyMMdd"; } }

        protected string Encrypt(string input)
        {
            return Security.Encrypt(input, KeyEncrypt);
        }

        protected string Decrypt(string input)
        {
            return Security.Decrypt(input, KeyEncrypt);
        }

        protected string GetMACAddress()
        {
            return HardwareInfo.GetMacAddress();
        }

        protected string GetCPUId()
        {
            return HardwareInfo.GetProcessorId();
        }

        protected string GetLicenseString(string computerKey, string secKey, DateTime generateDate, DateTime expiredDate)
        {
            //string macAddress = GetMACAddress();
            //string CPUId = GetCPUId();

            string licenseKey;
            licenseKey = computerKey
                         + generateDate.ToString(DateTimeFormat)
                         + expiredDate.ToString(DateTimeFormat)
                         + secKey;

            licenseKey = Encrypt(licenseKey);
            return licenseKey;
        }

        protected void WriteLocalStoredLicenseKey(string value)
        {
            RegSetting setting = new RegSetting();
            setting.CRegistryKey = LicenseRegistryPath;
            setting.SetValue("lic", value);
        }

        protected string GetLocalStoredLicenseKey()
        {
            RegSetting setting = new RegSetting();
            setting.CRegistryKey = LicenseRegistryPath;
            return setting.GetValue("lic");
        }

        public string GetSecKeySection(string computerKey)
        {
            //computerKey = Decrypt(computerKey);
            return GeneratePos.GetKeyDe(computerKey);
        }

        public string GetComputerKey()
        {
            //string macAddress = GetMACAddress();
            //string CPUId = GetCPUId();

            /// Seckey đã mã hóa
            var computerKey = GeneratePos.KeyGenerate();

            //computerKey = Encrypt(computerKey);
            return computerKey;
        }

        public bool RegisterLicense(string licenseKey, out string secKey)
        {
            bool res = true;
            secKey = null;
            try
            {
                if (licenseKey.IsBlank())
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không tìm thấy License key"
                        : "License key is not found";
                    ThrowException(ErrMsg);
                }
                DateTime generateDate;
                DateTime expiredDate = DateTime.MaxValue.Date;
                string strGenerateDate, strExpiredDate;
                try
                {
                    string decryptKey = Decrypt(licenseKey);
                    string computerInfo;
                    computerInfo = GetComputerKey();

                    if (!decryptKey.StartsWith(computerInfo))
                    {
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "License key không hợp lệ"
                            : "License key is not valid";
                        ThrowException(ErrMsg);
                    }
                    decryptKey = decryptKey.Remove(0, computerInfo.Length);

                    strGenerateDate = decryptKey.Substring(0, DateTimeFormat.Length);
                    decryptKey = decryptKey.Remove(0, DateTimeFormat.Length);

                    strExpiredDate = decryptKey.Substring(0, DateTimeFormat.Length);
                    decryptKey = decryptKey.Remove(0, DateTimeFormat.Length);

                    generateDate = DateTime.ParseExact(strGenerateDate, DateTimeFormat, CultureInfo.CurrentCulture);
                    expiredDate = DateTime.ParseExact(strExpiredDate, DateTimeFormat, CultureInfo.CurrentCulture);

                    secKey = decryptKey;

                    WriteLocalStoredLicenseKey(licenseKey);
                }
                catch
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License key không hợp lệ"
                        : "License key is not valid";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                res = false;
                ErrMsg = exception.Message;
            }
            return res;
        }

        public bool GetInfoLicense(string licenseKey,string computerkey, out string secKey, out DateTime exDateTime, out DateTime geDateTime)
        {
            bool res = true;
            secKey = null;
            exDateTime = DateTime.MaxValue;
            geDateTime = DateTime.MaxValue;
            try
            {
                if (licenseKey.IsBlank())
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không tìm thấy License key"
                        : "License key is not found";
                    ThrowException(ErrMsg);
                }
                DateTime generateDate;
                DateTime expiredDate = DateTime.MaxValue.Date;
                string strGenerateDate, strExpiredDate;
                try
                {
                    string decryptKey = Decrypt(licenseKey);
                    string computerInfo;
                    computerInfo = computerkey;//GetComputerKey();

                    if (!decryptKey.StartsWith(computerInfo))
                    {
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "License key không hợp lệ"
                            : "License key is not valid";
                        ThrowException(ErrMsg);
                    }
                    decryptKey = decryptKey.Remove(0, computerInfo.Length);

                    strGenerateDate = decryptKey.Substring(0, DateTimeFormat.Length);
                    decryptKey = decryptKey.Remove(0, DateTimeFormat.Length);

                    strExpiredDate = decryptKey.Substring(0, DateTimeFormat.Length);
                    decryptKey = decryptKey.Remove(0, DateTimeFormat.Length);

                    generateDate = DateTime.ParseExact(strGenerateDate, DateTimeFormat, CultureInfo.CurrentCulture);
                    expiredDate = DateTime.ParseExact(strExpiredDate, DateTimeFormat, CultureInfo.CurrentCulture);

                    secKey = decryptKey;
                    exDateTime = expiredDate;
                    geDateTime = generateDate;
                }
                catch(Exception ex)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License key không hợp lệ"
                        : "License key is not valid";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                res = false;
                ErrMsg = exception.Message;
            }
            return res;
        }

        public bool CheckLicense(string secKey)
        {
            bool res = true;
            try
            {
                string licenseKey = GetLocalStoredLicenseKey();
                if (licenseKey.IsBlank())
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không tìm thấy License key"
                        : "License key is not found";
                    ThrowException(ErrMsg);
                }
                DateTime generateDate;
                DateTime expiredDate = DateTime.MaxValue.Date;
                string strGenerateDate, strExpiredDate;
                try
                {
                    licenseKey = Decrypt(licenseKey);
                    string computerInfo;
                    computerInfo = GetComputerKey();

                    if (!licenseKey.StartsWith(computerInfo))
                    {
                        ErrMsg = ICurrentSessionService.VietNamLanguage
                            ? "License key không hợp lệ"
                            : "License key is not valid";
                        ThrowException(ErrMsg);
                    }
                    licenseKey = licenseKey.Remove(0, computerInfo.Length);

                    strGenerateDate = licenseKey.Substring(0, DateTimeFormat.Length);
                    licenseKey = licenseKey.Remove(0, DateTimeFormat.Length);

                    strExpiredDate = licenseKey.Substring(0, DateTimeFormat.Length);
                    generateDate = DateTime.ParseExact(strGenerateDate, DateTimeFormat, CultureInfo.CurrentCulture);
                    expiredDate = DateTime.ParseExact(strExpiredDate, DateTimeFormat, CultureInfo.CurrentCulture);

                    licenseKey = licenseKey.Replace(strExpiredDate, "");

                    if (licenseKey != secKey)
                    {
                        ThrowException();
                    }
                }
                catch
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License key không hợp lệ"
                        : "License key is not valid";
                    ThrowException(ErrMsg);
                }

                if (expiredDate.EndOfDate() < DateTime.Now)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "License key đã hết hạn"
                        : "License key is expired";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                res = false;
                ErrMsg = exception.Message;
            }
            return res;
        }

        public string MakeLicense(string computerKey, string secKey, DateTime expiredDate)
        {
            DateTime generateDate = DateTime.Now;

            string licenseKey = null;

            try
            {
                //try
                //{
                //    computerKey = Decrypt(computerKey);
                //}
                //catch
                //{
                //    ErrMsg = ICurrentSessionService.VietNamLanguage
                //        ? "License key không hợp lệ"
                //        : "License key is not valid";
                //    ThrowException(ErrMsg);
                //}

                //licenseKey = computerKey
                //             + generateDate.ToString("yyyyMMdd")
                //             + expiredDate.ToString("yyyyMMdd");
                //licenseKey = Encrypt(licenseKey);

                licenseKey = GetLicenseString(computerKey, secKey, generateDate, expiredDate);
            }
            catch (Exception exception)
            {
                licenseKey = null;
                ErrMsg = exception.Message;
            }
            return licenseKey;
        }

        public void Dispose()
        {
            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }
}