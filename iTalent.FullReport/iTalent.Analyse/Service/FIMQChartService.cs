﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Pattern;
using iTalent.Analyse.Repository;
using iTalent.Entity;

namespace iTalent.Analyse.Service
{
    public interface IFIMQChartService : IService
    {
        FIMQChart Find(int id);

        bool Add(FIMQChart obj);
        bool Update(FIMQChart obj);
        bool Delete(FIMQChart obj);
    }
    public class FIMQChartService : Pattern.Service, IFIMQChartService
    {
        public FIMQChartService(IUnitOfWorkAsync unitOfWork) : base(unitOfWork)
        {
        }

        protected override void InitRepositories()
        {
        }

        public FIMQChart Find(int id)
        {
            FIMQChart device = null;
            try
            {
                _unitOfWork.OpenConnection();
                FIMQChartRepository deviceRepository = new FIMQChartRepository(this._unitOfWork);
                device = deviceRepository.FindByKey(id);
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                device = null;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return device;
        }

        public bool Add(FIMQChart obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIMQChartRepository deviceRepository = new FIMQChartRepository(this._unitOfWork);
                FIMQChart obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 != null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã MQChart đã tồn tại"
                        : "MQChart Id is already existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Insert(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể thêm MQChart"
                        : "Cannot add new MQChart";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Update(FIMQChart obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIMQChartRepository deviceRepository = new FIMQChartRepository(this._unitOfWork);
                FIMQChart obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã MQChart không tồn tại"
                        : "MQChart Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Update(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể cập nhật thông tin MQChart"
                        : "Cannot modify MQChart infomartion";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }

        public bool Delete(FIMQChart obj)
        {
            bool res = true;
            try
            {
                _unitOfWork.OpenConnection();

                IFIMQChartRepository deviceRepository = new FIMQChartRepository(this._unitOfWork);
                FIMQChart obj2 = deviceRepository.FindByKey(obj.ID);
                if (obj2 == null)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Mã MQChart không tồn tại"
                        : "MQChart Id is not existed";
                    ThrowException(ErrMsg);
                }

                int resCount = deviceRepository.Delete(obj);
                if (resCount <= 0)
                {
                    ErrMsg = ICurrentSessionService.VietNamLanguage
                        ? "Không thể xóa MQChart"
                        : "Cannot remove MQChart";
                    ThrowException(ErrMsg);
                }
            }
            catch (Exception exception)
            {
                ErrMsg = exception.Message;
                res = false;
            }
            finally
            {
                _unitOfWork.CloseConnection();
            }
            return res;
        }
    }
}
