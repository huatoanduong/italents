﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using iTalent.Analyse.Pattern;

namespace iTalent.Analyse.Repository
{
    public interface IRepository : IErrorProvider
    {
        IDbConnection DbConnection { get; set; }
        IDbTransaction DbTransaction { get; set; }
    }

    public abstract class Repository : ErrorProvider, IRepository
    {
        public IDbConnection DbConnection { get; set; }
        public IDbTransaction DbTransaction { get; set; }

        protected string _SQLSelectByKey { get; set; }
        protected string _SQLUpdate { get; set; }
        protected string _SQLInsert { get; set; }
        protected string _SQLDelete { get; set; }
        protected string _SQLSelectAll { get; set; }

        protected void ToLowerQuery()
        {
            _SQLDelete = _SQLDelete.ToLower();
            _SQLUpdate = _SQLUpdate.ToLower();
            _SQLInsert = _SQLInsert.ToLower();
            _SQLSelectByKey = _SQLSelectByKey.ToLower();
        }

        protected void ToUpperQuery()
        {
            _SQLDelete = _SQLDelete.ToLower();
            _SQLUpdate = _SQLUpdate.ToLower();
            _SQLInsert = _SQLInsert.ToLower();
            _SQLSelectByKey = _SQLSelectByKey.ToLower();
        }

        protected abstract void InitSqlQuery();
    }
}