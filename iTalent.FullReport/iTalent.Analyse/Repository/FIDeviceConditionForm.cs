using System;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIDevice.
	/// </summary>
	public class FIDeviceConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private string _Devicename;
      private DateTime _ActiveDate;
      private string _ProductKey;
      private string _SerailKey;
      private string _SecKey;
      private string _Max;
      private string _Min;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetDevicename;
	public bool IsDevicenameNullable
      { get { return true;  } }
      public string Devicename
      {
         get { return _Devicename;  }
         set { 
		_Devicename = value; 
		IsSetDevicename = true;
		}
      }
	public bool IsSetActiveDate;
	public bool IsActiveDateNullable
      { get { return true;  } }
      public DateTime ActiveDate
      {
         get { return _ActiveDate;  }
         set { 
		_ActiveDate = value; 
		IsSetActiveDate = true;
		}
      }
	public bool IsSetProductKey;
	public bool IsProductKeyNullable
      { get { return true;  } }
      public string ProductKey
      {
         get { return _ProductKey;  }
         set { 
		_ProductKey = value; 
		IsSetProductKey = true;
		}
      }
	public bool IsSetSerailKey;
	public bool IsSerailKeyNullable
      { get { return true;  } }
      public string SerailKey
      {
         get { return _SerailKey;  }
         set { 
		_SerailKey = value; 
		IsSetSerailKey = true;
		}
      }
	public bool IsSetSecKey;
	public bool IsSecKeyNullable
      { get { return true;  } }
      public string SecKey
      {
         get { return _SecKey;  }
         set { 
		_SecKey = value; 
		IsSetSecKey = true;
		}
      }
	public bool IsSetMax;
	public bool IsMaxNullable
      { get { return true;  } }
      public string Max
      {
         get { return _Max;  }
         set { 
		_Max = value; 
		IsSetMax = true;
		}
      }
	public bool IsSetMin;
	public bool IsMinNullable
      { get { return true;  } }
      public string Min
      {
         get { return _Min;  }
         set { 
		_Min = value; 
		IsSetMin = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIDeviceConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _Devicename = EmptyValues.v_string;
	IsSetDevicename = false;
         _ActiveDate = EmptyValues.v_DateTime;
	IsSetActiveDate = false;
         _ProductKey = EmptyValues.v_string;
	IsSetProductKey = false;
         _SerailKey = EmptyValues.v_string;
	IsSetSerailKey = false;
         _SecKey = EmptyValues.v_string;
	IsSetSecKey = false;
         _Max = EmptyValues.v_string;
	IsSetMax = false;
         _Min = EmptyValues.v_string;
	IsSetMin = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetDevicename)
            {
                s += " AND Devicename = @Devicename ";
            }
            if (IsSetActiveDate)
            {
                s += " AND ActiveDate = @ActiveDate ";
            }
            if (IsSetProductKey)
            {
                s += " AND ProductKey = @ProductKey ";
            }
            if (IsSetSerailKey)
            {
                s += " AND SerailKey = @SerailKey ";
            }
            if (IsSetSecKey)
            {
                s += " AND SecKey = @SecKey ";
            }
            if (IsSetMax)
            {
                s += " AND Max = @Max ";
            }
            if (IsSetMin)
            {
                s += " AND Min = @Min ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}