﻿using System;
using System.Data;

namespace iTalent.Analyse.Repository
{
    public interface IUnitOfWorkAsync : IDisposable
    {
        IDbConnection DbConnection { get; set; }
        IDbTransaction DbTransaction { get; set; }
        void OpenConnection();
        void CloseConnection();
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
        void Commit();
        void Rollback();
    }

    public class UnitOfWorkAsync : IUnitOfWorkAsync
    {
        public IDbConnection DbConnection { get; set; }
        public IDbTransaction DbTransaction { get; set; }
        protected bool _disposed;

        public UnitOfWorkAsync(IDbConnection conn)
        {
            DbConnection = conn;
        }

        public void OpenConnection()
        {
            if (DbConnection.State != ConnectionState.Open)
            {
                DbConnection.Open();
            }
        }

        public void CloseConnection()
        {
            if (DbConnection.State != ConnectionState.Closed)
            {
                DbConnection.Close();
            }
        }

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            if (DbTransaction != null)
            {
                this.DbTransaction = DbConnection.BeginTransaction(isolationLevel);
            }
        }

        public void Commit()
        {
            DbTransaction.Commit();
        }

        public void Rollback()
        {
            this.DbTransaction.Rollback();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only

                try
                {
                    if (DbTransaction != null)
                    {
                        DbTransaction.Dispose();
                    }

                    if (DbConnection != null)
                    {
                        if (DbConnection.State == ConnectionState.Closed)
                        {
                            DbConnection.Close();
                        }
                        DbConnection.Close();
                    }
                }
                catch (ObjectDisposedException)
                {
                    // do nothing, the objectContext has already been disposed
                }

            }

            // release any unmanaged objects
            // set the object references to null

            _disposed = true;
        }
    }
}