using System;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIMQChart.
	/// </summary>
	public class FIMQChartConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _ReportID;
      private string _MQID;
      private string _MQDesc;
      private decimal _MQValue;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetReportID;
	public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { 
		_ReportID = value; 
		IsSetReportID = true;
		}
      }
	public bool IsSetMQID;
	public bool IsMQIDNullable
      { get { return true;  } }
      public string MQID
      {
         get { return _MQID;  }
         set { 
		_MQID = value; 
		IsSetMQID = true;
		}
      }
	public bool IsSetMQDesc;
	public bool IsMQDescNullable
      { get { return true;  } }
      public string MQDesc
      {
         get { return _MQDesc;  }
         set { 
		_MQDesc = value; 
		IsSetMQDesc = true;
		}
      }
	public bool IsSetMQValue;
	public bool IsMQValueNullable
      { get { return true;  } }
      public decimal MQValue
      {
         get { return _MQValue;  }
         set { 
		_MQValue = value; 
		IsSetMQValue = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIMQChartConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _ReportID = EmptyValues.v_Int32;
	IsSetReportID = false;
         _MQID = EmptyValues.v_string;
	IsSetMQID = false;
         _MQDesc = EmptyValues.v_string;
	IsSetMQDesc = false;
         _MQValue = EmptyValues.v_decimal;
	IsSetMQValue = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetReportID)
            {
                s += " AND ReportID = @ReportID ";
            }
            if (IsSetMQID)
            {
                s += " AND MQID = @MQID ";
            }
            if (IsSetMQDesc)
            {
                s += " AND MQDesc = @MQDesc ";
            }
            if (IsSetMQValue)
            {
                s += " AND MQValue = @MQValue ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}