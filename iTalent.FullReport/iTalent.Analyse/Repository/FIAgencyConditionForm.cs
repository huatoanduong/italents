using System;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIAgency.
	/// </summary>
	public class FIAgencyConditionForm
	{

      #region Fields

      private Int32 _ID;
      private string _Username;
      private string _Password;
      private string _Name;
      private Int16 _Point;
      private string _Gender;
      private DateTime _DOB;
      private string _Address1;
      private string _Address2;
      private string _City;
      private string _State;
      private string _PostalCode;
      private string _Country;
      private string _PhoneNo;
      private string _MobileNo;
      private string _Email;
      private string _PassEmail;
      private string _SaveImages;
      private DateTime _LastActiveDate;
      private Int16 _Day_Actived;
      private string _Dis_Activated;
      private string _ProductKey;
      private string _SerailKey;
      private string _SecKey;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetUsername;
	public bool IsUsernameNullable
      { get { return true;  } }
      public string Username
      {
         get { return _Username;  }
         set { 
		_Username = value; 
		IsSetUsername = true;
		}
      }
	public bool IsSetPassword;
	public bool IsPasswordNullable
      { get { return true;  } }
      public string Password
      {
         get { return _Password;  }
         set { 
		_Password = value; 
		IsSetPassword = true;
		}
      }
	public bool IsSetName;
	public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { 
		_Name = value; 
		IsSetName = true;
		}
      }
	public bool IsSetPoint;
	public bool IsPointNullable
      { get { return true;  } }
      public Int16 Point
      {
         get { return _Point;  }
         set { 
		_Point = value; 
		IsSetPoint = true;
		}
      }
	public bool IsSetGender;
	public bool IsGenderNullable
      { get { return true;  } }
      public string Gender
      {
         get { return _Gender;  }
         set { 
		_Gender = value; 
		IsSetGender = true;
		}
      }
	public bool IsSetDOB;
	public bool IsDOBNullable
      { get { return true;  } }
      public DateTime DOB
      {
         get { return _DOB;  }
         set { 
		_DOB = value; 
		IsSetDOB = true;
		}
      }
	public bool IsSetAddress1;
	public bool IsAddress1Nullable
      { get { return true;  } }
      public string Address1
      {
         get { return _Address1;  }
         set { 
		_Address1 = value; 
		IsSetAddress1 = true;
		}
      }
	public bool IsSetAddress2;
	public bool IsAddress2Nullable
      { get { return true;  } }
      public string Address2
      {
         get { return _Address2;  }
         set { 
		_Address2 = value; 
		IsSetAddress2 = true;
		}
      }
	public bool IsSetCity;
	public bool IsCityNullable
      { get { return true;  } }
      public string City
      {
         get { return _City;  }
         set { 
		_City = value; 
		IsSetCity = true;
		}
      }
	public bool IsSetState;
	public bool IsStateNullable
      { get { return true;  } }
      public string State
      {
         get { return _State;  }
         set { 
		_State = value; 
		IsSetState = true;
		}
      }
	public bool IsSetPostalCode;
	public bool IsPostalCodeNullable
      { get { return true;  } }
      public string PostalCode
      {
         get { return _PostalCode;  }
         set { 
		_PostalCode = value; 
		IsSetPostalCode = true;
		}
      }
	public bool IsSetCountry;
	public bool IsCountryNullable
      { get { return true;  } }
      public string Country
      {
         get { return _Country;  }
         set { 
		_Country = value; 
		IsSetCountry = true;
		}
      }
	public bool IsSetPhoneNo;
	public bool IsPhoneNoNullable
      { get { return true;  } }
      public string PhoneNo
      {
         get { return _PhoneNo;  }
         set { 
		_PhoneNo = value; 
		IsSetPhoneNo = true;
		}
      }
	public bool IsSetMobileNo;
	public bool IsMobileNoNullable
      { get { return true;  } }
      public string MobileNo
      {
         get { return _MobileNo;  }
         set { 
		_MobileNo = value; 
		IsSetMobileNo = true;
		}
      }
	public bool IsSetEmail;
	public bool IsEmailNullable
      { get { return true;  } }
      public string Email
      {
         get { return _Email;  }
         set { 
		_Email = value; 
		IsSetEmail = true;
		}
      }
	public bool IsSetPassEmail;
	public bool IsPassEmailNullable
      { get { return true;  } }
      public string PassEmail
      {
         get { return _PassEmail;  }
         set { 
		_PassEmail = value; 
		IsSetPassEmail = true;
		}
      }
	public bool IsSetSaveImages;
	public bool IsSaveImagesNullable
      { get { return true;  } }
      public string SaveImages
      {
         get { return _SaveImages;  }
         set { 
		_SaveImages = value; 
		IsSetSaveImages = true;
		}
      }
	public bool IsSetLastActiveDate;
	public bool IsLastActiveDateNullable
      { get { return true;  } }
      public DateTime LastActiveDate
      {
         get { return _LastActiveDate;  }
         set { 
		_LastActiveDate = value; 
		IsSetLastActiveDate = true;
		}
      }
	public bool IsSetDay_Actived;
	public bool IsDay_ActivedNullable
      { get { return true;  } }
      public Int16 Day_Actived
      {
         get { return _Day_Actived;  }
         set { 
		_Day_Actived = value; 
		IsSetDay_Actived = true;
		}
      }
	public bool IsSetDis_Activated;
	public bool IsDis_ActivatedNullable
      { get { return true;  } }
      public string Dis_Activated
      {
         get { return _Dis_Activated;  }
         set { 
		_Dis_Activated = value; 
		IsSetDis_Activated = true;
		}
      }
	public bool IsSetProductKey;
	public bool IsProductKeyNullable
      { get { return true;  } }
      public string ProductKey
      {
         get { return _ProductKey;  }
         set { 
		_ProductKey = value; 
		IsSetProductKey = true;
		}
      }
	public bool IsSetSerailKey;
	public bool IsSerailKeyNullable
      { get { return true;  } }
      public string SerailKey
      {
         get { return _SerailKey;  }
         set { 
		_SerailKey = value; 
		IsSetSerailKey = true;
		}
      }
	public bool IsSetSecKey;
	public bool IsSecKeyNullable
      { get { return true;  } }
      public string SecKey
      {
         get { return _SecKey;  }
         set { 
		_SecKey = value; 
		IsSetSecKey = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIAgencyConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _Username = EmptyValues.v_string;
	IsSetUsername = false;
         _Password = EmptyValues.v_string;
	IsSetPassword = false;
         _Name = EmptyValues.v_string;
	IsSetName = false;
         _Point = EmptyValues.v_Int16;
	IsSetPoint = false;
         _Gender = EmptyValues.v_string;
	IsSetGender = false;
         _DOB = EmptyValues.v_DateTime;
	IsSetDOB = false;
         _Address1 = EmptyValues.v_string;
	IsSetAddress1 = false;
         _Address2 = EmptyValues.v_string;
	IsSetAddress2 = false;
         _City = EmptyValues.v_string;
	IsSetCity = false;
         _State = EmptyValues.v_string;
	IsSetState = false;
         _PostalCode = EmptyValues.v_string;
	IsSetPostalCode = false;
         _Country = EmptyValues.v_string;
	IsSetCountry = false;
         _PhoneNo = EmptyValues.v_string;
	IsSetPhoneNo = false;
         _MobileNo = EmptyValues.v_string;
	IsSetMobileNo = false;
         _Email = EmptyValues.v_string;
	IsSetEmail = false;
         _PassEmail = EmptyValues.v_string;
	IsSetPassEmail = false;
         _SaveImages = EmptyValues.v_string;
	IsSetSaveImages = false;
         _LastActiveDate = EmptyValues.v_DateTime;
	IsSetLastActiveDate = false;
         _Day_Actived = EmptyValues.v_Int16;
	IsSetDay_Actived = false;
         _Dis_Activated = EmptyValues.v_string;
	IsSetDis_Activated = false;
         _ProductKey = EmptyValues.v_string;
	IsSetProductKey = false;
         _SerailKey = EmptyValues.v_string;
	IsSetSerailKey = false;
         _SecKey = EmptyValues.v_string;
	IsSetSecKey = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetUsername)
            {
                s += " AND Username = @Username ";
            }
            if (IsSetPassword)
            {
                s += " AND Password = @Password ";
            }
            if (IsSetName)
            {
                s += " AND Name = @Name ";
            }
            if (IsSetPoint)
            {
                s += " AND Point = @Point ";
            }
            if (IsSetGender)
            {
                s += " AND Gender = @Gender ";
            }
            if (IsSetDOB)
            {
                s += " AND DOB = @DOB ";
            }
            if (IsSetAddress1)
            {
                s += " AND Address1 = @Address1 ";
            }
            if (IsSetAddress2)
            {
                s += " AND Address2 = @Address2 ";
            }
            if (IsSetCity)
            {
                s += " AND City = @City ";
            }
            if (IsSetState)
            {
                s += " AND State = @State ";
            }
            if (IsSetPostalCode)
            {
                s += " AND PostalCode = @PostalCode ";
            }
            if (IsSetCountry)
            {
                s += " AND Country = @Country ";
            }
            if (IsSetPhoneNo)
            {
                s += " AND PhoneNo = @PhoneNo ";
            }
            if (IsSetMobileNo)
            {
                s += " AND MobileNo = @MobileNo ";
            }
            if (IsSetEmail)
            {
                s += " AND Email = @Email ";
            }
            if (IsSetPassEmail)
            {
                s += " AND PassEmail = @PassEmail ";
            }
            if (IsSetSaveImages)
            {
                s += " AND SaveImages = @SaveImages ";
            }
            if (IsSetLastActiveDate)
            {
                s += " AND LastActiveDate = @LastActiveDate ";
            }
            if (IsSetDay_Actived)
            {
                s += " AND Day_Actived = @Day_Actived ";
            }
            if (IsSetDis_Activated)
            {
                s += " AND Dis_Activated = @Dis_Activated ";
            }
            if (IsSetProductKey)
            {
                s += " AND ProductKey = @ProductKey ";
            }
            if (IsSetSerailKey)
            {
                s += " AND SerailKey = @SerailKey ";
            }
            if (IsSetSecKey)
            {
                s += " AND SecKey = @SecKey ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}