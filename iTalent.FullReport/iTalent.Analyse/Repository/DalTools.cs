using System;
using System.Data;
using System.Data.OleDb;

namespace iTalent.Analyse.Repository
{
   public class DalTools
   {
      /*static internal object GetEmptyValue(Type type)
      {
         switch (type.Name.ToLower())
         {
            case "boolean":  return false;
            case "byte":     return (byte)0;
            case "byte[]":   return new byte[0];
            case "datetime": return DateTime.MinValue;
            case "decimal":  return (decimal)0;
            case "double":   return (double)0;
            case "float":    return (float)0;
            case "guid":     return Guid.Empty;
            case "int":      return (int)0;
            case "int16":    return (Int16)0;
            case "int32":    return (Int32)0;
            case "int64":    return (Int64)0;
            case "single":   return (float)0;
            case "string":   return string.Empty;
            default: throw new DalException("Unknown variable type: " + type.FullName);
         }
      }*/


       //static public void AddDbDataParameter(IDbCommand command, string name, object value, DbType dbType)
       //{
       //    IDbDataParameter parameter = command.CreateParameter();
       //    parameter.DbType = dbType;
       //    parameter.Direction = ParameterDirection.Input;
       //    parameter.ParameterName = "@" + name;
       //    //parameter.SourceColumn = name;
       //    if (value == null) { parameter.Value = DBNull.Value; }
       //    else { parameter.Value = value; }
       //    command.Parameters.Add(parameter);
       //}

        static public void AddDbDataParameter(IDbCommand command, string name, object value, DbType dbType)
        {
            OleDbCommand oleCommand = (OleDbCommand)command;
            string parameterName = "@" + name;
            oleCommand.Parameters.AddWithValue(parameterName, value);
        }


        static public void AddDbDataParameter(IDbCommand command, string name, object value, DbType dbType, bool isNull)
       {
           IDbDataParameter parameter = command.CreateParameter();
           parameter.DbType = dbType;
           parameter.Direction = ParameterDirection.Input;
           parameter.ParameterName = "@" + name;
           //parameter.SourceColumn = name;
           parameter.Value = isNull ? DBNull.Value : value;
           command.Parameters.Add(parameter);
       }
    }
}