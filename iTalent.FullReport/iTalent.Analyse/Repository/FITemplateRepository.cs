using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFITemplateRepository
    {
        int Delete(FITemplate obj);
        int Insert(FITemplate obj);
        int Update(FITemplate obj);
        List<FITemplate> FindListByQuery(string query);
        FITemplate FindByKey(Int32 ID);
        FITemplate FindByName(string templateName);
        List<FITemplate> FindAll();
        List<FITemplate> FindByCondition(FITemplateConditionForm condt);
        int DeleteByCondition(FITemplateConditionForm condt);
    }

    /// <summary>
    /// Summary description for FITemplate.
    /// </summary>
    public class FITemplateRepository : Repository, IFITemplateRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FITemplateRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			Name, 
			TemplatePermit, 
			TemplatePoint, 
			TemplateFileName FROM FITemplate WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FITemplate SET AgencyID = @AgencyID, 
			Name = @Name, 
			TemplatePermit = @TemplatePermit, 
			TemplatePoint = @TemplatePoint, 
			TemplateFileName = @TemplateFileName WHERE (ID = @ID)";
            _SQLUpdate = query;

            query = @"INSERT INTO FITemplate (AgencyID, 
			Name, 
			TemplatePermit, 
			TemplatePoint, 
			TemplateFileName) VALUES (@AgencyID, 
			@Name, 
			@TemplatePermit, 
			@TemplatePoint, 
			@TemplateFileName)";
            _SQLInsert = query;

            query = @"DELETE FROM FITemplate WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			Name, 
			TemplatePermit, 
			TemplatePoint, 
			TemplateFileName 
		FROM 
			FITemplate";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FITemplate obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FITemplate obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FITemplate obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FITemplate> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        public FITemplate FindByName(string templateName)
        {
            IDbCommand command = DbConnection.CreateCommand();
            string query = @"SELECT * FROM FITemplate WHERE Name = @TemplateName";
            command.CommandText = query;
            DalTools.AddDbDataParameter(command, "TemplateName", templateName, DbType.String);
            List<FITemplate> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        protected List<FITemplate> FindListByCommand(IDbCommand command)
        {
            List<FITemplate> list = new List<FITemplate>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FITemplate obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FITemplate();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FITemplate obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (int) reader["AgencyID"];
                }
                if (!(reader["Name"] is DBNull))
                {
                    obj.Name = (string) reader["Name"];
                }
                if (!(reader["TemplatePermit"] is DBNull))
                {
                    obj.TemplatePermit = (string) reader["TemplatePermit"];
                }
                if (!(reader["TemplatePoint"] is DBNull))
                {
                    obj.TemplatePoint = (decimal) reader["TemplatePoint"];
                }
                if (!(reader["TemplateFileName"] is DBNull))
                {
                    obj.TemplateFileName = (string) reader["TemplateFileName"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FITemplate FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FITemplate> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FITemplate> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FITemplate> list = FindListByCommand(command);
            return list;
        }

        public List<FITemplate> FindByCondition(FITemplateConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FITemplate " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FITemplate> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FITemplateConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FITemplate " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FITemplateConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FITemplateConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
            DalTools.AddDbDataParameter(command, "TemplatePermit", obj.TemplatePermit, DbType.String);
            DalTools.AddDbDataParameter(command, "TemplatePoint", obj.TemplatePoint, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TemplateFileName", obj.TemplateFileName, DbType.String);
        }


        protected void FillParamToCommand(IDbCommand command, FITemplate obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
            DalTools.AddDbDataParameter(command, "TemplatePermit", obj.TemplatePermit, DbType.String);
            DalTools.AddDbDataParameter(command, "TemplatePoint", obj.TemplatePoint, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TemplateFileName", obj.TemplateFileName, DbType.String);
        }

        #endregion
    }
}