using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIMQChartRepository
    {
        int Delete(FIMQChart obj);
        int Insert(FIMQChart obj);
        int Update(FIMQChart obj);
        List<FIMQChart> FindListByQuery(string query);
        List<FIMQChart> FindByRecordId(int reportId);
        FIMQChart FindByKey(Int32 ID);
        List<FIMQChart> FindAll();
        List<FIMQChart> FindByCondition(FIMQChartConditionForm condt);
        int DeleteByCondition(FIMQChartConditionForm condt);
    }

	/// <summary>
	/// Summary description for FIMQChart.
	/// </summary>
	public class FIMQChartRepository : Repository, IFIMQChartRepository
	{
        #region Properties

        #endregion
        
        #region Constructor

        public FIMQChartRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;	
			this.DbTransaction = unitOfWorkAsync.DbTransaction;			
			InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery
	
	protected override void InitSqlQuery()
	{
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			MQID, 
			MQDesc, 
			MQValue FROM FIMQChart WHERE (ID = @ID)";
	    _SQLSelectByKey = query;

            query = @"UPDATE FIMQChart SET 
            AgencyID = @AgencyID, 
			ReportID = @ReportID, 
			MQID = @MQID, 
			MQDesc = @MQDesc, 
			MQValue = @MQValue WHERE (ID = @ID)";
	    _SQLUpdate      = query;

            query = @"INSERT INTO FIMQChart (
            AgencyID, 
			ReportID, 
			MQID, 
			MQDesc, 
			MQValue) VALUES (
            @AgencyID, 
			@ReportID, 
			@MQID, 
			@MQDesc, 
			@MQValue)";
	    _SQLInsert      = query;

            query = @"DELETE FROM FIMQChart WHERE (ID = @ID)";
	    _SQLDelete      = query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			MQID, 
			MQDesc, 
			MQValue 
		FROM 
			FIMQChart";
	    _SQLSelectAll   = query;
	}

        #endregion

        #region Save & Delete

        public int Insert(FIMQChart obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Update(FIMQChart obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIMQChart obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
				IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
				
                return res;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Command Execute & Read Properties

		public List<FIMQChart> FindListByQuery(string query)
		{
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
			return FindListByCommand(command);
		}

	    public List<FIMQChart> FindByRecordId(int reportId)
	    {
	        IDbCommand command;
	        command = DbConnection.CreateCommand();

	        string query = "SELECT * FROM FIMQChart WHERE [ReportID] = @ReportID;";
	        command.CommandText = query;
	        DalTools.AddDbDataParameter(command, "ReportID", reportId, DbType.Int32);

	        return FindListByCommand(command);
	    }

	    protected List<FIMQChart> FindListByCommand(IDbCommand command)
        {
            List<FIMQChart> list = new List<FIMQChart>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {	
				FIMQChart obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    	while (reader.Read()) 
						{ 
							obj = new FIMQChart();
							ReadProperties(obj, reader);
							list.Add(obj); 
						}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex) { throw ex; }
        }

      private void ReadProperties(FIMQChart obj, IDataReader reader)
      {
         try
         {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (int)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (int)reader["ReportID"];
		}
                if(!(reader["MQID"] is DBNull))
		{
			obj.MQID = (string)reader["MQID"];
		}
                if(!(reader["MQDesc"] is DBNull))
		{
			obj.MQDesc = (string)reader["MQDesc"];
		}
                if(!(reader["MQValue"] is DBNull))
		{
			obj.MQValue = (decimal)reader["MQValue"];
		}
         }
         catch (Exception ex)
         {
            //throw new DalException("Failed to read properties from DataReader.", ex);
            //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
		throw ex;
         }
      }

        #endregion
      
	public FIMQChart FindByKey(Int32 ID)
	{
		IDbCommand command  = DbConnection.CreateCommand();
		command.CommandText = _SQLSelectByKey + ";";
         DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
		List<FIMQChart> list =  FindListByCommand(command);

		if (list.Count == 0)
		{
			//throw new Exception("No data was returned"); 
                	return null;
            	}
            	return list[0];
	}

		public List<FIMQChart> FindAll()
		{
			IDbCommand command  = DbConnection.CreateCommand();
			command.CommandText = _SQLSelectAll + ";";
			List<FIMQChart> list =  FindListByCommand(command);
			return list;
		}

        public List<FIMQChart> FindByCondition(FIMQChartConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIMQChart " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIMQChart> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIMQChartConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIMQChart " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIMQChartConditionForm condt)
        {
			return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIMQChartConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
         DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
         DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
         DalTools.AddDbDataParameter(command, "MQID", obj.MQID, DbType.String);
         DalTools.AddDbDataParameter(command, "MQDesc", obj.MQDesc, DbType.String);
         DalTools.AddDbDataParameter(command, "MQValue", obj.MQValue, DbType.Decimal);
        }


        protected void FillParamToCommand(IDbCommand command, FIMQChart obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
         DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
         DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
         DalTools.AddDbDataParameter(command, "MQID", obj.MQID, DbType.String);
         DalTools.AddDbDataParameter(command, "MQDesc", obj.MQDesc, DbType.String);
         DalTools.AddDbDataParameter(command, "MQValue", obj.MQValue, DbType.Decimal);
        }

        #endregion
   }
}