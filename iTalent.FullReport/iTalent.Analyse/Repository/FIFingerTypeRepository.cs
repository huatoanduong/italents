using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIFingerTypeRepository
    {
        int Delete(FIFingerType obj);
        int Insert(FIFingerType obj);
        int Update(FIFingerType obj);
        List<FIFingerType> FindListByQuery(string query);
        FIFingerType FindByKey(Int32 ID);
        List<FIFingerType> FindAll();
        List<FIFingerType> FindByCondition(FIFingerTypeConditionForm condt);
        int DeleteByCondition(FIFingerTypeConditionForm condt);
    }

	/// <summary>
	/// Summary description for FIFingerType.
	/// </summary>
	public class FIFingerTypeRepository : Repository, IFIFingerTypeRepository
	{
        #region Properties

        #endregion
        
        #region Constructor

        public FIFingerTypeRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;	
			this.DbTransaction = unitOfWorkAsync.DbTransaction;			
			InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery
	
	protected override void InitSqlQuery()
	{
            string query;

            query = @"SELECT ID, 
			Name, 
			Types, 
			Remark FROM FIFingerType WHERE (ID = @ID)";
	    _SQLSelectByKey = query;

            query = @"UPDATE FIFingerType SET 
            [Name] = @Name, 
			[Types] = @Types, 
			Remark = @Remark WHERE (ID = @ID)";
	    _SQLUpdate      = query;

            query = @"INSERT INTO FIFingerType (
            [Name], 
			[Types], 
			[Remark]) VALUES (@Name, 
			@Types, 
			@Remark)";
	    _SQLInsert      = query;

            query = @"DELETE FROM FIFingerType WHERE (ID = @ID)";
	    _SQLDelete      = query;

            query = @"SELECT ID, 
			[Name], 
			[Types], 
			[Remark] 
		FROM 
			FIFingerType";
	    _SQLSelectAll   = query;
	}

        #endregion

        #region Save & Delete

        public int Insert(FIFingerType obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Update(FIFingerType obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIFingerType obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
				IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
				
                return res;
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region Command Execute & Read Properties

		public List<FIFingerType> FindListByQuery(string query)
		{
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
			return FindListByCommand(command);
		}
		
        protected List<FIFingerType> FindListByCommand(IDbCommand command)
        {
            List<FIFingerType> list = new List<FIFingerType>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {	
				FIFingerType obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    	while (reader.Read()) 
						{ 
							obj = new FIFingerType();
							ReadProperties(obj, reader);
							list.Add(obj); 
						}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex) { throw ex; }
        }

      private void ReadProperties(FIFingerType obj, IDataReader reader)
      {
         try
         {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["Name"] is DBNull))
		{
			obj.Name = (string)reader["Name"];
		}
                if(!(reader["Types"] is DBNull))
		{
			obj.Types = (string)reader["Types"];
		}
                if(!(reader["Remark"] is DBNull))
		{
			obj.Remark = (string)reader["Remark"];
		}
         }
         catch (Exception ex)
         {
            //throw new DalException("Failed to read properties from DataReader.", ex);
            //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
		throw ex;
         }
      }

        #endregion
      
	public FIFingerType FindByKey(Int32 ID)
	{
		IDbCommand command  = DbConnection.CreateCommand();
		command.CommandText = _SQLSelectByKey + ";";
         DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
		List<FIFingerType> list =  FindListByCommand(command);

		if (list.Count == 0)
		{
			//throw new Exception("No data was returned"); 
                	return null;
            	}
            	return list[0];
	}

		public List<FIFingerType> FindAll()
		{
			IDbCommand command  = DbConnection.CreateCommand();
			command.CommandText = _SQLSelectAll + ";";
			List<FIFingerType> list =  FindListByCommand(command);
			return list;
		}

        public List<FIFingerType> FindByCondition(FIFingerTypeConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIFingerType " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIFingerType> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIFingerTypeConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIFingerType " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIFingerTypeConditionForm condt)
        {
			return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIFingerTypeConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
         DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
         DalTools.AddDbDataParameter(command, "Types", obj.Types, DbType.String);
         DalTools.AddDbDataParameter(command, "Remark", obj.Remark, DbType.String);
        }


        protected void FillParamToCommand(IDbCommand command, FIFingerType obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
         DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
         DalTools.AddDbDataParameter(command, "Name", obj.Name, DbType.String);
         DalTools.AddDbDataParameter(command, "Types", obj.Types, DbType.String);
         DalTools.AddDbDataParameter(command, "Remark", obj.Remark, DbType.String);
        }

        #endregion
   }
}