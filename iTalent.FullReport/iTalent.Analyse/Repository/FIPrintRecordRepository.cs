using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Entity;
using iTalent.Utils;

namespace iTalent.Analyse.Repository
{
    public interface IFIPrintRecordRepository
    {
        int Delete(FIPrintRecord obj);
        int Insert(FIPrintRecord obj);
        int Update(FIPrintRecord obj);
        FIPrintRecord CreateEntity();
        List<FIPrintRecord> FindListByQuery(string query);
        FIPrintRecord FindByKey(Int32 ID);
        List<FIPrintRecord> FindAll();
        List<FIPrintRecord> FindByCondition(FIPrintRecordConditionForm condt);
        int DeleteByCondition(FIPrintRecordConditionForm condt);
    }

    /// <summary>
    /// Summary description for FIPrintRecord.
    /// </summary>
    public class FIPrintRecordRepository : Repository, IFIPrintRecordRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FIPrintRecordRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        public FIPrintRecord CreateEntity()
        {
            return new FIPrintRecord();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			TemplateID, 
			ReportID, 
			PrintDate, 
			PointID FROM FIPrintRecord WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FIPrintRecord SET AgencyID = @AgencyID, 
			TemplateID = @TemplateID, 
			ReportID = @ReportID, 
			PrintDate = @PrintDate, 
			PointID = @PointID WHERE (ID = @ID)";
            _SQLUpdate = query;

            query = @"INSERT INTO FIPrintRecord ([AgencyID], 
			[TemplateID], 
			[ReportID], 
			[PrintDate], 
			[PointID]) VALUES (@AgencyID, 
			@TemplateID, 
			@ReportID, 
			@PrintDate, 
			@PointID)";
            _SQLInsert = query;

            query = @"DELETE FROM FIPrintRecord WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			TemplateID, 
			ReportID, 
			PrintDate, 
			PointID 
		FROM 
			FIPrintRecord";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FIPrintRecord obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FIPrintRecord obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIPrintRecord obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FIPrintRecord> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        protected List<FIPrintRecord> FindListByCommand(IDbCommand command)
        {
            List<FIPrintRecord> list = new List<FIPrintRecord>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FIPrintRecord obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FIPrintRecord();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FIPrintRecord obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (int) reader["AgencyID"];
                }
                if (!(reader["TemplateID"] is DBNull))
                {
                    obj.TemplateID = (int) reader["TemplateID"];
                }
                if (!(reader["ReportID"] is DBNull))
                {
                    obj.ReportID = (int) reader["ReportID"];
                }
                if (!(reader["PrintDate"] is DBNull))
                {
                    obj.PrintDate = (DateTime) reader["PrintDate"];
                }
                if (!(reader["PointID"] is DBNull))
                {
                    obj.PointID = (decimal) reader["PointID"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FIPrintRecord FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FIPrintRecord> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIPrintRecord> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FIPrintRecord> list = FindListByCommand(command);
            return list;
        }

        public List<FIPrintRecord> FindByCondition(FIPrintRecordConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIPrintRecord " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIPrintRecord> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIPrintRecordConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIPrintRecord " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIPrintRecordConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIPrintRecordConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TemplateID", obj.TemplateID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "PrintDate", obj.PrintDate.ToDbFormatString(), DbType.String);
            DalTools.AddDbDataParameter(command, "PointID", obj.PointID, DbType.Decimal);
        }


        protected void FillParamToCommand(IDbCommand command, FIPrintRecord obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TemplateID", obj.TemplateID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "PrintDate", obj.PrintDate.ToDbFormatString(), DbType.String);
            DalTools.AddDbDataParameter(command, "PointID", obj.PointID, DbType.Decimal);
        }

        #endregion
    }
}