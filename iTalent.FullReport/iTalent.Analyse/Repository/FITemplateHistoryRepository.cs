using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Entity;
using iTalent.Utils;

namespace iTalent.Analyse.Repository
{
    public interface IFITemplateHistoryRepository
    {
        int Delete(FITemplateHistory obj);
        int Insert(FITemplateHistory obj);
        int Update(FITemplateHistory obj);
        List<FITemplateHistory> FindListByQuery(string query);
        FITemplateHistory FindByKey(Int32 ID);
        FITemplateHistory FindByName(string templateName);
        List<FITemplateHistory> FindAll();
        List<FITemplateHistory> FindByCondition(FITemplateHistoryConditionForm condt);
        int DeleteByCondition(FITemplateHistoryConditionForm condt);
    }

    /// <summary>
    /// Summary description for FITemplateHistory.
    /// </summary>
    public class FITemplateHistoryRepository : Repository, IFITemplateHistoryRepository
    {
        #region Properties

        #endregion

        #region Constructor

        public FITemplateHistoryRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			TemplateName, 
			DateCreate, 
			KeyEn, 
			Remark FROM FITemplateHistory WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FITemplateHistory SET AgencyID = @AgencyID, 
			TemplateName = @TemplateName, 
			DateCreate = @DateCreate, 
			KeyEn = @KeyEn, 
			Remark = @Remark WHERE (ID = @ID)";
            _SQLUpdate = query;

            query = @"INSERT INTO FITemplateHistory (
            AgencyID, 
			TemplateName, 
			DateCreate, 
			KeyEn, 
			Remark) VALUES (
            @AgencyID, 
			@TemplateName, 
			@DateCreate, 
			@KeyEn, 
			@Remark)";
            _SQLInsert = query;

            query = @"DELETE FROM FITemplateHistory WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			TemplateName, 
			DateCreate, 
			KeyEn, 
			Remark 
		FROM 
			FITemplateHistory";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FITemplateHistory obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FITemplateHistory obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillParamToCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FITemplateHistory obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FITemplateHistory> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        protected List<FITemplateHistory> FindListByCommand(IDbCommand command)
        {
            List<FITemplateHistory> list = new List<FITemplateHistory>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FITemplateHistory obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FITemplateHistory();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FITemplateHistory obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (Int32) reader["AgencyID"];
                }
                if (!(reader["TemplateName"] is DBNull))
                {
                    obj.TemplateName = (string) reader["TemplateName"];
                }
                if (!(reader["DateCreate"] is DBNull))
                {
                    obj.DateCreate = (DateTime) reader["DateCreate"];
                }
                if (!(reader["KeyEn"] is DBNull))
                {
                    obj.KeyEn = (string) reader["KeyEn"];
                }
                if (!(reader["Remark"] is DBNull))
                {
                    obj.Remark = (string) reader["Remark"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FITemplateHistory FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FITemplateHistory> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public FITemplateHistory FindByName(string templateName)
        {
            IDbCommand command = DbConnection.CreateCommand();
            string query = @"SELECT * FROM FITemplateHistory WHERE TemplateName = @TemplateName";
            command.CommandText = query;
            DalTools.AddDbDataParameter(command, "TemplateName", templateName, DbType.String);
            List<FITemplateHistory> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FITemplateHistory> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FITemplateHistory> list = FindListByCommand(command);
            return list;
        }

        public List<FITemplateHistory> FindByCondition(FITemplateHistoryConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FITemplateHistory " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FITemplateHistory> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FITemplateHistoryConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FITemplateHistory " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FITemplateHistoryConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FITemplateHistoryConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Int32);
            DalTools.AddDbDataParameter(command, "TemplateName", obj.TemplateName, DbType.String);
            DalTools.AddDbDataParameter(command, "DateCreate", obj.DateCreate.ToDbFormatString(), DbType.String);
            DalTools.AddDbDataParameter(command, "KeyEn", obj.KeyEn, DbType.String);
            DalTools.AddDbDataParameter(command, "Remark", obj.Remark, DbType.String);
        }


        protected void FillParamToCommand(IDbCommand command, FITemplateHistory obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Int32);
            DalTools.AddDbDataParameter(command, "TemplateName", obj.TemplateName, DbType.String);
            DalTools.AddDbDataParameter(command, "DateCreate", obj.DateCreate.ToDbFormatString(), DbType.String);
            DalTools.AddDbDataParameter(command, "KeyEn", obj.KeyEn, DbType.String);
            DalTools.AddDbDataParameter(command, "Remark", obj.Remark, DbType.String);
        }

        #endregion
    }
}