using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
    public interface IFIFingerAnalysisRepository
    {
        int Delete(FIFingerAnalysis obj);
        int Insert(FIFingerAnalysis obj);
        int Update(FIFingerAnalysis obj);

        FIFingerAnalysis FindByReportId(int reportId);

        List<FIFingerAnalysis> FindListByQuery(string query);
        FIFingerAnalysis FindByKey(Int32 ID);
        List<FIFingerAnalysis> FindAll();
        List<FIFingerAnalysis> FindByCondition(FIFingerAnalysisConditionForm condt);

        int DeleteByCondition(FIFingerAnalysisConditionForm condt);
    }

    /// <summary>
    /// Summary description for FIFingerAnalysis.
    /// </summary>
    public class FIFingerAnalysisRepository : Repository, IFIFingerAnalysisRepository
    {
        #region Properties

        protected string _SQLUpdate2 { get; set; }
        protected string _SQLInsert2 { get; set; }

        #endregion

        #region Constructor

        public FIFingerAnalysisRepository(IUnitOfWorkAsync unitOfWorkAsync)
        {
            this.DbConnection = unitOfWorkAsync.DbConnection;
            this.DbTransaction = unitOfWorkAsync.DbTransaction;
            InitSqlQuery();
        }

        #endregion

        #region InitSqlQuery

        protected override void InitSqlQuery()
        {
            string query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			LT_BLS, 
			RT_BLS, 
			Page_BLS, 
			SAP, 
			SBP, 
			SCP, 
			SDP, 
			SER, 
			TFRC, 
			EQVP, 
			IQVP, 
			AQVP, 
			CQVP, 
			L1RCP, 
			L2RCP, 
			L3RCP, 
			L4RCP, 
			L5RCP, 
			R1RCP, 
			R2RCP, 
			R3RCP, 
			R4RCP, 
			R5RCP, 
			L1RCP1, 
			L2RCP2, 
			L3RCP3, 
			L4RCP4, 
			L5RCP5, 
			R1RCP1, 
			R2RCP2, 
			R3RCP3, 
			R4RCP4, 
			R5RCP5, 
			ATDR, 
			ATDL, 
			ATDRA, 
			ATDRS, 
			ATDLA, 
			ATDLS, 
			AC2WP, 
			AC2UP, 
			AC2RP, 
			AC2XP, 
			AC2SP, 
			RCAP, 
			RCHP, 
			RCVP, 
			T1AP, 
			T1BP, 
			M1Q, 
			M2Q, 
			M3Q, 
			M4Q, 
			M5Q, 
			M6Q, 
			M7Q, 
			M8Q, 
			L1RCB, 
			L2RCB, 
			L3RCB, 
			L4RCB, 
			L5RCB, 
			R1RCB, 
			R2RCB, 
			R3RCB, 
			R4RCB, 
			R5RCB, 
			Label1, 
			Label2, 
			Label3, 
			Label4, 
			Label5, 
			Label6, 
			L1T, 
			L2T, 
			L3T, 
			L4T, 
			L5T, 
			R1T, 
			R2T, 
			R3T, 
			R4T, 
			R5T, 
			S1BZ, 
			S2BZ, 
			S3BZ, 
			S4BZ, 
			S5BZ, 
			S6BZ, 
			S7BZ, 
			S8BZ, 
			S9BZ, 
			S10BZ, 
			S11BZ, 
			S12BZ, 
			S13BZ, 
			S14BZ, 
			S15BZ, 
			S16BZ, 
			S17BZ, 
			S18BZ, 
			S1BZZ, 
			S2BZZ, 
			S3BZZ, 
			S4BZZ, 
			S5BZZ, 
			S6BZZ, 
			S7BZZ, 
			S8BZZ, 
			S9BZZ, 
			S10BZZ, 
			S11BZZ, 
			S12BZZ, 
			S13BZZ, 
			S14BZZ, 
			S15BZZ, 
			S16BZZ, 
			S17BZZ, 
			S18BZZ, 
			M1QPR, 
			M2QPR, 
			M3QPR, 
			M4QPR, 
			M5QPR, 
			M6QPR, 
			M7QPR, 
			M8QPR, 
			FPType1, 
			FPType2, 
			FPType3, 
			FPType4, 
			FPType5, 
			FPType6, 
			FPType7, 
			FPType8, 
			FPType9, 
			FPType10, 
			FPType11, 
			FPType12, 
			FPType13, 
			FPType14, 
			FPType15 FROM FIFingerAnalysis WHERE (ID = @ID)";
            _SQLSelectByKey = query;

            query = @"UPDATE FIFingerAnalysis SET 
            [AgencyID] = @AgencyID, 
			[ReportID] = @ReportID, 
			[LT_BLS] = @LT_BLS, 
			[RT_BLS] = @RT_BLS, 
			[Page_BLS] = @Page_BLS, 
			[SAP] = @SAP, 
			[SBP] = @SBP, 
			[SCP] = @SCP, 
			[SDP] = @SDP, 
			[SER] = @SER, 
			[TFRC] = @TFRC, 
			[EQVP] = @EQVP, 
			[IQVP] = @IQVP, 
			[AQVP] = @AQVP, 
			[CQVP] = @CQVP, 
			[L1RCP] = @L1RCP, 
			[L2RCP] = @L2RCP, 
			[L3RCP] = @L3RCP, 
			[L4RCP] = @L4RCP, 
			[L5RCP] = @L5RCP, 
			[R1RCP] = @R1RCP, 
			[R2RCP] = @R2RCP, 
			[R3RCP] = @R3RCP, 
			[R4RCP] = @R4RCP, 
			[R5RCP] = @R5RCP, 
			[L1RCP1] = @L1RCP1, 
			[L2RCP2] = @L2RCP2, 
			[L3RCP3] = @L3RCP3, 
			[L4RCP4] = @L4RCP4, 
			[L5RCP5] = @L5RCP5, 
			[R1RCP1] = @R1RCP1, 
			[R2RCP2] = @R2RCP2, 
			[R3RCP3] = @R3RCP3, 
			[R4RCP4] = @R4RCP4, 
			[R5RCP5] = @R5RCP5, 
			[ATDR] = @ATDR, 
			[ATDL] = @ATDL, 
			[ATDRA] = @ATDRA, 
			[ATDRS] = @ATDRS, 
			[ATDLA] = @ATDLA, 
			[ATDLS] = @ATDLS, 
			[AC2WP] = @AC2WP, 
			[AC2UP] = @AC2UP, 
			[AC2RP] = @AC2RP, 
			[AC2XP] = @AC2XP, 
			[AC2SP] = @AC2SP, 
			[RCAP] = @RCAP, 
			[RCHP] = @RCHP, 
			[RCVP] = @RCVP, 
			[T1AP] = @T1AP, 
			[T1BP] = @T1BP, 
			[M1Q] = @M1Q, 
			[M2Q] = @M2Q, 
			[M3Q] = @M3Q, 
			[M4Q] = @M4Q, 
			[M5Q] = @M5Q, 
			[M6Q] = @M6Q, 
			[M7Q] = @M7Q, 
			[M8Q] = @M8Q, 
			[L1RCB] = @L1RCB, 
			[L2RCB] = @L2RCB, 
			[L3RCB] = @L3RCB, 
			[L4RCB] = @L4RCB, 
			[L5RCB] = @L5RCB, 
			[R1RCB] = @R1RCB, 
			[R2RCB] = @R2RCB, 
			[R3RCB] = @R3RCB, 
			[R4RCB] = @R4RCB, 
			[R5RCB] = @R5RCB, 
			[Label1] = @Label1, 
			[Label2] = @Label2, 
			[Label3] = @Label3, 
			[Label4] = @Label4, 
			[Label5] = @Label5, 
			[Label6] = @Label6, 
			[L1T] = @L1T, 
			[L2T] = @L2T, 
			[L3T] = @L3T, 
			[L4T] = @L4T, 
			[L5T] = @L5T, 
			[R1T] = @R1T, 
			[R2T] = @R2T, 
			[R3T] = @R3T, 
			[R4T] = @R4T, 
			[R5T] = @R5T
            WHERE ([ID] = @ID)";
            _SQLUpdate = query;

            query = @"
            UPDATE FIFingerAnalysis SET 
            [S1BZ] = @S1BZ, 
			[S2BZ] = @S2BZ, 
			[S3BZ] = @S3BZ, 
			[S4BZ] = @S4BZ, 
			[S5BZ] = @S5BZ, 
			[S6BZ] = @S6BZ, 
			[S7BZ] = @S7BZ, 
			[S8BZ] = @S8BZ, 
			[S9BZ] = @S9BZ, 
			[S10BZ] = @S10BZ, 
			[S11BZ] = @S11BZ, 
			[S12BZ] = @S12BZ, 
			[S13BZ] = @S13BZ, 
			[S14BZ] = @S14BZ, 
			[S15BZ] = @S15BZ, 
			[S16BZ] = @S16BZ, 
			[S17BZ] = @S17BZ, 
			[S18BZ] = @S18BZ, 
			[S1BZZ] = @S1BZZ, 
			[S2BZZ] = @S2BZZ, 
			[S3BZZ] = @S3BZZ, 
			[S4BZZ] = @S4BZZ, 
			[S5BZZ] = @S5BZZ, 
			[S6BZZ] = @S6BZZ, 
			[S7BZZ] = @S7BZZ, 
			[S8BZZ] = @S8BZZ, 
			[S9BZZ] = @S9BZZ, 
			[S10BZZ] = @S10BZZ, 
			[S11BZZ] = @S11BZZ, 
			[S12BZZ] = @S12BZZ, 
			[S13BZZ] = @S13BZZ, 
			[S14BZZ] = @S14BZZ, 
			[S15BZZ] = @S15BZZ, 
			[S16BZZ] = @S16BZZ, 
			[S17BZZ] = @S17BZZ, 
			[S18BZZ] = @S18BZZ, 
			[M1QPR] = @M1QPR, 
			[M2QPR] = @M2QPR, 
			[M3QPR] = @M3QPR, 
			[M4QPR] = @M4QPR, 
			[M5QPR] = @M5QPR, 
			[M6QPR] = @M6QPR, 
			[M7QPR] = @M7QPR, 
			[M8QPR] = @M8QPR, 
			[FPType1] = @FPType1, 
			[FPType2] = @FPType2, 
			[FPType3] = @FPType3, 
			[FPType4] = @FPType4, 
			[FPType5] = @FPType5, 
			[FPType6] = @FPType6, 
			[FPType7] = @FPType7, 
			[FPType8] = @FPType8, 
			[FPType9] = @FPType9, 
			[FPType10] = @FPType10, 
			[FPType11] = @FPType11, 
			[FPType12] = @FPType12, 
			[FPType13] = @FPType13, 
			[FPType14] = @FPType14, 
			[FPType15] = @FPType15 
            WHERE ([ID] = @ID) ";
            _SQLUpdate2 = query;

            query = @"INSERT INTO FIFingerAnalysis (
            [AgencyID], 
			[ReportID], 
			[LT_BLS], 
			[RT_BLS], 
			[Page_BLS], 
			[SAP], 
			[SBP], 
			[SCP], 
			[SDP], 
			[SER], 
			[TFRC], 
			[EQVP], 
			[IQVP], 
			[AQVP], 
			[CQVP], 
			[L1RCP], 
			[L2RCP], 
			[L3RCP], 
			[L4RCP], 
			[L5RCP], 
			[R1RCP], 
			[R2RCP], 
			[R3RCP], 
			[R4RCP], 
			[R5RCP], 
			[L1RCP1], 
			[L2RCP2], 
			[L3RCP3], 
			[L4RCP4], 
			[L5RCP5], 
			[R1RCP1], 
			[R2RCP2], 
			[R3RCP3], 
			[R4RCP4], 
			[R5RCP5], 
			[ATDR], 
			[ATDL], 
			[ATDRA], 
			[ATDRS], 
			[ATDLA], 
			[ATDLS], 
			[AC2WP], 
			[AC2UP], 
			[AC2RP], 
			[AC2XP], 
			[AC2SP], 
			[RCAP], 
			[RCHP], 
			[RCVP], 
			[T1AP], 
			[T1BP], 
			[M1Q], 
			[M2Q], 
			[M3Q], 
			[M4Q], 
			[M5Q], 
			[M6Q], 
			[M7Q], 
			[M8Q], 
			[L1RCB], 
			[L2RCB], 
			[L3RCB], 
			[L4RCB], 
			[L5RCB], 
			[R1RCB], 
			[R2RCB], 
			[R3RCB], 
			[R4RCB], 
			[R5RCB], 
			[Label1], 
			[Label2], 
			[Label3], 
			[Label4], 
			[Label5], 
			[Label6], 
			[L1T], 
			[L2T], 
			[L3T], 
			[L4T], 
			[L5T], 
			[R1T], 
			[R2T], 
			[R3T], 
			[R4T], 
			[R5T], 
			[S1BZ], 
			[S2BZ], 
			[S3BZ], 
			[S4BZ], 
			[S5BZ], 
			[S6BZ], 
			[S7BZ], 
			[S8BZ], 
			[S9BZ], 
			[S10BZ], 
			[S11BZ], 
			[S12BZ], 
			[S13BZ], 
			[S14BZ], 
			[S15BZ], 
			[S16BZ], 
			[S17BZ], 
			[S18BZ], 
			[S1BZZ], 
			[S2BZZ], 
			[S3BZZ], 
			[S4BZZ], 
			[S5BZZ], 
			[S6BZZ], 
			[S7BZZ], 
			[S8BZZ], 
			[S9BZZ], 
			[S10BZZ], 
			[S11BZZ], 
			[S12BZZ], 
			[S13BZZ], 
			[S14BZZ], 
			[S15BZZ], 
			[S16BZZ], 
			[S17BZZ], 
			[S18BZZ], 
			[M1QPR], 
			[M2QPR], 
			[M3QPR], 
			[M4QPR], 
			[M5QPR], 
			[M6QPR], 
			[M7QPR], 
			[M8QPR], 
			[FPType1], 
			[FPType2], 
			[FPType3], 
			[FPType4], 
			[FPType5], 
			[FPType6], 
			[FPType7], 
			[FPType8], 
			[FPType9], 
			[FPType10], 
			[FPType11], 
			[FPType12], 
			[FPType13], 
			[FPType14], 
			[FPType15]
            ) VALUES (
            @AgencyID, 
			@ReportID, 
			@LT_BLS, 
			@RT_BLS, 
			@Page_BLS, 
			@SAP, 
			@SBP, 
			@SCP, 
			@SDP, 
			@SER, 
			@TFRC, 
			@EQVP, 
			@IQVP, 
			@AQVP, 
			@CQVP, 
			@L1RCP, 
			@L2RCP, 
			@L3RCP, 
			@L4RCP, 
			@L5RCP, 
			@R1RCP, 
			@R2RCP, 
			@R3RCP, 
			@R4RCP, 
			@R5RCP, 
			@L1RCP1, 
			@L2RCP2, 
			@L3RCP3, 
			@L4RCP4, 
			@L5RCP5, 
			@R1RCP1, 
			@R2RCP2, 
			@R3RCP3, 
			@R4RCP4, 
			@R5RCP5, 
			@ATDR, 
			@ATDL, 
			@ATDRA, 
			@ATDRS, 
			@ATDLA, 
			@ATDLS, 
			@AC2WP, 
			@AC2UP, 
			@AC2RP, 
			@AC2XP, 
			@AC2SP, 
			@RCAP, 
			@RCHP, 
			@RCVP, 
			@T1AP, 
			@T1BP, 
			@M1Q, 
			@M2Q, 
			@M3Q, 
			@M4Q, 
			@M5Q, 
			@M6Q, 
			@M7Q, 
			@M8Q, 
			@L1RCB, 
			@L2RCB, 
			@L3RCB, 
			@L4RCB, 
			@L5RCB, 
			@R1RCB, 
			@R2RCB, 
			@R3RCB, 
			@R4RCB, 
			@R5RCB, 
			@Label1, 
			@Label2, 
			@Label3, 
			@Label4, 
			@Label5, 
			@Label6, 
			@L1T, 
			@L2T, 
			@L3T, 
			@L4T, 
			@L5T, 
			@R1T, 
			@R2T, 
			@R3T, 
			@R4T, 
			@R5T, 
			@S1BZ, 
			@S2BZ, 
			@S3BZ, 
			@S4BZ, 
			@S5BZ, 
			@S6BZ, 
			@S7BZ, 
			@S8BZ, 
			@S9BZ, 
			@S10BZ, 
			@S11BZ, 
			@S12BZ, 
			@S13BZ, 
			@S14BZ, 
			@S15BZ, 
			@S16BZ, 
			@S17BZ, 
			@S18BZ, 
			@S1BZZ, 
			@S2BZZ, 
			@S3BZZ, 
			@S4BZZ, 
			@S5BZZ, 
			@S6BZZ, 
			@S7BZZ, 
			@S8BZZ, 
			@S9BZZ, 
			@S10BZZ, 
			@S11BZZ, 
			@S12BZZ, 
			@S13BZZ, 
			@S14BZZ, 
			@S15BZZ, 
			@S16BZZ, 
			@S17BZZ, 
			@S18BZZ, 
			@M1QPR, 
			@M2QPR, 
			@M3QPR, 
			@M4QPR, 
			@M5QPR, 
			@M6QPR, 
			@M7QPR, 
			@M8QPR, 
			@FPType1, 
			@FPType2, 
			@FPType3, 
			@FPType4, 
			@FPType5, 
			@FPType6, 
			@FPType7, 
			@FPType8, 
			@FPType9, 
			@FPType10, 
			@FPType11, 
			@FPType12, 
			@FPType13, 
			@FPType14, 
			@FPType15)";
            _SQLInsert = query;

            query = @"DELETE FROM FIFingerAnalysis WHERE (ID = @ID)";
            _SQLDelete = query;

            query = @"SELECT ID, 
			AgencyID, 
			ReportID, 
			LT_BLS, 
			RT_BLS, 
			Page_BLS, 
			SAP, 
			SBP, 
			SCP, 
			SDP, 
			SER, 
			TFRC, 
			EQVP, 
			IQVP, 
			AQVP, 
			CQVP, 
			L1RCP, 
			L2RCP, 
			L3RCP, 
			L4RCP, 
			L5RCP, 
			R1RCP, 
			R2RCP, 
			R3RCP, 
			R4RCP, 
			R5RCP, 
			L1RCP1, 
			L2RCP2, 
			L3RCP3, 
			L4RCP4, 
			L5RCP5, 
			R1RCP1, 
			R2RCP2, 
			R3RCP3, 
			R4RCP4, 
			R5RCP5, 
			ATDR, 
			ATDL, 
			ATDRA, 
			ATDRS, 
			ATDLA, 
			ATDLS, 
			AC2WP, 
			AC2UP, 
			AC2RP, 
			AC2XP, 
			AC2SP, 
			RCAP, 
			RCHP, 
			RCVP, 
			T1AP, 
			T1BP, 
			M1Q, 
			M2Q, 
			M3Q, 
			M4Q, 
			M5Q, 
			M6Q, 
			M7Q, 
			M8Q, 
			L1RCB, 
			L2RCB, 
			L3RCB, 
			L4RCB, 
			L5RCB, 
			R1RCB, 
			R2RCB, 
			R3RCB, 
			R4RCB, 
			R5RCB, 
			Label1, 
			Label2, 
			Label3, 
			Label4, 
			Label5, 
			Label6, 
			L1T, 
			L2T, 
			L3T, 
			L4T, 
			L5T, 
			R1T, 
			R2T, 
			R3T, 
			R4T, 
			R5T, 
			S1BZ, 
			S2BZ, 
			S3BZ, 
			S4BZ, 
			S5BZ, 
			S6BZ, 
			S7BZ, 
			S8BZ, 
			S9BZ, 
			S10BZ, 
			S11BZ, 
			S12BZ, 
			S13BZ, 
			S14BZ, 
			S15BZ, 
			S16BZ, 
			S17BZ, 
			S18BZ, 
			S1BZZ, 
			S2BZZ, 
			S3BZZ, 
			S4BZZ, 
			S5BZZ, 
			S6BZZ, 
			S7BZZ, 
			S8BZZ, 
			S9BZZ, 
			S10BZZ, 
			S11BZZ, 
			S12BZZ, 
			S13BZZ, 
			S14BZZ, 
			S15BZZ, 
			S16BZZ, 
			S17BZZ, 
			S18BZZ, 
			M1QPR, 
			M2QPR, 
			M3QPR, 
			M4QPR, 
			M5QPR, 
			M6QPR, 
			M7QPR, 
			M8QPR, 
			FPType1, 
			FPType2, 
			FPType3, 
			FPType4, 
			FPType5, 
			FPType6, 
			FPType7, 
			FPType8, 
			FPType9, 
			FPType10, 
			FPType11, 
			FPType12, 
			FPType13, 
			FPType14, 
			FPType15 
		FROM 
			FIFingerAnalysis";
            _SQLSelectAll = query;
        }

        #endregion

        #region Save & Delete

        public int Insert(FIFingerAnalysis obj)
        {
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLInsert;
                FillParamToCommand(command, obj);

                return ExecuteCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Update(FIFingerAnalysis obj)
        {
            try
            {
                int res = 0;
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate;
                FillUpdateCommand(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                res += ExecuteCommand(command);

                command = DbConnection.CreateCommand();
                command.CommandText = _SQLUpdate2;
                FillUpdateCommand2(command, obj);
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                res += ExecuteCommand(command);

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(FIFingerAnalysis obj)
        {
            if (DbConnection == null)
            {
                //throw new DalException("Connection has not been set.");
                throw new Exception("Connection has not been set.");
            }
            try
            {
                IDbCommand command;
                command = DbConnection.CreateCommand();
                command.CommandText = _SQLDelete + ";";
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
                int res = ExecuteCommand(command);
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Command Execute & Read Properties

        public List<FIFingerAnalysis> FindListByQuery(string query)
        {
            IDbCommand command;
            command = DbConnection.CreateCommand();
            command.CommandText = query;
            return FindListByCommand(command);
        }

        public FIFingerAnalysis FindByReportId(int reportId)
        {
            IDbCommand command;
            string query = "SELECT * FROM FIFingerAnalysis WHERE [ReportID] = @ReportID;";
            command = DbConnection.CreateCommand();
            command.CommandText =
                command.CommandText = query;
            DalTools.AddDbDataParameter(command, "ReportID", reportId, DbType.Int32);

            List<FIFingerAnalysis> list = FindListByCommand(command);
            if (list.Count == 0)
            {
                return null;
            }
            return list[0];
        }

        protected List<FIFingerAnalysis> FindListByCommand(IDbCommand command)
        {
            List<FIFingerAnalysis> list = new List<FIFingerAnalysis>();
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }
            try
            {
                FIFingerAnalysis obj;
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        obj = new FIFingerAnalysis();
                        ReadProperties(obj, reader);
                        list.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        protected int ExecuteCommand(IDbCommand command)
        {
            if (DbConnection == null) { throw new Exception("Connection has not been set."); }

            try
            {
                if (DbConnection.State != ConnectionState.Open) { DbConnection.Open(); }
                if (DbTransaction != null) { command.Transaction = DbTransaction; }
                int res = command.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("Zero rows were affected.");
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ReadProperties(FIFingerAnalysis obj, IDataReader reader)
        {
            try
            {
                if (!(reader["ID"] is DBNull))
                {
                    obj.ID = (Int32) reader["ID"];
                }
                if (!(reader["AgencyID"] is DBNull))
                {
                    obj.AgencyID = (int) reader["AgencyID"];
                }
                if (!(reader["ReportID"] is DBNull))
                {
                    obj.ReportID = (int) reader["ReportID"];
                }
                if (!(reader["LT_BLS"] is DBNull))
                {
                    obj.LT_BLS = (decimal) reader["LT_BLS"];
                }
                if (!(reader["RT_BLS"] is DBNull))
                {
                    obj.RT_BLS = (decimal) reader["RT_BLS"];
                }
                if (!(reader["Page_BLS"] is DBNull))
                {
                    obj.Page_BLS = (decimal) reader["Page_BLS"];
                }
                if (!(reader["SAP"] is DBNull))
                {
                    obj.SAP = (decimal) reader["SAP"];
                }
                if (!(reader["SBP"] is DBNull))
                {
                    obj.SBP = (decimal) reader["SBP"];
                }
                if (!(reader["SCP"] is DBNull))
                {
                    obj.SCP = (decimal) reader["SCP"];
                }
                if (!(reader["SDP"] is DBNull))
                {
                    obj.SDP = (decimal) reader["SDP"];
                }
                if (!(reader["SER"] is DBNull))
                {
                    obj.SER = (decimal) reader["SER"];
                }
                if (!(reader["TFRC"] is DBNull))
                {
                    obj.TFRC = (decimal) reader["TFRC"];
                }
                if (!(reader["EQVP"] is DBNull))
                {
                    obj.EQVP = (decimal) reader["EQVP"];
                }
                if (!(reader["IQVP"] is DBNull))
                {
                    obj.IQVP = (decimal) reader["IQVP"];
                }
                if (!(reader["AQVP"] is DBNull))
                {
                    obj.AQVP = (decimal) reader["AQVP"];
                }
                if (!(reader["CQVP"] is DBNull))
                {
                    obj.CQVP = (decimal) reader["CQVP"];
                }
                if (!(reader["L1RCP"] is DBNull))
                {
                    obj.L1RCP = (decimal) reader["L1RCP"];
                }
                if (!(reader["L2RCP"] is DBNull))
                {
                    obj.L2RCP = (decimal) reader["L2RCP"];
                }
                if (!(reader["L3RCP"] is DBNull))
                {
                    obj.L3RCP = (decimal) reader["L3RCP"];
                }
                if (!(reader["L4RCP"] is DBNull))
                {
                    obj.L4RCP = (decimal) reader["L4RCP"];
                }
                if (!(reader["L5RCP"] is DBNull))
                {
                    obj.L5RCP = (decimal) reader["L5RCP"];
                }
                if (!(reader["R1RCP"] is DBNull))
                {
                    obj.R1RCP = (decimal) reader["R1RCP"];
                }
                if (!(reader["R2RCP"] is DBNull))
                {
                    obj.R2RCP = (decimal) reader["R2RCP"];
                }
                if (!(reader["R3RCP"] is DBNull))
                {
                    obj.R3RCP = (decimal) reader["R3RCP"];
                }
                if (!(reader["R4RCP"] is DBNull))
                {
                    obj.R4RCP = (decimal) reader["R4RCP"];
                }
                if (!(reader["R5RCP"] is DBNull))
                {
                    obj.R5RCP = (decimal) reader["R5RCP"];
                }
                if (!(reader["L1RCP1"] is DBNull))
                {
                    obj.L1RCP1 = (decimal) reader["L1RCP1"];
                }
                if (!(reader["L2RCP2"] is DBNull))
                {
                    obj.L2RCP2 = (decimal) reader["L2RCP2"];
                }
                if (!(reader["L3RCP3"] is DBNull))
                {
                    obj.L3RCP3 = (decimal) reader["L3RCP3"];
                }
                if (!(reader["L4RCP4"] is DBNull))
                {
                    obj.L4RCP4 = (decimal) reader["L4RCP4"];
                }
                if (!(reader["L5RCP5"] is DBNull))
                {
                    obj.L5RCP5 = (decimal) reader["L5RCP5"];
                }
                if (!(reader["R1RCP1"] is DBNull))
                {
                    obj.R1RCP1 = (decimal) reader["R1RCP1"];
                }
                if (!(reader["R2RCP2"] is DBNull))
                {
                    obj.R2RCP2 = (decimal) reader["R2RCP2"];
                }
                if (!(reader["R3RCP3"] is DBNull))
                {
                    obj.R3RCP3 = (decimal) reader["R3RCP3"];
                }
                if (!(reader["R4RCP4"] is DBNull))
                {
                    obj.R4RCP4 = (decimal) reader["R4RCP4"];
                }
                if (!(reader["R5RCP5"] is DBNull))
                {
                    obj.R5RCP5 = (decimal) reader["R5RCP5"];
                }
                if (!(reader["ATDR"] is DBNull))
                {
                    obj.ATDR = (decimal) reader["ATDR"];
                }
                if (!(reader["ATDL"] is DBNull))
                {
                    obj.ATDL = (decimal) reader["ATDL"];
                }
                if (!(reader["ATDRA"] is DBNull))
                {
                    obj.ATDRA = (string) reader["ATDRA"];
                }
                if (!(reader["ATDRS"] is DBNull))
                {
                    obj.ATDRS = (string) reader["ATDRS"];
                }
                if (!(reader["ATDLA"] is DBNull))
                {
                    obj.ATDLA = (string) reader["ATDLA"];
                }
                if (!(reader["ATDLS"] is DBNull))
                {
                    obj.ATDLS = (string) reader["ATDLS"];
                }
                if (!(reader["AC2WP"] is DBNull))
                {
                    obj.AC2WP = (decimal) reader["AC2WP"];
                }
                if (!(reader["AC2UP"] is DBNull))
                {
                    obj.AC2UP = (decimal) reader["AC2UP"];
                }
                if (!(reader["AC2RP"] is DBNull))
                {
                    obj.AC2RP = (decimal) reader["AC2RP"];
                }
                if (!(reader["AC2XP"] is DBNull))
                {
                    obj.AC2XP = (decimal) reader["AC2XP"];
                }
                if (!(reader["AC2SP"] is DBNull))
                {
                    obj.AC2SP = (decimal) reader["AC2SP"];
                }
                if (!(reader["RCAP"] is DBNull))
                {
                    obj.RCAP = (decimal) reader["RCAP"];
                }
                if (!(reader["RCHP"] is DBNull))
                {
                    obj.RCHP = (decimal) reader["RCHP"];
                }
                if (!(reader["RCVP"] is DBNull))
                {
                    obj.RCVP = (decimal) reader["RCVP"];
                }
                if (!(reader["T1AP"] is DBNull))
                {
                    obj.T1AP = (decimal) reader["T1AP"];
                }
                if (!(reader["T1BP"] is DBNull))
                {
                    obj.T1BP = (decimal) reader["T1BP"];
                }
                if (!(reader["M1Q"] is DBNull))
                {
                    obj.M1Q = (decimal) reader["M1Q"];
                }
                if (!(reader["M2Q"] is DBNull))
                {
                    obj.M2Q = (decimal) reader["M2Q"];
                }
                if (!(reader["M3Q"] is DBNull))
                {
                    obj.M3Q = (decimal) reader["M3Q"];
                }
                if (!(reader["M4Q"] is DBNull))
                {
                    obj.M4Q = (decimal) reader["M4Q"];
                }
                if (!(reader["M5Q"] is DBNull))
                {
                    obj.M5Q = (decimal) reader["M5Q"];
                }
                if (!(reader["M6Q"] is DBNull))
                {
                    obj.M6Q = (decimal) reader["M6Q"];
                }
                if (!(reader["M7Q"] is DBNull))
                {
                    obj.M7Q = (decimal) reader["M7Q"];
                }
                if (!(reader["M8Q"] is DBNull))
                {
                    obj.M8Q = (decimal) reader["M8Q"];
                }
                if (!(reader["L1RCB"] is DBNull))
                {
                    obj.L1RCB = (decimal) reader["L1RCB"];
                }
                if (!(reader["L2RCB"] is DBNull))
                {
                    obj.L2RCB = (decimal) reader["L2RCB"];
                }
                if (!(reader["L3RCB"] is DBNull))
                {
                    obj.L3RCB = (decimal) reader["L3RCB"];
                }
                if (!(reader["L4RCB"] is DBNull))
                {
                    obj.L4RCB = (decimal) reader["L4RCB"];
                }
                if (!(reader["L5RCB"] is DBNull))
                {
                    obj.L5RCB = (decimal) reader["L5RCB"];
                }
                if (!(reader["R1RCB"] is DBNull))
                {
                    obj.R1RCB = (decimal) reader["R1RCB"];
                }
                if (!(reader["R2RCB"] is DBNull))
                {
                    obj.R2RCB = (decimal) reader["R2RCB"];
                }
                if (!(reader["R3RCB"] is DBNull))
                {
                    obj.R3RCB = (decimal) reader["R3RCB"];
                }
                if (!(reader["R4RCB"] is DBNull))
                {
                    obj.R4RCB = (decimal) reader["R4RCB"];
                }
                if (!(reader["R5RCB"] is DBNull))
                {
                    obj.R5RCB = (decimal) reader["R5RCB"];
                }
                if (!(reader["Label1"] is DBNull))
                {
                    obj.Label1 = (string) reader["Label1"];
                }
                if (!(reader["Label2"] is DBNull))
                {
                    obj.Label2 = (string) reader["Label2"];
                }
                if (!(reader["Label3"] is DBNull))
                {
                    obj.Label3 = (string) reader["Label3"];
                }
                if (!(reader["Label4"] is DBNull))
                {
                    obj.Label4 = (string) reader["Label4"];
                }
                if (!(reader["Label5"] is DBNull))
                {
                    obj.Label5 = (string) reader["Label5"];
                }
                if (!(reader["Label6"] is DBNull))
                {
                    obj.Label6 = (string) reader["Label6"];
                }
                if (!(reader["L1T"] is DBNull))
                {
                    obj.L1T = (string) reader["L1T"];
                }
                if (!(reader["L2T"] is DBNull))
                {
                    obj.L2T = (string) reader["L2T"];
                }
                if (!(reader["L3T"] is DBNull))
                {
                    obj.L3T = (string) reader["L3T"];
                }
                if (!(reader["L4T"] is DBNull))
                {
                    obj.L4T = (string) reader["L4T"];
                }
                if (!(reader["L5T"] is DBNull))
                {
                    obj.L5T = (string) reader["L5T"];
                }
                if (!(reader["R1T"] is DBNull))
                {
                    obj.R1T = (string) reader["R1T"];
                }
                if (!(reader["R2T"] is DBNull))
                {
                    obj.R2T = (string) reader["R2T"];
                }
                if (!(reader["R3T"] is DBNull))
                {
                    obj.R3T = (string) reader["R3T"];
                }
                if (!(reader["R4T"] is DBNull))
                {
                    obj.R4T = (string) reader["R4T"];
                }
                if (!(reader["R5T"] is DBNull))
                {
                    obj.R5T = (string) reader["R5T"];
                }
                if (!(reader["S1BZ"] is DBNull))
                {
                    obj.S1BZ = (decimal) reader["S1BZ"];
                }
                if (!(reader["S2BZ"] is DBNull))
                {
                    obj.S2BZ = (decimal) reader["S2BZ"];
                }
                if (!(reader["S3BZ"] is DBNull))
                {
                    obj.S3BZ = (decimal) reader["S3BZ"];
                }
                if (!(reader["S4BZ"] is DBNull))
                {
                    obj.S4BZ = (decimal) reader["S4BZ"];
                }
                if (!(reader["S5BZ"] is DBNull))
                {
                    obj.S5BZ = (decimal) reader["S5BZ"];
                }
                if (!(reader["S6BZ"] is DBNull))
                {
                    obj.S6BZ = (decimal) reader["S6BZ"];
                }
                if (!(reader["S7BZ"] is DBNull))
                {
                    obj.S7BZ = (decimal) reader["S7BZ"];
                }
                if (!(reader["S8BZ"] is DBNull))
                {
                    obj.S8BZ = (decimal) reader["S8BZ"];
                }
                if (!(reader["S9BZ"] is DBNull))
                {
                    obj.S9BZ = (decimal) reader["S9BZ"];
                }
                if (!(reader["S10BZ"] is DBNull))
                {
                    obj.S10BZ = (decimal) reader["S10BZ"];
                }
                if (!(reader["S11BZ"] is DBNull))
                {
                    obj.S11BZ = (decimal) reader["S11BZ"];
                }
                if (!(reader["S12BZ"] is DBNull))
                {
                    obj.S12BZ = (decimal) reader["S12BZ"];
                }
                if (!(reader["S13BZ"] is DBNull))
                {
                    obj.S13BZ = (decimal) reader["S13BZ"];
                }
                if (!(reader["S14BZ"] is DBNull))
                {
                    obj.S14BZ = (decimal) reader["S14BZ"];
                }
                if (!(reader["S15BZ"] is DBNull))
                {
                    obj.S15BZ = (decimal) reader["S15BZ"];
                }
                if (!(reader["S16BZ"] is DBNull))
                {
                    obj.S16BZ = (decimal) reader["S16BZ"];
                }
                if (!(reader["S17BZ"] is DBNull))
                {
                    obj.S17BZ = (decimal) reader["S17BZ"];
                }
                if (!(reader["S18BZ"] is DBNull))
                {
                    obj.S18BZ = (decimal) reader["S18BZ"];
                }
                if (!(reader["S1BZZ"] is DBNull))
                {
                    obj.S1BZZ = (string) reader["S1BZZ"];
                }
                if (!(reader["S2BZZ"] is DBNull))
                {
                    obj.S2BZZ = (string) reader["S2BZZ"];
                }
                if (!(reader["S3BZZ"] is DBNull))
                {
                    obj.S3BZZ = (string) reader["S3BZZ"];
                }
                if (!(reader["S4BZZ"] is DBNull))
                {
                    obj.S4BZZ = (string) reader["S4BZZ"];
                }
                if (!(reader["S5BZZ"] is DBNull))
                {
                    obj.S5BZZ = (string) reader["S5BZZ"];
                }
                if (!(reader["S6BZZ"] is DBNull))
                {
                    obj.S6BZZ = (string) reader["S6BZZ"];
                }
                if (!(reader["S7BZZ"] is DBNull))
                {
                    obj.S7BZZ = (string) reader["S7BZZ"];
                }
                if (!(reader["S8BZZ"] is DBNull))
                {
                    obj.S8BZZ = (string) reader["S8BZZ"];
                }
                if (!(reader["S9BZZ"] is DBNull))
                {
                    obj.S9BZZ = (string) reader["S9BZZ"];
                }
                if (!(reader["S10BZZ"] is DBNull))
                {
                    obj.S10BZZ = (string) reader["S10BZZ"];
                }
                if (!(reader["S11BZZ"] is DBNull))
                {
                    obj.S11BZZ = (string) reader["S11BZZ"];
                }
                if (!(reader["S12BZZ"] is DBNull))
                {
                    obj.S12BZZ = (string) reader["S12BZZ"];
                }
                if (!(reader["S13BZZ"] is DBNull))
                {
                    obj.S13BZZ = (string) reader["S13BZZ"];
                }
                if (!(reader["S14BZZ"] is DBNull))
                {
                    obj.S14BZZ = (string) reader["S14BZZ"];
                }
                if (!(reader["S15BZZ"] is DBNull))
                {
                    obj.S15BZZ = (string) reader["S15BZZ"];
                }
                if (!(reader["S16BZZ"] is DBNull))
                {
                    obj.S16BZZ = (string) reader["S16BZZ"];
                }
                if (!(reader["S17BZZ"] is DBNull))
                {
                    obj.S17BZZ = (string) reader["S17BZZ"];
                }
                if (!(reader["S18BZZ"] is DBNull))
                {
                    obj.S18BZZ = (string) reader["S18BZZ"];
                }
                if (!(reader["M1QPR"] is DBNull))
                {
                    obj.M1QPR = (decimal) reader["M1QPR"];
                }
                if (!(reader["M2QPR"] is DBNull))
                {
                    obj.M2QPR = (decimal) reader["M2QPR"];
                }
                if (!(reader["M3QPR"] is DBNull))
                {
                    obj.M3QPR = (decimal) reader["M3QPR"];
                }
                if (!(reader["M4QPR"] is DBNull))
                {
                    obj.M4QPR = (decimal) reader["M4QPR"];
                }
                if (!(reader["M5QPR"] is DBNull))
                {
                    obj.M5QPR = (decimal) reader["M5QPR"];
                }
                if (!(reader["M6QPR"] is DBNull))
                {
                    obj.M6QPR = (decimal) reader["M6QPR"];
                }
                if (!(reader["M7QPR"] is DBNull))
                {
                    obj.M7QPR = (decimal) reader["M7QPR"];
                }
                if (!(reader["M8QPR"] is DBNull))
                {
                    obj.M8QPR = (decimal) reader["M8QPR"];
                }
                if (!(reader["FPType1"] is DBNull))
                {
                    obj.FPType1 = (decimal) reader["FPType1"];
                }
                if (!(reader["FPType2"] is DBNull))
                {
                    obj.FPType2 = (decimal) reader["FPType2"];
                }
                if (!(reader["FPType3"] is DBNull))
                {
                    obj.FPType3 = (decimal) reader["FPType3"];
                }
                if (!(reader["FPType4"] is DBNull))
                {
                    obj.FPType4 = (decimal) reader["FPType4"];
                }
                if (!(reader["FPType5"] is DBNull))
                {
                    obj.FPType5 = (decimal) reader["FPType5"];
                }
                if (!(reader["FPType6"] is DBNull))
                {
                    obj.FPType6 = (decimal) reader["FPType6"];
                }
                if (!(reader["FPType7"] is DBNull))
                {
                    obj.FPType7 = (decimal) reader["FPType7"];
                }
                if (!(reader["FPType8"] is DBNull))
                {
                    obj.FPType8 = (decimal) reader["FPType8"];
                }
                if (!(reader["FPType9"] is DBNull))
                {
                    obj.FPType9 = (decimal) reader["FPType9"];
                }
                if (!(reader["FPType10"] is DBNull))
                {
                    obj.FPType10 = (decimal) reader["FPType10"];
                }
                if (!(reader["FPType11"] is DBNull))
                {
                    obj.FPType11 = (decimal) reader["FPType11"];
                }
                if (!(reader["FPType12"] is DBNull))
                {
                    obj.FPType12 = (decimal) reader["FPType12"];
                }
                if (!(reader["FPType13"] is DBNull))
                {
                    obj.FPType13 = (decimal) reader["FPType13"];
                }
                if (!(reader["FPType14"] is DBNull))
                {
                    obj.FPType14 = (decimal) reader["FPType14"];
                }
                if (!(reader["FPType15"] is DBNull))
                {
                    obj.FPType15 = (decimal) reader["FPType15"];
                }
            }
            catch (Exception ex)
            {
                //throw new DalException("Failed to read properties from DataReader.", ex);
                //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
                throw ex;
            }
        }

        #endregion

        public FIFingerAnalysis FindByKey(Int32 ID)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectByKey + ";";
            DalTools.AddDbDataParameter(command, "ID", ID, DbType.Int32);
            List<FIFingerAnalysis> list = FindListByCommand(command);

            if (list.Count == 0)
            {
                //throw new Exception("No data was returned"); 
                return null;
            }
            return list[0];
        }

        public List<FIFingerAnalysis> FindAll()
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = _SQLSelectAll + ";";
            List<FIFingerAnalysis> list = FindListByCommand(command);
            return list;
        }

        public List<FIFingerAnalysis> FindByCondition(FIFingerAnalysisConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "SELECT * FROM FIFingerAnalysis " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            List<FIFingerAnalysis> list = FindListByCommand(command);
            return list;
        }

        public int DeleteByCondition(FIFingerAnalysisConditionForm condt)
        {
            IDbCommand command = DbConnection.CreateCommand();
            command.CommandText = "DELETE FROM FIFingerAnalysis " + GetCondtionQuery(condt) + ";";
            FillParamToCommand(command, condt);
            return ExecuteCommand(command);
        }

        #region Make Condition Query & Fill Param from Condition to Command

        protected string GetCondtionQuery(FIFingerAnalysisConditionForm condt)
        {
            return condt.ConditionQuery;
        }

        protected void FillParamToCommand(IDbCommand command, FIFingerAnalysisConditionForm obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "LT_BLS", obj.LT_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RT_BLS", obj.RT_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Page_BLS", obj.Page_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SAP", obj.SAP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SBP", obj.SBP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SCP", obj.SCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SDP", obj.SDP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SER", obj.SER, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TFRC", obj.TFRC, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "EQVP", obj.EQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "IQVP", obj.IQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AQVP", obj.AQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "CQVP", obj.CQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCP", obj.L1RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCP", obj.L2RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCP", obj.L3RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCP", obj.L4RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCP", obj.L5RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCP", obj.R1RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCP", obj.R2RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCP", obj.R3RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCP", obj.R4RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCP", obj.R5RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCP1", obj.L1RCP1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCP2", obj.L2RCP2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCP3", obj.L3RCP3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCP4", obj.L4RCP4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCP5", obj.L5RCP5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCP1", obj.R1RCP1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCP2", obj.R2RCP2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCP3", obj.R3RCP3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCP4", obj.R4RCP4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCP5", obj.R5RCP5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDR", obj.ATDR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDL", obj.ATDL, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDRA", obj.ATDRA, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDRS", obj.ATDRS, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDLA", obj.ATDLA, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDLS", obj.ATDLS, DbType.String);
            DalTools.AddDbDataParameter(command, "AC2WP", obj.AC2WP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2UP", obj.AC2UP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2RP", obj.AC2RP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2XP", obj.AC2XP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2SP", obj.AC2SP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCAP", obj.RCAP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCHP", obj.RCHP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCVP", obj.RCVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "T1AP", obj.T1AP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "T1BP", obj.T1BP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M1Q", obj.M1Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M2Q", obj.M2Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M3Q", obj.M3Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M4Q", obj.M4Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M5Q", obj.M5Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M6Q", obj.M6Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M7Q", obj.M7Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M8Q", obj.M8Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCB", obj.L1RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCB", obj.L2RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCB", obj.L3RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCB", obj.L4RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCB", obj.L5RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCB", obj.R1RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCB", obj.R2RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCB", obj.R3RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCB", obj.R4RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCB", obj.R5RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Label1", obj.Label1, DbType.String);
            DalTools.AddDbDataParameter(command, "Label2", obj.Label2, DbType.String);
            DalTools.AddDbDataParameter(command, "Label3", obj.Label3, DbType.String);
            DalTools.AddDbDataParameter(command, "Label4", obj.Label4, DbType.String);
            DalTools.AddDbDataParameter(command, "Label5", obj.Label5, DbType.String);
            DalTools.AddDbDataParameter(command, "Label6", obj.Label6, DbType.String);
            DalTools.AddDbDataParameter(command, "L1T", obj.L1T, DbType.String);
            DalTools.AddDbDataParameter(command, "L2T", obj.L2T, DbType.String);
            DalTools.AddDbDataParameter(command, "L3T", obj.L3T, DbType.String);
            DalTools.AddDbDataParameter(command, "L4T", obj.L4T, DbType.String);
            DalTools.AddDbDataParameter(command, "L5T", obj.L5T, DbType.String);
            DalTools.AddDbDataParameter(command, "R1T", obj.R1T, DbType.String);
            DalTools.AddDbDataParameter(command, "R2T", obj.R2T, DbType.String);
            DalTools.AddDbDataParameter(command, "R3T", obj.R3T, DbType.String);
            DalTools.AddDbDataParameter(command, "R4T", obj.R4T, DbType.String);
            DalTools.AddDbDataParameter(command, "R5T", obj.R5T, DbType.String);
            DalTools.AddDbDataParameter(command, "S1BZ", obj.S1BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S2BZ", obj.S2BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S3BZ", obj.S3BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S4BZ", obj.S4BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S5BZ", obj.S5BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S6BZ", obj.S6BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S7BZ", obj.S7BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S8BZ", obj.S8BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S9BZ", obj.S9BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S10BZ", obj.S10BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S11BZ", obj.S11BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S12BZ", obj.S12BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S13BZ", obj.S13BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S14BZ", obj.S14BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S15BZ", obj.S15BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S16BZ", obj.S16BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S17BZ", obj.S17BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S18BZ", obj.S18BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S1BZZ", obj.S1BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S2BZZ", obj.S2BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S3BZZ", obj.S3BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S4BZZ", obj.S4BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S5BZZ", obj.S5BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S6BZZ", obj.S6BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S7BZZ", obj.S7BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S8BZZ", obj.S8BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S9BZZ", obj.S9BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S10BZZ", obj.S10BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S11BZZ", obj.S11BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S12BZZ", obj.S12BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S13BZZ", obj.S13BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S14BZZ", obj.S14BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S15BZZ", obj.S15BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S16BZZ", obj.S16BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S17BZZ", obj.S17BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S18BZZ", obj.S18BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "M1QPR", obj.M1QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M2QPR", obj.M2QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M3QPR", obj.M3QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M4QPR", obj.M4QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M5QPR", obj.M5QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M6QPR", obj.M6QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M7QPR", obj.M7QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M8QPR", obj.M8QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType1", obj.FPType1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType2", obj.FPType2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType3", obj.FPType3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType4", obj.FPType4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType5", obj.FPType5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType6", obj.FPType6, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType7", obj.FPType7, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType8", obj.FPType8, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType9", obj.FPType9, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType10", obj.FPType10, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType11", obj.FPType11, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType12", obj.FPType12, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType13", obj.FPType13, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType14", obj.FPType14, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType15", obj.FPType15, DbType.Decimal);
        }

        protected void FillUpdateCommand(IDbCommand command, FIFingerAnalysis obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "LT_BLS", obj.LT_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RT_BLS", obj.RT_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Page_BLS", obj.Page_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SAP", obj.SAP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SBP", obj.SBP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SCP", obj.SCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SDP", obj.SDP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SER", obj.SER, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TFRC", obj.TFRC, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "EQVP", obj.EQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "IQVP", obj.IQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AQVP", obj.AQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "CQVP", obj.CQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCP", obj.L1RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCP", obj.L2RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCP", obj.L3RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCP", obj.L4RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCP", obj.L5RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCP", obj.R1RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCP", obj.R2RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCP", obj.R3RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCP", obj.R4RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCP", obj.R5RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCP1", obj.L1RCP1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCP2", obj.L2RCP2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCP3", obj.L3RCP3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCP4", obj.L4RCP4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCP5", obj.L5RCP5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCP1", obj.R1RCP1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCP2", obj.R2RCP2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCP3", obj.R3RCP3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCP4", obj.R4RCP4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCP5", obj.R5RCP5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDR", obj.ATDR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDL", obj.ATDL, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDRA", obj.ATDRA, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDRS", obj.ATDRS, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDLA", obj.ATDLA, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDLS", obj.ATDLS, DbType.String);
            DalTools.AddDbDataParameter(command, "AC2WP", obj.AC2WP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2UP", obj.AC2UP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2RP", obj.AC2RP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2XP", obj.AC2XP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2SP", obj.AC2SP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCAP", obj.RCAP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCHP", obj.RCHP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCVP", obj.RCVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "T1AP", obj.T1AP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "T1BP", obj.T1BP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M1Q", obj.M1Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M2Q", obj.M2Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M3Q", obj.M3Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M4Q", obj.M4Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M5Q", obj.M5Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M6Q", obj.M6Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M7Q", obj.M7Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M8Q", obj.M8Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCB", obj.L1RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCB", obj.L2RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCB", obj.L3RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCB", obj.L4RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCB", obj.L5RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCB", obj.R1RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCB", obj.R2RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCB", obj.R3RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCB", obj.R4RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCB", obj.R5RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Label1", obj.Label1, DbType.String);
            DalTools.AddDbDataParameter(command, "Label2", obj.Label2, DbType.String);
            DalTools.AddDbDataParameter(command, "Label3", obj.Label3, DbType.String);
            DalTools.AddDbDataParameter(command, "Label4", obj.Label4, DbType.String);
            DalTools.AddDbDataParameter(command, "Label5", obj.Label5, DbType.String);
            DalTools.AddDbDataParameter(command, "Label6", obj.Label6, DbType.String);
            DalTools.AddDbDataParameter(command, "L1T", obj.L1T, DbType.String);
            DalTools.AddDbDataParameter(command, "L2T", obj.L2T, DbType.String);
            DalTools.AddDbDataParameter(command, "L3T", obj.L3T, DbType.String);
            DalTools.AddDbDataParameter(command, "L4T", obj.L4T, DbType.String);
            DalTools.AddDbDataParameter(command, "L5T", obj.L5T, DbType.String);
            DalTools.AddDbDataParameter(command, "R1T", obj.R1T, DbType.String);
            DalTools.AddDbDataParameter(command, "R2T", obj.R2T, DbType.String);
            DalTools.AddDbDataParameter(command, "R3T", obj.R3T, DbType.String);
            DalTools.AddDbDataParameter(command, "R4T", obj.R4T, DbType.String);
            DalTools.AddDbDataParameter(command, "R5T", obj.R5T, DbType.String);
        }

        protected void FillUpdateCommand2(IDbCommand command, FIFingerAnalysis obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "S1BZ", obj.S1BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S2BZ", obj.S2BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S3BZ", obj.S3BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S4BZ", obj.S4BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S5BZ", obj.S5BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S6BZ", obj.S6BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S7BZ", obj.S7BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S8BZ", obj.S8BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S9BZ", obj.S9BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S10BZ", obj.S10BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S11BZ", obj.S11BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S12BZ", obj.S12BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S13BZ", obj.S13BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S14BZ", obj.S14BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S15BZ", obj.S15BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S16BZ", obj.S16BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S17BZ", obj.S17BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S18BZ", obj.S18BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S1BZZ", obj.S1BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S2BZZ", obj.S2BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S3BZZ", obj.S3BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S4BZZ", obj.S4BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S5BZZ", obj.S5BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S6BZZ", obj.S6BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S7BZZ", obj.S7BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S8BZZ", obj.S8BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S9BZZ", obj.S9BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S10BZZ", obj.S10BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S11BZZ", obj.S11BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S12BZZ", obj.S12BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S13BZZ", obj.S13BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S14BZZ", obj.S14BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S15BZZ", obj.S15BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S16BZZ", obj.S16BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S17BZZ", obj.S17BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S18BZZ", obj.S18BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "M1QPR", obj.M1QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M2QPR", obj.M2QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M3QPR", obj.M3QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M4QPR", obj.M4QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M5QPR", obj.M5QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M6QPR", obj.M6QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M7QPR", obj.M7QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M8QPR", obj.M8QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType1", obj.FPType1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType2", obj.FPType2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType3", obj.FPType3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType4", obj.FPType4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType5", obj.FPType5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType6", obj.FPType6, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType7", obj.FPType7, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType8", obj.FPType8, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType9", obj.FPType9, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType10", obj.FPType10, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType11", obj.FPType11, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType12", obj.FPType12, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType13", obj.FPType13, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType14", obj.FPType14, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType15", obj.FPType15, DbType.Decimal);
        }

        protected void FillParamToCommand(IDbCommand command, FIFingerAnalysis obj, bool isUsePk = false)
        {
            if (isUsePk)
            {
                DalTools.AddDbDataParameter(command, "ID", obj.ID, DbType.Int32);
            }
            DalTools.AddDbDataParameter(command, "AgencyID", obj.AgencyID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ReportID", obj.ReportID, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "LT_BLS", obj.LT_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RT_BLS", obj.RT_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Page_BLS", obj.Page_BLS, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SAP", obj.SAP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SBP", obj.SBP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SCP", obj.SCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SDP", obj.SDP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "SER", obj.SER, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "TFRC", obj.TFRC, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "EQVP", obj.EQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "IQVP", obj.IQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AQVP", obj.AQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "CQVP", obj.CQVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCP", obj.L1RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCP", obj.L2RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCP", obj.L3RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCP", obj.L4RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCP", obj.L5RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCP", obj.R1RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCP", obj.R2RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCP", obj.R3RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCP", obj.R4RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCP", obj.R5RCP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCP1", obj.L1RCP1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCP2", obj.L2RCP2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCP3", obj.L3RCP3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCP4", obj.L4RCP4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCP5", obj.L5RCP5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCP1", obj.R1RCP1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCP2", obj.R2RCP2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCP3", obj.R3RCP3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCP4", obj.R4RCP4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCP5", obj.R5RCP5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDR", obj.ATDR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDL", obj.ATDL, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "ATDRA", obj.ATDRA, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDRS", obj.ATDRS, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDLA", obj.ATDLA, DbType.String);
            DalTools.AddDbDataParameter(command, "ATDLS", obj.ATDLS, DbType.String);
            DalTools.AddDbDataParameter(command, "AC2WP", obj.AC2WP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2UP", obj.AC2UP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2RP", obj.AC2RP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2XP", obj.AC2XP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "AC2SP", obj.AC2SP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCAP", obj.RCAP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCHP", obj.RCHP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "RCVP", obj.RCVP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "T1AP", obj.T1AP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "T1BP", obj.T1BP, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M1Q", obj.M1Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M2Q", obj.M2Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M3Q", obj.M3Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M4Q", obj.M4Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M5Q", obj.M5Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M6Q", obj.M6Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M7Q", obj.M7Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M8Q", obj.M8Q, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L1RCB", obj.L1RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L2RCB", obj.L2RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L3RCB", obj.L3RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L4RCB", obj.L4RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "L5RCB", obj.L5RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R1RCB", obj.R1RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R2RCB", obj.R2RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R3RCB", obj.R3RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R4RCB", obj.R4RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "R5RCB", obj.R5RCB, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "Label1", obj.Label1, DbType.String);
            DalTools.AddDbDataParameter(command, "Label2", obj.Label2, DbType.String);
            DalTools.AddDbDataParameter(command, "Label3", obj.Label3, DbType.String);
            DalTools.AddDbDataParameter(command, "Label4", obj.Label4, DbType.String);
            DalTools.AddDbDataParameter(command, "Label5", obj.Label5, DbType.String);
            DalTools.AddDbDataParameter(command, "Label6", obj.Label6, DbType.String);
            DalTools.AddDbDataParameter(command, "L1T", obj.L1T, DbType.String);
            DalTools.AddDbDataParameter(command, "L2T", obj.L2T, DbType.String);
            DalTools.AddDbDataParameter(command, "L3T", obj.L3T, DbType.String);
            DalTools.AddDbDataParameter(command, "L4T", obj.L4T, DbType.String);
            DalTools.AddDbDataParameter(command, "L5T", obj.L5T, DbType.String);
            DalTools.AddDbDataParameter(command, "R1T", obj.R1T, DbType.String);
            DalTools.AddDbDataParameter(command, "R2T", obj.R2T, DbType.String);
            DalTools.AddDbDataParameter(command, "R3T", obj.R3T, DbType.String);
            DalTools.AddDbDataParameter(command, "R4T", obj.R4T, DbType.String);
            DalTools.AddDbDataParameter(command, "R5T", obj.R5T, DbType.String);
            DalTools.AddDbDataParameter(command, "S1BZ", obj.S1BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S2BZ", obj.S2BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S3BZ", obj.S3BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S4BZ", obj.S4BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S5BZ", obj.S5BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S6BZ", obj.S6BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S7BZ", obj.S7BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S8BZ", obj.S8BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S9BZ", obj.S9BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S10BZ", obj.S10BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S11BZ", obj.S11BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S12BZ", obj.S12BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S13BZ", obj.S13BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S14BZ", obj.S14BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S15BZ", obj.S15BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S16BZ", obj.S16BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S17BZ", obj.S17BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S18BZ", obj.S18BZ, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "S1BZZ", obj.S1BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S2BZZ", obj.S2BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S3BZZ", obj.S3BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S4BZZ", obj.S4BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S5BZZ", obj.S5BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S6BZZ", obj.S6BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S7BZZ", obj.S7BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S8BZZ", obj.S8BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S9BZZ", obj.S9BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S10BZZ", obj.S10BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S11BZZ", obj.S11BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S12BZZ", obj.S12BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S13BZZ", obj.S13BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S14BZZ", obj.S14BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S15BZZ", obj.S15BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S16BZZ", obj.S16BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S17BZZ", obj.S17BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "S18BZZ", obj.S18BZZ, DbType.String);
            DalTools.AddDbDataParameter(command, "M1QPR", obj.M1QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M2QPR", obj.M2QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M3QPR", obj.M3QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M4QPR", obj.M4QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M5QPR", obj.M5QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M6QPR", obj.M6QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M7QPR", obj.M7QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "M8QPR", obj.M8QPR, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType1", obj.FPType1, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType2", obj.FPType2, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType3", obj.FPType3, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType4", obj.FPType4, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType5", obj.FPType5, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType6", obj.FPType6, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType7", obj.FPType7, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType8", obj.FPType8, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType9", obj.FPType9, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType10", obj.FPType10, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType11", obj.FPType11, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType12", obj.FPType12, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType13", obj.FPType13, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType14", obj.FPType14, DbType.Decimal);
            DalTools.AddDbDataParameter(command, "FPType15", obj.FPType15, DbType.Decimal);
        }

        #endregion
    }
}