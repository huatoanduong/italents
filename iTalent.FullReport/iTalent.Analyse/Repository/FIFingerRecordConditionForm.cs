using System;
using iTalent.Entity;

namespace iTalent.Analyse.Repository
{
	/// <summary>
	/// Summary description for FIFingerRecord.
	/// </summary>
	public class FIFingerRecordConditionForm
	{

      #region Fields

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _ReportID;
      private string _Type;
      private decimal _ATDPoint;
      private decimal _RCCount;
      private string _FingerType;
      private Int16 _ScanCheck;

      #endregion

      #region Properties

	public bool IsSetID;  
    public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { 
		_ID = value; 
		IsSetID = true;
		}
      }
	public bool IsSetAgencyID;
	public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { 
		_AgencyID = value; 
		IsSetAgencyID = true;
		}
      }
	public bool IsSetReportID;
	public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { 
		_ReportID = value; 
		IsSetReportID = true;
		}
      }
	public bool IsSetType;
	public bool IsTypeNullable
      { get { return true;  } }
      public string Type
      {
         get { return _Type;  }
         set { 
		_Type = value; 
		IsSetType = true;
		}
      }
	public bool IsSetATDPoint;
	public bool IsATDPointNullable
      { get { return true;  } }
      public decimal ATDPoint
      {
         get { return _ATDPoint;  }
         set { 
		_ATDPoint = value; 
		IsSetATDPoint = true;
		}
      }
	public bool IsSetRCCount;
	public bool IsRCCountNullable
      { get { return true;  } }
      public decimal RCCount
      {
         get { return _RCCount;  }
         set { 
		_RCCount = value; 
		IsSetRCCount = true;
		}
      }
	public bool IsSetFingerType;
	public bool IsFingerTypeNullable
      { get { return true;  } }
      public string FingerType
      {
         get { return _FingerType;  }
         set { 
		_FingerType = value; 
		IsSetFingerType = true;
		}
      }
	public bool IsSetScanCheck;
	public bool IsScanCheckNullable
      { get { return true;  } }
      public Int16 ScanCheck
      {
         get { return _ScanCheck;  }
         set { 
		_ScanCheck = value; 
		IsSetScanCheck = true;
		}
      }

	public string ConditionQuery
	{
		get { return GetCondtionQuery(); }
	}

      #endregion

      #region Constructors
      public FIFingerRecordConditionForm()
      {
         Reset();
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
	IsSetID = false;
         _AgencyID = EmptyValues.v_Int32;
	IsSetAgencyID = false;
         _ReportID = EmptyValues.v_Int32;
	IsSetReportID = false;
         _Type = EmptyValues.v_string;
	IsSetType = false;
         _ATDPoint = EmptyValues.v_decimal;
	IsSetATDPoint = false;
         _RCCount = EmptyValues.v_decimal;
	IsSetRCCount = false;
         _FingerType = EmptyValues.v_string;
	IsSetFingerType = false;
         _ScanCheck = EmptyValues.v_Int16;
	IsSetScanCheck = false;
      }

        private string GetCondtionQuery()
        {
            //if (obj == null) { return ""; }
            string s = "";
            if (IsSetID)
            {
                s += " AND ID = @ID ";
            }
            if (IsSetAgencyID)
            {
                s += " AND AgencyID = @AgencyID ";
            }
            if (IsSetReportID)
            {
                s += " AND ReportID = @ReportID ";
            }
            if (IsSetType)
            {
                s += " AND Type = @Type ";
            }
            if (IsSetATDPoint)
            {
                s += " AND ATDPoint = @ATDPoint ";
            }
            if (IsSetRCCount)
            {
                s += " AND RCCount = @RCCount ";
            }
            if (IsSetFingerType)
            {
                s += " AND FingerType = @FingerType ";
            }
            if (IsSetScanCheck)
            {
                s += " AND ScanCheck = @ScanCheck ";
            }

            if (s.Length > 0)
            {
                return " WHERE " + s.Substring(4);
            }
            return "";
        }
   }
}