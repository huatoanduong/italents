﻿using System;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using iTalent.Utils;

namespace iTalent.Analyse.Commons
{
    public static class HardwareInfo
    {
        /// <summary>
        ///     Retrieving Processor Id.
        /// </summary>
        /// <returns></returns>
        public static String GetProcessorId()
        {
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();
            String id = String.Empty;
            foreach (ManagementObject mo in moc.Cast<ManagementObject>())
            {
                id = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return id;
        }

        /// <summary>
        ///     Retrieving HDD Serial No.
        /// </summary>
        /// <returns></returns>
        public static String GetHddSerialNo()
        {
            ManagementClass mangnmt = new ManagementClass("Win32_LogicalDisk");
            ManagementObjectCollection mcol = mangnmt.GetInstances();
            return mcol.Cast<ManagementObject>()
                .Aggregate("", (current, strt) => current + Convert.ToString(strt["VolumeSerialNumber"]));
        }

        /// <summary>
        ///     Retrieving System MAC Address.
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = mc.GetInstances();
            string macAddress = String.Empty;
            foreach (ManagementObject mo in moc.Cast<ManagementObject>())
            {
                if (macAddress == String.Empty)
                {
                    if ((bool)mo["IPEnabled"]) macAddress = mo["MacAddress"].ToString();
                }
                mo.Dispose();
            }

            macAddress = macAddress.Replace(":", "");
            return macAddress;
        }

        /// <summary>
        ///     Retrieving Motherboard Manufacturer.
        /// </summary>
        /// <returns></returns>
        public static string GetBoardMaker()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
                "SELECT * FROM Win32_BaseBoard");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("Manufacturer").ToString();
                }

                catch
                {
                    return null;
                }
            }

            return "Board Maker: Unknown";
        }

        /// <summary>
        ///     Retrieving Motherboard Product Id.
        /// </summary>
        /// <returns></returns>
        public static string GetBoardProductId()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
                "SELECT * FROM Win32_BaseBoard");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("Product").ToString();
                }
                catch
                {
                    return null;
                }
            }

            return "Product: Unknown";
        }

        /// <summary>
        ///     Retrieving CD-DVD Drive Path.
        /// </summary>
        /// <returns></returns>
        public static string GetCdRomDrive()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
                "SELECT * FROM Win32_CDROMDrive");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("Drive").ToString();
                }
                catch
                {
                    return null;
                }
            }

            return "CD ROM Drive Letter: Unknown";
        }

        /// <summary>
        ///     Retrieving BIOS Maker.
        /// </summary>
        /// <returns></returns>
        public static string GetBioSmaker()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BIOS");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("Manufacturer").ToString();
                }

                catch
                {
                    return null;
                }
            }

            return "BIOS Maker: Unknown";
        }

        /// <summary>
        ///     Retrieving BIOS Serial No.
        /// </summary>
        /// <returns></returns>
        public static string GetBioSserNo()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BIOS");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("SerialNumber").ToString();
                }

                catch
                {
                    return null;
                }
            }

            return "BIOS Serial Number: Unknown";
        }

        /// <summary>
        ///     Retrieving BIOS Caption.
        /// </summary>
        /// <returns></returns>
        public static string GetBioScaption()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BIOS");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("Caption").ToString();
                }
                catch
                {
                    return null;
                }
            }
            return "BIOS Caption: Unknown";
        }

        /// <summary>
        ///     Retrieving System Account Name.
        /// </summary>
        /// <returns></returns>
        public static string GetAccountName()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2",
                "SELECT * FROM Win32_UserAccount");

            foreach (ManagementObject wmi in searcher.Get().Cast<ManagementObject>())
            {
                try
                {
                    return wmi.GetPropertyValue("Name").ToString();
                }
                catch
                {
                    return null;
                }
            }
            return "User Account Name: Unknown";
        }

        /// <summary>
        ///     Retrieving Physical Ram Memory.
        /// </summary>
        /// <returns></returns>
        public static string GetPhysicalMemory()
        {
            ManagementScope oMs = new ManagementScope();
            ObjectQuery oQuery = new ObjectQuery("SELECT Capacity FROM Win32_PhysicalMemory");
            ManagementObjectSearcher oSearcher = new ManagementObjectSearcher(oMs, oQuery);
            ManagementObjectCollection oCollection = oSearcher.Get();

            long memSize = oCollection.Cast<ManagementObject>().Sum(obj => Convert.ToInt64(obj["Capacity"]));

            // In case more than one Memory sticks are installed
            memSize = (memSize / 1024) / 1024;
            return memSize + "MB";
        }

        /// <summary>
        ///     Retrieving No of Ram Slot on Motherboard.
        /// </summary>
        /// <returns></returns>
        public static string GetNoRamSlots()
        {
            int memSlots = 0;
            ManagementScope oMs = new ManagementScope();
            ObjectQuery oQuery2 = new ObjectQuery("SELECT MemoryDevices FROM Win32_PhysicalMemoryArray");
            ManagementObjectSearcher oSearcher2 = new ManagementObjectSearcher(oMs, oQuery2);
            ManagementObjectCollection oCollection2 = oSearcher2.Get();
            foreach (ManagementObject obj in oCollection2.Cast<ManagementObject>())
            {
                memSlots = Convert.ToInt32(obj["MemoryDevices"]);
            }
            return memSlots.ToString(CultureInfo.InvariantCulture);
        }

        //Get CPU Temprature.
        /// <summary>
        ///     method for retrieving the CPU Manufacturer
        ///     using the WMI class
        /// </summary>
        /// <returns>CPU Manufacturer</returns>
        public static string GetCpuManufacturer()
        {
            string[] cpuMan = { String.Empty };
            //create an instance of the Managemnet class with the
            //Win32_Processor class
            ManagementClass mgmt = new ManagementClass("Win32_Processor");
            //create a ManagementObjectCollection to loop through
            ManagementObjectCollection objCol = mgmt.GetInstances();
            //start our loop for all processors found
            foreach (ManagementObject obj in objCol.Cast<ManagementObject>().Where(obj => cpuMan[0] == String.Empty))
            {
                // only return manufacturer from first CPU
                cpuMan[0] = obj.Properties["Manufacturer"].Value.ToString();
            }
            return cpuMan[0];
        }

        /// <summary>
        ///     method to retrieve the CPU's current
        ///     clock speed using the WMI class
        /// </summary>
        /// <returns>Clock speed</returns>
        public static int GetCpuCurrentClockSpeed()
        {
            int[] cpuClockSpeed = { 0 };
            //create an instance of the Managemnet class with the
            //Win32_Processor class
            ManagementClass mgmt = new ManagementClass("Win32_Processor");
            //create a ManagementObjectCollection to loop through
            ManagementObjectCollection objCol = mgmt.GetInstances();
            //start our loop for all processors found
            foreach (ManagementObject obj in objCol.Cast<ManagementObject>().Where(obj => cpuClockSpeed[0] == 0))
            {
                // only return cpuStatus from first CPU
                cpuClockSpeed[0] = Convert.ToInt32(obj.Properties["CurrentClockSpeed"].Value.ToString());
            }
            //return the status
            return cpuClockSpeed[0];
        }

        /// <summary>
        ///     method to retrieve the network adapters
        ///     default IP gateway using WMI
        /// </summary>
        /// <returns>adapters default IP gateway</returns>
        public static string GetDefaultIpGateway()
        {
            //create out management class object using the
            //Win32_NetworkAdapterConfiguration class to get the attributes
            //of the network adapter
            ManagementClass mgmt = new ManagementClass("Win32_NetworkAdapterConfiguration");
            //create our ManagementObjectCollection to get the attributes with
            ManagementObjectCollection objCol = mgmt.GetInstances();
            string gateway = String.Empty;
            //loop through all the objects we find
            foreach (ManagementBaseObject o in objCol)
            {
                ManagementObject obj = (ManagementObject)o;
                if (gateway == String.Empty) // only return MAC Address from first card
                {
                    //grab the value from the first network adapter we find
                    //you can change the string to an array and get all
                    //network adapters found as well
                    //check to see if the adapter's IPEnabled
                    //equals true
                    if ((bool)obj["IPEnabled"])
                    {
                        gateway = obj["DefaultIPGateway"].ToString();
                    }
                }
                //dispose of our object
                obj.Dispose();
            }
            //replace the ":" with an empty space, this could also
            //be removed if you wish
            gateway = gateway.Replace(":", "");
            //return the mac address
            return gateway;
        }

        /// <summary>
        ///     Retrieve CPU Speed.
        /// </summary>
        /// <returns></returns>
        public static double? GetCpuSpeedInGHz()
        {
            double? gHz = null;
            using (ManagementClass mc = new ManagementClass("Win32_Processor"))
            {
                foreach (ManagementBaseObject o in mc.GetInstances())
                {
                    ManagementObject mo = (ManagementObject)o;
                    gHz = 0.001 * (UInt32)mo.Properties["CurrentClockSpeed"].Value;
                    break;
                }
            }
            return gHz;
        }


        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        /// <summary>
        ///     Retrieve Win's Info.
        /// </summary>
        /// <returns></returns>
        public static string GetActiveWindowTitle()
        {
            const int nChars = 256;
            StringBuilder buff = new StringBuilder(nChars);
            IntPtr handle = GetForegroundWindow();

            return GetWindowText(handle, buff, nChars) > 0 ? buff.ToString() : null;
        }

        //Get name win
        public static string GetOsFriendlyName()
        {
            string result = string.Empty;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
            foreach (var os in searcher.Get().Cast<ManagementObject>())
            {
                result = os["Caption"].ToString();
                break;
            }
            return result;
        }

        public static string GetKeySystem()
        {
            Config config = new Config();
            var key = config.GetKeyAgency();
            if (key == "")
            {
                key = "i" + Security.Encrypt(GetMacAddress() + GetOsFriendlyName() + "!@italent@", GetMacAddress());
                config.SetKeyAgency(key);
            }
            return key;
        }

        public static string GetKeyA()
        {
            string key = GetKeySystem();
            char[] charStrings = key.ToCharArray();
            if (charStrings.Count() >= 7)
            {
                key = charStrings[0].ToString() + charStrings[8] + charStrings[5] + charStrings[10] + charStrings[3] +
                             charStrings[4] + charStrings[15];

                return key;
            }
            return null;
        }
        public static string GetKeyB()
        {
            string key = GetKeySystem();
            char[] charStrings = key.ToCharArray();
            if (charStrings.Count() >= 7)
            {
                key = charStrings[1].ToString() + charStrings[3] + charStrings[2] + charStrings[4] + charStrings[7] + charStrings[9] + charStrings[11];

                return Security.Encrypt(key, "@toro!");
            }
            return null;
        }

        public static string KeyB_De(string keyEn)
        {
            return Security.Decrypt(keyEn, "@toro!");
        }

        public static string GetKeyPos()
        {
            string key = GetKeySystem();
            char[] charStrings = key.ToCharArray();
            if (charStrings.Count() >= 7)
            {
                key = charStrings[0].ToString() + charStrings[8] + charStrings[5] + charStrings[10] + charStrings[3] +
                             charStrings[4] + charStrings[15] + charStrings[1] + charStrings[3] + charStrings[2] + charStrings[4] + charStrings[7] + charStrings[9] + charStrings[11];

                return key;
            }
            return null;
        }
    }
}