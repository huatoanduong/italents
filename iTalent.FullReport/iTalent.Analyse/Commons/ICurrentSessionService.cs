﻿﻿using System;
using System.IO;
using System.Windows.Forms;
﻿using iTalent.Entity;
// ReSharper disable All

namespace iTalent.Analyse.Commons
{
    public static class ICurrentSessionService
    {
        public enum StatusCustomer
        {
            Analysised = 1,
            Imported = 2,
            Exported = 3
        }

        public static int UserId { get; set; }
        public static string Username { get; set; }
        public static FIAgency CurAgency { get; set; }
        public static bool IsAdmin { get; set; }

        public static bool IsInstallDriver { get; set; }
        public static string Version { get; set; }
        public static string Serail { get; set; }
        public static string Seckey { get; set; }
        public static string Sec7Key { get; set; }
        public static string ProductKey { get; set; }


        public static string DesDirNameTemp = Directory.GetCurrentDirectory() + @"\Temp";
        public static string DesDirNameLoadTemp = Directory.GetCurrentDirectory() + @"\Temp\LoadTemp";

        private  static readonly string PathOs = Path.GetPathRoot(Environment.SystemDirectory);

        public static string TemplateFolderName = Path.Combine(PathOs, "FConfig", "template");
        
        public const string NGExcelFileName = "1.apk";
        public static string NGFExcelPath = Path.Combine(TemplateFolderName, NGExcelFileName);//"NGFTemplate.xlsx"

        public const string HeSoTiemNangExcelFileName = "2.apk";
        public static string HeSoTiemNangExcelPath = Path.Combine(TemplateFolderName, HeSoTiemNangExcelFileName);//"HeSoTiemNang.xlsx"

        public const string DacTinhTuDuyExcelFileName = "3.apk";
        public static string DacTinhTuDuyExcelPath = Path.Combine(TemplateFolderName, DacTinhTuDuyExcelFileName);//"DacTinhTuDuy.xlsx"

        public const string DacTinhTiemThucFileName = "4.apk";
        public static string DacTinhTiemThucExcelPath = Path.Combine(TemplateFolderName, DacTinhTiemThucFileName);//"DacTinhTiemThuc.xlsx"

        public const string Top3in16DescriptionExcelFileName = "5.apk";
        public static string Top3in16DescriptionExcelPath = Path.Combine(TemplateFolderName, Top3in16DescriptionExcelFileName);//"Top3In16.xlsx"

        public const string TinhCachHanhViExcelFileName_Adult = "6.apk";
        public static string TinhCachHanhViExcelPath_Adult = Path.Combine(TemplateFolderName, TinhCachHanhViExcelFileName_Adult);//"TinhCachHanhVi.xlsx"

        public const string KhuyenNghiExcelFileName_Adult = "7.apk";
        public static string KhuyenNghiExcelPath_Adult = Path.Combine(TemplateFolderName, KhuyenNghiExcelFileName_Adult);//"KhuyenNghi.xlsx"

        public const string TinhCachHanhViExcelFileName_Kid = "8.apk";
        public static string TinhCachHanhViExcelPath_Kid = Path.Combine(TemplateFolderName, TinhCachHanhViExcelFileName_Kid);//"TinhCachHanhVi.xlsx"

        public const string KhuyenNghiExcelFileName_Kid = "9.apk";
        public static string KhuyenNghiExcelPath_Kid = Path.Combine(TemplateFolderName, KhuyenNghiExcelFileName_Kid);//"KhuyenNghi.xlsx"

        private static string _DesDirNameTemplate = null;

        public static string DesDirNameTemplate
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_DesDirNameTemplate))
                {
                    string dir = Path.GetPathRoot(Environment.SystemDirectory);
                    if (System.Environment.Is64BitOperatingSystem)
                    {
                        dir += @"Windows\SysWOW64\FConfig";
                    }
                    else
                    {
                        dir += @"Windows\System32\FConfig";
                    }
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    _DesDirNameTemplate = dir;
                }
                return _DesDirNameTemplate;
            }
        }

        private static string _DesDirNameDatabase = null;

        public static string DesDirNameDatabase
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_DesDirNameDatabase))
                {

                    string dir = Path.GetPathRoot(Environment.SystemDirectory);
                    if (System.Environment.Is64BitOperatingSystem)
                    {
                        dir += @"Windows\SysWOW64\Database";
                    }
                    else
                    {
                        dir += @"Windows\System32\Database";
                    }
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    _DesDirNameDatabase = dir + @"\FINGERPRINTANALYSIS.mdb";
                }
                return _DesDirNameDatabase;
            }
        }

        public static string DesDirNameSource { get; set; } //= DirSaveImages + @"FingerprintCustomers";
        public static string DirSaveImages { get; set; }
        public static bool VietNamLanguage { get; set; }

        private static DateTime DateRegiter = new DateTime(2015, 09, 13);
        private static DateTime DateExpire = DateRegiter.AddDays(7);

        public static void CheckLicense()
        {
            DateTime today = DateTime.Now;
            TimeSpan trialDay = DateExpire.Subtract(today);
            if (trialDay.Days >= 0)
            {
                MessageBox.Show(
                    VietNamLanguage
                        ? "Bạn được sử dụng thêm " + trialDay.Days +
                          " ngày!\nVui lòng liên hệ nhà cung cấp để gia hạn sử dụng chương trình!"
                        : "Free - " + trialDay.Days + " day trials!\nPlease contact supplier!",
                    VietNamLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    VietNamLanguage
                        ? "Đã hết số ngày sử dụng.\nVui lòng liên hệ nhà cung cấp."
                        : "Expiration date on software!\nPlease contact supplier.",
                    VietNamLanguage ? "Thông Báo" : "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Environment.Exit(0);
            }

        }
    }
}