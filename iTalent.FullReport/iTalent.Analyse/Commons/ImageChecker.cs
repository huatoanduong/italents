﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace iTalent.Analyse.Commons
{
    public class ImageChecker
    {
        public ImageChecker()
        {
            ListFPNotCorrect = "";
            tempFP = "";
        }

        public string ListFPNotCorrect { get; set; }
        private string tempFP ;
        protected bool CheckImageBit(Image img)
        {
            //Image img = bmp;
            PixelFormat pixel = img.PixelFormat;
            return pixel == PixelFormat.Format24bppRgb || pixel == PixelFormat.Format8bppIndexed;
        }

        protected bool CheckImageKb(string pathImg)
        {
            var fileLength = new FileInfo(pathImg).Length;
            return fileLength <= 250000;
        }

        public bool CheckImage(string path)
        {
            string name = Path.GetFileName(path);
            if (name != null &&
                (name.Contains("1_1.b") || name.Contains("1_2.b") || name.Contains("1_3.b") || name.Contains("2_1.b") ||
                 name.Contains("2_2.b") || name.Contains("2_3.b") || name.Contains("3_1.b") || name.Contains("3_2.b") ||
                 name.Contains("3_3.b") || name.Contains("4_1.b") || name.Contains("4_2.b") || name.Contains("4_3.b") ||
                 name.Contains("5_1.b") || name.Contains("5_2.b") || name.Contains("5_3.b") || name.Contains("6_1.b") ||
                 name.Contains("6_2.b") || name.Contains("6_3.b") || name.Contains("7_1.b") || name.Contains("7_2.b") ||
                 name.Contains("7_3.b") || name.Contains("8_1.b") || name.Contains("8_2.b") || name.Contains("8_3.b") ||
                 name.Contains("9_1.b") || name.Contains("9_2.b") || name.Contains("9_3.b") || name.Contains("10_1.b") ||
                 name.Contains("10_2.b") || name.Contains("10_3.b")))
            {
                //string p = Path.ChangeExtension(path, ".bmp");
                Image bmp = Image.FromFile(path);
                bool kq = CheckImageBit(bmp) && CheckImageKb(path);
                if (!kq)
                {
                    int templen = "2_3.b; 2_3.b; 2_3.b; 2_3.b; 2_3.b; 2_3.b;".Length;
                    string xuonghang="";
                    if (tempFP.Length >= templen)
                    {
                        tempFP = "";
                        //tempFP += "\n";
                        xuonghang = "\n";
                    }
                    else xuonghang = "; ";
                    tempFP += xuonghang + name;

                    if (ListFPNotCorrect == "")
                        ListFPNotCorrect += name;
                    else
                        ListFPNotCorrect += xuonghang + name;
                }
                return kq;
            }
            return false;
        }
    }
}
