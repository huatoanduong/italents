﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestCurrentCulture
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal value = 1212.12m;
            string s = Math.Round((decimal)value, 2).ToString(CultureInfo.InvariantCulture);
            MessageBox.Show(s);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            decimal value = 1212.12m;
            string s = Math.Round((decimal)value, 2).ToString();
            MessageBox.Show(s);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            decimal value = 1212.12m;
            string s = Math.Round((decimal)value, 2).ToString(CultureInfo.GetCultureInfo("vi-VN"));
            MessageBox.Show(s);
        }
    }
}
