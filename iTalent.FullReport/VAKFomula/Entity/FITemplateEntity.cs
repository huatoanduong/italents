using System;

namespace VAKFomula.Entity
{
	/// <summary>
	/// Summary description for FITemplate.
	/// </summary>
	public partial class FITemplate
	{

      private Int32 _ID;
      private Int32 _AgencyID;
      private string _Name;
      private string _TemplatePermit;
      private decimal _TemplatePoint;
      private string _TemplateFileName;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { _Name = value; }
      }
      public bool IsTemplatePermitNullable
      { get { return true;  } }
      public string TemplatePermit
      {
         get { return _TemplatePermit;  }
         set { _TemplatePermit = value; }
      }
      public bool IsTemplatePointNullable
      { get { return true;  } }
      public decimal TemplatePoint
      {
         get { return _TemplatePoint;  }
         set { _TemplatePoint = value; }
      }
      public bool IsTemplateFileNameNullable
      { get { return true;  } }
      public string TemplateFileName
      {
         get { return _TemplateFileName;  }
         set { _TemplateFileName = value; }
      }

      #endregion

      #region Constructors
      public FITemplate()
      {
         Reset();
      }
      public FITemplate(FITemplate obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._Name = obj.Name;
	this._TemplatePermit = obj.TemplatePermit;
	this._TemplatePoint = obj.TemplatePoint;
	this._TemplateFileName = obj.TemplateFileName;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_Int32;
         _Name = EmptyValues.v_string;
         _TemplatePermit = EmptyValues.v_string;
         _TemplatePoint = EmptyValues.v_decimal;
         _TemplateFileName = EmptyValues.v_string;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (Int32)reader["AgencyID"];
		}
                if(!(reader["Name"] is DBNull))
		{
			obj.Name = (string)reader["Name"];
		}
                if(!(reader["TemplatePermit"] is DBNull))
		{
			obj.TemplatePermit = (string)reader["TemplatePermit"];
		}
                if(!(reader["TemplatePoint"] is DBNull))
		{
			obj.TemplatePoint = (decimal)reader["TemplatePoint"];
		}
                if(!(reader["TemplateFileName"] is DBNull))
		{
			obj.TemplateFileName = (string)reader["TemplateFileName"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["AgencyID"];
//            _Name = (reader["Name"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Name"];
//            _TemplatePermit = (reader["TemplatePermit"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["TemplatePermit"];
//            _TemplatePoint = (reader["TemplatePoint"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["TemplatePoint"];
//            _TemplateFileName = (reader["TemplateFileName"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["TemplateFileName"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, Int32 AgencyID, string Name, string TemplatePermit, decimal TemplatePoint, string TemplateFileName)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._Name = Name;
         this._TemplatePermit = TemplatePermit;
         this._TemplatePoint = TemplatePoint;
         this._TemplateFileName = TemplateFileName;
      }
   }
}