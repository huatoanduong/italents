using System;

namespace VAKFomula.Entity
{
	/// <summary>
	/// Summary description for FIAgency.
	/// </summary>
	public partial class FIAgency
	{

      private Int32 _ID;
      private string _Username;
      private string _Password;
      private string _Name;
      private Int16 _Point;
      private string _Gender;
      private DateTime _DOB;
      private string _Address1;
      private string _Address2;
      private string _City;
      private string _State;
      private string _PostalCode;
      private string _Country;
      private string _PhoneNo;
      private string _MobileNo;
      private string _Email;
      private string _PassEmail;
      private string _SaveImages;
      private DateTime _LastActiveDate;
      private Int16 _Day_Actived;
      private string _Dis_Activated;
      private string _ProductKey;
      private string _SerailKey;
      private string _SecKey;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsUsernameNullable
      { get { return true;  } }
      public string Username
      {
         get { return _Username;  }
         set { _Username = value; }
      }
      public bool IsPasswordNullable
      { get { return true;  } }
      public string Password
      {
         get { return _Password;  }
         set { _Password = value; }
      }
      public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { _Name = value; }
      }
      public bool IsPointNullable
      { get { return true;  } }
      public Int16 Point
      {
         get { return _Point;  }
         set { _Point = value; }
      }
      public bool IsGenderNullable
      { get { return true;  } }
      public string Gender
      {
         get { return _Gender;  }
         set { _Gender = value; }
      }
      public bool IsDOBNullable
      { get { return true;  } }
      public DateTime DOB
      {
         get { return _DOB;  }
         set { _DOB = value; }
      }
      public bool IsAddress1Nullable
      { get { return true;  } }
      public string Address1
      {
         get { return _Address1;  }
         set { _Address1 = value; }
      }
      public bool IsAddress2Nullable
      { get { return true;  } }
      public string Address2
      {
         get { return _Address2;  }
         set { _Address2 = value; }
      }
      public bool IsCityNullable
      { get { return true;  } }
      public string City
      {
         get { return _City;  }
         set { _City = value; }
      }
      public bool IsStateNullable
      { get { return true;  } }
      public string State
      {
         get { return _State;  }
         set { _State = value; }
      }
      public bool IsPostalCodeNullable
      { get { return true;  } }
      public string PostalCode
      {
         get { return _PostalCode;  }
         set { _PostalCode = value; }
      }
      public bool IsCountryNullable
      { get { return true;  } }
      public string Country
      {
         get { return _Country;  }
         set { _Country = value; }
      }
      public bool IsPhoneNoNullable
      { get { return true;  } }
      public string PhoneNo
      {
         get { return _PhoneNo;  }
         set { _PhoneNo = value; }
      }
      public bool IsMobileNoNullable
      { get { return true;  } }
      public string MobileNo
      {
         get { return _MobileNo;  }
         set { _MobileNo = value; }
      }
      public bool IsEmailNullable
      { get { return true;  } }
      public string Email
      {
         get { return _Email;  }
         set { _Email = value; }
      }
      public bool IsPassEmailNullable
      { get { return true;  } }
      public string PassEmail
      {
         get { return _PassEmail;  }
         set { _PassEmail = value; }
      }
      public bool IsSaveImagesNullable
      { get { return true;  } }
      public string SaveImages
      {
         get { return _SaveImages;  }
         set { _SaveImages = value; }
      }
      public bool IsLastActiveDateNullable
      { get { return true;  } }
      public DateTime LastActiveDate
      {
         get { return _LastActiveDate;  }
         set { _LastActiveDate = value; }
      }
      public bool IsDay_ActivedNullable
      { get { return true;  } }
      public Int16 Day_Actived
      {
         get { return _Day_Actived;  }
         set { _Day_Actived = value; }
      }
      public bool IsDis_ActivatedNullable
      { get { return true;  } }
      public string Dis_Activated
      {
         get { return _Dis_Activated;  }
         set { _Dis_Activated = value; }
      }
      public bool IsProductKeyNullable
      { get { return true;  } }
      public string ProductKey
      {
         get { return _ProductKey;  }
         set { _ProductKey = value; }
      }
      public bool IsSerailKeyNullable
      { get { return true;  } }
      public string SerailKey
      {
         get { return _SerailKey;  }
         set { _SerailKey = value; }
      }
      public bool IsSecKeyNullable
      { get { return true;  } }
      public string SecKey
      {
         get { return _SecKey;  }
         set { _SecKey = value; }
      }

      #endregion

      #region Constructors
      public FIAgency()
      {
         Reset();
      }
      public FIAgency(FIAgency obj)
      {
	this._ID = obj.ID;
	this._Username = obj.Username;
	this._Password = obj.Password;
	this._Name = obj.Name;
	this._Point = obj.Point;
	this._Gender = obj.Gender;
	this._DOB = obj.DOB;
	this._Address1 = obj.Address1;
	this._Address2 = obj.Address2;
	this._City = obj.City;
	this._State = obj.State;
	this._PostalCode = obj.PostalCode;
	this._Country = obj.Country;
	this._PhoneNo = obj.PhoneNo;
	this._MobileNo = obj.MobileNo;
	this._Email = obj.Email;
	this._PassEmail = obj.PassEmail;
	this._SaveImages = obj.SaveImages;
	this._LastActiveDate = obj.LastActiveDate;
	this._Day_Actived = obj.Day_Actived;
	this._Dis_Activated = obj.Dis_Activated;
	this._ProductKey = obj.ProductKey;
	this._SerailKey = obj.SerailKey;
	this._SecKey = obj.SecKey;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _Username = EmptyValues.v_string;
         _Password = EmptyValues.v_string;
         _Name = EmptyValues.v_string;
         _Point = EmptyValues.v_Int16;
         _Gender = EmptyValues.v_string;
         _DOB = EmptyValues.v_DateTime;
         _Address1 = EmptyValues.v_string;
         _Address2 = EmptyValues.v_string;
         _City = EmptyValues.v_string;
         _State = EmptyValues.v_string;
         _PostalCode = EmptyValues.v_string;
         _Country = EmptyValues.v_string;
         _PhoneNo = EmptyValues.v_string;
         _MobileNo = EmptyValues.v_string;
         _Email = EmptyValues.v_string;
         _PassEmail = EmptyValues.v_string;
         _SaveImages = EmptyValues.v_string;
         _LastActiveDate = EmptyValues.v_DateTime;
         _Day_Actived = EmptyValues.v_Int16;
         _Dis_Activated = EmptyValues.v_string;
         _ProductKey = EmptyValues.v_string;
         _SerailKey = EmptyValues.v_string;
         _SecKey = EmptyValues.v_string;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["Username"] is DBNull))
		{
			obj.Username = (string)reader["Username"];
		}
                if(!(reader["Password"] is DBNull))
		{
			obj.Password = (string)reader["Password"];
		}
                if(!(reader["Name"] is DBNull))
		{
			obj.Name = (string)reader["Name"];
		}
                if(!(reader["Point"] is DBNull))
		{
			obj.Point = (Int16)reader["Point"];
		}
                if(!(reader["Gender"] is DBNull))
		{
			obj.Gender = (string)reader["Gender"];
		}
                if(!(reader["DOB"] is DBNull))
		{
			obj.DOB = (DateTime)reader["DOB"];
		}
                if(!(reader["Address1"] is DBNull))
		{
			obj.Address1 = (string)reader["Address1"];
		}
                if(!(reader["Address2"] is DBNull))
		{
			obj.Address2 = (string)reader["Address2"];
		}
                if(!(reader["City"] is DBNull))
		{
			obj.City = (string)reader["City"];
		}
                if(!(reader["State"] is DBNull))
		{
			obj.State = (string)reader["State"];
		}
                if(!(reader["PostalCode"] is DBNull))
		{
			obj.PostalCode = (string)reader["PostalCode"];
		}
                if(!(reader["Country"] is DBNull))
		{
			obj.Country = (string)reader["Country"];
		}
                if(!(reader["PhoneNo"] is DBNull))
		{
			obj.PhoneNo = (string)reader["PhoneNo"];
		}
                if(!(reader["MobileNo"] is DBNull))
		{
			obj.MobileNo = (string)reader["MobileNo"];
		}
                if(!(reader["Email"] is DBNull))
		{
			obj.Email = (string)reader["Email"];
		}
                if(!(reader["PassEmail"] is DBNull))
		{
			obj.PassEmail = (string)reader["PassEmail"];
		}
                if(!(reader["SaveImages"] is DBNull))
		{
			obj.SaveImages = (string)reader["SaveImages"];
		}
                if(!(reader["LastActiveDate"] is DBNull))
		{
			obj.LastActiveDate = (DateTime)reader["LastActiveDate"];
		}
                if(!(reader["Day_Actived"] is DBNull))
		{
			obj.Day_Actived = (Int16)reader["Day_Actived"];
		}
                if(!(reader["Dis_Activated"] is DBNull))
		{
			obj.Dis_Activated = (string)reader["Dis_Activated"];
		}
                if(!(reader["ProductKey"] is DBNull))
		{
			obj.ProductKey = (string)reader["ProductKey"];
		}
                if(!(reader["SerailKey"] is DBNull))
		{
			obj.SerailKey = (string)reader["SerailKey"];
		}
                if(!(reader["SecKey"] is DBNull))
		{
			obj.SecKey = (string)reader["SecKey"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _Username = (reader["Username"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Username"];
//            _Password = (reader["Password"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Password"];
//            _Name = (reader["Name"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Name"];
//            _Point = (reader["Point"] is DBNull)?DalTools.EmptyValues.v_Int16:(Int16)reader["Point"];
//            _Gender = (reader["Gender"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Gender"];
//            _DOB = (reader["DOB"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["DOB"];
//            _Address1 = (reader["Address1"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Address1"];
//            _Address2 = (reader["Address2"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Address2"];
//            _City = (reader["City"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["City"];
//            _State = (reader["State"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["State"];
//            _PostalCode = (reader["PostalCode"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["PostalCode"];
//            _Country = (reader["Country"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Country"];
//            _PhoneNo = (reader["PhoneNo"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["PhoneNo"];
//            _MobileNo = (reader["MobileNo"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["MobileNo"];
//            _Email = (reader["Email"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Email"];
//            _PassEmail = (reader["PassEmail"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["PassEmail"];
//            _SaveImages = (reader["SaveImages"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["SaveImages"];
//            _LastActiveDate = (reader["LastActiveDate"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["LastActiveDate"];
//            _Day_Actived = (reader["Day_Actived"] is DBNull)?DalTools.EmptyValues.v_Int16:(Int16)reader["Day_Actived"];
//            _Dis_Activated = (reader["Dis_Activated"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Dis_Activated"];
//            _ProductKey = (reader["ProductKey"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ProductKey"];
//            _SerailKey = (reader["SerailKey"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["SerailKey"];
//            _SecKey = (reader["SecKey"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["SecKey"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, string Username, string Password, string Name, Int16 Point, string Gender, DateTime DOB, string Address1, string Address2, string City, string State, string PostalCode, string Country, string PhoneNo, string MobileNo, string Email, string PassEmail, string SaveImages, DateTime LastActiveDate, Int16 Day_Actived, string Dis_Activated, string ProductKey, string SerailKey, string SecKey)
      {
         this._ID = ID;
         this._Username = Username;
         this._Password = Password;
         this._Name = Name;
         this._Point = Point;
         this._Gender = Gender;
         this._DOB = DOB;
         this._Address1 = Address1;
         this._Address2 = Address2;
         this._City = City;
         this._State = State;
         this._PostalCode = PostalCode;
         this._Country = Country;
         this._PhoneNo = PhoneNo;
         this._MobileNo = MobileNo;
         this._Email = Email;
         this._PassEmail = PassEmail;
         this._SaveImages = SaveImages;
         this._LastActiveDate = LastActiveDate;
         this._Day_Actived = Day_Actived;
         this._Dis_Activated = Dis_Activated;
         this._ProductKey = ProductKey;
         this._SerailKey = SerailKey;
         this._SecKey = SecKey;
      }
   }
}