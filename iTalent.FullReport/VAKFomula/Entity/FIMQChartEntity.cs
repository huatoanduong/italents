using System;

namespace VAKFomula.Entity
{
	/// <summary>
	/// Summary description for FIMQChart.
	/// </summary>
	public partial class FIMQChart
	{

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _ReportID;
      private string _MQID;
      private string _MQDesc;
      private decimal _MQValue;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { _ReportID = value; }
      }
      public bool IsMQIDNullable
      { get { return true;  } }
      public string MQID
      {
         get { return _MQID;  }
         set { _MQID = value; }
      }
      public bool IsMQDescNullable
      { get { return true;  } }
      public string MQDesc
      {
         get { return _MQDesc;  }
         set { _MQDesc = value; }
      }
      public bool IsMQValueNullable
      { get { return true;  } }
      public decimal MQValue
      {
         get { return _MQValue;  }
         set { _MQValue = value; }
      }

      #endregion

      #region Constructors
      public FIMQChart()
      {
         Reset();
      }
      public FIMQChart(FIMQChart obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._ReportID = obj.ReportID;
	this._MQID = obj.MQID;
	this._MQDesc = obj.MQDesc;
	this._MQValue = obj.MQValue;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_Int32;
         _ReportID = EmptyValues.v_Int32;
         _MQID = EmptyValues.v_string;
         _MQDesc = EmptyValues.v_string;
         _MQValue = EmptyValues.v_decimal;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (Int32)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (Int32)reader["ReportID"];
		}
                if(!(reader["MQID"] is DBNull))
		{
			obj.MQID = (string)reader["MQID"];
		}
                if(!(reader["MQDesc"] is DBNull))
		{
			obj.MQDesc = (string)reader["MQDesc"];
		}
                if(!(reader["MQValue"] is DBNull))
		{
			obj.MQValue = (decimal)reader["MQValue"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["AgencyID"];
//            _ReportID = (reader["ReportID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ReportID"];
//            _MQID = (reader["MQID"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["MQID"];
//            _MQDesc = (reader["MQDesc"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["MQDesc"];
//            _MQValue = (reader["MQValue"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["MQValue"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, Int32 AgencyID, Int32 ReportID, string MQID, string MQDesc, decimal MQValue)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._ReportID = ReportID;
         this._MQID = MQID;
         this._MQDesc = MQDesc;
         this._MQValue = MQValue;
      }
   }
}