using System;

namespace VAKFomula.Entity
{
	/// <summary>
	/// Summary description for FIPrintRecord.
	/// </summary>
	public partial class FIPrintRecord
	{

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _TemplateID;
      private Int32 _ReportID;
      private DateTime _PrintDate;
      private decimal _PointID;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsTemplateIDNullable
      { get { return true;  } }
      public Int32 TemplateID
      {
         get { return _TemplateID;  }
         set { _TemplateID = value; }
      }
      public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { _ReportID = value; }
      }
      public bool IsPrintDateNullable
      { get { return true;  } }
      public DateTime PrintDate
      {
         get { return _PrintDate;  }
         set { _PrintDate = value; }
      }
      public bool IsPointIDNullable
      { get { return true;  } }
      public decimal PointID
      {
         get { return _PointID;  }
         set { _PointID = value; }
      }

      #endregion

      #region Constructors
      public FIPrintRecord()
      {
         Reset();
      }
      public FIPrintRecord(FIPrintRecord obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._TemplateID = obj.TemplateID;
	this._ReportID = obj.ReportID;
	this._PrintDate = obj.PrintDate;
	this._PointID = obj.PointID;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_Int32;
         _TemplateID = EmptyValues.v_Int32;
         _ReportID = EmptyValues.v_Int32;
         _PrintDate = EmptyValues.v_DateTime;
         _PointID = EmptyValues.v_decimal;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (Int32)reader["AgencyID"];
		}
                if(!(reader["TemplateID"] is DBNull))
		{
			obj.TemplateID = (Int32)reader["TemplateID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (Int32)reader["ReportID"];
		}
                if(!(reader["PrintDate"] is DBNull))
		{
			obj.PrintDate = (DateTime)reader["PrintDate"];
		}
                if(!(reader["PointID"] is DBNull))
		{
			obj.PointID = (decimal)reader["PointID"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["AgencyID"];
//            _TemplateID = (reader["TemplateID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["TemplateID"];
//            _ReportID = (reader["ReportID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ReportID"];
//            _PrintDate = (reader["PrintDate"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["PrintDate"];
//            _PointID = (reader["PointID"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["PointID"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, Int32 AgencyID, Int32 TemplateID, Int32 ReportID, DateTime PrintDate, decimal PointID)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._TemplateID = TemplateID;
         this._ReportID = ReportID;
         this._PrintDate = PrintDate;
         this._PointID = PointID;
      }
   }
}