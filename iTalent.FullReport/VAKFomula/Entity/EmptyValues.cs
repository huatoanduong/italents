using System;

namespace VAKFomula.Entity
{
    public struct EmptyValues
    {
        static public readonly Boolean v_Boolean = false;
        static public readonly bool v_bool = false;
        static public readonly byte v_byte = 0;
        static public readonly byte[] v_bytes = new byte[0];
        static public readonly DateTime v_DateTime = DateTime.MinValue;
        static public readonly decimal v_decimal = 0;
        static public readonly double v_double = 0;
        static public readonly float v_float = 0;
        static public readonly Guid v_Guid = Guid.Empty;
        static public readonly int v_int = 0;
        static public readonly Int16 v_Int16 = 0;
        static public readonly Int32 v_Int32 = 0;
        static public readonly Int64 v_Int64 = 0;
        static public readonly Single v_Single = 0;
        static public readonly string v_string = string.Empty;
    }
}
