﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic.CompilerServices;
using VAKFomula.Entity;

namespace VAKFomula.Model
{
    public class CustomerAnalyseModel
    {
        public FIAgency Distributor { get; set; }
        public FICustomer Customer { get; set; }
        public FIFingerAnalysis FingerAnalysis { get; set; }
        public FIMQChart MQ_Chart { get; set; }

        public FingerRecordModel RecordModel { get; set; }
        //protected FIMQChart fingerMQChart { get; set; }
        //protected FIFingerAnalysis fingerAnlysis { get; set; }

        public CustomerAnalyseModel(ref FIFingerAnalysis AnalysisEntity, ref List<FIMQChart> ArrayChartEntity
            , FingerRecordModel modelFingerRecordModel)
        {
            RecordModel = modelFingerRecordModel;
            BuildAnalysisEntity(ref AnalysisEntity);
            BuildChartEntity(ref ArrayChartEntity);
            FingerAnalysis = AnalysisEntity;
        }


        #region Properties

        public string learning_style
        {
            get { return get_learning_style(RCVP, RCHP, RCAP); }
        }

        public string RA_VP
        {
            get { return get_rank("RCVP"); }
        }

        public string RA_HP
        {
            get { return get_rank("RCHP"); }
        }

        public string RA_AP
        {
            get { return get_rank("RCAP"); }
        }

        public decimal FP1
        {
            get { return FingerAnalysis.FPType1; }
        }

        public decimal FP2
        {
            get { return FingerAnalysis.FPType2; }
        }

        public decimal FP3
        {
            get { return FingerAnalysis.FPType3; }
        }

        public decimal FP4
        {
            get { return FingerAnalysis.FPType4; }
        }

        public decimal FP5
        {
            get { return FingerAnalysis.FPType5; }
        }

        public decimal FP6
        {
            get { return FingerAnalysis.FPType6; }
        }

        public decimal FP7
        {
            get { return FingerAnalysis.FPType7; }
        }

        public decimal FP8
        {
            get { return FingerAnalysis.FPType8; }
        }
        public decimal FP9
        {
            get { return FingerAnalysis.FPType9; }
        }

        public decimal FP10
        {
            get { return FingerAnalysis.FPType10; }
        }
        public int L1L
        {
            get { return RecordModel.L1L; }
        }

        public int L1R
        {
            get { return RecordModel.L1R; }
        }

        public int Max_L1L_L1R
        {
            get { return RecordModel.Max_L1L_L1R; }
        }

        public int L2L
        {
            get { return RecordModel.L2L; }
        }

        public int L2R
        {
            get { return RecordModel.L2R; }
        }

        public int Max_L2L_L2R
        {
            get { return RecordModel.Max_L2L_L2R; }
        }

        public int L3L
        {
            get { return RecordModel.L3L; }
        }

        public int L3R
        {
            get { return RecordModel.L3R; }
        }

        public int Max_L3L_L3R
        {
            get { return RecordModel.Max_L3L_L3R; }
        }

        public int L4L
        {
            get { return RecordModel.L4L; }
        }

        public int L4R
        {
            get { return RecordModel.L4R; }
        }

        public int Max_L4L_L4R
        {
            get { return RecordModel.Max_L4L_L4R; }
        }

        public int L5L
        {
            get { return RecordModel.L5L; }
        }

        public int L5R
        {
            get { return RecordModel.L5R; }
        }

        public int Max_L5L_L5R
        {
            get { return RecordModel.Max_L5L_L5R; }
        }

        public int R1L
        {
            get { return RecordModel.R1L; }
        }

        public int R1R
        {
            get { return RecordModel.R1R; }
        }

        public int Max_R1L_R1R
        {
            get { return RecordModel.Max_R1L_R1R; }
        }

        public int R2L
        {
            get { return RecordModel.R2L; }
        }

        public int R2R
        {
            get { return RecordModel.R2R; }
        }

        public int Max_R2L_R2R
        {
            get { return RecordModel.Max_R2L_R2R; }
        }

        public int R3L
        {
            get { return RecordModel.R3L; }
        }

        public int R3R
        {
            get { return RecordModel.R3R; }
        }

        public int Max_R3L_R3R
        {
            get { return RecordModel.Max_R3L_R3R; }
        }

        public int R4L
        {
            get { return RecordModel.R4L; }
        }

        public int R4R
        {
            get { return RecordModel.R4R; }
        }

        public int Max_R4L_R4R
        {
            get { return RecordModel.Max_R4L_R4R; }
        }

        public int R5L
        {
            get { return RecordModel.R5L; }
        }

        public int R5R
        {
            get { return RecordModel.R5R; }
        }

        public int Max_R5L_R5R
        {
            get { return RecordModel.Max_R5L_R5R; }
        }

        //Finger Type
        public string L1T
        {
            get { return RecordModel.L1T; }
        }

        public string L2T
        {
            get { return RecordModel.L2T; }
        }

        public string L3T
        {
            get { return RecordModel.L3T; }
        }

        public string L4T
        {
            get { return RecordModel.L4T; }
        }

        public string L5T
        {
            get { return RecordModel.L5T; }
        }

        public string R1T
        {
            get { return RecordModel.R1T; }
        }

        public string R2T
        {
            get { return RecordModel.R2T; }
        }

        public string R3T
        {
            get { return RecordModel.R3T; }
        }

        public string R4T
        {
            get { return RecordModel.R4T; }
        }

        public string R5T
        {
            get { return RecordModel.R5T; }
        }

        //Chi so TFRC
        public decimal TFRC
        {
            get { return RecordModel.TFRC; }
        }

        //Form Database
        public double ATDR
        {
            get { return RecordModel.ATDR; }
        }

        public double ATDL
        {
            get { return RecordModel.ATDL; }
        }

        //Sum (MaxL), (MaxR)
        public double SumMaxL
        {
            get { return RecordModel.SUML; }
        }

        public double SumMaxR
        {
            get { return RecordModel.SUMR; }
        }

        public decimal LT_BLS
        {
            get { return (decimal) this.Cal_LT_BLS(); }
        }

        public decimal RT_BLS
        {
            get { return (decimal) this.Cal_RT_BLS(); }
        }

        public decimal Page_BLS
        {
            get { return this.Cal_Page_BLS(); }
        }

        public decimal L1RCB
        {
            get { return this.Max_L1L_L1R; }
        }

        public decimal L2RCB
        {
            get { return this.Max_L2L_L2R; }
        }

        public decimal L3RCB
        {
            get { return this.Max_L3L_L3R; }
        }

        public decimal L4RCB
        {
            get { return this.Max_L4L_L4R; }
        }

        public decimal L5RCB
        {
            get { return this.Max_L5L_L5R; }
        }

        public decimal R1RCB
        {
            get { return this.Max_R1L_R1R; }
        }

        public decimal R2RCB
        {
            get { return this.Max_R2L_R2R; }
        }

        public decimal R3RCB
        {
            get { return this.Max_R3L_R3R; }
        }

        public decimal R4RCB
        {
            get { return this.Max_R4L_R4R; }
        }

        public decimal R5RCB
        {
            get { return this.Max_R5L_R5R; }
        }

        public decimal Sum_RCB
        {
            get { return (decimal) this.Cal_Sum_RCB(); }
        }

        public decimal SAP
        {
            get { return (decimal) this.Cal_SAP(); }
        }

        public decimal SBP
        {
            get { return (decimal) this.Cal_SBP(); }
        }

        public decimal SCP
        {
            get { return (decimal) this.Cal_SCP(); }
        }

        public decimal SDP
        {
            get { return (decimal) this.Cal_SDP(); }
        }

        public decimal SER
        {
            get { return (decimal) this.Cal_Final_SER(); }
        }

        public decimal M1Q
        {
            get { return (decimal) this.Cal_M1Q(); }
        }

        public decimal M2Q
        {
            get { return (decimal) this.Cal_M2Q(); }
        }

        public decimal M3Q
        {
            get { return (decimal) this.Cal_M3Q(); }
        }

        public decimal M4Q
        {
            get { return (decimal) this.Cal_M4Q(); }
        }

        public decimal M5Q
        {
            get { return (decimal) this.Cal_M5Q(); }
        }

        public decimal M6Q
        {
            get { return (decimal) this.Cal_M6Q(); }
        }

        public decimal M7Q
        {
            get { return (decimal) this.Cal_M7Q(); }
        }

        public decimal M8Q
        {
            get { return (decimal) this.Cal_M8Q(); }
        }

        public decimal EQVP
        {
            get { return (decimal) this.Cal_EQVP(); }
        }

        public decimal IQVP
        {
            get { return (decimal) this.Cal_IQVP(); }
        }

        public decimal AQVP
        {
            get { return (decimal) this.Cal_AQVP(); }
        }

        public decimal CQVP
        {
            get { return (decimal) this.Cal_CQVP(); }
        }

        public decimal L1RCP
        {
            get { return (decimal) this.Cal_L1RCP(); }
        }

        public decimal L2RCP
        {
            get { return (decimal) this.Cal_L2RCP(); }
        }

        public decimal L3RCP
        {
            get { return (decimal) this.Cal_L3RCP(); }
        }

        public decimal L4RCP
        {
            get { return (decimal) this.Cal_L4RCP(); }
        }

        public decimal L5RCP
        {
            get { return (decimal) this.Cal_L5RCP(); }
        }

        public decimal R1RCP
        {
            get { return (decimal) this.Cal_R1RCP(); }
        }

        public decimal R2RCP
        {
            get { return (decimal) this.Cal_R2RCP(); }
        }

        public decimal R3RCP
        {
            get { return (decimal) this.Cal_R3RCP(); }
        }

        public decimal R4RCP
        {
            get { return (decimal) this.Cal_R4RCP(); }
        }

        public decimal R5RCP
        {
            get { return (decimal) this.Cal_R5RCP(); }
        }

        public decimal S1BZ
        {
            get { return (decimal) this.Cal_S1BZ(); }
        }

        public decimal S2BZ
        {
            get { return (decimal) this.Cal_S2BZ(); }
        }

        public decimal S3BZ
        {
            get { return (decimal) this.Cal_S3BZ(); }
        }

        public decimal S4BZ
        {
            get { return (decimal) this.Cal_S4BZ(); }
        }

        public decimal S5BZ
        {
            get { return (decimal) this.Cal_S5BZ(); }
        }

        public decimal S6BZ
        {
            get { return (decimal) this.Cal_S6BZ(); }
        }

        public decimal S7BZ
        {
            get { return (decimal) this.Cal_S7BZ(); }
        }

        public decimal S8BZ
        {
            get { return (decimal) this.Cal_S8BZ(); }
        }

        public decimal S9BZ
        {
            get { return (decimal) this.Cal_S9BZ(); }
        }

        public decimal S10BZ
        {
            get { return (decimal) this.Cal_S10BZ(); }
        }

        public decimal S11BZ
        {
            get { return (decimal) this.Cal_S11BZ(); }
        }

        public decimal S12BZ
        {
            get { return (decimal) this.Cal_S12BZ(); }
        }

        public decimal S13BZ
        {
            get { return (decimal) this.Cal_S13BZ(); }
        }

        public decimal S14BZ
        {
            get { return (decimal) this.Cal_S14BZ(); }
        }

        public decimal S15BZ
        {
            get { return (decimal) this.Cal_S15BZ(); }
        }

        public decimal S16BZ
        {
            get { return (decimal) this.Cal_S16BZ(); }
        }

        public decimal S17BZ
        {
            get { return (decimal) this.Cal_S17BZ(); }
        }

        public decimal S18BZ
        {
            get { return (decimal) this.Cal_S18BZ(); }
        }

        public string S1BZZ
        {
            get { return this.Cal_S1BZZ(); }
        }

        public string S2BZZ
        {
            get { return this.Cal_S2BZZ(); }
        }

        public string S3BZZ
        {
            get { return this.Cal_S3BZZ(); }
        }

        public string S4BZZ
        {
            get { return this.Cal_S4BZZ(); }
        }

        public string S5BZZ
        {
            get { return this.Cal_S5BZZ(); }
        }

        public string S6BZZ
        {
            get { return this.Cal_S6BZZ(); }
        }

        public string S7BZZ
        {
            get { return this.Cal_S7BZZ(); }
        }

        public string S8BZZ
        {
            get { return this.Cal_S8BZZ(); }
        }

        public string S9BZZ
        {
            get { return this.Cal_S9BZZ(); }
        }

        public string S10BZZ
        {
            get { return this.Cal_S10BZZ(); }
        }

        public string S11BZZ
        {
            get { return this.Cal_S11BZZ(); }
        }

        public string S12BZZ
        {
            get { return this.Cal_S12BZZ(); }
        }

        public string S13BZZ
        {
            get { return this.Cal_S13BZZ(); }
        }

        public string S14BZZ
        {
            get { return this.Cal_S14BZZ(); }
        }

        public string S15BZZ
        {
            get { return this.Cal_S15BZZ(); }
        }

        public string S16BZZ
        {
            get { return this.Cal_S16BZZ(); }
        }

        public string S17BZZ
        {
            get { return this.Cal_S17BZZ(); }
        }

        public string S18BZZ
        {
            get { return this.Cal_S18BZZ(); }
        }


        public decimal L1RCP1
        {
            get { return this.Cal_LRCPType("L1RCP1"); }
        }

        public decimal L2RCP2
        {
            get { return this.Cal_LRCPType("L2RCP2"); }
        }

        public decimal L3RCP3
        {
            get { return this.Cal_LRCPType("L3RCP3"); }
        }

        public decimal L4RCP4
        {
            get { return this.Cal_LRCPType("L4RCP4"); }
        }

        public decimal L5RCP5
        {
            get { return this.Cal_LRCPType("L5RCP5"); }
        }

        public decimal R1RCP1
        {
            get { return this.Cal_LRCPType("R1RCP1"); }
        }

        public decimal R2RCP2
        {
            get { return this.Cal_LRCPType("R2RCP2"); }
        }

        public decimal R3RCP3
        {
            get { return this.Cal_LRCPType("R3RCP3"); }
        }

        public decimal R4RCP4
        {
            get { return this.Cal_LRCPType("R4RCP4"); }
        }

        public decimal R5RCP5
        {
            get { return this.Cal_LRCPType("R5RCP5"); }
        }

        public decimal M1QPR
        {
            get { return (decimal) this.Cal_MQPRType("M1QPR"); }
        }

        public decimal M2QPR
        {
            get { return (decimal) this.Cal_MQPRType("M2QPR"); }
        }

        public decimal M3QPR
        {
            get { return (decimal) this.Cal_MQPRType("M3QPR"); }
        }

        public decimal M4QPR
        {
            get { return (decimal) this.Cal_MQPRType("M4QPR"); }
        }

        public decimal M5QPR
        {
            get { return (decimal) this.Cal_MQPRType("M5QPR"); }
        }

        public decimal M6QPR
        {
            get { return (decimal) this.Cal_MQPRType("M6QPR"); }
        }

        public decimal M7QPR
        {
            get { return (decimal) this.Cal_MQPRType("M7QPR"); }
        }

        public decimal M8QPR
        {
            get { return (decimal) this.Cal_MQPRType("M8QPR"); }
        }

        public decimal AC2WP
        {
            get { return this.Cal_ACPType("AC2WP"); }
        }

        public decimal AC2UP
        {
            get { return this.Cal_ACPType("AC2UP"); }
        }

        public decimal AC2RP
        {
            get { return this.Cal_ACPType("AC2RP"); }
        }

        public decimal AC2XP
        {
            get { return this.Cal_ACPType("AC2XP"); }
        }

        public decimal AC2SP
        {
            get { return this.Cal_ACPType("AC2SP"); }
        }

        public decimal RCAP
        {
            get { return (decimal) this.Cal_RCAP(); }
        }

        public decimal RCHP
        {
            get { return (decimal) this.Cal_RCHP(); }
        }

        public decimal RCVP
        {
            get { return (decimal) this.Cal_Final_RCVP(); }
        }

        public decimal T1AP
        {
            get { return (decimal) this.Cal_T1AP(); }
        }

        public decimal T1BP
        {
            get { return (decimal) this.Cal_T1BP(); }
        }

        public string ATDRA
        {
            get { return ""; }
        }

        public string ATDRS
        {
            get { return ""; }
        }

        public string ATDLA
        {
            get { return ""; }
        }

        public string ATDLS
        {
            get { return ""; }
        }

        public string Label1
        {
            get { return "%"; }
        }

        public string Label2
        {
            get { return "Series1"; }
        }

        public string Label3
        {
            get { return ""; }
        }

        public string Label4
        {
            get { return ""; }
        }

        public string Label5
        {
            get { return ""; }
        }

        public string Label6
        {
            get { return ""; }
        }

        #endregion

        #region ToDictionary

        private string get_learning_style(decimal n1, decimal n2, decimal n3)
        {
            if (n1 > n2 && n1 > n3)
            {
                return "THỊ GIÁC";
            }

            if (n2 > n1 && n2 > n3)
            {
                return "THÍNH GIÁC";
            }
            return "VẬN ĐỘNG";
            //return "VẬN ĐỘNG - XÚC GIÁC";
        }

        private string get_rank(string RC)
        {
            string min = get_min();
            string max = get_max();
            string rank = "1";
            switch (RC)
            {
                case "RCVP":
                    if (min == RC)
                        rank = "3";
                    else if (max == RC)
                        rank = "1";
                    else rank = "2";
                    break;

                case "RCHP":
                    if (min == RC)
                        rank = "3";
                    else if (max == RC)
                        rank = "1";
                    else rank = "2";
                    break;

                case "RCAP":
                    if (min == RC)
                        rank = "3";
                    else if (max == RC)
                        rank = "1";
                    else rank = "2";
                    break;
            }
            return rank;
        }

        private string get_min()
        {
            decimal n1 = RCVP;
            decimal n2 = RCHP;
            decimal n3 = RCAP;

            if (n1 <= n2 && n1 <= n3)
                return "RCVP";
            else if (n2 <= n1 && n2 <= n3)
                return "RCHP";
            else
                return "RCAP";
        }

        private string get_max()
        {
            decimal n1 = RCVP;
            decimal n2 = RCHP;
            decimal n3 = RCAP;

            if (n1 >= n2 && n1 >= n3)
                return "RCVP";
            else if (n2 >= n1 && n2 >= n3)
                return "RCHP";
            else
                return "RCAP";
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            //Distributor			
            dictionary.Add("NAME_AGENCY", ToString(Distributor.Name));

            //Customer	
            dictionary.Add("REPORT_ID", ToString(Customer.ReportID));
            dictionary.Add("CUS_NAME", ToString(Customer.Name));
            dictionary.Add("CUS_DATE", ToString(Customer.Date));
            dictionary.Add("CUS_SEX", ToString(Customer.Gender));
            dictionary.Add("CUS_BIRTH", ToString(Customer.DOB));
            dictionary.Add("CUS_PARENT", ToString(Customer.Parent));
            dictionary.Add("CUS_PHONE", ToString(Customer.Tel));
            dictionary.Add("CUS_EMAIL", ToString(Customer.Email));
            dictionary.Add("CUS_ADDRESS", ToString(Customer.Address1));

            dictionary.Add("FP1", ToString(FP1));
            dictionary.Add("FP2", ToString(FP2));
            dictionary.Add("FP3", ToString(FP3));
            dictionary.Add("FP4", ToString(FP4));
            dictionary.Add("FP5", ToString(FP5));
            dictionary.Add("FP6", ToString(FP6));
            dictionary.Add("FP7", ToString(FP7));
            dictionary.Add("FP8", ToString(FP8));
            dictionary.Add("FP9", ToString(FP9));
            dictionary.Add("FP10", ToString(FP10));

            dictionary.Add("learning_style", ToString(learning_style));

            dictionary.Add("RA_VP", ToString(RA_VP));
            dictionary.Add("RA_HP", ToString(RA_HP));
            dictionary.Add("RA_AP", ToString(RA_AP));

            //dictionary.Add("L1L", ToString(L1L));
            //dictionary.Add("L1R", ToString(L1R));
            //dictionary.Add("Max_L1L_L1R", ToString(Max_L1L_L1R));

            //dictionary.Add("L2L", ToString());
            //dictionary.Add("L2R", ToString(L2R));
            //dictionary.Add("Max_L2L_L2R", ToString(Max_L2L_L2R));

            //dictionary.Add("L3L", ToString(L3L));
            //dictionary.Add("L3R", ToString(L3R));
            //dictionary.Add("Max_L3L_L3R", ToString(Max_L3L_L3R));

            //dictionary.Add("L4L", ToString(L4L));
            //dictionary.Add("L4R", ToString(L4R));
            //dictionary.Add("Max_L4L_L4R", ToString(Max_L4L_L4R));

            //dictionary.Add("L5L", ToString(L5L));
            //dictionary.Add("L5R", ToString(L5R));
            //dictionary.Add("Max_L5L_L5R", ToString(Max_L5L_L5R));

            //dictionary.Add("R1L", ToString(R1L));
            //dictionary.Add("R1R", ToString(R1R));
            //dictionary.Add("Max_R1L_R1R", ToString(Max_R1L_R1R));

            //dictionary.Add("R2L", ToString(R2L));
            //dictionary.Add("R2R", ToString(R2R));
            //dictionary.Add("Max_R2L_R2R", ToString(Max_R2L_R2R));

            //dictionary.Add("R3L", ToString(R3L));
            //dictionary.Add("R3R", ToString(R3R));
            //dictionary.Add("Max_R3L_R3R", ToString(Max_R3L_R3R));

            //dictionary.Add("R4L", ToString(R4L));
            //dictionary.Add("R4R", ToString(R4R));
            //dictionary.Add("Max_R4L_R4R", ToString(Max_R4L_R4R));

            //dictionary.Add("R5L", ToString(R5L));
            //dictionary.Add("R5R", ToString(R5R));
            //dictionary.Add("Max_R5L_R5R", ToString(Max_R5L_R5R));

            dictionary.Add("L1T", ToString(L1T));
            dictionary.Add("L2T", ToString(L2T));
            dictionary.Add("L3T", ToString(L3T));
            dictionary.Add("L4T", ToString(L4T));
            dictionary.Add("L5T", ToString(L5T));
            dictionary.Add("R1T", ToString(R1T));
            dictionary.Add("R2T", ToString(R2T));
            dictionary.Add("R3T", ToString(R3T));
            dictionary.Add("R4T", ToString(R4T));
            dictionary.Add("R5T", ToString(R5T));

            //Chi so TFRC
            dictionary.Add("TFRC", ToString(TFRC));
            dictionary.Add("CB1", ToString(TFRC_CB1));
            dictionary.Add("CB2", ToString(TFRC_CB2));
            dictionary.Add("CB3", ToString(TFRC_CB3));
            dictionary.Add("CB4", ToString(TFRC_CB4));

            //Form Database
            dictionary.Add("ATDR", ToString(ATDR));
            dictionary.Add("ATDL", ToString(ATDL));

            //Sum (MaxL), (MaxR)
            //dictionary.Add("SumMaxL", ToString(SumMaxL));
            //dictionary.Add("SumMaxR", ToString(SumMaxR));

            //dictionary.Add("LT_BLS", ToString(LT_BLS));
            //dictionary.Add("RT_BLS", ToString(RT_BLS));
            //dictionary.Add("Page_BLS", ToString(Page_BLS));

            dictionary.Add("L1RCB", ToString(L1RCB));
            dictionary.Add("L2RCB", ToString(L2RCB));
            dictionary.Add("L3RCB", ToString(L3RCB));
            dictionary.Add("L4RCB", ToString(L4RCB));
            dictionary.Add("L5RCB", ToString(L5RCB));

            dictionary.Add("R1RCB", ToString(R1RCB));
            dictionary.Add("R2RCB", ToString(R2RCB));
            dictionary.Add("R3RCB", ToString(R3RCB));
            dictionary.Add("R4RCB", ToString(R4RCB));
            dictionary.Add("R5RCB", ToString(R5RCB));

            dictionary.Add("Sum_RCB", ToString(Sum_RCB));

            dictionary.Add("SAP", ToString(SAP));
            dictionary.Add("SBP", ToString(SBP));
            dictionary.Add("SCP", ToString(SCP));
            dictionary.Add("SDP", ToString(SDP));
            dictionary.Add("SER", ToString(SER));

            dictionary.Add("M1Q", ToString(M1Q));
            dictionary.Add("M2Q", ToString(M2Q));
            dictionary.Add("M3Q", ToString(M3Q));
            dictionary.Add("M4Q", ToString(M4Q));
            dictionary.Add("M5Q", ToString(M5Q));
            dictionary.Add("M6Q", ToString(M6Q));
            dictionary.Add("M7Q", ToString(M7Q));
            dictionary.Add("M8Q", ToString(M8Q));

            dictionary.Add("EQVP", ToString(EQVP));
            dictionary.Add("IQVP", ToString(IQVP));
            dictionary.Add("AQVP", ToString(AQVP));
            dictionary.Add("CQVP", ToString(CQVP));

            dictionary.Add("L1RCP", ToString(L1RCP));
            dictionary.Add("L2RCP", ToString(L2RCP));
            dictionary.Add("L3RCP", ToString(L3RCP));
            dictionary.Add("L4RCP", ToString(L4RCP));
            dictionary.Add("L5RCP", ToString(L5RCP));
            dictionary.Add("R1RCP", ToString(R1RCP));
            dictionary.Add("R2RCP", ToString(R2RCP));
            dictionary.Add("R3RCP", ToString(R3RCP));
            dictionary.Add("R4RCP", ToString(R4RCP));
            dictionary.Add("R5RCP", ToString(R5RCP));

            //dictionary.Add("S1BZ", ToString(S1BZ));
            //dictionary.Add("S2BZ", ToString(S2BZ));
            //dictionary.Add("S3BZ", ToString(S3BZ));
            //dictionary.Add("S4BZ", ToString(S4BZ));
            //dictionary.Add("S5BZ", ToString(S5BZ));
            //dictionary.Add("S6BZ", ToString(S6BZ));
            //dictionary.Add("S7BZ", ToString(S7BZ));
            //dictionary.Add("S8BZ", ToString(S8BZ));
            //dictionary.Add("S9BZ", ToString(S9BZ));
            //dictionary.Add("S10BZ", ToString(S10BZ));
            //dictionary.Add("S11BZ", ToString(S11BZ));
            //dictionary.Add("S12BZ", ToString(S12BZ));
            //dictionary.Add("S13BZ", ToString(S13BZ));
            //dictionary.Add("S14BZ", ToString(S14BZ));
            //dictionary.Add("S15BZ", ToString(S15BZ));
            //dictionary.Add("S16BZ", ToString(S16BZ));
            //dictionary.Add("S17BZ", ToString(S17BZ));
            //dictionary.Add("S18BZ", ToString(S18BZ));

            //dictionary.Add("S1BZZ", ToString(S1BZZ));
            //dictionary.Add("S2BZZ", ToString(S2BZZ));
            //dictionary.Add("S3BZZ", ToString(S3BZZ));
            //dictionary.Add("S4BZZ", ToString(S4BZZ));
            //dictionary.Add("S5BZZ", ToString(S5BZZ));
            //dictionary.Add("S6BZZ", ToString(S6BZZ));
            //dictionary.Add("S7BZZ", ToString(S7BZZ));
            //dictionary.Add("S8BZZ", ToString(S8BZZ));
            //dictionary.Add("S9BZZ", ToString(S9BZZ));
            //dictionary.Add("S10BZZ", ToString(S10BZZ));
            //dictionary.Add("S11BZZ", ToString(S11BZZ));
            //dictionary.Add("S12BZZ", ToString(S12BZZ));
            //dictionary.Add("S13BZZ", ToString(S13BZZ));
            //dictionary.Add("S14BZZ", ToString(S14BZZ));
            //dictionary.Add("S15BZZ", ToString(S15BZZ));
            //dictionary.Add("S16BZZ", ToString(S16BZZ));
            //dictionary.Add("S17BZZ", ToString(S17BZZ));
            //dictionary.Add("S18BZZ", ToString(S18BZZ));


            dictionary.Add("L1RCP1", ToString(L1RCP1));
            dictionary.Add("L2RCP2", ToString(L2RCP2));
            dictionary.Add("L3RCP3", ToString(L3RCP3));
            dictionary.Add("L4RCP4", ToString(L4RCP4));
            dictionary.Add("L5RCP5", ToString(L5RCP5));
            dictionary.Add("R1RCP1", ToString(R1RCP1));
            dictionary.Add("R2RCP2", ToString(R2RCP2));
            dictionary.Add("R3RCP3", ToString(R3RCP3));
            dictionary.Add("R4RCP4", ToString(R4RCP4));
            dictionary.Add("R5RCP5", ToString(R5RCP5));

            dictionary.Add("M1QPR", ToString(M1QPR));
            dictionary.Add("M2QPR", ToString(M2QPR));
            dictionary.Add("M3QPR", ToString(M3QPR));
            dictionary.Add("M4QPR", ToString(M4QPR));
            dictionary.Add("M5QPR", ToString(M5QPR));
            dictionary.Add("M6QPR", ToString(M6QPR));
            dictionary.Add("M7QPR", ToString(M7QPR));
            dictionary.Add("M8QPR", ToString(M8QPR));

            dictionary.Add("AC2WP", ToString(AC2WP));
            dictionary.Add("AC2UP", ToString(AC2UP));
            dictionary.Add("AC2RP", ToString(AC2RP));
            dictionary.Add("AC2XP", ToString(AC2XP));
            dictionary.Add("AC2SP", ToString(AC2SP));

            dictionary.Add("RCAP", ToString(RCAP));
            dictionary.Add("RCHP", ToString(RCHP));
            dictionary.Add("RCVP", ToString(RCVP));

            dictionary.Add("T1AP", ToString(T1AP));
            dictionary.Add("T1BP", ToString(T1BP));

            //dictionary.Add("ATDRA", ToString(ATDRA));
            //dictionary.Add("ATDRS", ToString(ATDRS));
            //dictionary.Add("ATDLA", ToString(ATDLA));
            //dictionary.Add("ATDLS", ToString(ATDLS));

            //dictionary.Add("Label1", ToString(Label1));
            //dictionary.Add("Label2", ToString(Label2));
            //dictionary.Add("Label3", ToString(Label3));
            //dictionary.Add("Label4", ToString(Label4));
            //dictionary.Add("Label5", ToString(Label5));
            //dictionary.Add("Label6", ToString(Label6));

            //Report
            dictionary.Add("100AQ", ToString(_100AQ));
            dictionary.Add("100CQ", ToString(_100CQ));
            dictionary.Add("100EQ", ToString(_100EQ));
            dictionary.Add("100IQ", ToString(_100IQ));
            //dictionary.Add("ATD_Average", ToString(ATD_Average));

            //dictionary.Add("IN1_IT_construction", ToString(IN1_IT_construction));
            //dictionary.Add("IN10_psycho", ToString(IN10_psycho));
            //dictionary.Add("IN10_sales", ToString(IN10_sales));
            //dictionary.Add("IN11_mass", ToString(IN11_mass));
            //dictionary.Add("IN12_lang", ToString(IN12_lang));
            //dictionary.Add("IN13_lit", ToString(IN13_lit));
            //dictionary.Add("IN14_edu", ToString(IN14_edu));
            //dictionary.Add("IN15_pol", ToString(IN15_pol));
            //dictionary.Add("IN16_mgn", ToString(IN16_mgn));
            //dictionary.Add("IN17_fin", ToString(IN17_fin));
            //dictionary.Add("IN18_spo", ToString(IN18_spo));
            //dictionary.Add("IN19_fin2", ToString(IN19_fin2));
            dictionary.Add("IN2_eng", ToString(IN2_eng));
            dictionary.Add("IN3_math", ToString(IN3_math));
            dictionary.Add("IN4_med", ToString(IN4_med));
            dictionary.Add("IN5_life", ToString(IN5_life));
            dictionary.Add("IN6_agri", ToString(IN6_agri));
            dictionary.Add("IN7_earth", ToString(IN7_earth));
            dictionary.Add("IN9_art", ToString(IN9_art));

            //dictionary.Add("L1TCT", ToString(L1TCT));

            dictionary.Add("L1RCBVA", ToString(L1RCBVA));
            //dictionary.Add("L1RCD", ToString(L1RCD));

            dictionary.Add("L2RCBVA", ToString(L2RCBVA));
            //dictionary.Add("L2RCD", ToString(L2RCD));
            dictionary.Add("L3RCBVA", ToString(L3RCBVA));
            //dictionary.Add("L3RCD", ToString(L3RCD));
            dictionary.Add("L4RCBVA", ToString(L4RCBVA));
            //dictionary.Add("L4RCD", ToString(L4RCD));
            dictionary.Add("L5RCBVA", ToString(L5RCBVA));
            //dictionary.Add("L5RCD", ToString(L5RCD));
            //dictionary.Add("L5RCP_Final", ToString(L5RCP_Final));

            dictionary.Add("LTRC", ToString(LTRC));
            dictionary.Add("LTRCP", ToString(LTRCP));

            dictionary.Add("M1QP", ToString(M1QP));
            dictionary.Add("M2QP", ToString(M2QP));
            dictionary.Add("M3QP", ToString(M3QP));
            dictionary.Add("M4QP", ToString(M4QP));
            dictionary.Add("M5QP", ToString(M5QP));
            dictionary.Add("M6QP", ToString(M6QP));
            dictionary.Add("M7QP", ToString(M7QP));
            dictionary.Add("M8QP", ToString(M8QP));

            dictionary.Add("MA1", ToString(MA1));
            dictionary.Add("MA2", ToString(MA2));
            dictionary.Add("MA3", ToString(MA3));
            dictionary.Add("MA4", ToString(MA4));
            dictionary.Add("MA5", ToString(MA5));
            dictionary.Add("MA6", ToString(MA6));
            dictionary.Add("MA7", ToString(MA7));
            dictionary.Add("MA8", ToString(MA8));

            dictionary.Add("MALATD", ToString(MALATD));
            dictionary.Add("MARATD", ToString(MARATD));

            dictionary.Add("plus_X", ToString(plus_X));

            dictionary.Add("R1RCBVA", ToString(R1RCBVA));
            //dictionary.Add("R1RCD", ToString(R1RCD));
            //dictionary.Add("R1TCT", ToString(R1TCT));
            dictionary.Add("R2RCBVA", ToString(R2RCBVA));
            //dictionary.Add("R2RCD", ToString(R2RCD));
            dictionary.Add("R3RCBVA", ToString(R3RCBVA));
            //dictionary.Add("R3RCD", ToString(R3RCD));
            dictionary.Add("R4RCBVA", ToString(R4RCBVA));
            //dictionary.Add("R4RCD", ToString(R4RCD));
            dictionary.Add("R5RCBVA", ToString(R5RCBVA));
            //dictionary.Add("R5RCD", ToString(R5RCD));

            dictionary.Add("Rank_AHF", ToString(Rank_AHP));
            dictionary.Add("RCVA", ToString(RCVA));
            dictionary.Add("RTRC", ToString(RTRC));
            dictionary.Add("RTRCP", ToString(RTRCP));
            //dictionary.Add("T1BP_2", ToString(T1BP_2));

            dictionary.Add("L1T_TextBox", ToString(L1TTextBox));
            dictionary.Add("R1T_TextBox", ToString(R1TTextBox));
            dictionary.Add("TFRC_TextBox", ToString(TFRC_TextBox));
            dictionary.Add("ATDR_TextBox", ToString(ATDR_TextBox1));
            dictionary.Add("ATDL_TextBox", ToString(ATDL_TextBox2));


            return dictionary;
        }

        protected string ToString(DateTime? value)
        {
            if (value == null)
            {
                return "";
            }
            return ((DateTime) value).ToString("dd/MM/yyyy");
        }

        protected string ToString(decimal? value)
        {
            if (value == null)
            {
                return "";
            }
            return Math.Round((decimal) value, 2).ToString();
        }

        protected string ToString(string value)
        {
            return value;
        }

        protected string ToString(int? value)
        {
            if (value == null)
            {
                return "";
            }
            return ((int) value).ToString();
        }

        protected string ToString(double? value)
        {
            if (value == null)
            {
                return "";
            }
            return (Math.Round((double) value, 2).ToString());
        }

        #endregion

        protected int Cal_LT_BLS()
        {
            if (this.TFRC == 0) return 0;
            int percent = (int) Math.Round(((double) this.SumMaxL / (double) this.TFRC) * 100.0);
            return percent;
        }

        protected int Cal_RT_BLS()
        {
            if (this.TFRC == 0) return 0;
            int percent = (int) Math.Round(((double) this.SumMaxR / (double) this.TFRC) * 100.0);
            return percent;
        }

        protected decimal Cal_Page_BLS()
        {
            decimal substract = this.LT_BLS - this.RT_BLS;
            if (substract < 5)
                return 2;
            else if (substract > 5)
                return 3;
            else
                return 1;
        }

        protected int Cal_Sum_RCB()
        {
            return (int)(this.L1RCB + this.L2RCB + this.L3RCB + this.L4RCB + this.L5RCB + this.R1RCB + this.R2RCB + this.R3RCB + this.R4RCB + this.R5RCB);
        }

        protected double Cal_SAP()
        {
            if (this.Sum_RCB == 0) return 0;
            return Math.Round(((double) (this.L1RCB + this.R1RCB) / (double) this.Sum_RCB) * 100.0, 2);
        }

        protected double Cal_SBP()
        {
            if (this.Sum_RCB == 0) return 0;
            return Math.Round(((double) (this.L2RCB + this.R2RCB) / (double) this.Sum_RCB) * 100.0, 2);
        }

        protected double Cal_SCP()
        {
            if (this.Sum_RCB == 0) return 0;
            return Math.Round(((double) (this.L3RCB + this.R3RCB) / (double) this.Sum_RCB) * 100.0, 2);
        }

        protected double Cal_SDP()
        {
            if (this.Sum_RCB == 0) return 0;
            return Math.Round(((double) (this.L4RCB + this.R4RCB) / (double) this.Sum_RCB) * 100.0, 2);
        }

        protected double Cal_SER()
        {
            if (this.Sum_RCB == 0) return 0;
            return Math.Round(((double) (this.L5RCB + this.L5RCB) / (double) this.Sum_RCB) * 100.0, 2);
        }

        protected double Cal_Final_SER()
        {
            double SUM_S = this.Cal_SAP() + this.Cal_SBP() + this.Cal_SCP() + this.Cal_SDP() + this.Cal_SER();
            SUM_S = Math.Round(SUM_S, 2);
            double Final_SER = this.Cal_SER();
            if (SUM_S > 100.0)
                Final_SER -= SUM_S - 100.0;
            if (SUM_S < 100.0)
                Final_SER += 100.0 - SUM_S;
            Final_SER = Math.Round(Final_SER, 2);
            return Final_SER;
        }

        protected double Cal_M1Q()
        {
            return ((((0.3 * (int) this.R4RCB) + (0.3 * (int) this.L4RCB)) + (0.2 * (int) this.R5RCB)) +(0.1 * (int) this.R2RCB)) + (0.1 * (int) this.R1RCB);
        }

        protected double Cal_M2Q()
        {
            return ((((0.4 * (int) this.R2RCB) + (0.2 * (int) this.L2RCB)) + (0.2 * (int) this.R4RCB)) +(0.1 * (int) this.R5RCB)) + (0.1 * (int) this.L3RCB);
        }

        protected double Cal_M3Q()
        {
            return ((0.4 * (int) this.L2RCB) + (0.3 * (int) this.R5RCB)) + (0.3 * (int) this.L5RCB);
        }

        protected double Cal_M4Q()
        {
            return (((0.2 * (int) this.R1RCB) + (0.3 * (int) this.R3RCB)) + (0.3 * (int) this.R5RCB)) +(0.2 * (int) this.L5RCB);
        }

        protected double Cal_M5Q()
        {
            return ((((0.1 * (int) this.L2RCB) + (0.1 * (int) this.L3RCB)) + (0.1 * (int) this.R3RCB)) +(0.4 * (int) this.L4RCB)) + (0.3 * (int) this.R4RCB);
        }

        protected double Cal_M6Q()
        {
            return ((0.4 * (int) this.R3RCB) + (0.4 * (int) this.L3RCB)) + (0.2 * (int) this.L2RCB);
        }

        protected double Cal_M7Q()
        {
            return ((((0.2 * (int) this.R1RCB) + (0.5 * (int) this.L1RCB)) + (0.1 * (int) this.R2RCB)) +(0.1 * (int) this.L4RCB)) + (0.1 * (int) this.R4RCB);
        }

        protected double Cal_M8Q()
        {
            return (((0.5 * (int) this.R1RCB) + (0.1 * (int) this.R4RCB)) + (0.2 * (int) this.R2RCB)) +(0.2 * (int) this.L1RCB);
        }

        private double Cal_SUM_MQ()
        {
            //double Q1 = this.Cal_M1Q();
            //double Q2 = this.Cal_M2Q();
            //double Q3 = this.Cal_M3Q();
            //double Q4 = this.Cal_M4Q();
            //double Q5 = this.Cal_M5Q();
            //double Q6 = this.Cal_M6Q();
            //double Q7 = this.Cal_M7Q();
            //double Q8 = this.Cal_M8Q();
            return this.Cal_M1Q() + this.Cal_M2Q() + this.Cal_M3Q() + this.Cal_M4Q() +this.Cal_M5Q() + this.Cal_M6Q() + this.Cal_M7Q() + this.Cal_M8Q();
        }

        #region Variable Temporary

        private double PercentM1Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            return Math.Round((this.Cal_M1Q() / sum) * 100.0, 2);
        }

        private double PercentM2Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            return Math.Round((this.Cal_M2Q() / sum) * 100.0, 2);
        }

        private double PercentM3Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0.0) return 0;
            return Math.Round((this.Cal_M3Q() / sum) * 100.0, 2);
        }

        private double PercentM4Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            return Math.Round((this.Cal_M4Q() / sum) * 100.0, 2);
        }

        private double PercentM5Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            return Math.Round((this.Cal_M5Q() / sum) * 100.0, 2);
        }

        private double PercentM6Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            return Math.Round((this.Cal_M6Q() / sum) * 100.0, 2);
        }

        private double PercentM7Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            double s = Math.Round((this.Cal_M7Q() / sum) * 100.0, 2);
            return s;
        }

        private double PercentM8Q()
        {
            double sum = this.Cal_SUM_MQ();
            if (sum == 0) return 0;
            double s = Math.Round((this.Cal_M8Q() / sum) * 100.0, 2);
            return s;
        }

        #endregion

        protected int Cal_EQVP()
        {
            return (int) Math.Round((double) (this.PercentM7Q() + this.PercentM8Q()));
        }

        protected int Cal_CQVP()
        {
            return (int) Math.Round((double) (this.PercentM3Q() + this.PercentM5Q()));
        }

        protected int Cal_AQVP()
        {
            return (int) Math.Round((double) (this.PercentM4Q() + this.PercentM6Q()));
        }

        protected int Cal_IQVP()
        {
            return (int) Math.Round((double) (this.PercentM1Q() + this.PercentM2Q()));
        }

        protected double Cal_L1RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.L1RCB) / ((double) this.Cal_Sum_RCB())) * 100.0;
        }

        protected double Cal_L2RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.L2RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_L3RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.L3RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_L4RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.L4RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_L5RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.L5RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_R1RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.R1RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_R2RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.R2RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_R3RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.R3RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_R4RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.R4RCB) / ((double) sum)) * 100.0;
        }

        protected double Cal_R5RCP()
        {
            int sum = this.Cal_Sum_RCB();
            if (sum == 0) return 0;
            return (((double) this.R5RCB) / ((double) sum)) * 100.0;
        }

        #region Cal For SBZ

        private double Cal_S1()
        {
            return ((double) ((((R1RCB * 2) + (R2RCB * 2)) + R5RCB) + L2RCB)) / 6.0;
        }

        private double Cal_S2()
        {
            return ((double) ((((R5RCB + (R2RCB * 2)) + (R1RCB * 2)) + R3RCB) + L2RCB)) / 7.0;
        }

        private double Cal_S3()
        {
            return ((double) (((R5RCB + (R2RCB * 2)) + R1RCB) + (L2RCB * 2))) / 6.0;
        }

        private double Cal_S4()
        {
            return ((double) ((((R5RCB + (R1RCB * 2)) + R2RCB) + L3RCB) + (R1RCB * 2))) / 7.0;
        }

        private double Cal_S5()
        {
            return ((double) (((((R5RCB * 2) + L5RCB) + R1RCB) + R2RCB) + (R3RCB * 2))) / 7.0;
        }

        private double Cal_S6()
        {
            return ((double) (((R5RCB + (L5RCB * 2)) + R1RCB) + (R3RCB * 2))) / 6.0;
        }

        private double Cal_S7()
        {
            return ((double) ((((R5RCB * 2) + R1RCB) + R3RCB) + (L2RCB * 2))) / 6.0;
        }

        private double Cal_S8()
        {
            return ((double) (((R5RCB + L5RCB) + R2RCB) + L2RCB)) / 4.0;
        }

        private double Cal_S9()
        {
            return ((double) (((((R5RCB + R3RCB) + L3RCB) + (L2RCB * 2)) + L1RCB) + L4RCB)) / 7.0;
        }

        private double Cal_S10()
        {
            return ((double) ((((L1RCB * 2) + R1RCB) + R2RCB) + (R5RCB * 2))) / 6.0;
        }

        private double Cal_S11()
        {
            return ((double) (((((R5RCB + R4RCB) + R2RCB) + (L1RCB * 2)) + L2RCB) + L3RCB)) / 7.0;
        }

        private double Cal_S12()
        {
            return ((double) (((R5RCB + R1RCB) + (R4RCB * 3)) + L1RCB)) / 6.0;
        }

        private double Cal_S13()
        {
            return ((double) ((((R5RCB + R1RCB) + R2RCB) + R4RCB) + L1RCB)) / 5.0;
        }

        private double Cal_S14()
        {
            return ((double) (((R5RCB + R1RCB) + R2RCB) + (R4RCB * 3))) / 6.0;
        }

        private double Cal_S15()
        {
            return ((double) ((((R5RCB + (R2RCB * 2)) + R4RCB) + (R1RCB * 2)) + L1RCB)) / 7.0;
        }

        private double Cal_S16()
        {
            return ((double) ((((((R5RCB + R1RCB) + R2RCB) + R4RCB) + R1RCB) + (L1RCB * 2)) + (L1RCB * 2))) / 9.0;
        }

        private double Cal_S17()
        {
            return ((double) ((((R2RCB + R3RCB) + R5RCB) + (R1RCB * 2)) + L1RCB)) / 6.0;
        }

        private double Cal_S18()
        {
            return ((double) ((((L3RCB * 3) + L2RCB) + L1RCB) + R3RCB)) / 6.0;
        }

        private double Cal_SUMS()
        {
            return this.Cal_S1() + this.Cal_S2() + this.Cal_S3() + this.Cal_S4() + this.Cal_S5() + this.Cal_S6() +
                   this.Cal_S7() + this.Cal_S8() + this.Cal_S9() +
                   this.Cal_S10() + this.Cal_S11() + this.Cal_S12() + this.Cal_S13() + this.Cal_S14() + this.Cal_S15() +
                   this.Cal_S16() + this.Cal_S17() + this.Cal_S18();
        }

        #endregion Cal For SBZ

        protected double Cal_S1BZ()
        {
            return this.Cal_S1() - this.Cal_SUMS();
        }

        protected double Cal_S2BZ()
        {
            return this.Cal_S2() - this.Cal_SUMS();
        }

        protected double Cal_S3BZ()
        {
            return this.Cal_S3() - this.Cal_SUMS();
        }

        protected double Cal_S4BZ()
        {
            return this.Cal_S4() - this.Cal_SUMS();
        }

        protected double Cal_S5BZ()
        {
            return this.Cal_S5() - this.Cal_SUMS();
        }

        protected double Cal_S6BZ()
        {
            return this.Cal_S6() - this.Cal_SUMS();
        }

        protected double Cal_S7BZ()
        {
            return this.Cal_S7() - this.Cal_SUMS();
        }

        protected double Cal_S8BZ()
        {
            return this.Cal_S8() - this.Cal_SUMS();
        }

        protected double Cal_S9BZ()
        {
            return this.Cal_S9() - this.Cal_SUMS();
        }

        protected double Cal_S10BZ()
        {
            return this.Cal_S10() - this.Cal_SUMS();
        }

        protected double Cal_S11BZ()
        {
            return this.Cal_S11() - this.Cal_SUMS();
        }

        protected double Cal_S12BZ()
        {
            return this.Cal_S12() - this.Cal_SUMS();
        }

        protected double Cal_S13BZ()
        {
            return this.Cal_S13() - this.Cal_SUMS();
        }

        protected double Cal_S14BZ()
        {
            return this.Cal_S14() - this.Cal_SUMS();
        }

        protected double Cal_S15BZ()
        {
            return this.Cal_S15() - this.Cal_SUMS();
        }

        protected double Cal_S16BZ()
        {
            return this.Cal_S16() - this.Cal_SUMS();
        }

        protected double Cal_S17BZ()
        {
            return this.Cal_S17() - this.Cal_SUMS();
        }

        protected double Cal_S18BZ()
        {
            return this.Cal_S18() - this.Cal_SUMS();
        }

        /// <summary>
        /// Cal for SBZZ TickCount
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string Get_Tick_Count(double num)
        {
            string str = "";
            if (num < -5.0)
            {
                return "*";
            }
            if ((num >= -5.0) & (num < 3.0))
            {
                return "**";
            }
            if ((num >= -3.0) & (num < 1.5))
            {
                return "***";
            }
            if ((num >= 1.5) & (num < 3.0))
            {
                return "****";
            }
            if (num >= 3.0)
            {
                str = "*****";
            }
            return str;
        }

        protected string Cal_S1BZZ()
        {
            return this.Get_Tick_Count((double) this.S1BZ);
        }

        protected string Cal_S2BZZ()
        {
            return this.Get_Tick_Count((double) this.S2BZ);
        }

        protected string Cal_S3BZZ()
        {
            return this.Get_Tick_Count((double) this.S3BZ);
        }

        protected string Cal_S4BZZ()
        {
            return this.Get_Tick_Count((double) this.S4BZ);
        }

        protected string Cal_S5BZZ()
        {
            return this.Get_Tick_Count((double) this.S5BZ);
        }

        protected string Cal_S6BZZ()
        {
            return this.Get_Tick_Count((double) this.S6BZ);
        }

        protected string Cal_S7BZZ()
        {
            return this.Get_Tick_Count((double) this.S7BZ);
        }

        protected string Cal_S8BZZ()
        {
            return this.Get_Tick_Count((double) this.S8BZ);
        }

        protected string Cal_S9BZZ()
        {
            return this.Get_Tick_Count((double) this.S9BZ);
        }

        protected string Cal_S10BZZ()
        {
            return this.Get_Tick_Count((double) this.S10BZ);
        }

        protected string Cal_S11BZZ()
        {
            return this.Get_Tick_Count((double) this.S11BZ);
        }

        protected string Cal_S12BZZ()
        {
            return this.Get_Tick_Count((double) this.S12BZ);
        }

        protected string Cal_S13BZZ()
        {
            return this.Get_Tick_Count((double) this.S13BZ);
        }

        protected string Cal_S14BZZ()
        {
            return this.Get_Tick_Count((double) this.S14BZ);
        }

        protected string Cal_S15BZZ()
        {
            return this.Get_Tick_Count((double) this.S15BZ);
        }

        protected string Cal_S16BZZ()
        {
            return this.Get_Tick_Count((double) this.S16BZ);
        }

        protected string Cal_S17BZZ()
        {
            return this.Get_Tick_Count((double) this.S17BZ);
        }

        protected string Cal_S18BZZ()
        {
            return this.Get_Tick_Count((double) this.S18BZ);
        }

        /// <summary>
        /// Get Rank
        /// </summary>
        /// <param name="s"></param>
        /// <param name="ArrayX"></param>
        /// <returns></returns>
        private int GetLever(string s, ref object ArrayX)
        {
            int num3 = 0;
            int num4 = 0;
            object[,] objArray = new object[11, 3];
            objArray = (object[,]) ArrayX;
            int num2 = 0;
            do
            {
                if (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(objArray[num2, 1], s,
                    false))
                {
                    return (num2 + 1);
                }
                num2++;
                num4 = 9;
            }
            while (num2 <= num4);
            return num3;
        }

        #region Calculate the Rank for LRCP

        private object[,] ArrayLRCP()
        {
            object[,] objArray2 = new object[11, 3];
            objArray2[0, 0] = this.L1RCP;
            objArray2[0, 1] = "L1RCP";
            objArray2[1, 0] = this.L2RCP;
            objArray2[1, 1] = "L2RCP";
            objArray2[2, 0] = this.L3RCP;
            objArray2[2, 1] = "L3RCP";
            objArray2[3, 0] = this.L4RCP;
            objArray2[3, 1] = "L4RCP";
            objArray2[4, 0] = this.L5RCP;
            objArray2[4, 1] = "L5RCP";
            objArray2[5, 0] = this.R1RCP;
            objArray2[5, 1] = "R1RCP";
            objArray2[6, 0] = this.R2RCP;
            objArray2[6, 1] = "R2RCP";
            objArray2[7, 0] = this.R3RCP;
            objArray2[7, 1] = "R3RCP";
            objArray2[8, 0] = this.R4RCP;
            objArray2[8, 1] = "R4RCP";
            objArray2[9, 0] = this.R5RCP;
            objArray2[9, 1] = "R5RCP";
            object arrayA = objArray2;
            this.GetTop10(ref arrayA);
            objArray2 = (object[,]) arrayA;
            return objArray2;
        }

        private void GetTop10(ref object ArrayA)
        {
            int num6;
            int num5 = 8;
            int num3 = 0;
            do
            {
                int num4 = 0;
                do
                {
                    if (
                        this.compare(
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5 + 1, 0}, null)),
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5, 0}, null))))
                    {
                        decimal num =
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5 + 1, 0}, null));
                        string str =
                            Conversions.ToString(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5 + 1, 1}, null));
                        decimal num2 =
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5, 0}, null));
                        string str2 =
                            Conversions.ToString(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5, 1}, null));
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5, 0, num}, null);
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5, 1, str}, null);
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5 + 1, 0, num2}, null);
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5 + 1, 1, str2}, null);
                    }
                    num5--;
                    num4++;
                    num6 = 8;
                }
                while (num4 <= num6);
                num5 = 8;
                num3++;
                num6 = 9;
            }
            while (num3 <= num6);
        }

        private bool compare(decimal x, decimal y)
        {
            return ((decimal.Compare(x, y) > 0) || ((decimal.Compare(x, y) < 0) && false));
        }

        #endregion Unknown Function

        /// <summary>
        /// Cal for LRCP
        /// </summary>
        /// <param name="LRCPType"></param>
        /// <returns></returns>
        /// 
        protected int Cal_LRCPType(string LRCPType)
        {
            object arrayA = this.ArrayLRCP();
            int rank = 0;
            switch (LRCPType)
            {
                case "L1RCP1":
                    rank = this.GetLever("L1RCP", ref arrayA);
                    break;
                case "L2RCP2":
                    rank = this.GetLever("L2RCP", ref arrayA);
                    break;
                case "L3RCP3":
                    rank = this.GetLever("L3RCP", ref arrayA);
                    break;
                case "L4RCP4":
                    rank = this.GetLever("L4RCP", ref arrayA);
                    break;
                case "L5RCP5":
                    rank = this.GetLever("L5RCP", ref arrayA);
                    break;

                case "R1RCP1":
                    rank = this.GetLever("R1RCP", ref arrayA);
                    break;
                case "R2RCP2":
                    rank = this.GetLever("R2RCP", ref arrayA);
                    break;
                case "R3RCP3":
                    rank = this.GetLever("R3RCP", ref arrayA);
                    break;
                case "R4RCP4":
                    rank = this.GetLever("R4RCP", ref arrayA);
                    break;
                case "R5RCP5":
                    rank = this.GetLever("R5RCP", ref arrayA);
                    break;
            }
            return rank;
        }

        #region Calculate the Rank for MQ

        private object[,] ArrayMQP()
        {
            object[,] objArray = new object[9, 3];
            objArray[0, 0] = this.PercentM1Q();
            objArray[0, 1] = "M1QP";
            objArray[1, 0] = this.PercentM2Q();
            objArray[1, 1] = "M2QP";
            objArray[2, 0] = this.PercentM3Q();
            objArray[2, 1] = "M3QP";
            objArray[3, 0] = this.PercentM4Q();
            objArray[3, 1] = "M4QP";
            objArray[4, 0] = this.PercentM5Q();
            objArray[4, 1] = "M5QP";
            objArray[5, 0] = this.PercentM6Q();
            objArray[5, 1] = "M6QP";
            objArray[6, 0] = this.PercentM7Q();
            objArray[6, 1] = "M7QP";
            objArray[7, 0] = this.PercentM8Q();
            objArray[7, 1] = "M8QP";
            object arrayA = objArray;
            this.GetTop8(ref arrayA);
            objArray = (object[,]) arrayA;
            return objArray;
        }

        private void GetTop8(ref object ArrayA)
        {
            int num6;
            int num5 = 6;
            int num3 = 0;
            do
            {
                int num4 = 0;
                do
                {
                    if (
                        this.compare(
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5 + 1, 0}, null)),
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5, 0}, null))))
                    {
                        decimal num =
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5 + 1, 0}, null));
                        string str =
                            Conversions.ToString(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5 + 1, 1}, null));
                        decimal num2 =
                            Conversions.ToDecimal(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5, 0}, null));
                        string str2 =
                            Conversions.ToString(NewLateBinding.LateIndexGet(ArrayA, new object[] {num5, 1}, null));
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5, 0, num}, null);
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5, 1, str}, null);
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5 + 1, 0, num2}, null);
                        NewLateBinding.LateIndexSet(ArrayA, new object[] {num5 + 1, 1, str2}, null);
                    }
                    num5--;
                    num4++;
                    num6 = 6;
                }
                while (num4 <= num6);
                num5 = 6;
                num3++;
                num6 = 7;
            }
            while (num3 <= num6);
        }

        #endregion

        protected double Cal_MQPRType(string MQPRType)
        {
            object arrayA = this.ArrayMQP();
            double rank = 0;
            switch (MQPRType)
            {
                case "M1QPR":
                    rank = this.GetLever("M1QP", ref arrayA);
                    break;
                case "M2QPR":
                    rank = this.GetLever("M2QP", ref arrayA);
                    break;
                case "M3QPR":
                    rank = this.GetLever("M3QP", ref arrayA);
                    break;
                case "M4QPR":
                    rank = this.GetLever("M4QP", ref arrayA);
                    break;
                case "M5QPR":
                    rank = this.GetLever("M5QP", ref arrayA);
                    break;
                case "M6QPR":
                    rank = this.GetLever("M6QP", ref arrayA);
                    break;
                case "M7QPR":
                    rank = this.GetLever("M7QP", ref arrayA);
                    break;
                case "M8QPR":
                    rank = this.GetLever("M8QP", ref arrayA);
                    break;
            }
            return rank;
        }

        #region Calulate the ACP

        private void Cal_ACP(ref int num1, ref int num2, ref int num3, ref int num4, ref int num5)
        {
            double w = num4;
            double u = num3;
            double num182 = num1;
            double x = num5;
            double s = num2;

            this.FingerType_Count(this.L1T, ref w, ref u, ref num182, ref x, ref s);
            num2 = (int) Math.Round(s);
            num5 = (int) Math.Round(x);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(u);
            num4 = (int) Math.Round(w);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.L2T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.L3T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.L4T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.L5T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.R1T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.R2T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.R3T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.R4T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
            s = num4;
            x = num3;
            num182 = num1;
            u = num5;
            w = num2;

            this.FingerType_Count(this.R5T, ref s, ref x, ref num182, ref u, ref w);
            num2 = (int) Math.Round(w);
            num5 = (int) Math.Round(u);
            num1 = (int) Math.Round(num182);
            num3 = (int) Math.Round(x);
            num4 = (int) Math.Round(s);
        }

        private void FingerType_Count(string FT, ref double W, ref double U, ref double R, ref double X, ref double S)
        {
            switch (FT)
            {
                case "Wt":
                case "We":
                case "Ws":
                case "Wi":
                case "Wp":
                case "Wc":
                case "Wd":
                case "Wx":
                    W++;
                    break;

                case "U":
                case "Lf":
                    U++;
                    break;

                case "R":
                case "Wlr":
                case "Wpr":
                    R++;
                    break;

                case "As":
                case "Ae":
                case "Au":
                case "Ar":
                case "At":
                case "Nx":
                case "Mx":
                case "Xa":
                    X++;
                    break;
            }
        }

        #endregion Calulate the ACP

        protected int Cal_ACPType(string ACPType)
        {
            int num1 = 0; //76
            int num2 = 0; //78
            int num3 = 0; //80
            int num4 = 0; //82
            int num5 = 0; //84
            this.Cal_ACP(ref num1, ref num2, ref num3, ref num4, ref num5);
            int result = 0;

            switch (ACPType)
            {
                case "AC2WP":
                    result = (int) Math.Round((double) ((((double) num4) / 10.0) * 100.0));
                    break;
                case "AC2UP":
                    result = (int) Math.Round((double) ((((double) num3) / 10.0) * 100.0));
                    break;
                case "AC2RP":
                    result = (int) Math.Round((double) ((((double) num1) / 10.0) * 100.0));
                    break;
                case "AC2XP":
                    result = (int) Math.Round((double) ((((double) num5) / 10.0) * 100.0));
                    break;
                case "AC2SP":
                    result = (int) Math.Round((double) ((((double) num2) / 10.0) * 100.0));
                    break;
            }
            return result;
        }

        #region Cal_RCP Help

        private int Sum_T1APTemp()
        {
            return (int) (this.L1RCB + this.R1RCB);
        }

        private int Sum_T1BPTemp()
        {
            return (int) (this.L2RCB + this.R2RCB);
        }

        private int Sum_TPTemp()
        {
            return (int) (this.Sum_T1APTemp() + this.Sum_T1BPTemp());
        }

        private int Sum_RCAPTemp()
        {
            return (int) (this.L3RCB + this.R3RCB);
        }

        private int Sum_RCHPTemp()
        {
            return (int) (this.L4RCB + this.R4RCB);
        }

        private int Sum_RCVPTemp()
        {
            return (int) (this.L5RCB + this.R5RCB);
        }

        private int Sum_RCPTemp()
        {
            return this.Sum_RCAPTemp() + this.Sum_RCHPTemp() + this.Sum_RCVPTemp();
        }

        #endregion Cal_RCP Help

        protected double Cal_RCAP()
        {
            int sum = this.Sum_RCPTemp();
            if (sum == 0) return 0;
            return Math.Round((((double) this.Sum_RCAPTemp()) / ((double) sum)) * 100.0, 1);
        }

        protected double Cal_RCHP()
        {
            int sum = this.Sum_RCPTemp();
            if (sum == 0) return 0;
            return Math.Round((((double) this.Sum_RCHPTemp()) / ((double) sum)) * 100.0, 1);
        }

        private double Cal_RCVP()
        {
            int sum = this.Sum_RCPTemp();
            if (sum == 0) return 0;
            return Math.Round((((double) this.Sum_RCHPTemp()) / ((double)sum)) * 100.0, 1);
        }

        protected double Cal_Final_RCVP()
        {
            double RCVP = 100.0 - this.Cal_RCAP() - this.Cal_RCHP();
            double SUM_RCP = this.Cal_RCAP() + this.Cal_RCHP() + RCVP;
            SUM_RCP = Math.Round(SUM_RCP, 1);
            if (SUM_RCP > 100.0)
                RCVP -= SUM_RCP - 100.0;
            if (SUM_RCP < 100.0)
                RCVP += 100.0 - SUM_RCP;
            RCVP = Math.Round(RCVP, 1);
            return RCVP;
        }

        protected double Cal_T1AP()
        {
            int sum = this.Sum_TPTemp();
            if (sum == 0) return 0;
            return (((double) this.Sum_T1APTemp()) / ((double) sum)) * 100.0;
        }

        protected double Cal_T1BP()
        {
            int sum = this.Sum_TPTemp();
            if (sum == 0) return 0;
            return (((double) this.Sum_T1BPTemp()) / ((double) sum)) * 100.0;
        }

        //public CustomerAnalyseModel(ref FIFingerAnalysis AnalysisEntity, ref List<FIMQChart> ArrayChartEntity)
        //{
        //    BulidAnalysisEntity(ref AnalysisEntity);
        //    BuildChartEntity(ref ArrayChartEntity);
        //}
        private void BuildAnalysisEntity(ref FIFingerAnalysis AnalysisEntity)
        {
            AnalysisEntity.LT_BLS = this.LT_BLS;
            AnalysisEntity.RT_BLS = this.RT_BLS;
            AnalysisEntity.Page_BLS = this.Page_BLS;
            AnalysisEntity.SAP = this.SAP;
            AnalysisEntity.SBP = this.SBP;
            AnalysisEntity.SCP = this.SCP;
            AnalysisEntity.SDP = this.SDP;
            AnalysisEntity.SER = this.SER;
            AnalysisEntity.TFRC = this.TFRC;
            AnalysisEntity.EQVP = this.EQVP;
            AnalysisEntity.IQVP = this.IQVP;
            AnalysisEntity.AQVP = this.AQVP;
            AnalysisEntity.CQVP = this.CQVP;
            AnalysisEntity.L1RCP = this.L1RCP;
            AnalysisEntity.L2RCP = this.L2RCP;
            AnalysisEntity.L3RCP = this.L3RCP;
            AnalysisEntity.L4RCP = this.L4RCP;
            AnalysisEntity.L5RCP = this.L5RCP;
            AnalysisEntity.R1RCP = this.R1RCP;
            AnalysisEntity.R2RCP = this.R2RCP;
            AnalysisEntity.R3RCP = this.R3RCP;
            AnalysisEntity.R4RCP = this.R4RCP;
            AnalysisEntity.R5RCP = this.R5RCP;
            AnalysisEntity.L1RCP1 = this.L1RCP1;
            AnalysisEntity.L2RCP2 = this.L2RCP2;
            AnalysisEntity.L3RCP3 = this.L3RCP3;
            AnalysisEntity.L4RCP4 = this.L4RCP4;
            AnalysisEntity.L5RCP5 = this.L5RCP5;
            AnalysisEntity.R1RCP1 = this.R1RCP1;
            AnalysisEntity.R2RCP2 = this.R2RCP2;
            AnalysisEntity.R3RCP3 = this.R3RCP3;
            AnalysisEntity.R4RCP4 = this.R4RCP4;
            AnalysisEntity.R5RCP5 = this.R5RCP5;
            AnalysisEntity.ATDR = (decimal) this.ATDR;
            AnalysisEntity.ATDL = (decimal) this.ATDL;
            AnalysisEntity.ATDRA = this.ATDRA;
            AnalysisEntity.ATDRS = this.ATDRS;
            AnalysisEntity.ATDLA = this.ATDLA;
            AnalysisEntity.ATDLS = this.ATDLS;
            AnalysisEntity.AC2WP = this.AC2WP;
            AnalysisEntity.AC2UP = this.AC2UP;
            AnalysisEntity.AC2RP = this.AC2RP;
            AnalysisEntity.AC2XP = this.AC2XP;
            AnalysisEntity.AC2SP = this.AC2SP;
            AnalysisEntity.RCAP = this.RCAP;
            AnalysisEntity.RCHP = this.RCHP;
            AnalysisEntity.RCVP = this.RCVP;
            AnalysisEntity.T1AP = this.T1AP;
            AnalysisEntity.T1BP = this.T1BP;
            AnalysisEntity.M1Q = this.M1Q;
            AnalysisEntity.M2Q = this.M2Q;
            AnalysisEntity.M3Q = this.M3Q;
            AnalysisEntity.M4Q = this.M4Q;
            AnalysisEntity.M5Q = this.M5Q;
            AnalysisEntity.M6Q = this.M6Q;
            AnalysisEntity.M7Q = this.M7Q;
            AnalysisEntity.M8Q = this.M8Q;
            AnalysisEntity.L1RCB = this.L1RCB;
            AnalysisEntity.L2RCB = this.L2RCB;
            AnalysisEntity.L3RCB = this.L3RCB;
            AnalysisEntity.L4RCB = this.L4RCB;
            AnalysisEntity.L5RCB = this.L5RCB;
            AnalysisEntity.R1RCB = this.R1RCB;
            AnalysisEntity.R2RCB = this.R2RCB;
            AnalysisEntity.R3RCB = this.R3RCB;
            AnalysisEntity.R4RCB = this.R4RCB;
            AnalysisEntity.R5RCB = this.R5RCB;
            AnalysisEntity.Label1 = this.Label1;
            AnalysisEntity.Label2 = this.Label2;
            AnalysisEntity.Label3 = this.Label3;
            AnalysisEntity.Label4 = this.Label4;
            AnalysisEntity.Label5 = this.Label5;
            AnalysisEntity.Label6 = this.Label6;
            AnalysisEntity.L1T = this.L1T;
            AnalysisEntity.L2T = this.L2T;
            AnalysisEntity.L3T = this.L3T;
            AnalysisEntity.L4T = this.L4T;
            AnalysisEntity.L5T = this.L5T;
            AnalysisEntity.R1T = this.R1T;
            AnalysisEntity.R2T = this.R2T;
            AnalysisEntity.R3T = this.R3T;
            AnalysisEntity.R4T = this.R4T;
            AnalysisEntity.R5T = this.R5T;
            AnalysisEntity.S1BZ = this.S1BZ;
            AnalysisEntity.S2BZ = this.S2BZ;
            AnalysisEntity.S3BZ = this.S3BZ;
            AnalysisEntity.S4BZ = this.S4BZ;
            AnalysisEntity.S5BZ = this.S5BZ;
            AnalysisEntity.S6BZ = this.S6BZ;
            AnalysisEntity.S7BZ = this.S7BZ;
            AnalysisEntity.S8BZ = this.S8BZ;
            AnalysisEntity.S9BZ = this.S9BZ;
            AnalysisEntity.S10BZ = this.S10BZ;
            AnalysisEntity.S11BZ = this.S11BZ;
            AnalysisEntity.S12BZ = this.S12BZ;
            AnalysisEntity.S13BZ = this.S13BZ;
            AnalysisEntity.S14BZ = this.S14BZ;
            AnalysisEntity.S15BZ = this.S15BZ;
            AnalysisEntity.S16BZ = this.S16BZ;
            AnalysisEntity.S17BZ = this.S17BZ;
            AnalysisEntity.S18BZ = this.S18BZ;

            AnalysisEntity.S1BZZ = this.S1BZZ;
            AnalysisEntity.S2BZZ = this.S2BZZ;
            AnalysisEntity.S3BZZ = this.S3BZZ;
            AnalysisEntity.S4BZZ = this.S4BZZ;
            AnalysisEntity.S5BZZ = this.S5BZZ;
            AnalysisEntity.S6BZZ = this.S6BZZ;
            AnalysisEntity.S7BZZ = this.S7BZZ;
            AnalysisEntity.S8BZZ = this.S8BZZ;
            AnalysisEntity.S9BZZ = this.S9BZZ;
            AnalysisEntity.S10BZZ = this.S10BZZ;
            AnalysisEntity.S11BZZ = this.S11BZZ;
            AnalysisEntity.S12BZZ = this.S12BZZ;
            AnalysisEntity.S13BZZ = this.S13BZZ;
            AnalysisEntity.S14BZZ = this.S14BZZ;
            AnalysisEntity.S15BZZ = this.S15BZZ;
            AnalysisEntity.S16BZZ = this.S16BZZ;
            AnalysisEntity.S17BZZ = this.S17BZZ;
            AnalysisEntity.S18BZZ = this.S18BZZ;

            AnalysisEntity.M1QPR = this.M1QPR;
            AnalysisEntity.M2QPR = this.M2QPR;
            AnalysisEntity.M3QPR = this.M3QPR;
            AnalysisEntity.M4QPR = this.M4QPR;
            AnalysisEntity.M5QPR = this.M5QPR;
            AnalysisEntity.M6QPR = this.M6QPR;
            AnalysisEntity.M7QPR = this.M7QPR;
            AnalysisEntity.M8QPR = this.M8QPR;
        }

        private void BuildChartEntity(ref List<FIMQChart> ArrayChartEntity)
        {
            FIMQChart objChart1 = new FIMQChart
                                  {
                                      MQID = "M1Q",
                                      MQDesc = "语言",
                                      MQValue = this.M1Q
                                  };
            FIMQChart objChart2 = new FIMQChart
                                  {
                                      MQID = "M2Q",
                                      MQDesc = "数学",
                                      MQValue = this.M2Q
                                  };
            FIMQChart objChart3 = new FIMQChart
                                  {
                                      MQID = "M3Q",
                                      MQDesc = "空间",
                                      MQValue = this.M3Q
                                  };
            FIMQChart objChart4 = new FIMQChart
                                  {
                                      MQID = "M4Q",
                                      MQDesc = "自然",
                                      MQValue = this.M4Q
                                  };
            FIMQChart objChart5 = new FIMQChart
                                  {
                                      MQID = "M5Q",
                                      MQDesc = "音乐",
                                      MQValue = this.M5Q
                                  };
            FIMQChart objChart6 = new FIMQChart
                                  {
                                      MQID = "M6Q",
                                      MQDesc = "肢体",
                                      MQValue = this.M6Q
                                  };
            FIMQChart objChart7 = new FIMQChart
                                  {
                                      MQID = "M7Q",
                                      MQDesc = "人际",
                                      MQValue = this.M7Q
                                  };
            FIMQChart objChart8 = new FIMQChart
                                  {
                                      MQID = "M8Q",
                                      MQDesc = "内省",
                                      MQValue = this.M8Q
                                  };

            ArrayChartEntity.Add(objChart1);
            ArrayChartEntity.Add(objChart2);
            ArrayChartEntity.Add(objChart3);
            ArrayChartEntity.Add(objChart4);
            ArrayChartEntity.Add(objChart5);
            ArrayChartEntity.Add(objChart6);
            ArrayChartEntity.Add(objChart7);
            ArrayChartEntity.Add(objChart8);
        }

        public decimal _100AQ
        {
            get { return this.Cal_100AQ(); }
        }

        public decimal _100CQ
        {
            get { return this.Cal_100CQ(); }
        }

        public decimal _100EQ
        {
            get { return this.Cal_100EQ(); }
        }

        public decimal _100IQ
        {
            get { return this.Cal_100IQ(); }
        }

        public decimal ATD_Average
        {
            get { return this.Cal_ATD_Average(); }
        }

        public string IN1_IT_construction
        {
            get { return this.Cal_IN1_IT_construction(); }
        }

        public string IN10_psycho
        {
            get { return this.Cal_IN10_psycho(); }
        }

        public string IN10_sales
        {
            get { return this.Cal_IN10_sales(); }
        }

        public string IN11_mass
        {
            get { return this.Cal_IN11_mass(); }
        }

        public string IN12_lang
        {
            get { return this.Cal_IN12_lang(); }
        }

        public string IN13_lit
        {
            get { return this.Cal_IN13_lit(); }
        }

        public string IN14_edu
        {
            get { return this.Cal_IN14_edu(); }
        }

        public string IN15_pol
        {
            get { return this.Cal_IN15_pol(); }
        }

        public string IN16_mgn
        {
            get { return this.Cal_IN16_mgn(); }
        }

        public string IN17_fin
        {
            get { return this.Cal_IN17_fin(); }
        }

        public string IN18_spo
        {
            get { return this.Cal_IN18_spo(); }
        }

        public string IN19_fin2
        {
            get { return this.Cal_IN19_fin2(); }
        }

        public string IN2_eng
        {
            get { return this.Cal_IN2_eng(); }
        }

        public string IN3_math
        {
            get { return this.Cal_IN3_math(); }
        }

        public string IN4_med
        {
            get { return this.Cal_IN4_med(); }
        }

        public string IN5_life
        {
            get { return this.Cal_IN5_life(); }
        }

        public string IN6_agri
        {
            get { return this.Cal_IN6_agri(); }
        }

        public string IN7_earth
        {
            get { return this.Cal_IN7_earth(); }
        }

        public string IN9_art
        {
            get { return this.Cal_IN9_art(); }
        }

        public string L1TCT
        {
            get { return this.Cal_L1TCT(); }
        }

        public decimal L1RCBVA
        {
            get { return this.Cal_L1RCBVA(); }
        }

        public decimal L1RCD
        {
            get { return this.Cal_L1RCD(); }
        }

        public decimal L2RCBVA
        {
            get { return this.Cal_L2RCBVA(); }
        }

        public decimal L2RCD
        {
            get { return this.Cal_L2RCD(); }
        }

        public decimal L3RCBVA
        {
            get { return this.Cal_L3RCBVA(); }
        }

        public decimal L3RCD
        {
            get { return this.Cal_L3RCD(); }
        }

        public decimal L4RCBVA
        {
            get { return this.Cal_L4RCBVA(); }
        }

        public decimal L4RCD
        {
            get { return this.Cal_L4RCD(); }
        }

        public decimal L5RCBVA
        {
            get { return this.Cal_L5RCBVA(); }
        }

        public decimal L5RCD
        {
            get { return this.Cal_L5RCD(); }
        }

        public decimal L5RCP_Final
        {
            get { return this.Cal_L5RCP_Final(); }
        }

        public decimal LTRC
        {
            get { return this.Cal_LTRC(); }
        }

        public decimal LTRCP
        {
            get { return this.Cal_LTRCP(); }
        }

        public decimal M1QP
        {
            get { return this.Cal_M1QP(); }
        }

        public decimal M2QP
        {
            get { return this.Cal_M2QP(); }
        }

        public decimal M3QP
        {
            get { return this.Cal_M3QP(); }
        }

        public decimal M4QP
        {
            get { return this.Cal_M4QP(); }
        }

        public decimal M5QP
        {
            get { return this.Cal_M5QP(); }
        }

        public decimal M6QP
        {
            get { return this.Cal_M6QP(); }
        }

        public decimal M7QP
        {
            get { return this.Cal_M7QP(); }
        }

        public decimal M8QP
        {
            get { return this.Cal_M8QP(); }
        }

        public string MA1
        {
            get { return this.Cal_MA1(); }
        }

        public string MA2
        {
            get { return this.Cal_MA2(); }
        }

        public string MA3
        {
            get { return this.Cal_MA3(); }
        }

        public string MA4
        {
            get { return this.Cal_MA4(); }
        }

        public string MA5
        {
            get { return this.Cal_MA5(); }
        }

        public string MA6
        {
            get { return this.Cal_MA6(); }
        }

        public string MA7
        {
            get { return this.Cal_MA7(); }
        }

        public string MA8
        {
            get { return this.Cal_MA8(); }
        }

        public string MALATD
        {
            get { return this.Cal_MALATD(); }
        }

        public string MARATD
        {
            get { return this.Cal_MARATD(); }
        }

        public string plus_X
        {
            get { return this.Cal_plus_X(); }
        }

        public decimal R1RCBVA
        {
            get { return this.Cal_R1RCBVA(); }
        }

        public decimal R1RCD
        {
            get { return this.Cal_R1RCD(); }
        }

        public string R1TCT
        {
            get { return this.Cal_R1TCT(); }
        }

        public decimal R2RCBVA
        {
            get { return this.Cal_R2RCBVA(); }
        }

        public decimal R2RCD
        {
            get { return this.Cal_R2RCD(); }
        }

        public decimal R3RCBVA
        {
            get { return this.Cal_R3RCBVA(); }
        }

        public decimal R3RCD
        {
            get { return this.Cal_R3RCD(); }
        }

        public decimal R4RCBVA
        {
            get { return this.Cal_R4RCBVA(); }
        }

        public decimal R4RCD
        {
            get { return this.Cal_R4RCD(); }
        }

        public decimal R5RCBVA
        {
            get { return this.Cal_R5RCBVA(); }
        }

        public decimal R5RCD
        {
            get { return this.Cal_R5RCD(); }
        }

        public string Rank_AHP
        {
            get { return this.Cal_Rank_AHP(); }
        }

        public decimal RCVA
        {
            get { return this.Cal_RCVA(); }
        }

        public decimal RTRC
        {
            get { return this.Cal_RTRC(); }
        }

        public decimal RTRCP
        {
            get { return this.Cal_RTRCP(); }
        }

        public decimal T1BP_2
        {
            get { return this.Cal_T1BP_2(); }
        }

        protected decimal Cal_100AQ()
        {
            decimal sum = (this.AQVP + this.EQVP + this.CQVP + this.IQVP);
            if (sum == 0) return 0;
            return (this.AQVP /  sum * 100);
        }

        protected decimal Cal_100CQ()
        {
            decimal sum = (this.AQVP + this.EQVP + this.CQVP + this.IQVP);
            if (sum == 0) return 0;
            return (this.CQVP / sum * 100);
        }

        protected decimal Cal_100EQ()
        {
            decimal sum = (this.AQVP + this.EQVP + this.CQVP + this.IQVP);
            if (sum == 0) return 0;
            return (this.EQVP / sum * 100);
        }

        protected decimal Cal_100IQ()
        {
            decimal sum = (this.AQVP + this.EQVP + this.CQVP + this.IQVP);
            if (sum == 0) return 0;
            return (this.IQVP / sum * 100);
        }

        protected decimal Cal_ATD_Average()
        {
            return (((decimal) this.ATDL + (decimal) this.ATDR) / 2);
        }

        protected string Cal_IN1_IT_construction()
        {
            string _IN1_IT_construction = "";
            if ((this.M2QPR + this.M3QPR) / 2 <= 2.4m)
            {
                _IN1_IT_construction = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR) / 2 > 2.4m)
                     && ((this.M2QPR + this.M3QPR) / 2 <= 3m))
            {
                _IN1_IT_construction = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR) / 2 > 3m)
                     && ((this.M2QPR + this.M3QPR) / 2 <= 4.99m))
            {
                _IN1_IT_construction = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR) / 2 > 4.99m)
                     && ((this.M2QPR + this.M3QPR) / 2 <= 7.99m))
            {
                _IN1_IT_construction = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M3QPR) / 2 > 7.99m)
            {
                _IN1_IT_construction = @"❤❤";
            }
            return _IN1_IT_construction;
        }

        protected string Cal_IN10_psycho()
        {
            string _IN10_psycho = "";
            if ((this.M7QPR + this.M8QPR) / 2 <= 2.4m)
            {
                _IN10_psycho = @"❤❤❤❤❤❤";
            }
            else if (((this.M7QPR + this.M8QPR) / 2 > 2.4m)
                     && ((this.M7QPR + this.M8QPR) / 2 <= 3m))
            {
                _IN10_psycho = @"❤❤❤❤❤";
            }
            else if (((this.M7QPR + this.M8QPR) / 2 > 3m)
                     && ((this.M7QPR + this.M8QPR) / 2 <= 4.99m))
            {
                _IN10_psycho = @"❤❤❤❤";
            }
            else if (((this.M7QPR + this.M8QPR) / 2 > 4.99m)
                     && ((this.M7QPR + this.M8QPR) / 2 <= 7.99m))
            {
                _IN10_psycho = @"❤❤❤";
            }
            else if ((this.M7QPR + this.M8QPR) / 2 > 7.99m)
            {
                _IN10_psycho = @"❤❤";
            }
            return _IN10_psycho;
        }

        protected string Cal_IN10_sales()
        {
            string _IN10_sales = "";
            if ((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 <= 2.4m)
            {
                _IN10_sales = @"❤❤❤❤❤❤";
            }
            else if (((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 > 2.4m) &&
                     ((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 <= 3m))
            {
                _IN10_sales = @"❤❤❤❤❤";
            }
            else if (((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 > 3m) &&
                     ((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 <= 4.99m))
            {
                _IN10_sales = @"❤❤❤❤";
            }
            else if (((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 > 4.99m) &&
                     ((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 <= 7.99m))
            {
                _IN10_sales = @"❤❤❤";
            }
            else if ((this.M1QPR + (this.M6QPR) + this.M7QPR) / 3 > 7.99m)
            {
                _IN10_sales = @"❤❤";
            }
            return _IN10_sales;
        }

        protected string Cal_IN11_mass()
        {
            string _IN11_mass = "";
            if ((this.M1QPR + this.M6QPR + this.M3QPR) / 3 <= 2.4m)
            {
                _IN11_mass = @"❤❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M6QPR + this.M3QPR) / 3 > 2.4m) &&
                     ((this.M1QPR + this.M6QPR + this.M3QPR) / 3 <= 3m))
            {
                _IN11_mass = @"❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M6QPR + this.M3QPR) / 3 > 3m) &&
                     ((this.M1QPR + this.M6QPR + this.M3QPR) / 3 <= 4.99m))
            {
                _IN11_mass = @"❤❤❤❤";
            }
            else if (((this.M1QPR + this.M6QPR + this.M3QPR) / 3 > 4.99m) &&
                     ((this.M1QPR + this.M6QPR + this.M3QPR) / 3 <= 7.99m))
            {
                _IN11_mass = @"❤❤❤";
            }
            else if ((this.M1QPR + this.M6QPR + this.M3QPR) / 3 > 7.99m)
            {
                _IN11_mass = @"❤❤";
            }
            return _IN11_mass;
        }

        protected string Cal_IN12_lang()
        {
            string _IN12_lang = "";
            if ((this.M6QPR + this.M3QPR) / 2 <= 2.4m)
            {
                _IN12_lang = @"❤❤❤❤❤❤";
            }
            else if (((this.M6QPR + this.M3QPR) / 2 > 2.4m) &&
                     ((this.M6QPR + this.M3QPR) / 2 <= 3))
            {
                _IN12_lang = @"❤❤❤❤❤";
            }
            else if (((this.M6QPR + this.M3QPR) / 2 >= 3m) &&
                     ((this.M6QPR + this.M3QPR) / 2 <= 4.99m))
            {
                _IN12_lang = @"❤❤❤❤";
            }
            else if (((this.M6QPR + this.M3QPR) / 2 > 4.99m) &&
                     ((this.M6QPR + this.M3QPR) / 2 <= 7.99m))
            {
                _IN12_lang = @"❤❤❤";
            }
            else if ((this.M6QPR + this.M3QPR) / 2 > 7.99m)
            {
                _IN12_lang = @"❤❤";
            }
            return _IN12_lang;
        }

        protected string Cal_IN13_lit()
        {
            string _IN13_lit = "";
            if ((this.M1QPR + this.M8QPR) / 2 <= 2.4m)
            {
                _IN13_lit = @"❤❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M8QPR) / 2 > 2.4m) &&
                     ((this.M1QPR + this.M8QPR) / 2 <= 3))
            {
                _IN13_lit = @"❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M8QPR) / 2 > 3m) &&
                     ((this.M1QPR + this.M8QPR) / 2 <= 4.99m))
            {
                _IN13_lit = @"❤❤❤❤";
            }
            else if (((this.M1QPR + this.M8QPR) / 2 > 4.99m) &&
                     ((this.M1QPR + this.M8QPR) / 2 <= 7.99m))
            {
                _IN13_lit = @"❤❤❤";
            }
            else if ((this.M1QPR + this.M8QPR) / 2 > 7.99m)
            {
                _IN13_lit = @"❤❤";
            }
            return _IN13_lit;
        }

        protected string Cal_IN14_edu()
        {
            string _IN14_edu = "";
            if ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 2.4m)
            {
                _IN14_edu = @"❤❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 2.4m) &&
                     ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 3m))
            {
                _IN14_edu = @"❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 3m) &&
                     ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 4.99m))
            {
                _IN14_edu = @"❤❤❤❤";
            }
            else if (((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 4.99m) &&
                     ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 7.99m))
            {
                _IN14_edu = @"❤❤❤";
            }
            else if ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 7.99m)
            {
                _IN14_edu = @"❤❤";
            }
            return _IN14_edu;
        }

        protected string Cal_IN15_pol()
        {
            string _IN15_pol = "";
            if ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 2.4m)
            {
                _IN15_pol = @"❤❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 2.4m) &&
                     ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 3))
            {
                _IN15_pol = @"❤❤❤❤❤";
            }
            else if (((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 3) &&
                     ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 4.99m))
            {
                _IN15_pol = @"❤❤❤❤";
            }
            else if (((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 4.99m) &&
                     ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 <= 7.99m))
            {
                _IN15_pol = @"❤❤❤";
            }
            else if ((this.M1QPR + this.M2QPR + this.M7QPR) / 3 > 7.99m)
            {
                _IN15_pol = @"❤❤";
            }
            return _IN15_pol;
        }

        protected string Cal_IN16_mgn()
        {
            string _IN16_mgn = "";
            if ((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 <= 2.4m)
            {
                _IN16_mgn = @"❤❤❤❤❤❤";
            }
            else if (((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 > 2.4m)
                     && ((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 <= 3m))
            {
                _IN16_mgn = @"❤❤❤❤❤";
            }
            else if (((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 > 3m)
                     && ((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 <= 4.99m))
            {
                _IN16_mgn = @"❤❤❤❤";
            }
            else if (((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 > 4.99m)
                     && ((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 <= 7.99m))
            {
                _IN16_mgn = @"❤❤❤";
            }
            else if ((this.M1QPR + (this.M8QPR) + this.M7QPR) / 3 > 7.99m)
            {
                _IN16_mgn = @"❤❤";
            }
            return _IN16_mgn;
        }

        protected string Cal_IN17_fin()
        {
            string _IN17_fin = "";
            if ((this.M2QPR + this.M2QPR) / 2 <= 2.4m)
            {
                _IN17_fin = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M2QPR) / 2 > 2.4m)
                     && ((this.M2QPR + this.M2QPR) / 2 <= 3m))
            {
                _IN17_fin = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M2QPR) / 2 > 3m)
                     && ((this.M2QPR + this.M2QPR) / 2 <= 4.99m))
            {
                _IN17_fin = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M2QPR) / 2 > 4.99m)
                     && ((this.M2QPR + this.M2QPR) / 2 <= 7.99m))
            {
                _IN17_fin = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M2QPR) / 2 > 7.99m)
            {
                _IN17_fin = @"❤❤";
            }
            return _IN17_fin;
        }

        protected string Cal_IN18_spo()
        {
            string _IN18_spo = "";
            if ((this.M6QPR + this.M3QPR) / 2 <= 2.4m)
            {
                _IN18_spo = @"❤❤❤❤❤❤";
            }
            else if (((this.M6QPR + this.M3QPR) / 2 > 2.4m)
                     && ((this.M6QPR + this.M3QPR) / 2 <= 3m))
            {
                _IN18_spo = @"❤❤❤❤❤";
            }
            else if (((this.M6QPR + this.M3QPR) / 2 > 3m)
                     && ((this.M6QPR + this.M3QPR) / 2 <= 4.99m))
            {
                _IN18_spo = @"❤❤❤❤";
            }
            else if (((this.M6QPR + this.M3QPR) / 2 > 4.99m)
                     && ((this.M6QPR + this.M3QPR) / 2 <= 7.99m))
            {
                _IN18_spo = @"❤❤❤";
            }
            else if ((this.M6QPR + this.M3QPR) / 2 > 7.99m)
            {
                _IN18_spo = @"❤❤";
            }
            return _IN18_spo;
        }

        protected string Cal_IN19_fin2()
        {
            string _IN19_fin2 = "";
            if ((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 <= 2.4m)
            {
                _IN19_fin2 = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 > 2.4m)
                     && ((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 <= 3m))
            {
                _IN19_fin2 = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 > 3m)
                     && ((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 <= 4.99m))
            {
                _IN19_fin2 = @"❤❤❤❤";
            }
            else if (((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 > 4.99m)
                     && ((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 <= 7.99m))
            {
                _IN19_fin2 = @"❤❤❤";
            }
            else if ((this.M2QPR + (this.M3QPR) + this.M7QPR) / 3 > 7.99m)
            {
                _IN19_fin2 = @"❤❤";
            }
            return _IN19_fin2;
        }

        protected string Cal_IN2_eng()
        {
            string _IN2_eng = "";
            if ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 2.4m)
            {
                _IN2_eng = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 2.4m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 3m))
            {
                _IN2_eng = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 3m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 4.99m))
            {
                _IN2_eng = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 4.99m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 7.99m))
            {
                _IN2_eng = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 7.99m)
            {
                _IN2_eng = @"❤❤";
            }
            return _IN2_eng;
        }

        protected string Cal_IN3_math()
        {
            string _IN3_math = "";
            if ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 2.4m)
            {
                _IN3_math = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 2.4m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 3m))
            {
                _IN3_math = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 3m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 4.99m))
            {
                _IN3_math = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 4.99m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 7.99m))
            {
                _IN3_math = @"❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 7.99m))
            {
                _IN3_math = @"❤❤";
            }
            return _IN3_math;
        }

        protected string Cal_IN4_med()
        {
            string _IN4_med = "";
            if ((this.M2QPR + this.M4QPR + this.M6QPR) / 3 <= 2.4m)
            {
                _IN4_med = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR + this.M6QPR) / 3 > 2.4m)
                     && ((this.M2QPR + this.M4QPR + this.M6QPR) / 3 <= 3m))
            {
                _IN4_med = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR + this.M6QPR) / 3 > 3m)
                     && ((this.M2QPR + this.M4QPR + this.M6QPR) / 3 <= 4.99m))
            {
                _IN4_med = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR + this.M6QPR) / 3 > 4.99m)
                     && ((this.M2QPR + this.M4QPR + this.M6QPR) / 3 <= 7.99m))
            {
                _IN4_med = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M4QPR + this.M6QPR) / 3 > 7.99m)
            {
                _IN4_med = @"❤❤";
            }
            return _IN4_med;
        }

        protected string Cal_IN5_life()
        {
            string _IN5_life = "";
            if ((this.M2QPR + this.M4QPR + this.M3QPR) / 3 <= 2.4m)
            {
                _IN5_life = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR + this.M3QPR) / 3 > 2.4m)
                     && ((this.M2QPR + this.M4QPR + this.M3QPR) / 3 <= 3m))
            {
                _IN5_life = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR + this.M3QPR) / 3 > 3m)
                     && ((this.M2QPR + this.M4QPR + this.M3QPR) / 3 <= 4.99m))
            {
                _IN5_life = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR + this.M3QPR) / 3 > 4.99m)
                     && ((this.M2QPR + this.M4QPR + this.M3QPR) / 3 <= 7.99m))
            {
                _IN5_life = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M4QPR + this.M3QPR) / 3 > 7.99m)
            {
                _IN5_life = @"❤❤";
            }
            return _IN5_life;
        }

        protected string Cal_IN6_agri()
        {
            string _IN6_agri = "";
            if ((this.M2QPR + this.M4QPR) / 2 <= 2.4m)
            {
                _IN6_agri = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR) / 2 > 2.4m)
                     && ((this.M2QPR + this.M4QPR) / 2 <= 3))
            {
                _IN6_agri = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR) / 2 > 3)
                     && ((this.M2QPR + this.M4QPR) / 2 <= 4.99m))
            {
                _IN6_agri = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M4QPR) / 2 > 4.99m)
                     && ((this.M2QPR + this.M4QPR) / 2 <= 7.99m))
            {
                _IN6_agri = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M4QPR) / 2 > 7.99m)
            {
                _IN6_agri = @"❤❤";
            }
            return _IN6_agri;
        }

        protected string Cal_IN7_earth()
        {
            string _IN7_earth = "";
            if ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 2.4m)
            {
                _IN7_earth = @"❤❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 2.4m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 3m))
            {
                _IN7_earth = @"❤❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 3m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 4.99m))
            {
                _IN7_earth = @"❤❤❤❤";
            }
            else if (((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 4.99m)
                     && ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 <= 7.99m))
            {
                _IN7_earth = @"❤❤❤";
            }
            else if ((this.M2QPR + this.M3QPR + this.M4QPR) / 3 > 7.99m)
            {
                _IN7_earth = @"❤❤";
            }
            return _IN7_earth;
        }

        protected string Cal_IN9_art()
        {
            string _IN9_art = "";
            if ((this.M5QPR + this.M3QPR + this.M6QPR) / 3 <= 2.4m)
            {
                _IN9_art = @"❤❤❤❤❤❤";
            }
            else if (((this.M5QPR + this.M3QPR + this.M6QPR) / 3 > 2.4m)
                     && ((this.M5QPR + this.M3QPR + this.M6QPR) / 3 <= 3))
            {
                _IN9_art = @"❤❤❤❤❤";
            }
            else if (((this.M5QPR + this.M3QPR + this.M6QPR) / 3 > 3)
                     && ((this.M5QPR + this.M3QPR + this.M6QPR) / 3 <= 4.99m))
            {
                _IN9_art = @"❤❤❤❤";
            }
            else if (((this.M5QPR + this.M3QPR + this.M6QPR) / 3 > 4.99m)
                     && ((this.M5QPR + this.M3QPR + this.M6QPR) / 3 <= 7.99m))
            {
                _IN9_art = @"❤❤❤";
            }
            else if ((this.M5QPR + this.M3QPR + this.M6QPR) / 3 > 7.99m)
            {
                _IN9_art = @"❤❤";
            }
            return _IN9_art;
        }

        protected decimal Cal_L1RCBVA()
        {
            return (this.L1RCB * this.RCVA);
        }

        protected decimal Cal_L1RCD()
        {
            decimal _L1RCD;
            if (this.L1T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L1RCD = (this.TFRC) / (2 * sum);
            }

            else if (this.L1T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L1RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _L1RCD = (this.L1RCB * 0.97m);
            }
            return _L1RCD;
        }

        protected string Cal_L1TCT()
        {
            string _L1TCT = "";
            switch (this.L1T)
            {
                case "Wt":
                case "We":
                case "Ws":
                case "Wi":
                case "Wp":
                case "Wc":
                case "Wd":
                    _L1TCT = "W";
                    break;
                case "U":
                case "Lf":
                    _L1TCT = "U";
                    break;
                case "R":
                case "Wlr":
                case "Wpr":
                    _L1TCT = "R";
                    break;
                case "As":
                case "Ae":
                case "Au":
                case "Ar":
                case "At":
                case "Nx":
                case "Mx":
                case "Xa":
                    _L1TCT = "X";
                    break;

                default:
                    _L1TCT = "";
                    break;
            }
            return _L1TCT;
        }

        protected decimal Cal_L2RCBVA()
        {
            return (this.L2RCB * this.RCVA);
        }

        protected decimal Cal_L2RCD()
        {
            decimal _L2RCD = 0;
            if (this.L2T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L2RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.L2T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L2RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _L2RCD = (this.L2RCB * 0.97m);
            }
            return _L2RCD;
        }

        protected decimal Cal_L3RCBVA()
        {
            return (this.L3RCB * this.RCVA);
        }

        protected decimal Cal_L3RCD()
        {
            decimal _L3RCD = 0;
            if (this.L3T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L3RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.L3T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L3RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _L3RCD = (this.L3RCB * 0.97m);
            }
            return _L3RCD;
        }

        protected decimal Cal_L4RCBVA()
        {
            return (this.L4RCB * this.RCVA);
        }

        protected decimal Cal_L4RCD()
        {
            decimal _L4RCD = 0;
            if (this.L4T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L4RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.L4T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L4RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _L4RCD = (this.L4RCB * 0.97m);
            }
            return _L4RCD;
        }

        protected decimal Cal_L5RCBVA()
        {
            return (this.L5RCB * this.RCVA);
        }

        protected decimal Cal_L5RCD()
        {
            decimal _L5RCD = 0;
            if (this.L5T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L5RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.L5T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _L5RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _L5RCD = (this.L5RCB * 0.97m);
            }
            return _L5RCD;
        }

        protected decimal Cal_L5RCP_Final()
        {
            return (100 - this.L1RCP - this.L2RCP - this.L3RCP - this.L4RCP - this.L5RCP - this.R1RCP - this.R2RCP -
                    this.R3RCP - this.R4RCP - this.R5RCP + this.L5RCP);
        }

        protected decimal Cal_LTRC()
        {
            return (this.L1RCB + this.L2RCB + this.L3RCB + this.L4RCB + this.L5RCB);
        }

        protected decimal Cal_LTRCP()
        {
            decimal sum = (this.L1RCP + this.L2RCP + this.L3RCP + this.L4RCP + this.L5RCP + this.R1RCP + this.R2RCP +this.R3RCP + this.R4RCP + this.R5RCP);
            if (sum == 0) return 0;
            return ((this.R1RCP + this.R2RCP + this.R3RCP + this.R4RCP + this.R5RCP) / sum) * 100;
        }

        protected decimal Cal_M1QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M1Q / (sum) * 100);
        }

        protected decimal Cal_M2QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M2Q / (sum) * 100);
        }

        protected decimal Cal_M3QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M3Q / (sum) * 100);
        }

        protected decimal Cal_M4QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M4Q / (sum) * 100);
        }

        protected decimal Cal_M5QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M5Q / (sum) * 100);
        }

        protected decimal Cal_M6QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M6Q / (sum) * 100);
        }

        protected decimal Cal_M7QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M7Q / (sum) * 100);
        }

        protected decimal Cal_M8QP()
        {
            decimal sum = this.M1Q + this.M2Q + this.M3Q + this.M4Q + this.M5Q + this.M6Q + this.M7Q + this.M8Q;
            if (sum == 0) return 0;
            return (this.M8Q / (sum) * 100);
        }

        protected string Cal_MA1()
        {
            string _MA1 = "";
            if (this.M1QPR == 1)
            {
                _MA1 = @"★★★★★";
            }
            else if ((this.M1QPR >= 1.01m) && (this.M1QPR <= 3m))
            {
                _MA1 = @"★★★★";
            }
            else if ((this.M1QPR >= 3.01m) && (this.M1QPR <= 4.99m))
            {
                _MA1 = @"★★★";
            }
            else if ((this.M1QPR >= 5m) && (this.M1QPR <= 7.99m))
            {
                _MA1 = @"★★";
            }
            else if (this.M1QPR == 8m)
            {
                _MA1 = @"★";
            }
            return _MA1;
        }

        protected string Cal_MA2()
        {
            string _MA2 = "";
            if (this.M2QPR == 1)
            {
                _MA2 = @"★★★★★";
            }
            else if ((this.M2QPR >= 1.01m) && (this.M2QPR <= 3m))
            {
                _MA2 = @"★★★★";
            }
            else if ((this.M2QPR >= 3.01m) && (this.M2QPR <= 4.99m))
            {
                _MA2 = @"★★★";
            }
            else if ((this.M2QPR >= 5m) && (this.M2QPR <= 7.99m))
            {
                _MA2 = @"★★";
            }
            else if (this.M2QPR == 8m)
            {
                _MA2 = @"★";
            }
            return _MA2;
        }

        protected string Cal_MA3()
        {
            string _MA3 = "";
            if (this.M3QPR == 1)
            {
                _MA3 = @"★★★★★";
            }
            else if ((this.M3QPR >= 1.01m) && (this.M3QPR <= 3m))
            {
                _MA3 = @"★★★★";
            }
            else if ((this.M3QPR >= 3.01m) && (this.M3QPR <= 4.99m))
            {
                _MA3 = @"★★★";
            }
            else if ((this.M3QPR >= 5m) && (this.M3QPR <= 7.99m))
            {
                _MA3 = @"★★";
            }
            else if (this.M3QPR == 8m)
            {
                _MA3 = @"★";
            }
            return _MA3;
        }

        protected string Cal_MA4()
        {
            string _MA4 = "";
            if (this.M4QPR == 1)
            {
                _MA4 = @"★★★★★";
            }
            else if ((this.M4QPR >= 1.01m) && (this.M4QPR <= 3m))
            {
                _MA4 = @"★★★★";
            }
            else if ((this.M4QPR >= 3.01m) && (this.M4QPR <= 4.99m))
            {
                _MA4 = @"★★★";
            }
            else if ((this.M4QPR >= 5m) && (this.M4QPR <= 7.99m))
            {
                _MA4 = @"★★";
            }
            else if (this.M4QPR == 8m)
            {
                _MA4 = @"★";
            }
            return _MA4;
        }

        protected string Cal_MA5()
        {
            string _MA5 = "";
            if (this.M5QPR == 1)
            {
                _MA5 = @"★★★★★";
            }
            else if ((this.M5QPR >= 1.01m) && (this.M5QPR <= 3m))
            {
                _MA5 = @"★★★★";
            }
            else if ((this.M5QPR >= 3.01m) && (this.M5QPR <= 4.99m))
            {
                _MA5 = @"★★★";
            }
            else if ((this.M5QPR >= 5m) && (this.M5QPR <= 7.99m))
            {
                _MA5 = @"★★";
            }
            else if (this.M5QPR == 8m)
            {
                _MA5 = @"★";
            }
            return _MA5;
        }

        protected string Cal_MA6()
        {
            string _MA6 = "";
            if (this.M6QPR == 1)
            {
                _MA6 = @"★★★★★";
            }
            else if ((this.M6QPR >= 1.01m) && (this.M6QPR <= 3m))
            {
                _MA6 = @"★★★★";
            }
            else if ((this.M6QPR >= 3.01m) && (this.M6QPR <= 4.99m))
            {
                _MA6 = @"★★★";
            }
            else if ((this.M6QPR >= 5m) && (this.M6QPR <= 7.99m))
            {
                _MA6 = @"★★";
            }
            else if (this.M6QPR == 8m)
            {
                _MA6 = @"★";
            }
            return _MA6;
        }

        protected string Cal_MA7()
        {
            string _MA7 = "";
            if (this.M7QPR == 1)
            {
                _MA7 = @"★★★★★";
            }
            else if ((this.M7QPR >= 1.01m) && (this.M7QPR <= 3m))
            {
                _MA7 = @"★★★★";
            }
            else if ((this.M7QPR >= 3.01m) && (this.M7QPR <= 4.99m))
            {
                _MA7 = @"★★★";
            }
            else if ((this.M7QPR >= 5m) && (this.M7QPR <= 7.99m))
            {
                _MA7 = @"★★";
            }
            else if (this.M7QPR == 8m)
            {
                _MA7 = @"★";
            }
            return _MA7;
        }

        protected string Cal_MA8()
        {
            string _MA8 = "";
            if (this.M8QPR == 1)
            {
                _MA8 = @"★★★★★";
            }
            else if ((this.M8QPR >= 1.01m) && (this.M8QPR <= 3m))
            {
                _MA8 = @"★★★★";
            }
            else if ((this.M8QPR >= 3.01m) && (this.M8QPR <= 4.99m))
            {
                _MA8 = @"★★★";
            }
            else if ((this.M8QPR >= 5m) && (this.M8QPR <= 7.99m))
            {
                _MA8 = @"★★";
            }
            else if (this.M8QPR == 8m)
            {
                _MA8 = @"★";
            }
            return _MA8;
        }

        protected string Cal_MALATD()
        {
            string _MALATD = "";
            if ((this.ATDL) < 35)
            {
                _MALATD = @"★★★★★";
            }
            else if ((this.ATDL) > 35 && (this.ATDL) < 40)
            {
                _MALATD = @"★★★★☆";
            }
            else if ((this.ATDL) > 39 && (this.ATDL) < 46)
            {
                _MALATD = @"★★★☆☆";
            }
            else if ((this.ATDL) > 45 && (this.ATDL) < 56)
            {
                _MALATD = @"★★☆☆☆";
            }
            else if ((this.ATDL) > 55)
            {
                _MALATD = @"★☆☆☆☆";
            }
            return _MALATD;
        }

        protected string Cal_MARATD()
        {
            string _MARATD = "";
            if ((this.ATDR) < 35)
            {
                _MARATD = @"★★★★★";
            }
            else if ((this.ATDR) > 35 && (this.ATDR) < 40)
            {
                _MARATD = @"★★★★☆";
            }
            else if ((this.ATDR) > 39 && (this.ATDR) < 46)
            {
                _MARATD = @"★★★☆☆";
            }
            else if ((this.ATDR) > 45 && (this.ATDR) < 56)
            {
                _MARATD = @"★★☆☆☆";
            }
            else if ((this.ATDR) > 55)
            {
                _MARATD = @"★☆☆☆☆";
            }
            return _MARATD;
        }

        protected string Cal_plus_X()
        {
            string _plus_X = "";
            if (this.AC2XP == 10)
            {
                _plus_X = @"+X";
            }
            else if (this.AC2XP == 20)
            {
                _plus_X = @"+2X";
            }
            else if (this.AC2XP == 30)
            {
                _plus_X = @"+3X";
            }
            else if (this.AC2XP == 40)
            {
                _plus_X = @"+4X";
            }
            else if (this.AC2XP == 50)
            {
                _plus_X = @"+5X";
            }
            else if (this.AC2XP == 60)
            {
                _plus_X = @"+6X";
            }
            else if (this.AC2XP == 70)
            {
                _plus_X = @"+7X";
            }
            else if (this.AC2XP == 80)
            {
                _plus_X = @"+8X";
            }
            else if (this.AC2XP == 90)
            {
                _plus_X = @"+9X";
            }
            else if (this.AC2XP == 100)
            {
                _plus_X = @"+10X";
            }
            return _plus_X;
        }

        protected decimal Cal_R1RCBVA()
        {
            return (this.R1RCB * this.RCVA);
        }

        protected decimal Cal_R1RCD()
        {
            decimal _R1RCD = 0;
            if (this.R1T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R1RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.R1T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R1RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _R1RCD = (this.R1RCB * 0.97m);
            }
            return _R1RCD;
        }

        protected string Cal_R1TCT()
        {
            string _R1TCT = "";
            switch (this.R1T)
            {
                case "Wt":
                case "We":
                case "Ws":
                case "Wi":
                case "Wp":
                case "Wc":
                case "Wd":
                    _R1TCT = "W";
                    break;
                case "U":
                case "Lf":
                    _R1TCT = "U";
                    break;
                case "Wlr":
                case "Wpr":
                case "R":
                    _R1TCT = "R";
                    break;
                case "As":
                case "Ae":
                case "Au":
                case "Ar":
                case "At":
                case "Nx":
                case "Mx":
                case "Xa":
                    _R1TCT = "X";
                    break;
                default:
                    _R1TCT = "";
                    break;
            }

            return _R1TCT;
        }

        protected decimal Cal_R2RCBVA()
        {
            return (this.R2RCB * this.RCVA);
        }

        protected decimal Cal_R2RCD()
        {
            decimal _R2RCD = 0;
            if (this.R2T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R2RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.R2T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R2RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _R2RCD = (this.R2RCB * 0.99m);
            }
            return _R2RCD;
        }

        protected decimal Cal_R3RCBVA()
        {
            return (this.R3RCB * this.RCVA);
        }

        protected decimal Cal_R3RCD()
        {
            decimal _R3RCD = 0;
            if (this.R3T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R3RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.R3T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R3RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _R3RCD = (this.R3RCB * 1.02m);
            }
            return _R3RCD;
        }

        protected decimal Cal_R4RCBVA()
        {
            return (this.R4RCB * this.RCVA);
        }

        protected decimal Cal_R4RCD()
        {
            decimal _R4RCD = 0;
            if (this.R4T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R4RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.R4T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R4RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _R4RCD = (this.R4RCB * 1.03m);
            }
            return _R4RCD;
        }

        protected decimal Cal_R5RCBVA()
        {
            return (this.R5RCB * this.RCVA);
        }

        protected decimal Cal_R5RCD()
        {
            decimal _R5RCD = 0;
            if (this.R5T == "AS")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R5RCD = (this.TFRC) / (2 * sum);
            }
            else if (this.R5T == "AT")
            {
                decimal sum = this.AC2WP + this.AC2UP + this.AC2RP + this.AC2SP;
                if (sum == 0) return 0;
                _R5RCD = (this.TFRC) / (2 * sum);
            }
            else
            {
                _R5RCD = (this.R5RCB * 1.07m);
            }
            return _R5RCD;
        }

        protected string Cal_Rank_AHP()
        {
            string _Rank_AHP = "";
            if (this.RCAP >= this.RCHP)
            {
                if (this.RCHP >= this.RCVP)
                {
                    _Rank_AHP = @"KAV";
                }
                else if (this.RCAP >= this.RCVP)
                {
                    _Rank_AHP = @"KVA";
                }
                else
                    _Rank_AHP = @"VKA";
            }
            else if (this.RCAP < this.RCHP)
            {
                if (this.RCAP >= this.RCVP)
                {
                    _Rank_AHP = @"AKV";
                }
                else if (this.RCHP >= this.RCVP)
                {
                    _Rank_AHP = @"AVK";
                }
                else
                    _Rank_AHP = @"VAK";
            }
            return _Rank_AHP;
        }

        protected decimal Cal_RCVA()
        {
            decimal _RCVA = 0;
            if (RTRC > LTRC)
            {
                if (LTRC == 0) return 0;
                _RCVA = RTRC / LTRC;
            }

            else
            {
                if (RTRC == 0) return 0;
                _RCVA = LTRC / RTRC;
            }
            return _RCVA;
        }

        protected decimal Cal_RTRC()
        {
            return (this.R1RCB + this.R2RCB + this.R3RCB + this.R4RCB + this.R5RCB);
        }

        protected decimal Cal_RTRCP()
        {
            return (((this.L1RCP + this.L2RCP + this.L3RCP + this.L4RCP + this.L5RCP) /
                     (this.L1RCP + this.L2RCP + this.L3RCP + this.L4RCP + this.L5RCP + this.R1RCP + this.R2RCP +
                      this.R3RCP + this.R4RCP + this.R5RCP)) * 100);
        }

        protected decimal Cal_T1BP_2()
        {
            return ((100 - this.T1AP - this.T1BP) + this.T1BP);
        }

        public string L1TTextBox
        {
            get { return this.Cal_L1T_TextBox(); }
        }

        public string R1TTextBox
        {
            get { return this.Cal_R1T_TextBox(); }
        }

        #region Fill Textbox trong trang "Khả năng giao tiếp bẩm sinh của tôi"

        protected string Cal_L1T_TextBox()
        {
            string sfill = "";
            switch (this.L1T)
            {
                case "As":
                    sfill = "Đặc điểm: Người theo chủ nghĩa duy tâm." + "\r\n" +
                            "\r\n" +
                            "Cần mẫn, chăm chỉ, có xu hướng làm việc theo tuần tự và tuân thủ quy trình. " + "\r\n" +
                            "Bảo thủ, không phô trương và giỏi giám sát công việc." + "\r\n" +
                            "Thật thà, cố chấp, làm việc cần cù, phấn đấu cho các nhu cầu cơ bản và an toàn." + "\r\n" +
                            "Ổn định và cố gắng phấn đấu." + "\r\n" +
                            "Nghiêm túc, ý chí mạnh mẽ." + "\r\n" +
                            "Hay che giấu cảm xúc, ít nói, và luôn phòng vệ." + "\r\n" +
                            "Quan tâm những việc đơn giản và cụ thể." + "\r\n" +
                            "Tiếp thu kiến thức như miếng bọt biển nhưng cần nhiều kiên nhẫn và thực hành." + "\r\n" +
                            "Phong cách làm việc: đơn giản, rõ ràng, bám sát quy trình.";
                    break;
                case "At":
                    sfill = "Đặc điểm: Người duy tâm lương thiện." + "\r\n" +
                            "\r\n" +
                            "Nhạy cảm nhưng đầy niềm đam mê và thực tế." + "\r\n" +
                            "Sống tình cảm và bốc đồng." + "\r\n" +
                            "Bảo thủ khi đưa ra quyết định, tiếp thu kiến thức như miếng bọt biển." + "\r\n" +
                            "Họ sẽ dễ dàng từ bỏ mục tiêu một khi không nhận được kết quả như mong đợi." + "\r\n" +
                            "Không mấy quan tâm đến ý kiến của người khác, có năng khiếu thưởng thức nghệ thuật." +
                            "\r\n" +
                            "Nghiêm túc, có trách nhiệm, chân thành." + "\r\n" +
                            "Phong cách làm việc: năng động, đi thẳng vào vấn đề, chân thành, thực tế.";
                    break;
                case "R":
                    sfill = "Đặc điểm: Nhà tư tưởng phê bình" + "\r\n" +
                            "Suy nghĩ phóng khoáng, độc lập." + "\r\n" +
                            "Tìm kiếm sự độc đáo, khác biệt từ người khác." + "\r\n" +
                            "Đam mê những điều mới lạ và bí ẩn." + "\r\n" +
                            "Tư duy sáng tạo." + "\r\n" +
                            "Khao khát tự do, hy vọng phá bỏ các khuôn khổ truyền thống" + "\r\n" +
                            "Óc quan sát nhạy bén, dể bị kích động (giàu cảm xúc)" + "\r\n" +
                            "Phản kháng, không bao giờ thỏa hiệp với những điều họ không muốn." + "\r\n" +
                            "Lối suy nghĩ tự do, không rập khuôn, không giới hạn bản thân, sở hữu tư duy phê bình." +
                            "\r\n" +
                            "Không chấp nhận cuộc sống đơn giản, tẻ nhạt, thích thú với những điều mới mẻ và thử thách. Linh hoạt và bí ẩn." +
                            "\r\n" +
                            "Đầu óc đầy sáng tạo. có thể đưa ra quyết định nhanh chóng và khả năng can thiệp khủng hoảng tốt." +
                            "\r\n" +
                            "Tư duy phê bình và luôn bộc phát nhiều ý tưởng mới." + "\r\n" +
                            "Thích hợp làm việc trong môi trường sáng tạo và SOHO." + "\r\n" +
                            "Không mấy quan tâm đến ý kiến người khác, luôn làm theo ý mình." + "\r\n" +
                            "Rất tò mò, có khả năng lập luận và tư duy ngược chiều tốt.";
                    break;
                case "U":
                    sfill = "Đặc điểm: Thành viên nhóm thân thiện." + "\r\n" +
                            "\r\n" +
                            "Xem trọng con người, là đồng nghiệp có tinh thần hợp tác và chấp hành tốt." + "\r\n" +
                            "Tư duy linh hoạt, giàu lòng bao dung, dễ chịu" + "\r\n" +
                            "Không thích sự xung đột, nhiệt tình hỗ trợ và dễ chấp thuận ý kiến của người khác." +
                            "\r\n" +
                            "Có khả năng thích nghi với môi trường xung quanh, dễ dàng hòa hợp với mọi người." + "\r\n" +
                            "Cư xử theo cách mà mọi người mong đợi." + "\r\n" +
                            "Chú ý tới các mối quan hệ cá nhân hay đồng cảm." + "\r\n" +
                            "Thỏa mái khi làm việc và thực hiện các hoạt động theo nhóm." + "\r\n" +
                            "Không giàu khả năng sáng tạo nhưng có khả năng mô phỏng tốt.";
                    break;
                case "Wd":
                    sfill = "Đặc điểm: Người đam mê thử thách." + "\r\n" +
                            "\r\n" +
                            "Tư duy hai chiều, đam mê thử thách." + "\r\n" +
                            "Không phải lúc nào cũng rập khuôn các nguyên tắc và quy định." + "\r\n" +
                            "Dể thích nghi, duy trì mối quan hệ tốt với mọi người, giàu kĩ năng giao tiếp với cộng đồng." +
                            "\r\n" +
                            "Đôi khi do cố gắng làm vừa lòng mọi người mà không giữ vững được lập trường riêng." +
                            "\r\n" +
                            "Có thể nhanh chóng xây dựng các mối quan hệ xã hội." + "\r\n" +
                            "Rất tò mò, hay thay đổi mục tiêu và thường theo đuổi nhiều mục tiêu cùng một lúc." + "\r\n" +
                            "Đam mê thử thách và rất linh hoạt, tuy nhiên khả năng kiểm soát mục tiêu không cao." +
                            "\r\n" +
                            "Dễ mất tập trung dẫn đến sai sót." + "\r\n" +
                            "Luôn luôn có kế hoạch B, thích có nhiều mục tiêu cùng một lúc." + "\r\n" +
                            "Dễ bị tác động và ảnh hưởng, không quyết đoán khi suy nghĩ vấn đề theo các khía cạnh khác nhau." +
                            "\r\n" +
                            "Hành vi linh hoạt, luôn luôn thay đổi các nguyên tắc và lập trường." + "\r\n" +
                            "Khả năng thích ứng tốt với thử thách trong tình huống khác nhau. Là một nhà lãnh đạo tình huống.";
                    break;
                case "We":
                    sfill = "Đặc điểm: Người theo chủ nghĩa hoàn hảo:" + "\r\n" +
                            "\r\n" +
                            "Là người theo đuổi mục tiêu, năng lực thiết lập mục tiêu và lên kế hoạch chi tiết rất tốt." +
                            "\r\n" +
                            "Cẩn trọng, chú ý đến độ chính xác của từng chi tiết nhỏ." + "\r\n" +
                            "Khả năng quản lý tốt, chính xác, tận tâm và rất tỉ mỉ." + "\r\n" +
                            "Là người rất thực tế, yêu cầu cao, làm việc có hệ thống, sáng tạo và kiên trì theo đuổi các chuẩn mực. " +
                            "\r\n" +
                            "Bạn có thể hoàn thành tốt các mục tiêu và công việc như đã định, nhưng thường hay bị mâu thuẫn giữa mặt lý trí/logic và mặt tình cảm, nhà lãnh đạo dạng này có xu hướng hay lo lắng và hay thay đổi tâm trạng đột ngột. " +
                            "\r\n" +
                            "Là người có năng lực tổ chức bẩm sinh và luôn theo đuổi sự hoàn hảo.";
                    break;
                case "Wi":
                    sfill = "Đặc điểm:  Người thử thách tận tâm" + "\r\n" +
                            "\r\n" +
                            "Luôn tìm kiếm các thử thách nhưng lại rất tận tâm trong công việc, do đó họ rất hay tự mâu thuẫn." +
                            "\r\n" +
                            "Tư duy hai chiều, đam mê thách thức." + "\r\n" +
                            "Rất tò mò và có xu hướng theo đuổi cùng lúc nhiều mục tiêu nhưng lại cân nhắc rất nhiều vấn đề." +
                            "\r\n" +
                            "Thích thử thách, đa dạng hóa mọi việc, kiểm soát mục tiêu rất chặt chẽ." + "\r\n" +
                            "Theo đuổi sự hoàn hảo nhưng lại rất dễ chán nản với các mục tiêu chưa đạt được." + "\r\n" +
                            "Chính xác, đầu óc phân tích, cẩn thận, tận tâm. " + "\r\n" +
                            "Là con người thực tế, rõ ràng, đặt tiêu chuẩn cao, làm việc có hệ thống. ";
                    break;
                case "Wp":
                    sfill = "Đặc điểm: Người theo đuổi trí tuệ ưu tú." + "\r\n" +
                            "\r\n" +
                            "Có năng lực thấu hiểu và tư duy tốt, khả năng tạo sự khác biệt, thể hiện khả năng lãnh đạo độc đáo và khả năng tư duy sáng tạo vượt trội." +
                            "\r\n" +
                            "Lối tư duy đa dạng, ấp ủ nhiều ý tưởng mới." + "\r\n" +
                            "Có nhận thức tuyệt vời, một nhà lãnh đạo rất ưu tú, có khả năng thuyết phục người khác." +
                            "\r\n" +
                            "Luôn hướng về phía trước, có năng lực tạo lập và khai phá các thị trường mới." + "\r\n" +
                            "Lạc quan, chu đáo và dễ được mọi người chấp nhận." + "\r\n" +
                            "Họ có phương pháp riêng để đạt được mục tiêu đặt ra." + "\r\n" +
                            "Có khả năng khám phá và thấu hiểu năng lực bản thân.";
                    break;
                case "Wpr":
                    sfill = "Đặc điểm: Nhà tư tưởng phê bình ưu tú." + "\r\n" +
                            "\r\n" +
                            "Tư duy phóng khoáng, độc lập." + "\r\n" +
                            "Tìm kiếm sự độc đáo, khác biệt từ người khác." + "\r\n" +
                            "Đam mê những điều mới lạ và bí ẩn. Tư suy đầy sáng tạo." + "\r\n" +
                            "Khao khát tự do, hy vọng phá bỏ các khuôn khổ truyền thống." + "\r\n" +
                            "Óc quan sát nhạy bén, dể bị kích động (giàu cảm xúc)." + "\r\n" +
                            "Phản kháng, không bao giờ thỏa hiệp với những điều họ không muốn." + "\r\n" +
                            "Lối suy nghĩ phóng khoáng, không rập khuôn, không giới hạn bản thân, tư duy phê bình." +
                            "\r\n" +
                            "Không chấp nhận cuộc sống đơn điệu, tẻ nhạt, thích thú với những điều mới mẻ và đầy thử thách. Linh hoạt và bí ẩn." +
                            "\r\n" +
                            "Đầu óc đầy sáng tạo, có thể đưa ra quyết định nhanh chóng và khả năng can thiệp khủng hoảng tốt." +
                            "\r\n" +
                            "Tư duy phê bình và luôn bộc phát nhiều ý tưởng mới." + "\r\n" +
                            "Thích hợp làm việc trong môi trường sáng tạo và SOHO." + "\r\n" +
                            "Không mấy quan tâm đến ý kiến người khác, luôn làm theo ý họ muốn." + "\r\n" +
                            "Rất tò mò, có khả năng lập luận và tư duy ngược chiều tốt.";
                    break;
                case "Ws":
                    sfill = "Đặc điểm: Người theo đuổi mục tiêu" + "\r\n" +
                            "\r\n" +
                            "Tự khám phá và thấu hiểu bản thân, tin tưởng vào lý luận và bằng chứng." + "\r\n" +
                            "Năng động, tràn đầy sức sống, hướng tới mục tiêu, tự khích lệ bản thân." + "\r\n" +
                            "Tích cực, chủ động, khả năng tự kiểm soát tốt, phấn đấu hướng tới mục tiêu" + "\r\n" +
                            "Cầu tiến, bốc đồng, năng động, hiệu quả làm việc cao." + "\r\n" +
                            "Tập trung cao độ và nghiêm túc khi làm việc, điều này khiến mọi người cảm thấy áp lực khi làm việc với bạn." +
                            "\r\n" +
                            "Đặc biệt xem trọng hình tượng, triển vọng và danh tiếng cá nhân." + "\r\n" +
                            "Không bao giờ bỏ cuộc, luôn kiên trì và nỗ lực hết mình.";
                    break;
                case "Wt":
                    sfill = "Đặc điểm bẩm sinh: Người theo đuổi mục tiêu." + "\r\n" +
                            "\r\n" +
                            "Tự khám phá và thấu hiểu bản thân, tin tưởng vào lý luận và bằng chứng." + "\r\n" +
                            "Năng động, tràn đầy sức sống, trung thành với mục tiêu, tự khích lệ bản thân." + "\r\n" +
                            "Tích cực, chủ động, khả năng tự kiểm soát tốt, phấn đấu hướng tới mục tiêu đề ra." + "\r\n" +
                            "Cầu tiến, bốc đồng, năng động, hiệu quả làm việc cao." + "\r\n" +
                            "Tập trung cao độ và nghiêm túc khi làm việc, điều này khiến mọi người cảm thấy áp lực khi làm việc cùng với bạn." +
                            "\r\n" +
                            "Đặc biệt xem trọng hình tượng, triển vọng và danh tiếng cá nhân." + "\r\n" +
                            "Không bao giờ bỏ cuộc, luôn kiên trì và nỗ lực mạnh mẽ.";
                    break;
                case "Wx":
                    sfill = "Đặc điểm: Nhà tư tưởng phê bình." + "\r\n" +
                            "\r\n" +
                            "Tính cách đa dạng, có nhiều ý tưởng mới lạ, độc đáo." + "\r\n" +
                            "Có chiều hướng hay thay đổi và không ổn định, năng lượng được phân tán ở khắp nơi." +
                            "\r\n" +
                            "Suy nghĩ phóng khoáng, chủ nghĩa cá nhân, khó đoán trước." + "\r\n" +
                            "Đòi hỏi sự khác biệt, độc đáo từ người khác." + "\r\n" +
                            "Thích những điều mới lạ và bí ẩn." + "\r\n" +
                            "Khao khát tự do, mong ước phá bỏ truyền thống." + "\r\n" +
                            "Óc quan sát nhạy bén, dễ bị kích động (giàu cảm xúc)" + "\r\n" +
                            "Phản kháng, không bao giờ thỏa hiệp với những điều họ không muốn." + "\r\n" +
                            "Lối suy nghĩ tự do, không rập khuôn, không giới hạn bản thân, có lối tư duy phê bình." +
                            "\r\n" +
                            "Không chấp nhận cuộc sống đơn điệu, tẻ nhạt, thích thú với những điều mới mẻ và thử thách. Linh hoạt và bí ẩn";
                    break;
            }
            return sfill;
        }

        protected string Cal_R1T_TextBox()
        {
            string sfill = "";
            switch (this.R1T)
            {
                case "As":
                    sfill = "Đặc điểm: Nhà duy tâm thực tế." + "\r\n" +
                            "\r\n" +
                            "Cần mẫn, chăm chỉ, có xu hướng làm việc theo tuần tự và tuân thủ quy trình. " + "\r\n" +
                            "Bảo thủ, không phô trương và giỏi giám sát công việc." + "\r\n" +
                            "Thật thà, cố chấp, làm việc cần cù, phấn đấu cho các nhu cầu cơ bản và an toàn." + "\r\n" +
                            "Ổn định và cố gắng phấn đấu." + "\r\n" +
                            "Nghiêm túc, ý chí mạnh mẽ." + "\r\n" +
                            "Hay che giấu cảm xúc, ít nói, và luôn phòng vệ." + "\r\n" +
                            "Quan tâm những việc đơn giản và cụ thể." + "\r\n" +
                            "Tiếp thu kiến thức như miếng bọt biển nhưng cần nhiều kiên nhẫn và thực hành." + "\r\n" +
                            "Phong cách làm việc: đơn giản, rõ ràng, bám sát quy trình.";
                    break;
                case "At":
                    sfill = "Đặc điểm: Nhà duy tâm lương thiện." + "\r\n" +
                            "\r\n" +
                            "Nhạy cảm nhưng đầy niềm đam mê và thực tế." + "\r\n" +
                            "Sống tình cảm và bốc đồng." + "\r\n" +
                            "Bảo thủ khi đưa ra quyết định, tiếp thu kiến thức như miếng bọt biển." + "\r\n" +
                            "Dễ dàng từ bỏ mục tiêu một khi không nhận được kết quả như mong đợi." + "\r\n" +
                            "Không mấy quan tâm đến ý kiến của người khác, có năng khiếu thưởng thức nghệ thuật." +
                            "\r\n" +
                            "Nghiêm túc, có trách nhiệm, chân thành." + "\r\n" +
                            "Phong cách làm việc: năng động, đi thẳng vào vấn đề, chân thành, thực tế.";
                    break;
                case "R":
                    sfill = "Đặc điểm: Nhà tư tưởng phê bình" + "\r\n" +
                            "Suy nghĩ phóng khoáng, độc lập." + "\r\n" +
                            "Tìm kiếm sự độc đáo, khác biệt từ người khác." + "\r\n" +
                            "Đam mê những điều mới lạ và bí ẩn. Tư duy sáng tạo." + "\r\n" +
                            "Khao khát tự do, hy vọng phá bỏ các khuôn khổ truyền thống" + "\r\n" +
                            "Óc quan sát nhạy bén, dể bị kích động (giàu cảm xúc)" + "\r\n" +
                            "Phản kháng, không bao giờ thỏa hiệp với những điều họ không muốn." + "\r\n" +
                            "Lối suy nghĩ tự do, không rập khuôn, không giới hạn bản thân, sở hữu tư duy phê bình." +
                            "\r\n" +
                            "Không chấp nhận cuộc sống đơn giản, tẻ nhạt, thích thú với những điều mới mẻ và thử thách. Linh hoạt và bí ẩn." +
                            "\r\n" +
                            "Đầu óc đầy sáng tạo. có thể đưa ra quyết định nhanh chóng và khả năng can thiệp khủng hoảng tốt." +
                            "\r\n" +
                            "Tư duy phê bình và luôn bộc phát nhiều ý tưởng mới." + "\r\n" +
                            "Thích hợp làm việc trong môi trường sáng tạo và SOHO." + "\r\n" +
                            "Không mấy quan tâm đến ý kiến người khác, luôn làm theo ý mình." + "\r\n" +
                            "Rất tò mò, có khả năng lập luận và tư duy ngược chiều tốt.";
                    break;
                case "U":
                    sfill = "Đặc điểm: Người cống hiến." + "\r\n" +
                            "\r\n" +
                            "Quan tâm tới mọi người, thích hợp với làm việc nhóm, năng động, tích cực, chấp hành tốt." +
                            "\r\n" +
                            "Tư duy linh hoạt, giàu lòng bao dung, dễ chịu" + "\r\n" +
                            "Không thích sự xung đột, nhiệt tình hỗ trợ và dễ chấp thuận ý kiến của người khác." +
                            "\r\n" +
                            "Có khả năng thích nghi với môi trường xung quanh, dễ dàng hòa hợp với mọi người." + "\r\n" +
                            "Hòa nhã, nhạy cảm, và thân thiện với mọi người." + "\r\n" +
                            "Quan tâm tới các mối quan hệ cá nhân, đồng cảm.";
                    break;
                case "Wd":
                    sfill = "Đặc điểm: Nhà xử lý đa tác vụ." + "\r\n" +
                            "\r\n" +
                            "Suy nghĩ hai chiều, đam mê thử thách." + "\r\n" +
                            "Không phải lúc nào cũng rập khuôn các quy tắc và luật lệ." + "\r\n" +
                            "Dễ thích nghi, có mối quan hệ giao tiếp tốt với mọi người xung quanh." + "\r\n" +
                            "Kĩ năng PR tốt, có thể nhanh chóng xây dựng các mối quan hệ xã hội." + "\r\n" +
                            "Hành vi đa dạng, luôn thay đổi nguyên tắc và lập trường." + "\r\n" +
                            "Có thể thích ứng với các thử thách trong hoàn cảnh khác nhau " + "\r\n" +
                            "Thích hợp với công việc quản lý dự án, quan hệ công chúng hay công việc kinh doanh.";
                    break;
                case "We":
                    sfill = "Đặc điểm: Người theo chủ nghĩa hoàn hảo:" + "\r\n" +
                            "\r\n" +
                            "Khả năng thiết lập mục tiêu và lập kế hoạch chi tiết rất tốt." + "\r\n" +
                            "Cẩn thận, chú ý đến độ chính xác của từng chi tiết." + "\r\n" +
                            "Năng lực quản lý tốt, độ chính xác cao, giàu khả năng phân tích, tận tâm, và rất cẩn trọng." +
                            "\r\n" +
                            "Sống thực tế, tạo lập và tuân thủ các chuẩn mục và hệ thống cao." + "\r\n" +
                            "Có thể hoàn thành tốt mục tiêu và công việc thông qua các kế hoạch cụ thể, chặt chẽ." +
                            "\r\n" +
                            "Là nhà tổ chức và lập kế hoạch bẩm sinh.";
                    break;
                case "Wi":
                    sfill = "Đặc điểm: Nhà quản lý tận tâm" + "\r\n" +
                            "Tìm kiếm sự thử thách, tận tâm, hay hoài nghi và luôn tự mâu thuẫn." +
                            "Hay tò mò, có xu hướng theo đuổi nhiều mục tiêu cùng lúc nhưng lại hay cân nhắc và do dự." +
                            "\r\n" +
                            "Thích thử thách, rất tỉ mỉ trong việc theo đuổi mục tiêu." + "\r\n" +
                            "Theo đuổi sự hoàn hảo, dễ bị chán nản nếu chưa đạt được mục tiêu." + "\r\n" +
                            "Chính xác, phân tích tận tâm, cẩn thận. " + "\r\n" +
                            "Người tìm kiếm chân lý, đòi hỏi sự chuẩn xác. Tiêu chuẩn cao, có hệ thống." + "\r\n" +
                            "Năng lực tổ chức bẩm sinh: người quản lý tự làm mọi việc, sáng tạo và duy trì hệ thống.";
                    break;
                case "Wp":
                    sfill = "Đặc điểm: Người theo đuổi trí tuệ ưu tú." + "\r\n" +
                            "\r\n" +
                            "Có năng lực thấu hiểu và tư duy tốt, có khả năng phân biệt sự việc rõ ràng, phát huy tốt khả năng lãnh đạo độc đáo trong nhóm và khả năng tư duy sáng tạo vượt trội." +
                            "\r\n" +
                            "Lối tư duy đa dạng, ấp ủ nhiều ý tưởng mới." + "\r\n" +
                            "Có nhận thức tuyệt vời, một nhà lãnh đạo rất ưu tú, có khả năng thuyết phục người khác." +
                            "\r\n" +
                            "Luôn hướng về phía trước, có năng lực tạo lập và khai phá các thị trường mới." + "\r\n" +
                            "Lạc quan, chu đáo và dễ được mọi người chấp nhận." + "\r\n" +
                            "Họ có phương pháp riêng để đạt được mục tiêu đặt ra." + "\r\n" +
                            "Có khả năng khám phá và thấu hiểu năng lực bản thân.";
                    break;
                case "Wpr":
                    sfill = "Đặc điểm: Nhà tư tưởng phê bình ưu tú." + "\r\n" +
                            "Tư duy phóng khoáng, độc lập." + "\r\n" +
                            "Tìm kiếm sự độc đáo, khác biệt từ người khác." + "\r\n" +
                            "Đam mê những điều mới lạ và bí ẩn. Tư suy đầy sáng tạo." + "\r\n" +
                            "Khao khát tự do, hy vọng phá bỏ các khuôn khổ truyền thống." + "\r\n" +
                            "Óc quan sát nhạy bén, dể bị kích động (giàu cảm xúc)." + "\r\n" +
                            "Phản kháng, không bao giờ thỏa hiệp với những điều họ không muốn." + "\r\n" +
                            "Lối suy nghĩ phóng khoáng, không rập khuôn, không giới hạn bản thân, tư duy phê bình." +
                            "\r\n" +
                            "Không chấp nhận cuộc sống đơn điệu, tẻ nhạt, thích thú với những điều mới mẻ và đầy thử thách. Linh hoạt và bí ẩn." +
                            "\r\n" +
                            "Đầu óc đầy sáng tạo, có thể đưa ra quyết định nhanh chóng và khả năng can thiệp khủng hoảng tốt." +
                            "\r\n" +
                            "Tư duy phê bình và luôn bộc phát nhiều ý tưởng mới." + "\r\n" +
                            "Thích hợp làm việc trong môi trường sáng tạo và SOHO." + "\r\n" +
                            "Không mấy quan tâm đến ý kiến người khác, luôn làm theo ý họ muốn." + "\r\n" +
                            "Rất tò mò, có khả năng lập luận và tư duy ngược chiều tốt.";
                    break;
                case "Ws":
                    sfill = "Đặc điểm: Mẫu người thể hiện." + "\r\n" +
                            "\r\n" +
                            "Luôn thúc đẩy bản thân trở thành người dẫn đầu: “The Best”" + "\r\n" +
                            "Thích sự cạnh tranh, thích cảm giác chiến thắng, có quyết tâm mạnh mẽ." + "\r\n" +
                            "Khi làm việc tập trung cao độ và rất nghiêm túc." + "\r\n" +
                            "Chủ quan, tự cho mình là trung tâm, chủ nghĩa cá nhân, rất khó bị thuyết phục." + "\r\n" +
                            "Chủ động, khả năng kiểm soát mạnh mẽ và phấn đấu hướng tới mục tiêu." + "\r\n" +
                            "Dám nghĩ dám làm, bốc đồng, cạnh tranh, năng động và có hiệu quả.";
                    break;
                case "Wt":
                    sfill = "Đặc điểm: Mẫu người thể hiện." + "\r\n" +
                            "\r\n" +
                            "Luôn thúc đẩy bản thân trở thành người dẫn đầu: “The Best”" + "\r\n" +
                            "Thích sự cạnh tranh, thích cảm giác chiến thắng, có quyết tâm mạnh mẽ." + "\r\n" +
                            "Khi làm việc tập trung cao độ và rất nghiêm túc." + "\r\n" +
                            "Chủ quan, tự cho mình là trung tâm, chủ nghĩa cá nhân, rất khó bị thuyết phục." + "\r\n" +
                            "Chủ động, khả năng kiểm soát mạnh mẽ và phấn đấu hướng tới mục tiêu." + "\r\n" +
                            "Dám nghĩ dám làm, bốc đồng, cạnh tranh, năng động và có hiệu quả.";
                    break;
                case "Wx":
                    sfill = "Đặc điểm: Nhà tư tưởng phê bình." + "\r\n" +
                            "\r\n" +
                            "Tính cách đa dạng, có nhiều ý tưởng mới lạ, độc đáo." + "\r\n" +
                            "Có chiều hướng hay thay đổi và không ổn định, năng lượng được phân tán ở khắp nơi." +
                            "\r\n" +
                            "Suy nghĩ phóng khoáng, chủ nghĩa cá nhân, khó đoán trước." + "\r\n" +
                            "Đòi hỏi sự khác biệt, độc đáo từ người khác." + "\r\n" +
                            "Thích những điều mới lạ và bí ẩn." + "\r\n" +
                            "Khao khát tự do, mong ước phá bỏ truyền thống." + "\r\n" +
                            "Óc quan sát nhạy bén, dễ bị kích động (giàu cảm xúc)" + "\r\n" +
                            "Phản kháng, không bao giờ thỏa hiệp với những điều họ không muốn." + "\r\n" +
                            "Lối suy nghĩ tự do, không rập khuôn, không giới hạn bản thân, có lối tư duy phê bình." +
                            "\r\n" +
                            "Không chấp nhận cuộc sống đơn điệu, tẻ nhạt, thích thú với những điều mới mẻ và thử thách. Linh hoạt và bí ẩn";
                    break;
            }
            return sfill;
        }

        #endregion


        public string TFRC_TextBox
        {
            get { return this.Cal_TFRC_TextBox(); }
        }

        #region TFRC TextBox trang Tổng số đường vân tay

        protected string Cal_TFRC_TextBox()
        {
            string sfill = "";
            if (TFRC < 60)
            {
                sfill = "TFRC < 60" + "\r\n" +
                        "Chỉ số TFRC của bạn cho thấy bạn có khả năng tiếp thu, học tập ở mức thấp. Thích hợp cho việc học và nghiên cứu một lĩnh vực. Bạn nên theo những gợi ý sau: " +
                        "\r\n" +
                        "* Cần kiên nhẫn trong việc học tập. Đòi hỏi nhiều thời gian và sự rèn luyện kiên trì." + "\r\n" +
                        "* Có khả năng đạt được thành công trong môi trường quen thuộc và ổn định.";
            }
            else if (TFRC < 100 && TFRC > 61)
            {
                sfill = "TFRC 60-100" + "\r\n" +
                        "Dựa theo số liệu kết quả,cho thấy khả năng học tập của bạn là bình thường. Thích hợp cho việc tập trung,tận tâm và nghiên cứu một hoặc hai lĩnh vực." +
                        "\r\n" +
                        "Gợi ý(khuyến nghị):" + "\r\n" +
                        "\r\n" +
                        "* Chìa khóa thành công là tập trung học tập trên một lĩnh vực cụ thể và trở thành chuyên gia về lĩnh vực đó." +
                        "\r\n" +
                        "* Không thích hợp cho việc đa nhiệm vụ hoặc đối phó với quá nhiều bài tập hoặc học nhiều môn cùng một lúc." +
                        "\r\n" +
                        "* Không phù hợp với nhiều công việc phức tạp." + "\r\n" +
                        "* Có thể trở thành chuyên gia về một lĩnh vực nào đó ,một số chuyên ngành như bác sĩ." + "\r\n" +
                        "* Nằm trong phạm vi trung bình của những người bình thường.";
            }
            else if (TFRC > 101 && TFRC < 150)
            {
                sfill = "TFRC 100-150" + "\r\n" +
                        "Dựa theo số liệu kết quả cho thấy khả năng học tập của bạn đang ở mức tốt. Bạn có khả năng học tập và phát triển đa ngành thông qua giáo dục và đào tạo." +
                        "\r\n" +
                        " Gợi ý(khuyến nghị):" + "\r\n" +
                        "* Cần phải có nhiều tình huống khác nhau trong môi trường học tập,như là tham gia vào các chương trình ngoại khóa" +
                        "\r\n" +
                        "  Hoạt động và tham gia vào các khóa học." + "\r\n" +
                        "* Sự đổi mới,đa dạng,đầy thử thách của các khóa học và phong cách học tập là cần thiết để tránh sự nhàm chán." +
                        "\r\n" +
                        "* Phân loại và ôn lại các bài học với các đối tượng khác nhau hoặc các phần và phân chia nội dung học tập." +
                        "\r\n" +
                        "Chia thành nhiều phần nghiên cứu sẽ hiệu quả hơn." + "\r\n" +
                        "* Thạm dự một khóa học cao cấp trong công việc để phát triển đầy đủ tiềm năng.";
            }
            else
            {
                sfill = "TFRC 151-200+ " + "\r\n" +
                        "Dựa theo số liệu kết quả cho thấy khả năng học tập của bạn đang ở mức tốt. Bạn có khả năng học tập và phát triển đa ngành thông qua giáo dục và đào tạo." +
                        "\r\n" +
                        "\r\n" +
                        "Gợi ý(khuyến nghị):" + "\r\n" +
                        "* Cần phải có nhiều tình huống khác nhau trong môi trường học tập,như là tham gia vào các chương trình ngoại khóa" +
                        "\r\n" +
                        "   Hoạt động và tham gia vào các khóa học." + "\r\n" +
                        "* Sự đổi mới,đa dạng,đầy thử thách của các khóa học và phong cách học tập là cần thiết để tránh sự nhàm chán." +
                        "\r\n" +
                        "* Phân loại và ôn lại các bài học với các đối tượng khác nhau hoặc các phần và phân chia nội dung học tập." +
                        "\r\n" +
                        "Chia thành nhiều phần nghiên cứu sẽ hiệu quả hơn." + "\r\n" +
                        "* Thạm dự một khóa học cao cấp trong công việc để phát triển đầy đủ tiềm năng.";
            }
            return sfill;
        }

        #endregion

        public string ATDR_TextBox1
        {
            get { return this.Cal_ATDR_Textbox1(); }
        }

        public string ATDL_TextBox2
        {
            get { return this.Cal_ATDL_Textbox2(); }
        }

        #region ATD Textbox  Chỉ số ATD và khả năng tiếp thu

        protected string Cal_ATDR_Textbox1()
        {
            string sfill = "";
            if (ATDR < 35)
            {
                sfill = "GÓC ĐỘ ATD VÀ ĐỘ NHẠY BÉN TRONG HỌC TẬP  < 35°" + "\r\n" +
                        "Não  trái có khả năng quan sát nhạy bén, nhanh nhẹn trong công việc, kỹ năng vận động tinh tế, điều đó chứng tỏ rằng bạn là người thông minh trong vấn đề học vấn, nhanh chóng tìm kiếm phương pháp học tập và logic các vấn đề để có thế hiểu rõ hơn. Tuy nhiên, bạn cần kiểm soát đến cảm xúc bất ổn, căng thẳng và lo lắng có thể phát sinh từ tính nhạy cảm của bạn." +
                        "\r\n" +
                        "Khuyến nghị:  " + "\r\n" +
                        "Xin chúc mừng về khả năng hiểu biết xuất chúng của bạn.Nhưng khuyến nghị rằng khi bạn phải đối diện với một vấn đề nào đó, bạn nên đối diện với một tinh thần thoải mái và tập thở bụng. Luôn mang theo một quyển sổ tay, để bạn có thể ghi lại tất cả các ý tưởng mà bạn có thể đột xuất nghĩ ra, rồi bạn sẽ thấy điều này vô cùng hữu ích cho bạn về sau. ";
            }
            else if (ATDR >= 36 && ATDR <= 40)
            {
                sfill = "GÓC ĐỘ ATD VÀ ĐỘ NHẠY BÉN TRONG HỌC TẬP 36° - 40°" + "\r\n" +
                        "Nằm trong giới hạn sinh lý học bình thường, nghĩa là tình trạng não trái ổn định và cân bằng về kỹ năng quan sát, khả năng thực hiện công việc, kỹ năng vận động cũng như nắm vững các phương pháp tiếp thu kiến thức và thông tin mới. Tiếp thu nhanh, phản ứng nhạy bén và có sự phối hợp mạnh mẽ giữa các cơ. Điều này thể hiện khả năng tiếp nhận và phản ứng nhanh với những điều mới mẻ và phương pháp học tập hiệu quả" +
                        "\r\n" +
                        "Khuyến nghị: " + "\r\n" +
                        "Rất tốt. Bạn khá là thông minh và có khả năng thể hiện đầy đủ sự tự tin và sự nhạy bén của bạn. Hãy vận dụng tối ưu trí tuệ, học hỏi, thực hành và bồi dưỡng kiến thức, phát huy năng lực để nâng cao hơn nữa tính chuyên nghiệp trong lĩnh vực của mình.";
            }
            else if (ATDR <= 45 && ATDR >= 41)
            {
                sfill = "Góc độ ATD & Độ nhạy bén trong học tập 41°-45°" + "\r\n" +
                        "Não trái có khả năng quan sát: phù hợp với phương pháp tiếp thu từng bước một, giúp phát huy được tiềm năng bẩm sinh. Tiến trình nắm vững các phương pháp và nội dung học tập của bạn từ từ và vững chắc." +
                        "\r\n" +
                        "\r\n" +
                        "Khuyến nghị:   " + "\r\n" +
                        "khi học, đòi hỏi phải xây dựng mục tiêu học tập của riêng mình, từ đó sẽ khơi dậy sự hứng thú và mong muốn học hỏi cao hơn. Hãy sử dụng những điểm mạnh của trí tuệ để dẫn dắt và hỗ trợ sự phát triển những mạnh còn hạn chế.";
            }
            else
            {
                sfill = "GÓC ĐỘ ATD VÀ ĐỘ NHẠY BÉN TRONG HỌC TẬP > 46°" + "\r\n" +
                        "Não trái phản ứng chậm trong việc tiếp nhận thông tin, vì vây cần nhiều thời gian và nhiều bước hay giai đoạn hơn. Quá trình tư duy sẽ khá lâu và khả năng vận động cũng khá yếu. Do đó, không nên dồn ép bản thân học nhiều thứ cùng một lúc mà nên chia quy trình ra thành nhiều bước nhỏ và dành nhiều thời gian để thực tập bằng cách lập đi lặp lại." +
                        "\r\n" +
                        "KHUYẾN NGHỊ:" + "\r\n" +
                        "Phản ứng chậm trong việc tiếp nhận thông tin không liên quan gì đến trí thông minh, chỉ đơn thuần là cần nhiều thời gian hơn để suy nghĩ và phản ứng. Những lời động viên và khuyến khích là rất cần thiết để xây dựng sự tự tin, vào những thời điểm thích hợp, rèn luyện thân thể và tập những môn thể dục giúp xây dựng kỹ năng, tốc độ như chạy bộ và chạy nước rút. Trong thời kỳ từ 0 đến 8 tuổi rất phù hợp với việc huấn luyện các kỹ năng, sự khéo léo của các ngón tay và cơ thể để tăng cường tính linh hoạt thể chất.";
            }
            return sfill;
        }

        protected string Cal_ATDL_Textbox2()
        {
            string sfill = "";
            if (ATDL < 35)
            {
                sfill = "GÓC ĐỘ ATD VÀ ĐỘ NHẠY BÉN TRONG HỌC TẬP  < 35°" + "\r\n" +
                        "Não phải có khả năng quan sát nhạy bén, nhanh nhẹn trong công việc, kỹ năng vận động tinh tế, điều đó chứng tỏ rằng bạn là người thông minh trong vấn đề học vấn, nhanh chóng tìm kiếm phương pháp học tập và logic các vấn đề để có thế hiểu rõ hơn. Tuy nhiên, bạn cần kiểm soát đến cảm xúc bất ổn, căng thẳng và lo lắng có thể phát sinh từ tính nhạy cảm của bạn." +
                        "\r\n" +
                        "Khuyến nghị:  " + "\r\n" +
                        "Xin chúc mừng về khả năng hiểu biết xuất chúng của bạn.Nhưng khuyến nghị rằng khi bạn phải đối diện với một vấn đề nào đó, bạn nên đối diện với một tinh thần thoải mái và tập thở bụng. Luôn mang theo một quyển sổ tay, để bạn có thể ghi lại tất cả các ý tưởng mà bạn có thể đột xuất nghĩ ra, rồi bạn sẽ thấy điều này vô cùng hữu ích cho bạn về sau. ";
            }
            else if (ATDL >= 36 && ATDL <= 40)
            {
                sfill = "GÓC ĐỘ ATD VÀ ĐỘ NHẠY BÉN TRONG HỌC TẬP 36° - 40°" + "\r\n" +
                        "Nằm trong giới hạn sinh lý học bình thường, nghĩa là tình trạng não trái ổn định và cân bằng về kỹ năng quan sát, khả năng thực hiện công việc, kỹ năng vận động cũng như nắm vững các phương pháp tiếp thu kiến thức và thông tin mới. Tiếp thu nhanh, phản ứng nhạy bén và có sự phối hợp mạnh mẽ giữa các cơ. Điều này thể hiện khả năng tiếp nhận và phản ứng nhanh với những điều mới mẻ và phương pháp học tập hiệu quả" +
                        "\r\n" +
                        "Khuyến nghị: " + "\r\n" +
                        "Rất tốt. Bạn khá là thông minh và có khả năng thể hiện đầy đủ sự tự tin và sự nhạy bén của bạn. Hãy vận dụng tối ưu trí tuệ, học hỏi, thực hành và bồi dưỡng kiến thức, phát huy năng lực để nâng cao hơn nữa tính chuyên nghiệp trong lĩnh vực của mình.";
            }
            else if (ATDL <= 45 && ATDL >= 41)
            {
                sfill = "Góc độ ATD & Độ nhạy bén trong học tập 41°-45°" + "\r\n" +
                        "Não phải có khả năng quan sát: phù hợp với phương pháp tiếp thu từng bước một, giúp phát huy được tiềm năng bẩm sinh. Tiến trình nắm vững các phương pháp và nội dung học tập của bạn từ từ và vững chắc." +
                        "\r\n" +
                        "\r\n" +
                        "Khuyến nghị:   " + "\r\n" +
                        "khi học, đòi hỏi phải xây dựng mục tiêu học tập của riêng mình, từ đó sẽ khơi dậy sự hứng thú và mong muốn học hỏi cao hơn. Hãy sử dụng những điểm mạnh của trí tuệ để dẫn dắt và hỗ trợ sự phát triển những mạnh còn hạn chế.";
            }
            else
            {
                sfill = "GÓC ĐỘ ATD VÀ ĐỘ NHẠY BÉN TRONG HỌC TẬP > 46°" + "\r\n" +
                        "Não phải/trái phản ứng chậm trong việc tiếp nhận thông tin, vì vây cần nhiều thời gian và nhiều bước hay giai đoạn hơn. Quá trình tư duy sẽ khá lâu và khả năng vận động cũng khá yếu. Do đó, không nên dồn ép bản thân học nhiều thứ cùng một lúc mà nên chia quy trình ra thành nhiều bước nhỏ và dành nhiều thời gian để thực tập bằng cách lập đi lặp lại." +
                        "\r\n" +
                        "KHUYẾN NGHỊ:" + "\r\n" +
                        "Phản ứng chậm trong việc tiếp nhận thông tin không liên quan gì đến trí thông minh, chỉ đơn thuần là cần nhiều thời gian hơn để suy nghĩ và phản ứng. Những lời động viên và khuyến khích là rất cần thiết để xây dựng sự tự tin, vào những thời điểm thích hợp, rèn luyện thân thể và tập những môn thể dục giúp xây dựng kỹ năng, tốc độ như chạy bộ và chạy nước rút. Trong thời kỳ từ 0 đến 8 tuổi rất phù hợp với việc huấn luyện các kỹ năng, sự khéo léo của các ngón tay và cơ thể để tăng cường tính linh hoạt thể chất.";
            }
            return sfill;
        }

        #endregion

        #region CheckBox TFRC

        public string TFRC_CB1
        {
            get { return this.Cal_TFRC_CheckBox1(); }
        }

        public string TFRC_CB2
        {
            get { return this.Cal_TFRC_CheckBox2(); }
        }

        public string TFRC_CB3
        {
            get { return this.Cal_TFRC_CheckBox3(); }
        }

        public string TFRC_CB4
        {
            get { return this.Cal_TFRC_CheckBox4(); }
        }

        private string Cal_TFRC_CheckBox1()
        {
            string sfill = "";
            if (TFRC < 60)
            {
                sfill = "X";
            }
            return sfill;
        }

        private string Cal_TFRC_CheckBox2()
        {
            string sfill = "";
            if (TFRC < 100 && TFRC > 61)
            {
                sfill = "X";
            }
            return sfill;
        }

        private string Cal_TFRC_CheckBox3()
        {
            string sfill = "";
            if (TFRC > 101 && TFRC < 150)
            {
                sfill = "X";
            }
            return sfill;
        }

        private string Cal_TFRC_CheckBox4()
        {
            string sfill = "";
            if (TFRC >= 150)
            {
                sfill = "X";
            }
            return sfill;
        }

        #endregion
    }
}