﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace VAKFomula.Model
{
    public class FingerImageModel
    {
        ////Singleton
        //private static FingerImageModel instance;
        ////Lazy Loading Managed
        //public static FingerImageModel Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = new FingerImageModel();
        //            instance.CheckSpecial();
        //        }
        //        else
        //        {
        //            instance.CheckSpecial();
        //        }
        //        return instance;
        //    }
        //}
        public string SaveImagePath { get; set; }
        public string ReportID { get; set; }
        public string UnsignCharCustomerName { get; set; }

        private string Front { get { return "F"; } }
        private string ImagePathMain { get { return SaveImagePath + @"\"; } }

        public Dictionary<string, Image> ToDictionary()
        {
            Dictionary<string, Image> dictionary = new Dictionary<string, Image>();
            dictionary.Add("Face_Pic", Face_image);
            dictionary.Add("L1T_Pic", L1FImage);
            dictionary.Add("L2T_Pic", L2FImage);
            dictionary.Add("L3T_Pic", L3FImage);
            dictionary.Add("L4T_Pic", L4FImage);
            dictionary.Add("L5T_Pic", L5FImage);

            dictionary.Add("R1T_Pic", R1FImage);
            dictionary.Add("R2T_Pic", R2FImage);
            dictionary.Add("R3T_Pic", R3FImage);
            dictionary.Add("R4T_Pic", R4FImage);
            dictionary.Add("R5T_Pic", R5FImage);

            return dictionary;
        }
        public Image Face_image { get { return this.GetImageFace("F"); } }
        public Image L1FImage { get { return this.GetImageF("1_1"); } }
        public Image L2FImage { get { return this.GetImageF("2_1"); } }
        public Image L3FImage { get { return this.GetImageF("3_1"); } }
        public Image L4FImage { get { return this.GetImageF("4_1"); } }
        public Image L5FImage { get { return this.GetImageF("5_1"); } }
        public Image R1FImage { get { return this.GetImageF("6_1"); } }
        public Image R2FImage { get { return this.GetImageF("7_1"); } }
        public Image R3FImage { get { return this.GetImageF("8_1"); } }
        public Image R4FImage { get { return this.GetImageF("9_1"); } }
        public Image R5FImage { get { return this.GetImageF("10_1"); } }

        protected Image GetImageF(string FingerType)
        {
            string path = "";
            Image image = new Bitmap(320, 480);
            path = this.ImagePathMain + ReportID + "_" + this.UnsignCharCustomerName + @"\";
            if (!Directory.Exists(path))
            {
                return new Bitmap(320, 480);
            }
            else
            {
                path += FingerType + ".b";
                if (File.Exists(path))
                {
                    image = Image.FromFile(path);
                }
            }
            return image;
        }
        protected Image GetImageFace(string FingerType)
        {
            string path = "";
            Image image = new Bitmap(320, 480);
            path = this.ImagePathMain + ReportID + "_" + this.UnsignCharCustomerName + @"\";
            if (!Directory.Exists(path))
            {
                return new Bitmap(320, 480);
            }
            else
            {
                path += this.ReportID + FingerType + ".bmp";
                if (File.Exists(path))
                {
                    image = Image.FromFile(path);
                }
            }
            return image;
        }
    }

}
