using System;

namespace iTalent.Entity
{
	/// <summary>
	/// Summary description for FIFingerRecord.
	/// </summary>
	public partial class FIFingerRecord
	{

      private Int32 _ID;
      private Int32 _AgencyID;
      private Int32 _ReportID;
      private string _Type;
      private decimal _ATDPoint;
      private decimal _RCCount;
      private string _FingerType;
      private Int16 _ScanCheck;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsReportIDNullable
      { get { return true;  } }
      public Int32 ReportID
      {
         get { return _ReportID;  }
         set { _ReportID = value; }
      }
      public bool IsTypeNullable
      { get { return true;  } }
      public string Type
      {
         get { return _Type;  }
         set { _Type = value; }
      }
      public bool IsATDPointNullable
      { get { return true;  } }
      public decimal ATDPoint
      {
         get { return _ATDPoint;  }
         set { _ATDPoint = value; }
      }
      public bool IsRCCountNullable
      { get { return true;  } }
      public decimal RCCount
      {
         get { return _RCCount;  }
         set { _RCCount = value; }
      }
      public bool IsFingerTypeNullable
      { get { return true;  } }
      public string FingerType
      {
         get { return _FingerType;  }
         set { _FingerType = value; }
      }
      public bool IsScanCheckNullable
      { get { return true;  } }
      public Int16 ScanCheck
      {
         get { return _ScanCheck;  }
         set { _ScanCheck = value; }
      }

      #endregion

      #region Constructors
      public FIFingerRecord()
      {
         Reset();
      }
      public FIFingerRecord(FIFingerRecord obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._ReportID = obj.ReportID;
	this._Type = obj.Type;
	this._ATDPoint = obj.ATDPoint;
	this._RCCount = obj.RCCount;
	this._FingerType = obj.FingerType;
	this._ScanCheck = obj.ScanCheck;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_Int32;
         _ReportID = EmptyValues.v_Int32;
         _Type = EmptyValues.v_string;
         _ATDPoint = EmptyValues.v_decimal;
         _RCCount = EmptyValues.v_decimal;
         _FingerType = EmptyValues.v_string;
         _ScanCheck = EmptyValues.v_Int16;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (Int32)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (Int32)reader["ReportID"];
		}
                if(!(reader["Type"] is DBNull))
		{
			obj.Type = (string)reader["Type"];
		}
                if(!(reader["ATDPoint"] is DBNull))
		{
			obj.ATDPoint = (decimal)reader["ATDPoint"];
		}
                if(!(reader["RCCount"] is DBNull))
		{
			obj.RCCount = (decimal)reader["RCCount"];
		}
                if(!(reader["FingerType"] is DBNull))
		{
			obj.FingerType = (string)reader["FingerType"];
		}
                if(!(reader["ScanCheck"] is DBNull))
		{
			obj.ScanCheck = (Int16)reader["ScanCheck"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["AgencyID"];
//            _ReportID = (reader["ReportID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ReportID"];
//            _Type = (reader["Type"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Type"];
//            _ATDPoint = (reader["ATDPoint"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["ATDPoint"];
//            _RCCount = (reader["RCCount"] is DBNull)?DalTools.EmptyValues.v_decimal:(decimal)reader["RCCount"];
//            _FingerType = (reader["FingerType"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["FingerType"];
//            _ScanCheck = (reader["ScanCheck"] is DBNull)?DalTools.EmptyValues.v_Int16:(Int16)reader["ScanCheck"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, Int32 AgencyID, Int32 ReportID, string Type, decimal ATDPoint, decimal RCCount, string FingerType, Int16 ScanCheck)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._ReportID = ReportID;
         this._Type = Type;
         this._ATDPoint = ATDPoint;
         this._RCCount = RCCount;
         this._FingerType = FingerType;
         this._ScanCheck = ScanCheck;
      }
   }
}