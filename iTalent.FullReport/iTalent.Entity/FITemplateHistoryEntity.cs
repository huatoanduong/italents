using System;

namespace iTalent.Entity
{
	/// <summary>
	/// Summary description for FITemplateHistory.
	/// </summary>
	public partial class FITemplateHistory
	{

      private Int32 _ID;
      private Int32 _AgencyID;
      private string _TemplateName;
      private DateTime _DateCreate;
      private string _KeyEn;
      private string _Remark;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsTemplateNameNullable
      { get { return true;  } }
      public string TemplateName
      {
         get { return _TemplateName;  }
         set { _TemplateName = value; }
      }
      public bool IsDateCreateNullable
      { get { return true;  } }
      public DateTime DateCreate
      {
         get { return _DateCreate;  }
         set { _DateCreate = value; }
      }
      public bool IsKeyEnNullable
      { get { return true;  } }
      public string KeyEn
      {
         get { return _KeyEn;  }
         set { _KeyEn = value; }
      }
      public bool IsRemarkNullable
      { get { return true;  } }
      public string Remark
      {
         get { return _Remark;  }
         set { _Remark = value; }
      }

      #endregion

      #region Constructors
      public FITemplateHistory()
      {
         Reset();
      }
      public FITemplateHistory(FITemplateHistory obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._TemplateName = obj.TemplateName;
	this._DateCreate = obj.DateCreate;
	this._KeyEn = obj.KeyEn;
	this._Remark = obj.Remark;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_Int32;
         _TemplateName = EmptyValues.v_string;
         _DateCreate = EmptyValues.v_DateTime;
         _KeyEn = EmptyValues.v_string;
         _Remark = EmptyValues.v_string;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (Int32)reader["AgencyID"];
		}
                if(!(reader["TemplateName"] is DBNull))
		{
			obj.TemplateName = (string)reader["TemplateName"];
		}
                if(!(reader["DateCreate"] is DBNull))
		{
			obj.DateCreate = (DateTime)reader["DateCreate"];
		}
                if(!(reader["KeyEn"] is DBNull))
		{
			obj.KeyEn = (string)reader["KeyEn"];
		}
                if(!(reader["Remark"] is DBNull))
		{
			obj.Remark = (string)reader["Remark"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["AgencyID"];
//            _TemplateName = (reader["TemplateName"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["TemplateName"];
//            _DateCreate = (reader["DateCreate"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["DateCreate"];
//            _KeyEn = (reader["KeyEn"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["KeyEn"];
//            _Remark = (reader["Remark"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Remark"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, Int32 AgencyID, string TemplateName, DateTime DateCreate, string KeyEn, string Remark)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._TemplateName = TemplateName;
         this._DateCreate = DateCreate;
         this._KeyEn = KeyEn;
         this._Remark = Remark;
      }
   }
}