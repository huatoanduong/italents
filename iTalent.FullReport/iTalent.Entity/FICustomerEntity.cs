using System;

namespace iTalent.Entity
{
	/// <summary>
	/// Summary description for FICustomer.
	/// </summary>
	public partial class FICustomer
	{

      private Int32 _ID;
      private Int32 _AgencyID;
      private string _ReportID;
      private DateTime _Date;
      private string _Name;
      private string _Gender;
      private DateTime _DOB;
      private string _Parent;
      private string _Tel;
      private string _Mobile;
      private string _Email;
      private string _Address1;
      private string _Address2;
      private string _City;
      private string _State;
      private string _ZipPosTalCode;
      private string _Country;
      private string _Remark;

      #region Properties

      public bool IsIDNullable
      { get { return false;  } }
      public Int32 ID
      {
         get { return _ID; }
         set { _ID = value; }
      }
      public bool IsAgencyIDNullable
      { get { return true;  } }
      public Int32 AgencyID
      {
         get { return _AgencyID;  }
         set { _AgencyID = value; }
      }
      public bool IsReportIDNullable
      { get { return true;  } }
      public string ReportID
      {
         get { return _ReportID;  }
         set { _ReportID = value; }
      }
      public bool IsDateNullable
      { get { return true;  } }
      public DateTime Date
      {
         get { return _Date;  }
         set { _Date = value; }
      }
      public bool IsNameNullable
      { get { return true;  } }
      public string Name
      {
         get { return _Name;  }
         set { _Name = value; }
      }
      public bool IsGenderNullable
      { get { return true;  } }
      public string Gender
      {
         get { return _Gender;  }
         set { _Gender = value; }
      }
      public bool IsDOBNullable
      { get { return true;  } }
      public DateTime DOB
      {
         get { return _DOB;  }
         set { _DOB = value; }
      }
      public bool IsParentNullable
      { get { return true;  } }
      public string Parent
      {
         get { return _Parent;  }
         set { _Parent = value; }
      }
      public bool IsTelNullable
      { get { return true;  } }
      public string Tel
      {
         get { return _Tel;  }
         set { _Tel = value; }
      }
      public bool IsMobileNullable
      { get { return true;  } }
      public string Mobile
      {
         get { return _Mobile;  }
         set { _Mobile = value; }
      }
      public bool IsEmailNullable
      { get { return true;  } }
      public string Email
      {
         get { return _Email;  }
         set { _Email = value; }
      }
      public bool IsAddress1Nullable
      { get { return true;  } }
      public string Address1
      {
         get { return _Address1;  }
         set { _Address1 = value; }
      }
      public bool IsAddress2Nullable
      { get { return true;  } }
      public string Address2
      {
         get { return _Address2;  }
         set { _Address2 = value; }
      }
      public bool IsCityNullable
      { get { return true;  } }
      public string City
      {
         get { return _City;  }
         set { _City = value; }
      }
      public bool IsStateNullable
      { get { return true;  } }
      public string State
      {
         get { return _State;  }
         set { _State = value; }
      }
      public bool IsZipPosTalCodeNullable
      { get { return true;  } }
      public string ZipPosTalCode
      {
         get { return _ZipPosTalCode;  }
         set { _ZipPosTalCode = value; }
      }
      public bool IsCountryNullable
      { get { return true;  } }
      public string Country
      {
         get { return _Country;  }
         set { _Country = value; }
      }
      public bool IsRemarkNullable
      { get { return true;  } }
      public string Remark
      {
         get { return _Remark;  }
         set { _Remark = value; }
      }

      #endregion

      #region Constructors
      public FICustomer()
      {
         Reset();
      }
      public FICustomer(FICustomer obj)
      {
	this._ID = obj.ID;
	this._AgencyID = obj.AgencyID;
	this._ReportID = obj.ReportID;
	this._Date = obj.Date;
	this._Name = obj.Name;
	this._Gender = obj.Gender;
	this._DOB = obj.DOB;
	this._Parent = obj.Parent;
	this._Tel = obj.Tel;
	this._Mobile = obj.Mobile;
	this._Email = obj.Email;
	this._Address1 = obj.Address1;
	this._Address2 = obj.Address2;
	this._City = obj.City;
	this._State = obj.State;
	this._ZipPosTalCode = obj.ZipPosTalCode;
	this._Country = obj.Country;
	this._Remark = obj.Remark;
      }
      #endregion

      public void Reset()
      {
         _ID = EmptyValues.v_Int32;
         _AgencyID = EmptyValues.v_Int32;
         _ReportID = EmptyValues.v_string;
         _Date = EmptyValues.v_DateTime;
         _Name = EmptyValues.v_string;
         _Gender = EmptyValues.v_string;
         _DOB = EmptyValues.v_DateTime;
         _Parent = EmptyValues.v_string;
         _Tel = EmptyValues.v_string;
         _Mobile = EmptyValues.v_string;
         _Email = EmptyValues.v_string;
         _Address1 = EmptyValues.v_string;
         _Address2 = EmptyValues.v_string;
         _City = EmptyValues.v_string;
         _State = EmptyValues.v_string;
         _ZipPosTalCode = EmptyValues.v_string;
         _Country = EmptyValues.v_string;
         _Remark = EmptyValues.v_string;
      }


        #region Hidden

/*
      //private void readProperties(IDataReader reader)
      //{
      //   try
      //   {
                if(!(reader["ID"] is DBNull))
		{
			obj.ID = (Int32)reader["ID"];
		}
                if(!(reader["AgencyID"] is DBNull))
		{
			obj.AgencyID = (Int32)reader["AgencyID"];
		}
                if(!(reader["ReportID"] is DBNull))
		{
			obj.ReportID = (string)reader["ReportID"];
		}
                if(!(reader["Date"] is DBNull))
		{
			obj.Date = (DateTime)reader["Date"];
		}
                if(!(reader["Name"] is DBNull))
		{
			obj.Name = (string)reader["Name"];
		}
                if(!(reader["Gender"] is DBNull))
		{
			obj.Gender = (string)reader["Gender"];
		}
                if(!(reader["DOB"] is DBNull))
		{
			obj.DOB = (DateTime)reader["DOB"];
		}
                if(!(reader["Parent"] is DBNull))
		{
			obj.Parent = (string)reader["Parent"];
		}
                if(!(reader["Tel"] is DBNull))
		{
			obj.Tel = (string)reader["Tel"];
		}
                if(!(reader["Mobile"] is DBNull))
		{
			obj.Mobile = (string)reader["Mobile"];
		}
                if(!(reader["Email"] is DBNull))
		{
			obj.Email = (string)reader["Email"];
		}
                if(!(reader["Address1"] is DBNull))
		{
			obj.Address1 = (string)reader["Address1"];
		}
                if(!(reader["Address2"] is DBNull))
		{
			obj.Address2 = (string)reader["Address2"];
		}
                if(!(reader["City"] is DBNull))
		{
			obj.City = (string)reader["City"];
		}
                if(!(reader["State"] is DBNull))
		{
			obj.State = (string)reader["State"];
		}
                if(!(reader["ZipPosTalCode"] is DBNull))
		{
			obj.ZipPosTalCode = (string)reader["ZipPosTalCode"];
		}
                if(!(reader["Country"] is DBNull))
		{
			obj.Country = (string)reader["Country"];
		}
                if(!(reader["Remark"] is DBNull))
		{
			obj.Remark = (string)reader["Remark"];
		}
//            _ID = (reader["ID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["ID"];
//            _AgencyID = (reader["AgencyID"] is DBNull)?DalTools.EmptyValues.v_Int32:(Int32)reader["AgencyID"];
//            _ReportID = (reader["ReportID"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ReportID"];
//            _Date = (reader["Date"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["Date"];
//            _Name = (reader["Name"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Name"];
//            _Gender = (reader["Gender"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Gender"];
//            _DOB = (reader["DOB"] is DBNull)?DalTools.EmptyValues.v_DateTime:(DateTime)reader["DOB"];
//            _Parent = (reader["Parent"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Parent"];
//            _Tel = (reader["Tel"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Tel"];
//            _Mobile = (reader["Mobile"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Mobile"];
//            _Email = (reader["Email"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Email"];
//            _Address1 = (reader["Address1"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Address1"];
//            _Address2 = (reader["Address2"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Address2"];
//            _City = (reader["City"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["City"];
//            _State = (reader["State"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["State"];
//            _ZipPosTalCode = (reader["ZipPosTalCode"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["ZipPosTalCode"];
//            _Country = (reader["Country"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Country"];
//            _Remark = (reader["Remark"] is DBNull)?DalTools.EmptyValues.v_string:(string)reader["Remark"];
      //   }
      //   catch (Exception ex)
      //   {
      //      //throw new DalException("Failed to read properties from DataReader.", ex);
      //      //throw new DalException("Failed to read properties from DataReader.\r\n" + ex.Message);
      //	throw ex;
      //   }
      //}
      
*/

        #endregion


      public void Fill(Int32 ID, Int32 AgencyID, string ReportID, DateTime Date, string Name, string Gender, DateTime DOB, string Parent, string Tel, string Mobile, string Email, string Address1, string Address2, string City, string State, string ZipPosTalCode, string Country, string Remark)
      {
         this._ID = ID;
         this._AgencyID = AgencyID;
         this._ReportID = ReportID;
         this._Date = Date;
         this._Name = Name;
         this._Gender = Gender;
         this._DOB = DOB;
         this._Parent = Parent;
         this._Tel = Tel;
         this._Mobile = Mobile;
         this._Email = Email;
         this._Address1 = Address1;
         this._Address2 = Address2;
         this._City = City;
         this._State = State;
         this._ZipPosTalCode = ZipPosTalCode;
         this._Country = Country;
         this._Remark = Remark;
      }
   }
}