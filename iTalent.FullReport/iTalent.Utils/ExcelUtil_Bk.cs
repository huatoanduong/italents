﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace iTalent.Utils
{
    public class ExcelUtil_Bkp
    {
        public static string ErrMsg = "";
        public static uint RowError = 0;
        public static uint CellError = 0;
        public static DataTable ReadFromExcel(string filePath)
        {
            DataTable dt = new DataTable();

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filePath, false))
            {

                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets =
                    spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart =
                    (WorksheetPart) spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                }

                foreach (Row row in rows) //this will also include your header row...
                {
                    DataRow dataRow = dt.NewRow();
                    IEnumerable<Cell> cells = row.Descendants<Cell>();
                    //get Row Index
                    RowError = row.RowIndex;
                    for (int i = 0; i < cells.Count(); i++)
                    {
                        CellError = (uint)i + 1;
                        dataRow[i] = GetCellValue(spreadSheetDocument, cells.ElementAt(i));
                    }

                    dt.Rows.Add(dataRow);
                }

            }
            dt.Rows.RemoveAt(0); //...so i'm taking it out here.
            return dt;
        }


        protected static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }
    }
}