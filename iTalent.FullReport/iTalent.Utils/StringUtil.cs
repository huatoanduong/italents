﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace iTalent.Utils
{
    public static class StringUtil
    {
        private static readonly MD5 md5 = MD5.Create();

        public static bool IsMD5Of(this string md5, string s)
        {
            return s.EncodeMD5() == md5;
        }

        public static string EncodeB64(this string s)
        {
            var data = Encoding.UTF8.GetBytes(s);
            return Convert.ToBase64String(data);
        }

        public static string DecodeB64(this string s)
        {
            var data = Convert.FromBase64String(s);
            return Encoding.UTF8.GetString(data);
        }

        public static string ToAscii(this string input)
        {
            var ss = Regex.Replace(input.ToLower(), "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
            ss = Regex.Replace(ss, "[đ]", "d");
            ss = Regex.Replace(ss, "[éèẻẽẹêếềểễệ]", "e");
            ss = Regex.Replace(ss, "[íìỉĩị]", "i");
            ss = Regex.Replace(ss, "[óòỏõọôốồổỗộơớờởỡợ]", "o");
            ss = Regex.Replace(ss, "[úùủũụưứừửữự]", "u");
            ss = Regex.Replace(ss, "[ýỳỷỹỵ]", "y");
            ss = Regex.Replace(ss, @"[\s]+", "-");
            return ss;
        }

        public static string ToShorten(this string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            var iNextSpace = input.LastIndexOf(" ", length);
            return (input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public static string ToCapitalize(this string input)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
        }
    }
}