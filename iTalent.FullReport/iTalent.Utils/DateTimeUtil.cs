using System;

namespace iTalent.Utils
{
    public class DateTimeUtil
    {
        public static DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }

        public static DateTime ConvertDate(string date)
        {
            return DateTime.Parse(date);
        }
    }
}