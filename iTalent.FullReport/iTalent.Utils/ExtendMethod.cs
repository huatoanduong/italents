﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace iTalent.Utils
{
    public static partial class ExtendMethod
    {
        public static char ThousandSeparator =
            Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);

        public static char DecimalSeparator =
            Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

        public static DateTime DefaultDateTime = new DateTime(1990, 1, 1, 0, 0, 0);

        public static bool DefaultBoolean = false;

        public const string DateTimeDisplayedFormat = "dd/MM/yyyy";
        public const string DateTimeCodeFormat = "yyyyMMdd";
        public const string DateTimeDatabaseFormat = "yyyy-MM-dd HH:mm:ss:ff";

        public static CultureInfo DefaultCulture = CultureInfo.InvariantCulture;

        public const byte DefaultInt = 0;

        private static MD5 md5 = MD5.Create();

        #region Number Extendsion Method

        public static int ToInt(this string input)
        {
            return int.Parse(input);
        }

        public static int ToIntOrDefault(this string input, int defaultValue = DefaultInt)
        {
            int i;
            bool res = int.TryParse(input, NumberStyles.Any, DefaultCulture, out i);

            return (res ? i : defaultValue);
        }

        public static decimal ToDecimal(this string input)
        {
            return decimal.Parse(input);
        }

        public static decimal ToDecimalOrDefault(this string input, decimal defaultValue = DefaultInt)
        {
            decimal i;
            //bool res = decimal.TryParse(input, NumberStyles.AllowHexSpecifier, DefaultCulture, out i);
            bool res = decimal.TryParse(input.Replace(",","."), NumberStyles.Any, DefaultCulture, out i);

            return (res ? i : defaultValue);
        }

        public static bool IsInt(this string input)
        {
            if (input.IsBlank())
            {
                return false;
            }
            int i;
            return int.TryParse(input, out i);
        }

        public static long ToLong(this string input)
        {
            return long.Parse(input);
        }

        public static long ToLongOrDefault(this string input, long defaultValue = DefaultInt)
        {
            long i;
            bool res = long.TryParse(input, NumberStyles.Any, DefaultCulture, out i);
            return (res ? i : defaultValue);
        }

        public static float ToFloat(this string input)
        {
            return long.Parse(input);
        }

        public static float ToFloatOrDefault(this string input, float defaultValue = DefaultInt)
        {
            long i;
            bool res = long.TryParse(input, NumberStyles.Any, DefaultCulture, out i);
            return (res ? i : defaultValue);
        }

        #region Default Value

        public static DateTime ToDefault(this DateTime? value)
        {
            return value ?? DefaultDateTime;
        }

        public static long ToDefault(this long? value)
        {
            return value ?? DefaultInt;
        }

        public static int ToDefault(this int? value)
        {
            return value ?? DefaultInt;
        }

        public static decimal ToDefault(this decimal? value)
        {
            return value ?? DefaultInt;
        }

        public static bool ToDefault(this bool? value)
        {
            return value ?? DefaultBoolean;
        }

        #endregion

        #endregion

        public static string ToUnsign(this string input)
        {
            if (input == null) return null;
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            var temp = input.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string ToSpaceIfBlank(this string input)
        {
            if (input.IsBlank())
            {
                return input.ToDefaultIfBlank(" ");
            }
            return input;
        }

        public static string ToNullIfEqual(this string input, string compareString)
        {
            if (input == compareString)
            {
                return null;
            }
            return input;
        }

        public static string ToDefaultIfBlank(this string input, string defaultString = "")
        {
            if (input.IsBlank())
            {
                return defaultString;
            }
            return input;
        }

        public static bool IsBlank(this string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }

        public static bool IsNull(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        public static string ToDbFormatString(this DateTime time)
        {
            return time.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static DateTime ToDateTime(this string input
            , string strFomatter = DateTimeDisplayedFormat
            , DateTime? defaultValue = null)
        {
            DateTime obj;
            try
            {
                try
                {
                    obj = DateTime.ParseExact(input, strFomatter,
                        CultureInfo.CurrentCulture);
                }
                catch (Exception)
                {

                    string[] time = input.Split('/');
                    int y = int.Parse(time[2]);
                    int m;
                    int d;

                    if (strFomatter.Contains("MM/dd/yyyy"))
                    {
                        m = int.Parse(time[0]);
                        d = int.Parse(time[1]);
                        obj = new DateTime(y, m, d);
                    }
                    else if (strFomatter.Contains("dd/MM/yyyy"))
                    {
                        m = int.Parse(time[1]);
                        d = int.Parse(time[0]);
                        obj = new DateTime(y, m, d);
                    }
                    else
                    {
                        obj = DateTime.Parse(input);
                    }
                }
            }
            catch (Exception)
            {
                try
                {
                    if (strFomatter.Contains("MM/dd/yyyy"))
                    {
                        obj = DateTime.ParseExact(input, strFomatter,
                            CultureInfo.CurrentCulture);
                    }
                    else if (strFomatter.Contains("dd/MM/yyyy"))
                    {
                        obj = DateTime.ParseExact(input, strFomatter,
                            CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        obj = DateTime.Parse(input);
                    }
                }
                catch (Exception)
                {
                    obj = defaultValue ?? DefaultDateTime;
                }
            }
            return obj;
        }

        public static bool IsMD5Of(this string value, string md5)
        {
            return value.EncodeMD5() == md5;
        }

        public static string EncodeMD5(this string value)
        {
            var bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(value));
            return Convert.ToBase64String(bytes);
        }

        public static string EncodeB64(this string value)
        {
            byte[] data = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(data);
        }

        public static string DecodeB64(this string value)
        {
            byte[] data = Convert.FromBase64String(value);
            return Encoding.UTF8.GetString(data);
        }

        public static string ToAscii(this string value)
        {
            string ss = Regex.Replace(value.ToLower(), "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
            ss = Regex.Replace(ss, "[đ]", "d");
            ss = Regex.Replace(ss, "[éèẻẽẹêếềểễệ]", "e");
            ss = Regex.Replace(ss, "[íìỉĩị]", "i");
            ss = Regex.Replace(ss, "[óòỏõọôốồổỗộơớờởỡợ]", "o");
            ss = Regex.Replace(ss, "[úùủũụưứừửữự]", "u");
            ss = Regex.Replace(ss, "[ýỳỷỹỵ]", "y");
            ss = Regex.Replace(ss, @"[\s]+", "-");
            return ss;
        }

        public static string ToShorten(this string value, int length)
        {
            if (value == null || value.Length < length)
                return value;
            int iNextSpace = value.LastIndexOf(" ", length);
            return (value.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public static string ToCapitalize(this string input)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
        }

        #region Image

        public static byte[] ToBytes(this System.Drawing.Image img)
        {
            if (img == null)
                return null;
            try
            {
                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public static System.Drawing.Image ToImage(this byte[] b)
        {
            if (b == null)
                return null;
            try
            {
                MemoryStream ms = new MemoryStream(b);
                System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                return returnImage;
            }
            catch
            {
                return null;
            }
        }

        #endregion
    }
}