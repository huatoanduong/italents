﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace iTalent.FingerprintScanDevice
{
    public class MessageDlg : Form
    {
        public delegate void OnCancelHandler();

        public delegate void OnSetParameterHandler(object value);

        private static MessageDlg _mDlg;
        private static AutoResetEvent _mInitEvent;
        private readonly IContainer components = null;
        private Button _mBtnCancel;
        private IntPtr _mHParent;
        private Label _mLblMessage;
        private string _mMessage;
        private string _mTitle;

        public MessageDlg()
        {
            InitializeComponent();
            _mMessage = string.Empty;
            _mTitle = string.Empty;
        }

        private string Message
        {
            set
            {
                if (value != null)
                {
                    _mMessage = value;
                }
            }
        }

        private IntPtr ParentWindow
        {
            set { _mHParent = value; }
        }

        private string Title
        {
            set
            {
                if (value != null)
                {
                    _mTitle = value;
                }
            }
        }

        public event OnCancelHandler CancelPressed;

        private void CloseDlg()
        {
            Close();
            Application.ExitThread();
            _mInitEvent.Set();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public static bool HideMessageDlg()
        {
            if (_mDlg != null)
            {
                _mInitEvent = new AutoResetEvent(false);
                _mDlg.Invoke(new OnCancelHandler(_mDlg.CloseDlg));
                _mInitEvent.WaitOne();
                _mInitEvent.Close();
                _mInitEvent = null;
                _mDlg = null;
            }
            return true;
        }

        private void InitializeComponent()
        {
            _mBtnCancel = new Button();
            _mLblMessage = new Label();
            SuspendLayout();
            _mBtnCancel.Location = new Point(0x11e, 0x18);
            _mBtnCancel.Name = "_mBtnCancel";
            _mBtnCancel.Size = new Size(0x4b, 0x17);
            _mBtnCancel.TabIndex = 0;
            _mBtnCancel.Text = @"Cancel";
            _mBtnCancel.UseVisualStyleBackColor = true;
            _mBtnCancel.Click += OnCancelClicked;
            _mLblMessage.Location = new Point(12, 9);
            _mLblMessage.Name = "_mLblMessage";
            _mLblMessage.Size = new Size(0x10c, 0x37);
            _mLblMessage.TabIndex = 1;
            _mLblMessage.Text = @"label1";
            _mLblMessage.TextAlign = ContentAlignment.MiddleCenter;
            AutoScaleDimensions = new SizeF(6f, 13f);
            //base.AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(0x175, 0x49);
            Controls.Add(_mLblMessage);
            Controls.Add(_mBtnCancel);
            Name = "MessageDlg";
            FormClosed += OnClosed;
            Load += OnLoad;
            ResumeLayout(false);
        }

        private void OnCancelClicked(object sender, EventArgs e)
        {
            if (CancelPressed != null)
            {
                CancelPressed();
            }
        }

        private void OnClosed(object sender, FormClosedEventArgs e)
        {
            if (CancelPressed != null)
            {
                CancelPressed();
            }
        }

        private void OnLoad(object sender, EventArgs e)
        {
            var control = FromHandle(_mHParent);
            var location = control.Location;
            var x = location.X + ((control.Width - Width)/2);
            var y = location.Y + ((control.Height - Height)/2);
            Location = new Point(x, y);
            Text = _mTitle;
            _mLblMessage.Text = _mMessage;
            _mInitEvent.Set();
        }

        private void SetMessage(object value)
        {
            _mLblMessage.Text = (string) value;
        }

        public static void SetMessageDlg(string szMessage)
        {
            if (_mDlg != null)
            {
                _mDlg.Invoke(new OnSetParameterHandler(_mDlg.SetMessage), szMessage);
            }
        }

        private static void ShowDialogThread(object param)
        {
            var parameters = (ThreadParameters) param;
            _mDlg = new MessageDlg();
            _mDlg.Message = parameters.Message;
            _mDlg.Title = parameters.Title;
            _mDlg.ParentWindow = parameters.HParent;
            if (parameters.Handler != null)
            {
                _mDlg.CancelPressed += parameters.Handler;
            }
            Application.Run(_mDlg);
        }

        public static bool ShowMessageDlg(IWin32Window hParent, string message, string title, OnCancelHandler handler)
        {
            if (_mDlg != null)
            {
                return false;
            }
            _mInitEvent = new AutoResetEvent(false);
            var parameter = new ThreadParameters();
            parameter.Message = message;
            parameter.Title = title;
            parameter.HParent = hParent.Handle;
            parameter.Handler = handler;

            var a = new Thread(ShowDialogThread);
            a.IsBackground = true;
            a.Start(parameter);
            _mInitEvent.WaitOne();
            _mInitEvent.Close();
            _mInitEvent = null;
            return true;
        }

        private class ThreadParameters
        {
            public OnCancelHandler Handler;
            public IntPtr HParent;
            public string Message;
            public string Title;
        }
    }
}