﻿using System;
using System.IO;
using System.Windows.Forms;
using C4FunComponent.Toolkit;
using iTalent.Analyse.Service;

namespace iTalent.ConvertExcelTemplate
{

    public partial class FrmConvertExcelTemplate : C4FunForm
    {
        public bool Issuccess { get; set; }
      
        public FrmConvertExcelTemplate()
        {
            InitializeComponent();
            Issuccess = false;
            txtInputNGFtemplate.Select();
        }

        private void btnToRptx_Click(object sender, System.EventArgs e)
        {
            ConvertToRptx();
        }

        private void btnExit_Click(object sender, System.EventArgs e)
        {
           Close();
        }

        private void btnBrowseOutput_Click(object sender, EventArgs e)
        {
            txtOutput.Text = getSavepath(txtInputNGFtemplate.Text);
        }

        private string getBrowerPath(string filter)
        {
            string path = "";
            OpenFileDialog dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = filter,
                Multiselect = false
            };


            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path = dialog.FileName;
            }

            return path;
        }

        private string getSavepath(string inputPath)
        {
            string path = "";
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                path = dialog.SelectedPath;
            }
            return path;
        }

        private bool ValidateInput()
        {
            //string inputPath = txtInputNGFtemplate.Text;
            //string outputPath = txtOutput.Text;
            //string errMsg;

            //if (inputPath.IsBlank())
            //{
            //    errMsg = ICurrentSessionService.VietNamLanguage ?
            //        "Vui lòng chọn file nhập"
            //        : "Please choose the input file";
            //    MessageBox.Show(errMsg);
            //    return false;
            //}

            //if (outputPath.IsBlank())
            //{
            //    errMsg = ICurrentSessionService.VietNamLanguage ?
            //        "Vui lòng chọn file xuất"
            //        : "Please choose the output file";
            //    MessageBox.Show(errMsg);
            //    return false;
            //}

            //if (!File.Exists(inputPath))
            //{
            //    errMsg = ICurrentSessionService.VietNamLanguage
            //        ? "Không tìm thấy file đưa vào. Vui lòng chọn lại"
            //        : "Cannot file input file. Please check again";
            //    MessageBox.Show(errMsg);
            //    return false;
            //}

            //if (File.Exists(outputPath))
            //{
            //    errMsg = ICurrentSessionService.VietNamLanguage
            //        ? "Tên file xuất ra đã tồn tại. Vui lòng chọn đường dẫn khác"
            //        : "Output file is already exist. Please choose another path";
            //    MessageBox.Show(errMsg);
            //    return false;
            //}

            return true;
        }

        private void ConvertToRptx()
        {
            string nfgTemplate = txtInputNGFtemplate.Text;
            string heSoTiemNang = txtInputHeSoTiemNang.Text;
            string dacTinhTuDuy = txtInputDacTinhTuDuy.Text;
            string dacTinhTiemThuc = txtInputDacTinhTiemThuc.Text;
            string top3 = txtInputTop3.Text;
            string hanhViAdult = txtInputHanhVi_Adult.Text;
            string khuyenNghiAdult = txtInputKhuyenNghi_Adult.Text;
            string hanhViKid = txtInputHanhVi_Kid.Text;
            string khuyenNghiKid = txtInputKhuyenNghi_Kid.Text;

            string outputPath = txtOutput.Text;
            if (outputPath=="")
            {
                MessageBox.Show("Please choose output file!");
                return;
            }

            if (!ValidateInput())
            {
                return;
            }
            bool kq = false;
            IExcelTemplateService service = new ExcelTemplateService();
            var namefile = "";
            if (File.Exists(nfgTemplate))
            {
                namefile = Path.Combine(outputPath, "1.apk");
                service.EncryptFile(nfgTemplate, namefile);
                kq = true;
            }
           
            if (File.Exists(heSoTiemNang))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "2.apk");
                service.EncryptFile(heSoTiemNang, namefile);
                kq = true;
            }
            
            if (File.Exists(dacTinhTuDuy))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "3.apk");
                service.EncryptFile(dacTinhTuDuy, namefile);
                kq = true;
            }
           
            if (File.Exists(dacTinhTiemThuc))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "4.apk");
                service.EncryptFile(dacTinhTiemThuc, namefile);
                kq = true;
            }
          
            if (File.Exists(top3))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "5.apk");
                service.EncryptFile(top3, namefile);
                kq = true;
            }
           
            if (File.Exists(hanhViAdult))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "6.apk");
                service.EncryptFile(hanhViAdult, namefile);
                kq = true;
            }
           
            if (File.Exists(khuyenNghiAdult))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "7.apk");
                service.EncryptFile(khuyenNghiAdult, namefile);
                kq = true;
            }
          
            if (File.Exists(hanhViKid))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "8.apk");
                service.EncryptFile(hanhViKid, namefile);
                kq = true;
            }
          
            if (File.Exists(khuyenNghiKid))
            {
                kq = false;
                namefile = Path.Combine(outputPath, "9.apk");
                service.EncryptFile(khuyenNghiKid, namefile);
                kq = true;
            }

            if(kq)
                MessageBox.Show("Successfully convert!");
        }

        private void btnBrowseInput_Click(object sender, EventArgs e)
        {
            txtInputNGFtemplate.Text = getBrowerPath("Excel file|*.xlsx"); 
        }
        private void btnInputHeSoTiemNang_Click(object sender, EventArgs e)
        {
            txtInputHeSoTiemNang.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputDacTinhTuDuy_Click(object sender, EventArgs e)
        {
            txtInputDacTinhTuDuy.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputDacTinhTiemThuc_Click(object sender, EventArgs e)
        {
            txtInputDacTinhTiemThuc.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputTop3_Click(object sender, EventArgs e)
        {
            txtInputTop3.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputHanhVi_Adult_Click(object sender, EventArgs e)
        {
            txtInputHanhVi_Adult.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputKhuyenNghi_Adult_Click(object sender, EventArgs e)
        {
            txtInputKhuyenNghi_Adult.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputHanhVi_Kid_Click(object sender, EventArgs e)
        {
            txtInputHanhVi_Kid.Text = getBrowerPath("Excel file|*.xlsx");
        }

        private void btnInputKhuyenNghi_Kid_Click(object sender, EventArgs e)
        {
            txtInputKhuyenNghi_Kid.Text = getBrowerPath("Excel file|*.xlsx");
        }
    }
}