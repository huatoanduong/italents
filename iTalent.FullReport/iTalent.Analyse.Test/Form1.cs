﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTalent.Analyse.Commons;
using iTalent.Analyse.Repository;
using iTalent.Analyse.Service;
using iTalent.Entity;
using iTalent.Utils;

namespace iTalent.Analyse.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //string dbFile = @"D:\Projects\Toro\Toro-iTalent\iTalent.VAK\database\FINGERPRINTANALYSIS2.mdb";
            //UnitOfWorkFactory.DesDirNameDatabase =
            //    //Path.GetFullPath(dbFile);
            //    dbFile;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.OpenConnection();
                IFIAgencyService service = new FIAgencyService(unitOfWork);
                FIAgency obj = service.CreateEntity();
                bool res = service.Add(obj);
                if (res)
                {
                    MessageBox.Show("Done");
                }
                else
                {
                    MessageBox.Show(service.ErrMsg);
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(UnitOfWorkFactory.DesDirNameDatabase);

            //using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork()    IFIAgencyService service = new FIAgencyService(unitOfWork);)
            //{
            //    iTalent.Entities.FIAgency agency = service.Find(2);
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
            {
                FIDeviceService service = new FIDeviceService(unitOfWork);
                FIDevice device = service.Find(1);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtKeyXacNhan.Text = GeneratePos.KeyGenerate();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string s = @"C:\Dir\";
            string fileName = "dddd.txt";
            List<string> strList = s.Split(new char[] {'|'}, StringSplitOptions.None).ToList();

            s = Path.Combine(s, fileName);
            MessageBox.Show(s);
        }
        private void DangKy()
        {
            try
            {
                using (IUnitOfWorkAsync unitOfWork = UnitOfWorkFactory.MakeUnitOfWork())
                {
                    unitOfWork.OpenConnection();
                    IFIPointModelService service = new FIPointModelService(unitOfWork);
                    IFIAgencyService serviceAg = new FIAgencyService(unitOfWork);
                    ICurrentSessionService.CurAgency = serviceAg.FindBySeckey("Nn/tf1f");
                    string key = txtKeyXacNhan.Text.Trim();
                    bool kq = service.RegisterLicense(key);
                    if (!kq)
                    {
                        MessageBox.Show(service.ErrMsg, @"Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    int slThem = service.GetNumAddKey(key);
                    MessageBox.Show("Nhập Key hợp lệ.\nCó: " + slThem + @" báo cáo được thêm vào hệ thống.", @"Thông Báo",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    int sl = service.GetNumberPointLeft(ICurrentSessionService.CurAgency.ID);
                    labNumber.Text = sl.ToString();
                }

                txtKeyXacNhan.Text = "";
                txtKeyXacNhan.Select();
            }
            catch (Exception ex)
            {
                labNumber.Text = @"0";
                MessageBox.Show(ex.Message, @"Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //throw;
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            DangKy();
        }
    }
}