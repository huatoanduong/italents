﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTalent.Analyse.Util;
using iTalent.Utils;

namespace iTalent.Analyse.Test
{
    public partial class frmTest2 : Form
    {
        private int i = 0;

        public frmTest2()
        {
            InitializeComponent();
            i = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            i += 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            i += 2;
            MessageBox.Show("2 click " + i);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            string filePath = @"D:\test.xlsx";
            DataTable dt = ExcelUtil_Bkp.ReadFromExcel(filePath);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string filePath = @"D:\test.docx";
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("CUSNAME", "Toan Duong");
            MSWordUtil.MergeAllTextbox(filePath);
            MSWordUtil.ReplaceFooterTextbox(filePath, dictionary);
        }

        private void WriteWordFile(string file)
        {
        }
    }
}